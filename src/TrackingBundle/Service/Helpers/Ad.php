<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 01/06/17
 * Time: 00:06
 */

namespace TrackingBundle\Service\Helpers;

use AppBundle\Entity\Notification;
use AppBundle\Entity\Payment\Order;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;

class Ad extends Agency
{

    /**
     *  constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->type = "ad";
    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     * @param boolean $trackingLater
     */
    function process($order, $advantage, $trackingLater){
        if(!$trackingLater) {
            $this->registerAction($order, $advantage);
        }
    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     * @return bool
     */
    function execute($order, $advantage)
    {
        $adsList = $this->em->getRepository("AppBundle:Ad")->getValidAdsByGroup($advantage->getAgency()->getGroup());
        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($adsList as $ad) {
            $this->getAvailableNumber($ad, $advantage);;
        }
        // add notification
        $notification = new Notification();
        $notification->setSubject("Annonces");
        $notification->setMessage("ads.auto_tracking_ok");
        $notification->setMessageParams(["%total%" => count($adsList)]);
        $notification->setUser($order->getUser());
        $notification->setRoute("ads_index");

        $this->em->persist($notification);
        $this->em->flush();

        return true;
    }
    /**
     * @param $ad
     * @return null|array
     */
    function getExtratTag($ad){
        return ["tag4" => $ad->getId()] ;
    }


}