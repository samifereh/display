<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 21/05/17
 * Time: 15:49
 */

namespace TrackingBundle\Service\Helpers;

use AppBundle\Entity\Configuration\AdvantageOption;
use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\Order;
use AppBundle\Entity\PendingActions;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;
use TrackingBundle\Entity\Phone;

class Agency
{

    /**
     * @var Container
     */
    protected $container;
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Symfony\Bridge\Monolog\Logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $type;

    /**
     *  constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
        $this->logger = $this->container->get('app.tracking.logger');
        $this->type = "agency";
    }


    /**
     * @param Order $order
     * @param Group $agency
     * @param AdvantageOption $advantageOption
     * @param boolean $trackingLater
     * @return boolean
     */
    public function setAdvantage(Order $order, Group $agency, AdvantageOption $advantageOption, $trackingLater = false){



        $this->logger->notice("[{$this->type}]".'setAgencyAdvantage : '.$agency->getName());
        /** @var Advantage $existantAdvantage */
        $existantAdvantage = $this->container->get('doctrine')->getRepository("TrackingBundle:Advantage")->
        findExistantTrackingAdvantage($agency->getAgency(), $this->type, $advantageOption->getAdvantage());
        $advantage = $existantAdvantage ? $existantAdvantage : new Advantage();


        if(!$existantAdvantage) $advantage->setDateBegin(new \DateTime('now'));

        $end     = !$existantAdvantage ? new \DateTime('now') : $existantAdvantage->getDateEnd();
        $advantage->setDateEnd($advantageOption->getMonth() > 0 ? $end->add(new \DateInterval("P{$advantageOption->getMonth()}M") ) : null);
        $advantage->setAmount($order->getDetails()['quantity']);
        $advantage->setAdvantage($advantageOption->getAdvantage());
        $advantage->setAdvantageOption($advantageOption);
        $advantage->setEnabled(true);
        $advantage->setPayed(true);
        $advantage->setAgency($agency->getAgency());
        $advantage->setType($this->type);
        $this->em->persist($advantage);
        $this->em->flush();
        $this->process($order, $advantage, $trackingLater);
        return true;

    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     * @param boolean $trackingLater
     */
    function process($order, $advantage, $trackingLater){

        $this->getAvailableNumber($advantage->getAgency(), $advantage);
    }



    /***
     * get available number based on agency information
     * try to check phone pattern, if like 33X, we use the 3 numbers to get  wannaspeak number,
     * if is like 0Y then use 33Y like pattern else check Zipcode XY... and pattern is like FRXY
     * otherwise get any available number
     * @param object $provider
     * @param Advantage $advantage
     * @return string
     */
    function getAvailableNumber($provider, $advantage)
    {

        $agency = $advantage->getAgency();
        $did_pattern = "";
        $trackPhone = "";
        $phone = $agency->getTelephone();
        if ($this->container->get("app.tracking.export.helpers")->checkPhoneTracking($provider) == "") {
            // has phoneNumber
            if ($agency->getTelephone() != "") {

                if (preg_match("/^33\d/", $phone, $output)) {
                    $did_pattern = $output[0] . "%";
                }
                if ($did_pattern == "" && preg_match("/^0\d/", $phone, $output)) {
                    $phone = preg_replace("/^0/", "33", $phone,1);
                    $did_pattern = str_replace("0", "33", $output[0]) . "%";
                }
            }
            // try to get available number with phone Pattern
            if ($did_pattern) {
                $trackPhone = $this->addTrackingNumber($provider, $advantage, $did_pattern, $phone);
            }

            //else check codepostal
            if($trackPhone == "" && $agency->getCodePostal() != ""){
                $did_pattern = "FR".substr($agency->getCodePostal(), 0, 2);
                $trackPhone = $this->addTrackingNumber($provider, $advantage, $did_pattern, $phone);

            }
            //else any nunmber
            if($trackPhone == "")
                $trackPhone = $this->addTrackingNumber($provider, $advantage, "33%", $phone);

            if($trackPhone != "" ) {
                $provider->setTrackingPhone($trackPhone);
                $this->setTrackingDetail($trackPhone, $advantage);
                $this->em->persist($provider);
                $this->em->flush();
            }else{
                $this->logger->error("[{$this->type}]"."Can't get available Phone tracking ");
                return false;
            }
        }elseif(!$this->addTrackingNumber($provider, $advantage, $provider->getTrackingPhone(), $phone)){
            $this->logger->error("[{$this->type}]"."Can't get available Phone tracking ");
            return false;
        }

        $this->logger->notice("[{$this->type}]".'Phone tracking : '.$provider->getTrackingPhone());
        return $provider->getTrackingPhone();
    }


    /**
     * @param object $provider
     * @param Advantage $advantage
     * @param string $trackingPhone
     * @param string $destination
     * @return string
     */
    function addTrackingNumber($provider, $advantage, $trackingPhone, $destination){

        $method = "add";
        if(strpos("%",$trackingPhone) === false){
            $exist = $this->container->get('doctrine')->getRepository("TrackingBundle:Phone")->findOneBy(["phone" => $trackingPhone]);
            if($exist)
                $method = "modify";
        }
        $params = [
            'api' => 'ct',
            'method' => $method ,
            'destination' => $destination,
            'did' => $trackingPhone,
            'name' => "Commiti-".$advantage->getAgency()->getSlug()."-".$this->type."-".$provider->getId(),
            'startdate' => $advantage->getDateBegin()->format("Y-m-d"),
            'stopdate' => $advantage->getDateEnd()->format("Y-m-d")
        ];

        $params["tag1"] = (
                $advantage->getAgency()->getGroup() && $advantage->getAgency()->getGroup()->getParentGroup() ?
                    $advantage->getAgency()->getGroup()->getParentGroup()->getId() : ""
        );
        $params["tag2"] = $advantage->getAgency()->getGroup()->getId() ;
        $params["leg2"] = "voicemail_".$advantage->getAgency()->getSlug()."-".$advantage->getAgency()->getVoicemailVersion();
        $extraTag = $this->getExtratTag($provider);
        $params = array_merge($params, $extraTag);

        if($advantage->getAgency()->getEmailAlias())
            $params["email"] = $advantage->getAgency()->getEmailAlias();
        elseif ($advantage->getAgency()->getEmail())
            $params["email"] = $advantage->getAgency()->getEmail();


        $this->logger->notice("[{$this->type}]".'Wannaspeak ADD args ', $params);

        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);

        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);
            $this->logger->notice("[{$this->type}]".'Phone tracking : '.$trackingPhone);
            if($response->error){
                $this->logger->error("[{$this->type}]"."[Error] Add Phone tracking {$trackingPhone}: ".$response->error->txt);
                return false;
            };
            return (isset($response->data->did) ? $response->data->did : $response->data->ok);
        }
        return false;
    }


    /**
     * @param object $provider
     * @return array
     */
    function getExtratTag($provider){
        return [];
    }

    /**
     * add new Tracking Phone detail if not exist
     * @param string $phone
     * @param Advantage $advantage
     */
    function setTrackingDetail($phone, $advantage){
        $phoneRepo = $this->em->getRepository("TrackingBundle:Phone");
        $trackingPhone = $phoneRepo->findOneBy(["phone" => $phone]);
        //not exist => create it
        if(!$trackingPhone){
            $this->logger->notice("[{$this->type}]".'Create Tracking Phone Number : '.$phone);
            $trackingPhone = new Phone();
            $trackingPhone->setPhone($phone);
            $trackingPhone->setAdvantage($advantage);
            $this->em->persist($trackingPhone);
        }

    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     */
    function registerAction($order, $advantage){

        $args = ["order" => $order->getId(), "advantage" => $advantage->getId()];
        $pendingAction = new PendingActions();
        $pendingAction->setService("app.tracking.call");
        $pendingAction->setArgs($args);
        $this->em->persist($pendingAction);
        $this->em->flush();
    }

}