<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 21/05/17
 * Time: 15:49
 */

namespace TrackingBundle\Service\Helpers;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Payment\Order;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;

class Portal extends Agency
{

    /**
     *  constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->type = "portal";
    }


    /**
     * @param Order $order
     * @param Advantage $advantage
     * @param boolean $trackingLater
     */
    function process($order, $advantage, $trackingLater){
        if(!$trackingLater) {
            $group = $advantage->getAgency()->getGroup();
            /** @var PortalRegistration $portalRegistration */
            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {
                if (
                    $portalRegistration->getBroadcastPortal()->getCluster() == "PAYANT" &&
                    $portalRegistration->getIsEnabled()

                )
                    $this->getAvailableNumber($portalRegistration, $advantage);
            }
        }
    }


    /**
     * @param $portalRegistration
     * @return null|array
     */
    function getExtratTag($portalRegistration){
        return ["tag3" => $portalRegistration->getBroadcastPortal()->getId()] ;
    }

}