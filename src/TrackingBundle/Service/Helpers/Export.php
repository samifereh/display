<?php
/**
 * Created by PhpStorm.
 * User: anis kossentini
 * Date: 04/05/17
 * Time: 17:33
 */

namespace TrackingBundle\Service\Helpers;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class Export
{
    /**
     * @var Container
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
        $this->logger = $this->container->get('app.tracking.logger');
    }


    function getNiceNumber($phone){
        return preg_replace("/^33/", "0", $phone,1);
    }

    /**
     * get Phone tracking form agency
     * @param Agency $ad
     * @return string
     */
    function getAgencyPhoneTracking(Agency $agency){

        return $this->getNiceNumber(
                ($agency->getTrackingPhone() && $this->checkPhoneTracking($agency)) ?
                    $agency->getTrackingPhone() : $agency->getTelephone()
        );
    }


    /**
     * get Phone tracking form ad, portal or agency
     * @param Ad $ad
     * @param Portal $portal
     * @return string
     */
    function getAdPhoneTracking(Ad $ad,PortalRegistration $portal = null){

        return $this->getNiceNumber(
            ($ad->getTrackingPhone() && $this->checkAdPhoneTracking($ad)) ? $ad->getTrackingPhone() :
                (
                    ($ad->getProgram() && $this->checkPhoneTracking($ad->getProgram())) ?
                        $ad->getProgram()->getTrackingPhone() :
                        (
                            ($portal && $portal->getTrackingPhone() && $this->checkPortalPhoneTracking($portal)) ?
                                $portal->getTrackingPhone() :
                                $this->getAgencyPhoneTracking( $ad->getGroup()->getAgency())
                        )
                )
        );
    }

    /**
     * get Ad Phone tracking, check if not expiry
     * @param Ad $ad
     * @return string
     */
    function checkAdPhoneTracking(Ad $ad){
        return $this->checkPhoneTracking($ad);
    }

    /**
     * get PortalRegistration Phone tracking, check if not expiry
     * @param Agency $agency
     * @return string
     */
    function checkPortalPhoneTracking(PortalRegistration $portalRegistration){
        return $this->checkPhoneTracking($portalRegistration);
    }


    /**
     * get Agency Phone tracking, check if not expiry
     * @param Agency $agency
     * @return string
     */
    function checkAgencyPhoneTracking(Agency $agency){
        return $this->checkPhoneTracking($agency);
    }


   /**
     * get Phone tracking, check if not expiry
     * @param $content
     * @return string
     */
   function checkPhoneTracking($content){

        $trackingPhone = $content->getTrackingPhone();
        if($this->isValid($trackingPhone))
            return $trackingPhone;

        $content->setTrackingPhone("");
        $this->em->persist($content);
        $this->em->flush();
        return "";

   }


    function isValid($trackingPhone){

        if(!$trackingPhone)
            return false;

        $phoneRepository = $this->em->getRepository("TrackingBundle:Phone");
        /** @var \TrackingBundle\Entity\Phone $phoneContent  **/
        $phoneContent = $phoneRepository->findOneBy(["phone" => $trackingPhone]);

        if(!$phoneContent || !$phoneContent->getAdvantage())
            return false;

        $now = new \DateTime();
        if($now <= $phoneContent->getAdvantage()->getDateEnd())
            return true;


        $this->container->get("app.tracking.call")->deleteTrackingNumber($trackingPhone);
        $this->em->remove($phoneContent);
        $this->em->flush();
        return false;
    }

    /** remove trackingPhone from db and wannaspeak
     * @param string $trackingPhone
     * @return bool
     */
    function remove($trackingPhone){

        if(!$trackingPhone)
            return false;

        $phoneRepository = $this->em->getRepository("TrackingBundle:Phone");
        /** @var \TrackingBundle\Entity\Phone $phoneContent  **/
        $phoneContent = $phoneRepository->findOneBy(["phone" => $trackingPhone]);

        if(!$phoneContent )
            return false;


        $this->container->get("app.tracking.call")->deleteTrackingNumber($trackingPhone);
        $this->em->remove($phoneContent);
        $this->em->flush();
        return false;
    }
}