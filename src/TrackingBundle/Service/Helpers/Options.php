<?php
/**
 * Created by PhpStorm.
 * User: anis kossentini
 * Date: 11/05/17
 * Time: 18:33
 */

namespace TrackingBundle\Service\Helpers;


use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\AdvantageOption;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class Options
{
    /**
     * @var Container
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
        $this->logger = $this->container->get('app.tracking.logger');
    }


    /**
     * get Advantage options basic on advantage and request
     * @param Advantage $advantage
     * @param int $trackingCount
     * @param AdvantageOption $advantageOption
     * @return mixed
     */
    function getAdvantageOptions(Advantage $advantage, $trackingCount, AdvantageOption $advantageOption = null){
        $options = null;
        if($trackingCount) {

            $optionsRepo = $this->em->getRepository('AppBundle:Configuration\AdvantageOption');
            // try to find with the exactly number
            $criteria = [
                "advantage" => $advantage,
                "unit" => $trackingCount
            ];
            if($advantageOption)
                $criteria["id"] = $advantageOption;

            $options = $optionsRepo->findBy(
                $criteria,
                ["price" => "asc"]
            );
            if(!$options) {
                // else get little and equal than ...
                $options = $optionsRepo->findNearerTrackingOptions($advantage, $trackingCount, $advantageOption);
                if(!$options) {
                    // else get little option in advantage
                    $options = $optionsRepo->findMinTrackingOptions($advantage, $trackingCount, $advantageOption);
                }
            }

        }
        return $options;
    }

}