<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 01/06/17
 * Time: 00:06
 */

namespace TrackingBundle\Service\Helpers;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Payment\Order;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;

class Lot extends Agency
{

    /**
     *  constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->type = "lot";
    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     * @param boolean $trackingLater
     */
    function process($order, $advantage, $trackingLater)
    {
        if (!$trackingLater) {
            $this->registerAction($order, $advantage);
        }
    }

    /**
     * @param Order $order
     * @param Advantage $advantage
     */
    function execute($order, $advantage)
    {
        $lots = $this->em->getRepository("AppBundle:Ad")->getValidLotsByGroup($advantage->getAgency()->getGroup());
        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($lots as $ad) {
            $this->getAvailableNumber($ad, $advantage);
        }
        // add notification
        $notification = new Notification();
        $notification->setSubject("Lots");
        $notification->setMessage("lots.auto_tracking_ok");
        $notification->setMessageParams(["%total%" => count($lots)]);
        $notification->setUser($order->getUser());
        $notification->setRoute("program_lots");
        $this->em->persist($notification);
        $this->em->flush();

        return true;
    }


    /**
     * @param Ad $ad
     * @return null|array
     */
    function getExtratTag($ad)
    {
        return [
            "tag4" => $ad->getId(),
            "tag5" => $ad->getProgram()->getId()
        ];
    }


}