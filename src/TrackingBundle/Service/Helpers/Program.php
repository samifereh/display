<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 01/06/17
 * Time: 00:06
 */

namespace TrackingBundle\Service\Helpers;

use AppBundle\Entity\Payment\Order;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;

class Program extends Ad
{

    /**
     *  constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->type = "program";
    }


    /**
     * @param Order $order
     * @param Advantage $advantage
     * @param boolean $trackingLater
     */
    function process($order, $advantage, $trackingLater){
        $programList = $this->em->getRepository("AppBundle:Program")->getValidProgramByGroup($order->getGroup());
        /** @var \AppBundle\Entity\Program $program */
        foreach ($programList as $program) {
            $this->getAvailableNumber($program, $advantage);
        }
    }

    /**
     * @param $ad
     * @return null|array
     */
    function getExtratTag($program){
        return ["tag5" => $program->getId()] ;
    }

}