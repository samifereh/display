<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 30/04/17
 * Time: 22:32
 */

namespace TrackingBundle\Service;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Configuration\AdvantageOption;
use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\Order;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use TrackingBundle\Entity\Advantage;
use TrackingBundle\Entity\Phone;
use TrackingBundle\Repository\AdvantageRepository;

class WannaSpeak
{

    /**
     * @var Container
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
        $this->logger = $this->container->get('app.tracking.logger');
    }


    /**
     * @param Order $order
     * @param Group $agency
     * @param AdvantageOption $advantageOption
     * @param bool $trackingLater
     * @return bool
     */
    public function setAdvantage(Order $order, Group $agency, AdvantageOption $advantageOption, $trackingLater=false){


        $slug = $order->getPackId();
        $type = str_replace("call-tracking-", "", $slug);
        $this->logger->notice('Call tracking type : '.$type);
        switch ($type){
            case "agency": return $this->container->get("app.tracking.agency.helpers")->setAdvantage($order, $agency, $advantageOption);break;
            case "portal": return $this->container->get("app.tracking.portal.helpers")->setAdvantage($order, $agency, $advantageOption, $trackingLater);break;
            case "ad": return $this->container->get("app.tracking.ad.helpers")->setAdvantage($order, $agency, $advantageOption, $trackingLater);break;
            case "program": return $this->container->get("app.tracking.program.helpers")->setAdvantage($order, $agency, $advantageOption, $trackingLater);break;
            case "lot": return $this->container->get("app.tracking.lot.helpers")->setAdvantage($order, $agency, $advantageOption, $trackingLater);break;
            default : {
                $message = "Tracking type not implemented ($type)";
                $this->logger->error($message);
                return false;
            }
        }

    }


    /**
     * @param string $trackingPhone
     * @return string
     */
    function deleteTrackingNumber( $trackingPhone ){

        $params = [
            'api' => 'ct',
            'method' => "delete",
            'did' => $trackingPhone,
        ];

        $this->logger->notice('Wannaspeak DELETE args ', $params);

        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);

        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);
            $this->logger->notice('Phone tracking : '.$trackingPhone);
            if($response->error){
                $this->logger->error("[Error] Deleting Phone tracking {$trackingPhone} : ".$response->error->txt);
                return false;
            };
            return true;
        }
        return false;
    }


    function execute($args){
        if(isset($args["order"]) && isset($args["advantage"]) ){
            $order = $this->em->getRepository("AppBundle:Payment\Order")->find($args["order"]);
            $advantage = $this->em->getRepository("TrackingBundle:Advantage")->find($args["advantage"]);
            if($order && $advantage) {
                switch ($advantage->getType()) {
                    case "ad":
                        return $this->container->get("app.tracking.ad.helpers")->execute($order, $advantage, $args);
                        break;
                    case "lot":
                        return $this->container->get("app.tracking.lot.helpers")->execute($order, $advantage, $args);
                        break;
                    default : {
                        $message = "Tracking type not implemented ({$advantage->getType()})";
                        $this->logger->error($message);
                        return false;
                    }
                }
            }
        }
        return false;
    }

    function ctModify($did, $destination, $email = null){
        $trackingAdvantageRepo = $this->em->getRepository("TrackingBundle:Advantage");
        $advantage = $trackingAdvantageRepo->findTrackingAdvantageByPhone($did);
        $params = [
            'api' => 'ct',
            'method' => "modify" ,
            'destination' => $destination,
            'did' => $did,
            'leg2' => "voicemail_".$advantage->getAgency()->getSlug()."-".$advantage->getAgency()->getVoicemailVersion()
        ];
        if($advantage){
            $params['startdate'] = $advantage->getDateBegin()->format("Y-m-d");
            $params['stopdate'] = $advantage->getDateEnd()->format("Y-m-d");
        }
        if($email)
            $params["email"] = $email;
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);

            return ($response->data && $response->data->ok);

        }
        return false;
    }

    /**
     * upload voicemail
     * @param Agency $agency
     * @return bool
     */
    function uploadVoiceMail(Agency $agency){

        $name = 'voicemail_'.$agency->getSlug()."-".$agency->getVoicemailVersion();
        /*
        $params = [
            'api' => 'sound',
            'method' => "delete" ,
            'name' => $name
        ];
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        */
        // load audio file
        $currentDir = $this->container->get('kernel')->getRootDir().'/..';
        $file = $currentDir.$agency->getVoicemail();
        $fp = fopen($file, 'rb');
        $data = stream_get_contents($fp);
        fclose($fp);
        define('MULTIPART_BOUNDARY', '--------------------------'.microtime(true));
        // prepare context
        $opts = array(
            'http'=>array(
                'method' => 'POST',
                'header'=>"Content-Type: multipart/form-data; boundary=" . MULTIPART_BOUNDARY
                    . "\r\n",
                'content' => implode( "\r\n", array(
                    '--' . MULTIPART_BOUNDARY,
                    'Content-Disposition: form-data; name="sound"; filename="' .
                    basename($file) . '"',
                    'Content-Type: application/octet-stream',
                    '',
                    $data,
                    '--' . MULTIPART_BOUNDARY . '--'
                ))
            )
        );
        $context = stream_context_create($opts);
        $accountID = $this->container->getParameter("wanna_speak.api.account_id");
        $accountKey = $this->container->getParameter("wanna_speak.api.secret_key");
        $server = $this->container->getParameter("wanna_speak.api.base_url");
        $now = time();
        $query = array(
            'id' => $accountID,
            'key' => $now . '-' . md5($accountID.$now.$accountKey ),
            'api' => 'sound',
            'method' => 'upload',
            'name' => $name
        );
        $query = http_build_query($query);
        $jsonResponse = file_get_contents($server . '?' . $query, FALSE, $context) ;
        $response = \GuzzleHttp\json_decode($jsonResponse);
        return ($response->data && $response->data->ok);

    }


}