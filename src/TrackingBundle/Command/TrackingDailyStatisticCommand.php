<?php

/**
 * Created by PhpStorm.
 * User: anis kossentini
 * Date: 03/07/17
 * Time: 18:58
 */
namespace TrackingBundle\Command;

use AppBundle\Entity\Gestion\ContactStatistics;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use TrackingBundle\Entity\Statistique\TrackingCallStatistique;

class TrackingDailyStatisticCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(EntityManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('tracking:call:statistic')
            ->setDescription('Get daily tracking call statistics');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = 0;
        $total = 0;
        $exclude = [];

        $phoneRepo = $this->em->getRepository('TrackingBundle:Phone');
        $statRepo = $this->em->getRepository('TrackingBundle:Statistique\TrackingCallStatistique');
        $contactStatRepo = $this->em->getRepository('AppBundle:Gestion\ContactStatistics');
        $output->writeln("");
        $output->writeln("-----------");
        $output->writeln(date("[Y-m-d H:i:s]")." Tracking contact section ");
        $response = $this->container->get("wanna_speak.api.statistics")->getAllStats(new \DateTime("today"));
        if($response['error'] == "" && $response['data']['calls']) {

            $dids = []; // did array
            foreach ($response['data']['calls'] as $call){
                if($call['terminatecause'] == "ANSWER") {
                    $inbound = $call['inbound'];
                    if (!isset($dids[$inbound])) {
                        $call['count'] = 0;
                        $dids[$inbound] = $call;
                    }
                    $dids[$inbound]['count']++;
                    $exclude[] = $inbound;
                }
            }
            $total = count($dids);
            foreach ($dids as $did => $value){
                $output->writeln("Check did existence in Phone entity ".$did);
                $phone = $phoneRepo->findOneBy(["phone" => $did]);
                if($phone){
                    $stat = $statRepo->findOneBy([
                        "phone" => $phone,
                        "date"  => new \DateTime("today")
                    ]);
                    if(!$stat)
                        $stat = new TrackingCallStatistique();

                    $output->writeln("Update Statistics, value: ".$value['count']);
                    $stat->setPhone($phone);
                    $stat->setValue($value['count']);
                    $this->em->persist($stat);
                    $count++;
                    //contact stats
                    $contactStat = $contactStatRepo->findOneBy([
                        "group" => $value["tag1"],
                        "agency" => $value["tag2"],
                        "portal" => $value["tag3"],
                        "ad" => $value["tag4"],
                        "program" => $value["tag5"],
                        "date"  => new \DateTime("today")
                    ]);
                    if(!$contactStat)
                        $contactStat = new ContactStatistics();

                    $output->writeln("Update Contact Statistics, value: ".$value['count']);
                    $contactStat->setGroup($value["tag1"]);
                    $contactStat->setAgency($value["tag2"]);
                    $contactStat->setPortal($value["tag3"]);
                    $contactStat->setAd($value["tag4"]);
                    $contactStat->setProgram($value["tag5"]);
                    $contactStat->setValue($value['count']);
                    $this->em->persist($contactStat);

                }

            }
        }
        if(count($exclude) > 0)
            $phones = $phoneRepo->getPhonesWithExclude($exclude);
        else
            $phones = $phoneRepo->findAll();
        // update the rest with 0
        foreach ($phones as $phone ){
            $output->writeln("Updating 0 for did Phone ".$phone->getPhone());
            $stat = $statRepo->findOneBy([
                "phone" => $phone,
                "date"  => new \DateTime("today")
            ]);
            if(!$stat)
                $stat = new TrackingCallStatistique();

            $output->writeln("Update Statistics, value: 0");
            $stat->setPhone($phone);
            $stat->setValue(0);
            $this->em->persist($stat);
            $count++;

        }

        $total += count($phones);
        $output->writeln("Finish tracking, updated/total: ".$count."/".$total);
        $this->em->flush();

        $output->writeln("opportunity contact section");
        $opportunityRepo = $this->em->getRepository("AppBundle:Gestion\Opportunity");
        $oppClients = $opportunityRepo->getOpportunityClient(new \DateTime("today"));

        $count = 0;
        //check existe
        /** @var \AppBundle\Entity\Gestion\Opportunity $oppClient **/
        foreach($oppClients as $oppClient){
            $contactStat = $contactStatRepo->findOneBy([
                "ad" => $oppClient->getAd(),
                "contact" => $oppClient->getContact(),
                "date" => new \DateTime("today")
            ]);

            if(!$contactStat) {
                $contactStat = new ContactStatistics();

                $output->writeln("Update Opportunity Contact Statistics, value: 1");

                $agency = $oppClient->getOpportunityGroup()->getAgency();
                $group = (
                    $agency->getGroup()->getParentGroup() ?
                        $agency->getGroup()->getParentGroup() :
                        $agency->getGroup()
                );
                $contactStat->setGroup($group);
                $contactStat->setAgency($agency);
                $contactStat->setAd($oppClient->getAd());
                $contactStat->setProgram($oppClient->getAd()->getProgram());
                $contactStat->setContact($oppClient->getContact());
                $contactStat->setValue(1);
                $this->em->persist($contactStat);
                $count++;
            }
        }
        $output->writeln("Finish Opp contact, updated/total: ".$count);
        $this->em->flush();
    }

}