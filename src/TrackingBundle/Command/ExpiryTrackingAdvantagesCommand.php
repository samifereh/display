<?php

/**
 * Created by PhpStorm.
 * User: anis kossentini
 * Date: 04/05/17
 * Time: 18:58
 */
namespace TrackingBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class ExpiryTrackingAdvantagesCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('ftp:run')
            ->setDescription('Export all archives');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $portals = $this->em->getRepository('AppBundle:Broadcast\\Portal')->getPortalsWithFTP();
        $currentDir = getcwd();
        $exportDir  = 'web/export';
        /** @var Portal $portal */
        foreach ($portals as $portal) {
            $host = $portal->getFtpHost();
            $username = $portal->getFtpUser();
            $password = $portal->getFtpPassword();
            /** @var Ftp $ftp */
            $ftp = $this->container->get('ijanki_ftp');

            try {
                $ftp->connect($host);
                $ftp->login($username, $password);
            } catch (FtpException $e) {
                echo 'Error: ', $e->getMessage();
            }
            $dir = $currentDir. DIRECTORY_SEPARATOR .$exportDir.DIRECTORY_SEPARATOR.$portal->getSlug();
            $files = scandir($dir);
            foreach ($files as $file) {
                if ($file != "." && $file != "..") {
                    try {
                        $ftp->put($file, $dir.DIRECTORY_SEPARATOR.$file, FTP_BINARY);
                    } catch (FtpException $e) {
                        echo 'Error: ', $e->getMessage();
                    }
                }
            }
        }
    }

}