<?php

namespace TrackingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phone
 *
 * @ORM\Table(name="tracking_phone")
 * @ORM\Entity(repositoryClass="TrackingBundle\Repository\PhoneRepository")
 */
class Phone
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, unique=true)
     */
    private $phone;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Advantage", inversedBy="phones",cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="advantage_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advantage;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="TrackingBundle\Entity\Statistique\TrackingCallStatistique", mappedBy="phone", cascade={"persist", "remove"})
     */
    private $stats;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return AdvantageDetail
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

    /**
     * @param mixed $advantage
     */
    public function setAdvantage($advantage)
    {
        $this->advantage = $advantage;
    }

    /**
     * @return mixed
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * @param mixed $stats
     */
    public function setStats($stats)
    {
        $this->stats = $stats;
    }


}

