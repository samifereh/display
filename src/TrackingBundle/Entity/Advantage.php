<?php

namespace TrackingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Advantage
 *
 * @ORM\Table(name="tracking_advantages")
 * @ORM\Entity(repositoryClass="TrackingBundle\Repository\AdvantageRepository")
 */
class Advantage
{

    CONST TYPES = array(
        "agency" => "Agency",
        "portal" => "Portal"
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;


    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var bool
     *
     * @ORM\Column(name="payed", type="boolean")
     */
    private $payed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateBegin", type="datetime")
     */
    private $dateBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="datetime")
     */
    private $dateEnd;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Configuration\Advantage", inversedBy="trackingAdvantages",cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="advantage_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advantage;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Configuration\AdvantageOption", inversedBy="trackingAdvantageOption",cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="advantage_option_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advantageOption;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Agency", inversedBy="trackingAdvantages")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    protected $agency;


    /**
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Phone", mappedBy="advantage",cascade={"persist"}, fetch="EAGER")
     */
    protected $phones;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Advantage
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Advantage
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return Advantage
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return bool
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return Advantage
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = clone $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return clone $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Advantage
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = clone $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return clone $this->dateEnd;
    }

    /**
     * @return mixed
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

    /**
     * @param mixed $advantage
     */
    public function setAdvantage($advantage)
    {
        $this->advantage = $advantage;
    }

    /**
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param mixed $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return ArrayCollection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * @param ArrayCollection $phones
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;
    }

    /**
     * @return mixed
     */
    public function getAdvantageOption()
    {
        return $this->advantageOption;
    }

    /**
     * @param mixed $advantageOption
     */
    public function setAdvantageOption($advantageOption)
    {
        $this->advantageOption = $advantageOption;
    }



}

