<?php

namespace TrackingBundle\Entity\Statistique;

use Doctrine\ORM\Mapping as ORM;
use TrackingBundle\Entity\Phone;

/**
 * TrackingCallStatistique
 *
 * @ORM\Table(name="tracking_call_statistique")
 * @ORM\Entity(repositoryClass="TrackingBundle\Repository\Statistique\TrackingCallStatistiqueRepository")
 */
class TrackingCallStatistique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="TrackingBundle\Entity\Phone", inversedBy="stats", cascade={"persist", "remove"})
     */
    protected $phone;

    public function __construct()
    {
        $this->date = new \DateTime('today');
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return TrackingCallStatistique
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return TrackingCallStatistique
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Phone $phone
     * @return TrackingCallStatistique
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }


}

