<?php

namespace TrackingBundle\Controller;

use AppBundle\Controller\PaymentController;
use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Group;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package TrackingBundle\Controller
 * @Route("/tracking")
 */

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $params = [
            'api' => 'ct',
            'method' => "modify" ,
            'destination' => "33476069149",
            'did' => "33482795516",
            'name' => "enseigne_primacoeur38",
            'email' => "f.gamberini@commiti.fr",
            'startdate' => "2017-02-24 00:00:00"
        ];
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);

            dump($params);
            dump($response);

        }
        die;
        return $this->render('TrackingBundle:Default:index.html.twig');
    }


    /**
     * @Route("/audio")
     */
    public function audioAction()
    {
        $params = [
            'api' => 'sound',
            'method' => 'available',
            'link'  => 1
        ];
        echo "<pre>";
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);

            print_r($params);
            print_r($response);

        }

        die;
    }

    /**
     * @Route("/list")
     */
    public function listAction()
    {
        $params = [
            'api' => 'ct',
            'method' => 'list'
        ];
        echo "<pre>";
        // @todo write log file (notice and error)
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);

            print_r($params);
            print_r($response);

        }

        $params = [
            'api' => 'ct',
            'method' => 'available'
        ];
        // @todo write log file (notice and error)
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);
            print_r($params);
            print_r($response);

        }

        $params = [
            'api' => 'stat',
            'method' => 'did',
            'starttime' => (new \DateTime("-7 days"))->format("Y-m-d"),
            'stoptime' => (new \DateTime("today"))->format("Y-m-d")
        ];
        // @todo write log file (notice and error)
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);
            print_r($params);
            print_r($response);

        }


        $params = [
            'api' => 'stat',
            'method' => 'newdid'
        ];
        // @todo write log file (notice and error)

        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);
            print_r($params);
            print_r($response);

        }

        $response = $this->container->get("wanna_speak.api.statistics")->getAllStats();
        echo "<pre>";
        dump($response);
        echo "</pre>";
        die;

        die;
    }

    /**
     * @Route("/delete/{did}")
     * @Method({"GET"})
     */
    public function deleteAction($did)
    {
        echo "$did";
        $this->get("app.tracking.export.helpers")->isValid($did);
        $em = $this->getDoctrine()->getManager();
        $agencyRepo = $em->getRepository("AppBundle:Agency");
        $portalRegistrationRepo = $em->getRepository("AppBundle:Broadcast\PortalRegistration");
        $adRepo = $em->getRepository("AppBundle:Ad");
        $programRepo = $em->getRepository("AppBundle:Program");
        $agencyRepo->resetTracking($did);
        $portalRegistrationRepo->resetTracking($did);
        $adRepo->resetTracking($did);
        $programRepo->resetTracking($did);
        if($this->get("app.tracking.call")->deleteTrackingNumber($did)){
            echo "OK";
        }else
            echo "NOK";
        die;
    }

    /**
     * @Route("/reset")
     */
    public function resetAction()
    {
        $params = [
            'api' => 'ct',
            'method' => 'list'
        ];
        // @todo write log file (notice and error)
        echo "reset";
        $res = $this->container->get("wanna_speak.http_client")->createAndSendRequest($params);
        $em = $this->getDoctrine()->getManager();
        $agencyRepo = $em->getRepository("AppBundle:Agency");
        $portalRegistrationRepo = $em->getRepository("AppBundle:Broadcast\PortalRegistration");
        $adRepo = $em->getRepository("AppBundle:Ad");
        $programRepo = $em->getRepository("AppBundle:Program");
        if($res->getStatusCode() == 200) {
            $jsonResponse = $res->getBody()->getContents();
            $response = \GuzzleHttp\json_decode($jsonResponse);

            echo "<pre>";
            print_r($params);
            print_r($response);
            foreach ($response->data->dids as $did){
                if($did->tag1 != ""){
                    echo "deleting : ".$did->did;
                    $this->get("app.tracking.export.helpers")->isValid($did->did);
                    $agencyRepo->resetTracking($did->did);
                    $portalRegistrationRepo->resetTracking($did->did);
                    $adRepo->resetTracking($did->did);
                    $programRepo->resetTracking($did->did);
                    if($this->get("app.tracking.call")->deleteTrackingNumber($did->did)){
                        echo "OK";
                    }else
                        echo "NOK";
                }
            }
        }

        $phones = $em->getRepository("TrackingBundle:Phone")->findAll();
        foreach ($phones as $phone){
            $agencyRepo->resetTracking($phone->getPhone());
            $portalRegistrationRepo->resetTracking($phone->getPhone());
            $em->remove($phone);

        }
        $em->flush();

        die;
    }


    /**
     * Return AdvantageOptions mapping with request params (trackingCount)
     * @Route("/options/{remoteId}/{slug}", options={"expose"=true}, name="tracking_advantage_options" )
     * @Method({"GET","POST"})
     * @var Request $request
     * @var Group $group
     * @var Advantage $advantage
     * @ParamConverter("group", options={"mapping": {"remoteId": "remoteId"}})
     * @ParamConverter("advantage", options={"mapping": {"slug": "slug"}})
     * @Template("TrackingBundle:Default/options.html.twig")
     * @return Response
     **/
    public function optionsAction(Request $request,Group $group, Advantage $advantage)
    {
        $options = $this->get("app.tracking.options.helpers")->getAdvantageOptions(
            $advantage, $request->get("trackingCount")
        );

        $em   = $this->getDoctrine()->getManager();
        $discount = $em->getRepository('AppBundle:GroupAdvantageDiscount')->findOneBy([ 'group' => $group, 'advantage' => $advantage]);

        $trackingParams = [
            'trackingCount' => $request->get('trackingCount'),
            "options" => $options
        ];
        return [
            "trackingParams" => $trackingParams,
            'advantage' => $advantage,
            'discount'  => $discount,
            'group'     => $group,
            'levels'    => PaymentController::LEVELS
        ];

    }

    /**
     * Return Advantage Phone information
     * @Route("/advantage/{id}", options={"expose"=true}, name="tracking_advantage_information" )
     * @var \TrackingBundle\Entity\Advantage $advantage
     * @ParamConverter("advantage", options={"mapping": {"id": "id"}})
     * @Template("TrackingBundle:Default/advantage.html.twig")
     * @return Response
     **/
    public function advantageAction(\TrackingBundle\Entity\Advantage $advantage)
    {

        return [
            'trackingAdvantage' => $advantage,
        ];

    }
}
