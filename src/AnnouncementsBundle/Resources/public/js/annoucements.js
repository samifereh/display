/************* ALL FUNCTIONS START*************/

    /******* Announcements Bundle functions start *******/
        function renderAnnouncementsList(showLoader)
        {
            showLoader = false;   
            var announcementsUrl = Routing.generate('list_announcements','',true);
            var announcementsPerPage    = $("#announcements-per-page").val();
            var searchKey       = $("#search-keyword").val();
            var curPage         = $("#current-announcements-page").html();
            var isSortingExist  = $("[sorted='yes']").length;
            var column          = "a.id";
            var order           = "asc";
            if(isSortingExist > 0) {
                column = $("[sorted='yes']").attr('sort-by');
                order  = $("[sorted='yes']").attr('order-by');
            }

            $.ajax({
                method: "POST",
                url: announcementsUrl,
                data: {
                    cur_page: curPage,
                    search_key: searchKey,
                    announcements_per_page: announcementsPerPage,
                    column: column,
                    orderby: order,
                },
                dataType: "json",
                beforeSend: function() {
                    if(showLoader) {
                        $("#loader-background").show();
                    }
                },
                success: function(result) {
                    $('#wrapper #announcements-list-rows').html(result.html);
                    $('#announcements-start').html(result.start);
                    $('#announcements-end').html(result.end);
                    $('.total-announcements').html($('#total-announcements').val());
                    if(showLoader) {
                        $("#loader-background").hide();
                    }
                }
            });
        }

        function viewAnnouncements(id)
        {
            var viewAnnouncementUrl = Routing.generate('view_announcements','',true);
            $.ajax({
                method: "POST",
                url: viewAnnouncementUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function(result) {
                    if($("#popup-wrapper").length) {
                        $("#popup-wrapper").remove();
                    }
                    $("#announcements-content").append(result.html);
                    //$("#loader-background").hide();
                    $(".popup2").trigger("click");
                }
            });
        }

        function editAnnouncements(id)
        {
            var editAnnouncementUrl = Routing.generate('edit_announcements','',true);
            $.ajax({
                method: "POST",
                url: editAnnouncementUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function(result) {
                    if($("#popup-wrapper").length) {
                        $("#popup-wrapper").remove();
                    }
                    $("#announcements-content").append(result.html);
                    //$("#loader-background").hide();
                    $(".popup2").trigger("click");
                }
            });
        }

        function deleteAnnouncements(id) 
        {
            var deleteAnnouncementUrl = Routing.generate('remove_announcements','',true);
            $.ajax({
                method: "POST",
                url: deleteAnnouncementUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function() {
                    $("#current-announcements-page").html('1');
                    renderAnnouncementsList(true);
                }
            });   
        }

        function printAnnouncements(printIds, showLoader)
        {
            var printAnnouncementUrl    = Routing.generate('print_announcements','',true);
            var isSortingExist          = $("[sorted='yes']").length;
            var column                  = "a.id";
            var order                   = "asc";

            if(isSortingExist > 0) {
                column = $("[sorted='yes']").attr('sort-by');
                order  = $("[sorted='yes']").attr('order-by');
            }

            $.ajax({
                method: "POST",
                url: printAnnouncementUrl,
                dataType: "json",
                data: {
                        id: printIds,
                        column: column,
                        orderby: order,
                    },
                beforeSend: function() {
                    if(showLoader) {
                        //$("#loader-background").show();
                    }
                },
                success: function(result) {
                    var popupWin = window.open('', '_blank', 'width=300,height=500');
                    popupWin.document.open();
                    popupWin.document.write(result.html);
                    popupWin.document.close();
                }
            });
        }
    /******* Announcements Bundle functions end *********/

/************* ALL FUNCTIONS END***************/


$("document").ready(function(){

    /********** Announcements Bundle start ***********/
        /********** Add Announcements start ***********/
            $("body").on("click","#add_announcement",function(){
                var addAnnouncementsUrl = Routing.generate('add_announcements','',true);
                $.ajax({
                    method: "POST",
                    url: addAnnouncementsUrl,
                    dataType: "json",
                    beforeSend: function() {
                        //$("#loader-background").show();
                    },
                    success: function(result) {
                        if($("#popup-wrapper").length) {
                            $("#popup-wrapper").remove();
                        }
                        $("#announcements-content").append(result.html);
                        labels = result.labels;
                        //$("#loader-background").hide();
                        $(".popup2").trigger("click");                        
                    }
                });
            });
        /********** Add Announcements end   ***********/

        /********** Save Announcements start ***********/
            $("body").on("submit","#announcement-form",function(event){
                event.preventDefault();
                
                var startDate   = new Date($("#startdate").val());
                var endDate     = new Date($("#enddate").val());

                if(endDate < startDate) {
                    $('#enddate').closest('.form-group').append('<div id="end-date-error"><span class="error-msg">End date must be equal or greater than Start date.</span></div>');
                    return true;
                } else {
                    $('#enddate').closest('.form-group').find('#end-date-error').remove();
                }

                var saveAnnouncementsUrl = Routing.generate('save_announcements','',true);
                $.ajax({
                    method: "POST",
                    url: saveAnnouncementsUrl,
                    data: {
                        data: $("#announcement-form").serialize()
                    },
                    dataType: "json",
                    beforeSend: function() {
                        /*$('#openModal').show();*/
                    },
                    success: function(result) {
                        $.fancybox.close();
                        $("#current-announcements-page").html('1');
                        renderAnnouncementsList(true);
                    }
                });
            });
        /********** Save Announcements end   ***********/

        /********** Observe view,edit and delete from listing and popup start ***********/
            $("body").on("click","#announcements-content [data-action],.fancybox-wrap .announcement-view  [data-action]",function(){
                var id = $(this).attr("data-id");
                var action = $(this).attr("data-action");

                switch(action) {
                    case "view": viewAnnouncements(id); break;
                    case "edit": editAnnouncements(id); break;
                    case "delete": deleteAnnouncements(id); break;
                }
            });
        /********** Observe view,edit and delete from listing and popup end *************/

        /********** Pagination Announcements start ***********/
            $("body").on("change","#announcements-per-page",function(){
                $("#current-announcements-page").html('1')
                renderAnnouncementsList(false);
            });
        /********** Pagination Announcements end *************/

        /********** Search Announcements start *************/
            $("body").on("keyup","#announcements-content #search-keyword",function(){
                $("#current-announcements-page").html('1');
                renderAnnouncementsList(false);
            });
        /********** Search Announcements end ***************/

        /********** Announcements Page navigation to next page start *************/
            $("body").on("click","#announcements-table_next",function(){
                var announcementsPerPage    = $("#announcements-per-page").val();
                
                var curPage         = $("#current-announcements-page").html();
                var totalAnnouncements      = $("#total-announcements").val();
                var lastPage        = totalAnnouncements / announcementsPerPage;
                
                if(curPage < lastPage) {
                    curPage             = parseInt(curPage) + 1; 

                    $("#current-announcements-page").html(curPage);

                    renderAnnouncementsList(false);
                }
            });
        /********** Announcements Page navigation to next page end ***************/

        /********** Announcements Page navigation to previous page start *************/
            $("body").on("click","#announcements-table_previous",function(){
                var curPage         = $("#current-announcements-page").html();
                if(curPage != 1) {
                    if(curPage > 1) {
                        curPage = parseInt(curPage) - 1; 
                    }

                    if (curPage <= 1) {
                        $(this).attr('disabled','true');
                    }
                    
                    $("#current-announcements-page").html(curPage);

                    renderAnnouncementsList(false);
                }  
            });
        /********** Announcements Page navigation to previous page end ***************/

        /********** Sorting Announcements start *************/
            $("body").on("click","#announcements-content .sorting",function(){
                var order           = "";

                if($(this).attr('order-by') == "asc") {
                   order = "desc";
                } else {
                   order = "asc";
                }

                $(".sorting").each(function(){
                    $(this).attr('sorted',"");
                    $(this).attr('order-by',"");
                });

                $(this).attr('order-by',order);
                $(this).attr('sorted',"yes");

                renderAnnouncementsList(true);
            });
        /********** Sorting Announcements end ***************/

        /********** Check - Uncheck all Announcements start *************/
            $("body").on("click","#check-all-announcement",function(){
                var isCheckedAll = $(this).is(':checked');
                if(isCheckedAll) {
                    $(".check-announcement").prop("checked",true);
                } else {
                    $(".check-announcement").prop("checked",false);
                }
            });

            $("body").on("click",".check-announcement",function(){
                var totalCheckbox   = $(".check-announcement").length;
                var checkedCheckbox = $(".check-announcement:checked").length;
                if(totalCheckbox != checkedCheckbox) {
                    $("#check-all-announcement").prop("checked",false);
                } else {
                    $("#check-all-announcement").prop("checked",true);
                }
            });
        /********** Check - Uncheck all Announcements end   *************/

        /********** Print Announcements start *************/
            $("body").on("click",".print_button",function(){
                var printIds = [];
                if($(this).attr('id') == "print_announcements") {
                    $(".check-announcement:checked").each(function(){
                        printIds.push($(this).attr('checkdata'));
                    });
                               
                    if(printIds.length > 0) {
                        printAnnouncements(printIds,false);
                    } else {
                        alert("Select atleast one announcement to print.");
                    }

                } else if($(this).attr('id') == "print_all_announcements") {
                    printAnnouncements("all",false);
                }
            });
        /********** Print Announcements end   *************/

        /********** Fancybox open start ***********/
            
            $(".popup2").fancybox({

                maxWidth   : 900,
                fitToView   : true,
                autoSize    : false,
                showCloseButton: true,
                autoHeight  : true,
                padding     : [0,0,0,0],
                margin      : [15,15,15,15],
                openSpeed   : 'fast',
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });
        /********** Fancybox open end *************/

        /********** Fancybox close start ***********/
            $("body").on("click",".fancybox-wrap [data-dismiss='modal']",function(){
                $.fancybox.close();
            });
        /********** Fancybox close end *************/
    /********** Announcements Bundle end *************/
});