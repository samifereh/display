<?php

namespace AnnouncementsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AnnouncementsBundle\Repository\AnnouncementsRepository as AnnouncementsRepository;
use AnnouncementsBundle\Entity\Announcements;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class AnnouncementsController extends Controller
{
    public function addAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $announcements = new Announcements();
        $form = $this->createFormBuilder($announcements)
            ->add('title', 'text', array('attr' => array(
                                      'required' => true,
                                  )))
            ->add('description', 'ckeditor', 
                                    array('attr' => array('required' => false)),
                                    array('config' => array('uiColor' => '#ffffff','toolbar' => 'basic'))
                                )
            ->add('startdate', DateType::class, array(
                                      'required' => true,
                                      'widget' => 'single_text',
                                      'attr' => ['class' => 'js-datepicker'],
                                  ))
            ->add('enddate', DateType::class, array(
                                      'required' => true,
                                      'widget' => 'single_text',
                                      'attr' => ['class' => 'js-datepicker'],
                                  ))
            ->add('shareWithAllTeamMember', CheckboxType::class, array(
                                      'required' => false,
                                      'label' => false,
                                  ))
            ->add('shareWithAllClient', CheckboxType::class, array(
                                      'required' => false,
                                      'label' => false,
                                  ))
            ->getForm();
        

        $data["html"] = $this->render('AnnouncementsBundle:Announcements:announcements.html.twig',array('action' => 'add','form' => $form->createView()))->getContent();
        return new Response(json_encode($data));
    }

    public function saveAction(Request $request)
    {
        $userId     = $this->getUser()->getId();
        $em         = $this->getDoctrine()->getManager();
        $data       = $request->request->all();
        $parsedData = array();
        parse_str($data['data'], $parsedData);

        $id                     = isset($parsedData['edit_announcement_id']) ? $parsedData['edit_announcement_id']:"";
        $title                  = isset($parsedData['form']['title']) ? $parsedData['form']['title']:"";
        $description            = isset($parsedData['form']['description']) ? $parsedData['form']['description']:"";
        $startdate              = isset($parsedData['form']['startdate']) ? $parsedData['form']['startdate']:"";
        $enddate                = isset($parsedData['form']['enddate']) ? $parsedData['form']['enddate']:"";
        $shareWithAllTeamMember = isset($parsedData['form']['shareWithAllTeamMember']) ? $parsedData['form']['shareWithAllTeamMember']:"";
        $shareWithAllClient     = isset($parsedData['form']['shareWithAllClient']) ? $parsedData['form']['shareWithAllClient']:"";

        if(!empty($id)) {
            $announcements = $em->getRepository('AnnouncementsBundle:Announcements')->findOneBy(array('id' => $id, 'userId' => $userId));
        } else {
            $announcements = new Announcements();
        }

        $announcements->setTitle($title);
        $announcements->setDescription($description);
        $announcements->setStartdate(new \DateTime($startdate));
        $announcements->setEnddate(new \DateTime($enddate));
        $announcements->setUserId($this->getUser());
        $announcements->setShareWithAllTeamMember($shareWithAllTeamMember);
        $announcements->setShareWithAllClient($shareWithAllClient);
        
        $em->persist($announcements);
        $em->flush();    

        return new Response(json_encode($data));
    }

    public function listAction(Request $request)
    {
    	$data               	= $request->request->all();
        $userId             	= $this->getUser()->getId();
        $em                 	= $this->getDoctrine()->getEntityManager();
        $query              	= $em->createQueryBuilder();
        $currentPage        	= isset($data["cur_page"])?$data["cur_page"]:1;
        $searchKeyword      	= isset($data["search_key"])?$data["search_key"]:"";
        $announcementsPerPage 	= isset($data["announcements_per_page"])?$data["announcements_per_page"]:5;
        $order              	= isset($data["orderby"])?$data["orderby"]:"ASC";
        $column             	= isset($data["column"])?$data["column"]:"a.id";
        $offset             	= ($currentPage * $announcementsPerPage) - $announcementsPerPage;
        $start              	= $offset+1;


        $query
            ->select("a","u")
            ->from('AnnouncementsBundle:Announcements', 'a')
            ->leftJoin("a.userId", "u")
            ->where('a.userId = :uid')
            ->setParameter('uid', $userId)
            ->orderBy($column,$order);

        if(!empty($searchKeyword)) {
            $query
            ->andWhere('a.title LIKE :searchKeyword OR a.startdate LIKE :searchKeyword OR a.enddate LIKE :searchKeyword OR u.name LIKE :searchKeyword')
            ->setParameter('searchKeyword', '%'.$searchKeyword."%");
        } 
        
        $queryTotalAnnouncements    = $query->getQuery()->getScalarResult();
        $totalAnnouncements         = count($queryTotalAnnouncements);

        $query
            ->setFirstResult($offset)
            ->setMaxResults($announcementsPerPage);

        $userAnnouncements    = $query->getQuery()->getScalarResult();
        
        $end    	= $offset + count($userAnnouncements);

        if($request->isXmlHttpRequest()) {
            $announcementsData = array();
            $announcementsData['html']  =   $this->render('AnnouncementsBundle:Announcements:ajaxlistannouncements.html.twig',array(
                                        'announcements'=> $userAnnouncements,
                                        'totalannouncements'=> $totalAnnouncements
                                    ))->getContent();
            $announcementsData['start'] =   $start;
            $announcementsData['end']   =   $end;
            
            return new Response(json_encode($announcementsData));
        } else {
            return $this->render('AnnouncementsBundle:Announcements:listannouncements.html.twig',array(
                'announcements'=>$userAnnouncements,
                'totalannouncements'=> $totalAnnouncements,
            ));
        }
    }

    public function viewAction(Request $request)
    {
        $userId             = $this->getUser()->getId();
        $userName           = $this->getUser()->getName();
        $announcementsPost  = $request->request->all();
        $em                 = $this->getDoctrine()->getManager();
        $query              = $em->createQueryBuilder();

        $announcements = $em->getRepository('AnnouncementsBundle:Announcements')->findOneBy(array('id' => $announcementsPost['id'], 'userId' => $userId));

        $data["html"] = $this->render('AnnouncementsBundle:Announcements:announcements.html.twig',array(
                            'announcements' => $announcements,
                            'user_name' => $userName,
                            'action' => 'view',
                        ))->getContent();

        return new Response(json_encode($data));
    }

    public function editAction(Request $request)
    {
        $userId             = $this->getUser()->getId();
        $announcementsPost  = $request->request->all();
        $em                 = $this->getDoctrine()->getManager();

        $announcements = $em->getRepository('AnnouncementsBundle:Announcements')->findOneBy(array('id' => $announcementsPost['id'], 'userId' => $userId));

        $form = $this->createFormBuilder($announcements)
            ->add('title', 'text', array('attr' => array(
                                      'required' => true,
                                  )))
            ->add('description', 'ckeditor', 
                                    array('attr' => array('required' => false)),
                                    array('config' => array('uiColor' => '#ffffff','toolbar' => 'basic'))
                                )
            ->add('startdate', DateType::class, array(
                                      'required' => true,
                                      'widget' => 'single_text',
                                      'attr' => ['class' => 'js-datepicker'],
                                  ))
            ->add('enddate', DateType::class, array(
                                      'required' => true,
                                      'widget' => 'single_text',
                                      'attr' => ['class' => 'js-datepicker'],
                                  ))
            ->add('shareWithAllTeamMember', CheckboxType::class, array(
                                      'required' => false,
                                      'label' => false,
                                  ))
            ->add('shareWithAllClient', CheckboxType::class, array(
                                      'required' => false,
                                      'label' => false,
                                  ))
            ->getForm();


        $data["html"] = $this->render('AnnouncementsBundle:Announcements:announcements.html.twig',array('action' => 'edit','form' => $form->createView(), 'announcement_id' => $announcementsPost['id']))->getContent();

        return new Response(json_encode($data));
    }

    public function removeAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $announcementsPost = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $announcements = $em->getRepository('AnnouncementsBundle:Announcements')->findOneBy(array('id' => $announcementsPost['id'], 'userId' => $userId));

        if(!empty($announcements)) {
            $em->remove($announcements);
            $em->flush();
        }

        return new Response(json_encode(""));
    }
    
    public function printAction(Request $request)
    {
        
        $postData = $request->request->all();
        
        $userId                 = $this->getUser()->getId();
        $em                     = $this->getDoctrine()->getEntityManager();
        $query                  = $em->createQueryBuilder();
        $order                  = isset($postData["orderby"])?$postData["orderby"]:"ASC";
        $column                 = isset($postData["column"])?$postData["column"]:"a.id";
        
        $query
            ->select("a","u")
            ->from('AnnouncementsBundle:Announcements', 'a')
            ->leftJoin("a.userId", "u")
            ->where('a.userId = :uid')
            ->setParameter('uid', $userId)
            ->orderBy($column,$order);
        
        if(isset($postData['id']) && is_array($postData['id'])) {
            $query
                ->andWhere('a.id IN (:ids)')
                ->setParameter('ids', $postData['id']);
        } 

        $userAnnouncements    = $query->getQuery()->getScalarResult();

        if($request->isXmlHttpRequest()) {
            $announcementsData = array();
            $announcementsData['html']  =   $this->render('AnnouncementsBundle:Announcements:printannouncements.html.twig',array(
                                        'announcements'=> $userAnnouncements,
                                    ))->getContent();
            return new Response(json_encode($announcementsData));
        } else {
            return $this->render('AnnouncementsBundle:Announcements:printannouncements.html.twig',array(
                'announcements'=>$userAnnouncements,
            ));
        }
    }
}
