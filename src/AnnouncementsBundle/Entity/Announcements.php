<?php

namespace AnnouncementsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Announcements.
 *
 * @ORM\Table(name="announcements")
 * @ORM\Entity(repositoryClass="AnnouncementsBundle\Repository\AnnouncementsRepository")
 */

class Announcements
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $enddate;

    /** 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="announcements")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
     protected $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="share_with_all_team_member", type="boolean", length=1, nullable=true)
     */
    private $shareWithAllTeamMember;


    /**
     * @var int
     *
     * @ORM\Column(name="share_with_all_client", type="boolean", length=1, nullable=true)
     */
    private $shareWithAllClient;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Announcements
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Announcements
     */
    
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }


    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Get startdate.
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }


    /**
     * Set startdate.
     *
     * @param \DateTime $startdate
     *
     * @return Announcements
     */
    
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    
    /**
     * Get enddate.
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }


    /**
     * Set enddate.
     *
     * @param \DateTime $enddate
     *
     * @return Announcements
     */
    
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Announcements
     */
    public function setUserId(\AppBundle\Entity\User $userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Set shareWithAllTeamMember.
     *
     * @param int $shareWithAllTeamMember
     *
     * @return Announcements
     */
    public function setShareWithAllTeamMember($shareWithAllTeamMember)
    {
        $this->shareWithAllTeamMember = $shareWithAllTeamMember;

        return $this;
    }

    /**
     * Get shareWithAllTeamMember.
     *
     * @return int
     */
    public function getShareWithAllTeamMember()
    {
        return $this->shareWithAllTeamMember;
    }


    /**
     * Set shareWithAllClient.
     *
     * @param int $shareWithAllClient
     *
     * @return Announcements
     */
    public function setShareWithAllClient($shareWithAllClient)
    {
        $this->shareWithAllClient = $shareWithAllClient;

        return $this;
    }

    /**
     * Get shareWithAllClient.
     *
     * @return int
     */
    public function getShareWithAllClient()
    {
        return $this->shareWithAllClient;
    }


}
