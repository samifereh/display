<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 01/07/17
 * Time: 17:52
 */

namespace AppBundle\Service;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Group;
use AppBundle\Entity\PendingActions;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class AgencyHelpers
{

    /**
     * @var Container
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
    }

    /**
     * @param Group $group
     * @param string $action
     * @param bool $force
     * @param bool $withVoiceMail
     *
     */
    function registerClosing($group, $action = "closing", $force = false, $withVoiceMail = false)
    {
        $args = array(
            "agency" => $group->getAgency()->getId(),
            "action" => "change",
            "voicemail" => $withVoiceMail
        );
        if(
            $group->getAgency() &&
            $group->getAgency()->getClosingPhone() &&
            $group->getAgency()->getReopeningDate()
        ) {
            $args["action"] = $action;
        }

        $service = "app.agency.helpers";
        $existAction = $this->em->getRepository("AppBundle:PendingActions")->findOneBy(
            [
                "service" => $service,
                "args"    => serialize($args),
                "status"  => PendingActions::STAND_BY_STATUS
            ]
        );
        //if does not existe then create new one
        if($force || !$existAction) {
            $pendingAction = new PendingActions();
            $pendingAction->setService($service);
            $pendingAction->setArgs($args);
            $this->em->persist($pendingAction);
            $this->em->flush();
        }
    }

    function execute($args)
    {
        if(isset($args["agency"]) && isset($args["action"]) ){
            /** @var Agency $agency **/
            $agency = $this->em->getRepository("AppBundle:Agency")->find($args["agency"]);
            if($agency) {
                $voicemail = (isset($args['voicemail']) ? $args['voicemail'] : false );
                if($voicemail)
                    $this->container->get("app.tracking.call")->uploadVoiceMail($agency);

                echo "\nAction : ".$args["action"];
                switch ($args["action"]) {
                    case "closing" :
                        return $this->closing($agency);break;
                    case "reopening" :
                        return $this->reopening($agency);break;
                    case "change" :
                        return $this->setDestinationTracked(
                            $agency, $agency->getTelephone());break;
                    default : echo "Action not yet implemented ".$args["action"];break;
                }
            }
        }
        return false;
    }

    /**
     * change destination phone for agency while closing
     * @param Agency $agency
     * @param bool $voicemail
     * @return bool
     */
    function closing($agency, $voicemail = false ){
        $currentTime = new \DateTime("now");
        if(
            $agency->getClosingDate() <= $currentTime &&
            $agency->getClosingPhone())
        {
            $this->setDestinationTracked($agency, $agency->getClosingPhone());
            //register another pendingAction
            $this->registerClosing($agency->getGroup(), "reopening", false, $voicemail);
        }else{
            //register another pendingAction
            $this->registerClosing($agency->getGroup(), "closing", true, $voicemail);
        }
        return true;
    }


    /**
     * change destination phone for agency after reopening
     * @param Agency $agency
     * @return bool
     */
    function reopening($agency){
        $currentTime = new \DateTime("now");
        if(
            $agency->getReopeningDate() <= $currentTime &&
            $agency->getTelephone()
        )
        {
            $this->setDestinationTracked($agency, $agency->getTelephone());
            //reset agency fields
            $agency->setClosingDate(null);
            $agency->setReopeningDate(null);
            $agency->setClosingPhone(null);
            $this->em->persist($agency);
            $this->em->flush();
        }else{
            //register another pendingAction
            $this->registerClosing($agency->getGroup(), "reopening", true);
        }
        return true;
    }

    /**
     * @param Agency $agency
     * @param mixed $destination
     * @return bool
     */
    function setDestinationTracked($agency, $destination){
        $trackedList = $this->getAgencyTrackedList($agency);

        $email = "";
        if($agency->getEmailAlias())
            $email = $agency->getEmailAlias();
        elseif ($agency->getEmail())
            $email = $agency->getEmail();

        /** @var \TrackingBundle\Entity\Phone $trackedItem **/
        foreach ($trackedList as $trackedItem){
            echo "\n[CT / Modify] did: ".$trackedItem->getPhone().
                ", destination: ".$destination;
            $this->container->get("app.tracking.call")->
            ctModify($trackedItem->getPhone(), $destination, $email);
        }
        return true;
    }

    function getAgencyTrackedList($agency){

        $repo = $this->em->getRepository("TrackingBundle:Phone");
        return $repo->getAgencyPhones($agency);

    }

}