<?php
/**
 * Client class for managing API requests
 * @package AppBundle\WebinairBundle
 */

namespace AppBundle\Service;

use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;

class FtpClient
{
    /**
     * @var string root URL for authorizing requests
     */
    const ENDPOINT = 'https://cldvm11.terrancle.net/api_commiti.php/';

    /**
     * @var string key to access the API
     */
    const API_KEY = "e3Bi6aSqk3IPfHru0753851S/k4ipsAh/v7CeqL";

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;

    /**
     * @return \GuzzleHttp\Client
     */
    public function getGuzzleClient()
    {
        return $this->guzzleClient;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setGuzzleClient($client)
    {
        $this->guzzleClient = $client;
    }

    public static function addFTP($user, $password)
    {
        $url = self::ENDPOINT;

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'apikey'  =>  self::API_KEY,
                'action'  => 'add',
                'user'    => $user,
                'password'=> $password,
                'home'    => '/ftp'
            ],
            'verify' => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, $options);
        if($response->getStatusCode()) {
            $jsonBody = json_decode($response->getBody(),true);
        }
        return null;
    }

    public static function deleteFTP($user)
    {
        $url = self::ENDPOINT;

        $options = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'apikey'  =>  self::API_KEY,
                'action'  => 'delete',
                'user'    => $user,
            ],
            'verify' => false
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $url, $options);
        if($response->getStatusCode()) {
            $jsonBody = json_decode($response->getBody(),true);
        }
        return null;
    }

}
