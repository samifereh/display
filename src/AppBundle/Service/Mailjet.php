<?php

namespace AppBundle\Service;

use AppBundle\Entity\Document;
use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Contact;

use AppBundle\Entity\Gestion\ContactList;
use Doctrine\Common\Persistence\ObjectManager;
use Mailjet\Resources;
use Doctrine\ORM\EntityManager;

class Mailjet
{
    /**
     * @var ObjectManager $em
     */
     var $em;

    /**
     * @var \Mailjet\Client $mailjet
     */
     var $mailjet;

    public function __construct(\Mailjet\Client $mailjet, $em){
        $this->mailjet = $mailjet;
        $this->em      = $em;
    }

    public function addContactList($name)
    {
        $body = [
            'Name' => $name
        ];
        $response = $this->mailjet->post(Resources::$Contactslist, ['body' => $body]);
        if($response->success()) {
            return $response->getBody()['Data'][0]['ID'];
        }
        return null;
    }

    public function affectContactToContactList(ContactList $contactList, Contact $contact)
    {
        $contactId = null;
        $contactsList = [];
        $response = $this->mailjet->get(['contact', $contact->getEmail()]);
        if($response->getStatus() != 404 && $response->getBody()['Count'] == 1) $contactId = $response->getBody()['Data'][0]['ID'];
        if($contactId) {
            $contactsList[] = [
                'ListID' => $contactList->getMailjetId(),
                'Action' => "addnoforce"
            ];
            if(count($contact->getGroupContacts())) {
                foreach ($contact->getGroupContacts() as $contactList) {
                    $groupMailjetId = $contactList->getMailjetId();
                    if($groupMailjetId) {
                        $contactsList[] = [
                            'ListID' => $contactList->getMailjetId(),
                            'Action' => "addnoforce"
                        ];
                    }
                }
            }

            $body = [
                'ContactsLists' => $contactsList
            ];

            $response = $this->mailjet->post(Resources::$ContactManagecontactslists, ['id' => $contactId, 'body' => $body]);
            if($response->success()) {
                return $response->getBody()['Data'][0]['ID'];
            }
        }
        return null;
    }

    public function addContact(Contact $contact)
    {
        if(!$contact->getEmail()) return null;

        $contactId = null;
        $response = $this->mailjet->get(['contact', $contact->getEmail()]);
        if($response->getStatus() != 404 && $response->getBody()['Count'] == 1) $contactId = $response->getBody()['Data'][0]['ID'];
        if(!$contactId) {
            $body = [
                'Email' => $contact->getEmail(),
                'Name' => $contact->getPrenom() . ' ' . $contact->getNom()
            ];
            $response = $this->mailjet->post(Resources::$Contact, ['body' => $body]);
            if (!$response->success()) return null;
            $contactId = $response->getBody()['Data'][0]['ID'];
            if(!count($contact->getGroupContacts()))
                return $contactId;
        }
        $contactsList = [];
        if(count($contact->getGroupContacts())) {
            foreach ($contact->getGroupContacts() as $contactList) {
                $groupMailjetId = $contactList->getMailjetId();
                if(!$groupMailjetId) {
                    $groupMailjetId = $this->addContactList($contactList->getGroup()->getName().' / '.$contactList->getName());
                    $contactList->setMailjetId($groupMailjetId);
                    $this->em->persist($contactList);
                    $this->em->flush();
                }
                $contactsList[] = [
                    'ListID' => $contactList->getMailjetId(),
                    'Action' => "addnoforce"
                ];
            }

            $body = [
                'ContactsLists' => $contactsList
            ];
            $response = $this->mailjet->post(Resources::$ContactManagecontactslists, ['id' => $contactId, 'body' => $body]);
            if($response->success()) {
                return $response->getBody()['Data'][0]['ContactsLists'][0]['ListID'];
            }
        }
        return null;
    }

    public function editContact(Contact $contact)
    {
        $response = $this->mailjet->get(['contact', $contact->getEmail()]);
        if($response->getStatus() != 404 && $response->getBody()['Count'] == 1) $contactId = $response->getBody()['Data'][0]['ID'];
        $contactsList = [];
        foreach ($contact->getGroupContacts() as $contactList) {
            $groupMailjetId = $contactList->getMailjetId();
            if(!$groupMailjetId) {
                $groupMailjetId = $this->addContactList($contactList->getGroup()->getName().' / '.$contactList->getName());
                $contactList->setMailjetId($groupMailjetId);
                $this->em->persist($contactList);
                $this->em->flush();
            }
            $contactsList[] = [
                'ListID' => $contactList->getMailjetId(),
                'Action' => "addnoforce"
            ];
        }

        $body = [
            'ContactsLists' => $contactsList
        ];
        $response = $this->mailjet->post(Resources::$ContactManagecontactslists, ['id' => $contactId, 'body' => $body]);
        if($response->success()) {
            return true;
        }
        return null;
    }


    public function createCampagne(Campagne &$campagne)
    {
        $body = [
            'Locale'             => 'fr_FR',
            'Sender'             => 'Display-Immo',
            'SenderEmail'        => 'no-reply@display-immo.fr',
            'Subject'            => $campagne->getSubject(),
            'ContactsListID'     => $campagne->getContactList()->getMailjetId(),
            'Title'              => $campagne->getTitle()
        ];

        $response = $this->mailjet->post(Resources::$Campaigndraft, ['body' => $body]);
        if($response->success()) {
            $id = $response->getBody()['Data'][0]['ID'];
            $campagne->setMailjetId($id);
            $body = [
                'Html-part'          => $campagne->getBodyHtml(),
                'Text-part'          => $campagne->getBodyText()
            ];
            $response = $this->mailjet->post(Resources::$CampaigndraftDetailcontent, ['id' => $id,'body' => $body]);
            if($response->success()) {
                return true;
            }
        }
        return null;
    }

    public function modifyCampagne(Campagne $campagne)
    {
        $body = [
            'Html-part'          => $campagne->getBodyHtml(),
            'Text-part'          => $campagne->getBodyText(),
        ];
        $response = $this->mailjet->post(Resources::$CampaigndraftDetailcontent, ['id' => $campagne->getMailjetId(),'body' => $body]);
        if($response->success()) {
            return true;
        }
        return false;
    }


    public function sendDraftTestCampagne(Campagne $campagne, $email)
    {
        $body = [
            'Recipients' => [
                [
                    'Email' => $email,
                    'Name' => "Mail de test"
                ]
            ]
        ];
        $response = $this->mailjet->post(Resources::$CampaigndraftTest, ['id' => $campagne->getMailjetId(), 'body' => $body]);
        if($response->success())
            return true;
        return false;

    }

    public function sendImmediatlyCampagne(Campagne $campagne)
    {
        $response = $this->mailjet->post(Resources::$CampaigndraftSend, ['id' => $campagne->getMailjetId()]);
        if($response->success())
            return isset($response->getData()[0]['Status']) ? $response->getData()[0]['Status'] : true;
        return null;

    }

    public function sendScheduleCampagne(Campagne $campagne)
    {
        $body = [
            'date' => $campagne->getDate()->format(\DateTime::ATOM)
        ];
        $response = $this->mailjet->post(Resources::$CampaigndraftSchedule, ['id' => $campagne->getMailjetId(), 'body' => $body]);
        if($response->success())
            return isset($response['Data'][0]['Status']) ? $response['Data'][0]['Status'] : true;
        return null;

    }

    public function checkStatusCampagne(Campagne $campagne)
    {
        $response = $this->mailjet->get(Resources::$Campaigndraft, ['id' => $campagne->getMailjetId()]);
        if($response->success()) {
            return $response->getBody()['Data'][0]['Status'];
        }
        return null;

    }

    public function getCampagneStatistics(array $filters)
    {
        $response = $this->mailjet->get(Resources::$Campaignstatistics,['filters' => $filters]);
        if($response->success()) {
            return $response->getBody()['Data'];
        }
        return null;
    }
}