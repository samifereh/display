<?php

namespace AppBundle\Service;

use AppBundle\Entity\Document;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Doctrine\ORM\EntityManager;

class DocumentService
{

    private $rootDir;
    private $entityManager;

    public function __construct($rootDir, EntityManager $entityManager){
        $this->rootDir = $rootDir;
        $this->entityManager = $entityManager;
    }

    public function remove($directory, Document $file)
    {

        if(!strlen($file->getName()) || !strlen($directory))
            return false;

        $directory = ($directory[0] == '/')?$directory:$directory.'/';
        $directory .= ($directory[strlen($directory)-1] == '/')?'':'/';

        $rootFolder = $this->rootDir . '/../web';

        $fs = new Filesystem();

        try {

            $fs->remove($rootFolder . $directory . $file->getName());

            $this->entityManager->remove($file);
            $this->entityManager->flush();

            return true;

        } catch (IOExceptionInterface $e) {
            var_dump($e->getMessage());
        }

        return false;
        
    }

}