<?php
/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 5/2/17
 * Time: 9:45 AM
 */

namespace AppBundle\Service;

class SimpleXMLExtended extends \SimpleXMLElement {
    public function addChildWithCDATA($name, $value = NULL) {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}