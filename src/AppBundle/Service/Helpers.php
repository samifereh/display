<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 09/08/16
 * Time: 09:53
 */

namespace AppBundle\Service;


use AppBundle\Entity\Brouillon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Container;

class Helpers
{
    private $container;
    private $entityManager;
    private $session;

    public function __construct(Container $container, $session){
        $this->container    = $container;
        $this->entityManager           = $this->container->get('doctrine')->getManager();
        $this->session       = $session;
    }

    public function generateAgencePrefix($length = 3) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $exist = true;
        while ($exist) {
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $randomString = $randomString.'_';
            if(!$this->entityManager->getRepository('AppBundle:Group')->findOneBy(['prefix'=>$randomString])) {
                $exist = false;
                return $randomString;
            }
        }
        return null;
    }

    public function createBrouillon($key,$targetId = null) {
        $brouillon = new Brouillon();
        $brouillon->setSessionId($this->session->getId());
        $brouillon->setTargetId($targetId);
        $this->entityManager->persist($brouillon);
        $this->entityManager->flush();
        $this->session->set($key,$brouillon->getId());
        return $brouillon;
    }

    /**
     * get current group, agency and commercial for current user
     * @param Request $request
     * @return array
     */
    function getFilters($request){

        $userService = $this->container->get("app.user.service");
        $groupsList   = null;
        $groupsList  = $this->entityManager->getRepository('AppBundle:Group')->findGroups();
        $parentGroup = $userService->checkUserGroup($request);

        $agenciesList = null;
        $group        = $userService->checkUserAgency($request);
        $agenciesList = $this->entityManager->getRepository('AppBundle:Group')->getAgencyGroups($parentGroup);

        $userRepository = $this->entityManager->getRepository('AppBundle:User');
        $commercials = $userRepository->getCommercialOnly($group);
        return [
            'commercials' => $commercials,
            'agenciesList'=> $agenciesList,
            'groupsList'  => $groupsList
        ];
    }

    /**
     * get current group, agency and commercial for current user
     * @param Request $request
     * @return array
     */
    function getStatFilters($request){

        $userService = $this->container->get("app.user.service");
        $groupsList   = null;
        $groupsList  = $this->entityManager->getRepository('AppBundle:Group')->findGroups();
        $parentGroup = $userService->checkStatUserGroup($request);

        $agenciesList = null;
        $group        = $userService->checkStatUserAgency($request);
        $agenciesList = $this->entityManager->getRepository('AppBundle:Group')->getAgencyGroups($parentGroup);

        $userRepository = $this->entityManager->getRepository('AppBundle:User');
        $commercials = $userRepository->getCommercialOnly($group);
        return [
            'commercials' => $commercials,
            'agenciesList'=> $agenciesList,
            'groupsList'  => $groupsList
        ];
    }
}