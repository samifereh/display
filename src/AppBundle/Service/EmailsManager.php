<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Document;
use AppBundle\Entity\Gestion\Opportunity;

use AppBundle\Entity\Statistique\OpportunityStatistique;
use AppBundle\Repository\AdRepository;
use AppBundle\Repository\Broadcast\PortalRepository;
use AppBundle\Repository\Gestion\OpportunityRepository;
use AppBundle\Repository\Statistique\OpportunityStatistiqueRepository;
use AppBundle\Repository\UserRepository;
use PhpImap\IncomingMail;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Doctrine\ORM\EntityManager;

class EmailsManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var AdRepository
     */
    private $adRepository;
    /**
     * @var OpportunityRepository
     */
    private $opportunityRepository;
    /**
     * @var PortalRepository
     */
    private $portalRepository;
    /**
     * @var OpportunityStatistiqueRepository
     */
    private $opportunityStatistiqueRepository;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager               = $entityManager;
        $this->adRepository                = $entityManager->getRepository('AppBundle:Ad');
        $this->portalRepository            = $entityManager->getRepository('AppBundle:Broadcast\\Portal');
        $this->opportunityRepository       = $entityManager->getRepository('AppBundle:Gestion\\Opportunity');
        $this->opportunityStatistiqueRepository       = $entityManager->getRepository('AppBundle:Statistique\\OpportunityStatistique');
    }

    public function convertEmailToProposal(IncomingMail $email) {
        $opportunity = null;
        switch($email->fromAddress) {
            case 'no.reply@leboncoin.fr':
                $opportunity = $this->leboncoin($email);
                break;
            case 'selogerplus@ms.seloger.com':
                $opportunity = $this->seloger($email);
                break;
            case 'news@logicimmo.nmp1.com':
                $opportunity = $this->logicimmo($email);
                break;
            case 'contact@paruvendupro.fr':
                $opportunity = $this->paruvendupro($email);
                break;
//            case 'info@lesiteimmo.com':
//                $opportunity = $this->lesiteimmo($email);
//                break;
            case 'h.palmier@mediacc.com':
                if(strpos($email->subject, 'Logic-Immo.com')) {
                    $opportunity = $this->logicimmo($email);
                } elseif(strpos($email->subject, 'Leboncoin.fr')) {
                    $opportunity = $this->leboncoin($email);
                } elseif(strpos($email->subject, 'SeLoger.com')) {
                    $opportunity = $this->seloger($email);
                }
                break;
        }

        if($opportunity) {
            $opportunity->setStatus(Opportunity::WAITING);
            $this->entityManager->persist($opportunity);
            $this->entityManager->flush();
            return $opportunity;
        }
        return false;
    }

    private function seloger($email) {
        preg_match("/votre annonce référence \[(.*?)\]/ms", $email->subject, $matches);
        if(!isset($matches[1]))
            return null;
        $ref = $matches[1];

        $text = trim(preg_replace('/\s+/', ' ', $email->textPlain));
        preg_match("/MESSAGE DU CONTACT(.*)INFORMATIONS SUR LE CONTACT\s+Nom : (.*)\s+Email : (.*)\s+Tel : ([a-z,0-9]{9,10})\s+/ms", $text, $matches);
        if(!isset($matches[1]))
            return null;

        $message = trim($matches[1]);
        $prenom  = $matches[2];
        $emailAdresse   = trim($matches[3]);
        $tel     = $matches[4];
//        $ref     = $matches[7];
        /** @var Ad $ad */
        $ad = $this->adRepository->findOneBy(['reference' => $ref]);
        if(!$ad)
            $ad = $this->adRepository->findOneBy(['referenceV1' => $ref]);

        if($ad) {
            $portal = $this->portalRepository->findOneBy(['name' => 'SELOGER']);
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $email->date, new \DateTimeZone('Europe/Paris'));
            $opportunity = $this->opportunityRepository->findOneBy(['email'=>$emailAdresse, 'ad' => $ad]);
            if(is_null($opportunity)) {
                $opportunity = new Opportunity();
                $opportunity
                    ->setCommercial($ad->getOwner())
                    ->setSource('email')
                    ->setOpportunityGroup($ad->getGroup())
                    ->setTitle($email->subject)
                    ->setDate($date)
                    ->setEmail($emailAdresse)
                    ->setPrenom($prenom)
                    ->setTelephone($tel)
                    ->setAd($ad)
                    ->setNote($message)
                    ->addPortal($portal)
                ;
            } else {
                $opportunity->addPortal($portal);
            }
            $this->statistique($ad,$portal);
            return $opportunity;
        }
        return null;
    }

    private function leboncoin($email) {
        $text = trim(preg_replace('/\s+/', ' ', $email->textPlain));
        preg_match("/(.*)Coordonnées du contact\s+:\s+Nom : (.*)\s+Email :\s+(.*)\s+Cet email vous.*référence : \"(.*)?\"/ms", $text, $matches);
        if(!isset($matches[1]))
            return null;

        $message = trim($matches[1]);
        $prenom  = $matches[2];
        $emailAdresse   = trim($matches[3]);
        $ref     = $matches[4];
        /** @var Ad $ad */
        $ad = $this->adRepository->findOneBy(['reference' => $ref]);
        if(!$ad)
            $ad = $this->adRepository->findOneBy(['referenceV1' => $ref]);

        if($ad) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $email->date, new \DateTimeZone('Europe/Paris'));
            $opportunity = $this->opportunityRepository->findOneBy(['email'=>$emailAdresse, 'ad' => $ad]);
            $portal = $this->portalRepository->findOneBy(['name' => 'LEBONCOIN']);
            if(is_null($opportunity)) {
                $opportunity = new Opportunity();
                $opportunity
                    ->setCommercial($ad->getOwner())
                    ->setSource('email')
                    ->setOpportunityGroup($ad->getGroup())
                    ->setTitle($email->subject)
                    ->setDate($date)
                    ->setEmail($emailAdresse)
                    ->setPrenom($prenom)
                    ->setAd($ad)
                    ->setNote($message)
                    ->addPortal($portal)
                ;
            } else {
                $opportunity->addPortal($portal);
            }
            $this->statistique($ad,$portal);
            return $opportunity;
        }
        return null;
    }

    private function logicimmo($email) {
        $text = trim(preg_replace('/\s+/', ' ', $email->textPlain));
        //annonce réf.(.*)\s+(sur Logic-Immo.com|et pourrait).*COORDONNÉES DE L'INTERNAUTE\s+Nom : (.*)\s+Téléphone : ["|^$](.*)["|^$]\s+Email\s+:(.*) VOTRE ANNONCE|Message\s+:\s+(.*).*
        preg_match("/annonce réf.(.*)\s+(sur Logic\-Immo.com|et pourrait).*COORDONNÉES DE L'INTERNAUTE\s+Nom : (.*)\s+Téléphone : (.*)\s+Email\s+:\s(.*)\s+Message\s:\s(.*)VOTRE ANNONCE|VOTRE ANNONCE/ms", $text, $matches);
        if(!isset($matches[6])) {
            preg_match("/annonce réf.(.*)\s+(sur Logic\-Immo.com|et pourrait).*COORDONNÉES DE L'INTERNAUTE\s+Nom : (.*)\s+Téléphone : (.*)\s+Email\s+:\s(.*)\s+(Message\s:\s(.*)VOTRE ANNONCE|VOTRE ANNONCE)/ms", $text, $matches);
        }

        if(!isset($matches[1]))
            return null;

        $ref     = trim($matches[1]);
        $prenom  = $matches[3];
        $emailAdresse   = trim($matches[5]);
        $tel   = str_replace($matches[4], '"','');
        $message = $matches[6] != 'VOTRE ANNONCE' ? $matches[6] : null;


        /** @var Ad $ad */
        $ad = $this->adRepository->findOneBy(['reference' => $ref]);
        if(!$ad)
            $ad = $this->adRepository->findOneBy(['referenceV1' => $ref]);

        if($ad) {
            $portal = $this->portalRepository->findOneBy(['name' => 'LOGICIMMO']);
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $email->date, new \DateTimeZone('Europe/Paris'));
            $opportunity = $this->opportunityRepository->findOneBy(['email'=>$emailAdresse, 'ad' => $ad]);
            if(is_null($opportunity)) {
                $opportunity = new Opportunity();
                $opportunity
                    ->setCommercial($ad->getOwner())
                    ->setSource('email')
                    ->setOpportunityGroup($ad->getGroup())
                    ->setTitle($email->subject)
                    ->setDate($date)
                    ->setEmail($emailAdresse)
                    ->setPrenom($prenom)
                    ->setTelephone($tel)
                    ->setAd($ad)
                    ->setNote($message)
                    ->addPortal($portal);
            } else {
                $opportunity->addPortal($portal);
            }
            $this->statistique($ad,$portal);
            return $opportunity;
        }
        return null;
    }

    private function paruvendupro($email) {
        $text = trim(preg_replace('/\s+/', ' ', strip_tags($email->textHtml)));
        preg_match("/Réf. Pro :\s+(PRI_[a-z,0-9]{5,6}).* sur Internet\)(.*)Téléphone\s:\s(.*)Email\s:\s(.*)\sVoici son message\s:\s(.*)Répondre &Agrave/", $text, $matches);

        if(!isset($matches[1]))
            return null;

        $ref     = $matches[1];
        $prenom  = $matches[2];
        $tel     = $matches[3];
        $emailAdresse   = trim($matches[4]);
        $message = trim($matches[5]);
        /** @var Ad $ad */
        $ad = $this->adRepository->findOneBy(['reference' => $ref]);
        if(!$ad)
            $ad = $this->adRepository->findOneBy(['referenceV1' => $ref]);

        if($ad) {
            $portal = $this->portalRepository->findOneBy(['name' => 'PARUVENDU']);
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $email->date, new \DateTimeZone('Europe/Paris'));
            $opportunity = $this->opportunityRepository->findOneBy(['email'=>$emailAdresse, 'ad' => $ad]);
            if(is_null($opportunity)) {
                $opportunity = new Opportunity();
                $opportunity
                    ->setCommercial($ad->getOwner())
                    ->setSource('email')
                    ->setOpportunityGroup($ad->getGroup())
                    ->setTitle($email->subject)
                    ->setDate($date)
                    ->setEmail($emailAdresse)
                    ->setPrenom($prenom)
                    ->setTelephone($tel)
                    ->setAd($ad)
                    ->setNote($message)
                    ->addPortal($portal);
            } else {
                $opportunity->addPortal($portal);
            }
            $this->statistique($ad,$portal);
            return $opportunity;
        }
        return null;
    }

    private function lesiteimmo($email) {
        $text = trim(preg_replace('/\s+/', ' ', $email->textPlain));
        preg_match("/€\s(.*)–(.*)------ Objectif du client.*email suivante\s:\s(.*), ou bien par téléphone au (.{9,10})\s.*/", $text, $matches);
        if(!isset($matches[1]))
            return null;

        $ref     = $matches[1];
        $prenom  = $matches[2];
        $tel     = $matches[3];
        $emailAdresse   = $matches[4];
        $message = $matches[5];

        /** @var Ad $ad */
        $ad = $this->adRepository->findOneBy(['reference' => $ref]);
        if(!$ad)
            $ad = $this->adRepository->findOneBy(['referenceV1' => $ref]);

        if($ad) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $email->date, new \DateTimeZone('Europe/Paris'));
            $opportunity = $this->opportunityRepository->findOneBy(['email'=>$emailAdresse, 'ad' => $ad]);
            $portal = $this->portalRepository->findOneBy(['name' => 'LESITEIMMO']);//todo: a revoir
            if(is_null($opportunity)) {
                $opportunity = new Opportunity();
                $opportunity
                    ->setSource('email')
                    ->setOpportunityGroup($ad->getGroup())
                    ->setTitle($email->subject)
                    ->setDate($date)
                    ->setEmail($emailAdresse)
                    ->setPrenom($prenom)
                    ->setTelephone($tel)
                    ->setNote($message)
                    ->addPortal($portal);
            } else {
                $opportunity->addPortal($portal);
            }
//            $this->statistique($ad,$portal);
            return $opportunity;
        }
        return null;
    }

    private function statistique(Ad $ad, Portal $portal) {
        /** @var OpportunityStatistique $opportunityStatistique */
        $opportunityStatistique = $this->opportunityStatistiqueRepository->findOneBy(['date' => new \DateTime('today'), 'portal' => $portal, 'ad' => $ad]);
        if($opportunityStatistique) {
            $opportunityStatistique->addNbOpportunityReceived();
        } else {
            $opportunityStatistique = new OpportunityStatistique();
            $opportunityStatistique->setPortal($portal);
            $opportunityStatistique->setAd($ad);
        }
        $this->entityManager->persist($opportunityStatistique);
        $this->entityManager->flush();
    }

}