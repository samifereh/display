<?php
/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 5/2/17
 * Time: 9:44 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Service\Export\ExportFactory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class ExportHelpers
{
    /** @var \Liip\ImagineBundle\Imagine\Cache\CacheManager  */
    public $liipImagine;
    public $entityManager;
    public $container;

    /**
     * Helpers constructor.
     * @param \Liip\ImagineBundle\Imagine\Cache\CacheManager $liipImagine
     */
    public function __construct(\Liip\ImagineBundle\Imagine\Cache\CacheManager $liipImagine, EntityManager $entityManager,$container)
    {
        $this->liipImagine      = $liipImagine;
        $this->entityManager    = $entityManager;
        $this->container        = $container;
    }


    /**
     * @param $groups
     * @param $portalOrPortalRegistration
     * @param $exportPath
     * @param $fileName
     */
    public function dispatch($groups, $portalOrPortalRegistration, $exportPath, $fileName){
        ExportFactory::dispatch($this->container, $groups, $portalOrPortalRegistration, $exportPath, $fileName);
    }
    /**
     * @param $groups
     * @param $portal
     * @return string
     */
    public function fromGroupstoXML($groups,$portal)
    {
        $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><biens></biens>" );
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group) {
            $groupRegistratedPortal = [];
            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {
                $groupRegistratedPortal[] = $portalRegistration->getBroadcastPortal();
            };
            if($portal->existIn($groupRegistratedPortal)) {
                $ads = $this->entityManager->getRepository('AppBundle:Ad')->getAdsByPortalDiffusion($group,$portal);
                $this->toXML($ads,$portal,true,$rootNode);
            }
        }

        return ($rootNode && $rootNode->count() ? $rootNode->asXML() : null);
    }

    public function toXML($ads,$portalRegistration, $callback=false,&$rootNode = null)
    {
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }
        if(!$callback)
            $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><biens></biens>" );
        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($ads as $ad) {
            if($ad->getPublished() && $ad->getDiffusion()) {
                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                $login  = $portalRegistration->getIdentifier();
                $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
                $group      = $ad->getGroup();
                $agency = $ad->getGroup()->getAgency();
                $diffusion = $ad->getTypeDiffusion();
                $Terrain = $ad->getTerrain();
                $isTerrain = $diffusion == 'terrain' ? true : false;
                $Maison  = null;
                if(!$isTerrain)
                    $Maison  = $ad->getMaison();

                if($isTerrainPortal && !$isTerrain || !$isTerrain && !$Maison)
                    continue;

                $itemNode = $rootNode->addChild('bien');
                $itemNode->addChild( 'agence_nom', $agency->getName() );
                $itemNode->addChild( 'agence_tel', $agency->getTelephone() );
                $itemNode->addChild( 'agence_tel2', '' );
                $itemNode->addChild( 'agence_mail', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
                $itemNode->addChild( 'agence_mobile', '' );
                $itemNode->addChild( 'agence_fax', '' );
                $itemNode->addChild( 'agence_adresse', $agency->getAdress() );
                $itemNode->addChild( 'agence_postal', $agency->getCodePostal() );
                $itemNode->addChild( 'agence_postal_insee', '' );
                $itemNode->addChild( 'agence_ville', $agency->getVille() );
                $itemNode->addChild( 'agence_pays', 'France' );
                $itemNode->addChild( 'agence_rcs', '' );
                $itemNode->addChild( 'code_passerelle', $login );
                $itemNode->addChild( 'reference_logiciel', $ad->getReference() );
                $itemNode->addChild( 'reference_client', $ad->getReferenceInterne() );
                $itemNode->addChild( 'numero_mandat', '' );
                $itemNode->addChild( 'type_mandat', '' );
                $itemNode->addChild( 'agence_tel_a_afficher', ($this->container->get("app.tracking.export.helpers")->getAgencyPhoneTracking($agency)));
                $itemNode->addChild( 'negociateur_nom', $agency->getNom().' '.$agency->getPrenom() );
                $itemNode->addChild( 'negociateur_tel', $agency->getTelephone() );
                $itemNode->addChild( 'negociateur_mail', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
                $imagesNode = $itemNode->addChild("photos");
                $imagesNode->addChild( 'photo', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(), 'poliris_image') : '' );
                if($ad->getImagesDocuments()){
                    foreach($ad->getImagesDocuments() as $index => $imageDoc) {
                        $imagesNode->addChild( 'photo',
                            $imageDoc->getName() ?
                                $this->liipImagine->getBrowserPath('/uploads/ad/'.$imageDoc->getName(), 'poliris_image') :
                                ''  );
                    }
                }
                $itemNode->addChild( 'type_transaction', 'vente' );
                $itemNode->addChild( 'type_bien', $isTerrain ?'Terrain':'Maison');
                $itemNode->addChildWithCDATA( 'publication_web', $ad->getDescription() );
                $itemNode->addChild( 'publication_luxe', '' );
                $itemNode->addChild( 'publication_liste', '' );
                $titres = $itemNode->addChild( 'titres');
                $titres->addChild('titre_web_fr',$ad->getLibelle());
                $titres->addChild('titre_print_fr',$ad->getLibelle());
                $titres->addChild('titre_web_en','');
                $titres->addChild('titre_print_en','');

                $descriptions = $itemNode->addChild( 'descriptions');
                $descriptions->addChildWithCDATA('descriptif_web_fr',$ad->getDescription());
                $descriptions->addChildWithCDATA('descriptif_print_fr',$ad->getDescription());
                $descriptions->addChildWithCDATA('descriptif_web_en','');
                $descriptions->addChildWithCDATA('descriptif_print_en','');
                $itemNode->addChild( 'adresse', $ad->getAdresse() );
                $itemNode->addChild( 'code_postal_a_afficher', $ad->getCodePostal() );
                $itemNode->addChild( 'code_postal_reel', $ad->getCodePostal() );
                $itemNode->addChild( 'code_postal_insee', $ad->getCodePostal() );
                $itemNode->addChild( 'commune_reel', $ad->getVille() );
                $itemNode->addChild( 'commune_a_afficher', $ad->getVille() );
                $itemNode->addChild( 'secteur', '' );
                $itemNode->addChild( 'is_confidence_commune', '' );
                $itemNode->addChild( 'gps_longitude', '' );
                $itemNode->addChild( 'gps_latitude', '' );
                $itemNode->addChild( 'proximite_liste', '' );
                $itemNode->addChild( 'proximite_gare', '' );
                $itemNode->addChild( 'proximite_metro', '' );
                $itemNode->addChild( 'proximite_commerce', '' );
                $itemNode->addChild( 'proximite_bus', '' );
                $itemNode->addChild( 'proximite_tourisme', '' );
                $itemNode->addChild( 'proximite_culture', '' );
                $itemNode->addChild( 'proximite_ecole', '' );
                $itemNode->addChild( 'proximite_espace_vert', '' );
                $itemNode->addChild( 'prix', $ad->getPrix() );
                $itemNode->addChild( 'montant_frais_agence', '' );
                $itemNode->addChild( 'montant_charge', '' );
                $itemNode->addChild( 'montant_taxe', '' );
                $itemNode->addChild( 'devise', 'EUR' );
                $itemNode->addChild( 'is_confidence_prix', '' );
                $itemNode->addChild( 'is_frais_agence_inclus', '' );
                $itemNode->addChild( 'is_charge_comprise', '' );
                $itemNode->addChild( 'dpe_bilan', '' );
                $itemNode->addChild( 'dpe_valeur', '' );
                $itemNode->addChild( 'dpe_date', '' );
                $itemNode->addChild( 'ges_bilan', '' );
                $itemNode->addChild( 'ges_valeur', '' );
                $itemNode->addChild( 'ges_date', '' );
                $itemNode->addChild( 'is_dpe_eligible', '' );
                $itemNode->addChild( 'is_ges_eligible', '' );
                $itemNode->addChild( 'is_batiment_basse_conso', '' );
                $itemNode->addChild( 'type_chauffage', '' );
                $itemNode->addChild( 'nature_chauffage', '' );

                $typeCuisine = '';
                if($Maison && $Maison->getTypeCuisine()) {
                    switch($Maison->getTypeCuisine()) {
                        case 1:$typeCuisine = '' ;break;
                        case 2:$typeCuisine = 'Americaine' ;break;
                        case 3:$typeCuisine = 'Separee' ;break;
                        case 4:$typeCuisine = 'Industrielle' ;break;
                        case 5:$typeCuisine = 'CoinCuisine' ;break;
                        case 6:$typeCuisine = 'AmericaineEquipee' ;break;
                        case 7:$typeCuisine = 'SepareeEquipee' ;break;
                        case 8:$typeCuisine = 'CoinCusineEquipee' ;break;
                    }
                }
                $itemNode->addChild( 'type_cuisine', $typeCuisine );

                $itemNode->addChild( 'nb_pieces', !$isTerrain ? $Maison->getNbPieces() : '');
                $itemNode->addChild( 'nb_chambres', $isTerrain ? '' : $Maison->getNbChambres() );
                $itemNode->addChild( 'nb_etages', '' );
                $itemNode->addChild( 'nb_gardiens', '' );
                $itemNode->addChild( 'nb_salle_d_eau', $isTerrain ? '' : $Maison->getNbSalleEau() );
                $itemNode->addChild( 'nb_salle_de_bain', $isTerrain ? '' : $Maison->getNbSalleBain() );
                $itemNode->addChild( 'nb_garages', '' );
                $itemNode->addChild( 'nb_parkings', '' );
                $itemNode->addChild( 'nb_parkings_exterieurs', '' );
                $itemNode->addChild( 'nb_parkings_interieurs', '' );
                $itemNode->addChild( 'nb_tennis', '' );
                $itemNode->addChild( 'nb_appartements', '' );
                $itemNode->addChild( 'nb_personnes', '' );
                $itemNode->addChild( 'nb_wc', !$isTerrain && $Maison->getSiToiletteValue() ? $Maison->getNbToilette() : '' );
                $itemNode->addChild( 'surface_bien', $isTerrain ? '' : ($Maison?(int)$Maison->getSurface():0) );
                $itemNode->addChild( 'surface_sejour',$isTerrain ? '' : ($Maison->getSiSejourValue() ? $Maison->getSurfaceSejour() : '') );
                $itemNode->addChild( 'surface_terrain', $Terrain?(int)$Terrain->getSurface():0 );
                $itemNode->addChild( 'surface_terrasse', $isTerrain ? '' : ($Maison->getSiTerrasseValue() ? $Maison->getSurfaceTerrasse() : '') );
                $itemNode->addChild( 'is_duplex', '' );
                $itemNode->addChild( 'is_investissement', '' );
                $itemNode->addChild( 'is_meuble', '' );
                $itemNode->addChild( 'is_recent', '' );
                $itemNode->addChild( 'is_refait', '' );
                $itemNode->addChild( 'is_travaux', '' );
                $itemNode->addChild( 'is_acces_handicapes', '' );
                $itemNode->addChild( 'is_animaux_acceptes', '' );
                $itemNode->addChild( 'is_loi_carrez', '' );
                $itemNode->addChild( 'is_terrain_constructible', $isTerrain && $Terrain->getType() == 'terrain_a_batir' ? true : false);
                $itemNode->addChild( 'has_ascenseur', '' );
                $itemNode->addChild( 'has_balcon', $isTerrain ? '' : ($Maison->getSiBalconsValue() ? true : false) );
                $itemNode->addChild( 'has_terrasse', $isTerrain ? '' : ($Maison->getSiTerrasseValue() ? true : false) );
                $itemNode->addChild( 'has_climatisation', '' );
                $itemNode->addChild( 'has_piscine', '' );
                $itemNode->addChild( 'has_cabletv', '' );
                $itemNode->addChild( 'has_cave', $isTerrain ? '' : ($Maison->getSiCaveValue() ? true : false) );
                $itemNode->addChild( 'has_cellier', '' );
                $itemNode->addChild( 'has_garage', '' );
                $itemNode->addChild( 'has_box', '' );
                $itemNode->addChild( 'has_grenier', '' );
                $itemNode->addChild( 'has_jardin', '' );
                $itemNode->addChild( 'has_veranda', $isTerrain ? '' : ($Maison->getSiVerandaValue() ? true : false) );
                $itemNode->addChild( 'annee_construction', '' );
                $itemNode->addChild( 'date_de_livraison', '' );
                $itemNode->addChild( 'liste_annexes', '' );
                $itemNode->addChild( 'etage', '' );
                $itemNode->addChild( 'expositions', $isTerrain ? $Terrain->getExposition() : '' );
                $itemNode->addChild( 'url_visite_virtuelle', $ad->getLienVisitVirtuel() );
                $itemNode->addChild( 'url_bien_internet', '' );
            }
        }
        if(!$rootNode)
            return ($rootNode && $rootNode->count() ? $rootNode->asXML() : null);
        return $rootNode;
    }

    public function fromGroupstoAchatTerrainXML($groups,$portal)
    {
        $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><ANNONCES></ANNONCES>" );
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group) {
            $groupRegistratedPortal = [];
            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration)
                $groupRegistratedPortal[] = $portalRegistration->getBroadcastPortal();
            if($portal->existIn($groupRegistratedPortal)) {
                $ads = $this->entityManager->getRepository('AppBundle:Ad')->getAdsByPortalDiffusion($group,$portal);
                $this->toAchatTerrainXML($ads,$portal,true,$rootNode);
            }
        }
        return $rootNode->asXML();
    }

    public function toAchatTerrainXML($ads,$portalRegistration,$callback = false, &$rootNode = null)
    {
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }
        if(!$callback)
            $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><ANNONCES></ANNONCES>" );
        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($ads as $ad) {
            if($ad->getPublished() && $ad->getDiffusion()) {
                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
                $group = $ad->getGroup();
                $agency = $ad->getGroup()->getAgency();
                $diffusion = $ad->getTypeDiffusion();
                $isTerrain = $diffusion == 'terrain' ? true : false;
                $Terrain = $ad->getTerrain();
                $Maison  = null;
                if($ad->getTypeDiffusion() != 'terrain')
                    $Maison  = $ad->getMaison();

                if($isTerrainPortal && !$isTerrain)
                    continue;
                $itemNode = $rootNode->addChild('ANNONCE');
                $itemNode->addChild( 'AGENCE_REF', $ad->getReferenceInterne() );
                $itemNode->addChildWithCDATA( 'AGENCE_NOM', $agency->getName() );
                $itemNode->addChild( 'AGENCE_URL', '' );
                $itemNode->addChildWithCDATA( 'AGENCE_ADRESSE', $agency->getAdress() );
                $itemNode->addChild( 'AGENCE_VILLE', $agency->getVille() );
                $itemNode->addChild( 'AGENCE_CP', $agency->getCodePostal() );
                $itemNode->addChild( 'AGENCE_MAIL', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
                $itemNode->addChild( 'AGENCE_TEL', $agency->getTelephone() );
                $itemNode->addChild( 'AGENCE_FAX', '' );
                $itemNode->addChild( 'AGENCE_LOGO', $agency->getAvatar() ? $this->liipImagine->getBrowserPath($agency->getAvatar(), 'poliris_image') : '' );
                $itemNode->addChild( 'BIEN_REF', $ad->getReference() );
                $itemNode->addChild( 'BIEN_DATE', '' );
                $itemNode->addChild( 'BIEN_DATE_MODIF', '' );
                $itemNode->addChild( 'BIEN_VILLE', $ad->getVille() );
                $itemNode->addChild( 'BIEN_CP', $ad->getCodePostal() );
                $itemNode->addChild( 'BIEN_SURFACE_TERRAIN', $isTerrain ? ($Terrain?(int)$Terrain->getSurface():0) : '' );
                $itemNode->addChild( 'BIEN_SURFACE_MAISON', $Maison ? ($Maison?(int)$Maison->getSurface():'') : '' );
                $itemNode->addChild( 'BIEN_NOMBRE_PIECES', $Maison ? $Maison->getNbPieces() : '' );
                $itemNode->addChild( 'BIEN_NOMBRE_CHAMBRES', $Maison ? $Maison->getNbChambres() : '' );
                $itemNode->addChild( 'BIEN_MODELE', '' );
                $itemNode->addChild( 'BIEN_PRIX', $ad->getPrix() );
                $typeBien = 22;
                if($diffusion == 'terrain') {
                    switch($Terrain->getType()) {
                        default: $typeBien = 20 ;
                        case 'terrain_a_batir': $typeBien = 20 ;break;
                        case 'terrain_de_loisir': $typeBien = 24 ;break;
                        case 'terrain_agricole': $typeBien = 25 ;break;
                        //todo: manque terrain industriel
                    }
                }

                $itemNode->addChild( 'BIEN_TYPE', $typeBien );//todo voir annexe
                $itemNode->addChildWithCDATA( 'BIEN_TITRE', $ad->getLibelle() );
                $itemNode->addChildWithCDATA( 'BIEN_TEXTE', $ad->getDescription() );
                $itemNode->addChild( 'BIEN_PHOTO_1', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(), 'poliris_image') : '' );
                for($i=0; $i <= 2; $i++) {
                    if($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                        $itemNode->addChild( 'BIEN_PHOTO_'.($i+2), $ad->getImagesDocuments()[$i]->getName() ? $this->liipImagine->getBrowserPath('/uploads/ad/'.$ad->getImagesDocuments()[$i]->getName(), 'poliris_image') : '' );
                    } else {
                        $itemNode->addChild( 'BIEN_PHOTO_'.($i+2), '' );
                    }
                }
                $itemNode->addChild( 'BIEN_VENDU', 0 );
            }
        }
        if(!$callback)
            return $rootNode->asXML();

        return $rootNode;
    }

    public function toUbiflowXML($ads,$portalRegistration,$callback = false, &$rootNode = null)
    {
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        $login  = $portalRegistration->getIdentifier();
        if(!$login || !$portal)
            return;

        if(!$callback) {
            $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><annonceur code='$login'></annonceur>" );
            $coordonneesNode = $rootNode->addChild('coordonnees');
            $coordonneesNode->addChild('raison_sociale','test_diff');
            $coordonneesAdresseNode =   $coordonneesNode->addChild('adresse');
            $coordonneesAdresseNode->addChild('voirie','6 rue de flow');
            $coordonneesAdresseNode->addChild('code_postal','35000');
            $coordonneesAdresseNode->addChild('ville','rennes');
            $coordonneesNode->addChild('telephone','test_diff');
            $coordonneesNode->addChild('fax','');
            $coordonneesNode->addChild('email','');
            $coordonneesNode->addChild('web','');
        }

        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($ads as $ad) {
            if($ad->getPublished() && $ad->getDiffusion()) {
                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
                $group = $ad->getGroup();
                $agency = $ad->getGroup()->getAgency();
                $diffusion = $ad->getTypeDiffusion();
                $isTerrain = $diffusion == 'terrain' ? true : false;
                /** @var Terrain $Terrain */
                $Terrain = $ad->getTerrain();
                /** @var Maison|null $Maison */
                $Maison  = null;
                if($ad->getTypeDiffusion() != 'terrain')
                    $Maison  = $ad->getMaison();

                if($isTerrainPortal && !$isTerrain)
                    continue;

                if(!$isTerrain && !$Maison) { continue; }

                $annoncesNode = $rootNode->addChild('annonces');
                $annonceNode  = $annoncesNode->addChild( 'annonce');
                $annonceNode->addAttribute('id',$ad->getId());
                $annonceNode->addChild('reference',$ad->getReferenceInterne());
                $annonceNode->addChild('titre',substr($ad->getLibelle(), 0 ,60));
                $annonceNode->addChild('titre_anglais','');
                $annonceNode->addChildWithCDATA( 'texte', $ad->getDescription() );
                $annonceNode->addChildWithCDATA( 'texte_anglais', '' );
                $annonceNode->addChild('date_saisie',$ad->getUpdatedAt()->format('d/m/Y'));
                $annonceNode->addChild('date_integration',$ad->getUpdatedAt()->format('d/m/Y H:i:s'));
                $annonceNode->addChild('contact_a_afficher',$agency->getNom().' '.$agency->getPrenom());
                $annonceNode->addChild('email_a_afficher',$group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());
                $annonceNode->addChild('telephone_a_afficher',($agency->getTrackingPhone() ? $agency->getTrackingPhone() : $agency->getTelephone()));
                $annoncePhotosNode = $annonceNode->addChild('photos');
                $annoncePhotosNode->addChild( 'photo', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(), 'poliris_image') : '' );
                for($i=0; $i <= 2; $i++) {
                    if($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                        $annoncePhotosNode->addChild( 'photo', $ad->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/ad/'.$ad->getImagesDocuments()[$i]->getName(), 'poliris_image') : '' );
                    }
                }
                $annonceNode->addChild('visite_virtuelle',$ad->getLienVisitVirtuel());

                $annonceBienNode = $annonceNode->addChild('bien');
                $annonceBienNode->addChild('code_type','0'); //todo: a revoir
                $annonceBienNode->addChild('libelle_type','Terrain');
                $annonceBienNode->addChild('constructible',$Terrain->getConstructible() ? 'O' : 'N');
                $annonceBienNode->addChild('avec_pente',$Terrain->getPente() ? 'O' : 'N');
                $annonceBienNode->addChild('avec_vis_a_vis',$Terrain->getVisAVis() ? 'O' : 'N');
                $annonceBienNode->addChild('cos','12.5');
                $annonceBienNode->addChild('longueur_terrain','');
                $annonceBienNode->addChild('largeur_terrain','');
                $annonceBienNode->addChild('nb_pieces_logement',!$isTerrain ? $Maison->getNbPieces() : '');
                $annonceBienNode->addChild('surface_sejour',$isTerrain ? '' : ($Maison->getSiSejourValue() ? $Maison->getSurfaceSejour() : ''));
                $annonceBienNode->addChild('nombre_de_chambres',$isTerrain ? '' : $Maison->getNbChambres());
                $annonceBienNode->addChild('exposition',$isTerrain ? $Terrain->getExpositionValue() : $Maison->getExpositionValue());
                $annonceBienNode->addChild('type_cuisine',$isTerrain ? '' : $Maison->getTypeCuisineValue());
                $annonceBienNode->addChild('nb_salles_de_bain',$isTerrain ? '' :  $Maison->getNbSalleBain() );
                $annonceBienNode->addChild('nb_salles_d_eau',$isTerrain ? '' :  ($Maison->getSiSalleEauValue() ? $Maison->getNbSalleEau() : '') );
                $annonceBienNode->addChild('terrasse',$isTerrain ? '' :  ($Maison->getSiTerrasseValue() ? 'O' : 'N') );

                $terrainProche = [];
                if($Terrain->getProcheGare()) { $terrainProche[] = 'Gare'; }
                if($Terrain->getProcheMetro()) { $terrainProche[] = 'Metro'; }

                $annonceBienNode->addChild('commentaires_proche_transports',$isTerrain ? implode($terrainProche,',') : '' );
                $annonceBienNode->addChild('commentaires_proche_ecoles',$isTerrain ? ($Terrain->getProcheEcole() ? 'Oui' : 'Non') : '' );
                $annonceBienNode->addChild('commentaires_proche_commerces',$isTerrain ? ($Terrain->getProcheCommerces() ? 'Oui' : 'Non') : '' );
                $annonceBienNode->addChild('garage',!$isTerrain ? ($Maison->getSiParkingValue() ? 'O' : 'N') : '' );
                $annonceBienNode->addChild('nb_garages',!$isTerrain ? ($Maison->getSiParkingValue() ? $Maison->getNbParking() : '') : '' );
                $annonceBienNode->addChild('style_construction',!$isTerrain ? $Maison->getTypeArchitectureValue() : '' );
                $annonceBienNode->addChild('cheminee',$isTerrain ? '' :  ($Maison->getSiChemineeValue() ? 'O' : 'N') );
                $annonceBienNode->addChild('nb_wc',$isTerrain ? '' :  ($Maison->getSiToiletteValue() ? $Maison->getNbToilette() : '') );
                $annonceBienNode->addChild('cave',$isTerrain ? '' :  ($Maison->getSiCaveValue() ? 'O' : 'N') );
                $annonceBienNode->addChild('veranda',$isTerrain ? '' :  ($Maison->getSiVerandaValue() ? 'O' : 'N') );
                $annonceBienNode->addChild('piscine',$isTerrain ? '' :  ($Maison->getSiPiscine() ? 'O' : 'N') );
                $annonceBienNode->addChild('chauffage_type',$isTerrain ? '' : $Maison->getTypeDeChauffageValue() );
                $annonceBienNode->addChild('chauffage_energie',$isTerrain ? '' : $Maison->getEnergieDeChauffage() );

                $annonceBienNode->addChild('pays','France');
                $annonceBienNode->addChild('code_postal',$agency->getCodePostal());
                $annonceBienNode->addChild('departement',substr($agency->getCodePostal(), 0,2));
                $annonceBienNode->addChild('ville',$agency->getVille());
                $annonceBienNode->addChild('surface_terrain',$Terrain?(int)$Terrain->getSurface():'');
                if($Terrain) $annonceBienNode->addChild('prix_terrain',$Terrain->getPrix());
                $annonceBienDisgnostiquesNode = $annonceBienNode->addChild('diagnostiques');
                if(!$Terrain) {
                    $annonceBienDisgnostiquesNode->addChild('dpe_valeur_conso',$Maison->getDpeValue());
                    $annonceBienDisgnostiquesNode->addChild('dpe_valeur_ges',$Maison->getGesValue());
                    $annonceBienDisgnostiquesNode->addChild('dpe_etiquette_conso',$Maison->getDPE());
                    $annonceBienDisgnostiquesNode->addChild('dpe_etiquette_ges',$Maison->getGES());
                }

                $annoncePrestationNode = $annonceNode->addChild('prestation');
                $annoncePrestationNode->addChild('type','V');
                $annoncePrestationNode->addChild('prix',$ad->getPrix());
            }
        }
        if(!$callback)
            return $rootNode->asXML();

        return $rootNode;
    }

    public function fromGroupstoPolirisCSV($groups,$portal)
    {
        $csv = '';
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group) {
            $groupRegistratedPortal = [];
            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration)
                $groupRegistratedPortal[] = $portalRegistration->getBroadcastPortal();
            if($portal->existIn($groupRegistratedPortal)) {
                $ads = $this->entityManager->getRepository('AppBundle:Ad')->getAdsByPortalDiffusion($group,$portal);
                if(count($ads) > 0)
                    $csv .= $this->toPolirisCSV($ads,$portal, false);
            }
        }
        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    public function toPolirisCSV($ads,$portalRegistration, $convert = true)
    {
        if(!count($ads))
            return null;

        $csv = '';
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $registration */
            foreach ($portalRegistration->getBroadcastPortalRegistrations() as $registration) {
                if($registration->getGroup() == $ads[0]->getGroup()) {
                    $portalRegistration = $registration;
                    break;
                }
            }
        } else {
            $portal = null;
        }

        //todo test expire
        /** @var \AppBundle\Entity\Ad $ad */
        foreach ($ads as $ad) {
            if(
                $ad->getPublished() && $ad->getDiffusion() &&
                $portal->existIn($ad->getDiffusion()->getBroadcastPortals()) &&
                $portalRegistration instanceof PortalRegistration
            ) {
                if ($portal->getCluster() == "GRATUIT" && $portal->getExportFileName()) {
                    $login = $portal->getExportFileName();
                } else {
                    $login = $portalRegistration->getIdentifier();
                    if (!$login || !$portal) {
                        continue;
                    }
                }

                $newLine = "\r\n";
                if (!$ad instanceof Ad) {
                    continue;
                }
                $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true : false;
                $isMaisonPortal = $portal->getTypeDiffusion() == 'maison' ? true : false;

                /** @var Portal $portalName */
                $portalName = $portal->getName();
                $isEvrovilla = ($portal->getSlug() == 'evrovilla');
                $agency = $ad->getGroup()->getAgency();
                $group = $ad->getGroup();
                $diffusion = $ad->getTypeDiffusion();
                /** @var Terrain  $Terrain */
                $Terrain = $ad->getTerrain();
                $Maison = null;
                $isTerrain = $diffusion == 'terrain' ? true : false;
                $isSiteEnseigne = ($portal->getSlug() == 'site-enseigne') ;

                if (!$isTerrain) {
                    /** @var Maison $Maison */
                    $Maison = $ad->getMaison();
                }

                if (($isTerrainPortal && !$isTerrain) || ($isMaisonPortal && $isTerrain) || (!$isTerrain && !$Maison)) {
                    continue;
                }

                $title = $ad->getLibelle();
                $description = $ad->getDescription();
                if ($adRewrite = $ad->getGroup()->getAdRewrite()) {
                    if ($adRewrite->getPortals()->contains($portal)) {
                        $title = $this->Spin($adRewrite->getTitle(), $ad);
                        $description = $this->Spin($adRewrite->getDescription(), $ad);
                    }
                }
                if ($adMentionLegals = $ad->getGroup()->getAdMentionLegals()) {
                    if ($adMentionLegals->getPortals()->contains($portal)) {
                        $description .= $adMentionLegals->getBody();
                    }
                }
                $csv .= self::generateCSVField($login);  #1
                $csv .= self::generateCSVField($ad->getReference());        # 2 //todo Reference generé ou autre?
                $csv .= self::generateCSVField('vente', 'select');                                            # 3
                $csv .= self::generateCSVField($isTerrain ? 'terrain' : "maison", 'select');        # 4
                $csv .= self::generateCSVField($ad->getCodePostal(),
                    'number');                                        # 5
                $csv .= self::generateCSVField($ad->getVille());                                             # 6
                $csv .= self::generateCSVField('France');/*$ad->getPays()*/                                  # 7
                $csv .= self::generateCSVField($ad->getAdresse());                                           # 8
                $csv .= self::generateCSVField('');                                                          # 9 //todo use xml flux to set quartier value
                $csv .= self::generateCSVField('');                                                          # 10
                $csv .= self::generateCSVField($ad->getPrix(), 'float'); # 11
                $csv .= self::generateCSVField('');                                                          # 12
                $csv .= self::generateCSVField('');                                                          # 13
                $csv .= self::generateCSVField('');                                                          # 14
                $csv .= self::generateCSVField('');                                                          # 15
                $csv .= self::generateCSVField($isTerrain ? '' : ($Maison ? $Maison->getSurface() : ''),
                    'float');                                                          # 16
                $csv .= self::generateCSVField(($Terrain ? $Terrain->getSurface() : ""),
                    'float');                    # 17
                $csv .= $isTerrain ? self::generateCSVField('') : self::generateCSVField($Maison->getNbPieces(),
                    'number'); # 18

                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbChambres(),
                    'number');                   # 19
                $csv .= self::generateCSVField($title);                                           # 20
                if ($portalName == "LEBONCOINSP" && !$isTerrain) {
                    $description = "Sur la commune de " . $ad->getVille() . ", " . $agency->getName() . " vous propose une villa de " . $Maison->getSurfaceModel() . " m2 habitables sur un terrain de " . ($Terrain ? $Terrain->getSurface() : "") . " m2.";
                }
                $csv .= self::generateCSVField($description);                                       # 21 //todo limité les caractères specials

                $csv .= self::generateCSVField('');                                                          # 22
                $csv .= self::generateCSVField('');                                                          # 23 //todo: a voir avec Hervé
                $csv .= self::generateCSVField('');                                                          # 24
                $csv .= self::generateCSVField('');                                                          # 25
                $csv .= self::generateCSVField('');                                                          # 26
                $csv .= self::generateCSVField('');                                                          # 27
                $csv .= self::generateCSVField('');                                                          # 28
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbSalleBain(),
                    'number');                 # 29
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbSalleEau(),
                    'number');                  # 30
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiToiletteValue() ? $Maison->getNbToilette() : '',
                    'number');                  # 31
                $csv .= self::generateCSVField('');                                                          # 32
                $csv .= self::generateCSVField('');                                                          # 33
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getTypeCuisine(),
                    'number');                 # 34
                $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),
                    'sud') !== false ? 'OUI' : 'NON') : '', 'boolean');   # 35
                $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),
                    'est') !== false && strpos($Terrain->getExposition(), 'ouest') == false ? 'OUI' : 'NON') : '',
                    'boolean');   # 36
                $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),
                    'ouest') !== false ? 'OUI' : 'NON') : '', 'boolean');
                $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),
                    'nord') !== false ? 'OUI' : 'NON') : '', 'boolean');   # 38
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiBalcons() ? $Maison->getNbBalcons() : '',
                    'number');                         # 39
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiBalcons() ? $Maison->getSurfaceBalcons() : '',
                    'number');                    # 40
                $csv .= self::generateCSVField('');                                                                 # 41
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiCaveValue() ? 'OUI' : 'NON',
                    'boolean');                             # 42
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiParkingValue() ? $Maison->getNbParking() : '',
                    'number');               # 43
                $csv .= self::generateCSVField('');               # 44
                $csv .= self::generateCSVField('');               # 45
                $csv .= self::generateCSVField('');               # 46
                $csv .= self::generateCSVField('');               # 47
                $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiTerrasseValue() ? 'OUI' : 'NON'),
                    'boolean');               # 48
                $csv .= self::generateCSVField('');               # 49
                $csv .= self::generateCSVField('');               # 50
                $csv .= self::generateCSVField('');               # 51
                $csv .= self::generateCSVField('');               # 52
                $csv .= self::generateCSVField('');               # 53
                $csv .= self::generateCSVField('');               # 54
                $csv .= self::generateCSVField('');               # 55
                $csv .= self::generateCSVField('');               # 56
                $csv .= self::generateCSVField('');               # 57
                $csv .= self::generateCSVField('');               # 58
                $csv .= self::generateCSVField('');               # 59
                $csv .= self::generateCSVField('');               # 60
                $csv .= self::generateCSVField('');               # 61
                $csv .= self::generateCSVField('');               # 62
                $csv .= self::generateCSVField('');               # 63
                $csv .= self::generateCSVField('');               # 64
                $csv .= self::generateCSVField('');               # 65
                $csv .= self::generateCSVField('');               # 66
                $csv .= self::generateCSVField('');               # 67
                $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiChemineeValue() ? 'OUI' : 'NON'),
                    'boolean');                # 68
                $csv .= self::generateCSVField('');               # 69
                $csv .= self::generateCSVField('');               # 70
                $csv .= self::generateCSVField('');               # 71
                $csv .= self::generateCSVField('');               # 72
                $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiPlacardsValue() ? 'OUI' : 'NON'),
                    'boolean');                 # 73
                $csv .= self::generateCSVField('');               # 74
                $csv .= self::generateCSVField('');               # 75
                $csv .= self::generateCSVField('');               # 76
                $csv .= self::generateCSVField('');               # 77
                $csv .= self::generateCSVField('');               # 78
                $csv .= self::generateCSVField('');              # 79
                $csv .= self::generateCSVField('');               # 80
                $csv .= self::generateCSVField('');               # 81
                $csv .= self::generateCSVField('');               # 82
                $csv .= self::generateCSVField('');               # 83
                $csv .= self::generateCSVField('');               # 84
                $csv .= self::generateCSVField($ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(),
                    'poliris_image') : '');               # 85

                for ($i = 0; $i <= 8; $i++) {
                    if ($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                        $csv .= self::generateCSVField($ad->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/ad/' . $ad->getImagesDocuments()[$i]->getName(),
                            'poliris_image') : '');
                    } else {
                        $csv .= self::generateCSVField('');
                    }
                }
                # 94 -> 102
                for ($i = 0; $i <= 8; $i++) {
                    if ($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                        $csv .= self::generateCSVField($ad->getImagesDocuments()[$i]->getTitle());
                    } else {
                        $csv .= self::generateCSVField('');
                    }
                }
                $csv .= self::generateCSVField($ad->getLienVisitVirtuel());        # 104
                $csv .= self::generateCSVField( $isSiteEnseigne ?
                    $this->container->get("app.tracking.export.helpers")->getAgencyPhoneTracking($agency) :
                    ($portalRegistration instanceof PortalRegistration ?
                        $this->container->get("app.tracking.export.helpers")
                            ->getAdPhoneTracking($ad, $portalRegistration) : '')); #105
                $csv .= self::generateCSVField($isSiteEnseigne ? $agency->getName() :
                    $agency->getPrenom() . ' ' . $agency->getNom());                # 106
                $csv .= self::generateCSVField($group->getEmailAlias() ? $group->getEmailAlias() :
                    $agency->getEmail());                                           # 107
                if ($portalName == "REFLEXIMMO" || $portalName == "IMMOGOODDEAL") {
                    //todo 105 ->
                }
                $csv .= self::generateCSVField($agency->getCodePostal());            # 108
                $csv .= self::generateCSVField($agency->getVille());                 # 109
                $csv .= self::generateCSVField('');               # 110
                $csv .= self::generateCSVField('');               # 111
                $csv .= self::generateCSVField($ad->getReferenceInterne());           # 112
                $csv .= self::generateCSVField('');               # 113
                $csv .= self::generateCSVField('');               # 114
                $csv .= self::generateCSVField('');               # 115
                $csv .= self::generateCSVField('');               # 116
                if ($portal->getAfficherMondat() || $portalName == "ORNOX"
                    || $portalName == "EMAILIMMO" || $portalName == "TERRAINCONSTRUCTION2")
                {
                    $csv .= self::generateCSVField($agency->getAdress());               # 117
                    $csv .= self::generateCSVField($agency->getCodePostal());               # 118
                    $csv .= self::generateCSVField($agency->getVille());               # 119
                } else {
                    $csv .= self::generateCSVField('');               # 117
                    $csv .= self::generateCSVField('');               # 118
                    $csv .= self::generateCSVField('');               # 119
                }
                $csv .= self::generateCSVField('');               # 120
                $csv .= self::generateCSVField('');               # 121
                $csv .= self::generateCSVField('');               # 122
                $csv .= self::generateCSVField($isSiteEnseigne ? $ad->getOwner()->getName() : '');               # 123
                $csv .= self::generateCSVField('');               # 124
                $csv .= self::generateCSVField('');               # 125
                $csv .= self::generateCSVField('');               # 126
                $csv .= self::generateCSVField('');               # 127
                $csv .= self::generateCSVField('');               # 128
                $csv .= self::generateCSVField('');               # 129
                $csv .= self::generateCSVField('');               # 130
                $csv .= self::generateCSVField('');               # 131
                $csv .= self::generateCSVField('');               # 132
                $csv .= self::generateCSVField('');               # 133
                $csv .= self::generateCSVField('');               # 134
                $csv .= self::generateCSVField('');               # 135
                if ($portal->getAfficherNumerotraking()) {
                    $csv .= self::generateCSVField($isSiteEnseigne ? $ad->getId() : $agency->getTelephone());    # 136
                    $csv .= self::generateCSVField($isSiteEnseigne ? $ad->getOwner()->getEmail() : '');          # 137
                } else {
                    $csv .= self::generateCSVField($isSiteEnseigne ? $ad->getId() : '');               # 136
                    $csv .= self::generateCSVField($isSiteEnseigne ? $ad->getOwner()->getEmail() : '');               # 137
                }
                if ($isSiteEnseigne) {
                    $nameSplitList = explode(' ', trim($ad->getOwner()->getName()));
                    $csv .= self::generateCSVField($nameSplitList && count($nameSplitList) > 0 ?
                        $nameSplitList[0] : 'getChampPersonnalise3'
                    );               # 138
                    $csv .= self::generateCSVField($nameSplitList && count($nameSplitList) > 1 ?
                        $nameSplitList[1] : 'getChampPersonnalise4');               # 139
                }else {
                    $csv .= self::generateCSVField('getChampPersonnalise3');               # 138
                    $csv .= self::generateCSVField('getChampPersonnalise4');               # 139
                }
                $csv .= self::toCSVField($ad, 'getChampPersonnalise5');               # 140
                $csv .= self::toCSVField($ad, 'getChampPersonnalise6');               # 141
                $csv .= self::toCSVField($ad, 'getChampPersonnalise7');               # 142
                $csv .= self::toCSVField($ad, 'getChampPersonnalise8');               # 143
                $csv .= self::toCSVField($ad, 'getChampPersonnalise9');               # 144
                $csv .= self::toCSVField($ad, 'getChampPersonnalise10');               # 145
                $csv .= self::toCSVField($ad, 'getChampPersonnalise11');               # 146
                $csv .= self::toCSVField($ad, 'getChampPersonnalise12');               # 147
                $csv .= self::toCSVField($ad, 'getChampPersonnalise13');               # 148
                $csv .= self::toCSVField($ad, 'getChampPersonnalise14');               # 149
                $csv .= self::toCSVField($ad, 'getChampPersonnalise15');               # 150
                $csv .= self::toCSVField($ad, 'getChampPersonnalise16');               # 151
                $csv .= self::toCSVField($ad, 'getChampPersonnalise17');               # 152
                $csv .= self::toCSVField($ad, 'getChampPersonnalise18');               # 153
                $csv .= self::generateCSVField($isEvrovilla ?
                    '' : 'getChampPersonnalise19');                                    # 154
                $csv .= self::toCSVField($ad, 'getChampPersonnalise20');               # 155
                $csv .= self::toCSVField($ad, 'getChampPersonnalise21');               # 156
                $csv .= self::toCSVField($ad, 'getChampPersonnalise22');               # 157
                $csv .= self::generateCSVField($isEvrovilla ?
                    '' : 'getTitrePhoto23');                                           # 158
                $csv .= self::generateCSVField($isEvrovilla ?
                    '' : 'getTitrePhoto24');                                           # 159
                $csv .= self::generateCSVField($isEvrovilla ?
                    date_format($ad->getUpdatedAt(), 'Y-m-d H:i:s'): 'getChampPersonnalise25'); # 160
                $csv .= self::toCSVField($ad, 'getDepotGarantie');                     # 161
                $csv .= self::generateCSVField('');                       # 162
                $csv .= self::generateCSVField('');                        # 163
                $csv .= self::toCSVField($ad, 'getPhoto10');               # 164
                $csv .= self::toCSVField($ad, 'getPhoto11');               # 165
                $csv .= self::toCSVField($ad, 'getPhoto12');               # 166
                $csv .= self::toCSVField($ad, 'getPhoto13');               # 167
                $csv .= self::toCSVField($ad, 'getPhoto14');               # 168
                $csv .= self::toCSVField($ad, 'getPhoto15');               # 169
                $csv .= self::toCSVField($ad, 'getPhoto16');               # 170
                $csv .= self::toCSVField($ad, 'getPhoto17');               # 171
                $csv .= self::toCSVField($ad, 'getPhoto18');               # 172
                $csv .= self::toCSVField($ad, 'getPhoto19');               # 173
                $csv .= self::toCSVField($ad, 'getPhoto20');               # 174
                $csv .= self::generateCSVField($ad->getId().'-'.($isTerrain ? $Terrain->getId() : $Maison->getId()),'number');               # 175
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getDpeValue());               # 176
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getDPE(),'select');               # 177
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getGesValue());                                     # 178
                $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getGES(),'select');               # 179
                $csv .= self::generateCSVField('');               # 180
                $csv .= self::generateCSVField(!$isTerrain ? '' : '');                     # 181
                $csv .= self::toCSVField($ad, 'getPeriodesDisponibilite');            # 182
                $csv .= self::toCSVField($ad, 'getPeriodesBasseSaison');               # 183
                $csv .= self::toCSVField($ad, 'getPeriodesHauteSaison');               # 184
                $csv .= self::toCSVField($ad, 'getPrixBouquet');               # 185
                $csv .= self::toCSVField($ad, 'getRenteMensuelle');               # 186
                $csv .= self::toCSVField($ad, 'getAgeHomme');               # 187
                $csv .= self::toCSVField($ad, 'getAgeFemme');               # 188
                $csv .= self::toCSVField($ad, 'isSiEntree');               # 189
                $csv .= self::toCSVField($ad, 'isResidence');               # 190
                $csv .= self::toCSVField($ad, 'isSiParquet');               # 191
                $csv .= self::toCSVField($ad, 'isSiVisAVis');               # 192
                $csv .= self::toCSVField($ad, 'getLigneTransport');               # 193
                $csv .= self::toCSVField($ad, 'getStationTransport');               # 194
                $csv .= self::toCSVField($ad, 'getDureeBail');               # 195
                $csv .= self::toCSVField($ad, 'getNbPlacesEnSalle');               # 196
                $csv .= self::toCSVField($ad, 'isSiMonteCharge');               # 197
                $csv .= self::toCSVField($ad, 'getSiQuai');               # 198
                $csv .= self::toCSVField($ad, 'getNbBureaux');               # 199
                $csv .= self::toCSVField($ad, 'getPrixDroitEntree');               # 200
                $csv .= self::toCSVField($ad, 'isPrixMasque');               # 201
                $csv .= self::toCSVField($ad, 'getLoyerAnnuelGlobal');               # 202
                $csv .= self::toCSVField($ad, 'getChargesAnnuellesGlobales');               # 203
                $csv .= self::toCSVField($ad, 'getLoyerAnnuelAuMCarre');               # 204
                $csv .= self::toCSVField($ad, 'getChargesAnnuellesAuMCarre');               # 205
                $csv .= self::toCSVField($ad, 'isChargesMensuellesHT');               # 206
                $csv .= self::toCSVField($ad, 'isSiLoyerAnnuelCC');               # 207
                $csv .= self::toCSVField($ad, 'isSiLoyerAnnuelHT');               # 208
                $csv .= self::toCSVField($ad, 'isSiChargesAnnuellesGlobalesHT');               # 209
                $csv .= self::toCSVField($ad, 'isSiLoyerAnnuelAuMCarreCC');               # 210
                $csv .= self::toCSVField($ad, 'isSiLoyerAnnuelAuMCarreHT');               # 211
                $csv .= self::toCSVField($ad, 'isSichargesAnnuellesAuMCarreHT');               # 212
                $csv .= self::toCSVField($ad, 'isSiDivisible');               # 213
                $csv .= self::toCSVField($ad, 'getSurfaceDivisibleMin');               # 214
                $csv .= self::toCSVField($ad, 'getSurfaceDivisibleMax');               # 215
                $csv .= self::toCSVField($ad, 'getSurfaceSejour');               # 216
                $csv .= self::toCSVField($ad, 'getNbVehicules');               # 217
                $csv .= self::toCSVField($ad, 'getPrixDroitAuBail');               # 218
                $csv .= self::toCSVField($ad, 'getValeurAchat');               # 219
                $csv .= self::toCSVField($ad, 'getRepartitionCA');               # 220
                $csv .= self::generateCSVField(!$isTerrain ? '' : ($Terrain->getType() == 'terrain_agricole' ? 'OUI': 'NON'),'boolean');
                $csv .= self::toCSVField($ad, 'isSiEquipementBebe');               # 222
                $csv .= self::generateCSVField(!$isTerrain ? '' : ($Terrain->getType() == 'terrain_a_batir' ? 'OUI': 'NON'),'boolean');
                $csv .= self::toCSVField($ad, 'getResultatN2');               # 224
                $csv .= self::toCSVField($ad, 'getResultatN1');               # 225
                $csv .= self::toCSVField($ad, 'getResultatN');               # 226
                $csv .= self::toCSVField($ad, 'isSiDansImmeubleParking');               # 227
                $csv .= self::toCSVField($ad, 'isSiParkingIsole');               # 228
                $csv .= self::toCSVField($ad, 'isVenduLibre');               # 229
                $csv .= self::toCSVField($ad, 'isADisposition');               # 230
                $csv .= self::generateCSVField('');
                $csv .= self::generateCSVField('');
                $csv .= self::toCSVField($ad, 'isSiLaveLinge');               # 233
                $csv .= self::toCSVField($ad, 'isSecheLinge');               # 234
                $csv .= self::toCSVField($ad, 'isSiConnexionInternet');               # 235
                $csv .= self::toCSVField($ad, 'getChiffreAffaireN2');               # 236
                $csv .= self::toCSVField($ad, 'getChiffreAffaireN1');               # 237
                $csv .= self::toCSVField($ad, 'getConditionsFinancieres');               # 238
                $csv .= self::toCSVField($ad, 'getPrestationsDiverses');               # 239
                $csv .= self::toCSVField($ad, '');               # 240
                $csv .= self::toCSVField($ad, 'getLongueurFacade');               # 241
                $csv .= self::toCSVField($ad, 'getMontantRapport');               # 242
                $csv .= self::toCSVField($ad, 'getNatureBail');               # 243
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiTerrasseValue() ? $Maison->getNbTerrasse() : '','number');               # 244
                $csv .= self::toCSVField($ad, 'isSiPrixHT');               # 245
                $csv .= self::generateCSVField($isTerrain  ? '' : ($Maison->getSiSalleAMangerValue() ? 'OUI' : 'NON'),'boolean');                # 246
                $csv .= self::generateCSVField($isTerrain  ? '' : ($Maison->getSiSejourValue() ? 'OUI' : 'NON'),'boolean');                # 247
                $csv .= self::generateCSVField('');
                $csv .= self::toCSVField($ad, 'isSiImmeubleBureaux');               # 249
                $csv .= self::generateCSVField('');
                $csv .= self::toCSVField($ad, 'isSiEquipementVideo');               # 251
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiCaveValue() ? $Maison->getSurfaceCave() : '','float');               # 252
                $csv .= self::generateCSVField(!$isTerrain && $Maison->getSiSalleAMangerValue() ? $Maison->getSurfaceSalleAManger() : '','float');               # 252
                $csv .= self::toCSVField($ad, 'getSituationCommerciale');               # 254
                $csv .= self::toCSVField($ad, 'getSurfaceMaxBureau');               # 255
                $csv .= self::generateCSVField($isEvrovilla ?
                    '' : 'getTitrePhoto39');              # 256
                $csv .= self::generateCSVField($isEvrovilla ?
                    '' : 'getTitrePhoto39');              # 257
                $csv .= self::toCSVField($ad, 'isSiCopropriete');               # 258
                $csv .= self::toCSVField($ad, 'getNbLotsCopropriete');               # 259
                $csv .= self::toCSVField($ad, 'getChargesAnnuellesGlobales');               # 260
                $csv .= self::toCSVField($ad, 'isSiProcedureSyndicat');               # 261
                $csv .= self::toCSVField($ad, 'getDetailsProcedureSyndicat');               # 262
                $csv .= self::toCSVField($ad, 'getChampPersonnalise26');               # 263
                $csv .= self::toCSVField($ad, 'getPhoto21');               # 264
                $csv .= self::toCSVField($ad, 'getPhoto22');               # 265
                $csv .= self::toCSVField($ad, 'getPhoto23');               # 266
                $csv .= self::toCSVField($ad, 'getPhoto24');               # 267
                $csv .= self::toCSVField($ad, 'getPhoto25');               # 268
                $csv .= self::toCSVField($ad, 'getPhoto26');               # 269
                $csv .= self::toCSVField($ad, 'getPhoto27');               # 270
                $csv .= self::toCSVField($ad, 'getPhoto28');               # 271
                $csv .= self::toCSVField($ad, 'getPhoto29');               # 272
                $csv .= self::toCSVField($ad, 'getPhoto30');               # 273
                $csv .= self::toCSVField($ad, 'getTitrePhoto10');               # 274
                $csv .= self::toCSVField($ad, 'getTitrePhoto11');               # 275
                $csv .= self::toCSVField($ad, 'getTitrePhoto12');               # 276
                $csv .= self::toCSVField($ad, 'getTitrePhoto13');               # 278
                $csv .= self::toCSVField($ad, 'getTitrePhoto14');               # 279
                $csv .= self::toCSVField($ad, 'getTitrePhoto15');               # 280
                $csv .= self::toCSVField($ad, 'getTitrePhoto16');               # 281
                $csv .= self::toCSVField($ad, 'getTitrePhoto17');               # 282
                $csv .= self::toCSVField($ad, 'getTitrePhoto18');               # 283
                $csv .= self::toCSVField($ad, 'getTitrePhoto19');               # 284
                $csv .= self::toCSVField($ad, 'getTitrePhoto20');               # 285
                $csv .= self::toCSVField($ad, 'getTitrePhoto21');               # 286
                $csv .= self::toCSVField($ad, 'getTitrePhoto22');               # 287
                $csv .= self::toCSVField($ad, 'getTitrePhoto23');               # 288
                $csv .= self::toCSVField($ad, 'getTitrePhoto24');               # 289
                $csv .= self::toCSVField($ad, 'getTitrePhoto25');               # 290
                $csv .= self::toCSVField($ad, 'getTitrePhoto26');               # 291
                $csv .= self::toCSVField($ad, 'getTitrePhoto27');               # 292
                $csv .= self::toCSVField($ad, 'getTitrePhoto28');               # 293
                $csv .= self::toCSVField($ad, 'getTitrePhoto29');               # 294
                if($isEvrovilla) {
                    $csv .= self::generateCSVField($isTerrain ?
                        $Terrain->getPrix() : '');                   # 295
                    $csv .= self::generateCSVField($Maison  ?
                        $ad->getPrix() : '');                        # 296
                    $csv .= self::generateCSVField( $agency->getName() );                    # 297
                    $csv .= self::generateCSVField('');                            # 298
                    $csv .= self::generateCSVField('' );                           # 299
                    $csv .= self::generateCSVField('' );                       # 300
                    $csv .= self::generateCSVField('4.08-006');                            # 301
                    $csv .= self::generateCSVField('');                                               # 302
                    $csv .= self::generateCSVField( $ad->getPrix());                            # 303
                    $csv .= self::generateCSVField('');         # 304
                    $csv .= self::generateCSVField('');      # 305
                    $csv .= self::generateCSVField('');      # 306
                }
                $csv .= $newLine;
            }
        }
        return $convert ? mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8') : $csv;
    }

    public function toHouseModelPolirisCSV($houseModels,$portalRegistration)
    {
        $csv = '';
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        /** @var \AppBundle\Entity\HouseModel $houseModel */
        foreach ($houseModels as $houseModel) {
            $login  = $portalRegistration->getIdentifier();
            if(!$login || !$portal)
                continue;

            $newLine = "\r\n";
            if (!$houseModel instanceof HouseModel)
                continue;

            $portalName = $portal->getName();
            $agency = $houseModel->getGroup()->getAgency();
            $group      = $houseModel->getGroup();
            $Maison = $houseModel->getMaison();

            $csv .= self::generateCSVField($login);                                                          # 1 //todo
            $csv .= self::generateCSVField($houseModel->getRemoteId());        # 2
            $csv .= self::generateCSVField($houseModel->getName());                                            # 3
            $csv .= self::generateCSVField(utf8_decode($Maison->getType()));        # 4
            $csv .= self::generateCSVField($Maison->getTypeArchitectureValue());                                        # 5
            $csv .= self::generateCSVField($Maison->getTypeCharpante());                                             # 6
            $csv .= self::generateCSVField($Maison->getTypeTuile());/*$ad->getPays()*/                                  # 7
            $csv .= self::generateCSVField($Maison->getMateriauxTuile());                                           # 8
            $csv .= self::generateCSVField($Maison->getZinguerie());                                                          # 9 //todo use xml flux to set quartier value
            $csv .= self::generateCSVField($Maison->getMenuiserie());                                                          # 10
            $csv .= self::generateCSVField($Maison->getVolets()); # 11
            $csv .= self::generateCSVField($Maison->getPorteGarage());                                                          # 12
            $csv .= self::generateCSVField($Maison?$Maison->getSurface():'','float');                                                          # 13
            $csv .= self::generateCSVField($Maison->getDescription());                                                          # 14
            $csv .= self::generateCSVField($Maison->getTitreImagePrincipale());                                                          # 15
            $csv .= self::generateCSVField($Maison->getImagePrincipale() ? $this->liipImagine->getBrowserPath($Maison->getImagePrincipale(), 'poliris_image') : '');               # 85
            $csv .= self::generateCSVField($Maison->getNbPieces(),'number'); # 18
            $csv .= self::generateCSVField($Maison->getNbChambres(),'number');                   # 19
            $csv .= self::generateCSVField($Maison->getNbSalleBain(),'number');                 # 29
            $csv .= self::generateCSVField($Maison->getNbSalleEau(),'number');                  # 30
            $csv .= self::generateCSVField($Maison->getSiToiletteValue() ? $Maison->getNbToilette() : '','number');                  # 31
            $csv .= self::generateCSVField($Maison->getSiSalleAMangerValue() ? $Maison->getSurfaceSalleAManger() : '','float');               # 252
            $csv .= self::generateCSVField($Maison->getSiChemineeValue() ? $Maison->getNumberCheminee() : '','number');               # 252
            $csv .= self::generateCSVField($Maison->getTypeCuisine(),'number');                 # 34
            $csv .= self::generateCSVField($Maison->getRevetementDeSolValue());                 # 34
            $csv .= self::generateCSVField($Maison->getTypeDeChauffageValue());                 # 34
            $csv .= self::generateCSVField($Maison->getEnergieDeChauffage());                 # 34
            $csv .= self::generateCSVField($Maison->getSiBureauValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiBureauValue() ? $Maison->getSurfaceBureau() : "");                    # 17
            $csv .= self::generateCSVField($Maison->getSiPlacardsValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiPlacardsValue() ? $Maison->getNumberPlacards() : "",'number');                    # 17
            $csv .= self::generateCSVField($Maison->getSiDressingValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiPlacardsValue() ? $Maison->getSurfaceDressing() : "",'float');                    # 17
            $csv .= self::generateCSVField($Maison->getSiBuanderieValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiBuanderieValue() ? $Maison->getSurfaceBuanderie() : "",'float');                    # 17
            $csv .= self::generateCSVField($Maison->getSiAlarme() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiTV() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiPMR() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiParkingValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiParkingValue() ? $Maison->getNbParking() : "",'number');                    # 17
            $csv .= self::generateCSVField($Maison->getSiParkingValue() ? $Maison->getSurfaceParking() : "",'float');
            $csv .= self::generateCSVField($Maison->getSiTerrasseValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiTerrasseValue() ? $Maison->getNbTerrasse() : "",'number');
            $csv .= self::generateCSVField($Maison->getSiBalconsValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiBalconsValue() ? $Maison->getSurfaceBalcons() : "",'float');
            $csv .= self::generateCSVField($Maison->getSiVerandaValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiVerandaValue() ? $Maison->getSurfaceVeranda() : "",'float');
            $csv .= self::generateCSVField($Maison->getSiCaveValue() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getSiCaveValue() ? $Maison->getSurfaceCave() : "",'float');
            $csv .= self::generateCSVField($Maison->getSiPiscine() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getDpeValue());               # 176
            $csv .= self::generateCSVField($Maison->getDPE(),'select');               # 177
            $csv .= self::generateCSVField($Maison->getGesValue());                                     # 178
            $csv .= self::generateCSVField($Maison->getGES(),'select');               # 179
            $csv .= self::generateCSVField($Maison->getHauteQualiteEnvironnementale() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getNfLogement() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getLabelHabitatEnvironnement() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getTHPE() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getHPEENR() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getLabelBatimentBasseConsommation() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getLabelHabitatEnvironnementEffinergie() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getPlanLocalHabitat() ? "OUI" : "NON");                    # 17
            $csv .= self::generateCSVField($Maison->getRT2012() ? "OUI" : "NON");                    # 17

            for($i=0; $i <= 10; $i++) {
                if($houseModel->getImagesDocuments() && isset($houseModel->getImagesDocuments()[$i])) {
                    $csv .= self::generateCSVField($houseModel->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$houseModel->getImagesDocuments()[$i]->getName(), 'poliris_image') : '');
                } else {
                    $csv .= self::generateCSVField('');
                }
            }
            # 94 -> 102
            for($i=0; $i <= 10; $i++) {
                if($houseModel->getImagesDocuments() && isset($houseModel->getImagesDocuments()[$i])) {
                    $csv .= self::generateCSVField($houseModel->getImagesDocuments()[$i]->getTitle());
                } else {
                    $csv .= self::generateCSVField('');
                }
            }

            for($i=0; $i <= 10; $i++) {
                if($houseModel->getPdfDocuments() && isset($houseModel->getPdfDocuments()[$i])) {
                    $csv .= self::generateCSVField($this->container->get('templating.helper.assets')->getUrl('/uploads/housemodels/'.$houseModel->getPdfDocuments()[0]->getName(),null));
                } else {
                    $csv .= self::generateCSVField('');
                }
            }
            $csv .= $newLine;
        }
        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    public function toPolirisProgramsCSV($programs,$portalRegistration)
    {
        $csv = '';
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        /** @var \AppBundle\Entity\Program $program */
        foreach ($programs as $program) {
            if($program->getPublished() && $program->getDiffusion() && $portal->existIn($program->getDiffusion()->getBroadcastPortals())) {
                $login  = $portalRegistration->getIdentifier();
                if(!$login || !$portal)
                    continue;

                $newLine = "\r\n";
                if (!$program instanceof Program)
                    continue;

                $containMaison      = $program->getType() == 'maison';
                $containTerrrain    = $program->getType() == 'terrain';
                $containAppartement = $program->getType() == 'appartement';
                $nbMinPieces        = '';
                $nbMaxPieces        = '';
                $surfaceMinTerrain  = '';
                $surfaceMaxTerrain  = '';


                /** @var \AppBundle\Entity\Ad $ad */
                foreach ($program->getAds() as $ad) {
                    $bien = $ad->getBienImmobilier()[0];
                    if($program->getType() == 'mixte') {
                        if(!$containMaison && $bien instanceof Maison){
                            $containMaison = true;
                        } elseif(!$containTerrrain && $bien instanceof Terrain){
                            $containTerrrain = true;
                        } elseif(!$containAppartement && $bien instanceof Appartement){
                            $containAppartement = true;
                        }
                    }
                    if($program->getType() != 'terrain') {
                        if($bien->getNbPieces() < $nbMinPieces || $nbMinPieces == '') {
                            $nbMinPieces = $bien->getNbPieces();
                        }
                        if($bien->getNbPieces() > $nbMaxPieces || $nbMaxPieces == '') {
                            $nbMaxPieces = $bien->getNbPieces();
                        }
                    } else if($bien) {
                        if($bien->getSurface() < $surfaceMinTerrain || $surfaceMinTerrain == '') {
                            $surfaceMinTerrain = $bien->getSurface();
                        }
                        if($bien->getSurface() > $surfaceMaxTerrain || $surfaceMaxTerrain == '') {
                            $surfaceMaxTerrain = $bien->getSurface();
                        }
                    }

                }

                $labels               = [];
                if($program->getHauteQualiteEnvironnementale())         $labels[]  =  'Label HQE';
                if($program->getNfLogement())                           $labels[]  =  'NF Logement';
                if($program->getLabelHabitatEnvironnement())            $labels[]  =  'Habitat et Environnement';
                if($program->getTHPE())                                 $labels[]  =  'THPE ENR';
                if($program->getHPEENR())                               $labels[]  =  'HPE 3 étoiles';
                if($program->getLabelBatimentBasseConsommation())       $labels[]  =  'BBC';
                if($program->getLabelHabitatEnvironnementEffinergie())  $labels[]  =  'EFFINERGIE';
                //if($program->getPlanLocalHabitat())                     $labels[]  =  'Label';
                if($program->getRT2012())                               $labels[]  =  'RT 2012';

                $typeInvestissement   = [];
                if($program->getLoiDuflot())                 $typeInvestissement[]  =  'Loi duflo';
                if($program->getLMPLMNP())                   $typeInvestissement[]  =  'LMP ;LMNP';
                if($program->getTva1())                      $typeInvestissement[]  =  'TVA 5,5%';
                if($program->getTva2())                      $typeInvestissement[]  =  'TVA 7%';
                if($program->getCensiBouvard())              $typeInvestissement[]  =  'Loi Censi-Bouvard';
                if($program->getPretLocatifSocial())         $typeInvestissement[]  =  'PLS Investisseur';
                if($program->getNuePropriete())              $typeInvestissement[]  =  'Démembrement/Nue propriété';


                $group      = $program->getGroup();
                $agency     = $program->getGroup()->getAgency();

                $csv .= self::generateCSVField($login);                                                          # 1
                $csv .= self::generateCSVField($program->getReference());                                        # 2
                $csv .= self::generateCSVField($program->getCodePostal(),'number');                              # 3
                $csv .= self::generateCSVField($program->getVille());                                            # 4
                $csv .= self::generateCSVField('France');/*$ad->getPays()*/                                      # 5
                $csv .= self::generateCSVField($program->getAdresse());                                          # 6
                $csv .= self::generateCSVField('');                                                              # 7
                $csv .= self::generateCSVField($program->getTitle());                                            # 8
                $csv .= self::generateCSVField($program->getDescription());                                      # 9
                $csv .= self::generateCSVField('');                                                              # 10
                $csv .= self::toCSVField($program, 'getDateLivraison');                                         # 11
                $csv .= self::generateCSVField($containMaison ? 'OUI' : 'NON','boolean');                        # 12
                $csv .= self::generateCSVField($containAppartement ? 'OUI' : 'NON','boolean');                   # 13
                $csv .= self::generateCSVField($containTerrrain ? 'OUI' : 'NON','boolean');                      # 14
                $csv .= self::generateCSVField($nbMinPieces,'number');                                           # 15
                $csv .= self::generateCSVField($nbMaxPieces,'number');                                           # 16
                $csv .= self::generateCSVField($containAppartement ? $program->getNbetages() : '','number');     # 17
                $csv .= self::generateCSVField('');                                                              # 18
                $csv .= self::generateCSVField('');                                                              # 19
                $csv .= self::generateCSVField($surfaceMinTerrain, 'float');                                     # 20
                $csv .= self::generateCSVField($surfaceMaxTerrain, 'float');                                     # 21
                $csv .= self::generateCSVField('');                                                              # 22
                $csv .= self::generateCSVField('');                                                              # 23
                $csv .= self::generateCSVField('');                                                              # 24
                $csv .= self::generateCSVField('');                                                              # 25
                $csv .= self::generateCSVField('');                                                              # 26
                $csv .= self::generateCSVField('');                                                              # 27
                $csv .= self::generateCSVField('');                                                              # 28
                $csv .= self::generateCSVField('');                                                              # 29
                $csv .= self::generateCSVField('');                                                              # 30
                $csv .= self::generateCSVField('');                                                              # 31
                $csv .= self::generateCSVField('');                                                              # 32
                $csv .= self::generateCSVField('');                                                              # 33
                $csv .= self::generateCSVField(0,'number');                                                      # 34
                $csv .= self::generateCSVField(0,'number');                                                      # 35
                $csv .= self::generateCSVField(0,'number');                                                      # 36
                $csv .= self::generateCSVField(0,'number');                                                      # 37
                $csv .= self::generateCSVField(0,'number');                                                      # 38
                $csv .= self::generateCSVField(0,'number');                                                      # 39
                $csv .= self::generateCSVField(0,'number');                                                      # 40
                $csv .= self::generateCSVField(0,'number');                                                      # 41
                $csv .= self::generateCSVField(0,'number');                                                      # 42
                $csv .= self::generateCSVField(0,'number');                                                      # 43
                $csv .= self::generateCSVField(0,'number');                                                      # 44
                $csv .= self::generateCSVField(0,'number');                                                      # 45
                $csv .= self::generateCSVField('');                                                              # 46
                $csv .= self::generateCSVField('');                                                              # 47
                $csv .= self::generateCSVField('');                                                              # 48
                $csv .= self::generateCSVField('');                                                              # 49
                $csv .= self::generateCSVField('');                                                              # 50
                $csv .= self::generateCSVField('');                                                              # 51
                $csv .= self::generateCSVField('');                                                              # 52
                $csv .= self::generateCSVField('');                                                              # 53
                $csv .= self::generateCSVField('');                                                              # 54
                $csv .= self::generateCSVField('');                                                              # 55
                $csv .= self::generateCSVField('');                                                              # 56
                $csv .= self::generateCSVField('');                                                              # 57
                $csv .= self::generateCSVField('');                                                              # 58
                $csv .= self::generateCSVField('');                                                              # 59
                $csv .= self::generateCSVField('');                                                              # 60
                $csv .= self::generateCSVField('');                                                              # 61
                $csv .= self::generateCSVField('');                                                              # 62
                $csv .= self::generateCSVField('');                                                              # 63
                $csv .= self::generateCSVField('');                                                              # 64
                $csv .= self::generateCSVField('');                                                              # 65
                $csv .= self::generateCSVField('');                                                              # 66
                $csv .= self::generateCSVField('');                                                              # 67
                $csv .= self::generateCSVField('');                                                              # 68
                $csv .= self::generateCSVField('');                                                              # 69
                $csv .= self::generateCSVField('');                                                              # 70
                $csv .= self::generateCSVField('');                                                              # 71
                $csv .= self::generateCSVField('');                                                              # 72
                $csv .= self::generateCSVField('');                                                              # 73
                $csv .= self::generateCSVField('');                                                              # 74
                $csv .= self::generateCSVField('');                                                              # 75
                $csv .= self::generateCSVField('');                                                              # 76
                $csv .= self::generateCSVField('');                                                              # 77
                $csv .= self::generateCSVField('');                                                              # 78
                $csv .= self::generateCSVField('');                                                              # 79
                $csv .= self::generateCSVField('');                                                              # 80
                $csv .= self::generateCSVField('');                                                              # 81
                $csv .= self::generateCSVField('');                                                              # 82
                $csv .= self::generateCSVField('');                                                              # 83
                $csv .= self::generateCSVField('');                                                              # 84
                $csv .= self::generateCSVField('');                                                              # 85
                $csv .= self::generateCSVField('');                                                              # 86
                $csv .= self::generateCSVField('');                                                              # 87
                $csv .= self::generateCSVField('');                                                              # 88
                $csv .= self::generateCSVField($program->getSiTV() ? 'OUI' : 'NON','boolean');                   # 89
                $csv .= self::generateCSVField('');                                                              # 90
                $csv .= self::generateCSVField('');                                                              # 91
                $csv .= self::generateCSVField('');                                                              # 92
                $csv .= self::generateCSVField('');                                                              # 93
                $csv .= self::generateCSVField('');                                                              # 94
                $csv .= self::generateCSVField('');                                                              # 95
                $csv .= self::generateCSVField('');                                                              # 96
                $csv .= self::generateCSVField('');                                                              # 97
                $csv .= self::generateCSVField('');                                                              # 98
                $csv .= self::generateCSVField('');                                                              # 99
                $csv .= self::generateCSVField('');                                                              # 100
                $csv .= self::generateCSVField('');                                                              # 101
                $csv .= self::generateCSVField('');                                                              # 102
                $csv .= self::generateCSVField('');                                                              # 103
                $csv .= self::generateCSVField('');                                                              # 104
                $csv .= self::generateCSVField('');                                                              # 105
                $csv .= self::generateCSVField('');                                                              # 106
                $csv .= self::generateCSVField('');                                                              # 107
                $csv .= self::generateCSVField('');                                                              # 108
                $csv .= self::generateCSVField('');                                                              # 109
                $csv .= self::generateCSVField('');                                                              # 110
                $csv .= self::generateCSVField('');                                                              # 111
                $csv .= self::generateCSVField('');                                                              # 112
                $csv .= self::generateCSVField('');                                                              # 113
                $csv .= self::generateCSVField($program->getImagePrincipale() ? $this->liipImagine->getBrowserPath($program->getImagePrincipale(), 'poliris_image') : '');# 114
                # 115 -> 133
                for($i=0; $i < 19; $i++) {
                    if($program->getImagesDocuments() && isset($program->getImagesDocuments()[$i])) {
                        $csv .= self::generateCSVField($program->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagesDocuments()[$i]->getName(), 'poliris_image') : '');
                    } else {
                        $csv .= self::generateCSVField('');
                    }
                }
                $csv .= self::generateCSVField($agency->getTelephone(),'string');                                          # 134
                $csv .= self::generateCSVField($agency->getPrenom().' '.$agency->getNom());                       # 135
                $csv .= self::generateCSVField($group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());                                              # 136
                $csv .= self::generateCSVField($program->getDpeValue());                                          # 137
                $csv .= self::generateCSVField($program->getDpe(),'select');                                      # 138
                $csv .= self::generateCSVField($program->getGesValue());                                          # 139
                $csv .= self::generateCSVField($program->getGes(),'select');                                      # 140
                $csv .= self::generateCSVField('');                                                               # 141
                $csv .= self::generateCSVField('');                                                               # 142
                $csv .= self::generateCSVField('');                                                               # 143
                $csv .= self::generateCSVField($program->getNbAds());                                             # 144
                $i = 0;
                /** @var Ad $ad */
                foreach ($program->getAds() as $ad) {
                    if($ad->getStat() != Ad::VENDU && $ad->getStat() != Ad::RESERVER ) {
                        $i++;
                    }
                }
                $csv .= self::generateCSVField($program->getNbAds() - $i);                  # 145
                $csv .= self::generateCSVField('');                                                               # 146
                $csv .= self::generateCSVField($program->getTypeAvancement(),'select');                           # 147
                $csv .= self::generateCSVField('');                                                               # 148
                $csv .= self::generateCSVField(implode(' ;',$labels));                                            # 149
                $csv .= self::generateCSVField('');                                                               # 150
                $csv .= self::generateCSVField(implode(' ;',$typeInvestissement));                                # 151
                $csv .= self::generateCSVField('');                                                               # 152
                $csv .= self::generateCSVField('NON');                                                            # 153
                if($program->getPdfDocuments() && isset($program->getPdfDocuments()[0])) {
                    $csv .= self::generateCSVField($this->container->get('templating.helper.assets')->getUrl('/uploads/housemodels/'.$program->getPdfDocuments()[0]->getName(),null));
                } else {
                    $csv .= self::generateCSVField('');                                                           # 154
                }
                $csv .= self::generateCSVField($program->getRemoteId());                                          # 155
                $csv .= self::generateCSVField('');                                                               # 156
                $csv .= self::generateCSVField($program->getSiAscenseur() ? 'OUI' : 'NON','boolean');             # 157
                $csv .= self::generateCSVField($program->getSiInterphone() ? 'OUI' : 'NON','boolean');            # 158
                $csv .= self::generateCSVField($program->getSiDigicode() ? 'OUI' : 'NON','boolean');              # 159
                $csv .= self::generateCSVField($program->getSiGardien() ? 'OUI' : 'NON','boolean');               # 160
                $csv .= self::generateCSVField($program->getLienVisitVirtuel());                                  # 161
                $csv .= self::toCSVField($program, 'getDateDebutOffre');                                          # 162
                $csv .= self::toCSVField($program, 'getDateFinOffre');                                          # 163
                $csv .= self::generateCSVField($program->getDescriptionOffre());                                  # 164
                $csv .= self::generateCSVField($program->getMentionLegalOffre());                                 # 165

                $csv .= $newLine;
            }
        }
        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    public function toPolirisAdsProgramsCSV($programs,$portalRegistration)
    {
        $csv = '';
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        /** @var \AppBundle\Entity\Program $program */
        foreach ($programs as $program) {
            if(
                $program->getPublished() &&
                $program->getDiffusion()
            ) {
                $login  = $portalRegistration->getIdentifier();
                if(!$login || !$portal)
                    continue;

                $newLine = "\r\n";
                if (!$program instanceof Program)
                    continue;

                $containMaison      = $program->getType() == 'maison';
                $containTerrrain    = $program->getType() == 'terrain';
                $containAppartement = $program->getType() == 'appartement';
                $group              = $program->getGroup();
                $agency             = $program->getGroup()->getAgency();

                /** @var \AppBundle\Entity\Ad $ad */
                foreach ($program->getAds() as $ad) {
                    if ($portal->existIn($ad->getDiffusion()->getBroadcastPortals())) {
                        $bien = $ad->getBienImmobilier()[0];
                        $typeBien = null;
                        if ($bien instanceof Maison) {
                            $typeBien = 'maison';
                            $containMaison = true;
                        } elseif ($bien instanceof Terrain) {
                            $typeBien = 'terrain';
                            $containTerrrain = true;
                        } elseif ($bien instanceof Appartement) {
                            $typeBien = 'Appartement';
                            $containAppartement = true;
                        }
                        if (!is_null($typeBien)) {
                            $csv .= self::generateCSVField($login);                                                                             # 1
                            $csv .= self::generateCSVField($program->getReference());                                                                       # 2
                            $csv .= self::generateCSVField($ad->getReference());                                                            # 3
                            $csv .= self::generateCSVField($program->getCodePostal(),
                                'number');                                             # 4
                            $csv .= self::generateCSVField($program->getVille());                                                           # 5
                            $csv .= self::generateCSVField('France');/*$ad->getPays()*/                                                     # 6
                            $csv .= self::generateCSVField($program->getAdresse());                                                         # 7
                            $csv .= self::generateCSVField('');                                                                             # 8
                            $csv .= self::generateCSVField($ad->getLibelle());                                                           # 9
                            $csv .= self::generateCSVField($ad->getDescription());                                                     # 10
                            $csv .= self::generateCSVField('');                                                                             # 11
                            $csv .= self::toCSVField($program,
                                'getDateLivraison');                                                         # 12
                            $csv .= self::generateCSVField($typeBien);                                                                      # 13
                            $csv .= self::generateCSVField($ad->getPrix(),
                                'float');                                                      # 14
                            $csv .= self::generateCSVField('');                                                                             # 15
                            $csv .= self::generateCSVField($bien ? $bien->getSurface() : '',
                                'float');                                                    # 16
                            $csv .= self::generateCSVField('');                                                                             # 17
                            $csv .= self::generateCSVField($containAppartement ? $program->getNbetages() : '',
                                'number');                    # 18
                            $csv .= self::generateCSVField($containTerrrain ? '' : $bien->getNbChambres(),
                                'number');                                                # 19
                            $csv .= self::generateCSVField('');                                                                             # 20
                            $csv .= self::generateCSVField($program->getType() == 'appartement' ? $bien->getNbEtages() : '',
                                'number');                       # 21
                            $csv .= self::generateCSVField(!$containTerrrain ? $bien->getNbSalleBain() : '',
                                'number');                     # 22
                            $csv .= self::generateCSVField(!$containTerrrain ? $bien->getNbSalleBain() : '',
                                'number');                     # 23
                            $csv .= self::generateCSVField(!$containTerrrain ? $bien->getNbToilette() : '',
                                'number');                      # 24
                            $csv .= self::generateCSVField('');                                                                             # 25
                            $csv .= self::generateCSVField($program->getTypeDeChauffage());                                                 # 26
                            $csv .= self::generateCSVField($containTerrrain ? '' : $bien->getTypeCuisine());                                                        # 27
                            $csv .= self::generateCSVField($containTerrrain ? (strpos($bien->getExposition(),
                                'sud') !== false ? 'OUI' : 'NON') : '',
                                'boolean');                                                         # 28
                            $csv .= self::generateCSVField($containTerrrain ? (strpos($bien->getExposition(),
                                'est') !== false && strpos($bien->getExposition(),
                                'ouest') == false ? 'OUI' : 'NON') : '', 'boolean');     # 29
                            $csv .= self::generateCSVField($containTerrrain ? (strpos($bien->getExposition(),
                                'ouest') !== false ? 'OUI' : 'NON') : '',
                                'boolean');                                                       # 30
                            $csv .= self::generateCSVField($containTerrrain ? (strpos($bien->getExposition(),
                                'nord') !== false ? 'OUI' : 'NON') : '',
                                'boolean');                                                        # 31
                            $csv .= self::generateCSVField(!$containTerrrain ? $bien->getNbBalcons() : '',
                                'number');                       # 32
                            $csv .= self::generateCSVField(!$containTerrrain ? $bien->getSurfaceBalcons() : '',
                                'number');                  # 33
                            $csv .= self::generateCSVField(!$containTerrrain && $program->getSiAscenseur() ? 'OUI' : 'NON',
                                'boolean');      # 34
                            $csv .= self::generateCSVField(!$containTerrrain && $program->getSiCave() ? 'OUI' : 'NON',
                                'boolean');           # 35
                            $csv .= self::generateCSVField($containMaison && $bien->getSiParkingValue() ? $bien->getNbParking() : '',
                                'boolean');              # 36
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiBox() ? $bien->getNbBox() : '',
                                'boolean');      # 37
                            $csv .= self::generateCSVField($program->getSiInterphone() ? 'OUI' : 'NON',
                                'boolean');                          # 38
                            $csv .= self::generateCSVField($program->getSiDigicode() ? 'OUI' : 'NON',
                                'boolean');                            # 39
                            $csv .= self::generateCSVField($program->getSiGardien() ? 'OUI' : 'NON',
                                'boolean');                             # 40
                            $csv .= self::generateCSVField($program->getSiTerrasse() ? 'OUI' : 'NON',
                                'boolean');                            # 41
                            $csv .= self::generateCSVField($ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(),
                                'poliris_image') : ''); # 42
                            # 43 -> 62
                            for ($i = 0; $i < 19; $i++) {
                                if ($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                                    $csv .= self::generateCSVField($ad->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/' . $ad->getImagesDocuments()[$i]->getName(),
                                        'poliris_image') : '');
                                } else {
                                    $csv .= self::generateCSVField('');
                                }
                            }
                            $csv .= self::generateCSVField($agency->getTelephone());                                                        # 63
                            $csv .= self::generateCSVField($agency->getPrenom() . ' ' . $agency->getNom());                                     # 64
                            $csv .= self::generateCSVField($group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());                                                            # 65
                            $csv .= self::generateCSVField($containMaison ? $bien->getDpeValue() : '');                                                           # 66
                            $csv .= self::generateCSVField($containMaison ? $bien->getDpe() : '',
                                'select');                                                       # 67
                            $csv .= self::generateCSVField($containMaison ? $bien->getGesValue() : '');                                                           # 68
                            $csv .= self::generateCSVField($containMaison ? $bien->getGes() : '',
                                'select');                                                       # 69
                            $csv .= self::generateCSVField('');                                                                             # 70
                            $csv .= self::generateCSVField('');                                                                             # 71
                            $csv .= self::generateCSVField('');                                                                             # 72
                            $csv .= self::generateCSVField('');                                                                             # 73
                            if ($program->getPdfDocuments() && isset($program->getPdfDocuments()[0])) {
                                $csv .= self::generateCSVField($this->container->get('templating.helper.assets')->getUrl('/uploads/housemodels/' . $bien->getImagesDocuments()[0]->getName(),
                                    null));
                            } else {
                                $csv .= self::generateCSVField('');                                                                         # 74
                            }
                            $csv .= self::generateCSVField($ad->getId() . '-' . $bien->getId());                                                 # 75
                            $csv .= self::generateCSVField('');                                                                              # 76 todo: a prévoir
                            $csv .= self::generateCSVField('');                                                                              # 77 todo: a prévoir
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiTerrasseValue() ? $bien->getSurfaceTerrasse() : '',
                                'float');               # 78
                            $csv .= self::generateCSVField($containMaison && $bien->getSiJardin() ? 'OUI' : 'NON',
                                'boolean');                                  # 79
                            $csv .= self::generateCSVField('');                                                                              # 80
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiChemineeValue() ? 'OUI' : 'NON',
                                'boolean');      # 81
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiAlarme() ? 'OUI' : 'NON',
                                'boolean');             # 82
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiSejourValue() ? 'OUI' : 'NON',
                                'boolean');        # 83
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiSejourValue() ? $bien->getSurfaceSejour() : '',
                                'float');  # 84
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiSalleAMangerValue() ? 'OUI' : 'NON',
                                'boolean');           # 85
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiSejourValue() ? $bien->getSurfaceSejour() : '',
                                'float');  # 86
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiCaveValue() ? 'OUI' : 'NON',
                                'boolean');                   # 87
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiCaveValue() ? $bien->getSurfaceCave() : '',
                                'float');      # 88
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiPlacardsValue() ? 'OUI' : 'NON',
                                'boolean');               # 89
                            $csv .= self::generateCSVField('');                                                                                       # 90
                            $csv .= self::generateCSVField('');                                                                                       # 91
                            $csv .= self::generateCSVField('');                                                                                       # 92
                            $csv .= self::generateCSVField($ad->getLienVisitVirtuel());                                                             # 93
                            $csv .= self::generateCSVField(!$containTerrrain && $bien->getSiTerrasseValue() ? $bien->getNbTerrasse() : '',
                                'number');  # 94
                            $csv .= self::generateCSVField($program->getType() == 'appartement' ? ($bien->getSiDuplex() ? 'OUI' : 'NON') : '',
                                'boolean');              # 95

                            $csv .= $newLine;
                        }

                    }
                }
            }
        }
        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    /**
     * @param $programs
     * @param $portalRegistration
     * @return mixed|null
     */
    public function toXMLProgramImmoneuf($programs,$portalRegistration) {

        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><programmes></programmes>" );
        /** @var \AppBundle\Entity\Program $program */
        foreach ($programs as $program) {
            if(
                $program->getPublished() &&
                $program->getDiffusion() &&
                $portal->existIn($program->getDiffusion()->getBroadcastPortals())
            ) {
                $login  = $portalRegistration->getIdentifier();
                if(!$login || !$portal)
                    continue;

                if (!$program instanceof Program)
                    continue;

                $containMaison      = $program->getType() == 'maison';
                $containTerrrain    = $program->getType() == 'terrain';
                $containAppartement = $program->getType() == 'appartement';
                $prixMaisonMin            = [];
                $prixMaisonMax            = [];
                $surfaceMaisonMin         = [];
                $surfaceMaisonMax         = [];

                $prixAppartementMin            = [];
                $prixAppartementMax            = [];
                $surfaceAppartementMin         = [];
                $surfaceAppartementMax         = [];

                /** @var \AppBundle\Entity\Ad $ad */
                foreach ($program->getAds() as $ad) {
                    $bien = $ad->getBienImmobilier()[0];
                    if($program->getType() == 'mixte') {
                        if(!$containMaison && $bien instanceof Maison){
                            $containMaison = true;
                        } elseif(!$containTerrrain && $bien instanceof Terrain){
                            $containTerrrain = true;
                        } elseif(!$containAppartement && $bien instanceof Appartement){
                            $containAppartement = true;
                        }
                    }

                    if($program->getType() == 'maison' || ($program->getType() == 'mixte' && $bien instanceof Maison)) {
                        if(!isset($prixMaisonMin[$bien->getNbPieces()]) || $ad->getPrix() < $prixMaisonMin[$bien->getNbPieces()]) {
                            $prixMaisonMin[$bien->getNbPieces()] = $ad->getPrix();
                        }
                        if(!isset($prixMaisonMax[$bien->getNbPieces()]) || $ad->getPrix() > $prixMaisonMax[$bien->getNbPieces()]) {
                            $prixMaisonMax[$bien->getNbPieces()] = $ad->getPrix();
                        }
                        if($bien){
                            if(!isset($surfaceMaisonMin[$bien->getNbPieces()]) || $bien->getSurface() < $surfaceMaisonMin[$bien->getNbPieces()]) {
                                $surfaceMaisonMin[$bien->getNbPieces()] = $bien->getSurface();
                            }
                            if(!isset($surfaceMaisonMax[$bien->getNbPieces()]) || $bien->getSurface() > $surfaceMaisonMax[$bien->getNbPieces()]) {
                                $surfaceMaisonMax[$bien->getNbPieces()] = $bien->getSurface();
                            }
                        }
                    } elseif($program->getType() == 'appartement' || ($program->getType() == 'mixte' && $bien instanceof Appartement)) {
                        if(!isset($prixAppartementMin[$bien->getNbPieces()]) || $ad->getPrix() < $prixAppartementMin[$bien->getNbPieces()]) {
                            $prixAppartementMin[$bien->getNbPieces()] = $ad->getPrix();
                        }
                        if(!isset($prixAppartementMax[$bien->getNbPieces()]) || $ad->getPrix() > $prixAppartementMax[$bien->getNbPieces()]) {
                            $prixAppartementMax[$bien->getNbPieces()] = $ad->getPrix();
                        }
                        if($bien) {
                            if (!isset($surfaceAppartementMin[$bien->getNbPieces()]) || $bien->getSurface() < $surfaceAppartementMin[$bien->getNbPieces()]) {
                                $surfaceAppartementMin[$bien->getNbPieces()] = $bien->getSurface();
                            }
                            if (!isset($surfaceAppartementMax[$bien->getNbPieces()]) || $bien->getSurface() > $surfaceAppartementMax[$bien->getNbPieces()]) {
                                $surfaceAppartementMax[$bien->getNbPieces()] = $bien->getSurface();
                            }
                        }
                    }
                }

                $loi = '';
                if($program->getLoiDuflot())                 $loi  =  'DUFLOT';
                if($program->getLMPLMNP())                   $loi  =  'LMNP/LMP';

                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                $login  = $portalRegistration->getIdentifier();
                $group  = $program->getGroup();
                $agency = $program->getGroup()->getAgency();

                $itemNode = $rootNode->addChild('programme');
                $itemNode->addChild( 'identifiant_programme', $program->getReference() );
                $itemNode->addChild( 'programme_nom', $program->getTitle() );
                $itemNode->addChild( 'departement' );
                $itemNode->addChild( 'cp', $program->getCodePostal() );
                $itemNode->addChild( 'ville', $program->getVille() );
                $itemNode->addChild( 'programme_adresse', $program->getAdresse() );
                $itemNode->addChild( 'date_livraison', $program->getDateLivraison()->format('d-m-Y') );
                $photos = $itemNode->addChild( 'images' );
                $photos->addChild( 'photo', $program->getImagePrincipale() ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagePrincipale(), 'poliris_image') : '' );
                for($i=0; $i <= 7; $i++) {
                    if($program->getImagesDocuments() && isset($program->getImagesDocuments()[$i])) {
                        $photos->addChild( 'photo', $program->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagesDocuments()[$i]->getName(), 'poliris_image') : '' );
                    }
                }
                $itemNode->addChild( 'descriptif', $program->getDescription() );
                $itemNode->addChild( 'lien' );
                $itemNode->addChild( 'acces' );
                $itemNode->addChild( 'bureau_vente', $agency->getAdress() );
                $itemNode->addChild( 'promoteur_nom', $agency->getName() );
                $itemNode->addChild( 'promoteur_logo', $agency->getAvatar() ? $this->liipImagine->getBrowserPath($agency->getAvatar(), 'poliris_image') : '' );
                $itemNode->addChild( 'promoteur_detail1', $agency->getAdress() );
                $itemNode->addChild( 'promoteur_detail2', $agency->getTelephone() );
                $itemNode->addChild( 'informations_prix' );
                $itemNode->addChild( 'has_appartement', $containAppartement ? 1 : 0 );
                $itemNode->addChild( 'has_maison', $containMaison ? 1 : 0 );
                $tags = $itemNode->addChild( 'tags' );
                $tags->addChild( 'garage',$program->getSiGarage() ? 1 : 0 );
                $tags->addChild( 'jardin',$program->getSiJardin() ? 1 : 0 );
                $tags->addChild( 'balcon',$program->getSiBalcons() ? 1 : 0 );
                $tags->addChild( 'terrasse',$program->getSiTerrasse() ? 1 : 0 );

                $itemNode->addChild( 'email1', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
                $itemNode->addChild( 'email2');
                $itemNode->addChild( 'email3');
                $itemNode->addChild( 'email4');
                $itemNode->addChild( 'loi',$loi);
                $itemNode->addChild( 'tva_reduite',$program->getTva1() || $program->getTva2() ? 1 : 0);
                $itemNode->addChild( 'm3','-1');
                $itemNode->addChild( 'm4','-1');
                $itemNode->addChild( 'm5','-1');
                $itemNode->addChild( 'm6','-1');
                $itemNode->addChild( 'm7','-1');
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'prix_min_t'.$i,isset($prixMaisonMin[$i]) ? $prixMaisonMin[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'prix_max_t'.$i,isset($prixMaisonMax[$i]) ? $prixMaisonMax[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'surface_min_t'.$i,isset($surfaceMaisonMin[$i]) ? $surfaceMaisonMin[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'surface_max_t'.$i,isset($surfaceMaisonMax[$i]) ? $surfaceMaisonMax[$i] : '-1');
                }
                $itemNode->addChild( 't1','-1');
                $itemNode->addChild( 't2','-1');
                $itemNode->addChild( 't3','-1');
                $itemNode->addChild( 't4','-1');
                $itemNode->addChild( 't5','-1');
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'prix_min_m'.$i,isset($prixAppartementMin[$i]) ? $prixAppartementMin[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'prix_max_m'.$i,isset($prixAppartementMax[$i]) ? $prixAppartementMax[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'surface_min_m'.$i,isset($surfaceAppartementMin[$i]) ? $surfaceAppartementMin[$i] : '-1');
                }
                for($i=3; $i <= 7; $i++) {
                    $itemNode->addChild( 'surface_max_m'.$i,isset($surfaceAppartementMax[$i]) ? $surfaceAppartementMax[$i] : '-1');
                }

                foreach ($program->getAds() as $ad) {
                    $bien = $ad->getBienImmobilier()[0];
                    $lot = $itemNode->addChild( 'lot' );
                    $lot->addChild( 'identifiant_lot',$ad->getReference() );
                    $lot->addChild( 'categorie',$bien instanceof Maison ? 'maison' : ($bien instanceof Appartement ? 'appartement' : 'terrain') );
                    $lot->addChild( 'prix',$ad->getPrix() );
                    $lot->addChild( 'etage',$bien instanceof Appartement ? $bien->getNbEtages() : 0 );
                    $lot->addChild( 'surface',$bien instanceof Terrain ? $bien->getSurface() : $bien->getSurface() );
                    $lot->addChild( 'balcon',$bien->getSiBalconsValue() ? 1 : 0 );
                    $lot->addChild( 'terrasse',$bien->getSiTerrasseValue() ? 1 : 0 );
                    $lot->addChild( 'garage',$bien->getSiParkingValue() ? 1 : 0 );
                    $lot->addChild( 'jardin',$bien->getSiJardin() ? 1 : 0 );
                }
            }
        }
        if(count($rootNode->xpath("programme")))
            return $rootNode->asXML();
        return null;
    }

    public function toXMLProgram($programs,$portalRegistration) {

        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }

        $rootNode = new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><programmes></programmes>" );
        /** @var \AppBundle\Entity\Program $program */
        foreach ($programs as $program) {
            if($program->getPublished() && $program->getDiffusion() && $portal->existIn($program->getDiffusion()->getBroadcastPortals()) && $program->getType() != 'terrain') {
                $login  = $portalRegistration->getIdentifier();
                if(!$login || !$portal)
                    continue;

                if (!$program instanceof Program)
                    continue;

                $containMaison      = $program->getType() == 'maison';
                $containAppartement = $program->getType() == 'appartement';

                /** @var \AppBundle\Entity\Ad $ad */
                foreach ($program->getAds() as $ad) {
                    $bien = $ad->getBienImmobilier()[0];
                    if($program->getType() == 'mixte') {
                        if(!$containMaison && $bien instanceof Maison){
                            $containMaison = true;
                        } elseif(!$containAppartement && $bien instanceof Appartement){
                            $containAppartement = true;
                        }
                    }
                }

                $labels               = [];
                if($program->getHauteQualiteEnvironnementale())         $labels[]  =  'HQE';
                if($program->getNfLogement())                           $labels[]  =  'NF LOGEMENTS';
                if($program->getLabelHabitatEnvironnement())            $labels[]  =  'HABITAT ET ENVIRONNEMENT';
                if($program->getTHPE())                                 $labels[]  =  'THPE';
                if($program->getHPEENR())                               $labels[]  =  'HPE';
                if($program->getLabelBatimentBasseConsommation())       $labels[]  =  'BBC';
                if($program->getLabelHabitatEnvironnementEffinergie())  $labels[]  =  'EFFINERGIE';
                if($program->getRT2012())                               $labels[]  =  'RT-2012';

                $defiscalisation   = [];
                if($program->getLoiPinel())                  $defiscalisation[]   =  'Pinel';
                if($program->getLMPLMNP())                   $defiscalisation[]  =  'LMNP/LMP';
                if($program->getTva2())                      $defiscalisation[]  =  'TVA 7%';
                if($program->getCensiBouvard())              $defiscalisation[]  =  'Bouvard';
                if($program->getPretLocatifSocial())         $defiscalisation[]  =  'Prêt locatif social';
                if($program->getNuePropriete())              $defiscalisation[]  =  'Nue-propriété';
                if($program->getLoiMalraux())                $defiscalisation[]  =  'Malraux';
                if($program->getLoiGirardinPaul())           $defiscalisation[]  =  'Girardin';

                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                $login  = $portalRegistration->getIdentifier();
                $group = $program->getGroup();
                $agency = $program->getGroup()->getAgency();

                $itemNode = $rootNode->addChild('programme');
                $itemNode->addChild( 'contrat', $login );
                $itemNode->addChild( 'nom', $program->getTitle() );
                $itemNode->addChild( 'reference', $program->getReference() );
                $itemNode->addChild( 'phase-commerciale', $program->getTypeAvancement() );
                $itemNode->addChild( 'adresse', $program->getAdresse() );
                $itemNode->addChild( 'cp', $program->getCodePostal() );
                $itemNode->addChild( 'ville', $program->getVille() );
                $itemNode->addChild( 'pays', 'France' );
                $itemNode->addChild( 'secteur' );
                $itemNode->addChild( 'gps-latitude' );
                $itemNode->addChild( 'gps-longitude' );
                $itemNode->addChild( 'proximite','proximite' );
                $itemNode->addChild( 'prix-moyen-m2' );
                if($program->getPdfDocuments() && isset($program->getPdfDocuments()[0])) {
                    $itemNode->addChild( 'url-documentation',$this->container->get('templating.helper.assets')->getUrl('/uploads/housemodels/'.$program->getImagesDocuments()[0]->getName(),null) );
                } else {
                    $itemNode->addChild( 'url-documentation');                                                           # 154
                }
                $itemNode->addChild( 'url' );
                $itemNode->addChild( 'accroche-url' );
                $itemNode->addChild( 'nb-lots', $program->getNbAds() );
                $itemNode->addChild( 'descriptif-long', $program->getDescription() );
                $itemNode->addChild( 'descriptif-court' );
                $itemNode->addChild( 'date-livraison', $program->getDateLivraison()->format('d-m-Y') );
                $itemNode->addChild( 'dpe_bilan', $program->getDpe());
                $itemNode->addChild( 'dpe_valeur', $program->getDpeValue() );
                $offre = $itemNode->addChild( 'operation-speciale' );
                $offre->addChild( 'date-debut', $program->getDateDebutOffre() );
                $offre->addChild( 'date-fin', $program->getDateFinOffre() );
                $offre->addChild( 'titre', $program->getTitreOffre() );
                $offre->addChild( 'mention', $program->getMentionLegalOffre() );
                $offre->addChild( 'text', $program->getDescriptionOffre() );
                $photos = $itemNode->addChild( 'liste-photos-programme' );
                $photos->addChild( 'photo', $program->getImagePrincipale() ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagePrincipale(), 'poliris_image') : '' );
                for($i=0; $i <= 7; $i++) {
                    if($program->getImagesDocuments() && isset($program->getImagesDocuments()[$i])) {
                        $photos->addChild( 'photo', $program->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagesDocuments()[$i]->getName(), 'poliris_image') : '' );
                    }
                }
                $bureaux = $itemNode->addChild( 'liste-bureaux-vente' );
                $bureaux->addChild( 'nom', $agency->getName() );
                $bureaux->addChild( 'adresse', $agency->getAdress() );
                $bureaux->addChild( 'code-postal', $agency->getCodePostal() );
                $bureaux->addChild( 'commune', $agency->getVille() );
                $bureaux->addChild( 'nom', 'France' );
                $bureaux->addChild( 'email', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
                $bureaux->addChild( 'email2' );
                $bureaux->addChild( 'email3' );
                $bureaux->addChild( 'telephone', $agency->getTelephone() );
                $bureaux->addChild( 'fax' );
                $bureaux->addChild( 'horaires' );
                $bureaux->addChild( 'texte', $agency->getContent() );
                $bureaux->addChild( 'gps_longitude' );
                $bureaux->addChild( 'gps_latitude' );
                $complements = $itemNode->addChild( 'complements-programme' );
                $complements->addChild('piscine',$program->getSiPiscine() ? 'true' : 'false');
                $complements->addChild('ascenseur',$program->getSiAscenseur() ? 'true' : 'false');
                if($program->getTypeDeChauffage() == '4096') { $typeChauffage = 'Collectif'; } elseif($program->getTypeDeChauffage() == '8192') {$typeChauffage = 'individuel';} else {$typeChauffage = '';}
                $complements->addChild('nature-chauffage',$typeChauffage);
                $complements->addChild('type-chauffage',$program->getEnergieDeChauffage());
                $complements->addChild('is_copropriete');
                $complements->addChild('has_interphone',$program->getSiInterphone() ? 'true' : 'false');
                $complements->addChild('has_digicode',$program->getSiDigicode() ? 'true' : 'false');
                $complements->addChild('has_visiophone');
                $complements->addChild('has_gardien',$program->getSiGardien() ? 'true' : 'false');
                $complements->addChild('has_jardin',$program->getSiJardin() ? 'true' : 'false');
                $complements->addChild('has_acces_handicape');
                foreach ($labels as $item)
                    $complements->addChild('label',$item);
                $complements->addChild('usage-logement');
                $complements->addChild('type-logement');
                foreach ($defiscalisation as $item)
                    $complements->addChild('defiscalisation',$item);

                $lots = $itemNode->addChild( 'lots' );
                foreach ($program->getAds() as $ad) {
                    $bien = $ad->getBienImmobilier()[0];
                    $isAppartement = false;
                    $isMaison = false;
                    if($bien instanceof Maison){
                        $typeBien = 'Maison';$isMaison =true;
                    } elseif($bien instanceof Appartement){
                        $typeBien = 'Appartement';$isAppartement =true;
                    } else {
                        continue;
                    }

                    $lot = $lots->addChild( 'lot' );
                    $lot->addChild( 'quantite',1 );
                    $lot->addChild( 'type-bien',$typeBien );
                    $lot->addChild( 'reference',$ad->getReference() );
                    $lot->addChild( 'nb-pieces',$bien->getNbPieces() );
                    $lot->addChild( 'prix',$ad->getPrix() );
                    $lot->addChild( 'descriptif',$ad->getDescription() );
                    $photos = $lot->addChild( 'liste-photos-lot' );
                    $photos->addChild( 'photo', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$ad->getImagePrincipale(), 'poliris_image') : '' );
                    for($i=0; $i <= 7; $i++) {
                        if($program->getImagesDocuments() && isset($program->getImagesDocuments()[$i])) {
                            $photos->addChild( 'photo', $program->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/housemodels/'.$program->getImagesDocuments()[$i]->getName(), 'poliris_image') : '' );
                        }
                    }
                    $complements_lot = $lot->addChild( 'complements-lot' );
                    $complements_lot->addChild('ascenseur',$isAppartement ? ($bien->getSiAscenseur() ? 'true' : 'false') : '');
                    $complements_lot->addChild('etage',$isAppartement ? ($bien->getNbEtages() ? 'true' : 'false') : '');
                    $complements_lot->addChild('expositions',$bien->getExposition());
                    $complements_lot->addChild('ascenseur',$bien->getSiBalconsValue() ? 'true' : 'false');
                    $complements_lot->addChild('piscine');
                    $complements_lot->addChild( 'surface',($bien?$bien->getSurface():'') );
                    $complements_lot->addChild( 'nb_chambre',$isMaison ? $bien->getNbChambres() : '' );
                    $complements_lot->addChild( 'nb_salle_de_bain',$isMaison ? $bien->getNbSalleBain() : '' );
                    $complements_lot->addChild( 'nature_chauffage' );
                    $complements_lot->addChild( 'type_chauffage' );
                    $complements_lot->addChild( 'dpe_valeur', $isMaison ? $bien->getDpeValue() : '' );
                    $complements_lot->addChild( 'dpe_bilan', $isMaison ? $bien->getDpe() : '');
                    $complements_lot->addChild( 'has_box', $isMaison && $bien->getSiBox() ? 'true' : 'false');
                    $complements_lot->addChild( 'has_garage', $isMaison && $bien->getSiBox() ? 'true' : 'false');
                    $complements_lot->addChild( 'has_cave', $isMaison && $bien->getSiCaveValue() ? 'true' : 'false');
                    $complements_lot->addChild( 'has_parking', $isMaison && $bien->getSiParkingValue() ? 'true' : 'false');
                    $complements_lot->addChild( 'is_acces_handicape');
                    $complements_lot->addChild( 'has_climatisation');
                    $complements_lot->addChild( 'has_terrasse', $isMaison && $bien->getSiTerrasseValue() ? 'true' : 'false');
                    $complements_lot->addChild( 'is_meuble');
                }
            }
        }
        if(count($rootNode->xpath("programme")))
            return $rootNode->asXML();
        return null;
    }

    public function toParuvenduCSV($ad)
    {
        $csv = "";
        $newLine = "\r\n";

        if (!$ad instanceof Ad)
            throw new Exception('Every elements of the array must be an Ad');
        $diffusions = $ad->getTypeDiffusion() == 'duo' ? ['projet','terrain'] : [$ad->getTypeDiffusion()];
        $Terrain = $ad->getTerrain();
        $Maison  = $ad->getMaison();
        foreach($diffusions as $diffusion) {
            $isTerrain = $diffusion == 'terrain' ? true : false;
            //Titles
            $csv .= self::generateCSVField('Code Agence');
            $csv .= self::generateCSVField('Référence Annonce');
            $csv .= self::generateCSVField('Cahier');
            $csv .= self::generateCSVField('Famille');
            $csv .= self::generateCSVField('Rubrique');
            $csv .= self::generateCSVField('Sous-Rubrique');
            $csv .= self::generateCSVField('Adresse');
            $csv .= self::generateCSVField('Code postal');
            $csv .= self::generateCSVField('Ville');
            $csv .= self::generateCSVField('Pays');
            $csv .= self::generateCSVField('Titre');
            $csv .= self::generateCSVField('Text');
            $csv .= self::generateCSVField('Téléphone');
            $csv .= self::generateCSVField('Téléphone 2');
            $csv .= self::generateCSVField('Mail');
            $csv .= self::generateCSVField('Photo 1');
            $csv .= self::generateCSVField('Photo 2');
            $csv .= self::generateCSVField('Photo 3');
            $csv .= self::generateCSVField('Photo 4');
            $csv .= self::generateCSVField('Photo 5');
            $csv .= self::generateCSVField('Photo 6');
            $csv .= self::generateCSVField('Prix');
            $csv .= self::generateCSVField('Loyer Charges Comprises');
            $csv .= self::generateCSVField('Loyer Hors Charges');
            $csv .= self::generateCSVField('Charges');
            $csv .= self::generateCSVField('Honoraires');
            $csv .= self::generateCSVField('Constructibilité');
            $csv .= self::generateCSVField('Surface');
            $csv .= self::generateCSVField('Superficie');
            $csv .= self::generateCSVField('Charges');
        }
    }

    public function toAnnoncesJaunesPolirisCSV($ads,$portalRegistration)
    {
        $csv = '';
        $csv .= self::generateCSVField('Code agence');                                                          # 1 //todo
        $csv .= self::generateCSVField('Reference annonce');                                                          # 1 //todo
        $csv .= self::generateCSVField('Type transaction');                                                          # 1 //todo
        $csv .= self::generateCSVField('Type bien');                                                          # 1 //todo
        $csv .= self::generateCSVField('Mandat en exclusivite');                                                          # 1 //todo
        $csv .= self::generateCSVField('Titre annonce');                                                          # 1 //todo
        $csv .= self::generateCSVField('Description bien');                                                          # 1 //todo
        $csv .= self::generateCSVField('Date disponibilite');                                                          # 1 //todo
        $csv .= self::generateCSVField('Adresse');                                                          # 1 //todo
        $csv .= self::generateCSVField('Code postal');                                                          # 1 //todo
        $csv .= self::generateCSVField('Commune');                                                          # 1 //todo
        $csv .= self::generateCSVField('Pays');                                                          # 1 //todo
        $csv .= self::generateCSVField('Quartier proximite');                                                          # 1 //todo
        $csv .= self::generateCSVField('Url');                                                          # 1 //todo
        $csv .= self::generateCSVField('Description Url');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nom contact');                                                          # 1 //todo
        $csv .= self::generateCSVField('Contact telephone fixe');                                                          # 1 //todo
        $csv .= self::generateCSVField('Contact telephone mobile');                                                          # 1 //todo
        $csv .= self::generateCSVField('Contact Email');                                                          # 1 //todo
        $csv .= self::generateCSVField('Anciennete');                                                          # 1 //todo
        $csv .= self::generateCSVField('Etat general');                                                          # 1 //todo
        $csv .= self::generateCSVField('Refait a neuf');                                                          # 1 //todo
        $csv .= self::generateCSVField('Chauffage');                                                          # 1 //todo
        $csv .= self::generateCSVField('Situation location vacances');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer avec charges');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer sans charges');                                                          # 1 //todo
        $csv .= self::generateCSVField('Charges');                                                          # 1 //todo
        $csv .= self::generateCSVField('Depot de garantie');                                                          # 1 //todo
        $csv .= self::generateCSVField('Taxe habitation');                                                          # 1 //todo
        $csv .= self::generateCSVField('Prix vente');                                                          # 1 //todo
        $csv .= self::generateCSVField('Rente');                                                          # 1 //todo
        $csv .= self::generateCSVField('Taxe fonciere');                                                          # 1 //todo
        $csv .= self::generateCSVField('Frais d\'agence');                                                          # 1 //todo
        $csv .= self::generateCSVField('Annee construction');                                                          # 1 //todo
        $csv .= self::generateCSVField('Meuble');                                                          # 1 //todo
        $csv .= self::generateCSVField('Duplex');                                                          # 1 //todo
        $csv .= self::generateCSVField('Triplex');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loft');                                                          # 1 //todo
        $csv .= self::generateCSVField('Sans vis-a-vis');                                                          # 1 //todo
        $csv .= self::generateCSVField('Vue sur mer');                                                          # 1 //todo
        $csv .= self::generateCSVField('Etage');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre etages immeuble');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface habitable');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface totale');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface Carrez');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface sejour');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface salle … manger');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface terrasses balcons');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface cave');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre salles bain');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre salles eau');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre toilettes');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre pieces');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre chambres');                                                          # 1 //todo
        $csv .= self::generateCSVField('Nombre bureaux');                                                          # 1 //todo
        $csv .= self::generateCSVField('Double sejour');                                                          # 1 //todo
        $csv .= self::generateCSVField('Dernier etage');                                                          # 1 //todo
        $csv .= self::generateCSVField('Mitoyenne');                                                          # 1 //todo
        $csv .= self::generateCSVField('Plain pied');                                                          # 1 //todo
        $csv .= self::generateCSVField('Avec etages');                                                          # 1 //todo
        $csv .= self::generateCSVField('Avec sous-sol');                                                          # 1 //todo
        $csv .= self::generateCSVField('Orientation sud');                                                          # 1 //todo
        $csv .= self::generateCSVField('Orientation ouest');                                                          # 1 //todo
        $csv .= self::generateCSVField('Orientation nord');                                                          # 1 //todo
        $csv .= self::generateCSVField('Orientation est');                                                          # 1 //todo
        $csv .= self::generateCSVField('Interphone');                                                          # 1 //todo
        $csv .= self::generateCSVField('Digicode');                                                          # 1 //todo
        $csv .= self::generateCSVField('Alarme');                                                          # 1 //todo
        $csv .= self::generateCSVField('Ascenseur');                                                          # 1 //todo
        $csv .= self::generateCSVField('Gardien');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cheminee');                                                          # 1 //todo
        $csv .= self::generateCSVField('Parquet');                                                          # 1 //todo
        $csv .= self::generateCSVField('Terrasse');                                                          # 1 //todo
        $csv .= self::generateCSVField('Climatisation');                                                          # 1 //todo
        $csv .= self::generateCSVField('Balcon');                                                          # 1 //todo
        $csv .= self::generateCSVField('Jardin');                                                          # 1 //todo
        $csv .= self::generateCSVField('Piscine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Tennis');                                                          # 1 //todo
        $csv .= self::generateCSVField('Garage');                                                          # 1 //todo
        $csv .= self::generateCSVField('Parking');                                                          # 1 //todo
        $csv .= self::generateCSVField('Grenier');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cave');                                                          # 1 //todo
        $csv .= self::generateCSVField('Placards');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cuisine equipee');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cuisine americaine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Surface terrain');                                                          # 1 //todo
        $csv .= self::generateCSVField('Terrain constructible');                                                          # 1 //todo
        $csv .= self::generateCSVField('Capacite accueil');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer haute saison semaine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer haute saison quinzaine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer haute saison mois');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer basse saison semaine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer basse saison quinzaine');                                                          # 1 //todo
        $csv .= self::generateCSVField('Loyer basse saison mois');                                                          # 1 //todo
        $csv .= self::generateCSVField('Date debut location');                                                          # 1 //todo
        $csv .= self::generateCSVField('Date fin location');                                                          # 1 //todo
        $csv .= self::generateCSVField('Internet');                                                          # 1 //todo
        $csv .= self::generateCSVField('Telephone');                                                          # 1 //todo
        $csv .= self::generateCSVField('Television');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cable satellite');                                                          # 1 //todo
        $csv .= self::generateCSVField('Lave linge');                                                          # 1 //todo
        $csv .= self::generateCSVField('Lave vaisselle');                                                          # 1 //todo
        $csv .= self::generateCSVField('Congelateur');                                                          # 1 //todo
        $csv .= self::generateCSVField('Linge maison');                                                          # 1 //todo
        $csv .= self::generateCSVField('Four');                                                          # 1 //todo
        $csv .= self::generateCSVField('Microonde');                                                          # 1 //todo
        $csv .= self::generateCSVField('Barbecue');                                                          # 1 //todo
        $csv .= self::generateCSVField('Equipement bebe');                                                          # 1 //todo
        $csv .= self::generateCSVField('Femme menage');                                                          # 1 //todo
        $csv .= self::generateCSVField('Animaux autorises');                                                          # 1 //todo
        $csv .= self::generateCSVField('Non fumeur');                                                          # 1 //todo
        $csv .= self::generateCSVField('Cheques vacances');                                                          # 1 //todo
        $csv .= self::generateCSVField('Proche pistes ski');                                                          # 1 //todo
        $csv .= self::generateCSVField('Amenagement handicapes');                                                          # 1 //todo
        $csv .= self::generateCSVField('Duree minimale jours');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo1');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo2');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo3');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo4');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo5');                                                          # 1 //todo
        $csv .= self::generateCSVField('Photo6');                                                          # 1 //todo
        $csv .= self::generateCSVField('consommation énergie');                                                          # 1 //todo
        $csv .= self::generateCSVField('bilan consommation (classification)');                                                          # 1 //todo
        $csv .= self::generateCSVField('emission GES');                                                          # 1 //todo
        $csv .= self::generateCSVField('Bilan emission GES (classification)');                                                          # 1 //todo
        $csv .= self::generateCSVField('ville internet');                                                          # 1 //todo
        $csv .= self::generateCSVField('CP internet');                                                          # 1 //todo
        $csv .= "\r\n";

        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }
        /** @var Ad $ad */
        foreach ($ads as $ad) {
            if($ad->getDiffusion())
                if($ad && $ad->getPublished() && $ad->getDiffusion() && $portal->existIn($ad->getDiffusion()->getBroadcastPortals())) {
                    $login  = $portalRegistration->getIdentifier();
                    if(!$login || !$portal)
                        continue;

                    $newLine = "\r\n";
                    if (!$ad instanceof Ad)
                        continue;
                    $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
                    $isMaisonPortal = $portal->getTypeDiffusion() == 'maison' ? true: false;

                    $agency = $ad->getGroup()->getAgency();
                    $group      = $ad->getGroup();
                    $diffusion = $ad->getTypeDiffusion();
                    /** @var Terrain $Terrain */
                    $Terrain = $ad->getTerrain();
                    $Maison  = null;
                    $isTerrain = $diffusion == 'terrain' ? true : false;

                    if(!$isTerrain) {
                        /** @var Maison $Maison */
                        $Maison  = $ad->getMaison();
                    }

                    if(($isTerrainPortal && !$isTerrain) || ($isMaisonPortal && $isTerrain) || (!$isTerrain && !$Maison))
                        continue;

                    $title = $ad->getLibelle();
                    $description = $ad->getDescription();
                    if($adRewrite = $ad->getGroup()->getAdRewrite()) {
                        if($adRewrite->getPortals()->contains($portal)) {
                            $title = $this->Spin($adRewrite->getTitle(),$ad);
                            $description = $this->Spin($adRewrite->getDescription(),$ad);
                        }
                    }

                    if($adMentionLegals = $ad->getGroup()->getAdMentionLegals()) {
                        if($adMentionLegals->getPortals()->contains($portal)) {
                            $description .= $adMentionLegals->getBody();
                        }
                    }

                    $csv .= self::generateCSVField($login);                                                          # 1 //todo
                    $csv .= self::generateCSVField($ad->getReference());        # 2 //todo Reference generé ou autre?
                    $csv .= self::generateCSVField('vente','select');                                            # 3
                    $csv .= self::generateCSVField($isTerrain ? 'Terrain' : 'Maison - villa','select');        # 4
                    $csv .= self::generateCSVField('N');
                    $csv .= self::generateCSVField($title);
                    $csv .= self::generateCSVField($description);
                    $csv .= self::generateCSVField($ad->getCreatedAt()->format('d/m/Y'));
                    $csv .= self::generateCSVField($ad->getAdresse());                                           # 8
                    $csv .= self::generateCSVField($ad->getCodePostal());                                           # 8
                    $csv .= self::generateCSVField($ad->getVille());                                           # 8
                    $csv .= self::generateCSVField('France');                                  # 7
                    $csv .= self::generateCSVField('');                                                          # 9 //todo use xml flux to set quartier value
                    $csv .= self::generateCSVField($ad->getLienVisitVirtuel() ? $ad->getLienVisitVirtuel() : '');                                                          # 10
                    $csv .= self::generateCSVField($agency->getPrenom().' '.$agency->getNom());
                    $csv .= self::generateCSVField($this->container->get("app.tracking.export.helpers")->getAgencyPhoneTracking($agency));
                    $csv .= self::generateCSVField($this->container->get("app.tracking.export.helpers")->getAgencyPhoneTracking($agency));
                    $csv .= self::generateCSVField($group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getTypeDeChauffageValue());
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField($ad->getPrix(),'float'); # 11
                    $csv .= self::generateCSVField('');                                                          # 12
                    $csv .= self::generateCSVField('');                                                          # 13
                    $csv .= self::generateCSVField('');                                                          # 14
                    $csv .= self::generateCSVField('');                                                          # 15
                    $csv .= self::generateCSVField('');                                                          # 15
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getTypeArchitectureValue() == 'Duplex' ? 'O' : 'N'));
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getTypeArchitectureValue() == 'Triplex' ? 'O' : 'N'));
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getTypeArchitectureValue() == 'Loft' ? 'O' : 'N'));
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getVisAVis() ? 'O' : 'N'));
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField(!$isTerrain && $Maison->getNbEtages() ? $Maison->getNbEtages() : '');
                    $csv .= self::generateCSVField('');
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison?$Maison->getSurface():''),'float' );                                                          # 16
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiSejourValue() ? $Maison->getSurfaceSejour() : ''),'float');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiSalleAMangerValue() ? $Maison->getSurfaceSalleAManger() : ''),'float');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiBalconsValue() || $Maison->getSiTerrasseValue() ? $Maison->getSurfaceBalcons() + $Maison->getSurfaceTerrasse() : ''),'float');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiCaveValue() ? $Maison->getSurfaceCave() : ''),'float');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbSalleBain(),'number');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbSalleEau(),'number');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbToilette(),'number');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbPieces(),'number');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getNbChambres(),'number');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiBureauValue() ? 1 : 0),'number');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),'sud') !== false ? 'OUI' : 'NON') : '','boolean');   # 35
                    $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),'ouest') !== false ? 'OUI' : 'NON') : '','boolean');
                    $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),'nord') !== false ? 'OUI' : 'NON') : '','boolean');   # 38
                    $csv .= self::generateCSVField($isTerrain ? (strpos($Terrain->getExposition(),'est')  !== false && strpos($Terrain->getExposition(),'ouest') == false ? 'OUI' : 'NON') : '','boolean');   # 36
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiInterphone() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiDigicode() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiAlarme() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiGardien() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiChemineeValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiTerrasseValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiBalconsValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiJardin() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiPiscine() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiParkingValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField('');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiCaveValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiPlacardsValue() ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getTypeCuisineValue() == 'Équipée' ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getTypeCuisineValue() == 'Américaine' ? 'O' : 'N'),'boolean');            # 158
                    $csv .= self::generateCSVField($Terrain?$Terrain->getSurface():'','float');                    # 17
                    $csv .= self::generateCSVField($Terrain->getConstructible() ? 'O' : 'N','boolean');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField($isTerrain ? '' : ($Maison->getSiTV() ? 'O': 'N'), 'boolean');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField($ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(), 'poliris_image') : '');               # 85
                    for($i=0; $i < 5; $i++) {
                        if($ad->getImagesDocuments() && isset($ad->getImagesDocuments()[$i])) {
                            $csv .= self::generateCSVField($ad->getImagesDocuments()[$i] ? $this->liipImagine->getBrowserPath('/uploads/ad/'.$ad->getImagesDocuments()[$i]->getName(), 'poliris_image') : '');
                        } else {
                            $csv .= self::generateCSVField('');
                        }
                    }
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getDpeValue());               # 176
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getDPE(),'select');               # 177
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getGesValue());                                     # 178
                    $csv .= self::generateCSVField($isTerrain ? '' : $Maison->getGES(),'select');
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= self::generateCSVField('');                    # 17
                    $csv .= $newLine;
                }
        }
        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    /**
     * @param $string
     * @return string
     */
    protected static function generateCSVField($string,$type = 'string')
    {
        $quotationMark = "\"";
        $separatorMark = "!#";

        $string = html_entity_decode($string, ENT_QUOTES, "ISO-8859-15");
        $string = preg_replace("#\"#", "'", $string);
        $string = preg_replace("#[\r\n|\r|\n]#", '<BR>', $string);
//        if(empty($string) || $type != 'string') {
//            return $string.$separatorMark;
//        } else {
        return $quotationMark.$string.$quotationMark.$separatorMark;
//        }
    }

    /**
     *
     * @param $object
     * @param $methodName
     * @return string
     */
    protected static function toCSVField($object, $methodName)
    {
        if (method_exists($object, $methodName)) {
            if (is_bool($object->$methodName())) {
                if ($object->$methodName()) {
                    return self::generateCSVField('OUI');
                } else {
                    return self::generateCSVField('NON');
                }
            } elseif($object->$methodName() instanceof \DateTime) {
                return self::generateCSVField($object->$methodName()->format('d/m/Y'));
            } else {
                return self::generateCSVField($object->$methodName());
            }
        } else {
            return self::generateCSVField("");
        }
    }

    /**
     * @param $txt
     * @param Ad $ad
     * @return mixed
     */
    public function Spin($txt, $ad){

        /** @var Maison $maison */
        $maison = $ad->getMaison();
        $surfaceMaison = $maison ? $maison->getSurface() : null;
        /** @var Terrain $terrain */
        $terrain = $ad->getTerrain();
        $surfaceTerrain = $terrain ? $terrain->getSurface() : null;

        $variables = [
            '[VILLE]' => $ad->getVille(),
            '[ENSEIGNE]' => $ad->getGroup()->getName(),
            '[SURFACE MAISON]' => $surfaceMaison,
            '[SURFACE TERRAIN]' => $surfaceTerrain,
            '[REFERENCE ANNONCE]' => $ad->getReference(),
        ];

        $txt = str_replace(array_keys($variables),array_values($variables),$txt);
        $pattern = '#\{([^{}]*)\}#msi';
        preg_match_all($pattern, $txt, $out);

        $toFind = Array();
        $toReplace = Array();

        foreach($out[0] AS $id => $match){
            $choices = explode("~", $out[1][$id]);
            $toFind[]=$match;
            $toReplace[]=trim($choices[rand(0, count($choices)-1)]);
        }

        return str_replace($toFind, $toReplace, $txt);
    }
}
