<?php
/**
 * Client class for managing API requests
 * @package AppBundle\WebinairBundle
 */

namespace AppBundle\Service;

use AppBundle\Entity\Api\Auth;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;

/**
 * Class Dexem
 * @package AppBundle\Service\Dexem
 */
class Dexem
{
    /**
     * @var string root URL for authorizing requests
     */
    private $endpoint = 'https://calltracking.dexem.com/api/v1';

    /**
     * @var string key to access the API
     */
    private $apiKey;

    private $userId;
    private $password;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleClient;

    private $auth = null;

    /**
     * Default constructor.
     *
     * Configures the client for authenticating.
     *
     * @param string $apiKey client ID or API key
     * @param string|null $accessToken optionally provide an obtained OAuth access token
     *   to configure the auth property
     * @param LoggerInterface|null $logger logger implementation to log requests and responses against
     */
    public function __construct($apiKey, $password, $accessToken = NULL, LoggerInterface $logger = NULL)
    {
        $this->apiKey   = $apiKey;
        $this->password = $password;
        $this->guzzleClient = new \GuzzleHttp\Client(array(
            'base_url' => $this->endpoint
        ));
        if ($accessToken !== NULL) {
            $auth = new Auth();
            $auth->setAccessToken($accessToken);
            $this->setAuth($auth);
        } else {
            $auth = $this->authenticate();
            $this->setAuth($auth);
        }
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getGuzzleClient()
    {
        return $this->guzzleClient;
    }

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function setGuzzleClient($client)
    {
        $this->guzzleClient = $client;
    }

    /**
     * @return Auth
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param Auth $auth
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;
    }

    /**
     * Authenticate with the server and retrieve an auth token.
     *
     * Currently implements Citrix's {@link https://developer.citrixonline.com/page/direct-login "Direct Login"} method.
     *
     * @param string $userId
     * @param string $password
     * @return Auth
     */
    public function authenticate()
    {
        $url = "{$this->endpoint}/campaigns.json";

        $options = [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'auth' => [
                $this->getApiKey(), $this->password
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, $options);
        if($response->getStatusCode()) {
            $jsonBody = json_decode($response->getBody(),true);
            dump($jsonBody);exit;
            return new Auth($jsonBody);
        }
        return null;
    }

    /**
     * Handle sending requests to the API. All responses returned as JSON.
     *
     * Request body sent as JSON.
     *
     * @param string $method HTTP method for the request
     * @param string $path relative URL to append to the root API endpoint
     * @param bool $isAuthRequest optional flag to not pass the OAuth token with request
     *  because we do not have it yet
     * @param array $postBody body content for a POST or PUT request
     * @return mixed
     * @throws \GuzzleHttp\Exception\RequestException
     */
    public function sendRequest($method, $path,array $params = [])
    {
        $guzzleClient = $this->getGuzzleClient();
        $options = array(
            'headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ]
        );
        if (!$this->auth)
            $this->auth = $this->authenticate();

        $accessToken = $this->auth->getAccessToken();
        $options['headers']['Authorization'] = $accessToken;
        if($params)
            $options = array_merge($options,$params);

        $path =  "{$this->endpoint}G2M/rest/{$path}";
        $response = $guzzleClient->request($method, $path, $options);

        return json_decode($response->getBody(),true);
    }

}
