<?php
/**
 * Created by PhpStorm.
 * User: novactive
 * Date: 20/12/17
 * Time: 12:12
 */

namespace AppBundle\Service\Export;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Maison;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Service\SimpleXMLExtended;

class Mitula extends StandardFlux{

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * check if the xml is not empty => no ads
     * @param SimpleXMLExtended $rootNode
     * @return int
     */
    function countNodes($rootNode){
        return count($rootNode->xpath("ad"));
    }

    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    function createRootNode()
    {
        return new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' ?><Mitula></Mitula>" );
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addNode($ad, $portal, &$rootNode)
    {
        if($ad->getPublished() && $ad->getDiffusion()) {
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
            $agency = $ad->getGroup()->getAgency();
            $diffusion = $ad->getTypeDiffusion();
            $isTerrain = $diffusion == 'terrain' ? true : false;
            $Terrain = $ad->getTerrain();
            $typeBien = 'terrain';
            $Maison  = null;
            $appartement  = null;
            if($ad->getTypeDiffusion() != 'terrain') {
                /** @var Maison $Maison **/
                $Maison = $ad->getMaison();
                $typeBien = 'maison';
            }
            if( $ad->getProgram() && $ad->getProgram()->getType() == 'appartement') {
                $typeBien = 'appartement';
                /** @var Appartement $appartement **/
                $appartement = $ad->getAppartement();
            }

            if($isTerrainPortal && !$isTerrain)
                return false;
            $itemNode = $rootNode->addChildWithCDATA('ad');
            $itemNode->addChildWithCDATA( 'id', $ad->getId());
            $itemNode->addChildWithCDATA( 'url', $ad->getLienVisitVirtuel() );
            $itemNode->addChildWithCDATA( 'title', $ad->getLibelle() );
            $itemNode->addChildWithCDATA( 'type', $isTerrain ?  'Land For Sale' : 'For Sale' );
            //agency information
            $itemNode->addChildWithCDATA( 'agency', $agency->getName() );

            $itemNode->addChildWithCDATA( 'content', $ad->getDescription() );
            $itemNode->addChildWithCDATA( 'price', $ad->getPrix() );
            $itemNode->addChildWithCDATA( 'property_type', $typeBien );
            $itemNode->addChildWithCDATA( 'floor_area', $isTerrain ?
                ($Terrain ? (int)$Terrain->getSurface() : 0) :
                ($appartement ? (int)$appartement->getSurface() :
                    ($Maison ? (int)$Maison->getSurface() : '')));
            $itemNode->addChildWithCDATA( 'rooms', $appartement ? $appartement->getNbChambres():
                ($Maison ? $Maison->getNbChambres() : '' ));
            $itemNode->addChild( 'bathrooms', $appartement ?(int)$appartement->getNbToilette():
                ($Maison?(int)$Maison->getNbToilette():0)
            );
            $itemNode->addChild( 'parking', $appartement ?(int)$appartement->getNbParking():
                ($Maison?(int)$Maison->getNbParking():0)
            );
            $itemNode->addChildWithCDATA( 'address', $ad->getAdresse() );
            $itemNode->addChildWithCDATA( 'city', $ad->getVille() );
            $itemNode->addChildWithCDATA( 'city_area', '' );
            $itemNode->addChildWithCDATA( 'postcode', $ad->getCodePostal() );
            $itemNode->addChildWithCDATA( 'region', '' );
            $itemNode->addChildWithCDATA( 'latitude', '' );
            $itemNode->addChildWithCDATA( 'longitude', '' );
            $imagesNode = $itemNode->addChild("pictures");
            $imageNode = $imagesNode->addChild( 'picture');
            $imageNode->addChildWithCDATA( 'picture_url', $ad->getImagePrincipale() ?
                $this->liipImagine->getBrowserPath(
                    '/uploads/ad/'.$ad->getImagePrincipale(), 'poliris_image') :
                '' );
            $imageNode->addChildWithCDATA( 'picture_title', $ad->getTitreImagePrincipale() ? $ad->getTitreImagePrincipale() : '' );
            if($ad->getImagesDocuments()){
                foreach($ad->getImagesDocuments() as $index => $imageDoc) {
                    $imageUrl = $imageDoc->getName() ?
                        $this->liipImagine->getBrowserPath('/uploads/ad/'.$imageDoc->getName(), 'poliris_image') :
                        '' ;
                    if($imageUrl) {
                        $imageNode = $imagesNode->addChild('picture');
                        $imageNode->addChildWithCDATA('picture_url', $imageUrl);
                        $imageNode->addChildWithCDATA('picture_title',
                            $imageDoc->getTitle() ? $imageDoc->getTitle() : '');
                    }
                }
            }
            $itemNode->addChildWithCDATA( 'virtual_tour', $ad->getLienVisitVirtuel());
            $itemNode->addChildWithCDATA( 'date', date_format($ad->getCreatedAt(), 'Y-m-d'));
        }
    }
}