<?php
/**
 * Created by PhpStorm.
 * User: novactive
 * Date: 20/12/17
 * Time: 12:12
 */

namespace AppBundle\Service\Export;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Maison;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Service\SimpleXMLExtended;

class Achatterrain extends StandardFlux
{

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }


    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    function createRootNode()
    {
        $rootNode = new SimpleXMLExtended("<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
                    <ANNONCES></ANNONCES>");
        return $rootNode;
    }

    /**
     * check if the xml is not empty => no ads
     * @param SimpleXMLExtended $rootNode
     * @return int
     */
    function countNodes($rootNode)
    {
        return count($rootNode->xpath("ANNONCE"));
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addNode($ad, $portal, &$rootNode)
    {
        if ($ad->getPublished() && $ad->getDiffusion()) {
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            $group = $ad->getGroup();
            $agency = $ad->getGroup()->getAgency();
            $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true : false;
            $diffusion = $ad->getTypeDiffusion();
            $isTerrain = $diffusion == 'terrain' ? true : false;
            $Terrain = $ad->getTerrain();
            $typeBien = 'terrain';
            $Maison = null;
            if ($ad->getTypeDiffusion() != 'terrain') {
                /** @var Maison $Maison * */
                $Maison = $ad->getMaison();
                $typeBien = 'maison';
            }

            if ($isTerrainPortal && !$isTerrain) {
                return false;
            }

            $itemAnnonceNode = $rootNode->addChild("ANNONCE");
            //agency information
            $itemAnnonceNode->addChild('AGENCE_REF', $agency->getId());
            $itemAnnonceNode->addChildWithCDATA('AGENCE_NOM', $agency->getName());
            $itemAnnonceNode->addChild('AGENCE_URL', '');
            $itemAnnonceNode->addChildWithCDATA('AGENCE_ADRESSE', $agency->getAdress());
            $itemAnnonceNode->addChildWithCDATA('AGENCE_VILLE', $agency->getVille());
            $itemAnnonceNode->addChild('AGENCE_CP', $agency->getCodePostal());
            $itemAnnonceNode->addChild('AGENCE_MAIL',
                $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());
            $itemAnnonceNode->addChild('AGENCE_TEL', $agency->getExportTelephone());
            $itemAnnonceNode->addChild('AGENCE_FAX', '');
            $itemAnnonceNode->addChild('AGENCE_LOGO', $agency->getAvatar() ?
                $this->liipImagine->getBrowserPath('/uploads/ad/' . $agency->getAvatar(), 'poliris_image') : '');
            //ad information
            $itemAnnonceNode->addChild('BIEN_REF', $ad->getReference());
            $itemAnnonceNode->addChild('BIEN_DATE', date_format($ad->getCreatedAt(), 'Y-m-d'));
            $itemAnnonceNode->addChild('BIEN_DATE_MODIF', date_format($ad->getUpdatedAt(), 'Y-m-d'));
            $itemAnnonceNode->addChild('BIEN_VILLE', $ad->getVille());
            $itemAnnonceNode->addChild('BIEN_CP', $ad->getCodePostal());
            $itemAnnonceNode->addChild('BIEN_SURFACE_TERRAIN', $Terrain?(int)$Terrain->getSurface():'');
            $itemAnnonceNode->addChild('BIEN_SURFACE_MAISON', $Maison?(int)$Maison->getSurface():'');
            $itemAnnonceNode->addChild('BIEN_NOMBRES_PIECES', $Maison ? $Maison->getNbPieces() : '' );
            $itemAnnonceNode->addChild('BIEN_NOMBRES_CHAMBRES', $Maison ? $Maison->getNbChambres() : '' );
            $itemAnnonceNode->addChild('BIEN_MODELES', '' );
            $itemAnnonceNode->addChild('BIEN_PRIX', $ad->getPrix() );
            $itemAnnonceNode->addChild('BIEN_TYPE', "22" );
            $itemAnnonceNode->addChildWithCDATA('BIEN_TITRE', $ad->getLibelle() );
            $itemAnnonceNode->addChildWithCDATA('BIEN_TEXTE', $ad->getDescription() );


            $itemAnnonceNode->addChild( 'BIEN_PHOTO_1', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath('/uploads/ad/'.$ad->getImagePrincipale(), 'poliris_image') : '' );
            $imageIndex = 2;
            if($ad->getImagesDocuments()){
                foreach($ad->getImagesDocuments() as $index => $imageDoc) {
                    $imageUrl = $imageDoc->getName() ?
                        $this->liipImagine->getBrowserPath(
                            '/uploads/ad/' . $imageDoc->getName(),
                            'poliris_image'
                        ) : '';
                    if($imageUrl) {
                        $itemAnnonceNode->addChild('BIEN_PHOTO_' . ($imageIndex++), $imageUrl);
                    }
                }

            }
            $itemAnnonceNode->addChild( 'BIEN_VENDU', 0 );
        }
        return $rootNode;
    }
}










