<?php
/**
 * Created by PhpStorm.
 * User: novactive
 * Date: 20/12/17
 * Time: 12:12
 */

namespace AppBundle\Service\Export;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Maison;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Service\SimpleXMLExtended;

class Zilek extends StandardFlux{

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->isArchive = true;
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     */
    function addNode($ad, $portal, &$rootNode)
    {
        if($ad->getPublished() && $ad->getDiffusion()) {
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
            $group = $ad->getGroup();
            $agency = $ad->getGroup()->getAgency();
            $diffusion = $ad->getTypeDiffusion();
            $isTerrain = $diffusion == 'terrain' ? true : false;
            $Terrain = $ad->getTerrain();
            $typeBien = 'terrain';
            $Maison  = null;
            $appartement  = null;
            if($ad->getTypeDiffusion() != 'terrain') {
                /** @var Maison $Maison **/
                $Maison = $ad->getMaison();
                $typeBien = 'maison';
            }
            if( $ad->getProgram() && $ad->getProgram()->getType() == 'appartement') {
                $typeBien = 'appartement';
                /** @var Appartement $appartement **/
                $appartement = $ad->getAppartement();
            }
            if($isTerrainPortal && !$isTerrain)
                return;
            $itemNode = $rootNode->addChild('bien');
            $itemNode->addAttribute("id", $ad->getId());
            //agency information
            $itemNode->addChildWithCDATA( 'nom_agence', $agency->getName() );
            $itemNode->addChild( 'telephone_agence', $agency->getExportTelephone() );
            $itemNode->addChild( 'fax_agence', '' );
            $itemNode->addChild( 'email_agence', $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail() );
            $itemNode->addChild( 'url_agence', '' );
            $itemNode->addChild( 'ville_agence', $agency->getVille() );
            $itemNode->addChild( 'code_postal_agence', $agency->getCodePostal() );
            $itemNode->addChildWithCDATA( 'adresse_agence', $agency->getAdress() );

            // ad information
            $itemNode->addChild( 'code_postal', $ad->getCodePostal() );
            $itemNode->addChild( 'ville', $ad->getVille() );
            $itemNode->addChild( 'reference', $ad->getReference() );
            $itemNode->addChild( 'nom_secteur', '' );
            $itemNode->addChild( 'proximite', '' );
            $itemNode->addChildWithCDATA( 'adresse', $ad->getAdresse() );
            $itemNode->addChild( 'gps_longitude', '' );
            $itemNode->addChild( 'gps_latitude', '' );
            $itemNode->addChild( 'annee_construction', '' );

            $itemNode->addChild( 'titre', $ad->getLibelle() );
            $itemNode->addChildWithCDATA( 'description', $ad->getDescription() );
            $itemNode->addChild( 'type_transaction', "vente" );
            $itemNode->addChild( 'type_bien', $typeBien );
            $itemNode->addChild( 'nb_piece', $appartement ? (int)$appartement->getNbPieces():
                    ($Maison?(int)$Maison->getNbPieces():0)
            );
            $itemNode->addChild( 'nb_chambre', $appartement?(int)$appartement->getNbChambres():
                ($Maison?(int)$Maison->getNbPieces():0)
            );
            $itemNode->addChild( 'nb_sdb', $appartement ?(int)$appartement->getNbSalleBain():
                ($Maison?(int)$Maison->getNbSalleBain():0)
            );
            $itemNode->addChild( 'nb_sde', $appartement ?(int)$appartement->getNbSalleEau():
                ($Maison?(int)$Maison->getNbSalleEau():0)
            );
            $itemNode->addChild( 'nb_wc', $appartement ?(int)$appartement->getNbToilette():
                ($Maison?(int)$Maison->getNbToilette():0)
            );
            $itemNode->addChild( 'nb_parking', $appartement ?(int)$appartement->getNbParking():
                ($Maison?(int)$Maison->getNbParking():0)
            );
            $itemNode->addChild( 'etage', '' );
            $itemNode->addChild( 'nb_etage', $appartement ?(int)$appartement->getNbEtages():
                ($Maison?(int)$Maison->getNbEtages():0)
            );
            $itemNode->addChild( 'surface_habitable', $isTerrain ?
                ($Terrain ? (int)$Terrain->getSurface() : 0) :
                ($appartement ? (int)$appartement->getSurface() :
                    ($Maison ? (int)$Maison->getSurface() : '')));
            $itemNode->addChild( 'surface_jardin', $appartement ? (int)$appartement->getSurfaceTerrasse():
                ($Maison ? (int)$Maison->getSurfaceTerrasse():'')
            );
            $itemNode->addChild( 'valeur_energie', $appartement ? (int)$appartement->getDpeValue() :
                ($Maison ? (int) $Maison->getDpeValue():''));
            $itemNode->addChild( 'valeur_ges', $appartement ? (int)$appartement->getGesValue():
                ($Maison ? $Maison->getGesValue() : ''));
            $itemNode->addChild( 'prix', $ad->getPrix() );
            $itemNode->addChild( 'nb_cave', $appartement ? (int)$appartement->getSiCaveValue():
                ($Maison && $Maison->getSiCaveValue()? 1 : ''));
            $itemNode->addChild( 'ascenseur', $appartement && $appartement->getSiAscenseur()? 'oui' : 'non' );
            $itemNode->addChild( 'balcon', $appartement && $appartement->getSiBalcons() ? 'oui':
                ($Maison && $Maison->getSiBalcons()? 'oui' : 'non' ));
            $itemNode->addChild( 'terrasse', $appartement && $appartement->getSiTerrasse()? 'oui':
                ($Maison && $Maison->getSiTerrasse()? 'oui' : 'non' ));
            $itemNode->addChild( 'piscine', $appartement && $appartement->getSiPiscine() ? 'oui':
                ($Maison && $Maison->getSiPiscine()? 'oui' : 'non' ));
            $itemNode->addChild( 'taxes', '' );
            $chauffageType = "sans";
            if($appartement && $appartement->getEnergieDeChauffage()){
                switch ($appartement->getEnergieDeChauffage()){
                    case "gaz":
                    case "fuel":
                    case "bois":
                        $chauffageType = $appartement->getEnergieDeChauffage();
                        break;
                    case "éléctricité" :
                        $chauffageType = "électrique";
                        break;
                    case "solaire" :
                        $chauffageType = "sol";
                        break;
                    default : $chauffageType = "collectif"; break;
                }
            }
            elseif($Maison && $Maison->getEnergieDeChauffage()){
                switch ($Maison->getEnergieDeChauffage()){
                    case "gaz":
                    case "fuel":
                    case "bois":
                        $chauffageType = $Maison->getEnergieDeChauffage();
                        break;
                    case "éléctricité" :
                        $chauffageType = "électrique";
                        break;
                    case "solaire" :
                        $chauffageType = "sol";
                        break;
                    default : $chauffageType = "collectif"; break;
                }
            }
            $itemNode->addChild( 'type_chauffage', $chauffageType );
            $itemNode->addChild( 'type_cuisine', $appartement ? $appartement->getTypeCuisineValue() :
                ($Maison ? $Maison->getTypeCuisineValue() : '' ));
            $imagesNode = $itemNode->addChild("images");
            $imageElm = $imagesNode->addChild( 'image', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(), 'poliris_image') : '' );
            $imageElm->addAttribute("id", "1");
            $indexImage = 2;
            if($ad->getImagesDocuments()){
                foreach($ad->getImagesDocuments() as $index => $imageDoc) {
                    $imageUrl = $imageDoc->getName() ?
                        $this->liipImagine->getBrowserPath('/uploads/ad/'.$imageDoc->getName(), 'poliris_image') :
                        ''  ;
                    if($imageUrl) {
                        $imageElm = $imagesNode->addChild('image', $imageUrl );
                        $imageElm->addAttribute("id", $indexImage ++);
                    }
                }
            }
        }

    }

    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    function createRootNode()
    {
        return new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' ?><biens></biens>" );
    }
}