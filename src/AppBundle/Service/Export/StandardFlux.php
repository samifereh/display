<?php
/**
 * Created by PhpStorm.
 * User: anis
 * Date: 27/12/17
 * Time: 21:42
 */

namespace AppBundle\Service\Export;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\File;
use AppBundle\Service\SimpleXMLExtended;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use ZipArchive;

abstract class StandardFlux
{
    protected static $instance;
    /**
     * @var Container
     */
    protected $container;
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Liip\ImagineBundle\Imagine\Cache\CacheManager|object
     */
    protected $liipImagine;


    /**
     * @var string
     */
    protected $exportPath;


    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var bool
     */
    protected $isArchive;


    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        $this->container    = $container;
        $this->em           = $this->container->get('doctrine')->getManager();
        $this->liipImagine           = $this->container->get('liip_imagine.cache.manager');
        $this->isArchive = true;
    }

    /**
     * @param string $exportPath
     */
    public function setExportPath($exportPath)
    {
        $this->exportPath = $exportPath;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @param mixed $groups  can be group or array of group
     * @param mixed $portalOrPortalRegistration can be a portal or registration
     * @return bool
     */
    public function generate($groups, $portalOrPortalRegistration) {
        $rootNode = $this->createRootNode();
        if ($portalOrPortalRegistration instanceof PortalRegistration) {
            $group = $groups;
            $this->toXML($group, $portalOrPortalRegistration, $rootNode);
        } elseif($portalOrPortalRegistration instanceof Portal) {
            $portal = $portalOrPortalRegistration;
            /** @var \AppBundle\Entity\Group $group */
            foreach ($groups as $group) {
                $groupRegistratedPortal = [];
                foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {
                    if($portalRegistration->getBroadcastPortal())
                        $groupRegistratedPortal[] = $portalRegistration->getBroadcastPortal();
                }
                if($portal->existIn($groupRegistratedPortal)) {
                    $this->toXML($group, $portal, $rootNode);
                }
            }
        } else {
            $portal = null;
        }

        if($this->countNodes($rootNode)) {
            $xmlFile = File::mkFile($this->fileName . '.xml', true);
            $xmlFile->write($rootNode->asXML());
            if ($this->isArchive) {


                $zip = new ZipArchive();
                $zip->open($this->exportPath . $this->fileName . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
                $zip->addFile($xmlFile->getPath(), $this->fileName . '.xml');
                $zip->close();

                $xmlFile->delete();
                return true;

            }
        }
        return false;
    }

    /**
     * check if the xml is not empty => no ads
     * @param SimpleXMLExtended $rootNode
     * @return int
     */
    protected function countNodes($rootNode){
        return count($rootNode->xpath("bien"));
    }

    public function toXML($group,$portalRegistration, &$rootNode)
    {
        if ($portalRegistration instanceof PortalRegistration) {
            $portal = $portalRegistration->getBroadcastPortal();
        } elseif($portalRegistration instanceof Portal) {
            $portal = $portalRegistration;
        } else {
            $portal = null;
        }
        $ads = $this->em->getRepository('AppBundle:Ad')->getAdsByPortalDiffusion($group,$portal);
        if($ads){
            $adsRootNode = $this->addAgencyNode($group, $rootNode);
            /** @var \AppBundle\Entity\Ad $ad */
            foreach ($ads as $ad) {
                $this->addNode($ad, $portal, $adsRootNode);
            }
        }
        return $rootNode;
    }

    /**
     * @param $group
     * @param $rootNode
     * @return mixed
     */
    public function addAgencyNode($group, &$rootNode){
        return $rootNode;
    }

    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    abstract function createRootNode();

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    abstract function addNode($ad, $portal, &$rootNode);


}

