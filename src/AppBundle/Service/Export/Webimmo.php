<?php
/**
 * Created by PhpStorm.
 * User: novactive
 * Date: 20/12/17
 * Time: 12:12
 */

namespace AppBundle\Service\Export;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Maison;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Service\SimpleXMLExtended;

class Webimmo extends StandardFlux{

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    function createRootNode()
    {
        return new SimpleXMLExtended( "<?xml version='1.0' encoding='UTF-8' ?><listings></listings>" );
    }

    protected function countNodes($rootNode){
        return count($rootNode->xpath("listing"));
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addAgencyNode($group, &$rootNode)
    {
        /** @var Agency $agency */
        $agency = $group->getAgency();
        $itemNode = $rootNode->addChildWithCDATA('listing');
        $itemAnnonceur = $itemNode->addChildWithCDATA('annonceur');
        $itemAnnonceur->addChildWithCDATA( 'nom_societe', $agency->getName());
        $itemAnnonceur->addChildWithCDATA( 'site_societe', '' );
        $itemAnnonceur->addChildWithCDATA( 'ville_societe', $agency->getVille() );
        $itemAnnonceur->addChildWithCDATA( 'pays_societe', 'France' );
        $itemAnnonceur->addChildWithCDATA( 'email_societe', $agency->getEmail() );
        $itemAnnonceur->addChildWithCDATA( 'tel_societe', $agency->getTelephone() );
        $itemAnnonceur->addChildWithCDATA( 'langue_fr', 'oui');
        $itemAnnonceur->addChildWithCDATA( 'langue_en', 'non');
        $itemAnnonceur->addChild("prenom_contact", $agency->getPrenom());
        $itemAnnonceur->addChild("nom_contact", $agency->getNom());
        return $itemNode;
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addNode($ad, $portal, &$rootNode)
    {
        if($ad->getPublished() && $ad->getDiffusion()) {
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true: false;
            $agency = $ad->getGroup()->getAgency();
            $diffusion = $ad->getTypeDiffusion();
            $isTerrain = $diffusion == 'terrain' ? true : false;
            $Terrain = $ad->getTerrain();
            $typeBien = 'terrain';
            $Maison  = null;
            if($ad->getTypeDiffusion() != 'terrain') {
                /** @var Maison $Maison **/
                $Maison = $ad->getMaison();
                $typeBien = 'maison';
            }

            if($isTerrainPortal && !$isTerrain)
                return false;
            $itemAnnonce = $rootNode->addChildWithCDATA('annonce');
            $itemAnnonce->addChildWithCDATA( 'reference', $ad->getReference() );
            $itemAnnonce->addChildWithCDATA( 'date_derniere_modification', date_format($ad->getUpdatedAt(), 'Y-m-d'));
            $itemAnnonce->addChildWithCDATA( 'type_transac', 'vente' );
            $itemAnnonce->addChildWithCDATA( 'type_bien', $typeBien );
            $itemAnnonce->addChildWithCDATA( 'pieces', $Maison ? $Maison->getNbPieces() : '' );
            $itemAnnonce->addChildWithCDATA( 'chambres', $Maison ? $Maison->getNbChambres() : '' );
            $itemAnnonce->addChildWithCDATA( 'surface', $isTerrain ?
                ($Terrain?(int)$Terrain->getSurface():0) :
                ($Maison?(int)$Maison->getSurface():''));
            $itemAnnonce->addChildWithCDATA( 'adresse', $ad->getAdresse() );
            $itemAnnonce->addChildWithCDATA( 'code_postal', $ad->getCodePostal() );
            $itemAnnonce->addChildWithCDATA( 'ville', $ad->getVille() );
            $itemAnnonce->addChildWithCDATA( 'region', '' );
            $itemAnnonce->addChildWithCDATA( 'type_localisation', '' );
            $itemAnnonce->addChildWithCDATA( 'prix', $ad->getPrix() );
            $itemAnnonce->addChildWithCDATA( 'devise', 'eur' );
            $itemAnnonce->addChildWithCDATA( 'meuble', '' );
            $itemAnnonce->addChildWithCDATA( 'jardin', $Maison && $Maison->getSiJardin()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'balcon', $Maison && $Maison->getSiBalcons()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'terrasse', $Maison && $Maison->getSiTerrasse()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'parking', $Maison && $Maison->getSiParking()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'garage', $Maison && $Maison->getPorteGarage()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'cave', $Maison && $Maison->getSiCave()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'vue_mer', '' );
            $itemAnnonce->addChildWithCDATA( 'piscine', $Maison && $Maison->getSiPiscine()? 'oui' : 'non' );
            $itemAnnonce->addChildWithCDATA( 'golf', '' );
            $itemAnnonce->addChildWithCDATA( 'latitude', '' );
            $itemAnnonce->addChildWithCDATA( 'longitude', '' );
            $itemAnnonce->addChildWithCDATA( 'point_fort_1_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'point_fort_2_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'point_fort_3_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'point_fort_4_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'point_fort_5_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'description_courte_fr', $ad->getLibelle() );
            $itemAnnonce->addChildWithCDATA( 'description_longue_propriete_fr', $ad->getDescription() );
            $itemAnnonce->addChildWithCDATA( 'description_longue_localisation_fr', '' );
            $itemAnnonce->addChildWithCDATA( 'date_parution', date_format($ad->getCreatedAt(), 'Y-m-d'));
            $itemAnnonce->addChildWithCDATA( 'photo_principale', $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath('/uploads/ad/'.$ad->getImagePrincipale(), 'poliris_image') : '' );
            $indexImage = 2;
            if($ad->getImagesDocuments()){
                foreach($ad->getImagesDocuments() as $index => $imageDoc) {
                    $imageUrl = $imageDoc->getName() ?
                        $this->liipImagine->getBrowserPath('/uploads/ad/'.$imageDoc->getName(), 'poliris_image') :
                        ''  ;
                    if($imageUrl) {
                        $itemAnnonce->addChildWithCDATA( 'photo_'.($indexImage++), $imageUrl );
                    }

                }
            }
        }
    }
}