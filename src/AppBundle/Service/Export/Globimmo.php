<?php
/**
 * Created by PhpStorm.
 * User: novactive
 * Date: 20/12/17
 * Time: 12:12
 */

namespace AppBundle\Service\Export;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Maison;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Service\SimpleXMLExtended;

class Globimmo extends StandardFlux
{

    /**
     * UserService constructor.
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }


    /**
     * create rootNode
     * @return SimpleXMLExtended
     */
    function createRootNode()
    {
        $rootNode = new SimpleXMLExtended("<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
                    <globimmo></globimmo>");
        $rootNode->addAttribute("xmlns:g", "http://www.globimmo.net");
        $info = $rootNode->addChild('info');
        $info->addAttribute("version", "2.0");
        $info->addAttribute("techsupport", "support@globimmo.net");
        return $rootNode;
    }

    /**
     * check if the xml is not empty => no ads
     * @param SimpleXMLExtended $rootNode
     * @return int
     */
    function countNodes($rootNode)
    {
        return count($rootNode->xpath("agency"));
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addAgencyNode($group, &$rootNode)
    {
        $agency = $group->getAgency();
        $itemAgencyNode = $rootNode->addChild('agency');
        $itemInfoAgencyNode = $itemAgencyNode->addChild("agency_info");
        //agency information
        $itemInfoAgencyNode->addChildWithCDATA('agency_id', $agency->getId());
        $itemInfoAgencyNode->addChildWithCDATA('agency_name', $agency->getName());
        $itemInfoAgencyNode->addChildWithCDATA('agency_email',
            $group->getEmailAlias() ? $group->getEmailAlias() : $agency->getEmail());
        $itemInfoAgencyNode->addChild('agency_tel', $agency->getExportTelephone());
        $itemInfoAgencyNode->addChildWithCDATA('agency_country', 'France');
        $itemInfoAgencyNode->addChildWithCDATA('agency_city', $agency->getVille());
        $itemInfoAgencyNode->addChildWithCDATA('agency_zip', $agency->getCodePostal());
        $itemInfoAgencyNode->addChildWithCDATA('agency_street', $agency->getAdress());
        $itemInfoAgencyNode->addChildWithCDATA('agency_logo', $agency->getAvatar() ?
            $this->liipImagine->getBrowserPath('/uploads/ad/' . $agency->getAvatar(), 'poliris_image') : '');
        $itemObjectsNode = $itemAgencyNode->addChild("Objects");
        return $itemObjectsNode;
    }

    /**
     * @param Ad $ad
     * @param Portal $portal
     * @param SimpleXMLExtended $rootNode
     * @return mixed
     */
    function addNode($ad, $portal, &$rootNode)
    {
        if ($ad->getPublished() && $ad->getDiffusion()) {
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            $isTerrainPortal = $portal->getTypeDiffusion() == 'terrain' ? true : false;
            $diffusion = $ad->getTypeDiffusion();
            $isTerrain = $diffusion == 'terrain' ? true : false;
            $Terrain = $ad->getTerrain();
            $typeBien = 'RESIDENTIAL_LAND';
            $Maison = null;
            $appartement  = null;
            if ($ad->getTypeDiffusion() != 'terrain') {
                /** @var Maison $Maison * */
                $Maison = $ad->getMaison();
                $typeBien = 'HOUSE';
            }
            if( $ad->getProgram() && $ad->getProgram()->getType() == 'appartement') {
                $typeBien = 'STUDIO_APARTMENT';
                /** @var Appartement $appartement **/
                $appartement = $ad->getAppartement();
            }

            if ($isTerrainPortal && !$isTerrain) {
                return false;
            }

            $itemObjectNode = $rootNode->addChild("Object");
            //ad information
            $itemObjectNode->addChild('object_id', $ad->getId());
            $itemObjectNode->addChild('advert_type', "SALE");
            $itemObjectNode->addChild('object_type', $typeBien);
            $itemObjectPrice = $itemObjectNode->addChild("object_price", $ad->getPrix());
            $itemObjectPrice->addAttribute('currency', 'EUR');
            $itemObjectArea = $itemObjectNode->addChild("object_area", $isTerrain ?
                ($Terrain ? (int)$Terrain->getSurface() : 0) :
                ($appartement ? (int)$appartement->getSurface() :
                    ($Maison ? (int)$Maison->getSurface() : '')));
            $itemObjectArea->addAttribute('unitSystem', 'M');
            $itemObjectNode->addChild('object_Floor', '');
            $itemObjectNode->addChild( 'object_NoOfFloors', $appartement ?(int)$appartement->getNbEtages():
                ($Maison?(int)$Maison->getNbEtages():0)
            );
            $itemObjectNode->addChild('object_BuiltYear', $appartement ? date_format($appartement->getCreatedAt(), 'Y-m-d'):
                ($Maison ? date_format($Maison->getCreatedAt(), 'Y-m-d'): ''));
            $itemObjectNode->addChild('object_Condition', '');
            $itemObjectNode->addChild('object_Material', '');
            $itemObjectNode->addChild('object_Balcony', $appartement && $appartement->getSiBalcons() ? 'oui':
                ($Maison && $Maison->getSiBalcons() ? 'oui' : 'non'));
            $itemObjectNode->addChild('object_Cellar', $appartement && $appartement->getSiCave() ? 'oui':
                ($Maison && $Maison->getSiCave() ? 'oui' : 'non'));
            $itemObjectNode->addChild('object_Elevator', $appartement && $appartement->getSiAscenseur() ? 'oui' :'non');
            $itemObjectNode->addChild('object_AvailableFrom', '');
            $itemObjectNode->addChild('object_rentalPeriod', '');
            $itemObjectNode->addChild('object_maxPerson', '');
            $itemObjectNode->addChild('object_Furnishing', '');
            $itemObjectAdress = $itemObjectNode->addChild("object_Address");
            $itemObjectAdress->addChild('object_country', $ad->getPays() ? $ad->getPays() : '');
            $itemObjectAdress->addChild('object_city', $ad->getVille() ? $ad->getVille() : '');
            $itemObjectAdress->addChild('object_zip', $ad->getCodePostal() ? $ad->getCodePostal() : '');
            $itemObjectAdress->addChild('object_street', $ad->getAdresse() ? $ad->getAdresse() : '');
            $itemObjectAdress->addChild('object_lat', '');
            $itemObjectAdress->addChild('object_long', '');
            $itemObjectDescription = $itemObjectNode->addChild("object_descriptions",
                $ad->getDescription() ? $ad->getDescription() : '');
            $itemObjectDescription->addAttribute('lang', 'fr');


            $imagesNode = $itemObjectNode->addChild("object_images");
            $imageElm = $imagesNode->addChildWithCDATA('image',
                $ad->getImagePrincipale() ? $this->liipImagine->getBrowserPath($ad->getImagePrincipale(),
                    'poliris_image') : '');
            $imageElm->addAttribute("id", "1");
            $indexImage = 2;
            if ($ad->getImagesDocuments()) {
                foreach ($ad->getImagesDocuments() as $index => $imageDoc) {
                    $imageUrl = $imageDoc->getName() ?
                        $this->liipImagine->getBrowserPath('/uploads/ad/' . $imageDoc->getName(), 'poliris_image') :
                        '';
                    if ($imageUrl) {
                        $imageElm = $imagesNode->addChild('image', $imageUrl);
                        $imageElm->addAttribute("id", $indexImage++);
                    }
                }
            }

            $contactNode = $itemObjectNode->addChild("object_contact");
            $contactNode->addChild("agent_id", $ad->getOwner()->getId());
            $arr = explode(' ', trim($ad->getOwner()->getName()));
            $contactNode->addChild("agent_Firstname", $arr[0]);
            $contactNode->addChild("agent_Surname", $arr[1]);
            $contactNode->addChild("agent_email", $ad->getOwner()->getEmail());
            $contactNode->addChild("agent_tel", '');
            $contactNode->addChildWithCDATA("agent_photo", '');
        }
    }
}