<?php
/**
 * Created by PhpStorm.
 * User: anis
 * Date: 27/12/17
 * Time: 21:44
 */

namespace AppBundle\Service\Export;


use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class ExportFactory
{
    static $instanceList;

    /**
     * @param Container $container
     * @param $groups
     * @param $portalOrPortalRegistration
     * @param $exportPath
     * @param $fileName
     * @return bool
     */
    static function dispatch(Container $container, $groups, $portalOrPortalRegistration, $exportPath, $fileName){
        $namespace = "\\AppBundle\\Service\\Export\\";
        $portal = $portalOrPortalRegistration;
        if($portalOrPortalRegistration instanceof PortalRegistration)
            $portal = $portalOrPortalRegistration->getBroadcastPortal();
        $portalType = $portal->getType();
        $className = $namespace.ucfirst($portalType);
        if(class_exists($className)) {
            if(!isset(self::$instanceList[$portalType])){
                self::$instanceList[$portalType] = new $className($container);

            }
            self::$instanceList[$portalType]->setExportPath($exportPath);
            self::$instanceList[$portalType]->setFileName($fileName);
            self::$instanceList[$portalType]->generate($groups, $portalOrPortalRegistration);
        }
        return false;
    }

}