<?php

namespace AppBundle\Service;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\AgendaEventReminder;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\BugComment;
use AppBundle\Entity\Payment\AuthorizedAgency;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\User;
use AppBundle\Repository\AdRepository;
use AppBundle\Repository\Broadcast\PortalRegistrationRepository;
use AppBundle\Repository\Gestion\AgendaEventsRepository;
use AppBundle\Repository\Gestion\BugCommentRepository;
use AppBundle\Repository\Gestion\BugsRepository;
use AppBundle\Repository\Gestion\DiscussionRepository;
use AppBundle\Repository\Gestion\OpportunityCallRepository;
use AppBundle\Repository\Gestion\OpportunityRepository;
use AppBundle\Repository\HouseModelRepository;
use AppBundle\Repository\Payment\AuthorizedAgencesRepository;
use AppBundle\Repository\Payment\AuthorizedUsersRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class NotificationManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var AdRepository
     */
    private $adRepository;
    /**
     * @var OpportunityRepository
     */
    private $opportunityRepository;

    /**
     * @var PortalRegistrationRepository
     */
    private $portalRegistrationRepository;
    /**
     * @var HouseModelRepository
     */
    private $modelRepository;

    /**
     * @var User
     */
    private $user;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @param EntityManager $entityManager
     * @param $token
     * @param AuthorizationChecker $authorizationChecker
     * @param Router $router
     * @param Translator $translator
     */
    public function __construct(
        EntityManager $entityManager,
        $token,
        AuthorizationChecker $authorizationChecker,
        Router $router,
        Translator $translator
    )
    {
        $this->entityManager                = $entityManager;
        $this->adRepository                 = $entityManager->getRepository('AppBundle:Ad');
        $this->modelRepository              = $entityManager->getRepository('AppBundle:HouseModel');
        $this->opportunityRepository        = $entityManager->getRepository('AppBundle:Gestion\\Opportunity');
        $this->portalRegistrationRepository = $entityManager->getRepository('AppBundle:Broadcast\\PortalRegistration');
        $this->authorizationChecker         = $authorizationChecker;
        $this->router                       = $router;
        $this->translator                   = $translator;

        if (null === $token) { $this->user = null; }
        if ($token && !is_object($this->user = $token->getUser())) { $this->user = null; }
    }

    public function getNotifications() {
        $links = [];
        if(!$this->user) return null;

        if($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $waitingPortalsRegistrations = $this->portalRegistrationRepository->getWaitingPortalRegistrationCount();
            if($waitingPortalsRegistrations > 0) {
                $links[] = [
                    'label' => $this->translator->trans('Portail'),
                    'text'  => $this->translator->trans('%value% Portails en attente',['%value%' => $waitingPortalsRegistrations]),
                    'url'   => $this->router->generate('portails_waiting')
                ];
            }

            /** @var BugCommentRepository $bugCommentRepository */
            $bugCommentRepository = $this->entityManager->getRepository('AppBundle:Gestion\\BugComment');
            $messages = $bugCommentRepository->getWaitingBugMessages($this->user);
            if($messages) {
                /** @var BugComment $message */
                foreach ($messages as $message) {
                    $url = $this->router->generate('gestion_bugs_comments',['remoteId' => $message->getBug()->getRemoteId()]);
                    $links[] = [
                        'label' => $this->translator->trans('Bug'),
                        'text'  => $this->translator->trans('Vous avez un nouveau message - "%value%"',['%value%' => $message->getBug()->getTitle()]),
                        'url'   => $url
                    ];
                }
            }


            /** @var BugsRepository $bugRepository */
            $bugRepository = $this->entityManager->getRepository('AppBundle:Gestion\\Bug');
            $bugs = $bugRepository->getWaitingBug($this->user);
            if($bugs) {
                /** @var Bug $bug */
                foreach ($bugs as $bug) {
                    $url     = $this->router->generate('gestion_bugs_view',['remoteId' => $bug->getRemoteId()]);
                    $links[] = [
                        'label' => $this->translator->trans('Bug'),
                        'text'  => $this->translator->trans('Vous avez un bug reporter - "%value%"',['%value%' => $bug->getTitle()]),
                        'url'   => $url
                    ];
                }
            }

        } else {
            //SHARED NOTIFICATIONS

            /** @var OpportunityRepository $opportunityRepository */
            $opportunityRepository = $this->entityManager->getRepository('AppBundle:Gestion\\Opportunity');
            $waitingOpportunity = $opportunityRepository->getWaitingsOpportunity($this->user);
            if($waitingOpportunity > 0) {
                $links[] = [
                    'label' => $this->translator->trans('Opportunité'),
                    'text'  => $this->translator->trans('%value% opportunité en attente',['%value%' => $waitingOpportunity]),
                    'url'   => $this->router->generate('gestion_opportunity_list')
                ];
            }

            /** @var DiscussionRepository $discussionRepository */
            $discussionRepository = $this->entityManager->getRepository('AppBundle:Gestion\\Discussion');
            $waitingInboxMessages = $discussionRepository->unreadedMessages($this->user);
            if($waitingInboxMessages > 0) {
                $links[] = [
                    'label' => $this->translator->trans('E-mails'),
                    'text'  => $this->translator->trans('%value% message(s) en attente',['%value%' => $waitingInboxMessages]),
                    'url'   => $this->router->generate('gestion_mailbox_inbox')
                ];
            }

            //CUSTOM NOTIFICATIONS
            if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {

                /** @var BugCommentRepository $bugCommentRepository */
                $bugCommentRepository = $this->entityManager->getRepository('AppBundle:Gestion\\BugComment');
                $messages = $bugCommentRepository->getWaitingBugMessages($this->user);
                if($messages) {
                    /** @var BugComment $message */
                    foreach ($messages as $message) {
                        $url = $this->router->generate('gestion_bugs_comments',['remoteId' => $message->getBug()->getRemoteId()]);
                        $links[] = [
                            'label' => $this->translator->trans('Bug'),
                            'text'  => $this->translator->trans('Vous avez un nouveau message - "%value%"',['%value%' => $message->getBug()->getTitle()]),
                            'url'   => $url
                        ];
                    }
                }

                /** @var AuthorizedUsersRepository $authorizedUsersRepository */
                $authorizedUsersRepository = $this->entityManager->getRepository('AppBundle:Payment\\AuthorizedUser');
                $expireUserPayment     = $authorizedUsersRepository->getSoonExpireAuthorization($this->user);
                if($expireUserPayment) {
                    /** @var AuthorizedUser $authorizedUser */
                    foreach ($expireUserPayment as $authorizedUser) {
                        $url = $this->user->getGroup()->hasRole('ROLE_GROUP') ? $this->router->generate('group_agency_show',['remoteId' => $authorizedUser->getGroup()->getRemoteId()])  : $this->router->generate('agency_my');
                        $links[] = [
                            'label' => $this->translator->trans('Expire'),
                            'text'  => $this->translator->trans('La session de l\'utilisateur "%value%" va bientôt expiré',['%value%' => $authorizedUser->getUser()->getName()]),
                            'url'   => $url
                        ];
                    }
                }

                /** @var AuthorizedAgencesRepository $authorizedAgencesRepository */
                $authorizedAgencesRepository = $this->entityManager->getRepository('AppBundle:Payment\\AuthorizedAgency');
                $expireAgencyPayment     = $authorizedAgencesRepository->getSoonExpireAuthorization($this->user);
                /** @var AuthorizedAgency $authorizedAgency */
                foreach ($expireAgencyPayment as $authorizedAgency) {
                    $url = $this->user->getGroup()->hasRole('ROLE_GROUP') ? $this->router->generate('group_agency_show',['remoteId' => $authorizedAgency->getGroup()->getRemoteId()])  : $this->router->generate('agency_my');
                    $links[] = [
                        'label' => $this->translator->trans('Expire'),
                        'text'  => $this->translator->trans('La session de l\'agence "%value%" va bientôt expiré',['%value%' => $authorizedAgency->getGroup()->getName()]),
                        'url'   => $url
                    ];
                }
            } elseif($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {

                $waitingAds = $this->adRepository->getWaitingsAds($this->user);
                if($waitingAds > 0) {
                    $links[] = [
                        'label' => $this->translator->trans('Annonces'),
                        'text'  => $this->translator->trans('%value% annonces en attente',['%value%' => $waitingAds]),
                        'url'   => $this->router->generate('ads_moderation')
                    ];
                }
                $waitingModels = $this->modelRepository->getWaitingsModels($this->user);
                if($waitingModels > 0) {
                    $links[] = [
                        'label' => $this->translator->trans('Modèle'),
                        'text'  => $this->translator->trans('%value% modèles en attente',['%value%' => $waitingModels]),
                        'url'   => $this->router->generate('housemodels_moderation_list')
                    ];
                }

            } elseif($this->user->hasRole('ROLE_SALESMAN')) {
                /** @var OpportunityCallRepository $opportunityCallRepository */
                $opportunityCallRepository = $this->entityManager->getRepository('AppBundle:Gestion\\OpportunityCall');
                $waitingCallsOpportunity = $opportunityCallRepository->getWaitingsCallOpportunity($this->user);
                if($waitingCallsOpportunity > 0) {
                    $links[] = [
                        'label' => $this->translator->trans('Appel'),
                        'text'  => $this->translator->trans('%value% appel(s) à venir',['%value%' => $waitingCallsOpportunity]),
                        'url'   => $this->router->generate('gestion_opportunity_list')
                    ];
                }

                /** @var AgendaEventsRepository $agendaEventsRepository */
                $agendaEventsRepository = $this->entityManager->getRepository('AppBundle:Gestion\\AgendaEvent');
                $waitingMeetingsOpportunity      = $agendaEventsRepository->getWaitingsEvents($this->user);
                if($waitingMeetingsOpportunity > 0) {
                    $links[] = [
                        'label' => $this->translator->trans('Rendez-vous'),
                        'text'  => $this->translator->trans('%value% rendez-vous à venir',['%value%' => $waitingMeetingsOpportunity]),
                        'url'   => $this->router->generate('gestion_opportunity_list')
                    ];
                }
            }

            // get pending actions notifcations
            $notificationRepository = $this->entityManager->getRepository("AppBundle:Notification");
            $waitingList = $notificationRepository->getWaitingMessages($this->user);
            /** @var \AppBundle\Entity\Notification $waitingItem **/
            foreach ($waitingList as $waitingItem){
                $links[] = [
                    'label' => $this->translator->trans(
                        $waitingItem->getSubject(),
                        $waitingItem->getMessageParams(),
                        "commiti"),
                    'text'  => $this->translator->trans(
                        $waitingItem->getMessage(),
                        $waitingItem->getMessageParams(),
                        "commiti"
                    ),
                    'url'   => $this->router->generate('notification_pull', ['id' => $waitingItem->getId()])
                ];
            }

        }

        return $links;
    }
}