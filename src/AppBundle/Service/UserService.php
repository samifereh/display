<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\Document;

use AppBundle\Repository\UserRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class UserService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var  \AppBundle\Entity\User $user */
    private $user;

    /** @var  \AppBundle\Repository\UserRepository $userRepository */
    private $userRepository;
    private $security;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     * @param UserRepository $userRepository
     * @param $user
     */
    public function __construct(EntityManager $entityManager,UserRepository $userRepository,$security, $user)
    {
        $this->entityManager      = $entityManager;
        $this->user               = $user;
        $this->userRepository     = $userRepository;
        $this->security           = $security;
    }

    public function checkCommercialUser(&$request, $group = null) {
        $commercial = $this->user;
        if(is_null($group)) { $group = $this->user->getAgencyGroup(); }
        if(!$this->user->hasRole('ROLE_SALESMAN')){
            $commercialId = $request->cookies->get('write_as_commercial');
            if(!$commercialId) { return null; }
            $commercial = $this->userRepository->findOneBy( ['remoteId' => $commercialId] );
//            if(!$commercial || $group == 'all' || ($commercial && !$commercial->isEmployeeOf($group))) { return null; }
        }
        return $commercial;
    }

    public function checkUserGroup(&$request) {
        $parentGroup = $this->user->getGroup();
        if($this->security->isGranted('ROLE_MARKETING')){
            return null;
            $groupRemoteId = $request->cookies->get('use_group');
            if($groupRemoteId) {
                $parentGroup =  $this->entityManager->getRepository('AppBundle:Group')->findOneBy(['remoteId' => $groupRemoteId]);
            } else {
                $parentGroup = null;
            }
        }
        return $parentGroup;
    }

    public function checkUserAgency(&$request, $full = false) {
        if(!$this->user->getGroup())
            return null;
        return $this->user->getGroup()->hasRole('ROLE_AGENCY') ? $this->user->getGroup() : null;

        $parentGroup = $this->checkUserGroup($request);
        $agencyRemoteId = $request->cookies->get('use_agency');
        if($agencyRemoteId == 'all' && $full) { return 'all'; }
        $agency         = null;

        if($agencyRemoteId) {
            $agency = $this->entityManager->getRepository('AppBundle:Group')->findOneBy(['parentGroup' => $parentGroup, 'remoteId'=>$agencyRemoteId]);
            if(!$agency && $this->security->isGranted('ROLE_ADMIN')) {
                $agency = $this->entityManager->getRepository('AppBundle:Group')->getFirstAgencyGroups($parentGroup);
            }
        }
        if(!$agency)
            return 'all';

        return $agency;
    }

    /**
     * check Commercial for statistic filters
     * @param $request
     * @return \AppBundle\Entity\User|null|object
     */
    public function checkStatCommercialUser(&$request) {
        $commercial = null;
        $commercialId = $request->request->get('stat_filter_commercial');
        if($commercialId)
            $commercial = $this->userRepository->findOneBy( ['remoteId' => $commercialId] );
        return $commercial;
    }

    /**
     * @param Request $request
     * @return \AppBundle\Entity\Group|mixed|null|object
     */
    public function checkStatUserGroup(&$request) {
        $groupRemoteId = $request->request->get('stat_filter_group');
        $parentGroup = null;
        if($groupRemoteId) {
            $parentGroup =  $this->entityManager->getRepository('AppBundle:Group')->findOneBy(['remoteId' => $groupRemoteId]);
        }
        return $parentGroup;
    }

    /**
     * @param Request $request
     * @return \AppBundle\Entity\Group|mixed|null|object
     */
    public function checkStatUserAgency(&$request) {

        $parentGroup = $this->checkStatUserGroup($request);
        $agencyRemoteId = $request->request->get('stat_filter_agency');

        $agency         = null;

        if($agencyRemoteId) {
            $agency = $this->entityManager->getRepository('AppBundle:Group')->findOneBy(['parentGroup' => $parentGroup, 'remoteId'=>$agencyRemoteId]);
        }

        return $agency;
    }

}