<?php

namespace AppBundle\Service;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\AgendaEventReminder;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class AgendaManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /** @var \AppBundle\Repository\Gestion\AgendaEventRemindersRepository $agendaEventRemindRepository */
    private $agendaEventRemindRepository;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(
        Container $container,
        EntityManager $entityManager
    )
    {
        $this->container                    = $container;
        $this->entityManager                = $entityManager;
        $this->agendaEventRemindRepository  = $entityManager->getRepository('AppBundle:Gestion\\AgendaEventReminder');
    }

    public function getAgendaReminderFromEvent(AgendaEvent $agendaEvent, AgendaEventReminder $agendaEventReminder = null) {

        $unit = substr($agendaEvent->getUnit(), 0, 1);
        $date = $agendaEvent->getDateStart();
        if($unit == 'H') {
            $dateInterval = new \DateInterval('PT'.$agendaEvent->getTemps().$unit);
        } else {
            $dateInterval = new \DateInterval('P'.$agendaEvent->getTemps().$unit);
        }
        $date->sub($dateInterval);

        if(!$agendaEventReminder)
            $agendaEventReminder = new AgendaEventReminder();

        $agendaEventReminder->setDate($date);
        $agendaEventReminder->setAgendaEvent($agendaEvent);

        return $agendaEventReminder;
    }

    public function sendAgendaRemind() {
        $eventsReminders = $this->agendaEventRemindRepository->getWaitingsRemindEvents();
        /** @var \AppBundle\Entity\Gestion\AgendaEventReminder $eventReminder */
        foreach ($eventsReminders as $eventReminder) {
            $agendaEvent = $eventReminder->getAgendaEvent();
            $toList = [$agendaEvent->getOwner()];
            if($agendaEvent->getSharedUsers()->count()) {
                    /** @var \AppBundle\Entity\User $user */
                    foreach ($agendaEvent->getSharedUsers() as $user)
                        $toList[] = $user;
            }
            $toList = array_unique($toList);
            foreach ($toList as $to) {

                $this->container->get("app.mailer.manager")->sendAgendaReminder(['user' => $to, 'event' => $agendaEvent]);

                $eventReminder->setStatus(AgendaEventReminder::COMPLETE);
                $this->entityManager->persist($agendaEvent);
                $this->entityManager->flush();
            }
        }
    }
}