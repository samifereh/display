<?php
// src/AppBundle/Service/CitiesListService.php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

class CitiesListService
{

    private $entityManager;

    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
    }

    public function findCity($by, $stuff){

        switch ($by) {
            case 'name':
                return $this->getCitiesByName($stuff); break;

            case 'postalCode':
                return $this->getCitiesByPostalCode($stuff); break;

            default:
                return $this->getCountries(); break;
        }

    }

    public function getCountries()
    {

        return new JsonResponse(array(
            "France"
        ));

    }

    public function getCitiesByName($name){

        return new JsonResponse($this->entityManager->fetchAll(
            'SELECT c.ville_departement as dept, c.ville_nom_reel as name, c.ville_code_postal as postalcode FROM villes_france c '.
            'WHERE c.ville_nom LIKE UPPER(%?%) OR c.ville_nom_simple LIKE LOWER(%?%) OR c.ville_nom_reel LIKE %?%',
            array($name, $name, $name)
        ));

    }

    public function getCitiesByPostalCode($postalCodes){

        return new JsonResponse($this->entityManager->fetchAll(
            'SELECT c.ville_departement as dept, c.ville_nom_reel as name, c.ville_code_postal as postalcode FROM villes_france c '.
            'WHERE c.ville_code_postal LIKE ?',
            array($name)
        ));

    }

}