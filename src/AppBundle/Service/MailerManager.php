<?php
namespace AppBundle\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class MailerManager
{
    CONST LIMIT = 10;
    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var  \AppBundle\Entity\User $user */
    private $user;


    private $translator;


    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     * @param $user
     */
    public function __construct(Container $container, EntityManager $entityManager, TokenStorage $tokenStorage )
    {
        $this->container      = $container;
        $this->entityManager      = $entityManager;
        $this->user             = (
            $tokenStorage->getToken() && $tokenStorage->getToken()->getUser() ?
                $tokenStorage->getToken()->getUser() : null
        ) ;
        $this->translator = $this->container->get('translator');
        $this->mailer = $this->container->get('mailer');
    }


    /**
     * send PortalRegistration request for admin and the owner
     * @param $params
     *
     */
    public function sendPortalRequestRegistration($params){
        $this->send(
            "portal.registration_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $this->container->getParameter("mailer_sender"),
            'AppBundle:Emails/Portal:request_portal_registration_admin.html.twig',
            $params
        );

        $this->send(
            "portal.registration_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $this->user->getEmail(),
            'AppBundle:Emails/Portal:request_portal_registration.html.twig',
            $params
        );

    }

    /** send when status is changed or Enblaed
     * @param $params
     */
    function sendPortalEditStatutRegistration($params){

        // check if Registration has owner
        if(
            $params['portalRegistration']->getOwner() &&
            $params['portalRegistration']->getOwner()->getEmail()
        )
            $owner = $params['portalRegistration']->getOwner();
        else{ // else check brandleader

            $userRepository = $this->entityManager->getRepository('AppBundle:User');
            $chef = $userRepository->findByRole('ROLE_BRANDLEADER', $params['portalRegistration']->getGroup());
            $owner = $chef;
        }
        $params["owner"] = $owner;
        $this->send(
            "portal.registration_change_status_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $owner->getEmail(),
            'AppBundle:Emails/Portal:status_portal_registration.html.twig',
            $params
        );

    }

    /**
     * send for creation new model to approve
     * @param $params
     */
    function sendRequestModelAdd($params){
        $this->send(
            "user.moderate_model_subject",
            $this->container->getParameter("mailer_reply"),
            $params['chef']->getEmail(),
            'AppBundle:Emails/Model:request_model.html.twig',
            $params
        );
    }

    /**
     * Moderation ok
     * @param $params
     */
    function sendModelModerateOk($params){
        $this->send(
            "user.validate_model_subject",
            $this->container->getParameter("mailer_reply"),
            $params['model']->getOwner()->getEmail(),
            'AppBundle:Emails/Model:modelRequestOk.html.twig',
            $params
        );
    }

    /**
     * Moderation Reply
     * @param $params
     */
    function sendModelModerateReply($params){
        $this->send(
            "model.reply_has_been_sent_mail_subject",
            $params['chef']->getEmail(),
            $params['model']->getOwner()->getEmail(),
            'AppBundle:Emails/Model:model_notification_commercial.html.twig',
            $params
        );
    }

    /**
     * send for creation new ad to approve
     * @param $params
     */
    function sendRequestAdCreate($params){
        $this->send(
            "ads.moderation_create_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['chef']->getEmail(),
            'AppBundle:Emails/Ads:request_create.html.twig',
            $params
        );
    }

    /**
     * send for creation new ad to approve
     * @param $params
     */
    function sendCommentAd($params){
        $this->send(
            "ads.response_comment_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['ad']->getOwner()->getEmail(),
            'AppBundle:Emails/Ads:response_comment.html.twig',
            $params
        );
    }

    /**
     * send moderation approve
     * @param $params
     */
    function sendAdModerateOk($params){
        $this->send(
            "ads.moderation_ok_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['ad']->getOwner()->getEmail(),
            'AppBundle:Emails/Ads:moderation_ok.html.twig',
            $params
        );
    }

    /**
     * send notif moderation created
     * @param $params
     */
    function sendNotifModerateCreated($params){
        $this->send(
            "program.notif_moderation_created_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['chef']->getEmail(),
            'AppBundle:Emails/Program:request_program_moderation.html.twig',
            $params
        );

        $this->send(
            "program.notif_moderation_created_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['program']->getOwner()->getEmail(),
            'AppBundle:Emails/Program:notif_user_program_created.html.twig',
            $params
        );
    }

    /**
     * send program moderation approved
     * @param $params
     */
    function sendProgramModerateOk($params){
        $this->send(
            "program.moderation_ok_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['program']->getOwner()->getEmail(),
            'AppBundle:Emails/Program:moderation_ok.html.twig',
            $params
        );
    }

    /**
     * send program moderation pending
     * @param $params
     */
    function sendProgramModeratePending($params){
        $this->send(
            "program.moderation_waiting_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['program']->getOwner()->getEmail(),
            'AppBundle:Emails/Program:moderation_pending.html.twig',
            $params
        );
    }

    /**
     * send program moderation canceled
     * @param $params
     */
    function sendProgramModerateCanceled($params){
        $this->send(
            "program.moderation_canceled_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['program']->getOwner()->getEmail(),
            'AppBundle:Emails/Program:moderation_canceled.html.twig',
            $params
        );
    }

    /**
     * send comment program
     * @param $params
     */
    function sendCommentProgram($params){
        $this->send(
            "action.send_comment_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['program']->getOwner()->getEmail(),
            'AppBundle:Emails/Program:response_comment.html.twig',
            $params
        );
    }


    /**
     * send create new bug ticket
     * @param $params
     */
    function sendBugCreate($params){
        $this->send(
            "bugs.new_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['bug']->getOwner()->getEmail(),
            'AppBundle:Emails/Bugs:new_ticket.html.twig',
            $params
        );
        $this->send(
            "bugs.new_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $this->container->getParameter("mailer_sender"),
            'AppBundle:Emails/Bugs:new_ticket_admin.html.twig',
            $params
        );
    }

    /**
     * send bug response
     * @param $params
     */
    function sendBugResponse($params){
        $this->send(
            "bugs.response_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['bug']->getOwner()->getEmail(),
            'AppBundle:Emails/Bugs:response_ticket.html.twig',
            $params
        );

    }
    
    /**
 * send bug response
 * @param $params
 */
    function sendBugChangeStatus($params){
        $this->send(
            "bugs.change_status_mail_subject",
            $this->container->getParameter("mailer_reply"),
            $params['bug']->getOwner()->getEmail(),
            'AppBundle:Emails/Bugs:change_status_ticket.html.twig',
            $params
        );
    }

    /**
     * send suscribe notif
     * @param $params
     */
    function sendNewAccount($params){
        $this->send(
            "user.subscribe.subject.mail",
            $this->container->getParameter("mailer_reply"),
            $params['user']->getEmail(),
            'AppBundle:Emails/Users:notif_new_account.html.twig',
            $params
        );
    }


    /**
     * send notify new message received
     * @param $params
     */
    function sendNewMessageReceived($params){
        $this->send(
            "mail.mail_received_subject",
            $this->container->getParameter("mailer_reply"),
            $params['recipient']->getEmail(),
            'AppBundle:Emails/Message:new.html.twig',
            $params
        );

    }

     /**
     * send notify new message received
     * @param $params
     */
    function sendAgendaReminder($params){
        $this->send(
            "agenda.reminder.mail.subject",
            $this->container->getParameter("mailer_reply"),
            $params['user']->getEmail(),
            'AppBundle:Emails/Agenda:reminder.html.twig',
            $params
        );

    }

    /**
     * send notify new invoice
     * @param $params
     */
    function sendNewInvoice($params){
        $this->send(
            "invoice.mail.subject",
            $this->container->getParameter("mailer_reply"),
            $params['email'],
            'AppBundle:Emails/Invoice:new.html.twig',
            $params
        );

    }

    /**
     * send notify Order request
     * @param $params
     */
    function sendOrderReceived($params){
        $this->send(
            "invoices.order_received_mail_subjet",
            $this->container->getParameter("mailer_reply"),
            $params['email'],
            'AppBundle:Emails/Order:received.html.twig',
            $params
        );

    }

    /**
     * send notify Order payment confirm
     * @param $params
     */
    function sendOrderPaymentOk($params){
        $this->send(
            "invoices.payment_activation_mail_subjet",
            $this->container->getParameter("mailer_reply"),
            $params['email'],
            'AppBundle:Emails/Order:payment_confirmed.html.twig',
            $params
        );

    }

    /**
     * send notify new opportunity
     * @param $params
     */
    function sendNewOpportunity($params){
        $this->send(
            "opportunity.received_subject",
            $this->container->getParameter("mailer_reply"),
            $params['opportunity']->getCommercial()->getEmail(),
            'AppBundle:Emails/Opportunity:new.html.twig',
            $params
        );

    }

    /**
     * send confirm notif to confirm password update
     * @param $params
     */
    function sendEmailPasswordUpdateOk($params){
        $this->send(
            "user.password.update.subject.mail",
            $this->container->getParameter("mailer_reply"),
            $params['user']->getEmail(),
            'AppBundle:Emails/Users:confirm_update_password.html.twig',
            $params
        );
    }

    /**
     * send notif for receiving news
     * @param $params
     */
    function sendEmailReceiveNews($params){
        $this->send(
            "dashboard.news.subject.mail",
            $this->container->getParameter("mailer_reply"),
            $params['user']->getEmail(),
            'AppBundle:Emails/News:receiving_news.html.twig',
            $params
        );
    }

    /**
     * send notif for receiving FAQ
     * @param $params
     */
    function sendEmailReceiveNewFAQ($params){
        $this->send(
            "faq.new.faq.subject.mail",
            $this->container->getParameter("mailer_reply"),
            $params['user']->getEmail(),
            'AppBundle:Emails/FAQ:receiving_new_FAQ.html.twig',
            $params
        );
    }

    function processNews($newsId){
        $news = $this->entityManager->getRepository("AppBundle:News")->find($newsId);
        if($news) {
            //Send Notify Email
            $userManager = $this->entityManager->getRepository('AppBundle:User');
            $offset = 0;

            while ($users = $userManager->findBy([], null, self::LIMIT, $offset)) {
                while ($user = array_shift($users)) {
                    $this->sendEmailReceiveNews([
                        'user' => $user,
                        'news' => $news
                    ]);
                }
                $offset = ($offset+1)* self::LIMIT;
            }

        }
        return true;
    }

    function processFAQ($faqId){
        $faq = $this->entityManager->getRepository("AppBundle:Gestion\FAQ")->find($faqId);
        if($faq) {
            //Send Notify Email
            $userManager = $this->entityManager->getRepository('AppBundle:User');
            $offset = 0;

            while ($users = $userManager->findBy([], null, self::LIMIT, $offset)) {
                while ($user = array_shift($users)) {
                    $this->sendEmailReceiveNewFAQ([
                        'user' => $user,
                        'faq' => $faq
                    ]);
                }
                $offset = ($offset+1)* self::LIMIT;
            }

        }
        return true;
    }
    /** function called from PendingActionsCommand
     * @param $args
     * @return bool
     */
    function execute($args)
    {
        if(isset($args["contentId"]) && isset($args["action"]) ){

            switch ($args["action"]) {
                case "news" :
                    return $this->processNews($args["contentId"]);break;
                case "faq" :
                    return $this->processFAQ($args["contentId"]);break;
            }
        }
        return true;
    }

    /**
     * send notify Order Option expiry
     * @param $params
     */
    function sendAlertPaymentExpiry($params){
        $this->send(
            "invoices.order_expiry_alert",
            $this->container->getParameter("mailer_reply"),
            $params['email'],
            'AppBundle:Emails/Invoice:notif_alerte_non_renouv_option.html.twig',
            $params
        );

    }

    /**
     * generic sender
     * @param $subject
     * @param $from
     * @param $to
     * @param $view
     * @param $params
     */
    function send($subject, $from, $to, $view, $params){

        $message = new \Swift_Message();
        $message->setSubject(
            $this->translator->trans($subject, [], 'commiti')
        )
            ->setFrom($from, 'Display-immo')
            ->setTo($to)
            ->setBody(
                $this->renderView( $view, $params ),
                'text/html');
        $this->mailer->send($message);

    }

    /**
     * render mail tpl view
     * @param $view
     * @param array $parameters
     * @return mixed|string
     */
    public function renderView($view, array $parameters = array())
    {
        if ($this->container->has('templating')) {
            return $this->container->get('templating')->render($view, $parameters);
        }

        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "renderView" method if the Templating Component or the Twig Bundle are not available.');
        }

        return $this->container->get('twig')->render($view, $parameters);
    }
}