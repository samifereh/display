<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\Document;

use AppBundle\Repository\UserRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Doctrine\ORM\EntityManager;

class Payment
{
    /**
     * 'ROLE_SALESMAN', 'ROLE_BRANDLEADER', 'ROLE_MARKETING', 'ROLE_DIRECTOR'
     */
    const PACK_1 = [
        'ROLE_DIRECTOR'     => 1,
        'ROLE_MARKETING'    => 1,
        'ROLE_BRANDLEADER'  => 1,
    ];

    const PACK_GROUP = [
        'ROLE_DIRECTOR'     => 1,
        'ROLE_MARKETING'    => 1,
    ];
    const PACK_GROUP_AGENCY = [
        'ROLE_BRANDLEADER'  => 1,
    ];

}