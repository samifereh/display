<?php

namespace AppBundle\Service;

use AppBundle\Entity\Dashboard;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class StatistiquesService
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /** @var  \AppBundle\Entity\User $user */
    private $user;

    private $security;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     * @param $user
     */
    public function __construct(Container $container, EntityManager $entityManager,AuthorizationChecker $security, $user)
    {
        $this->container      = $container;
        $this->entityManager      = $entityManager;
        $this->security           = $security;
        $this->user               = $user;
    }

    public function getDashboardStatistique(Request $request) {
        $userService = $this->container->get("app.user.service");
        $user = $userService->checkStatCommercialUser($request);
        $group = null;
        $agency = null;

        // not user filter => check group or agency filter
        if(!$user) {
            // set current user
            $user  = $this->user;
            // check agency filter
            $agency = $userService->checkStatUserAgency($request);

            if(!$agency) {
                // check if group filter
                $group = $userService->checkStatUserGroup($request);
                if ($group == $user->getGroup()) {
                    $group = null;
                }
            }


        }else{
            //@todo check rules
        }

        $contactRepository = $this->entityManager->getRepository('AppBundle:Gestion\\Contact');
        $contactCount  = $contactRepository->getContactCountByUser($user, $agency, $group);

        /**
         * @todo
         */
        $discussionRepository = $this->entityManager->getRepository('AppBundle:Gestion\\Discussion');
        $waitingInboxMessages = $discussionRepository->unreadedMessages($user, $agency, $group);

        $adRepository   = $this->entityManager->getRepository('AppBundle:Ad');
        $adStatistiques = $adRepository->getAdsStatistique($user, $agency, $group);
        $adAchivementStatistics = $adRepository->getAdsAchivementStatistics($user, $agency, $group);
        $adDiffusionStatistics = $adRepository->getAdsDiffusionStatistics($user, $agency, $group);

        $houseModelRepository   = $this->entityManager->getRepository('AppBundle:HouseModel');
        $houseModelStatistiques = $houseModelRepository->getStatistique($user, $agency, $group);

        $diffusionPortalRepository   = $this->entityManager->getRepository('AppBundle:Broadcast\Diffusion');
        $diffusionPortalStatistiques = $diffusionPortalRepository->getPortalDiffusionStatistics($user, $agency, $group);



        $dashRepo = $this->entityManager->getRepository('AppBundle:Dashboard');
        $dashboard = $dashRepo->findOneBy([]);
        if(!$dashboard)
            $dashboard = new Dashboard();
        //Latest Actuality
        $repo = $this->entityManager->getRepository('AppBundle:News');
        $limit = $this->container->getParameter("dashboard")['news'];
        if($dashboard && $dashboard->getNbNews())
            $limit = $dashboard->getNbNews();

        $newsList = $repo->getLatestNews($limit);


        $events = $this->entityManager->getRepository('AppBundle:Gestion\\AgendaEvent')->getEventsByUser($user, true);

        $opportunityRepository   = $this->entityManager->getRepository('AppBundle:Gestion\Opportunity');

        $opportunityCount     =  $opportunityRepository->getOpportunityCountByStatus($user, $agency, $group);
        $opportunityReceived = $opportunityRepository->getReceivedStatistics($user, $agency, $group);
        $opportunityTransform = $opportunityRepository->getTransformStatistics($user, $agency, $group);

        $contactStatisticsRepo = $this->entityManager->getRepository("AppBundle:Gestion\ContactStatistics");
        $contactStats = $contactStatisticsRepo->getContactStatistics($user, $agency, $group);
        $topAds = $contactStatisticsRepo->getTopAds($user, $agency, $group);


        return [
            'contactCount'              => $contactCount,
            'contactStats'              => $contactStats,
            'topAds'                    => $topAds,
            'waitingInboxMessages'      => $waitingInboxMessages,
            'adStatistiques'            => $adStatistiques,
            'adAchivementStatistics'    => $adAchivementStatistics,
            'adDiffusionStatistics'     => $adDiffusionStatistics,
            'diffusionPortalStatistiques'=> $diffusionPortalStatistiques,
            'houseModelStatistiques'    => $houseModelStatistiques,
            'latest_news'               => $newsList,
            'events'                    => $events,
            'dashboard'                 => $dashboard,
            'opportunityCount'          => $opportunityCount,
            'opportunityReceived'       => $opportunityReceived,
            'opportunityTransform'      => $opportunityTransform,
            'user'                      => $user,
            'hasSuiviOption'            => $user->hasSuiviOption()
        ];
    }


}