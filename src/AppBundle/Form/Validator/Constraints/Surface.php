<?php
namespace AppBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 9/18/16
 * Time: 3:58 PM
 */
class Surface extends Constraint
{
    public $message = 'Ce champ est obligatoire';
    public $messageNumber = 'Veuillez saisir une surface valide';

}