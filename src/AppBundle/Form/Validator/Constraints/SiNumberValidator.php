<?php

namespace AppBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 9/18/16
 * Time: 3:56 PM
 */
class SiNumberValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value['si'] && $value['number'] < 1) {
            $this->context->buildViolation($constraint->messageNumber)->addViolation();
        } elseif(!is_int($value['si'])) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}