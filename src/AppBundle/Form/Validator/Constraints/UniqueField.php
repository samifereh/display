<?php
namespace AppBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class UniqueField extends Constraint
{
    public $message = 'La valeur choisie est déjà utilisé';
    public $repository;
    public $field_name;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->repository || null === $this->field_name) {
            throw new MissingOptionsException('Either option "repository" and "field_name" must be given for constraint',['repository','field_name']);
        }



    }

}