<?php

namespace AppBundle\Form\Validator\Constraints;

use AppBundle\CheckoutBundle\Repository\DiscountRepository;
use AppBundle\Repository\GroupRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class UniqueFieldValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        $repository    = $this->entityManager->getRepository($constraint->repository);
        $currentEntity = $this->context->getRoot()->getViewData();
        $instance      = $repository->findOneBy([$constraint->field_name => $value]);
        if($instance && $instance->getId() != $currentEntity->getId()) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}