<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\BienImmobilier;
use AppBundle\Entity\Group;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Fields\SiChoicesType;
use AppBundle\Form\Type\Fields\SiNumberType;
use AppBundle\Form\Type\Fields\SurfaceNumberType;
use AppBundle\Form\Type\Fields\SurfaceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class ProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $program     = $builder->getData();
        $projectType = $program->getType();
        parent::buildForm($builder, $options);
        switch ($options['flow_step']) {
            case 1:
                $builder->add('title', TextType::class,
                    [
                        'label' => 'ads.ad.title',
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] ),
                            new Assert\Length( ['groups' => ["Default",'flow_program_step1'], 'max' => 64] )
                        ]
                    ]
                    )
                    ->add('referenceInterne', TextType::class,
                        [
                            'label' => 'ads.ad.internal_reference',
                            'required' => false
                        ]
                    )
                    ->add('description', TextareaType::class, [
                        'label' => "ads.ad.description",
                        'required' => true,
                        'attr' => [
                            'rows' => '20'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    /*->add('pays', CountryType::class,
                        [
                            'label' => 'ads.ad.country',
                            'preferred_choices' => ['FR'],
                            'constraints' => [
                                new Assert\Country(['groups' => ["Default",'flow_program_step1']]),
                                new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                            ],
                            'required' => true,
                            'attr' => [ 'class' => 'select2' ]
                        ]
                    )*/
                    ->add('pays', ChoiceType::class,
                        [
                            'label' => 'ads.ad.country',
                            'preferred_choices' => ['FR'],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ],
                            'required' => true,
                            'choices'  => [
                                'FR' => 'France'
                            ],
                            'attr' => [ 'class' => 'select2' ]
                        ]
                    )
                    ->add('ville', TextType::class,
                        [
                            'label' => 'ads.ad.city',
                            'required' => true,
                            'attr' => [
                                'class' => 'city-list',
                                'typeahead' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('codePostal', NumberType::class,
                        [
                            'label' => 'ads.ad.zip_code',
                            'required' => true,
                            'attr' => [
                                'class' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\Length(['groups' => ["Default",'flow_program_step1'], 'min' => 4, 'max' => 6]),
                                new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('adresse', TextType::class,
                        [
                            'label' => 'ads.ad.adresse',
                            'required' => false
                        ]
                    );
                    $builder->add('adresseBulleDeVente', TextType::class,
                        [
                            'label' => 'ads.ad.adresse_de_bulle_de_vente',
                            'required' => false
                        ]
                    );
                    $builder->add('titreImagePrincipale', TextType::class,
                        [
                            'label' => 'ads.ad.principale_image_title',
                            'required' => false
                        ]);

                    $groups = $options['checkImageEmpty'] ? ["Default",'flow_program_step1'] : ['Default'];
                    $builder->add('imagePrincipale', ImageUploadableType::class, [
                        'data' => new File($program->getImagePrincipale(), false),
                        'label' => 'ads.ad.principale_image',
                        'required' => true,
                        'attr' => [
                            'class' => 'col-md-12 filestyle uplodable',
                            'data-buttonText' => 'choose_file'
                        ],
                        'error_bubbling' => false,
                        'constraints' => [
                            new Assert\File( ['groups'     => $groups, 'mimeTypes' => ["image/jpeg", "image/pjpeg"] ] )
                        ]
                    ]);
                    $builder->add('typeAvancement', ChoiceType::class, [
                        'label' => 'program.type_avancement',
                        'required' => true,
                        'choices' => Program::COMMERCIALISATION,
                        'attr' => [
                            'class' => 'select2'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('lienVisitVirtuel', TextType::class,
                        [
                            'label' => 'ads.ad.virtual_visit_link',
                            'required' => false,
                            'constraints' => [
                                new Assert\Url(['groups' => ["Default",'flow_program_step1']])
                            ]
                        ]
                    )
                    ->add('dateCommercialisation', DateType::class, [
                        'label'  => 'program.date_commercialisation',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'datepicker',
                            'autocomplete' => 'off'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('dateLivraison', DateType::class, [
                        'label'  => 'program.date_livraison',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'datepicker',
                            'autocomplete' => 'off'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step1'], 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('nbLots', TextType::class,
                        [
                            'attr'           => ['class' => 'si_number'],
                            'empty_data' => null
                        ])
                    ->add('nbLotsDisponible', TextType::class,
                        [
                            'attr'           => ['class' => 'si_number'],
                            'empty_data' => null
                        ]);


                /*Bons Plans*/
                $builder->add('titreOffre', TextType::class, [
                    'label'    => "program.offre.title",
                    'required' => false
                    ])
                    ->add('descriptionOffre', TextareaType::class, [
                        'label' => "program.offre.descriptif",
                        'required' => false,
                        'attr' => [
                            'rows' => '20'
                        ]
                    ])
                    ->add('mentionLegalOffre', TextType::class, [
                        'label'    => "program.offre.mention_legal",
                        'required' => false
                    ])
                    ->add('dateDebutOffre', DateType::class, [
                        'label'    => "program.offre.date_debut",
                        'required' => false,
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'datepicker',
                            'autocomplete' => 'off'
                        ]
                    ])
                    ->add('dateFinOffre', DateType::class, [
                        'label'    => "program.offre.date_fin",
                        'required' => false,
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'datepicker',
                            'autocomplete' => 'off'
                        ]
                    ]);
            break;
            case 2:
                $builder
                    ->add('siViabilise', SiChoicesType::class,
                        [
                            'label' => "ads.ad.viabilise",
                            'required' => false,
                            'choices' =>  Terrain::VIABILISE,
                        ])
                    ->add('standing', ChoiceType::class, [
                        'label' => "ads.ad.standing",
                        'required' => false,
                        'choices' => BienImmobilier::STANDING,
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ])
                    ->add('nbetages', TextType::class,
                    [
                        'attr'           => ['class' => 'si_number'],
                        'empty_data' => null
                    ])
                    ->add('siAscenseur', CheckboxType::class, [
                        'label'    => 'ads.ad.ascenseur',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                ;

                $builder->add('investisseur', CheckboxType::class, [
                    'label'    => 'ads.ad.investisseur',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                    ->add('habitat', CheckboxType::class, [
                        'label'    => 'ads.ad.habitat',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ]);
                if($projectType != 'terrain') {
                    $builder->add('typeDeChauffage', ChoiceType::class, [
                        'label' => 'ads.ad.type_chauffage',
                        'required' => false,
                        'choices' => [
                            '4096'          => 'Collectif',
                            '8192'          => 'individuel'
                        ],
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ])->add('energieDeChauffage', ChoiceType::class, [
                            'label' => 'ads.ad.energie_de_chauffage',
                            'required' => false,
                            'choices' => [
                                'aquathermie'          => 'Aquathermie',
                                'aérothermie'          => 'Aérothermie',
                                'biocombustible'       => 'Biocombustible',
                                'bois'                 => 'Bois',
                                'charbon'              => 'Charbon',
                                'climatisation réversible'              => 'Climatisation réversible',
                                'fuel'              => 'Fuel',
                                'gaz'              => 'Gaz',
                                'granulés'              => 'Granulés',
                                'géothermie'              => 'Géothermie',
                                'pompe à chaleur'              => 'Pompe à chaleur',
                                'solaire'              => 'Solaire',
                                'éléctricité'              => 'éléctricité',
                                'éolienne'              => 'éolienne',
                            ],
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('revetementDeSol', ChoiceType::class, [
                            'label' => "ads.ad.revetement_du_sol",
                            'required' => false,
                            'choices' => [
                                'béton ciré'        => 'béton ciré',
                                'carrelage'         => 'carrelage',
                                'fibre végétale'    => 'fibre végétale',
                                'linoléum'          => 'linoléum',
                                'moquette'          => 'moquette',
                                'PVC'               => 'PVC',
                                'marbre'            => 'marbre',
                                'parquet'           => 'parquet',
                                'parquet contrecollé' => 'parquet contrecollé',
                                'parquet flottant'    => 'parquet flottant',
                                'parquet massif'      => 'parquet massif',
                                'parquet stratifié'   => 'parquet stratifié',
                                'pierre naturelle'    => 'pierre naturelle',
                                'tomette'             => 'tomette',
                                'vinyle'              => 'vinyle'
                            ],
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('vitrage', ChoiceType::class, [
                            'label' => 'program.vitrage',
                            'required' => false,
                            'choices' => Maison::VITRAGES,
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('siGarage', SiNumberType::class,
                            [
                                'label' => "ads.ad.garage",
                                'required' => false,
                                'constraints' => [
                                    new AppAssert\SiNumber()
                                ]
                            ])
                        ->add('siParking', SiNumberType::class,
                            [
                                'label' => "ads.ad.parking",
                                'required' => false,
                                'constraints' => [
                                    new AppAssert\SiNumber()
                                ]
                            ])
                        ->add('siBox', CheckboxType::class, [
                            'label'    => 'ads.ad.box',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siTerrasse', CheckboxType::class, [
                            'label'    => 'ads.ad.terrasse',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siBalcons', CheckboxType::class, [
                            'label'    => 'ads.ad.balcons',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siCave', CheckboxType::class, [
                            'label'    => 'ads.ad.cave',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siJardin', CheckboxType::class, [
                            'label'    => 'ads.ad.jardin',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('typeJardin', ChoiceType::class, [
                            'label' => 'ads.ad.type_jardin',
                            'required' => false,
                            'choices' => [
                                'non privatif'      => 'Non privatif',
                                'privatif'          => 'Privatif',
                            ],
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('toitTerrasse', CheckboxType::class, [
                            'label'    => 'ads.ad.toitTerrasse',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('grenier', ChoiceType::class, [
                            'label' => 'ads.ad.grenier',
                            'required' => false,
                            'choices' => BienImmobilier::GARNIER,
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('siVoletsRoulantsElectriques', CheckboxType::class, [
                            'label'    => 'ads.ad.volet_roulants_electriques',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siClimatise', CheckboxType::class, [
                            'label'    => 'ads.ad.climatise',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siTV', CheckboxType::class, [
                            'label'    => 'ads.ad.TV',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siPMR', CheckboxType::class, [
                            'label'    => 'ads.ad.PMR',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                        ->add('siPiscine', CheckboxType::class, [
                            'label'    => 'ads.ad.piscine',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ]);

                    /* Sécurité */
                    if($projectType == 'appartement') {
                        $builder->add('siGardien', CheckboxType::class, [
                            'label'    => 'ads.ad.gardien',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ])
                            ->add('siVideophone', CheckboxType::class, [
                                'label'    => 'ads.ad.videophone',
                                'required' => false,
                                'attr' => [
                                    'class' => 'i-checks'
                                ]
                            ])
                            ->add('siInterphone', CheckboxType::class, [
                                'label'    => 'ads.ad.interphone',
                                'required' => false,
                                'attr' => [
                                    'class' => 'i-checks'
                                ]
                            ]);
                    }
                    $builder->add('siDigicode', CheckboxType::class, [
                        'label' => 'ads.ad.digicode',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                        ->add('siAlarme', CheckboxType::class, [
                            'label' => 'ads.ad.alarme',
                            'required' => false,
                            'attr' => [
                                'class' => 'i-checks'
                            ]
                        ]);

                    /*Energie*/
                    $builder
                        ->add('DPE', ChoiceType::class, [
                            'label' => "ads.ad.bilan_consomation_energie",
                            'required' => false,
                            /** @Ignore */'choices' => Ad::CONSOMATION,
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('dpeValue', NumberType::class,
                            [
                                'label' => 'ads.ad.consomation_energie',
                                'required' => false
                            ])
                        ->add('GES', ChoiceType::class, [
                            'label' => "ads.ad.bilan_emission_ges",
                            'required' => false,
                            /** @Ignore */'choices' => Ad::CONSOMATION,
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ])
                        ->add('gesValue', NumberType::class,
                            [
                                'label' => 'ads.ad.emission_ges',
                                'required' => false
                            ]);
                } else {
                    $builder->add('libreConstruction', ChoiceType::class, [
                        'empty_value' =>false,
                        'label' => "ads.ad.libreConstruction",
                        'required' => false,
                        'choices' =>  [true => 'Oui',false => 'Non'],
                        'expanded' => true
                    ])
                        ->add('viabilise', ChoiceType::class, [
                            'empty_value' =>false,
                            'label' => "ads.ad.viabilise",
                            'required' => false,
                            'choices' =>  [true => 'Oui',false => 'Non'],
                            'expanded' => true
                        ])
                        ->add('constructible', ChoiceType::class, [
                            'empty_value' =>false,
                            'label' => "ads.ad.constructible",
                            'required' => false,
                            'choices' =>  [true => 'Oui',false => 'Non'],
                            'expanded' => true
                        ])
                        ->add('servitude', ChoiceType::class, [
                            'empty_value' =>false,
                            'label' => "ads.ad.servitude",
                            'required' => false,
                            'choices' =>  [true => 'Oui',false => 'Non'],
                            'expanded' => true
                        ])
                        ->add('pente', ChoiceType::class, [
                            'empty_value' =>false,
                            'label' => "ads.ad.pente",
                            'required' => false,
                            'choices' =>  [true => 'Oui',false => 'Non'],
                            'expanded' => true
                        ])
                        ->add('belleVue', ChoiceType::class, [
                            'empty_value' =>false,
                            'label' => "ads.ad.nice_view",
                            'required' => false,
                            'choices' =>  [true => 'Oui',false => 'Non'],
                            'expanded' => true
                        ])
                        ->add('typeTerrain', ChoiceType::class, [
                            'label'         => 'ads.ad.type',
                            'required'      => true,
                            'empty_value'   => '',
                            'choices'       => Terrain::TERRAIN_TYPES,
                            'attr' => [
                                'class' => 'select2'
                            ],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups' => ["Default",'flow_program_step2'], 'message' => "form.field_is_required" ] )
                            ]
                        ])
                        ->add('exposition', ChoiceType::class, [
                            'label' => 'ads.ad.exposition',
                            'required' => false,
                            'mapped'   => false,
                            'choices' => Terrain::EXPOSITION,
                            'attr' => [
                                'class' => 'select2'
                            ]
                        ]);
                }
                $builder->add('environnement', ChoiceType::class, [
                    'label' => 'Environnement',
                    'required' => false,
                    'choices' => BienImmobilier::ENVIRONNEMENT,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]);
            break;
            case 3:
                $builder->add('siProximiteAeroport', CheckboxType::class, [
                    'label'    => 'ads.ad.proximite.proximite_aeroport',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                    ->add('siProximiteSortieAutoroute', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_sortie_autoroute',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteGare', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximité_gare',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteBus', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximité_bus',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteTramway', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_tramway',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteParking', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_parking',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCommerces', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_commerces',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCentreCommercial', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_centre_commercial',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCentreville', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_centre_ville',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCreche', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_creche',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteEcolePrimaire', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_ecole_primaire',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCollege', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_college',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteLycee', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_lycee',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteUniversite', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_universite',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteEspacesVerts', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_espaces_verts',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ]);


                /*Avantages environnementaux*/
                $builder->add('siEcoQuartier', CheckboxType::class, [
                    'label'    => 'ads.ad.env.eco_quartier',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])->add('siBioConstruction', CheckboxType::class, [
                    'label'    => 'ads.ad.env.bio_construction',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ]);

                /*Avantages Investissement*/
                $builder->add('loiPinel', CheckboxType::class, [
                    'label'    => 'ads.ad.avantages.loi_pinel',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                    ->add('loiDuflot', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.loi_duflot',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('censiBouvard', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.ensi_bouvard',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('LMP_LMNP', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.LMP',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('loi_Girardin_Paul', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.loi_girardin_paul',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('loi_Malraux', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.loimalraux',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('loi_Demessine', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.loi_demessine',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('regime_SCPI', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.regime_SCPI',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('nue_Propriete', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.nue_propriete',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('pret_Locatif_Social', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.pret_locatif_social',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('PSLA', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.pret_social_location_accession', //(PSLA)
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('monumentHistorique', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.monument_historique',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('elligiblePret', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.elligible_pret_taux_zero',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('tva_1', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.tva_5',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('tva_2', CheckboxType::class, [
                        'label'    => 'ads.ad.avantages.tva_7',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ]);

                /*Certifications et labels*/
                $builder->add('hauteQualiteEnvironnementale', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.haute_qualite_environnementale',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                    ->add('nfLogement', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.nf_logement',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('labelHabitatEnvironnement', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.label_habitat_environnement',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('THPE', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.label_THPE',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('HPE_ENR', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.label_HPE_ENR',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('labelBatimentBasseConsommation', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.label_batiment_basse_consommation',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('labelHabitatEnvironnementEffinergie', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.label_habitat_environnement_effinergie',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('planLocalHabitat', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.plan_local_habitat',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('RT2012', CheckboxType::class, [
                        'label'    => 'ads.ad.labels.RT2012',
                        'required' => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ]);
            break;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'flow_step'       => null,
            'projectType'     => null,
            'checkImageEmpty' => true,
            'data_class'      => Program::class,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'program';
    }
}