<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface; 

use Symfony\Component\HttpFoundation\File\File;

class ImageUploadableType extends AbstractType
{
 
    public function configureOptions(OptionsResolver $resolver)
    {
    	$resolver->setDefaults(array(
            'compound' => false,
            'required'  => false,
            'data'      => new File('', false)
        ));
    }

    public function getParent()
    {
        return FileType::class;
    }
}