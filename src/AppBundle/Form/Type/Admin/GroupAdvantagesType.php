<?php

namespace AppBundle\Form\Type\Admin;

use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\GroupAdvantageDiscount;
use AppBundle\Repository\Configuration\ConfigurationRepository;
use AppBundle\Repository\GroupAdvantageDiscountRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAdvantagesType extends AbstractType
{
    private $advantages;
    private $groupAdvantages;
    private $currentGroupAdvantages = null;
    private $configurationRepository;
    private $type;

    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository        = $configurationRepository;
        $this->advantages                     = $this->configurationRepository->getConfiguration()->getAdvantages();
    }

    private function getGroupAdvantagesList() {
        $this->groupAdvantages = new ArrayCollection();
        /** @var Advantage $advantage */
        foreach ($this->advantages as $advantage) {
            if($advantage->getGroupType() == $this->type) {
                $haveGroupAdvantage = false;
                if($this->currentGroupAdvantages) {
                    foreach ($this->currentGroupAdvantages as $groupAdvantage) {
                        if($groupAdvantage->getAdvantage()->getId() == $advantage->getId()) {
                            $this->groupAdvantages->add($groupAdvantage);
                            $haveGroupAdvantage = true;
                            break;
                        }
                    }
                }
                if(!$haveGroupAdvantage)
                    $this->groupAdvantages->add((new GroupAdvantage())->setAdvantage($advantage));
            }
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($builder->getData()) {
            $this->type = $builder->getData()->hasRole('ROLE_GROUP') ? 'group' : 'agence';
            $this->currentGroupAdvantages = $builder->getData();
        } else {
            $this->type = $options['role'] == 'ROLE_GROUP' ? 'group' : 'agence';
        }
        $this->getGroupAdvantagesList();
        parent::buildForm($builder, $options);
        #### Advantages ####
        $builder->add('advantages', CollectionType::class,
            [
                'label'      =>  /** @Ignore */ false,
                'entry_type' => GroupAdvantageType::class,
                'prototype_data' => new GroupAdvantage(),
                'data'           => $this->groupAdvantages,
                'by_reference' => false,
                'allow_delete' => true,
                'allow_add'    => false
            ]
        );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Group::class,
            'role'       => []
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group_advantages';
    }
}