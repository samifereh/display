<?php

namespace AppBundle\Form\Type\Admin;

use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\GroupAdvantageDiscount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAdvantageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder
                ->add('quantite', NumberType::class, [
                    'label'    => 'Quantité',
                    'required' => false
                ])
                ->add('dateBegin', DateTimeType::class, [
                    'label'    => false,
                    'html5' => true,
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => [
                        'class' => 'datetimepicker',
                        'autocomplete' => 'off'
                    ]
                ])
                ->add('dateEnd', DateTimeType::class, [
                    'label'    => false,
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => [
                        'class' => 'datetimepicker',
                        'autocomplete' => 'off'
                    ]
                ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GroupAdvantage::class
        ));
    }

    public function getName()
    {
        return 'group_advantage';
    }
}