<?php

namespace AppBundle\Form\Type\Admin;

use AppBundle\Entity\Group;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupsPricingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('oneMonthPrice', MoneyType::class, [
                'label' => 'Abonnement 1 mois',
                'attr' => ['prepend' => true],
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                ]
            ])
            ->add('threeMonthPrice', MoneyType::class, [
                'label' => 'Abonnement 3 mois',
                'attr' => ['prepend' => true],
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                ]
            ])
            ->add('sixMonthPrice', MoneyType::class, [
                'label' => 'Abonnement 6 mois',
                'attr' => ['prepend' => true],
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                ]
            ])
            ->add('oneYearPrice', MoneyType::class, [
                'label' => 'Abonnement 12 mois',
                'attr' => ['prepend' => true],
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Group::class,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group_pricing';
    }
}