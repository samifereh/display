<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Flow;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\Form\FormTypeInterface;
use Craue\FormFlowBundle\Event\GetStepsEvent;
use Craue\FormFlowBundle\Event\PostBindFlowEvent;
use Craue\FormFlowBundle\Event\PostBindRequestEvent;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Craue\FormFlowBundle\Event\PostValidateEvent;
use Craue\FormFlowBundle\Event\PreBindEvent;
use Craue\FormFlowBundle\Form\FormFlowEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdFlow extends FormFlow implements EventSubscriberInterface
{
    /**
     * @var FormTypeInterface
     */
    protected $formType;
    protected $allowDynamicStepNavigation = true;
    protected $oldImage = null;

    public function setFormType(FormTypeInterface $formType) {
        $this->formType = $formType;
    }

    protected function loadStepsConfig() {
        return array(
            array(
                'label' => 'ads.step2_set_ad',
                'type'  => $this->formType,
            ),
            array(
                'label' => 'ads.step3_bien_immobilier',
                'type' => $this->formType
            ),
            array(
                'label' => 'ads.step4_confirmation',
                'type' => $this->formType
            )
        );
    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher) {
        parent::setEventDispatcher($dispatcher);
        $dispatcher->addSubscriber($this);
    }

    public static function getSubscribedEvents() {
        return array(
            FormFlowEvents::POST_BIND_REQUEST => 'onPostBindRequest',
            FormFlowEvents::POST_BIND_FLOW    => 'onPostBindFlow'
        );
    }

    public function onPostBindFlow(PostBindFlowEvent $event) {
        $this->oldImage = $event->getFormData()->getImagePrincipale();
    }

    public function onPostBindRequest(PostBindRequestEvent $event) {
        try {
            if($event->getFlow()->getCurrentStepNumber() == 1) {
                if(!$event->getFormData()->getImagePrincipale() && $event->getFormData()->getId()) {
                    $event->getFormData()->setImagePrincipale($this->oldImage);
                }
            }
        } catch (\Exception $e){
            dump($e);exit;
        }
    }



    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'] = array($options['flow_step_key'].$options['flow_step']);
        $options['translation_domain']= 'commiti';
        return $options;
    }

    public function getName() {
        return 'ad';
    }

}