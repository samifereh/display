<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Flow;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\Form\FormTypeInterface;
use Craue\FormFlowBundle\Event\GetStepsEvent;
use Craue\FormFlowBundle\Event\PostBindFlowEvent;
use Craue\FormFlowBundle\Event\PostBindRequestEvent;
use Craue\FormFlowBundle\Event\PostBindSavedDataEvent;
use Craue\FormFlowBundle\Event\PostValidateEvent;
use Craue\FormFlowBundle\Event\PreBindEvent;

class ProgramFlow extends FormFlow
{
    /**
     * @var FormTypeInterface
     */
    protected $formType;
    protected $allowDynamicStepNavigation = true;

    public function setFormType(FormTypeInterface $formType) {
        $this->formType = $formType;
    }

    protected function loadStepsConfig() {
        return array(
            array(
                'label' => 'program.step2',//Étape 2 - Principale
                'type'  => $this->formType,
            ),
            array(
                'label' => 'program.step3', //Étape 3 - Détails
                'type' => $this->formType
            ),
            array(
                'label' => 'program.step4', //Étape 4 - Autres
                'type' => $this->formType
            ),
            array(
                'label' => 'program.step5', //Étape 5 - Visuel
                'type' => $this->formType
            )
        );
    }

    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $options['validation_groups'] = array($options['flow_step_key'].$options['flow_step']);
        $options['translation_domain']= 'commiti';
        return $options;
    }

    public function getName() {
        return 'program';
    }

}