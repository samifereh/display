<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\CoreBundle\Entity\Message;
use AppBundle\Entity\Gestion\BugComment;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\FileBundle\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BugCommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', TextareaType::class, [
                'label'        =>  /** @Ignore */ false,
                'attr' => [
                    'class'=> 'form-control textarea-editor'
                ]
            ])
            ->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => BugComment::class
        ));
    }

    public function getName()
    {
        return 'bug_comment';
    }

    public function getBlockPrefix(){

        return 'bug_comment';
    }
}