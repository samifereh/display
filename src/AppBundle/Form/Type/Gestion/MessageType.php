<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\Message;
use AppBundle\Form\Type\ImageUploadableType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $message = $builder->getData();
        $builder
            ->add('message', TextareaType::class, [
                'label'        =>  /** @Ignore */ false,
                'attr' => [
                    'class' => 'form-group form-control',
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Message::class
        ));
    }

    public function getName()
    {
        return 'discussion_message';
    }

    public function getBlockPrefix(){

        return 'discussion_message';
    }
}