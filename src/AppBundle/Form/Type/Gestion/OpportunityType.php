<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class OpportunityType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $opportunity = $builder->getData();
        if($this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->user->getGroup() && !$this->user->getGroup()->hasRole('ROLE_AGENCY'))
        ) {
            $builder->add('opportunityGroup', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple'   => false,
                'expanded'   => false,
                'empty_data' => null,
                'required'   => false
            ]);
        }

        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'opportunity.title',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('source', ChoiceType::class,
                [
                    'label' => 'opportunity.source',
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => ['Phone'],'message' => "form.field_is_required" ] )
                    ],
                    'required' => true,
                    'choices'  => [
                        'email'             => 'Email',
                        'phone'             => 'Téléphone',
                        'sms'               => 'SMS',
                        'ancien_client'     => 'Ancien client',
                        'annuaire'          => 'Annuaire',
                        'apporteur'         => 'Apporteur',
                        'journal'           => 'Journal',
                        'notiriété'         => 'Notiriété',
                        'panneau'           => 'Panneau',
                        'passage'           => 'Passage',
                        'pige'              => 'Pige',
                        'préconisation'     => 'Préconisation',
                        'relation'          => 'Relation',
                        'salon_immobilier'  => 'Salon Immobilier',
                        'vitrine'           => 'Vitrine',
                        'autre'             => 'Autre'
                    ],
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('sourceAutre', TextType::class,
                [
                    'label' => 'Autre',
                    'required' => false
                ]
            )
            ->add('organisation', TextType::class,
                [
                    'label' => 'opportunity.organisation',
                    'required' => false
                ]
            )
            ->add('note', TextareaType::class, [
                'label' => "opportunity.note",
                'required' => false,
                'attr' => [
                    'rows' => '20',
                    'class'=> 'textarea-editor'
                ]
            ])
            ->add('progress', TextType::class, [
                'label'     => 'Progression',
                'attr' => [
                    'class'     => 'slider',
                ]
            ]);

        if($this->user->hasRole('ROLE_SALESMAN')) {
            $builder
                ->add('ad', EntityType::class, [
                    'class' => Ad::class,
                    'label' => 'Annonces',
                    'choice_label'  => function ($ad) {
                        return $ad->getLibelle().' / '.$ad->getReference();
                    },
                    'choice_value'  => 'remoteId',
                    'query_builder' => function (EntityRepository $qb) {
                        $group = $this->user->getAgencyGroup();
                        return $qb->createQueryBuilder('u')
                            ->where('u.group = :group')
                            ->setParameter('group',$group);
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'compound' => false,
                    'empty_data' => null,
                    'attr' => [ 'class' => 'select2' ],
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
                ])
                ->add('contact', EntityType::class, [
                    'class' => Contact::class,
                    'choice_label' => function ($contact) {
                        return $contact->getNom() . ' ' . $contact->getPrenom();
                    },
                    'query_builder' => function (EntityRepository $qb) {
                        $group = $this->user->getAgencyGroup();
                        return $qb->createQueryBuilder('u')
                            ->where('u.group = :group')
                            ->andWhere('u.type = :type')
                            ->setParameter('type', Contact::CONTACT)
                            ->setParameter('group', $group);
                    },
                    'required' => false,
                    'empty_data' => null,
                    'multiple' => false,
                    'expanded' => false,
                    'attr' => ['class' => 'select2']
                ]);
        } elseif(
                $this->authorizationChecker->isGranted('ROLE_ADMIN') ||
                ($this->user->getGroup() && !$this->user->getGroup()->hasRole('ROLE_AGENCY'))
            )  {
            $formModifier = function (FormInterface $form, $group = null) use ($opportunity) {
                $form->add('ad', EntityType::class, [
                    'class' => Ad::class,
                    'label' => 'Annonces',
                    'choice_label'  => function ($ad) {
                        return $ad->getLibelle().' / '.$ad->getReference();
                    },
                    'query_builder' => function (EntityRepository $qb) use ($group) {
                        if(is_null($group)) {
                            if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                                return $qb->createQueryBuilder('u');
                            return $qb->createQueryBuilder('u')
                                ->leftJoin('u.group','g')
                                ->where('g.parentGroup = :parentGroup')
                                ->setParameter('parentGroup',$this->user->getGroup());
                        } else {
                            return $qb->createQueryBuilder('u')
                                ->where('u.group = :group')
                                ->setParameter('group',$group);
                        }
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'compound' => false,
                    'empty_data' => null,
                    'attr' => [ 'class' => 'select2' ],
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
                ]);
                $form->add('contact', EntityType::class, [
                    'class' => Contact::class,
                    'label' => /** @Ignore */ 'Contact',
                    'choice_label' => function ($contact) {
                        return $contact->getPrenom() . ' ' . $contact->getNom();
                    },
                    'query_builder' => function (EntityRepository $qb) use ($group) {
                        if (is_null($group)) {
                            if ($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                                return $qb->createQueryBuilder('u')
                                    ->andWhere('u.type = :type')
                                    ->setParameter('type', Contact::CONTACT);
                            return $qb->createQueryBuilder('u')
                                ->leftJoin('u.group', 'g')
                                ->where('g.parentGroup = :parentGroup')
                                ->andWhere('u.type = :type')
                                ->setParameter('type', Contact::CONTACT)
                                ->setParameter('parentGroup', $this->user->getGroup());
                        } else {
                            return $qb->createQueryBuilder('u')
                                ->where('u.group = :group')
                                ->andWhere('u.type = :type')
                                ->setParameter('type', Contact::CONTACT)
                                ->setParameter('group', $group);
                        }
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'empty_data' => null,
                    'required' => false,
                    'attr' => ['class' => 'select2']
                ]);
            };

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getData()->getOpportunityGroup();
                    $formModifier($event->getForm(), $group );
                }
            );

            $builder->get('opportunityGroup')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $group);
                }
            );
        }
        $builder
            ->add('nom', TextType::class,
                [
                    'label' => 'opportunity.nom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ],
                    'attr' => [
                        'class' => 'dependency'
                    ]
                ]
            )
            ->add('prenom', TextType::class,
                [
                    'label' => 'opportunity.prenom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ],
                    'attr' => [
                        'class' => 'dependency'
                    ]
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'opportunity.email',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['groups' => ['Email'],'message' => "form.field_is_required" ] ),
                    ]
                ]
            )
            ->add('telephone', TextType::class,
                [
                    'label' => 'opportunity.phone',
                    'required' => false,
                    'constraints' => [
                        new Assert\NotBlank( ['groups' => ['Phone'],'message' => "form.field_is_required" ] ),
                    ]

                ]
            )
            ->add('mobile', TextType::class,
                [
                    'label' => 'opportunity.mobile',
                    'required' => false,

                ]
            )
            ->add('telephoneBureau', TextType::class,
                [
                    'label' => 'opportunity.telephoneBureau',
                    'required' => false,

                ]
            )
            ->add('ville', TextType::class,
                [
                    'label' => 'opportunity.city',
                    'required' => true,
                    'attr' => [
                        'class' => 'dependency city-list',
                        'typeahead' => 'cp'
                    ]
                ]
            )
            ->add('codePostal', NumberType::class,
                [
                    'label' => 'opportunity.zip_code',
                    'required' => true,
                    'attr' => [
                        'class' => 'cp dependency'
                    ],
                    'constraints' => [
                        new Assert\Length(['min'=>4,'max'=>6])
                    ]
                ]
            )
            ->add('address', TextType::class,
                [
                    'label' => 'opportunity.address',
                    'required' => false,
                    'attr' => [
                        'class' => 'dependency'
                    ]
                ]
            );
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Opportunity::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                if( ($data->getSource() == 'phone' || $data->getSource() == 'sms' ) && !$data->getTelephone())
                    return array('Default', 'Phone');
                elseif($data->getSource() == 'email' && !$data->getEmail())
                    return array('Default', 'Email');
                else
                    return array('Default');
            }
        ));
    }

    public function getName() {
        return 'opportunity';
    }

}