<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\FAQ;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class FAQType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', TextType::class,
                [
                    'label' => 'faq.question',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('answer', TextareaType::class, [
                'label' => "faq.answer",
                'required' => false,
                'attr' => [
                    'rows' => '20',
                    'class'=> 'textarea-editor'
                ]
            ])
            ->add('youtubeEmbed', TextareaType::class,
                [
                    'label' => 'faq.youtube',
                    'required' => false,
                    'attr' => [
                        'rows' => '20',
                    ]
                ]
            )
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => FAQ::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'faq';
    }

}