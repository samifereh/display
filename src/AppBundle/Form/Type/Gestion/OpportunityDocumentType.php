<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\CoreBundle\Entity\Message;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityDocuments;
use AppBundle\FileBundle\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class OpportunityDocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, [
                'label'        =>  /** @Ignore */ false,
                'attr' => [
                    'class'=> 'form-control textarea-editor'
                ]
            ])
            ->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpportunityDocuments::class
        ));
    }

    public function getName()
    {
        return 'opportunity_documents';
    }

    public function getBlockPrefix(){

        return 'opportunity_documents';
    }
}