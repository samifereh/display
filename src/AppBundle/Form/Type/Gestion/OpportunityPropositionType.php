<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;


class OpportunityPropositionType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                    = $user;
        $this->authorizationChecker    = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $group = $options['group'];
        /** @var \AppBundle\Entity\Gestion\Opportunity $opportunity */
        $opportunity = $options['opportunity'];
        /** @var \AppBundle\Entity\Gestion\OpportunityProposition $proposition */
        $proposition = $opportunity->getPropositionDetails();
        $builder
            ->add('title', TextType::class, [
                'label'     => 'Titre',
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ])
            ->add('status', ChoiceType::class,
                [
                    'label' => 'proposition.status',
                    'required' => false,
                    'choices'  => OpportunityProposition::ETATS,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('date', DateType::class, [
                'label'     => 'Date de proposition',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy',
                'attr' => [
                    'class'     => 'datepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('budgetMaisonFrom', MoneyType::class, [
                'label' => 'Budget Maison min',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetMaisonTo', MoneyType::class, [
                'label' => 'Budget Maison max',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'error_bubbling'     => true,
                'required'     => false,

                'empty_data' => null
            ])

            ->add('surfaceMaisonSouhaiterFrom', TextType::class, [
                'label' => 'Surface Maison min',
                'attr' => ['class' => 'surface'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('surfaceMaisonSouhaiterTo', TextType::class, [
                'label' => 'Surface Maison max',
                'attr' => ['class' => 'surface'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('nombreChambreFrom', TextType::class, [
                'label' => 'Nombre de chambre min',
                'attr' => ['class' => 'si_number'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('nombreChambreTo', TextType::class, [
                'label' => 'Nombre de chambre max',
                'attr' => ['class' => 'si_number'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ]);

        if(!$proposition->getPossessionTerrain()) {
            $builder
                ->add('lieu', TextType::class, [
                    'label'        =>  'Lieu',
                    'required'     => false
                ])
                ->add('lieuAlentour', TextType::class, [
                    'label'        => 'Lieu alentour (en KM)',
                    'attr'         => ['class' => 'si_number'],
                    'required'     => false
                ])
                ->add('surfaceTerrainSouhaiter', TextType::class, [
                    'attr' => ['class' => 'surface'],
                    'required' => false,
                    'empty_data' => null
                ])
                ->add('budgetTerrainFrom', MoneyType::class, [
                    'attr' => ['prepend' => true],
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                    'required'     => false,
                    'empty_data' => null
                ])
                ->add('budgetTerrainTo', MoneyType::class, [
                    'attr' => ['prepend' => true],
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                    'required'     => false,
                    'empty_data' => null
                ]);
        }

        $builder
            ->add('budgetTotalFrom', MoneyType::class, [
                'label' => 'Budget total min',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required' => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetTotalTo', MoneyType::class, [
                'label' => 'Budget total max',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required' => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
        ;

        $builder
        ->add('ads', EntityType::class, [
            'class'         => Ad::class,
            'label'         => 'Annonces',
            'choice_label'  => function($ad) {
                /** @var Ad $ad */
                return $ad->getLibelle().'|'.$ad->getReference().'|'.$ad->getImagePrincipale();
            },
            'query_builder' => function (EntityRepository $qb) use ($group) {
                return $qb->createQueryBuilder('u')
                    ->where(':group = u.group')
                    ->setParameter('group',$group);
            },
            'multiple' => true,
            'expanded' => false,
            'empty_data' => null
        ])
        ->add('modeles', EntityType::class, [
            'class'         => HouseModel::class,
            'label'         => 'Modèles',
            'choice_label'  => function($model) {
                /** @var HouseModel $model */
                return $model->getName().'|'.$model->getImagePrincipale();
            },
            'query_builder' => function (EntityRepository $qb) use ($group) {
                return $qb->createQueryBuilder('u')
                    ->where(':group = u.group')
                    ->setParameter('group',$group);
            },
            'multiple' => true,
            'expanded' => false,
            'empty_data' => null
        ])
        ->add('customComment', TextareaType::class, [
            'label'        =>  'Commentaire',
            'attr' => [
                'class'=> 'form-control textarea-editor'
            ]
        ]);

        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
            $builder->add('commercials', EntityType::class, [
                'class'         => User::class,
                'label'         => 'Assigner à',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($group) {
                    return $qb->createQueryBuilder('u')
                        ->where(':group MEMBER OF u.groups')
                        ->setParameter('group',$group);
                },
                'multiple' => true,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpportunityProposition::class,
            'group'      => null,
            'opportunity'      => null
        ));
    }

    public function getName()
    {
        return 'opportunity_proposition';
    }

    public function getBlockPrefix(){

        return 'opportunity_proposition';
    }
}