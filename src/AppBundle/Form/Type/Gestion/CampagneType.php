<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class CampagneType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'empty_value' => '',
                'empty_data' => null,
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
            $formModifier = function (FormInterface $form, $group = null) {
                $form->add('contactList', EntityType::class, [
                    'class' => ContactList::class,
                    'label' => 'Liste de contacts',
                    'choice_label'  => 'name',
                    'query_builder' => function (EntityRepository $qb) use ($group) {
                        return $qb->createQueryBuilder('u')
                            ->where('u.group = :group')
                            ->setParameter('group',$group);
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'compound' => false,
                    'empty_data' => null,
                    'attr' => [ 'class' => 'select2' ],
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
                ]);
            };

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getData()->getGroup();
                    $formModifier($event->getForm(), $group );
                }
            );

            $builder->get('group')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $group);
                }
            );
        } else {
            $builder->add('contactList', EntityType::class, [
                'class' => ContactList::class,
                'label' => 'Liste de contacts',
                'choice_label'  => 'name',
                'choice_value'  => 'remoteId',
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('u')
                        ->where('u.group = :group')
                        ->setParameter('group',$this->user->getGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'compound' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }

        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'campagne.title',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('subject', TextType::class,
                [
                    'label' => 'campagne.subject',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ]
            )
            ->add('senderName', TextType::class,
                [
                    'label' => 'campagne.sender_name',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ]
            )
            ->add('date', DateTimeType::class, [
                'label' => 'Date d\'envoie <small>(Envoi immediate si vide)</small>',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'required' => false,
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('bodyHtml', TextareaType::class, [
                'label'        =>  'Mail <small>(format html)</small>',
                'attr' => [
                    'class'=> 'form-control'
                ]
            ])
            ->add('bodyText', TextareaType::class, [
                'label'        =>  'Mail <small>(format text)</small>',
                'attr' => [
                    'class'=> 'form-control'
                ]
            ])
            ->add('submit',SubmitType::class, [
                'label' => 'action.submit',
                'attr'  => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->add('emailBuilder',SubmitType::class, [
                'label' => 'action.email_builder',
                'attr'  => [
                    'class' => 'btn btn-warning'
                ]
            ]);
        if($builder->getData() && $builder->getData()->getStatus() == Campagne::DRAFT)
            $builder->add('draft',SubmitType::class, [
                'label' => 'action.draft',
                'attr'  => [
                    'class' => 'btn btn-warning'
                ]
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Campagne::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
            'isEdit'             => false
        ));
    }

    public function getName() {
        return 'campagne';
    }

}