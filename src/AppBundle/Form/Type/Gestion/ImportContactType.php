<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\File;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Form\Type\ImageUploadableType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class ImportContactType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->authorizationChecker->isGranted('ROLE_ADMIN') || !$this->user->getAgencyGroup()) {
            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }

        $builder
            ->add('file', ImageUploadableType::class, [
                'label'=> 'Fichier <small>(*.CSV)</small>',
                'mapped' => false,
                'attr' => [
                    'class' => 'col-md-12 filestyle',
                    'data-buttonText' => 'Choisissez un fichier CSV'
                ],
                'constraints' => new Assert\File([ 'mimeTypes' => [ 'maxSize' => "100000000","text/plain", "text/csv", "application/csv", 'mimeTypesMessage' => "Please upload a valid CSV" ] ])
            ])
            ->add('group_contacts', EntityType::class, [
                'label' => 'Groupes de contacts',
                'class' => ContactList::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u');

                    $group = $this->user->getGroup();
                    return $qb->createQueryBuilder('u')
                        ->where('u.group = :group')
                        ->setParameter('group',$group);
                },
                'multiple' => true,
                'expanded' => false,
                'empty_data' => null,
                'attr'       => [
                    'class' => 'select2'
                ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => null,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'import_contact';
    }

}