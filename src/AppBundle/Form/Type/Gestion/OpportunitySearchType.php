<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\DoctrineBundle\Registry;

class OpportunitySearchType extends AbstractType
{
    private $doctrine;

    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(Registry $doctrine, User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->doctrine                = $doctrine;
        $this->user                    = $user;
        $this->authorizationChecker    = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod("GET");
        $data = $builder->getData();

        $builder
            ->add('status', ChoiceType::class,
                [
                    'label' => 'proposition.status',
                    'required' => false,
                    'choices'  => OpportunityProposition::ETATS,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('progress', TextType::class, [
                'label'     => 'Progression',
                'attr' => [
                    'class'     => 'slider',
                ]
            ])
            ->add('dateStart', DateType::class, [
                'label'     => 'Date de début du projet',
                'html5'     => true,
                'required'  => false,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy',
                'attr' => [
                    'class'     => 'datepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('dateEnd', DateType::class, [
                'label'     => 'Date de Fin du projet',
                'html5'     => true,
                'required'  => false,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy',
                'attr' => [
                    'class'     => 'datepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('logementActuel', ChoiceType::class,
                [
                    'label' => 'proposition.logement_actuel',
                    'required' => false,
                    'choices'  => OpportunityProposition::LOGEMENT,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('typeAcquisition', ChoiceType::class,
                [
                    'label' => 'proposition.type_acquisition',
                    'required' => false,
                    'choices'  => OpportunityProposition::ACQUISITION,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('lieu', TextType::class, [
                'label'        =>  'Lieu',
                'required'     => false
            ])
            ->add('lieuAlentour', TextType::class, [
                'label'        => 'Lieu alentour (en KM)',
                'attr'         => ['class' => 'si_number'],
                'required'     => false
            ])
            ->add('possessionTerrain', ChoiceType::class, [
                'label' => 'Possession terrain',
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'required' => true,
                'error_bubbling'     => true,
                'data' => true
            ])
            ->add('apportPersonnel', MoneyType::class, [
                'label' => 'Apport personnel',
                'attr' => ['prepend' => true],
                'required' => false,
                'error_bubbling'     => true
            ])
            ->add('budgetTerrainFrom', MoneyType::class, [
                'attr' => ['prepend' => true],
                'required'     => false
            ])
            ->add('budgetTerrainTo', MoneyType::class, [
                'attr' => ['prepend' => true],
                'required'     => false
            ])
            ->add('budgetMaisonFrom', MoneyType::class, [
                'label' => 'Budget Maison min',
                'attr' => ['prepend' => true],
                'required'     => false,
                'error_bubbling'     => true
            ])
            ->add('budgetMaisonTo', MoneyType::class, [
                'label' => 'Budget Maison max',
                'attr' => ['prepend' => true],
                'error_bubbling'     => true,
                'required'     => false
            ])
            ->add('surfaceMaisonSouhaiterFrom', TextType::class, [
                'label' => 'Surface Maison min',
                'attr' => ['class' => 'surface'],
                'required'         => false,
                'error_bubbling'   => true
            ])
            ->add('surfaceMaisonSouhaiterTo', TextType::class, [
                'label' => 'Surface Maison max',
                'attr' => ['class' => 'surface'],
                'required'     => false,
                'error_bubbling'     => true
            ])
            ->add('nombreChambreFrom', TextType::class, [
                'label' => 'Nombre de chambre min',
                'attr' => ['class' => 'si_number'],
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('nombreChambreTo', TextType::class, [
                'label' => 'Nombre de chambre max',
                'attr' => ['class' => 'si_number'],
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetTotalFrom', MoneyType::class, [
                'label' => 'Budget total min',
                'attr' => ['prepend' => true],
                'required' => false,
                'error_bubbling'     => true
            ])
            ->add('budgetTotalTo', MoneyType::class, [
                'label' => 'Budget total max',
                'attr' => ['prepend' => true],
                'required' => false,
                'error_bubbling'     => true
            ])
            ;

        ##Signature
        $builder
            ->add('compromisTerrain', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('contratConstruction', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('permisConstruireDeposer', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label'       => 'Appliquer',
                'attr' => [
                    'class'   => 'btn btn-success btn-block'
                ]
            ]);
        if($data) {
            $builder
                ->add('save', SubmitType::class, [
                    'label'       => 'créer une liste de contacts',
                    'attr' => [
                        'class'   => 'btn btn-info btn-block'
                    ]
                ]);
        }
        if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
            $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');

            if(count($this->user->getGroups()) > 1 || $isAdmin) {
                $bigGroupsIds = [];
                foreach ($this->user->getGroups() as $g)
                    $bigGroupsIds[] = $g->getId();

                $builder->add('bigGroups', EntityType::class, [
                        'label' => 'lots.filters.ads_big_groups',
                        'class' => Group::class,
                        'choice_label'  => 'name',
                        'choice_value'  => 'id',
                        'query_builder' => function (EntityRepository $qb) use ($isAdmin,$bigGroupsIds) {
                            if($isAdmin)
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup IS NULL')
                                    ->andWhere('g.roles NOT LIKE :roles')
                                    ->setParameter('roles','%ROLE_AGENCY%')
                                    ;
                            else
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.id IN (:groups)')
                                    ->setParameter('groups',$bigGroupsIds);
                        },
                        'multiple'   => false,
                        'expanded'   => false,
                        'empty_data' => null,
                        'required' => false,
                        'attr' => [
                            'class'   => 'select2'
                        ]
                    ]
                );
            }

            $builder->add('group', EntityType::class, [
                    'label' => 'Agences',
                    'class' => Group::class,
                    'choice_label'  => 'name',
                    'choice_value'  => 'id',
                    'query_builder' => function (EntityRepository $qb) use ($data,$isAdmin) {
                        if(!empty($data['bigGroups'])) {
                            $currentGroup = $this->doctrine->getRepository('AppBundle:Group')->find($data['bigGroups']);

                            if($isAdmin || $this->user->getGroups()->contains($currentGroup)) {
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup = :group')
                                    ->setParameter('group',$currentGroup);
                            }
                        }

                        if($isAdmin)
                            return $qb->createQueryBuilder('g')->andWhere('g.parentGroup IS NOT NULL');
                        else
                            return $qb->createQueryBuilder('g')->andWhere('g.parentGroup IN (:group)')->setParameter('group',null);
                    },
                    'multiple'   => false,
                    'expanded'   => false,
                    'empty_data' => null,
                    'required' => false,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ]
            );

            $formModifier = function (FormInterface $form, $data = null) {
                $group = null;
                if($data && is_array($data) && isset($data['group'])) {
                    $group = $this->doctrine->getRepository('AppBundle:Group')->find($data['group']);
                } else {

                }

                if($group) {
                    $form->add('commercial', EntityType::class, [
                            'label' => 'lots.filters.group_commercials',
                            'class' => User::class,
                            'choice_label'  => 'name',
                            'query_builder' => function (EntityRepository $qb) use ($group) {
                                return $qb->createQueryBuilder('u')
                                    ->leftJoin('u.groups', 'g')
                                    ->where('u.roles LIKE :roles OR g.roles LIKE :roles')
                                    ->andWhere(':group MEMBER OF u.groups')
                                    ->setParameter('roles', '%"ROLE_SALESMAN"%')
                                    ->setParameter('group', $group);
                            },
                            'multiple'   => false,
                            'expanded'   => false,
                            'required' => false,
                            'attr' => ['class' => 'select2']
                        ]
                    )
                    ->add('contact', EntityType::class, [
                        'class' => Contact::class,
                        'choice_label' => function ($contact) {
                            return $contact->getNom() . ' ' . $contact->getPrenom();
                        },
                        'query_builder' => function (EntityRepository $qb) use ($group) {
                            return $qb->createQueryBuilder('u')
                                ->where('u.group = :group')
                                ->andWhere('u.type = :type')
                                ->setParameter('type', Contact::CONTACT)
                                ->setParameter('group', $group);
                        },
                        'required' => false,
                        'empty_data' => null,
                        'multiple' => false,
                        'expanded' => false,
                        'attr' => ['class' => 'select2']
                    ]);
                }
            };

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier) {
                    $data = $event->getData();
                    $formModifier($event->getForm(), $data);
                }
            );

            $builder->get('group')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $group);
                }
            );
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'opportunity_search';
    }

    public function getBlockPrefix(){

        return 'opportunity_search';
    }
}