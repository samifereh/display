<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityMeeting;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;


class OpportunityMeetingType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                    = $user;
        $this->authorizationChecker    = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $group = $options['group'];
        $builder
            ->add('title', TextType::class, [
                'label'        =>  'Titre',
                'required'     => true,
                'constraints'  => new Assert\NotBlank([ 'message'  => 'form.field_is_required' ])
            ])
            ->add('note', TextType::class, [
                'label'        =>  'Note',
                'required'     => false
            ])
            ->add('dateStart', DateTimeType::class, [
                'label'     => 'Date Début',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('dateEnd', DateTimeType::class, [
                'label'     => 'Date Fin',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ]);
        /*if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
            $builder->add('commercial', EntityType::class, [
                'class'         => User::class,
                'label'         => 'Commercial',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($group) {
                    return $qb->createQueryBuilder('u')
                        ->where(':group MEMBER OF u.groups')
                        ->setParameter('group',$group);
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }*/
        $builder
            ->add('remind', CheckboxType::class, [
                'required' => false,
                'label' => 'Programmer un rappel'
            ])
            ->add('submit',SubmitType::class, [
                'label' => 'action.submit',
                'attr'  => [
                    'class' => 'btn btn-success'
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AgendaEvent::class,
            'group'      => null
        ));
    }

    public function getName()
    {
        return 'opportunity_meeting';
    }

    public function getBlockPrefix(){

        return 'opportunity_meeting';
    }
}