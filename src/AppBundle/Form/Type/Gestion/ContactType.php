<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class ContactType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(!$options['isXmlHttp'] && ($this->authorizationChecker->isGranted('ROLE_ADMIN') || !$this->user->getGroup()->hasRole('ROLE_AGENCY'))) {
            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }

        $builder
            ->add('civilite', ChoiceType::class,
                [
                    'label' => 'contact.civilite',
                    'choices'  => [
                        'monsieur' => 'Monsieur',
                        'madame' => 'Madame'
                    ],
                    'required' => true,
                    'expanded' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('nom', TextType::class,
                [
                    'label' => 'contact.nom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('prenom', TextType::class,
                [
                    'label' => 'contact.prenom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'contact.email',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ]
            )
            ->add('telephone', TextType::class,
                [
                    'label' => 'contact.phone',
                    'required' => false
                ]
            )
            ->add('mobile', TextType::class,
                [
                    'label' => 'contact.mobile',
                    'required' => false
                ]
            )
            ->add('telephoneBureau', TextType::class,
                [
                    'label' => 'contact.telephoneBureau',
                    'required' => false
                ]
            )
            ->add('birthday', BirthdayType::class, [
                'label' => 'contact.birthday',
                'required' => false
            ])
            ->add('pays', ChoiceType::class,
                [
                    'label' => 'ads.ad.country',
                    'preferred_choices' => ['FR'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                    ],
                    'required' => true,
                    'choices'  => [
                        'FR' => 'France'
                    ],
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('ville', TextType::class,
                [
                    'label' => 'contact.city',
                    'required' => true,
                    'attr' => [
                        'class' => 'city-list',
                        'typeahead' => 'cp'
                    ],
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('codePostal', NumberType::class,
                [
                    'label' => 'contact.zip_code',
                    'required' => true,
                    'attr' => [
                        'class' => 'cp'
                    ],
                    'constraints' => [
                        new Assert\Length(['min'=>4,'max'=>6]),
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('address', TextType::class,
                [
                    'label' => 'contact.address',
                    'required' => false
                ]
            )
            ->add('organisation', TextType::class,
                [
                    'label' => 'contact.organisation',
                    'required' => false
                ]
            )->add('group_contacts', EntityType::class, [
                'label' => 'Groupes de contacts',
                'class' => ContactList::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u');

                    $group = $this->user->getGroup();
                    return $qb->createQueryBuilder('u')
                        ->where('u.group = :group')
                        ->setParameter('group',$group);
                },
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'empty_data' => null,
                'attr'       => [
                    'class' => 'select2'
                ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);

        $builder
            ->add('conjoint_nom', TextType::class,
                [
                    'label' => 'contact.conjoint_nom',
                    'required' => false
                ]
            )
            ->add('conjoint_prenom', TextType::class,
                [
                    'label' => 'contact.conjoint_prenom',
                    'required' => false
                ]
            )
            ->add('conjoint_email', EmailType::class,
                [
                    'label' => 'contact.conjoint_email',
                    'required' => false
                ]
            )
            ->add('conjoint_telephone', TextType::class,
                [
                    'label' => 'contact.conjoint_telephone',
                    'required' => false
                ]
            )
            ->add('conjoint_mobile', TextType::class,
                [
                    'label' => 'contact.conjoint_mobile',
                    'required' => false
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Contact::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
            'isXmlHttp' => false
        ));
    }

    public function getName() {
        return 'contact';
    }

}