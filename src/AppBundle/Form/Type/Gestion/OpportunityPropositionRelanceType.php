<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\Gestion\OpportunityPropositionRelance;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;


class OpportunityPropositionRelanceType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('objectif', TextareaType::class, [
            'label'        =>  'Objectif',
            'attr' => [
                'class'=> 'form-control textarea-editor'
            ]
        ])
        ->add('date', DateType::class, [
            'label'     => 'Date de relance',
            'html5'     => true,
            'required'  => false,
            'widget'    => 'single_text',
            'format'    => 'dd/MM/yyyy',
            'attr' => [
                'class'     => 'datepicker',
                'autocomplete' => 'off',
                'placeholder' => '__/__/____'
            ]
        ])
        ->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpportunityPropositionRelance::class,
            'group'      => null
        ));
    }

    public function getName()
    {
        return 'opportunity_proposition_relance';
    }

    public function getBlockPrefix(){

        return 'opportunity_proposition_relance';
    }
}