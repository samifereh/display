<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\AgendaEventReminder;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class AgendaEventReminderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('temps', IntegerType::class, [
                'label' => false,
                'required' => false,
                'constraints' => [
                    new Assert\Range(['min' => 0 ]),
                    new Assert\NotBlank([ 'message' => 'Ce champs est obligatoire.' ])
                ]
            ])
            ->add('unit', ChoiceType::class, [
                'label' => false,
                'choices'  => [
                    'Hours'  => 'Heures',
                    'Days'   => 'Jours',
                    'Weeks'  => 'Semaines',
                    'Months' => 'Mois'
                ],
                'required' => false,
                'empty_value' => false,
                'constraints' => [
                    new Assert\NotBlank([ 'message' => 'Ce champs est obligatoire.' ])
                ]
            ]);

    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => AgendaEventReminder::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
        ));
    }

    public function getName() {
        return 'agenda_event_reminder';
    }

}