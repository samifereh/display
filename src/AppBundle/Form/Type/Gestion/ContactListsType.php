<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class ContactListsType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;


    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->user->getGroup() && !$this->user->getGroup()->hasRole('ROLE_AGENCY'))
            ) {
            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'attr' => ['placeholder' => ' --- Groupe ---'],
                'required' => false
            ]);
        }
        $builder
            ->add('name', TextType::class, [
                'label'        =>  'Nom',
                'required'     => true,
                'constraints'  => new Assert\NotBlank([
                    'message'  => 'form.field_is_required'
                ])
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => ContactList::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'contact_groups';
    }

}