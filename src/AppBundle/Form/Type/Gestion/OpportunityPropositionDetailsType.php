<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;


class OpportunityPropositionDetailsType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                    = $user;
        $this->authorizationChecker    = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $group = $options['group'];
        $builder
            ->add('status', ChoiceType::class,
                [
                    'label' => 'proposition.status',
                    'required' => false,
                    'choices'  => OpportunityProposition::ETATS,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('progress', TextType::class, [
                'label'     => 'Progression',
                'attr' => [
                    'class'     => 'slider',
                ]
            ])
            ->add('dateStart', DateType::class, [
                'label'     => 'Date de début du projet',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy',
                'attr' => [
                    'class'     => 'datepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('dateEnd', DateType::class, [
                'label'     => 'Date de Fin du projet',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy',
                'attr' => [
                    'class'     => 'datepicker',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('logementActuel', ChoiceType::class,
                [
                    'label' => 'proposition.logement_actuel',
                    'required' => false,
                    'choices'  => OpportunityProposition::LOGEMENT,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('typeAcquisition', ChoiceType::class,
                [
                    'label' => 'proposition.type_acquisition',
                    'required' => false,
                    'choices'  => OpportunityProposition::ACQUISITION,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('lieu', TextType::class, [
                'label'        =>  'Lieu',
                'required'     => false
            ])
            ->add('lieuAlentour', TextType::class, [
                'label'        => 'Lieu alentour (en KM)',
                'attr'         => ['class' => 'si_number'],
                'required'     => false
            ])
            ->add('possessionTerrain', ChoiceType::class, [
                'label' => 'Possession terrain',
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('surfaceTerrainSouhaiter', TextType::class, [
                'attr' => ['class' => 'surface'],
                'required' => false,
                'empty_data' => null
            ])
            ->add('apportPersonnel', MoneyType::class, [
                'label' => 'Apport personnel',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required' => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetTerrainFrom', MoneyType::class, [
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'empty_data' => null
            ])
            ->add('budgetTerrainTo', MoneyType::class, [
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'empty_data' => null
            ])
            ->add('budgetMaisonFrom', MoneyType::class, [
                'label' => 'Budget Maison min',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetMaisonTo', MoneyType::class, [
                'label' => 'Budget Maison max',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'error_bubbling'     => true,
                'required'     => false,

                'empty_data' => null
            ])
            ->add('surfaceMaisonSouhaiterFrom', TextType::class, [
                'label' => 'Surface Maison min',
                'attr' => ['class' => 'surface'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('surfaceMaisonSouhaiterTo', TextType::class, [
                'label' => 'Surface Maison max',
                'attr' => ['class' => 'surface'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('nombreChambreFrom', TextType::class, [
                'label' => 'Nombre de chambre min',
                'attr' => ['class' => 'si_number'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('nombreChambreTo', TextType::class, [
                'label' => 'Nombre de chambre max',
                'attr' => ['class' => 'si_number'],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required'     => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetTotalFrom', MoneyType::class, [
                'label' => 'Budget total min',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required' => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
            ->add('budgetTotalTo', MoneyType::class, [
                'label' => 'Budget total max',
                'attr' => ['prepend' => true],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                'required' => false,
                'error_bubbling'     => true,
                'empty_data' => null
            ])
        ;
        ##Signature
        $builder->add('compromisTerrain', CheckboxType::class, [
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])
        ->add('dateCompromisTerrain', DateType::class, [
            'label'     => 'Date',
            'html5'     => true,
            'required'  => false,
            'widget'    => 'single_text',
            'format'    => 'dd/MM/yyyy',
            'attr' => [
                'class'     => 'datepicker',
                'autocomplete' => 'off',
                'placeholder' => '__/__/____'
            ]
        ])
        ->add('contratConstruction', CheckboxType::class, [
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])
        ->add('dateContratConstruction', DateType::class, [
            'label'     => 'Date',
            'html5'     => true,
            'required'  => false,
            'widget'    => 'single_text',
            'format'    => 'dd/MM/yyyy',
            'attr' => [
                'class'     => 'datepicker',
                'autocomplete' => 'off',
                'placeholder' => '__/__/____'
            ]
        ])
        ->add('permisConstruireDeposer', CheckboxType::class, [
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])
        ->add('datePermisConstruireDeposer', DateType::class, [
            'label'     => 'Date',
            'html5'     => true,
            'required'  => false,
            'widget'    => 'single_text',
            'format'    => 'dd/MM/yyyy',
            'attr' => [
                'class'     => 'datepicker',
                'autocomplete' => 'off',
                'placeholder' => '__/__/____'
            ]
        ])
        ;

        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
            $builder->add('commercials', EntityType::class, [
                'class'         => User::class,
                'label'         => 'Assigner à',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($group) {
                    return $qb->createQueryBuilder('u')
                        ->where(':group MEMBER OF u.groups')
                        ->setParameter('group',$group);
                },
                'multiple' => true,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);

        $formModifier = function (FormInterface $form, $possessionTerrain = null) {
            $form
                ->add('possessionTerrainDetail', ChoiceType::class, [
                    'choices' =>  $possessionTerrain ? OpportunityProposition::POSSESSION_01 : OpportunityProposition::POSSESSION_02,
                    'expanded' => true,
                    'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
                    'empty_data' => null,
                    'label' => 'Détails possession terrain',
                    'error_bubbling'     => true,
                ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $possessionTerrain = $event->getData()->getPossessionTerrain();
                $formModifier($event->getForm(), $possessionTerrain );
            }
        );

        $builder->get('possessionTerrain')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $possessionTerrain = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $possessionTerrain);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpportunityProposition::class,
            'group'      => null
        ));
    }

    public function getName()
    {
        return 'opportunity_proposition';
    }

    public function getBlockPrefix(){

        return 'opportunity_proposition';
    }
}