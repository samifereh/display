<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class AgendaEventType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user, AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->user->getGroup() && !$this->user->getGroup()->hasRole('ROLE_AGENCY'))
        ) {
            $group = null;
            /** @var AgendaEvent|null $agendaEvent */
            $agendaEvent = $builder->getData();
            if($agendaEvent->getOpportunity()) { $group = $agendaEvent->getOpportunity()->getOpportunityGroup(); }

            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    $groups =  $this->user->getGroups();
                    $query = $qb->createQueryBuilder('u');
                    if(!$this->user->getGroup()->hasRole('ROLE_GROUP')) {
                        $group = $this->user->getAgencyGroup();
                        $query->leftJoin("u.agencyGroups",'g');
                        $query->andWhere($query->expr()->orX(':group MEMBER OF u.groups',':group MEMBER OF g.agencyGroups'))
                            ->andWhere('u.id <> :user_id')
                            ->setParameter('user_id', $this->user->getId())
                            ->setParameter('group', $group);
                    } else {
                        $agency = [];
                        foreach ($groups as $group)
                            foreach ($group->getAgencyGroups() as $a)
                                $agency[] = $a->getId();

                        $query
                            ->leftJoin('u.agencyGroups','g')
                            ->andWhere('g.id IN (:group)')
                            ->andWhere('u.id <> :user_id')
                            ->setParameter('user_id', $this->user->getId())
                            ->setParameter('group', $agency);
                    }
                    return $query;
                },
                'multiple'   => false,
                'expanded'   => false,
                'mapped'    => false,
                'empty_data' => null,
                'required'   => false,
                'data'       => $group
            ]);

            $formModifier = function (FormInterface $form, $group = null) {
                if($group) {
                    $form
                        ->add('_type', ChoiceType::class, [
                            'label' => false,
                            'required' => true,
                            'choices' =>  [
                                null              => 'Note ou rappel',
                                'listOpportunity' => 'Liée a une opportunité',
                                'listSharedUsers' => 'Partager avec des membres de votre équipe'
                            ],
                            'expanded' => true
                        ])
                        ->add('sharedUsers', EntityType::class, [
                        'class'         => User::class,
                        'label'         => 'Partager avec des membres de votre équipe',
                        'choice_label'  => 'name',
                        'query_builder' => function (EntityRepository $qb) use($group) {
                            return $qb->createQueryBuilder('u')
                                ->where(':group MEMBER OF u.groups')
                                ->setParameter('group',$group);
                        },
                        'multiple' => true,
                        'required' => false,
                        'expanded' => false,
                        'empty_data' => null,
                        'attr' => [ 'class' => 'select2' ]
                    ])
                    ->add('opportunity', EntityType::class, [
                        'class'         => Opportunity::class,
                        'label'         => 'Opportunité',
                        'choice_label'  => 'title',
                        'query_builder' => function (EntityRepository $qb) use ($group) {
                            return $qb->createQueryBuilder('o')
                                ->where(':group = o.opportunityGroup')
                                ->andWhere('o.status <> :status')
                                ->setParameter('status', Opportunity::CLOSED)
                                ->setParameter('group',$group);
                        },
                        'multiple' => false,
                        'expanded' => false,
                        'required' => false,
                        'empty_data' => null,
                        'attr' => [ 'class' => 'select2' ]
                    ]);
                }
            };

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier) {
                    $group = null;
                    /** @var AgendaEvent|null $agendaEvent */
                    $agendaEvent = $event->getData();
                    if($agendaEvent->getOpportunity()) { $group = $agendaEvent->getOpportunity()->getOpportunityGroup(); }
                    else { $group = $event->getData()->getOwner() ? $event->getData()->getOwner()->getGroup() : null; }
                    $formModifier($event->getForm(), $group );
                }
            );

            $builder->get('group')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier) {
                    $group = $event->getForm()->getData();
                    $formModifier($event->getForm()->getParent(), $group);
                }
            );

        } else {
            $builder
            ->add('_type', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' =>  [
                    null              => 'Note ou rappel',
                    'listOpportunity' => 'Liée a une opportunité',
                    'listSharedUsers' => 'Partager avec des membres de votre équipe'
                ],
                'expanded' => true
            ])
            ->add('sharedUsers', EntityType::class, [
                'class'         => User::class,
                'label'         => 'Partager avec des membres de votre équipe',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('u')
                        ->where(':group MEMBER OF u.groups')
                        ->setParameter('group',$this->user->getAgencyGroup());
                },
                'multiple' => true,
                'required' => false,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ]
            ])
            ->add('opportunity', EntityType::class, [
                'class'         => Opportunity::class,
                'label'         => 'Opportunité',
                'choice_label'  => 'title',
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('o')
                        ->where(':group = o.opportunityGroup')
                        ->andWhere('o.status <> :status')
                        ->setParameter('status', Opportunity::CLOSED)
                        ->setParameter('group',$this->user->getAgencyGroup());
                },
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ]
            ]);
        }

        $builder
            ->add('title', TextType::class, [
                'label'        =>  'Titre',
                'required'     => true,
                'constraints'  => new Assert\NotBlank([ 'message'  => 'form.field_is_required' ])
            ])
            ->add('note', TextareaType::class, [
                'label'        =>  'Note',
                'required'     => false
            ])
            ->add('dateStart', DateTimeType::class, [
                'label'     => 'Date Début',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'yyyy-MM-dd HH:mm',
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'constraints'  => new Assert\NotBlank([ 'message'  => 'form.field_is_required' ])
            ])
            ->add('dateEnd', DateTimeType::class, [
                'label'     => 'Date Fin',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'yyyy-MM-dd HH:mm',
                'attr' => [
                    'autocomplete' => 'off'
                ],
                'constraints'  => new Assert\NotBlank([ 'message'  => 'form.field_is_required' ])
            ])
            ->add('color', HiddenType::class)
            ->add('remind', CheckboxType::class, [
                'required' => false,
                'label' => 'Programmer un rappel'
            ])
            ->add('temps', IntegerType::class, [
                'label' => false,
                'required' => false,
                'constraints' => [
                    new Assert\Range(['min' => 0 ]),
                    new Assert\NotBlank([ 'message' => 'Ce champs est obligatoire.' ])
                ]
            ])
            ->add('unit', ChoiceType::class, [
                'label' => false,
                'choices'  => [
                    'Hours'  => 'Heures',
                    'Days'   => 'Jours',
                    'Weeks'  => 'Semaines',
                    'Months' => 'Mois'
                ],
                'required' => false,
                'empty_value' => false,
                'constraints' => [
                    new Assert\NotBlank([ 'message' => 'Ce champs est obligatoire.' ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label'       => 'Enregistrer',
                'attr' => [
                    'class'   => 'btn btn-success btn-block'
                ]
            ]);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => AgendaEvent::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
        ));
    }

    public function getName() {
        return 'agenda_event';
    }

}