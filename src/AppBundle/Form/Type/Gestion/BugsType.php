<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class BugsType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user, AuthorizationChecker $authorizationChecker)
    {
        $this->user                           = $user;
        $this->authorizationChecker           = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
            $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');
            if (count($this->user->getGroups()) > 1 || $isAdmin) {
                $bigGroupsIds = [];
                foreach ($this->user->getGroups() as $g)
                    $bigGroupsIds[] = $g->getId();

                $builder->add('bigGroups', EntityType::class, [
                        'label' => 'lots.filters.ads_big_groups',
                        'class' => Group::class,
                        'choice_label' => 'name',
                        'choice_value' => 'id',
                        'query_builder' => function (EntityRepository $qb) use ($isAdmin, $bigGroupsIds) {
                            if ($isAdmin)
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup IS NULL')
                                    ->andWhere('g.roles NOT LIKE :roles')
                                    ->setParameter('roles', '%ROLE_AGENCY%');
                            else
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.id IN (:groups)')
                                    ->setParameter('groups', $bigGroupsIds);
                        },
                        'multiple' => false,
                        'expanded' => false,
                        'empty_data' => null,
                        'required' => false,
                        'attr' => [
                            'class' => 'select2'
                        ],
                        'mapped' => false
                    ]
                );

                $parentGroup = $options['bigGroup'];
                if($parentGroup) {
                    $builder->add('group', EntityType::class, [
                        'label' => 'Agences',
                        'class' => Group::class,
                        'choice_label'  => 'name',
                        'query_builder' => function (EntityRepository $qb) use ($parentGroup){
                            if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                                return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                            return $qb->createQueryBuilder('u')
                                ->where('u.parentGroup = :parentGroup')
                                ->andWhere('u.agencyGroups IS EMPTY')
                                ->setParameter('parentGroup',$parentGroup);
                        },
                        'multiple'   => false,
                        'expanded'   => false,
                        'empty_data' => null,
                        'required'   => false
                    ]);
                }
            }
        } elseif($this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            ($this->user->getGroup() && !$this->user->getGroup()->hasRole('ROLE_AGENCY'))
        ) {
            $builder->add('group', EntityType::class, [
                'class' => Group::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                        return $qb->createQueryBuilder('u')->where('u.agencyGroups IS EMPTY');

                    return $qb->createQueryBuilder('u')
                        ->where('u.parentGroup = :parentGroup')
                        ->andWhere('u.agencyGroups IS EMPTY')
                        ->setParameter('parentGroup',$this->user->getGroup());
                },
                'multiple'   => false,
                'expanded'   => false,
                'empty_data' => null,
                'required'   => false
            ]);
        }

        if($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $builder->add('reporter', EntityType::class, [
                'class' => User::class,
                'label' => 'Assigner à',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) {
                    $query = $qb->createQueryBuilder('u');
                    $query
                        ->where($query->expr()->orX(
                            $query->expr()->like('u.roles', ':roles1'),
                            $query->expr()->like('u.roles', ':roles2')
                        ))
                        ->setParameter('roles1', '%"ROLE_ADMIN"%')
                        ->setParameter('roles2', '%"ROLE_DEVELOPER"%');
                    return $query;
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ])
            ->add('status', ChoiceType::class,
                [
                    'label' => 'bugs.status',
                    'required' => false,
                    'choices'  => Bug::STATUS,
                    'attr' => [ 'class' => 'select2' ]
                ]
            );
        }

        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'bugs.title',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('priority', ChoiceType::class,
                [
                    'label' => 'bugs.priority',
                    'required' => true,
                    'choices'  => Bug::PRIORITY,
                    'attr' => [ 'class' => 'select2' ]
                ]
            )
            ->add('description', TextareaType::class, [
                'label' => "bugs.description",
                'required' => true,
                'attr' => [
                    'rows' => '20',
                    'class'=> 'textarea-editor'
                ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ]),
            ]);
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Bug::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti',
            'bigGroup' => null,
        ));
    }

    public function getName() {
        return 'bugs';
    }

}