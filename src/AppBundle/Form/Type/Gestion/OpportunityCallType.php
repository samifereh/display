<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\CoreBundle\Entity\Message;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\User;
use AppBundle\FileBundle\Form\ImageType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;


class OpportunityCallType extends AbstractType
{
    /** @var User */
    private $user;

    /** @var AuthorizationChecker */
    private $authorizationChecker;

    public function __construct(User $user,AuthorizationChecker $authorizationChecker)
    {
        $this->user                    = $user;
        $this->authorizationChecker    = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $group = $options['group'];
        $builder
            ->add('summary', TextType::class, [
                'label'        =>  'Note',
                'required'     => false
            ])
            ->add('date', DateTimeType::class, [
                'label'     => 'Date',
                'html5'     => true,
                'required'  => true,
                'widget'    => 'single_text',
                'format'    => 'dd/MM/yyyy HH:mm',
                'attr' => [
                    'class' => 'datetimepicker',
                    'autocomplete' => 'off'
                ]
            ]);
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
            $builder->add('responsible', EntityType::class, [
                'class'         => User::class,
                'label'         => 'Responsible',
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($group) {
                    return $qb->createQueryBuilder('u')
                        ->where(':group MEMBER OF u.groups')
                        ->setParameter('group',$group);
                },
                'multiple' => false,
                'expanded' => false,
                'empty_data' => null,
                'attr' => [ 'class' => 'select2' ],
                'constraints' => new Assert\NotBlank([ 'message' => 'form.field_is_required' ])
            ]);
        }
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.submit',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => OpportunityCall::class,
            'group'      => null
        ));
    }

    public function getName()
    {
        return 'opportunity_call';
    }

    public function getBlockPrefix(){

        return 'opportunity_call';
    }
}