<?php

namespace AppBundle\Form\Type\Gestion;

use AppBundle\Entity\Gestion\Discussion;
use AppBundle\Entity\Gestion\Message;
use AppBundle\Entity\User;
use AppBundle\Form\Transformer\UserTransformer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;

class DiscussionType extends AbstractType
{
    /** @var $security AuthorizationChecker */
    private $authorizationChecker;
    /** @var  \AppBundle\Entity\User $user */
    private $user;
    private $em;

    public function __construct($user, AuthorizationChecker $authorizationChecker, EntityManager $em)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->user                 = $user;
        $this->em                   = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $discussion = $builder->getData();
        $builder->add('users', EntityType::class, [
            'class' => User::class,
            'choice_label'  => 'name',
            'query_builder' => function (EntityRepository $qb) {
                if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                    return $qb->createQueryBuilder('u');

                $groups =  $this->user->getGroups();
                $query = $qb->createQueryBuilder('u');
                if(!$this->user->getGroup()->hasRole('ROLE_GROUP')) {
                    $group = $this->user->getAgencyGroup();
                    $query->leftJoin("u.groups",'g');
                    $query->andWhere($query->expr()->orX(':group MEMBER OF u.groups',':group MEMBER OF g.agencyGroups'))
                        ->andWhere('u.id <> :user_id')
                        ->setParameter('user_id', $this->user->getId())
                        ->setParameter('group', $group);
                } else {
                    $agency = [];
                    foreach ($groups as $group)
                        foreach ($group->getAgencyGroups() as $a)
                            $agency[] = $a;
                    $query
                        ->leftJoin('u.groups','g')
                        ->andWhere('g.id IN (:group)')
                        ->andWhere('u.id <> :user_id')
                        ->setParameter('user_id', $this->user->getId())
                        ->setParameter('group', $agency);
                }
                return $query;
            },
            'label'    => 'À',
            'multiple' => true,
            'expanded' => false,
            'empty_data' => null,
            'constraints' => new NotBlank([
                'message' => 'form.field_is_required'
            ])
        ])->add('subject', TextType::class, [
            'label'        =>  'Object',
            'required'     => true,
            'constraints'  => new NotBlank([
                'message'  => 'form.field_is_required'
            ])
        ]);

        if($options['new']) {
            $messages = new ArrayCollection();
            $messages->add(new Message());
            $builder->add('messages', CollectionType::class,
                [
                    'label'        =>  /** @Ignore */ false,
                    'entry_type'   => MessageType::class,
                    'data'         => $messages,
                    'by_reference' => false,
                    'allow_delete' => false,
                    'allow_add'    => false,
                    'block_name'   => 'discussion_messages'
                ]
            );
        } elseif($options['edit']) {
            $builder->add('messages', CollectionType::class,
                [
                    'label'        =>  /** @Ignore */ false,
                    'entry_type'   => MessageType::class,
                    'by_reference' => false,
                    'allow_delete' => false,
                    'allow_add'    => false,
                    'block_name'   => 'discussion_messages'
                ]
            );
        }
        $builder->add('submit', SubmitType::class, [
            'label' => 'Envoyer',
            'attr' => [
                'class'         => 'btn btn-3d btn-primary',
            ]
        ])->add('save', SubmitType::class, [
            'label' => 'Enregistrer',
            'attr' => [
                'class'         => 'btn btn-default',
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Discussion::class,
            'new'        => true,
            'edit'       => false
        ));
    }

    public function getBlockPrefix(){
        return 'discussion';
    }
}