<?php

namespace AppBundle\Form\Type\Configuration;

use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\AdvantageDetails;
use AppBundle\Entity\Configuration\AdvantageOption;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Repository\Configuration\AdvantageDetailsRepository;
use AppBundle\Repository\Configuration\AdvantageRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvantageType extends AbstractType
{

    /** @var AuthorizationChecker */
    private $security;
    public function __construct(AuthorizationChecker $security)
    {
        $this->security           = $security;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder
                ->add('name', TextType::class,
                    [
                        'label' => 'Nom',
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['message' => "Le nom de l'avantage est obligatoire" ] )
                        ]
                    ]
                )
                ->add('unite', TextType::class,
                    [
                        'label'     => 'Unité',
                        'required'  => false
                    ]
                )
                ->add('description', TextareaType::class,
                    [
                        'label' => 'Description',
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['message' => "La description est obligatoire" ] )
                        ]
                    ]
                )
                ->add('options', CollectionType::class,
                    [
                        'label'      =>  /** @Ignore */ false,
                        'entry_type' => AdvantageOptionType::class,
                        'prototype_data' => new AdvantageOption(),
                        'by_reference' => false,
                        'allow_delete' => true,
                        'allow_add'    => true,
                        'prototype' => true,
                        'prototype_name' => '__advantage_opt__'
                    ]
                );

            if($this->security->isGranted('ROLE_DEVELOPER')) {
                $builder
                    ->add('slug', TextType::class,
                        [
                            'label' => 'Slug'
                        ]
                    )
                    ->add('groupType', ChoiceType::class,
                        [
                            'label' => 'Type de Groupe',
                            'empty_value' =>false,
                            'required' => false,
                            'choices' =>  ['group' => 'Groupe','agence'=> 'Agence'],
                            'expanded' => true

                        ]
                    )
                    ->add('recurrent', CheckboxType::class, [
                        'label'    => 'Récurrentielle',
                        'required' => false,
                        'attr' => [
                            'class'      => 'i-checks'
                        ]
                    ]);
            }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Advantage::class
        ));
    }

    public function getName()
    {
        return 'advantage';
    }
}