<?php

namespace AppBundle\Form\Type\Configuration;

use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\Configuration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvantagesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder
                ->add('advantages', CollectionType::class,
                    [
                        'label'      =>  /** @Ignore */ false,
                        'entry_type' => AdvantageType::class,
                        'prototype_data' => new Advantage(),
                        'by_reference' => false,
                        'allow_delete' => true,
                        'allow_add'    => true
                    ]
                )
                ->add('submit',SubmitType::class, [
                    'label' => 'Confirmer',
                    'attr'  => [
                        'class' => 'btn btn-success'
                    ]
                ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Configuration::class
        ));
    }

    public function getName()
    {
        return 'advantages';
    }
}