<?php

namespace AppBundle\Form\Type\Configuration;

use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\AdvantageDetails;
use AppBundle\Entity\Configuration\AdvantageOption;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Repository\Configuration\AdvantageDetailsRepository;
use AppBundle\Repository\Configuration\AdvantageRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvantageOptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder
                ->add('reference', TextType::class,
                    [
                        'label'       => false,
                        'attr' => [ 'placeholder' => 'Référence'],
                        'required'    => true,
                        'constraints' => [
                            new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                        ]
                    ]
                )
                ->add('unit', NumberType::class,
                    [
                        'label' => false,
                        'required' => false,
                        'attr' => [ 'placeholder' => 'Illimité'],
                        'constraints' => [
                            new Assert\NotBlank( ['groups'=>['Unit'],'message' => "form.field_is_required" ] )
                        ]
                    ]
                )
                ->add('month', ChoiceType::class, [
                    'empty_value' =>false,
                    'label' => "Durée",
                    'required' => true,
                    'choices' =>
                        [
                           -1 => 'Illimité',
                            1  => '1 mois',
                            2  => '2 mois',
                            3  => '3 mois',
                            6  => '6 mois',
                            12 => '12 mois'
                        ],
                    'expanded' => false
                ])
                ->add('price', MoneyType::class,
                    [
                        'label' => false,
                        'attr' => [ 'placeholder' => 'Prix', 'prepend' => true],
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['message' => "Le prix est obligatoire" ] )
                        ]
                    ]
                )
            ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AdvantageOption::class,
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                if($data->getMonth() < 0)
                    return array('Default', 'Unit');
                else
                    return array('Default');
            }
        ));
    }

    public function getName()
    {
        return 'advantage_option';
    }
}