<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Diffusion;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DiffusionSearchType extends AbstractType
{

    private $user;
    private $authorizationChecker;
    private $doctrine;

    public function __construct(User $user,Registry $doctrine, AuthorizationChecker $authorizationChecker){
        $this->user = $user;
        $this->authorizationChecker = $authorizationChecker;
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        parent::buildForm($builder, $options);
        $data = $builder->getData();
        if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
            $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');
            if (count($this->user->getGroups()) > 1 || $isAdmin) {
                $bigGroupsIds = [];
                foreach ($this->user->getGroups() as $g)
                    $bigGroupsIds[] = $g->getId();

                $builder->add('bigGroups', EntityType::class, [
                        'label' => 'lots.filters.ads_big_groups',
                        'class' => Group::class,
                        'choice_label' => 'name',
                        'choice_value' => 'id',
                        'query_builder' => function (EntityRepository $qb) use ($isAdmin, $bigGroupsIds) {
                            if ($isAdmin)
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup IS NULL')
                                    ->andWhere('g.roles NOT LIKE :roles')
                                    ->setParameter('roles', '%ROLE_AGENCY%');
                            else
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.id IN (:groups)')
                                    ->setParameter('groups', $bigGroupsIds);
                        },
                        'multiple' => false,
                        'expanded' => false,
                        'empty_data' => null,
                        'required' => false
                    ]
                );
            }
            $builder->add('group', EntityType::class, [
                    'label' => 'lots.filters.ads_group',
                    'class' => Group::class,
                    'choice_label' => 'name',
                    'choice_value' => 'id',
                    'query_builder' => function (EntityRepository $qb) use ($isAdmin, $data) {
                        if (!empty($data['bigGroups'])) {
                            $currentGroup = $this->doctrine->getRepository('AppBundle:Group')->find($data['bigGroups']);

                            if ($isAdmin || $this->user->getGroups()->contains($currentGroup)) {
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup = :group')
                                    ->setParameter('group', $currentGroup);
                            }
                        }
                        $qBuilder = $qb->createQueryBuilder('g');
                        if ($isAdmin)
                            return $qBuilder
                                ->andWhere($qBuilder->expr()
                                ->like('g.roles', ':roles'))
                                ->setParameter('roles','%ROLE_AGENCY%');
                        else
                            return $qb->createQueryBuilder('g')
                                ->andWhere('g.parentGroup IN (:group)')
                                ->setParameter('group', $this->user->getGroups());
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'empty_data' => null,
                    'required' => false
                ]
            );
        }
            $builder
                ->add('zoneGeographique', ChoiceType::class, [
                    'label'       => 'portal.zone_geographique',
                    'required' => false,
                    'choices' => Portal::ZONES,
                    'empty_value' => '--- Zones Géographique ---'
                ])
                ->add('cluster', ChoiceType::class, [
                    'label'       => 'portal.type',
                    'empty_value' => '--- Types ---',
                    'required' => false,
                    'choices' => Portal::CLUSTER
                ])
                ->add('typeDiffusion', ChoiceType::class, [
                    'label'       => 'portal.type_diffusion',
                    'multiple'    => true,
                    'required' => false,
                    'choices' => Portal::TYPES,
                    'attr' => [
                        'class'   => 'select2',
                        'data-placeholder' => 'Type d\'annonce'
                    ]
                ])
                ->add('categories', ChoiceType::class, [
                    'label'       => 'portal.categories_principales',
                    'empty_value' => null,
                    'required' => false,
                    'multiple'    => true,
                    'choices' => Portal::CATEGORIES,
                    'attr' => [
                        'class'   => 'select2',
                        'data-placeholder' => 'Catégorie du portail'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Appliquer',
                    'attr' => [
                        'class'   => 'btn btn-success'
                    ]
                ])
            ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'translation_domain' => 'commiti',
            'csrf_protection' => false,
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'diffusion_search';
    }
}