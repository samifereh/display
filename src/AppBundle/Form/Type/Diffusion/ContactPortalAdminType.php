<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Diffusion;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactPortalAdminType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');
        parent::buildForm($builder, $options);
            $builder
                ->add('nom', TextType::class, [
                    'label'       => 'Nom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ])
                ->add('prenom', TextType::class, [
                    'label'       => 'Prénom',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ])
                ->add('company', TextType::class, [
                    'label'       => 'Société',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ])
                ->add('telephone', TextType::class, [
                    'label'       => 'Telephone',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    ]
                ])
                ->add('email', TextType::class, [
                    'label'       => 'Email',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                        new Assert\Email(  )
                    ]
                ])
                ->add('nb_annonces', TextType::class, [
                    'label'       => 'Nombre d\'annonces souhaitées',
                    'required' => false,
                    'attr' => [
                        'class' => 'si_number'
                    ]
                ])
                ->add('nb_annonces', TextareaType::class, [
                    'label'       => 'Commentaires',
                    'required' => false,
                    'attr' => [
                        'rows' => 5
                    ]
                ])
                ->add('receive_infos', CheckboxType::class, [
                    'label'    => 'Je souhaite recevoir des informations de votre site (promotions, news...)',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Valider',
                    'attr' => [
                        'class'   => 'btn btn-success'
                    ]
                ])
            ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'contact_portal_admin';
    }
}