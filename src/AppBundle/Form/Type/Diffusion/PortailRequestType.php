<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Diffusion;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortailRequestType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        parent::buildForm($builder, $options);
            $builder
                ->add('zoneGeographique', ChoiceType::class, [
                    'label'       => 'portal.zone_geographique',
                    'required' => false,
                    'choices' => Portal::ZONES,
                    'empty_value' => '--- Zones Géographique ---'
                ])
                ->add('cluster', ChoiceType::class, [
                    'label'       => 'portal.type',
                    'empty_value' => '--- Types ---',
                    'required' => false,
                    'choices' => Portal::CLUSTER
                ])
                ->add('typeDiffusion', ChoiceType::class, [
                    'label'       => 'portal.type_diffusion',
                    'multiple'    => true,
                    'required' => false,
                    'choices' => Portal::TYPES,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('categories', ChoiceType::class, [
                    'label'       => 'portal.categories_principales',
                    'empty_value' => null,
                    'required' => false,
                    'multiple'    => true,
                    'choices' => Portal::CATEGORIES,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Appliquer',
                    'attr' => [
                        'class'   => 'btn btn-success'
                    ]
                ])
            ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'diffusion_search';
    }
}