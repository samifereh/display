<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Group;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\GroupAdvantageDiscount;
use AppBundle\Repository\Configuration\ConfigurationRepository;
use AppBundle\Repository\GroupAdvantageDiscountRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAdvantagesDiscountType extends AbstractType
{
    private $discounts;
    private $currentDiscount;
    private $configurationRepository;
    private $advantages;
    private $group;

    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository        = $configurationRepository;
        $this->advantages                     = $this->configurationRepository->getConfiguration()->getAdvantages();
    }

    private function getDiscountList() {
        $this->discounts = new ArrayCollection();
        $type = $this->group->hasRole('ROLE_GROUP') ? 'group' : 'agence';
        foreach ($this->advantages as $advantage) {
            if($advantage->getGroupType() == $type) {
                $haveAdvantageDiscount = false;
                foreach ($this->currentDiscount as $discount) {
                    if($discount->getAdvantage()->getId() == $advantage->getId()) {
                        $this->discounts->add($discount);
                        $haveAdvantageDiscount = true;
                        break;
                    }
                }
                if(!$haveAdvantageDiscount)
                    $this->discounts->add((new GroupAdvantageDiscount())->setAdvantage($advantage));
            }
        }
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->group           = $builder->getData();
        $this->currentDiscount = $builder->getData()->getAdvantagesDiscount();
        $this->getDiscountList();
        parent::buildForm($builder, $options);
        #### Advantages ####
        $builder->add('advantagesDiscount', CollectionType::class,
            [
                'label'      =>  /** @Ignore */ false,
                'entry_type' => GroupAdvantageDiscountType::class,
                'prototype_data' => new GroupAdvantageDiscount(),
                'data'           => $this->discounts,
                'by_reference' => false,
                'allow_delete' => true,
                'allow_add'    => false
            ]
        );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Group::class,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group_advantages';
    }
}