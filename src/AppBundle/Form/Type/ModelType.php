<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\HouseModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;
use Symfony\Component\HttpFoundation\File\File;

class ModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $model = $builder->getData();
        switch ($options['flow_step']) {
            case 1:
                $builder->add('name', TextType::class,
                    [
                        'label' => 'model.nom',
                        'attr' => [
                            'class' => 'model_name'
                        ],
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['groups'=>["Default",'flow_model_step1'],'message' => "form.field_is_required" ] )
                        ]
                    ]
                )
                ->add('titreImagePrincipale', TextType::class,
                    [
                        'label' => 'ads.ad.principale_image_title',
                        'required' => false
                    ]);
                $groups = $options['checkImageEmpty'] ? ["Default",'flow_model_step1'] : ['Default'];
                $builder->add('imagePrincipale', ImageUploadableType::class, [
                    'data' => new File($model->getImagePrincipale(), false),
                    'required' => true,
                    'label'   => 'ads.ad.principale_image', //Image principale <p>("image/jpeg", "image/pjpeg", "image/bmp")</p>
                    'attr' => [
                        'class' => 'col-md-12 filestyle',
                        'data-buttonText' => 'choose_file'
                    ],
                    'error_bubbling' => false,
                    'constraints' => [
                        new Assert\NotBlank( ['groups' =>$groups,  'message'   => "form.field_is_required" ] ),
                        new Assert\File( ['groups'     => $groups, 'mimeTypes' => ["image/jpeg", "image/pjpeg"] ] )
                    ]
                ]);

                $builder->add('maison', MaisonModelType::class, ['label' => false,'flow_step'=>$options['flow_step'] ]);
                break;
            case 2:
                //Submit
                $builder->add('maison', MaisonModelType::class, ['label' => false, 'flow_step'=>$options['flow_step']]);

                break;
        }
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'class_data'         => HouseModel::class,
            'cascade_validation' => true,
            'checkImageEmpty'    => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'model';
    }
}