<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\BienImmobilier;
use AppBundle\Entity\Group;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Fields\SiNumberType;
use AppBundle\Form\Type\Fields\SurfaceNumberType;
use AppBundle\Form\Type\Fields\SurfaceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class MaisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'ads.ad.type_maison',
                'required' => true,
                'choices' => Maison::HOUSE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('typeArchitecture', ChoiceType::class, [
                'label' => "ads.ad.type_architecture",
                'required' => true,
                'choices' => Maison::ARCHITECTURE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('surface', TextType::class,
                [
                    'label' => 'ads.ad.surface',
                    'required' => true,
                    'attr' => ['class' => 'surface'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbPieces', TextType::class,
                [
                    'label' => 'ads.ad.pieces_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\Length( [ 'groups' => $options['groups'],'min' => 1, 'max' => 7 ] ),
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbChambres', TextType::class,
                [
                    'label' => 'ads.ad.rooms_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbSalleBain', TextType::class,
                [
                    'label' => 'ads.ad.bathroom_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'], 'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('siSalleEau', SiNumberType::class,
                [
                    'label' => "ads.ad.salle_eau",
                    'required' => true,
                    'constraints' => [
                        new AppAssert\SiNumber(['groups' => $options['groups']])
                    ]
                ])
            ->add('siSalon', ChoiceType::class, [
                    'label' => "ads.ad.salon",
                    'required' => false,
                    'choices' =>  [true => 'Oui',false => 'Non'],
                    'expanded' => true,
                    'empty_value' => null,
                    'data'        => false
            ])
            ->add('siParking', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.parking",
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] ),
//                        new AppAssert\SurfaceNumber(['groups' => $options['groups']])
                    ]
                ])
            ->add('description', TextareaType::class, [
                'label' => "ads.ad.description",
                'required' => true,
                'attr' => [
                    'rows' => '20'
                ]
            ]);


        /******************** Optional *************************/
        $builder->add('siToilette', SiNumberType::class,
            [
                'label' => "ads.ad.wc",
                'required' => false,
            ])
            ->add('siSejour', SurfaceType::class,
            [
                'label' => "ads.ad.livingroom",
                'required' => false
            ])
            ->add('siSalleAManger', SurfaceType::class,
            [
                'label' => "ads.ad.dinningroom",
                'required' => false,
            ])
            ->add('typeCuisine', ChoiceType::class, [
                'label' => "ads.ad.kitchen_type",
                'required' => false,
                'empty_value'  => null,
                'choices' => Maison::KITCHEN_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('typeCharpante', ChoiceType::class, [
                'label' => "ads.ad.type_charpante",
                'required' => false,
                'choices' => Maison::CHARPENTE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('typeTuile', ChoiceType::class, [
                'label' => "ads.ad.type_tuile",
                'required' => false,
                'choices' => Maison::TUILE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('materiauxTuile', ChoiceType::class, [
                'label' => "ads.ad.materiaux_tuile",
                'required' => false,
                'choices' => Maison::MATERIAUX_TUILE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('siBureau', SurfaceType::class,
                [
                    'label' => "ads.ad.bureau",
                    'required' => false
                ])
            ->add('siBuanderie', SurfaceType::class,
                [
                    'label' => "ads.ad.buanderie",
                    'required' => false
                ])
            ->add('siTerrasse', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.terrasse",
                    'required' => false,
                ])

            ->add('siDressing', SurfaceType::class,
                [
                    'label' => "ads.ad.dressing",
                    'required' => false
                ])
            ->add('siBalcons', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.balcons",
                    'required' => false,
                ])
            ->add('siCave', SurfaceType::class,
                [
                    'label' => "ads.ad.cave",
                    'required' => false
                ])
            ->add('siVeranda', SurfaceType::class,
                [
                    'label' => "ads.ad.veranda",
                    'required' => false
                ])
            ->add('siCheminee', SiNumberType::class,
                [
                    'label' => "ads.ad.cheminee",
                    'required' => false
                ])
            ->add('siPlacards', SiNumberType::class,
                [
                    'label' => "ads.ad.placards",
                    'required' => false
                ])
                ->add('siJardin', CheckboxType::class, [
                    'label'    => 'ads.ad.jardin',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('typeJardin', ChoiceType::class, [
                    'label' => 'ads.ad.type_jardin',
                    'required' => false,
                    'choices' => [
                        'non privatif'      => 'Non privatif',
                        'privatif'          => 'Privatif',
                    ],
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
            ->add('revetementDeSol', ChoiceType::class, [
                'label' => "ads.ad.revetement_du_sol",
                'required' => false,
                'choices' => Maison::REVETEMENT_SOL,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('DPE', ChoiceType::class, [
                'label' => "ads.ad.bilan_consomation_energie",
                'required' => false,
                /** @Ignore */'choices' => Ad::CONSOMATION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('dpeValue', NumberType::class,
            [
                'label' => 'ads.ad.consomation_energie', // Consommation énergie (kWhEP/m2/an)
                'required' => false
            ])
            ->add('GES', ChoiceType::class, [
                'label' => "ads.ad.bilan_emission_ges",
                'required' => false,
                /** @Ignore */'choices' => Ad::CONSOMATION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('gesValue', NumberType::class,
                [
                    'label' => 'ads.ad.emission_ges', //Emissions GES (kg éqCO2/m2/an)
                    'required' => false
                ])
            ->add('typeDeChauffage', ChoiceType::class, [
                'label' => 'ads.ad.type_chauffage',
                'required' => false,
                'choices' => Maison::CHAUFFAGE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('energieDeChauffage', ChoiceType::class, [
                'label' => 'ads.ad.energie_de_chauffage',
                'required' => false,
                'choices' => Maison::ENERGIE_CHAUFFAGE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('vitrage', ChoiceType::class, [
                'label' => 'program.vitrage',
                'required' => false,
                'choices' => Maison::VITRAGES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('zinguerie', ChoiceType::class, [
                'label' => "ads.ad.zinguerie",
                'required' => false,
                'choices' => Maison::ZINGUERIE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('menuiserie', ChoiceType::class, [
                'label' => "ads.ad.menuiserie",
                'required' => false,
                'choices' => Maison::MENUISERIE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('volets', ChoiceType::class, [
                'label' => "ads.ad.volets",
                'required' => false,
                'choices' => Maison::VOLETS,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('porteGarage', ChoiceType::class, [
                'label' => "ads.ad.porteGarage",
                'required' => false,
                'choices' => Maison::PORTES_GARAGE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('siAlarme', ChoiceType::class, [
                'label' => "ads.ad.alarme",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' => null,
                'data'        => false
            ]);

            $builder->add('investisseur', CheckboxType::class, [
                'label'    => 'ads.ad.investisseur',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('habitat', CheckboxType::class, [
                'label'    => 'ads.ad.habitat',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('prestige', CheckboxType::class, [
                'label'    => 'ads.ad.prestige',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);

            $builder->add('siVoletsRoulantsElectriques', CheckboxType::class, [
                'label'    => 'ads.ad.volet_roulants_electriques', //Volet roulants electriques
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siClimatise', CheckboxType::class, [
                'label'    => 'ads.ad.climatise',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siTV', CheckboxType::class, [
                'label'    => 'ads.ad.TV',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siPMR', CheckboxType::class, [
                'label'    => 'ads.ad.PMR',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siPiscine', CheckboxType::class, [
                'label'    => 'ads.ad.piscine',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('environnement', ChoiceType::class, [
                'label' => 'ads.ad.environnement',
                'required' => false,
                'choices' => Terrain::TERRAIN_ENV,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('exposition', ChoiceType::class, [
                'label' => 'ads.ad.exposition',
                'required' => false,
                'choices' => Terrain::EXPOSITION,
                'attr' => [
                    'class' => 'select2'
                ]
            ]);

            $builder->add('siProximiteAeroport', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_aeroport',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteSortieAutoroute', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_sortie_autoroute',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteGare', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximité_gare',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteBus', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximité_bus',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteTramway', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_tramway',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteParking', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_parking',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteCommerces', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_commerces',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteCentreCommercial', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_centre_commercial',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteCentreville', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_centre_ville',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteCreche', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_creche',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteEcolePrimaire', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_ecole_primaire',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteCollege', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_college',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteLycee', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_lycee',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteUniversite', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_universite',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteEspacesVerts', CheckboxType::class, [
                'label'    => 'ads.ad.proximite.proximite_espaces_verts',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siProximiteMetro', CheckboxType::class, [
                'label'    => 'ads.ad.procheMetro',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);


        /*Avantages environnementaux*/
        $builder->add('siEcoQuartier', CheckboxType::class, [
            'label'    => 'ads.ad.env.eco_quartier',
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])->add('siBioConstruction', CheckboxType::class, [
            'label'    => 'ads.ad.env.bio_construction',
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ]);

        /*Avantages Investissement*/
        $builder->add('loiPinel', CheckboxType::class, [
            'label'    => 'ads.ad.avantages.loi_pinel',
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])
            ->add('loiDuflot', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.loi_duflot',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('censiBouvard', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.ensi_bouvard',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('LMP_LMNP', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.LMP',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('loi_Girardin_Paul', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.loi_girardin_paul',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('loi_Malraux', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.loimalraux',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('loi_Demessine', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.loi_demessine',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('regime_SCPI', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.regime_SCPI',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('nue_Propriete', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.nue_propriete',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('pret_Locatif_Social', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.pret_locatif_social',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('PSLA', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.pret_social_location_accession', //(PSLA)
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('monumentHistorique', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.monument_historique',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('elligiblePret', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.elligible_pret_taux_zero',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('tva_1', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.tva_5',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('tva_2', CheckboxType::class, [
                'label'    => 'ads.ad.avantages.tva_7',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);

        /*Certifications et labels*/
        $builder->add('hauteQualiteEnvironnementale', CheckboxType::class, [
            'label'    => 'ads.ad.labels.haute_qualite_environnementale',
            'required' => false,
            'attr' => [
                'class' => 'i-checks'
            ]
        ])
            ->add('nfLogement', CheckboxType::class, [
                'label'    => 'ads.ad.labels.nf_logement',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('labelHabitatEnvironnement', CheckboxType::class, [
                'label'    => 'ads.ad.labels.label_habitat_environnement',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('THPE', CheckboxType::class, [
                'label'    => 'ads.ad.labels.label_THPE',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('HPE_ENR', CheckboxType::class, [
                'label'    => 'ads.ad.labels.label_HPE_ENR',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('labelBatimentBasseConsommation', CheckboxType::class, [
                'label'    => 'ads.ad.labels.label_batiment_basse_consommation',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('labelHabitatEnvironnementEffinergie', CheckboxType::class, [
                'label'    => 'ads.ad.labels.label_habitat_environnement_effinergie',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('planLocalHabitat', CheckboxType::class, [
                'label'    => 'ads.ad.labels.plan_local_habitat',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('RT2012', CheckboxType::class, [
                'label'    => 'ads.ad.labels.RT2012',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);

        $builder
            ->add('toitTerrasse', CheckboxType::class, [
                'label'    => 'ads.ad.toitTerrasse',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('grenier', ChoiceType::class, [
                'label' => 'ads.ad.grenier',
                'required' => false,
                'choices' => BienImmobilier::GARNIER,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('degagement', CheckboxType::class,
                [
                    'label' => "ads.ad.degagement",
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
            ])
            ->add('exposition', ChoiceType::class, [
                'label' => 'ads.ad.exposition',
                'required' => false,
                'choices' => BienImmobilier::EXPOSITION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('vis_a_vis', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.vis_a_vis",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
        ;

        $builder->add('_type', HiddenType::class, array(
                 'data'   => 'maison',
                 'mapped' => false
             ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'isDuo'           => false,
            'index_property'  => null,
            'data_class'      => Maison::class,
            'model_class'     => Maison::class,
            'groups'          => ['Default'],
            'groups_terrain'          => null,
            'translation_domain' => 'commiti'
        ));
    }

    public function getBlockPrefix()
    {
        return 'maison';
    }
}