<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Group;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Form\Validator\Constraints\UniqueField;
use AppBundle\Form\Validator\Constraints\UniquePrefix;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupsAgencyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $user = $options['user'];
        $builder->add('name',  TextType::class, [
            'label'     => 'group.organisation',
            'constraints' => [
                new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
            ]
        ])
            ->add('prefix', TextType::class, [
                'label'    => 'group.prefix',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['groups' => ['Options'], 'message' => "form.field_is_required" ] ),
                    new Assert\Length(['groups' => ['Options'], 'min' => 4, 'max' => 4]),
                    new UniqueField(['groups' => ['Options'], 'message' => 'form.prefix_already_used', 'field_name' => 'prefix', 'repository' => 'AppBundle:Group'])
                ]
            ])
            ->add('agency', AgencyType::class, [
                'label'    => 'group.details',
                'required' => false,
                'avatar' => $options["avatar"]
            ])
        ;

        if($user && $user->hasRole('ROLE_MARKETING') && count($user->getGroups()) > 1) {
            $builder
                ->add('parentGroup', EntityType::class, [
                    'class' => Group::class,
                    'label' => 'Groupe',
                    'choice_label'  => 'name',
                    'placeholder' => '',
                    'query_builder' => function (EntityRepository $qb) use ($user) {
                        $groups = [];
                        foreach ($user->getGroups() as $g) {
                            foreach ($g->getAuthorizedAgency() as $ga) {
                                if(is_null($ga->getGroup())) {
                                    $groups[] = $g->getId();
                                    continue;
                                }
                            }
                        }
                        return $qb->createQueryBuilder('g')
                            ->where('g.roles LIKE :roles')
                            ->setParameter('roles', '%ROLE_GROUP%')
                            ->andWhere('g.id IN (:groups)')
                            ->setParameter('groups',$groups);
                    },
                    'attr'     => [
                        'class' => 'table table-bordered table-striped',
                    ],
                    'multiple' => false,
                    'expanded' => false
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Group::class,
            'translation_domain' => 'commiti',
            'user' => null,
            'avatar' => null,
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                if(in_array('ROLE_AGENCY',$data->getRoles()))
                    return array('Default', 'Options');
                elseif(in_array('ROLE_GROUP',$data->getRoles()))
                    return array('Default', 'Group');
                else
                    return array('Default');
            }
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group_agency';
    }
}