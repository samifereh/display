<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserModerationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        /** @var Group $group */
        $group = $builder->getData()->getGroup();
        if($group->hasAnnoncesOption()) {
            $builder
            ->add('houseModelsModeration', CheckboxType::class, [
                'label'    => 'user.moderate_model',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('adsModeration', CheckboxType::class, [
                'label'    => 'user.moderate_ads',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);
        }
        if($group->hasProgramOption()) {
            $builder->add('programModeration', CheckboxType::class, [
                'label' => 'user.moderate_program',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);
        }
        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.assigner',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group';
    }
}