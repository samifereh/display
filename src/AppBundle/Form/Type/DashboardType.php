<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Dashboard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class DashboardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('welcome',TextareaType::class,[
                'label' => 'dashbord.welcome', //Message
                'attr' => [
                    'rows' => '10',
                    'class'=> 'textarea-editor-full'
                ]
            ])
            ->add('nbNews', TextType::class,
                [
                    'label' => 'dashboard.nbNews',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\NotBlank([
                            'groups'=>['Default','flow_model_step1'],
                            'message' => "form.field_is_required"]
                        )
                    ]
                ])
            ->add('faqText', TextareaType::class,[
                'label' => 'dashbord.welcome', //Message
                'attr' => [
                    'rows' => '8',
                    'class'=> 'textarea-editor-full'
                ]
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Dashboard::class,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'Dashboard';
    }


}
