<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use Infinite\FormBundle\Form\Type\PolyCollectionType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class AdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ad = $builder->getData();
        switch ($options['flow_step']) {
            case 1:
                $builder->add('libelle', TextType::class,
                    [
                        'label' => 'ads.ad.title',
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['groups'=>['Default','flow_ad_step1'], 'message' => "form.field_is_required" ] ),
                            new Assert\Length( ['groups'=>['Default','flow_ad_step1'],'max' => 64] )
                        ]
                    ])
                    ->add('referenceInterne', TextType::class,
                        [
                            'label' => 'ads.ad.internal_reference',
                            'required' => false
                        ]
                    )
                    ->add('prix', MoneyType::class,
                        [
                            'label' => 'ads.ad.price',
                            'attr' => [ 'prepend' => true],
                            'required' => true,
                            'constraints' => [
                                new Assert\NotBlank( ['groups'=>['Default','flow_ad_step1'], 'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('pays', ChoiceType::class,
                        [
                            'label' => 'ads.ad.country',
                            'preferred_choices' => ['FR'],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ],
                            'required' => true,
                            'choices'  => [
                               'FR' => 'France'
                            ],
                            'attr' => [ 'class' => 'select2' ]
                        ]
                    )
                    ->add('ville', TextType::class,
                        [
                            'label' => 'ads.ad.city',
                            'required' => true,
                            'attr' => [
                                'class' => 'city-list',
                                'typeahead' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('codePostal', TextType::class,
                        [
                            'label' => 'ads.ad.zip_code',
                            'required' => true,
                            'attr' => [
                                'class' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\Length(['groups'=>['Default','flow_ad_step1'],'min'=>4,'max'=>6]),
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('adresse', TextType::class,
                        [
                            'label' => 'ads.ad.adresse',
                            'required' => false
                        ]
                    )
                    ->add('lienVisitVirtuel', TextType::class,
                        [
                            'label' => 'ads.ad.virtual_visit_link',
                            'required' => false,
                            'constraints' => [
                                new Assert\Url(['groups'=>['Default','flow_ad_step1']])
                            ]
                        ]
                    )
                    ->add('description', TextareaType::class, [
                        'label' => "ads.ad.description",
                        'required' => true,
                        'attr' => [
                            'rows' => '20'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                        ]
                    ]);

                    $builder->add('titreImagePrincipale', TextType::class, [
                        'label'         => 'ads.ad.principale_image_title',
                        'required'      => false
                    ]);
                    $groups = $options['checkImageEmpty'] ? ["Default",'flow_ad_step1'] : ['Default'];
                    $builder->add('imagePrincipale', ImageUploadableType::class, [
                        'data' => new File($ad->getImagePrincipale(), false),
                        'required' => true,
                        'label'    => 'ads.ad.principale_image',
                        'attr' => [
                            'class' => 'col-md-12 filestyle',
                            'data-buttonText' => 'ads.ad.choose_file'
                        ],
                        'error_bubbling' => false,
                        'constraints' => [
                            new Assert\File( ['groups'=>$groups, 'mimeTypes' => ["image/jpeg", "image/pjpeg"] ] )
                        ]
                    ]);
                break;
            case 2:
                $isDuo = $ad->getTypeDiffusion() == 'duo' ? true : false;
                $builder->add('bienImmobilier', PolyCollectionType::class, array(
                    'types' => array(
                        MaisonType::class,
                        TerrainType::class,
                        AppartementType::class
                    ),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => ['isDuo' => $isDuo, 'groups'=> ['Default','flow_ad_step2']]

                ));
                break;
        }
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'isDuo'           => false,
            'bienImage'       => null,
            'checkImageEmpty' => true,
            'data_class'      => Ad::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'ad';
    }

}