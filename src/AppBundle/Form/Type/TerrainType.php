<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\BienImmobilier;
use AppBundle\Entity\Group;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Fields\SiChoicesType;
use AppBundle\Form\Type\Fields\SiNumberType;
use AppBundle\Form\Type\Fields\SurfaceNumberType;
use AppBundle\Form\Type\Fields\SurfaceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class TerrainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        if($options['isDuo']) {
        $builder->add('title', TextType::class,
            [
                'label' => 'ads.bien.terrain.title',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['groups' => $options['groups_terrain'] ? $options['groups_terrain'] : $options['groups_terrain'] ? $options['groups_terrain'] : $options['groups'], 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('prix', MoneyType::class,
                [
                    'label' => 'ads.ad.price',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['groups' => $options['groups_terrain'] ? $options['groups_terrain'] : $options['groups_terrain'] ? $options['groups_terrain'] : $options['groups'], 'message' => "form.field_is_required" ] )
                    ]
                ]
            )
            ->add('titreImagePrincipale', TextType::class, [
                'label'         => 'ads.ad.principale_image_title',
                'required'      => false,
                'mapped'        => true
            ])
            ->add('imagePrincipale', ImageUploadableType::class, [
                'data' => new File('', false),
                'required' => true,
                'label'    => 'ads.ad.principale_image',
                'attr' => [
                    'class' => 'col-md-12 filestyle',
                    'data-buttonText' => 'choose_file'
                ],
                'error_bubbling' => false,
                'mapped'        => true,
                'constraints' => [
                    new Assert\File( ['mimeTypes' => ["image/jpeg", "image/pjpeg"] ] )
                ]
            ]);
        }
            //todo photos terrain
        $builder->add('type', ChoiceType::class, [
                'label'         => 'ads.ad.type_terrain',
                'required'      => false,
                'empty_value'   => '',
                'choices'       => Terrain::TERRAIN_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('surface', TextType::class,
                [
                    'label' => 'ads.ad.surface',
                    'required' => true,
                    'attr' => ['class' => 'surface'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups_terrain'] ? $options['groups_terrain'] : $options['groups'], 'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('assainissementEauUsees', ChoiceType::class, [
                'label' => 'ads.ad.assainissement_eau_usses',
                'required' => false,
                'choices' => Terrain::ASSAINISSEMENT_EAU_USEES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('assainissementEauPluviale', ChoiceType::class, [
                'label' => 'ads.ad.assainissement_eau_pluviale',
                'required' => false,
                'choices' => Terrain::ASSAINISSEMENT_EAU_PLUVIALE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('certificatUrbanismeDelivrer', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.certificat_urbanisme_delivrer",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('surfacePlancher', TextType::class,
                [
                    'label' => 'ads.ad.surface_plancher',
                    'required' => false,
                    'attr' => ['class' => 'surface']
            ])
            ->add('exposition', ChoiceType::class, [
                'label' => 'ads.ad.exposition',
                'required' => false,
                'choices' => Terrain::EXPOSITION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('environnement', ChoiceType::class, [
                'label' => 'ads.ad.environnement',
                'required' => false,
                'choices' => Terrain::TERRAIN_ENV,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('belleVue', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.nice_view",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('vis_a_vis', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.vis_a_vis",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('calme', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.calme",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteBus', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheBus",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteTramway', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheTram",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteMetro', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheMetro",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteGare', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheGare",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteCommerces', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheCommerces",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteCentreville', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.proximite_centre_ville",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteCentreCommercial', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.proximite_centre_commercial",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteCreche', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheCreche",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
            ])
            ->add('siProximiteEcolePrimaire', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_ecole_primaire',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteCollege', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_college',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteLycee', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_lycee',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteUniversite', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_universite',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteAeroport', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_aeroport',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteSortieAutoroute', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_sortie_autoroute',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteGare', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximité_gare',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteBus', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximité_bus',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_value' =>false
            ])
            ->add('siProximiteMetro', ChoiceType::class, [
                'empty_value' =>false,
                'label' => 'ads.ad.proximite.proximite_metro',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siProximiteParking', ChoiceType::class, [
                'label'    => 'ads.ad.proximite.proximite_parking',
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'empty_value' =>false,
                'expanded' => true
            ])
            ->add('siProximiteEspacesVerts', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.procheEspaceVerts",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('libreConstruction', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.libreConstruction",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siViabilise', SiChoicesType::class,
                [
                    'label' => "ads.ad.viabilise",
                    'required' => false,
                    'choices' =>  Terrain::VIABILISE,
                ])
            ->add('constructible', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.constructible",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('servitude', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.servitude",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('pente', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.pente",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('_type', HiddenType::class, array(
                'data'   => 'terrain',
                'mapped' => false
            ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'isDuo'       => false,
            'data_class'  => Terrain::class,
            'model_class' => Terrain::class,
            'step_type'   => null,
            'groups'      => ['Default'],
            'groups_terrain'      => null,
            'translation_domain' => 'commiti'
        ));
    }

    public function getBlockPrefix()
    {
        return 'terrain';
    }
}