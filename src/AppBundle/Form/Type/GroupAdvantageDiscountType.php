<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\GroupAdvantageDiscount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAdvantageDiscountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder
                ->add('percent', PercentType::class, [
                    'label'    => false,
                    'required' => false,
                    'type'     => 'integer',
                    'attr'     => ['prepend' => true]
                ])
                ->add('dateBeginDiscount', DateTimeType::class, [
                    'label'    => false,
                    'html5' => true,
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => [
                        'class' => 'datetimepicker',
                        'autocomplete' => 'off'
                    ]
                ])
                ->add('dateEndDiscount', DateTimeType::class, [
                    'label'    => false,
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => [
                        'class' => 'datetimepicker',
                        'autocomplete' => 'off'
                    ]
                ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => GroupAdvantageDiscount::class,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName()
    {
        return 'group_advantage_discount';
    }
}