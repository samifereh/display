<?php

namespace AppBundle\Form\Type\Fields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SurfaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('si', ChoiceType::class, [
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true,
                'empty_data' => null

            ])
            ->add('surface', TextType::class,
                [
                    'attr' => ['class' => 'surface'],
                    'empty_data' => null
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['required' => false,'error_bubbling' => false]);
    }

}
