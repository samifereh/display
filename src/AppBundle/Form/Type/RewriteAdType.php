<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ad;
use AppBundle\Entity\AdRewrite;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RewriteAdType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $data = $builder->getData();
        $currentGroup = $data ? $data->getGroup() : null;
        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'ad_rewrite.title',
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                    ]
            ])
            ->add('description', TextareaType::class, [
                'label' => "ad_rewrite.description",
                'required' => true,
                'attr' => [
                    'rows' => '20',
                    'class'=> 'summernote-textarea'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('portals', EntityType::class, [
                'label' => 'ad_rewrite.portals_list',
                'class' => Portal::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($currentGroup) {
                    if($currentGroup) {
                        return $qb->createQueryBuilder('p')
                            ->leftJoin('p.broadcastPortalRegistrations','bpr')
                            ->andWhere('bpr.group = :group')
                            ->andWhere('p.isAvailable = 1')
                            ->setParameter('group',$currentGroup)
                            ;
                    } else {
                        return $qb->createQueryBuilder('p')
                            ->andWhere('p.isAvailable = 1');
                    }

                },
                'multiple'   => true,
                'expanded'   => false,
                'empty_data' => null,
                'required'   => false,
                'attr' => [
                    'class'=> 'select2'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label'       => 'Appliquer',
                'attr' => [
                    'class'   => 'btn btn-success btn-block'
                ]
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AdRewrite::class,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName()
    {
        return 'ad_rewite';
    }
}