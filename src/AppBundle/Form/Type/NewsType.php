<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder->add('title',  TextType::class,['label' => 'news.title'])
                    ->add('date', DateType::class, [
                        'label' => 'news.date_creation',//Date de création
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'datepicker',
                            'autocomplete' => 'off'
                        ]
                    ])
                    ->add('content',  TextareaType::class,[
                        'label' => 'news.message', //Message
                        'attr' => [
                            'rows' => '20',
                            'class'=> 'textarea-editor-full'
                        ]
                    ]);

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => News::class,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'news';
    }
}