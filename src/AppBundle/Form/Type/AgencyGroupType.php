<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Group;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgencyGroupType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data   = null;
        parent::buildForm($builder, $options);
        $builder->add('parentGroup', EntityType::class, [
                    'label' => 'Group',
                    'class' => Group::class,
                    'choice_label'  => 'name',
                    'query_builder' => function (EntityRepository $qb) {
                        return $qb->findGroups(false);
                    },
                    'expanded'   => false,
                    'empty_data' => null,
                    'required'   => false,
                ]);
        $builder
            ->add('submit', SubmitType::class, [
                'label'       => 'Mettre à jour groupe',
                'attr' => [
                    'class'   => 'btn btn-primary'
                ]
            ]);
    }




    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Group::class,
            'translation_domain' => 'commiti',
            'cascade_validation' => true
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'agency_group';
    }
}