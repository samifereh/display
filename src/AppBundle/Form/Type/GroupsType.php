<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\GroupAdvantageDiscount;
use AppBundle\Form\Type\Admin\GroupAdvantagesType;
use AppBundle\Form\Type\Admin\GroupAdvantageType;
use AppBundle\Form\Validator\Constraints\UniqueField;
use AppBundle\Form\Validator\Constraints\UniquePrefix;
use AppBundle\Repository\Configuration\ConfigurationRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupsType extends AbstractType
{
    private $advantages;
    private $groupAdvantages;
    private $currentGroupAdvantages = null;
    private $configurationRepository;
    private $type;
    private $group;

    private $discounts;
    private $currentDiscount;

    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository        = $configurationRepository;
        $this->advantages                     = $this->configurationRepository->getConfiguration()->getAdvantages();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $this->group = $builder->getData();

        $builder->add('name',  TextType::class, [
            'label'     => 'Nom de l\'organisation',
            'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('roles', 'choice',
                array(
                    'label' => 'Roles',
                    'choices' =>  array_combine(Group::ROLES, Group::ROLES), // array_combine : put values to keys
                    'multiple'  => true,
                    'attr' => [
                        'class' => 'select2-once'
                    ],
                    'constraints' => [
                        new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                    ]
                )
            )
            ->add('prefix', TextType::class, [
                'label'    => 'Prefix (4 caractères)',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['Prefix'], 'message' => "form.field_is_required" ] ),
                    new Assert\Length(['groups' => ['Prefix'], 'min' => 4, 'max' => 4]),
                    new UniqueField(['groups' => ['Prefix'], 'message' => 'Ce prefix est déjà utiliser', 'field_name' => 'prefix', 'repository' => 'AppBundle:Group'])
                ]
            ])
            ->add('annonces_option', CheckboxType::class, [
                'label'    => 'Constructeur de maison individuelles (CMI)',
                'attr' => [
                    'class' => 'i-checks'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['Options'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('program_option', CheckboxType::class, [
                'label'    => 'Promoteurs ou lotisseurs',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks',
                    'dependency' => '#program_options'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['Options'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('suivi_option', CheckboxType::class, [
                'label'    => 'Suivi commercial',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('programMaisonOption', CheckboxType::class, [
                'label'    => 'Maisons',
                'required' => false,
                'attr' => [
                    'class'      => 'i-checks'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['ProgramOptions'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('programAppartementOption', CheckboxType::class, [
                'label'    => 'Appartements',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['ProgramOptions'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('programTerrainOption', CheckboxType::class, [
                'label'    => 'Terrains',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks',
                    'dependency' => 'program_option'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['ProgramOptions'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('programMixteOption', CheckboxType::class, [
                'label'    => 'Mixte (appartements + maisons)',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['ProgramOptions'],'message' => "Vous devez choisir au moin une option" ] )
                ]
            ])
            ->add('parentGroup', EntityType::class, [
                'class' => Group::class,
                'label' => 'Groupe',
                'choice_label'  => 'name',
                'placeholder' => '',
                'query_builder' => function (EntityRepository $qb) {
                    return $qb->createQueryBuilder('u')
                        ->where('u.roles LIKE :roles')
                        ->setParameter('roles', '%ROLE_GROUP%');
                },
                'attr'     => [
                    'class' => 'table table-bordered table-striped',
                ],
                'multiple' => false,
                'expanded' => false
            ])
            ->add('diffusionOption', CheckboxType::class, [
                'label'    => 'Activer la diffusion',
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
        ;



        #### IMPORT D'ANNONCES ####
        /*$builder
            ->add('importHost', TextType::class, [
                'label'    => 'Host',
                'required' => false
            ])
            ->add('importUser', TextType::class, [
                'label'    => 'Nom d\'utilisateur',
                'required' => false
            ])
            ->add('importPassword', TextType::class, [
                'label'    => 'Mot de passe',
                'required' => false
            ])
            ->add('importFileName', TextType::class, [
                'label'    => 'Nom du fichier',
                'required' => false
            ]);*/

        ### Agence ###
        $builder->add('agency', AgencyType::class, [
            'label'    => false,
            'data'     => $this->group->getAgency(),
            'edit'     => $options['edit'],
            'isAdmin'  => true
        ]);

        #### Advantages Discount ####
        $this->currentDiscount = $builder->getData()->getAdvantagesDiscount();
        $this->getDiscountList();
        $builder->add('advantagesDiscount', CollectionType::class,
            [
                'label'      =>  /** @Ignore */ false,
                'entry_type' => GroupAdvantageDiscountType::class,
                'prototype_data' => new GroupAdvantageDiscount(),
                'data'           => $this->discounts,
                'by_reference' => false,
                'allow_delete' => true,
                'allow_add'    => false,
                'block_name'   => 'group_advantages_discount'
            ]
        );

        $formModifier = function (FormInterface $form, $roles = null) {
            if($roles) {
                $this->type = in_array('ROLE_GROUP',$roles) ? 'group' : 'agence';
                $this->currentGroupAdvantages = $this->group->getAdvantages();
                $this->getGroupAdvantagesList();
                #### Advantages ####
                $form->add('advantages', CollectionType::class,
                    [
                        'label'      =>  /** @Ignore */ false,
                        'entry_type' => GroupAdvantageType::class,
                        'prototype_data' => new GroupAdvantage(),
                        'data'           => $this->groupAdvantages,
                        'by_reference' => false,
                        'allow_delete' => false,
                        'allow_add'    => false,
                        'block_name'   => 'group_advantages'
                    ]
                );
            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $group = $event->getData();
                $formModifier($event->getForm(), $group->getRoles());
            }
        );

        $builder->get('roles')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $roles = $event->getForm()->getData();
                $formModifier($event->getForm()->getParent(), $roles);
            }
        );
    }

    private function getGroupAdvantagesList() {
        $this->groupAdvantages = new ArrayCollection();
        /** @var Advantage $advantage */
        foreach ($this->advantages as $advantage) {
            if($advantage->getGroupType() == $this->type && !$advantage->getRecurrent()) {
                $haveGroupAdvantage = false;
                if($this->currentGroupAdvantages) {
                    foreach ($this->currentGroupAdvantages as $groupAdvantage) {
                        if($groupAdvantage->getAdvantage()->getId() == $advantage->getId()) {
                            $this->groupAdvantages->add($groupAdvantage);
                            $haveGroupAdvantage = true;
                            break;
                        }
                    }
                }
                if(!$haveGroupAdvantage)
                    $this->groupAdvantages->add((new GroupAdvantage())->setAdvantage($advantage)->setEnabled(true)->setPayed(true));
            }
        }
    }

    private function getDiscountList() {
        $this->discounts = new ArrayCollection();
        $type = $this->group->hasRole('ROLE_GROUP') ? 'group' : 'agence';
        foreach ($this->advantages as $advantage) {
            if($advantage->getGroupType() == $type) {
                $haveAdvantageDiscount = false;
                if($this->currentDiscount) {
                foreach ($this->currentDiscount as $discount) {
                    if($discount->getAdvantage()->getId() == $advantage->getId()) {
                        $this->discounts->add($discount);
                        $haveAdvantageDiscount = true;
                        break;
                    }
                }
                }
                if(!$haveAdvantageDiscount)
                    $this->discounts->add((new GroupAdvantageDiscount())->setAdvantage($advantage));
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Group::class,
            'translation_domain' => 'commiti',
            'edit' => true,
            'isAdmin' => false,
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                $groups = ['Default'];
                if(in_array('ROLE_AGENCY',$data->getRoles()) && (!$data->getProgramOption() && !$data->getAnnoncesOption()))
                    $groups[] = 'Options';
                elseif(in_array('ROLE_GROUP',$data->getRoles()))
                    $groups[] = 'Group';
                elseif($data->getPrefix())
                    $groups[] = 'Prefix';

                if($data->getProgramOption() && (!$data->getProgramMaisonOption() && !$data->getProgramAppartementOption() && !$data->getProgramTerrainOption() && !$data->getProgramMixteOption()))
                    $groups[] = 'ProgramOptions';
                return $groups;
            }
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'group';
    }
}