<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Group;
use AppBundle\Entity\Maison;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Fields\SiNumberType;
use AppBundle\Form\Type\Fields\SurfaceNumberType;
use AppBundle\Form\Type\Fields\SurfaceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class MaisonModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if($options['flow_step'] == 1) {
            $builder
                ->add('type', ChoiceType::class, [
                    'label' => 'ads.ad.type',
                    'required' => true,
                    'choices' => Maison::HOUSE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ],
                    'constraints' => [
                        new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"])
                    ]
                ])
                ->add('typeArchitecture', ChoiceType::class, [
                    'label' => "ads.ad.type_architecture",
                    'required' => true,
                    'choices' => Maison::ARCHITECTURE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ],
                    'constraints' => [
                        new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"])
                    ]
                ])
                ->add('typeCharpante', ChoiceType::class, [
                    'label' => "ads.ad.type_charpante",
                    'required' => false,
                    'choices' => Maison::CHARPENTE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('typeTuile', ChoiceType::class, [
                    'label' => "ads.ad.type_tuile",
                    'required' => false,
                    'choices' => Maison::TUILE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('materiauxTuile', ChoiceType::class, [
                    'label' => "ads.ad.materiaux_tuile",
                    'required' => false,
                    'choices' => Maison::MATERIAUX_TUILE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('zinguerie', ChoiceType::class, [
                    'label' => "ads.ad.zinguerie",
                    'required' => false,
                    'choices' => Maison::ZINGUERIE,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('menuiserie', ChoiceType::class, [
                    'label' => "ads.ad.menuiserie",
                    'required' => false,
                    'choices' => Maison::MENUISERIE,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('volets', ChoiceType::class, [
                    'label' => "ads.ad.volets",
                    'required' => false,
                    'choices' => Maison::VOLETS,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('porteGarage', ChoiceType::class, [
                    'label' => "ads.ad.porteGarage",
                    'required' => false,
                    'choices' => Maison::PORTES_GARAGE,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('surface', TextType::class,
                    [
                        'label' => 'ads.ad.surface',
                        'required' => true,
                        'attr' => ['class' => 'surface'],
                        'constraints' => [
                            new Assert\NotBlank(['message' => "form.field_is_required"])
                        ]
                ])
                ->add('description', TextareaType::class, [
                    'label' => "ads.ad.description",
                    'required' => true,
                    'attr' => [
                        'rows' => '20'
                    ]
                ]);
        }
        if($options['flow_step'] == 2) {
            /******************** Optional *************************/
            $builder->add('siAlarme', ChoiceType::class, [
                    'label' => "ads.ad.alarme",
                    'required' => false,
                    'choices' =>  [true => 'Oui',false => 'Non'],
                    'expanded' => true,
                    'empty_value' => null,
                    'data'        => false
                ])
                ->add('nbPieces', TextType::class,
                    [
                        'label' => 'ads.ad.pieces_number',
                        'required' => true,
                        'attr' => ['class' => 'surface_number'],
                        'constraints' => [
                            new Assert\Length(['groups'=>['Default','flow_model_step1'],'min' => 1, 'max' => 7]),
                            new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"])
                        ]
                    ])
                ->add('nbChambres', TextType::class,
                    [
                        'label' => 'ads.ad.rooms_number',
                        'required' => true,
                        'attr' => ['class' => 'surface_number'],
                        'constraints' => [
                            new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"])
                        ]
                    ])
                ->add('nbSalleBain', TextType::class,
                    [
                        'label' => 'ads.ad.bathroom_number',
                        'required' => true,
                        'attr' => ['class' => 'surface_number'],
                        'constraints' => [
                            new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"])
                        ]
                    ])
                ->add('siSalleEau', SiNumberType::class,
                    [
                        'label' => "ads.ad.salle_eau",
                        'required' => true,
                        'constraints' => [
                            new AppAssert\SiNumber(['groups'=>['Default','flow_model_step1']])
                        ]
                    ])
                ->add('siParking', SurfaceNumberType::class,
                    [
                        'label' => "ads.ad.parking",
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank(['groups'=>['Default','flow_model_step1'],'message' => "form.field_is_required"]),
                            new AppAssert\SurfaceNumber(['groups'=>['Default','flow_model_step1']])
                        ]
                    ])
                ->add('siToilette', SiNumberType::class,
                [
                    'label' => "ads.ad.wc",
                    'required' => false,
                ])
                ->add('siSalleAManger', SurfaceType::class,
                    [
                        'label' => "ads.ad.dinningroom",
                        'required' => false,
                    ])
                ->add('typeCuisine', ChoiceType::class, [
                    'label' => "ads.ad.kitchen_type",
                    'required' => false,
                    'empty_value' => null,
                    'choices' => Maison::KITCHEN_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('siTV', ChoiceType::class, [
                    'label' => "ads.ad.TV",
                    'required' => false,
                    'choices' =>  [true => 'Oui',false => 'Non'],
                    'expanded' => true,
                    'empty_value' => null,
                    'data'        => false
                ])
                ->add('siPMR', ChoiceType::class, [
                    'label' => "ads.ad.PMR",
                    'required' => false,
                    'choices' =>  [true => 'Oui',false => 'Non'],
                    'expanded' => true,
                    'empty_value' => null,
                    'data'        => false
                ])
                ->add('siBureau', SurfaceType::class,
                    [
                        'label' => "ads.ad.bureau",
                        'required' => false
                    ])
                ->add('siBuanderie', SurfaceType::class,
                    [
                        'label' => "ads.ad.buanderie",
                        'required' => false
                    ])
                ->add('siTerrasse', SurfaceNumberType::class,
                    [
                        'label' => "ads.ad.terrasse",
                        'required' => false,
                    ])
                ->add('siDressing', SurfaceType::class,
                    [
                        'label' => "ads.ad.dressing",
                        'required' => false
                    ])
                ->add('siBalcons', SurfaceNumberType::class,
                    [
                        'label' => "ads.ad.balcons",
                        'required' => false,
                    ])
                ->add('siCave', SurfaceType::class,
                    [
                        'label' => "ads.ad.cave",
                        'required' => false
                    ])
                ->add('siVeranda', SurfaceType::class,
                    [
                        'label' => "ads.ad.veranda",
                        'required' => false
                    ])
                ->add('siCheminee', SiNumberType::class,
                    [
                        'label' => "ads.ad.cheminee",
                        'required' => false
                    ])
                ->add('siPlacards', SiNumberType::class,
                    [
                        'label' => "ads.ad.placards",
                        'required' => false
                ])

                ->add('siPiscine', ChoiceType::class, [
                    'label' => "ads.ad.piscine",
                    'required' => false,
                    'choices' =>  [true => 'Oui',false => 'Non'],
                    'expanded' => true,
                    'empty_value' => null,
                    'data'        => false
                ])
                ->add('revetementDeSol', ChoiceType::class, [
                    'label' => "ads.ad.revetement_du_sol",
                    'required' => false,
                    'choices' => [
                        'béton ciré' => 'béton ciré',
                        'carrelage' => 'carrelage',
                        'fibre végétale' => 'fibre végétale',
                        'linoléum' => 'linoléum',
                        'moquette' => 'moquette',
                        'PVC' => 'PVC',
                        'marbre' => 'marbre',
                        'parquet' => 'parquet',
                        'parquet contrecollé' => 'parquet contrecollé',
                        'parquet flottant' => 'parquet flottant',
                        'parquet massif' => 'parquet massif',
                        'parquet stratifié' => 'parquet stratifié',
                        'pierre naturelle' => 'pierre naturelle',
                        'tomette' => 'tomette',
                        'vinyle' => 'vinyle'
                    ],
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('DPE', ChoiceType::class, [
                    'label' => "ads.ad.bilan_consomation_energie",
                    'required' => false,
                    /** @Ignore */'choices' => Ad::CONSOMATION,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('dpeValue', NumberType::class,
                    [
                        'label' => 'ads.ad.consomation_energie',
                        'required' => false
                    ])
                ->add('GES', ChoiceType::class, [
                    'label' => "ads.ad.bilan_emission_ges",
                    'required' => false,
                    /** @Ignore */'choices' => Ad::CONSOMATION,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('gesValue', NumberType::class,
                    [
                        'label' => 'ads.ad.emission_ges',
                        'required' => false
                ])
                ->add('typeDeChauffage', ChoiceType::class, [
                    'label' => 'ads.ad.type_chauffage',
                    'required' => false,
                    'choices' => Maison::CHAUFFAGE_TYPES,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
                ->add('energieDeChauffage', ChoiceType::class, [
                    'label' => 'ads.ad.energie_de_chauffage',
                    'required' => false,
                    'choices' => Maison::ENERGIE_CHAUFFAGE,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ])
            ;

            /*Certifications et labels*/
            $builder->add('hauteQualiteEnvironnementale', CheckboxType::class, [
                'label'    => 'ads.ad.labels.haute_qualite_environnementale',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
                ->add('nfLogement', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.nf_logement',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('labelHabitatEnvironnement', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.label_habitat_environnement',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('THPE', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.label_THPE',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('HPE_ENR', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.label_HPE_ENR',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('labelBatimentBasseConsommation', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.label_batiment_basse_consommation',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('labelHabitatEnvironnementEffinergie', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.label_habitat_environnement_effinergie',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('planLocalHabitat', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.plan_local_habitat',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('RT2012', CheckboxType::class, [
                    'label'    => 'ads.ad.labels.RT2012',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ]);
        }
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'imagePrincipale' => null,
            'flow_step'       => null,
            'index_property'  => null,
            'data_class'      => Maison::class,
            'model_class'     => Maison::class,
            'translation_domain' => 'commiti'
        ));
    }

    public function getBlockPrefix()
    {
        return 'maison_form';
    }
}