<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\BienImmobilier;
use AppBundle\Entity\Group;
use AppBundle\Entity\Maison;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Fields\SiNumberType;
use AppBundle\Form\Type\Fields\SurfaceNumberType;
use AppBundle\Form\Type\Fields\SurfaceType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class AppartementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'ads.ad.type_appartement',
                'required' => true,
                'choices' => Appartement::TYPES,
                'attr' => [
                    'class' => 'select2'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('surface', TextType::class,
                [
                    'label' => 'ads.ad.surface',
                    'required' => true,
                    'attr' => ['class' => 'surface'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbEtages', TextType::class,
                [
                    'label' => 'ads.ad.numero_etage',
                    'attr'           => ['class' => 'si_number'],
                    'required'       => false,
                    'empty_data'     => null
                ])
            ->add('lotNumber', TextType::class,
                [
                    'label' => 'ads.ad.lot_number',
                    'attr'           => ['class' => 'si_number'],
                    'required'       => false,
                    'empty_data'     => null
                ])
            ->add('adresseBulleDeVente', TextType::class,
                [
                    'label' => 'ads.ad.adresse_bulle_de_vente',
                    'required'       => false,
                    'empty_data'     => null
                ])
            ->add('nbPieces', TextType::class,
                [
                    'label' => 'ads.ad.pieces_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\Length( [ 'groups' => $options['groups'],'min' => 1, 'max' => 7 ] ),
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbChambres', TextType::class,
                [
                    'label' => 'ads.ad.rooms_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('nbSalleBain', TextType::class,
                [
                    'label' => 'ads.ad.bathroom_number',
                    'required' => true,
                    'attr' => ['class' => 'surface_number'],
                    'constraints' => [
                        new Assert\NotBlank( [ 'groups' => $options['groups'],'message' => "form.field_is_required" ] )
                    ]
                ])
            ->add('siSalleEau', SiNumberType::class,
                [
                    'label' => "ads.ad.salle_eau",
                    'required' => true,
                    'constraints' => [
                        new AppAssert\SiNumber(['groups' => $options['groups']])
                    ]
                ])
            ->add('siParking', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.parking",
                    'required' => true,
                    'constraints' => [
                        new Assert\NotBlank( ['groups' => $options['groups'], 'message' => "form.field_is_required" ] ),
                        new AppAssert\SurfaceNumber(['groups' => $options['groups']])
                    ]
                ])
            ->add('siGardien', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.gardien",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ]);

        /******************** Optional *************************/
        $builder
            ->add('siVideophone', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.videophone",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siInterphone', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.interphone",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siDigicode', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.digicode",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siDuplex', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.duplex",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siSalon', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.salon",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siAlarme', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.alarme",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siToilette', SiNumberType::class,
            [
                'label' => "ads.ad.wc",
                'required' => false,
            ])
            ->add('siSejour', SurfaceType::class,
            [
                'label' => "ads.ad.livingroom",
                'required' => false
            ])
            ->add('siSalleAManger', SurfaceType::class,
            [
                'label' => "ads.ad.dinningroom",
                'required' => false,
            ])
            ->add('typeCuisine', ChoiceType::class, [
                'label' => "ads.ad.kitchen_type",
                'required' => false,
                'empty_value'  => null,
                'choices' => [
                    1 => 'Aucune',
                    2 => 'Américaine',
                    3 => 'Séparée',
                    4 => 'Industrielle',
                    5 => 'Coin cuisine',
                    6 => 'Américaine équipée',
                    7 => 'Séparée équipée',
                    8 => 'Coin cuisine équipé',
                    9 => 'Équipée'
                ],
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('typeDeChauffage', ChoiceType::class, [
                'label' => 'ads.ad.type_chauffage',
                'required' => false,
                'choices' => Maison::CHAUFFAGE_TYPES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('energieDeChauffage', ChoiceType::class, [
                'label' => 'ads.ad.energie_de_chauffage',
                'required' => false,
                'choices' => Maison::ENERGIE_CHAUFFAGE,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('vitrage', ChoiceType::class, [
                'label' => 'ads.ad.vitrage',
                'required' => false,
                'choices' => Maison::VITRAGES,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('siBureau', SurfaceType::class,
                [
                    'label' => "ads.ad.bureau",
                    'required' => false
                ])
            ->add('siBox', SiNumberType::class,
                [
                    'label' => "ads.ad.box",
                    'required' => false
                ])
            ->add('siBuanderie', SurfaceType::class,
                [
                    'label' => "ads.ad.buanderie",
                    'required' => false
                ])
            ->add('siTerrasse', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.terrasse",
                    'required' => false,
                ])
            ->add('siGarage', CheckboxType::class, [
                'label'    => 'ads.ad.garage',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])

            ->add('siDressing', SurfaceType::class,
                [
                    'label' => "ads.ad.dressing",
                    'required' => false
                ])
            ->add('siBalcons', SurfaceNumberType::class,
                [
                    'label' => "ads.ad.balcons",
                    'required' => false,
                ])
            ->add('siCave', SurfaceType::class,
                [
                    'label' => "ads.ad.cave",
                    'required' => false
                ])
            ->add('siVeranda', SurfaceType::class,
                [
                    'label' => "ads.ad.veranda",
                    'required' => false
                ])
            ->add('siCheminee', SiNumberType::class,
                [
                    'label' => "ads.ad.cheminee",
                    'required' => false
                ])
            ->add('siPlacards', SiNumberType::class,
                [
                    'label' => "ads.ad.placards",
                    'required' => false
                ])
            ->add('toitTerrasse', CheckboxType::class, [
                'label'    => 'ads.ad.toitTerrasse',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('grenier', ChoiceType::class, [
                'label' => 'ads.ad.grenier',
                'required' => false,
                'choices' => BienImmobilier::GARNIER,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('siJardin', CheckboxType::class, [
                'label'    => 'ads.ad.jardin',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('typeJardin', ChoiceType::class, [
                'label' => 'ads.ad.type_jardin',
                'required' => false,
                'choices' => [
                    'non privatif'      => 'Non privatif',
                    'privatif'          => 'Privatif',
                ],
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('revetementDeSol', ChoiceType::class, [
                'label' => "ads.ad.revetement_du_sol",
                'required' => false,
                'choices' => Maison::REVETEMENT_SOL,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('environnement', ChoiceType::class, [
                'label' => 'Environnement',
                'required' => false,
                'choices' => BienImmobilier::ENVIRONNEMENT,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('DPE', ChoiceType::class, [
                'label' => "ads.ad.bilan_consomation_energie",
                'required' => false,
                /** @Ignore */'choices' => Ad::CONSOMATION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('dpeValue', NumberType::class,
            [
                'label' => 'ads.ad.consomation_energie',
                'required' => false
            ])
            ->add('GES', ChoiceType::class, [
                'label' => "ads.ad.bilan_emission_ges",
                'required' => false,
                /** @Ignore */'choices' => Ad::CONSOMATION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('gesValue', NumberType::class,
                [
                    'label' => 'ads.ad.emission_ges',
                    'required' => false
                ])
            ->add('siVoletsRoulantsElectriques', CheckboxType::class, [
                'label'    => 'ads.ad.volet_roulants_electriques', //Volet roulants electriques
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('degagement', CheckboxType::class,
                [
                    'label' => "ads.ad.degagement",
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
            ->add('exposition', ChoiceType::class, [
                'label' => 'ads.ad.exposition',
                'required' => false,
                'choices' => BienImmobilier::EXPOSITION,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('vis_a_vis', ChoiceType::class, [
                'empty_value' =>false,
                'label' => "ads.ad.vis_a_vis",
                'required' => false,
                'choices' =>  [true => 'Oui',false => 'Non'],
                'expanded' => true
            ])
            ->add('siTV', CheckboxType::class, [
                'label'    => 'ads.ad.TV',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siPMR', CheckboxType::class, [
                'label'    => 'ads.ad.PMR',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('siPiscine', CheckboxType::class, [
                'label'    => 'ads.ad.piscine',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('belleVue', CheckboxType::class, [
                'label'    => 'ads.ad.nice_view',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('vueSurMer', CheckboxType::class, [
                'label'    => 'ads.ad.sea_view',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('calme', CheckboxType::class, [
                'label'    => 'ads.ad.calme',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
             ->add('_type', HiddenType::class, array(
                 'data'   => 'appartement',
                 'mapped' => false
             ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'isDuo'           => false,
            'index_property'  => null,
            'data_class'      => Appartement::class,
            'model_class'     => Appartement::class,
            'groups'          => ['Default'],
            'groups_terrain'          => null,
            'translation_domain' => 'commiti'
        ));
    }

    public function getBlockPrefix()
    {
        return 'appartement';
    }
}