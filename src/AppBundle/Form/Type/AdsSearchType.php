<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use AppBundle\Repository\AdRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdsSearchType extends AbstractType
{

    private $doctrine;
    private $authorizationChecker;

    public function __construct(Registry $doctrine, AuthorizationChecker $authorizationChecker){
        $this->doctrine = $doctrine;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        parent::buildForm($builder, $options);
            /** @var User $user */
            $user = $options['user'];


            $data = $builder->getData();

            // cherche la liste des agences
            $agencies = [];
            if(!empty($data['group'])) {
            $agencies[] = $this->doctrine->getRepository('AppBundle:Group')->find($data['group']);
            }
            elseif(!empty($data['bigGroups'])) {
                $currentGroup = $this->doctrine->getRepository('AppBundle:Group')->find($data['bigGroups']);
                $agencies = $currentGroup->getAgencyGroups()->toArray();
            }elseif($this->authorizationChecker->isGranted('ROLE_GROUP')){
                /** @var Group $group */
                foreach ($user->getGroups() as $group) {
                    $agencies = array_merge($agencies, $group->getAgencyGroups()->toArray());
                }
            }
            else{
                $agencies = $user->getAgencies();
            }
            /** @var AdRepository $adRepository */
            $adRepository = $this->doctrine->getRepository('AppBundle:Ad');
            $builder
                ->add('dateRemonterMin', DateType::class, [
                    'label'  => 'lots.filters.date_remonter_min',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'class' => 'datepicker',
                        'autocomplete' => 'off'
                    ],
                    'required'   => false
                ])
                ->add('dateRemonterMax', DateType::class, [
                    'label'  => 'lots.filters.date_remonter_max',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'class' => 'datepicker',
                        'autocomplete' => 'off'
                    ],
                    'required'   => false
                ])
                ->add('prixMin', TextType::class,
                    [
                        'label'       => 'lots.filters.prix_min',
                        'attr' => ['class' => 'si_number'],
                        'empty_data' => null,
                        'required'   => false
                ])
                ->add('prixMax', TextType::class,
                    [
                        'label'       => 'lots.filters.prix_max',
                        'attr' => ['class' => 'si_number'],
                        'empty_data' => null,
                        'required'   => false
                ])
                ->add('portalDiffusion', EntityType::class, [
                    'label' => 'lots.filters.portal_diffusion',
                    'class' => Portal::class,
                    'choice_label'  => 'name',
                    'query_builder' => function (EntityRepository $qb) use($agencies) {
                        $qBuilder = $qb->createQueryBuilder('u');
                        $qBuilder->andWhere('u.isAvailable = 1');
                        if($agencies) {
                            $qBuilder->leftJoin("u.broadcastPortalRegistrations", "bp")
                                ->andWhere($qBuilder->expr()->in("bp.group", $agencies));
                                }
                                return $qBuilder;

                    },
                    'multiple'   => true,
                    'expanded'   => false,
                    'empty_data' => null,
                    'required'   => false,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('typeAd', ChoiceType::class, [
                    'label'       => 'lots.filters.type_ad',
                    'multiple'    => false,
                    'required' => false,
                    'choices' => Ad::TYPES_ADS,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('etat', ChoiceType::class, [
                    'label'         => 'lots.filters.etat',
                    'required'      => false,
                    'expanded'      => false,
                    'choices'       => Ad::STATUS_ADS,
                    'multiple'      => true,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('ville', ChoiceType::class, [
                    'label' => 'lots.filters.ville',
                    'choices' => $adRepository->getAdCountryList($user),
                    'multiple'   => true,
                    'expanded'   => false,
                    'empty_data' => null,
                    'required'   => false,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Appliquer',
                    'attr' => [
                        'class'   => 'btn btn-success btn-block'
                    ]
                ])
            ;

            if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
                $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');
                if(count($user->getGroups()) > 1 || $isAdmin) {
                    $bigGroupsIds = [];
                    foreach ($user->getGroups() as $g)
                        $bigGroupsIds[] = $g->getId();

                    $builder->add('bigGroups', EntityType::class, [
                        'label' => 'lots.filters.ads_big_groups',
                        'class' => Group::class,
                        'choice_label'  => 'name',
                        'choice_value'  => 'id',
                        'query_builder' => function (EntityRepository $qb) use ($user, $isAdmin,$bigGroupsIds) {
                            if($isAdmin)
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup IS NULL')
                                    ->andWhere('g.roles NOT LIKE :roles')
                                    ->setParameter('roles','%ROLE_AGENCY%')
                                    ;
                            else
                            return $qb->createQueryBuilder('g')
                                ->andWhere('g.id IN (:groups)')
                                ->setParameter('groups',$bigGroupsIds);
                        },
                        'multiple'   => false,
                        'expanded'   => false,
                        'empty_data' => null,
                        'required' => false,
                        'attr' => [
                            'class'   => 'select2'
                            ]
                        ]
                    );
                }

                $builder->add('group', EntityType::class, [
                    'label' => 'lots.filters.ads_group',
                    'class' => Group::class,
                    'choice_label'  => 'name',
                    'choice_value'  => 'id',
                    'query_builder' => function (EntityRepository $qb) use ($user, $isAdmin, $data, $agencies) {
                        if(!empty($data['bigGroups'])) {
                            $currentGroup = $this->doctrine->getRepository('AppBundle:Group')->find($data['bigGroups']);
                            if($isAdmin || $user->getGroups()->contains($currentGroup)) {
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup = :group')
                                    ->setParameter('group',$currentGroup);
                            }
                        }
                        $qBuilder = $qb->createQueryBuilder('g');
                        if($isAdmin)
                            return $qBuilder
                                ->andWhere($qBuilder->expr()
                                ->like('g.roles', ':roles'))
                                ->setParameter('roles','%ROLE_AGENCY%');
                        else if ($agencies){
                            return $qb->createQueryBuilder('g')->andWhere('g IN (:group)')->setParameter('group', $agencies);
                        }
                        else {
                            return $qb->createQueryBuilder('g')->andWhere('g.parentGroup IN (:group)')->setParameter('group', null);
                        }
                    },
                    'multiple'   => false,
                    'expanded'   => false,
                    'empty_data' => null,
                    'required' => false,
                    'attr' => [
                        'class'   => 'select2'
                        ]
                    ]
                );

                $group = null;
                if($data && is_array($data) && !empty($data['group']) && !empty($data['bigGroups']))
                    $group = $this->doctrine->getRepository('AppBundle:Group')->find($data['group']);

                if($group) {
                    $builder->add('commercial', EntityType::class, [
                            'label' => 'lots.filters.group_commercials',
                            'class' => User::class,
                            'choice_label'  => 'name',
                            'query_builder' => function (EntityRepository $qb) use ($group) {
                                return $qb->createQueryBuilder('u')
                                    ->leftJoin('u.groups', 'g')
                                    ->where('u.roles LIKE :roles OR g.roles LIKE :roles')
                                    ->andWhere(':group MEMBER OF u.groups')
                                    ->setParameter('roles', '%"ROLE_SALESMAN"%')
                                    ->setParameter('group', $group);
                            },
                            'multiple'   => false,
                            'expanded'   => false,
                            'required' => false
                        ]
                    );
                }


            } elseif($user->hasRole('ROLE_BRANDLEADER')) {
                $group = $user->getGroup();
                $builder->add('commercial', EntityType::class, [
                        'label' => 'lots.filters.group_commercials',
                        'class' => User::class,
                        'choice_label'  => 'name',
                        'query_builder' => function (EntityRepository $qb) use ($group) {
                            return $qb->createQueryBuilder('u')
                                ->leftJoin('u.groups', 'g')
                                ->where('u.roles LIKE :roles OR g.roles LIKE :roles')
                                ->andWhere(':group MEMBER OF u.groups')
                                ->setParameter('roles', '%"ROLE_SALESMAN"%')
                                ->setParameter('group', $group);
                        },
                        'multiple'   => false,
                        'expanded'   => false,
                        'required' => false
                    ]
                );
            }

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'user' => null,
            'csrf_protection' => false,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ads_search';
    }
}