<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortalRegistrationSearchType extends AbstractType
{

    private $user;
    private $authorizationChecker;
    private $doctrine;

    public function __construct(User $user,Registry $doctrine, AuthorizationChecker $authorizationChecker){
        $this->user = $user;
        $this->authorizationChecker = $authorizationChecker;
        $this->doctrine = $doctrine;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        parent::buildForm($builder, $options);
        $data = $builder->getData();
        if($this->authorizationChecker->isGranted('ROLE_MARKETING')) {
            $isAdmin = $this->authorizationChecker->isGranted('ROLE_ADMIN');
            if (count($this->user->getGroups()) > 1 || $isAdmin) {
                $bigGroupsIds = [];
                foreach ($this->user->getGroups() as $g)
                    $bigGroupsIds[] = $g->getId();

                $builder->add('bigGroups', EntityType::class, [
                        'label' => 'lots.filters.ads_big_groups',
                        'class' => Group::class,
                        'choice_label' => 'name',
                        'choice_value' => 'id',
                        'query_builder' => function (EntityRepository $qb) use ($isAdmin, $bigGroupsIds) {
                            if ($isAdmin)
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.parentGroup IS NULL')
                                    ->andWhere('g.roles NOT LIKE :roles')
                                    ->setParameter('roles', '%ROLE_AGENCY%');
                            else
                                return $qb->createQueryBuilder('g')
                                    ->andWhere('g.id IN (:groups)')
                                    ->setParameter('groups', $bigGroupsIds);
                        },
                        'multiple' => false,
                        'expanded' => false,
                        'empty_data' => null,
                        'required' => false
                    ]
                );
            }

            $builder->add('group', EntityType::class, [
                    'label' => 'lots.filters.ads_group',
                    'class' => Group::class,
                    'choice_label' => 'name',
                    'choice_value' => 'id',
                    'query_builder' => function (EntityRepository $qb) use ($isAdmin, $data) {
                        $qBuilder = $qb->createQueryBuilder('g');
                        if (!empty($data['bigGroups'])) {
                            $currentGroup = $this->doctrine->getRepository('AppBundle:Group')->find($data['bigGroups']);

                            if ($isAdmin || $this->user->getGroups()->contains($currentGroup)) {
                                return $qBuilder->andWhere('g.parentGroup = :group')
//                                        ->innerJoin("g.broadcastPortalRegistrations", "bpr")
//                                        ->andWhere("bpr.isEnabled=false")
                                        ->setParameter('group', $currentGroup);
                            }
                        }

                        if ($isAdmin)
                            return $qBuilder->andWhere(
                                $qBuilder->expr()->like('g.roles', ':roles'))
                                ->setParameter('roles', '%"ROLE_AGENCY"%');
                        else
                            return $qBuilder->andWhere('g.parentGroup IN (:group)')->setParameter('group', $this->user->getGroups());
                    },
                    'multiple' => false,
                    'expanded' => false,
                    'empty_data' => null,
                    'required' => false
                ]
            );
        }

            $builder
                ->add('zoneGeographique', ChoiceType::class, [
                    'label'       => 'portal.zone_geographique',
                    'required' => false,
                    'choices' => Portal::ZONES,
                    'empty_value' => '--- Zones Géographique ---'
                ])
                ->add('cluster', ChoiceType::class, [
                    'label'       => 'portal.type',
                    'empty_value' => '--- Types ---',
                    'required' => false,
                    'choices' => Portal::CLUSTER
                ])
                ->add('typeDiffusion', ChoiceType::class, [
                    'label'       => 'portal.type_diffusion',
                    'multiple'    => true,
                    'required' => false,
                    'choices' => Portal::TYPES,
                    'attr' => [
                        'class'   => 'select2',
                        'data-placeholder' => 'Type d\'annonce'
                    ]
                ])
                ->add('categories', ChoiceType::class, [
                    'label'       => 'portal.categories_principales',
                    'placeholder'       => 'portal.categories_principales',
                    'empty_value' => null,
                    'required' => false,
                    'multiple'    => true,
                    'choices' => Portal::CATEGORIES,
                    'attr' => [
                        'class'   => 'select2',
                        'data-placeholder' => 'Catégorie du portail'
                    ]
                ])
                ->add('status', ChoiceType::class, [
                    'label'       => 'portal.status',
                    'placeholder' => 'status',
                    'empty_value' => null,
                    'required' => false,
                    'multiple'    => true,
                    'choices' => [
                        'portal.status_sent_to_commiti',
                        'portal.status_waiting_support',
                    ],
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Appliquer',
                    'attr' => [
                        'class'   => 'btn btn-success'
                    ]
                ])
            ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'portal_registration_search';
    }
}