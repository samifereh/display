<?php

/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:35
 */

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Form\Type\ImageUploadableType;
use AppBundle\Repository\PortalRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;


class DiffusionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $isNewDiffusion = $builder->getData()->getId();
        parent::buildForm($builder, $options);
        $builder
            ->add('broadcastPortals', EntityType::class, [
                'class' => Portal::class,
                'choice_label'  => 'name',
                'choice_value' => function (Portal $p) {
                    return 'portal_'.$p->getSlug();
                },
                'choice_attr' => function($portal, $key, $index) use ($options, $isNewDiffusion) {
                    $return            = !$portal->existIn($options['alreadyPayedPortals']) && $portal->getCluster() =='PAYANT'  ?  ['disabled' => 'disabled'] : [];

                    $return['is_selected'] = false;
                    if(
                        isset($options['ad']) && $options['ad'] &&
                        $options['ad'] && $options['ad']->getDiffusion() &&
                        $portal->existIn($options['ad']->getDiffusion()->getBroadcastPortals())
                    ) {
                        $return['is_selected'] = true;
                    }
//                    return $return;

                    $return['checked'] =  false;
                    $diffusionType =
                        isset($options['ad']) ?
                            $options['ad']->getTypeDiffusion() :
                            (
                                isset($options['program']) ?
                                    $options['program']->getType() : null
                            );

                    if($diffusionType == 'terrain' && $portal->getTypeDiffusion() == 'maison') {
                        $return['disabled'] = 'disabled';
                        return $return;
                    }

                    /*if($isNewDiffusion) {
                        return $return;
                    }*/



                    if( $portal->getCluster() =='GRATUIT') {
                        $return['checked'] = true;
                        return $return;
                    }

                    if(isset($options['commercialLimit'][$portal->getId()])) {
                        $rest = $options['commercialLimit'][$portal->getId()]['rest'] ? ($options['commercialLimit'][$portal->getId()]['rest'] - $options['substractValue']) : ($options['commercialLimit'][$portal->getId()]['limit']->getLimit() - $options['substractValue']);

                        if(
                            $return['is_selected'] && $rest >= 0 && $options['commercialLimit'][$portal->getId()]['rest'] > 0)
                            $return['checked'] = true;
                        elseif($options['commercialLimit'][$portal->getId()]['rest'] == 0 && !$return['is_selected'])
                            $return['disabled'] = 'disabled';
                    }
                    return $return;
                },
                'attr'     => [
                    'class' => 'table table-bordered table-striped i-checks',
                ],
                'multiple' => true,
                'expanded' => true,
                'choices' => $options['allPortals'],
            ]);

            //Submit
            $builder->add('submit',SubmitType::class, [
                'label' => 'action.diffuser',
                'attr'  => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->add('cancel',SubmitType::class, [
                'label' => 'action.cancel',
                'attr'  => [
                    'class' => 'btn btn-failed'
                ]
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'ad'                    => null,
            'program'               => null,
            'allPortals'            => null,
            'alreadyPayedPortals'   => null,
            'commercialLimit'       => null,
            'substractValue'        => 0,
            'data_class'            => Diffusion::class,
            'translation_domain' => 'commiti'
        ));
    }

}