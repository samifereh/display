<?php

/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:35
 */

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\AppBundle;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\User;
use AppBundle\Form\Type\ImageUploadableType;
use AppBundle\Repository\Broadcast\PortalRegistrationRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CommercialPortalLimitType extends AbstractType
{
    protected $commercialPortalsFormType;
    protected $portalRegistrationRepository;

    public function __construct(
        CommercialPortalsType $commercialPortalsFormType,
        PortalRegistrationRepository $portalRegistrationRepository
    )
    {
        $this->commercialPortalsFormType           = $commercialPortalsFormType;
        $this->portalRegistrationRepository        = $portalRegistrationRepository;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $commercial = $builder->getData();
        $registredPortals = $commercial->getCommercialsPortal();
        $payantRegistredPortals  = new ArrayCollection();
        foreach($registredPortals as $registredPortal) {
            if($registredPortal->getPortal()->getCluster() == 'PAYANT') {
                $payantRegistredPortals->add($registredPortal);
            }
        }

        $groupRegistredPortals = $this->portalRegistrationRepository->getPortalRegistrationByGroup($commercial->getGroup(),null);
        $payantRegistredPortals = new ArrayCollection();
        foreach ($commercial->getCommercialsPortal() as $commercialPortalLimit) {
            foreach ($groupRegistredPortals as $groupRegistredPortal) {
                if($groupRegistredPortal->getBroadcastPortal()->getId() == $commercialPortalLimit->getPortal()->getId() &&
                    $commercialPortalLimit->getPortal()->getCluster() == 'PAYANT'
                ) {
                    $payantRegistredPortals->add($commercialPortalLimit);
                    break;
                }
            }
        }

        parent::buildForm($builder, $options);
        $builder->add('commercialsPortal',CollectionType::class,[
            'entry_type' => $this->commercialPortalsFormType,
            'data'       => $payantRegistredPortals,
            'options'    => ['commercial' => $commercial]
        ]);
        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.assigner',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ])
            ->add('cancel',SubmitType::class, [
                'label' => 'action.cancel',
                'attr'  => [
                    'class' => 'btn btn-failed'
                ]
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => User::class,
            'translation_domain' => 'commiti'
        ));
    }

}