<?php

/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:35
 */

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Form\Type\ImageUploadableType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortalType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('name', TextType::class, [
                'label'       => 'portal.name',
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('ftpHost', TextType::class, [
                'label'       => 'portal.ftp_host',
                'required'    => false,
            ])
            ->add('ftpUser', TextType::class, [
                'label'       => 'portal.ftp_user',
                'required'    => false
            ])
            ->add('ftpPassword', TextType::class, [
                'label'       => 'portal.password',
                'required'    => false
            ])
            ->add('exportFileName', TextType::class, [
                'label'       => 'portal.export_file',
                'required'    => false
            ])
            ->add('cluster', ChoiceType::class, [
                'label'       => 'portal.type',
                'empty_value' => null,
                'choices' => Portal::CLUSTER
            ])
            ->add('type', ChoiceType::class, [
                'label'       => 'portal.format_diffusion',
                'empty_value' => null,
                'choices' => [
                    'achatterrain'           => 'Achat Terrain XML',
                    'xml_ubiflow'            => 'CMI XML UBIFLOW',
                    'globimmo'               => 'Globimmo.net XML',
                    'mitula'                 => 'Mitula XML',
                    'poliris_annoncesjaunes' => 'Poliris Annonces Jaunes',
                    'poliris'                => 'Poliris CSV',
                    'xml'                    => 'Poliris XML',
                    'trovit'                 => 'Trovit',
                    'webimmo'                => 'Webimmo',
                    'xml_program_immoneuf'   => 'XML Programmes Immoneuf',
                    'zilek'                  => 'Zilek XML',
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('exportHouseModel', CheckboxType::class, [
                'label' => "portal.export_housemodel",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('typeDiffusion', ChoiceType::class, [
                'label'       => 'portal.type_diffusion',
                'empty_value' => null,
                'multiple'    => true,
                'choices' => Portal::TYPES,
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('categories', ChoiceType::class, [
                'label'       => 'portal.categories_principales',
                'empty_value' => null,
                'multiple'    => true,
                'choices' => Portal::CATEGORIES,
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('diffuseProgram', CheckboxType::class, [
                'label' => "portal.diffusion_programmes",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('diffuseTerrainOnly', CheckboxType::class, [
                'label' => "portal.diffusion_only_terrain",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('isAvailable', CheckboxType::class, [
                'label' => "portal.disponible",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('afficherMondat', CheckboxType::class, [
                'label' => "portal.afficher_mondat",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => "portal.description",
                'required' => false,
                'attr' => [
                    'rows' => '20',
                    'class' => 'textarea-editor'
                ]
            ])
            ->add('descriptionOffre', TextareaType::class, [
                'label' => "portal.description_offre",
                'required' => false,
                'attr' => [
                    'rows' => '20',
                    'class' => 'textarea-editor'
                ]
            ])
            ->add('espacePro', TextType::class, [
                'label'    => 'portal.acces_espace_pro',
                'required' => false
            ])
            ->add('icon', ImageUploadableType::class, [
                'required' => false,
                'attr'     => ['class'=>'filestyle','data-buttonText' => 'portal.choose_icon'],
                'data' => new File($options['icon'], false)
            ])
            ->add('viewedPagesPerMonth', TextType::class, [
                'label' => 'portal.nombre_pages_vue_par_mois',
                'attr' => [
                    'class' => 'number'
                ]
            ])
            ->add('hasMobileApp', CheckboxType::class, [
                'label' => "portal.application_mobile",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('monthlyVisits', TextType::class, [
                'label' => 'portal.nbr_visiteurs_unique_par_mois',
                'attr' => [
                    'class' => 'number'
                ]
            ])
            ->add('anneeCreation', TextType::class, [
                'label' => 'portal.annee_creation',
                'attr' => [
                    'class' => 'number'
                ]
            ])
            ->add('nbrAnnonces', TextType::class, [
                'label' => 'portal.nbr_annonces',
                'attr' => [
                    'class' => 'number'
                ]
            ])
            ->add('ratioProsParticuliers', TextType::class,[
                'label'   => 'portal.ratio_pro_particuliers',
                'required' => false
            ])
            ->add('frequenceMaj', TextType::class,[
                'label'   => 'portal.frequence_de_mise_a_jour',
                'required' => false
            ])
            ->add('multiDiffusionVersAutresPortails', CheckboxType::class, [
                'label' => "portal.multi_diffusion_vers_autre_portails",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('dedoublonnageAnnonces', CheckboxType::class, [
                'label' => "portal.dedoublonnage_des_annonces",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('affichageDansListeResultats', CheckboxType::class, [
                'label' => "portal.affichage_dans_liste_resultats",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('affichageDetailsAnnonce', CheckboxType::class, [
                'label' => "portal.affichage_details_annonce",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('affichageNomAnnonceur', CheckboxType::class, [
                'label' => "portal.affichage_nom_annonceur",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('formulaireDeContact', CheckboxType::class, [
                'label' => "portal.formulaire_de_contact",
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('zoneGeographique', ChoiceType::class, [
                'label'       => 'portal.zone_geographique',
                'empty_value' => null,
                'choices' => Portal::ZONES
            ])
        ;

        //Contact
        $builder->add('email', EmailType::class, [
                'label'       => 'portal.email',
                'required'    => false,
                'constraints' => [
                    new Assert\Email()
                ]
            ])
            ->add('firstname', TextType::class, [
                'label'    => 'portal.prenom',
                'required' => false
            ])
            ->add('lastname', TextType::class, [
                'label'    => 'portal.nom',
                'required' => false
            ])
            ->add('websiteUrl', UrlType::class,[
                'label'     => 'portal.website',
                'required' => false
            ])
            ->add('phoneNumber', TextType::class, [
                'label'    => 'portal.phone',
                'required' => false
            ]);

            //Submit
            $builder->add('submit',SubmitType::class, [
                'label' => 'action.validate',
                'attr'  => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'icon'            => null,
            'data_class'      => Portal::class,
            'translation_domain'    => 'commiti'
        ));
    }

}