<?php

/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:35
 */

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\AppBundle;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\ImageUploadableType;
use AppBundle\Repository\PortalRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GroupPortalRegistrationsType extends AbstractType
{
    protected $aPortals;
    protected $registredPortals;
    protected $group;
    protected $broadcastPortalRegistrationType;


    public function __construct($aPortalsRepository, $user, \AppBundle\Form\Type\Broadcast\BroadcastPortalRegistrationType $broadcastPortalRegistrationType)
    {
        $this->aPortals                               = $aPortalsRepository->findAll();
        $this->broadcastPortalRegistrationType        = $broadcastPortalRegistrationType;
    }

    private function getRegisterPortalsList() {
        $registredPortalsList = new ArrayCollection();
        /** @var \AppBundle\Entity\Broadcast\Portal $portal */
        foreach ($this->aPortals as $portal) {
            $alreadyRegistred = false;
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $registredPortal */
            foreach ($this->registredPortals as $registredPortal) {
                if($portal->equals($registredPortal->getBroadcastPortal())) {
                    $registredPortalsList->add($registredPortal);
                    $alreadyRegistred = true;
                    break;
                }
            }
            if($alreadyRegistred) {
                continue;
            }
        }

        return $registredPortalsList;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->group = $builder->getData();
        $this->registredPortals = $this->group->getBroadcastPortalRegistrations();

        parent::buildForm($builder, $options);
        $builder->add('broadcastPortalRegistrations',CollectionType::class,[
            'entry_type' => $this->broadcastPortalRegistrationType,
            'data'       => $this->getRegisterPortalsList()
        ]);
        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.assigner',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ])
            ->add('cancel',SubmitType::class, [
                'label' => 'action.cancel',
                'attr'  => [
                    'class' => 'btn btn-failed'
                ]
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allPortals'            => null,
            'alreadyPayedPortals'   => null,
            'data_class'            => Group::class,
            'compound'              => true,
            'translation_domain'    => 'commiti'
        ));
    }

}