<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 14:27
 */

namespace AppBundle\Form\Type\Broadcast;


use AppBundle\Entity\Broadcast\CommercialPortalLimit;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\User;
use AppBundle\Repository\Broadcast\PortalRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Validator\Constraints as Assert;


class CommercialPortalsType extends AbstractType
{

    /** @var \AppBundle\Repository\Broadcast\PortalRepository */
    public $portalRepository;
    /** @var \AppBundle\Repository\Broadcast\PortalRegistrationRepository */
    public $portalRegistrationRepository;
    /** @var \AppBundle\Repository\Broadcast\CommercialPortalLimitRepository */
    public $commercialPortalLimitRepository;
    public $group;

    public function __construct($em)
    {
        $this->portalRepository                    = $em->getRepository('AppBundle:Broadcast\\Portal');
        $this->portalRegistrationRepository        = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
        $this->commercialPortalLimitRepository     = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->group = $options['commercial']->getAgencyGroup();
        parent::buildForm($builder, $options);
        $builder
            ->add('portal', HiddenType::class);

        $builder->get('portal')->addModelTransformer(new CallbackTransformer(
            function ($portal) {
                return $portal;
            },
            function ($portal) {
                if(is_string($portal))
                    return $this->portalRepository->find($portal);
                return $portal;
            }
        ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $commercialPortal = $event->getData();
            $form             = $event->getForm();
            $portalRegistration = $this->portalRegistrationRepository->findOneBy(['group' => $this->group, 'broadcastPortal' => $commercialPortal->getPortal()]);
            $used = $this->commercialPortalLimitRepository->getAgencyPortalRestLimit($this->group,$commercialPortal->getPortal());
            $rest = $portalRegistration->getAdLimit()-$used+$commercialPortal->getLimit();
            $form->add('_limit', NumberType::class,[
                'label'    => false,
                'required' => false,
                'attr'     => [
                    'class' => 'si_number',
                    'placeholder' => 'rest '.($portalRegistration->getAdLimit()-$used),
                    'data-limit'  => 'rest '.($portalRegistration->getAdLimit()-$used).' sur '.$portalRegistration->getAdLimit()
                ],
                'error_bubbling'    => true,
                'data'              => $commercialPortal->getLimit(),
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                    new Assert\Range( ['min' => 0, 'max'=> $rest ] )
                ]
            ]);
        });

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'commercial'        => null,
            'error_bubbling'    => false,
            'cascade_validation' => true,
            'data_class'         => CommercialPortalLimit::class,
            'translation_domain' => 'commiti'
        ));
    }


    public function getBlockPrefix()
    {
        return 'commercial_portal';
    }

}