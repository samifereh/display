<?php

namespace AppBundle\Form\Type\Broadcast;

use AppBundle\Entity\Broadcast\PortalRegistration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PortalRegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('isEnabled', CheckboxType::class, [
                'label'    => 'portal.is_enbaled',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'mapped'   => true,
                'multiple' => false,
                'choices' => [
                    'portal.status_sent_to_commiti',
                    'portal.status_waiting_support',
                ],
                'attr' => [
                    'class' => 'select2'
                ]]
            )
            ->add('adLimit')
            ->add('adRest')
            ->add('trackingPhone')
            ->add('ftpHost')
            ->add('ftpUser')
            ->add('ftpPassword')
            ->add('identifier')
            ->add('login')
            ->add('password')
            ->add('group')
            ->add('broadcastPortal')
        ;
        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.send',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ])
            ->add('cancel',SubmitType::class, [
                'label' => 'action.cancel',
                'attr'  => [
                    'class' => 'btn btn-failed'
                ]
            ]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PortalRegistration::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }
}
