<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 14:27
 */

namespace AppBundle\Form\Type\Broadcast;


use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\User;
use AppBundle\Repository\PortalRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Validator\Constraints as Assert;


class BroadcastPortalRegistrationType extends AbstractType
{

    /** @var  \AppBundle\Entity\Broadcast\Portal */
    private $portal;
    /** @var \AppBundle\Repository\Broadcast\PortalRepository */
    public $portalRepository;
    public $user;
    public $authorization_checker;


    public function __construct(\AppBundle\Repository\Broadcast\PortalRepository $portalRepository, $user, $authorization_checker)
    {
        $this->portalRepository        = $portalRepository;
        $this->user                    = $user;
        $this->authorization_checker   = $authorization_checker;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->portal = $options['portal'];
        parent::buildForm($builder, $options);
        $builder
            ->add('adLimit', NumberType::class,[
                'label'    => false,
                'required' => false,
                'attr'     => [
                    'class' => 'si_number',
                    'placeholder' => 'portal.client_limit_diffusion'
                ],
                'error_bubbling'    => true,
                'constraints' => [
                    new Assert\NotBlank( [ 'groups' => ['isChecked'],'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('broadcastPortal', HiddenType::class);
        if($this->authorization_checker->isGranted('ROLE_ADMIN')) {
            $builder->add('isEnabled', CheckboxType::class, [
                'label'    => false,
                'required' => false
                ])
                ->add('ftpHost', TextType::class, [
                'label'    => false,
                'required' => false,
                'attr'     => [
                    'placeholder' => 'portal.ftp_host'
                    ]
                ])
                ->add('ftpUser', TextType::class, [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'portal.ftp_user'
                    ]
                ])
                ->add('ftpPassword', TextType::class, [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'portal.ftp_password'
                    ]
                ])
                ->add('trackingPhone', TextType::class, [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'portal.track_phone'
                    ]
                ])->add('identifier', TextType::class, [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'portal.client_identifier'
                    ]
                ]);
            }
            if($this->authorization_checker->isGranted('ROLE_MARKETING')) {
                $builder->add('isUsed', CheckboxType::class, [
                    'label'    => false,
                    'required' => false
                ])
                ->add('login', TextType::class, [
                    'label'    => false,
                    'required' => false,
                    'attr'     => [
                        'placeholder' => 'portal.client_login'
                    ]
                ]);
                if($this->authorization_checker->isGranted('ROLE_MARKETING')) {
                    $builder->add('password', TextType::class, [
                        'label' => false,
                        'required' => false,
                        'attr' => [
                            'placeholder' => '******'
                        ]
                    ]);
                } else {
                    $builder->add('password', PasswordType::class, [
                        'label' => false,
                        'required' => false,
                        'attr' => [
                            'placeholder' => '******'
                        ]
                    ]);
                }
            }

        $builder->get('broadcastPortal')->addModelTransformer(new CallbackTransformer(
            function ($portal) {
                return $portal;
            },
            function ($portal) {
                if(is_string($portal))
                    return $this->portalRepository->find($portal);
                return $portal;
            }
        ));

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'portal'            => null,
            'error_bubbling'    => false,
            'cascade_validation' => true,
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                if($data->getIsEnabled() == true && $data->getBroadcastPortal()->getCluster() == 'PAYANT')
                    return array('Default', 'isChecked');
                else
                    return array('Default');
            },
            'data_class'        => PortalRegistration::class,
            'translation_domain' => 'commiti'
        ));
    }


    public function getBlockPrefix()
    {
        return 'broadcast_portal';
    }

}