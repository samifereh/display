<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LotsSearchType extends AbstractType
{

    private $doctrine;

    public function __construct(Registry $doctrine){
        $this->doctrine = $doctrine;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        parent::buildForm($builder, $options);
            $builder
                ->add('dateRemonter', DateType::class, [
                    'label'  => 'lots.filters.date_remonter_max',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'class' => 'datepicker',
                        'autocomplete' => 'off'
                    ],
                    'required'   => false
                ])
                ->add('prixMax', TextType::class,
                    [
                        'label'       => 'lots.filters.prix_max',
                        'attr' => ['class' => 'si_number'],
                        'empty_data' => null,
                        'required'   => false
                ])
                ->add('portalDiffusion', EntityType::class, [
                    'label' => 'lots.filters.portal_diffusion',
                    'class' => Portal::class,
                    'choice_label'  => 'name',
                    'query_builder' => function (EntityRepository $qb) {
                        return $qb->createQueryBuilder('u')->andWhere('u.isAvailable = 1');
                    },
                    'multiple'   => false,
                    'expanded'   => false,
                    'attr' => ['class'   => 'select2'],
                    'empty_data' => null,
                    'required'   => false
                ])
                ->add('typeLot', ChoiceType::class, [
                    'label'       => 'lots.filters.type_lot',
                    'multiple'    => false,
                    'required' => false,
                    'choices' => Ad::TYPES_LOTS,
                    'attr' => [
                        'class'   => 'select2'
                    ]
                ])
                ->add('zipcode', ChoiceType::class, [
                    'label' => 'lots.filters.zipcode',
                    'choices' => $this->doctrine->getRepository('AppBundle:Ad')->getAdZipCodeList(),
                    'multiple'   => true,
                    'expanded'   => false,
                    'empty_data' => null,
                    'required'   => false
                ])
                ->add('etat', ChoiceType::class, [
                    'label'         => 'lots.filters.etat',
                    'required'      => false,
                    'expanded'      => true,
                    'choices'       => Ad::STATUS,
                    'multiple'      => true
                ])
                ->add('submit', SubmitType::class, [
                    'label'       => 'Appliquer',
                    'attr' => [
                        'class'   => 'btn btn-success btn-block'
                    ]
                ])
            ;

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'diffusion_search';
    }
}