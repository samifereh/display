<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Agency;
use AppBundle\Form\Validator\Constraints\UniqueField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgencyType extends AbstractType
{

    /** @var AuthorizationChecker */
    private $security;
    public function __construct(AuthorizationChecker $security)
    {
        $this->security           = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data   = null;
        if($options["avatar"]) {
            $data = new File($options["avatar"], false);
        }
        $voicemailData   = null;
        if(isset($options["voicemail"]) && $options["voicemail"]) {
            $voicemailData = new File($options["voicemail"], false);
        }
        parent::buildForm($builder, $options);
                if(!$options['isAdmin']) {
                    $builder->add('name', TextType::class, [
                                'label' => 'agence.organisation'
                            ]);
                    }
                $builder->add('reference', TextType::class, [
                    'label' => 'agence.reference',
                    'constraints' => [
                        new UniqueField(['groups' => ['ReferenceUnique'],'field_name' => 'reference','repository' => 'AppBundle:Agency'])
                    ],
                    'attr' => [
                        'placeholder' => 'PRI_xxxx'
                    ]
                ]);
                $builder->add('avatar', ImageUploadableType::class, [
                        'label'=> 'Logo',
                        'data' => $data,
                        'attr' => [
                            'class' => 'avatar'
                        ]
                    ])
                    ->add('color',  TextType::class, [
                        'label'     => 'agence.color',
                        'required'  => false,
                        'attr' => [
                            'class' => 'color_picker'
                        ]
                    ])
                    ->add('nom',  TextType::class, [
                        'label' => 'agence.contact.nom',
                        'constraints' => [
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('prenom',  TextType::class, [
                        'label' => 'agence.contact.prenom',
                        'constraints' => [
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('telephone',  TextType::class, [
                        'label' => 'agence.contact.phone',
                        'constraints' => [
                            new Assert\Length(10),
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('adress',  TextType::class, [
                        'label' => 'agence.contact.adresse',
                        'constraints' => [
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('ville',  TextType::class,[
                        'label' => 'agence.contact.city',
                        'required' => true,
                        'attr' => [
                            'class' => 'city-list',
                            'typeahead' => 'cp'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('codePostal',  TextType::class, [
                        'label' => 'agence.contact.zip_code',
                        'required' => true,
                        'attr' => [
                            'class' => 'cp'
                        ],
                        'constraints' => [
                            new Assert\Length(5),
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                        ]
                    ])
                    ->add('email',  TextType::class, [
                        'label' => 'agence.contact.email',
                        'constraints' => [
                            new Assert\NotBlank( [ 'message' => "form.field_is_required" ] ),
                            new Assert\Email()
                        ]
                    ])
                    ->add('content',  TextareaType::class,[
                        'label' => 'agence.contact.description',
                        'attr' => [
                            'rows' => '20',
                            'class'=> 'textarea-editor'
                        ]
                    ]);
                //Facturation
                $builder
                    ->add('facturationNomEntite',  TextType::class, [
                        'label' => 'agence.facturation.nom_entite',
                        'required' => false
                    ])
                    ->add('facturationAdresse',  TextType::class, [
                        'label' => 'agence.facturation.adresse',
                        'required' => false
                    ])
                    ->add('facturationCodePostal',  TextType::class, [
                        'label' => 'agence.facturation.zip_code',
                        'required' => false,
                        'attr' => [
                            'class' => 'cp-2'
                        ]
                    ])
                    ->add('facturationVille',  TextType::class,[
                        'label' => 'agence.facturation.city',
                        'required' => false,
                        'attr' => [
                            'class' => 'city-list-2',
                            'typeahead' => 'cp-2'
                        ]
                    ]);
                //Fermeture / réouverture Agence
                $builder
                    ->add('closingDate', DateTimeType::class, [
                    'label'    => 'agence.contact.closing_date',
                    'html5' => true,
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => [
                        'class' => 'datetimepicker',
                        'autocomplete' => 'off'
                        ]
                    ])
                    ->add('reopeningDate', DateTimeType::class, [
                        'label'    => 'agence.contact.reopening_date',
                        'required' => false,
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy HH:mm',
                        'attr' => [
                            'class' => 'datetimepicker',
                            'autocomplete' => 'off'
                        ]
                    ])
                    ->add('closingPhone',  TextType::class, [
                        'label' => 'agence.contact.closing_phone',
                        'required' => false,
                        'constraints' => [
                            new Assert\Length(10)
                        ]
                    ])
                    ->add('voicemail', FileType::class, [
                        'label'=> 'agence.custom_voicemail',
                        'required' => false,
                        'data' => $voicemailData,
                        'attr' => [
                            'class' => 'voicemail',
                            'placeholder' => 'agence.voicemail_placeholder',
                            'help' => 'agence.voicemail_placeholder',
                            'data-url'=> '/web/uploads/voicemail',
                            'accept' => "audio/mpeg, audio/x-mpeg, audio/mp3, audio/x-mp3, audio/mpeg3, audio/x-mpeg3, audio/mpg, audio/x-mpg, audio/x-mpegaudio"
                        ]
                    ])
                ;
    }




    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Agency::class,
            'translation_domain' => 'commiti',
            'avatar' => null,
            'edit'               => true,
            'isAdmin'            => false,
            'cascade_validation' => true
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'agency';
    }
}