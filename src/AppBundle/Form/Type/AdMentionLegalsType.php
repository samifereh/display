<?php
/**
 * User: y.chakroun
 * Date: 02/08/17
 * Time: 11:27
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Ad;
use AppBundle\Entity\AdMentionLegals;
use AppBundle\Entity\AdRewrite;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdMentionLegalsType extends AbstractType
{

    /** @var \AppBundle\Repository\Broadcast\PortalRepository */
    public $portalRepository;

    public function __construct(\AppBundle\Repository\Broadcast\PortalRepository $portalRepository)
    {
        $this->portalRepository        = $portalRepository;
//        dump($this->portalRepository->getPortalByCluster());exit;
        //group_by
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $data = $builder->getData();
        $currentGroup = $data ? $data->getGroup() : null;
        $builder
            ->add('body', TextareaType::class, [
                'label' => "ad_mention_legals.body",
                'required' => true,
                'attr' => [
                    'rows' => '20',
                    'class'=> 'summernote-textarea'
                ],
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "form.field_is_required" ] )
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'ad_mention_legals.portals_type',
                'required' => false,
                'choices' => [
                    'PAYANT' => 'Tout les payants',
                    "GRATUIT" => 'Tout les gratuits'
                ],
                'expanded'   => true,
                'multiple'   => true,
                'mapped'   => false
            ])
            ->add('portals', EntityType::class, [
                'label' => 'ad_mention_legals.portals_list',
                'class' => Portal::class,
                'choice_label'  => 'name',
                'query_builder' => function (EntityRepository $qb) use ($currentGroup) {
                    if($currentGroup) {
                        return $qb->createQueryBuilder('p')
                            ->leftJoin('p.broadcastPortalRegistrations','bpr')
                            ->andWhere('bpr.group = :group')
                            ->andWhere('p.isAvailable = 1')
                            ->setParameter('group',$currentGroup)
                            ;
                    } else {
                        return $qb->createQueryBuilder('p')
                            ->andWhere('p.isAvailable = 1');
                    }
                },
                'group_by' => 'cluster',
                'multiple'   => true,
                'expanded'   => false,
                'empty_data' => null,
                'required'   => false,
                'attr' => [
                    'data-plugin-multiselect' => '',
                    'data-plugin-options' => '{ "maxHeight": 200, "enableCaseInsensitiveFiltering": true }'
                ],
                'label_attr' => [
                    'style' => "margin-right:10px;"
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label'       => 'Appliquer',
                'attr' => [
                    'class'   => 'btn btn-success btn-block'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AdMentionLegals::class,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName()
    {
        return 'ad_mention_legals';
    }
}