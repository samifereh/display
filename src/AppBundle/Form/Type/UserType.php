<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    protected $entityManager;
    protected $user;
    protected $group;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $user = $builder->getData();
        $roles = $options['roles'];
        $userRoles = !is_null($user) ? $user->getRoles() : [];
        $builder->add('name',  TextType::class, ['label' => 'user.nom_et_prenom'])
            ->add('username',  TextType::class, ['label' => 'user.username'])
            ->add('email', EmailType::class,['label' => 'user.email'])
            ->add('password', PasswordType::class, [
                'required' => false,
                'label' => 'user.password'
            ])
            ->add('avatar', ImageUploadableType::class, [
                'data' => new File(!is_null($user) ? $user->getAvatar() : null, false),
                'label' => 'user.avatar',
                'attr' => [
                    'class' => 'avatar'
                ]
            ])
            ->add(
                'groups', 'entity', [
                'class'    => 'AppBundle:Group',
                'label' => 'user.group',
                'property' => 'name',
                'multiple' => true,
                'required' => false,
                'attr' => [
                    'class' => 'select2'
                ],
            ])
            ->add('roles', 'choice',
                array(
                    'label' => 'user.role',
                    'choices' =>  array_combine($roles, $roles), // array_combine : put values to keys
                    'multiple'  => true,
                    'attr' => [
                        'class' => 'select2 select2_roles'
                    ],
                    'data' => array_combine($userRoles, $userRoles)
                )
            );
        if($options['isEdit']) {
            $builder
                ->add('enabled', CheckboxType::class, [
                    'label'    => 'user.enabled',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'roles'      => [],
            'isEdit'      => false,
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'user';
    }
}