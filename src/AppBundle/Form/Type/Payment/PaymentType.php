<?php

namespace AppBundle\Form\Type\Payment;


use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Payum\Core\Bridge\Symfony\Validator\Constraints\CreditCardDate;
use Payum\Core\Bridge\Symfony\Validator\Constraints\CreditCardDateValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
            $builder->add('method', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'expanded' => true,
                'choices' => [
                    'credit-card'  => 'Carte Credit',
                    'paypal'       => 'Paypal',
                    'bank-wire'    => 'Paiement par virement bancaire',
                    'check-wire'    => 'Paiement par chèque',
                ],
                'data' => 'credit-card',
                'constraints' => [
                    new Assert\NotBlank( [ 'message' => "Vous devez choisir une méthode de paiement" ] )
                ]
            ]);

            $builder->add('card', TextType::class, [
                    'label'       => false,
                    'attr'      => [
                        'placeholder'   => 'Numéro de carte de crédit',
                        'class'         => 'col-sm-12 collide collide-bottom'
                    ],
                    'error_bubbling'    => true,
                    'constraints' => [
                        new Assert\NotBlank(['groups' => 'CreditCard','message'=> "Le numéro de card de crédit est obligatoire"]),
                        new Assert\CardScheme( [ 'groups' => 'CreditCard','schemes' => ['VISA','MASTERCARD','DISCOVER'], 'message' => "Votre carte crédit n'est pas valide." ] )
                    ]
                ])
                ->add('exp_date', TextType::class, [
                    'label'  => false,
                    'attr'      => [
                        'placeholder'   => 'YYYY/MM',
                        'format'        =>  'YYYY-MM',
                        'class'         => 'col-sm-6 collide card-expire',
                    ],
                    'error_bubbling'    => true,
                    'constraints' => [
                        new Assert\NotBlank(['groups' => 'CreditCard','message'=> "La date d'expiration est obligatoire"]),
                        new CreditCardDate(['groups' => 'CreditCard', 'min' => 'today'])//, "message" => 'La date d\'expiration est invalide'
                    ]
                ])
                ->add('cvv2', TextType::class, [
                    'label'     => false,
                    'attr'      => [
                        'placeholder'   => 'CVV',
                        'class'         => 'col-sm-6 collide collide-left'
                    ],
                    'error_bubbling'    => true,
                    'constraints' => [
                        new Assert\NotBlank(['groups' => 'CreditCard','message'=> "Le CVV est obligatoire"]),
                        new Assert\Regex(['groups' => 'CreditCard', 'pattern' => "/^[0-9]{3,4}$/", "message" => 'Le numéro CVV n\'est pas valide'])
                    ]
                ]);

                //Submit
                $builder->add('pay',SubmitType::class, [
                    'label' => 'payment.payer',
                    'attr'  => [
                        'class' => 'btn btn-success col-sm-12'
                    ]
                ])
                ->add('paypal_pay',SubmitType::class, [
                    'label' => 'payment.continue_to_paypal',//Continuer vers PayPal
                    'attr'  => [
                        'class' => 'btn btn-success'
                    ]
                ])
                ->add('bank_pay',SubmitType::class, [
                    'label' => 'payment.confirm_commande',
                    'attr'  => [
                        'class' => 'btn btn-success'
                    ]
                ])
                ->add('check_pay',SubmitType::class, [
                    'label' => 'payment.confirm_commande',
                    'attr'  => [
                        'class' => 'btn btn-success'
                    ]
                ])
                ->getForm();

        $builder->get('exp_date')->addModelTransformer(new CallbackTransformer(
            function ($exp_date) {
                return $exp_date;
            },
            function ($exp_date) {
                if(is_string($exp_date)) {
                    return \DateTime::createFromFormat('m/Y', $exp_date);
                }
                return $exp_date;
            }
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'commiti',
            'validation_groups' => function(FormInterface $form){
                $data = $form->getData();
                if($data['method'] == 'credit-card')
                    return array('Default', 'CreditCard');
                else
                    return array('Default');
            }
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'payment';
    }
}