<?php

namespace AppBundle\Form\Type\Payment;

use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AutorizedUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $roles = UserRepository::getAgencyEmployeesRoles();
        $builder
            ->add('role', 'choice',
                array(
                    'label' => 'user.role',
                    'choices' =>  array_combine($roles, $roles),
                    'multiple'  => false,
                    'attr' => [
                        'class' => 'select2 select2_roles'
                    ]
                )
            )
            ->add('startDate', DateType::class, [
                'label'  => 'autorized_user.start_date',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => 'off'
                ],
                'required'   => false,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                ]
            ])
            ->add('endDate', DateType::class, [
                'label'  => 'autorized_user.end_date',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => [
                    'class' => 'datepicker',
                    'autocomplete' => 'off'
                ],
                'required'   => false
            ])
            ->add('duplicate', NumberType::class,
                [
                    'label' => 'autorized_user.duplication',
                    'required' => true,
                    'mapped' => false,
                    'constraints' => [
                        new Assert\Length(['min'=>1,'max'=>30]),
                        new Assert\NotBlank( ['message' => "form.field_is_required" ] )
                    ]
                ]
            );

        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.validate',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AuthorizedUser::class,
            'roles'      => [],
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'authorized_user';
    }
}