<?php

namespace AppBundle\Form\Type\Payment;

use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\Payment\Order;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('title', TextType::class,
            [
                'label' => 'Titre',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                ]
            ])->add('reference', TextType::class,
            [
                'label' => 'Référence',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                ],
                'mapped' => false
            ])
            ->add('totalAmount', MoneyType::class,
            [
                'label' => 'Montant TTC',
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank( ['message' => "form.field_is_required" ] ),
                ]
            ]);

        $builder->add('group', EntityType::class, [
                'label' => 'Group',
                'class' => Group::class,
                'choice_label'  => 'name',
                'choice_value'  => 'id',
                'query_builder' => function (EntityRepository $qb){
                    return $qb->createQueryBuilder('g')->andWhere('g.parentGroup IS NULL');
                },
                'multiple'   => false,
                'expanded'   => false,
                'empty_data' => null,
                'required' => false,
                'attr' => [
                    'class'   => 'select2'
                ]
            ]
        );

        //Submit
        $builder->add('submit',SubmitType::class, [
            'label' => 'action.validate',
            'attr'  => [
                'class' => 'btn btn-success'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Order::class,
            'roles'      => [],
            'translation_domain' => 'commiti'
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'invoice';
    }
}