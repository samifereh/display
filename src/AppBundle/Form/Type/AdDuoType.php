<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 02/08/16
 * Time: 11:27
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use Infinite\FormBundle\Form\Type\PolyCollectionType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Form\Validator\Constraints as AppAssert;

class AdDuoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Ad $ad */
        $ad = $builder->getData();
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('pays', ChoiceType::class,
                        [
                            'label' => 'ads.ad.country',
                            'preferred_choices' => ['FR'],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ],
                            'required' => true,
                            'choices'  => [
                               'FR' => 'France'
                            ],
                            'attr' => [ 'class' => 'select2' ]
                        ]
                    )
                    ->add('ville', TextType::class,
                        [
                            'label' => 'ads.ad.city',
                            'required' => true,
                            'attr' => [
                                'class' => 'city-list',
                                'typeahead' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('codePostal', TextType::class,
                        [
                            'label' => 'ads.ad.zip_code',
                            'required' => true,
                            'attr' => [
                                'class' => 'cp'
                            ],
                            'constraints' => [
                                new Assert\Length(['groups'=>['Default','flow_ad_step1'],'min'=>4,'max'=>6]),
                                new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step1'],'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )
                    ->add('adresse', TextType::class,
                        [
                            'label' => 'ads.ad.adresse',
                            'required' => false
                        ]
                    )
                    ->add('lienVisitVirtuel', TextType::class,
                        [
                            'label' => 'ads.ad.virtual_visit_link',
                            'required' => false,
                            'constraints' => [
                                new Assert\Url(['groups'=>['Default','flow_ad_step1']])
                            ]
                        ]
                    )
                    ->add('siProximiteAeroport', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_aeroport',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteSortieAutoroute', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_sortie_autoroute',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteGare', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximité_gare',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteBus', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximité_bus',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteTramway', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_tramway',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteParking', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_parking',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCommerces', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_commerces',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCentreCommercial', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_centre_commercial',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCentreville', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_centre_ville',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCreche', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_creche',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteEcolePrimaire', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_ecole_primaire',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteCollege', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_college',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteLycee', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_lycee',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteUniversite', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_universite',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteEspacesVerts', CheckboxType::class, [
                        'label'    => 'ads.ad.proximite.proximite_espaces_verts',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('siProximiteMetro', CheckboxType::class, [
                        'label'    => 'ads.ad.procheMetro',
                        'required' => false,
                        'mapped'   => false,
                        'attr' => [
                            'class' => 'i-checks'
                        ]
                    ])
                    ->add('environnement', ChoiceType::class, [
                        'label' => 'ads.ad.environnement',
                        'required' => false,
                        'mapped'   => false,
                        'choices' => Terrain::TERRAIN_ENV,
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ])
                    ->add('exposition', ChoiceType::class, [
                        'label' => 'ads.ad.exposition',
                        'required' => false,
                        'mapped'   => false,
                        'choices' => Terrain::EXPOSITION,
                        'attr' => [
                            'class' => 'select2'
                        ]
                    ])
                    ->add('belleVue', ChoiceType::class, [
                        'empty_value' =>false,
                        'label' => "ads.ad.nice_view",
                        'required' => false,
                        'mapped'   => false,
                        'choices' =>  [true => 'Oui',false => 'Non'],
                        'expanded' => true
                    ])
                    ->add('calme', ChoiceType::class, [
                        'empty_value' =>false,
                        'label' => "ads.ad.calme",
                        'required' => false,
                        'mapped'   => false,
                        'choices' =>  [true => 'Oui',false => 'Non'],
                        'expanded' => true
                    ]);
                break;
            case 2:
                $isDuo = $ad->getTypeDiffusion() == 'duo' ? true : false;
                $builder->add('libelle', TextType::class,
                    [
                        'label' => 'ads.ad.title',
                        'required' => true,
                        'constraints' => [
                            new Assert\NotBlank( ['groups'=>['Default','flow_ad_step2'], 'message' => "form.field_is_required" ] ),
                            new Assert\Length( ['groups'=>['Default','flow_ad_step2'],'max' => 64] )
                        ]
                    ])
                    ->add('referenceInterne', TextType::class,
                        [
                            'label' => 'ads.ad.internal_reference',
                            'required' => false
                        ]
                    )
                    ->add('prix', MoneyType::class,
                        [
                            'label' => 'ads.ad.price',
                            'attr' => [ 'prepend' => true],
                            'required' => true,
                            'constraints' => [
                                new Assert\NotBlank( ['groups'=>['Default','flow_ad_step2'], 'message' => "form.field_is_required" ] )
                            ]
                        ]
                    )->add('lienVisitVirtuel', TextType::class,
                        [
                            'label' => 'ads.ad.virtual_visit_link',
                            'required' => false,
                            'constraints' => [
                                new Assert\Url(['groups'=>['Default','flow_ad_step2']])
                            ]
                        ]
                    )
                    ->add('description', TextareaType::class, [
                        'label' => "ads.ad.description",
                        'required' => true,
                        'attr' => [
                            'rows' => '20'
                        ],
                        'constraints' => [
                            new Assert\NotBlank( [ 'groups'=>['Default','flow_ad_step2'],'message' => "form.field_is_required" ] )
                        ]
                    ]);

                $builder->add('titreImagePrincipale', TextType::class, [
                    'label'         => 'ads.ad.principale_image_title',
                    'required'      => false
                ]);
                $groups = $options['checkImageEmpty'] ? ["Default",'flow_ad_step2'] : ['Default'];
                $builder->add('imagePrincipale', ImageUploadableType::class, [
                    'data' => new File($ad->getImagePrincipale(), false),
                    'required' => true,
                    'label'    => 'ads.ad.principale_image',
                    'attr' => [
                        'class' => 'col-md-12 filestyle',
                        'data-buttonText' => 'choose_file'
                    ],
                    'error_bubbling' => false,
                    'constraints' => [
                        new Assert\File( ['groups'=>$groups, 'mimeTypes' => ["image/jpeg", "image/pjpeg"] ] )
                    ]
                ]);
                $builder->add('bienImmobilier', PolyCollectionType::class, array(
                    'types' => array(
                        MaisonType::class,
                        TerrainType::class,
                        AppartementType::class
                    ),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => ['isDuo' => $isDuo, 'groups'=> ['Default','flow_ad_step2'], 'groups_terrain' => ['Default','flow_ad_step3']],
                    'error_bubbling' => true

                ));
                //$builder->add('maison', MaisonType::class, ['label' => false,'flow_step'=> 'flow_ad_step2', 'data' => $ad->getMaison() ]);
            break;
            case 3:
                $builder->add('bienImmobilier', PolyCollectionType::class, array(
                    'types' => array(
                        MaisonType::class,
                        TerrainType::class,
                        AppartementType::class
                    ),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'options' => ['isDuo' => true, 'groups'=> ['Default','flow_ad_step2'], 'groups_terrain' => ['Default','flow_ad_step3']]

                ));
                //$builder->add('terrain', TerrainType::class, ['label' => false,'flow_step'=> 'flow_ad_step2' ]);
                break;
            case 4:
                break;
        }
        parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'isDuo'           => false,
            'bienImage'       => null,
            'checkImageEmpty' => true,
            'data_class'      => Ad::class,
            'cascade_validation' => true,
            'translation_domain' => 'commiti'
        ));
    }

    public function getName() {
        return 'ad';
    }

}