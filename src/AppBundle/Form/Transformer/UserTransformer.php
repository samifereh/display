<?php
namespace AppBundle\Form\Transformer;

use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Created by PhpStorm.
 * User: cha9chi
 * Date: 12/20/16
 * Time: 5:00 PM
 */
class UserTransformer implements DataTransformerInterface
{
    /**
     *
     * @param  ArrayCollection|null $users
     * @return string
     */
    public function transform($users)
    {
        if($users instanceof ArrayCollection)
            return $users;
        return null;
    }

    /***
     * @return ArrayCollection|null
     * @throws TransformationFailedException
     */
    public function reverseTransform($users)
    {
        if($users instanceof User)
            return new ArrayCollection([$users]);
        if(!$users)
            return new ArrayCollection();
        return $users;
    }
}