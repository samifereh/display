<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;


use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;


/**
 * Class InvoiceModerationDatatable
 *
 * @package AppBundle\Datatables
 */
class InvoiceModerationDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    protected function initFeatures(array $options = [])
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('invoice_create'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_moderation_invoices'),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    protected function buildColumns(array $options = [])
    {
        $this->columnBuilder
            ->add('remoteId','column',['title' => 'Id', 'class'=> 'hidden'])
            ->add('group.name','column',['title' => $this->translator->trans('Agence',array(),'commiti')])
            ->add('totalAmount','column',['title' => $this->translator->trans('Montant',array(),'commiti')])
            ->add('payMethod','column',['title' => $this->translator->trans('Paiement',array(),'commiti')])
            ->add(
                'createdAt','datetime',
                [
                    'title' => $this->translator->trans('created_at',array(),'commiti'),
                    'date_format' => 'DD-MM-YYYY'
                ]
            )
            ->add('packId', 'column', array('title' => $this->translator->trans('product',array(),'commiti') ))
            ->add('status', 'column', array('title' => $this->translator->trans('status',array(),'commiti') ))
            ->add(
                null,
                'action',
                [
                    'title' => $this->translator->trans('datatables.actions.title'),
                    'actions' => [
                        [
                            'route' => 'invoice_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'invoice_pdf',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-save',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.export'),
                                'class' => 'btn btn-warning btn-xs btn-without-label btn-orange',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'moderation_invoices_validate',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-check',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.validate'),
                                'class' => 'btn btn-success btn-xs btn-without-label btn-green',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'moderation_invoices_decline',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-remove',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.decline'),
                                'class' => 'btn btn-danger btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ]
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Payment\Order';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'invoice_datatable';
    }
}