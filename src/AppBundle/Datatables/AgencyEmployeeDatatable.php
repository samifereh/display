<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\User;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class UserDatatable
 *
 * @package AppBundle\Datatables
 */
class AgencyEmployeeDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter(array $options = array())
    {
        $formatter = function($line){
            $roles = [];
            foreach($line['roles'] as $role) {
                $roles[] = /** @Ignore */$this->translator->trans($role,[],'commiti');
            }
            $line['roles'] = implode(', ',$roles);
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $groupRemoteId = isset($options['group']) ? $options['group']->getRemoteId() : null;
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div><div id="delete_user_confirm" class="hide">'.$this->translator->trans('datatables.actions.delete_user_confirm', [] ,'commiti').'</div> ',
            'actions' => array(
                array(
                    'route' => $this->router->generate('agency_employee_add',['remoteId'=>$groupRemoteId]),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-primary',
                        'role' => 'button'
                    ),
                    'render_if' => function() use ($options) {
                        if(isset($options['group'])) {
                            foreach($options['group']->getAuthorizedUser() as $autorization) {
                                if(is_null($autorization->getUser()))
                                    return true;
                            }
                            return false;
                        }
                        return true;
                    }
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));
        $this->ajax->set(array(
            'url' => $this->router->generate('api_agency_employees',['remoteId'=>$groupRemoteId]),
            'type' => 'GET'
        ));
    
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('username', 'column',['visible' => false])
            ->add('name', 'column', array(
                'title' => $this->translator->trans('Nom',array(),'commiti')
            ))
            ->add('remoteId', 'column', ['visible' => false])
            ->add('roles', 'column', array(
                'title' => $this->translator->trans('Roles',array(),'commiti')
            ))
            ->add(null, 'action', array(
                'title' => "Actions",
                'actions' => array(
                    array(
                        'route' => 'agency_employee_show',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show',array(),'commiti'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'agency_employee_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'));
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit',array(),'commiti'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'agency_employee_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function($row) {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING') &&
                                ($this->securityToken->getToken()->getUser()->getId() != $row['id'])
                            );
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "L'utilisateur a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete',array(),'commiti'),
                            'class' => 'btn btn-danger btn-xs need_confirmation user-delete-confirm btn-without-label trigger-delete',
                        ),
                    ),
                    array(
                        'route' => 'homepage',
                        'route_parameters' => array(
                            '_switch_user' => 'username'
                        ),
                        'render_if' => function($row) {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                        },
                        'label' => '',
                        'icon' => 'fa fa-exchange',
                        'attributes' => array(
                            'title' => $this->translator->trans('datatables.actions.switch_user',array(),'commiti'),
                            'class' => 'btn btn-blue btn-xs btn-without-label',
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return User::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_datatable';
    }

}
