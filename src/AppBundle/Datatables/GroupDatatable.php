<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class GroupDatatable
 *
 * @package AppBundle\Datatables
 */
class GroupDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter(array $options = array())
    {
        $repository = $this->em->getRepository('AppBundle:Group');

        $formatter = function($line) use($repository) {

            /** @var Group $group */
            $group = $repository->find($line['id']);

            $roles = [];
            foreach($line['roles'] as $role) {
                $roles[] = /** @Ignore */$this->translator->trans($role,[],'commiti');
            }
            $line['roles'] = implode(', ',$roles);
            $line['hasPricing'] = $group && !$group->getParentGroup() ? true : false;
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('groups_admin_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $this->ajax->set(array(
            'url' => $this->router->generate('api_groups'),
            'type' => 'GET'
        ));

        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );

        $this->columnBuilder
            ->add('remoteId', 'column', array(
                'title' => 'Id',
                'class'=> 'hidden'
            ))
            ->add('name', 'column', array(
                'title' => $this->translator->trans('Nom',array(),'commiti')
            ))
            ->add('roles', 'column', array(
                'title' => $this->translator->trans('Type',array(),'commiti')
            ))
            ->add('parentGroup.name', 'column', array(
                'title' => $this->translator->trans('Groupe',array(),'commiti')
            ))
            ->add('blocked', 'boolean', array(
                'title' => $this->translator->trans('Blocked',array(),'commiti'),
                'true_icon' => 'glyphicon glyphicon-ok',
                'false_icon' => 'glyphicon glyphicon-remove',
                'true_label' => $this->translator->trans('Yes',array(),'commiti'),
                'false_label' => $this->translator->trans('No',array(),'commiti'),
            ))
            ->add(null, 'action', array(
                'title' => '',
                'actions' => array(
                    array(
                        'route' => 'groups_admin_show',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'groups_admin_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-pencil',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'groups_admin_block',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-ban-circle',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.blocked'),
                            'class' => 'btn btn-success btn-xs btn-without-label btn-green',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'groups_admin_authorization',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-lock',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.authorization'),
                            'class' => 'btn btn-success btn-xs btn-without-label btn-warning',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return $row['roles'] != 'Groupe';
                        }
                    ),
                    array(
                        'route' => 'groups_admin_pricing',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eur',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.pricing'),
                            'class' => 'btn btn-info btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return $row['parentGroup'] == null;
                        }
                    ),
                    array(
                        'route' => 'groups_admin_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => $this->translator->trans('group.group_has_been_deleted',array(),'commiti'),
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Group';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'group_datatable';
    }
}
