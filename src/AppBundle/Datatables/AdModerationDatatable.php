<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;


use AppBundle\Entity\Ad;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;


/**
 * Class AdModerationDatatable
 *
 * @package AppBundle\Datatables
 */
class AdModerationDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);

        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/ads_moderation.js.twig'
            )
        ));
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {

        $groupId = isset($options['group']) ? ($options['group'] instanceof \AppBundle\Entity\Group ? $options['group']->getId() : $options['group']) : 0;
        $data    = isset($options['data']) ? http_build_query($options['data']) : null;

        $this->ajax->set(array(
            'url' => $this->router->generate('api_ads_moderation', ['id' => $groupId, 'data' => $data]),
            'type' => 'GET'
        ));
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Ad');

        $formatter = function($line) use ($repository){
            /** @var Ad $ad */
            $ad = $repository->find($line['id']);

            $line['ref'] = '<b>'.$ad->getReference() . '</b><br/><small>' . $ad->getLibelle().' 
            ('.$ad->getCodePostal().')</small>';
            $line['date'] = '<b>'.$this->translator->trans('action.remonter',array(),'commiti').'
            :</b> '.$ad->getUpdatedAt()->format('d/m/Y') . '<br/><b>Créée le: </b>' . $ad->getCreatedAt()->format('d/m/Y');

            switch($ad->getStatValue()) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green'; break;
            }
            $line['stat'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-status" 
            data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            $line['informer'] = '<a href="javascript:void(0);" data-target="#notificationModal" data-toggle="modal"  
            class="btn btn-stat btn-icon btn-datatable-informer" data-id="'.$line['remoteId'].'">
            <i class="glyphicon glyphicon-send"></i></a>';
            $line['affecter'] = $ad->getGroup() ? '<a href="javascript:void(0);" data-target="#affectModal"
            data-toggle="modal"  class="btn btn-stat btn-icon btn-datatable-affect" data-id="'.$line['remoteId'].'" 
            data-group="'.$ad->getGroup()->getRemoteId().'"><i class="glyphicon glyphicon-flag"></i></a>' : '';
            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        $this->columnBuilder
            ->add('remoteId','column',['visible' => false])
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'delete',
                        ],
                        'label' => $this->translator->trans('datatables.actions.delete'),
                        'icon' => 'fa fa-trash',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'activer',
                        ],
                        'label' => $this->translator->trans('datatables.actions.activate'),
                        'icon' => 'glyphicon glyphicon-check',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.activate'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-blue',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'desactiver',
                        ],
                        'label' => $this->translator->trans('datatables.actions.disactivate'),
                        'icon' => 'glyphicon glyphicon-off',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.disactivate'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-orange',
                            'role' => 'button'
                        ),
                    )
                )
            ))
            ->add('ref','virtual',['title' => $this->translator->trans('Réf. + Nom',array(),'commiti')])
            ->add('ville', 'column', array('title' => $this->translator->trans('ads.ad.city',array(),'commiti') ))
            ->add('group.name', 'column', array('title' => $this->translator->trans('Agence',array(),'commiti'),
                'searchable' => false ))
            ->add('owner.username', 'column', array('title' => $this->translator->trans('Commercial',array(),'commiti') ))
            ->add('date','virtual',['title' => $this->translator->trans('Date',array(),'commiti')])
            ->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('percent', 'progress_bar', array(
                'title'         => $this->translator->trans('Complete',array(),'commiti'),
                'render'        => 'render_progress_bar_extend',
                'label'         => true,
                'class'         => 'progress m-t-xs full',
                'value_min'     => '0',
                'value_max'     => '100',
                'multi_color'   => true
            ))
            ->add('stat','column',['title' => $this->translator->trans('publish',array(),'commiti')])
            ->add('informer','virtual',['title' => $this->translator->trans('Informer',array(),'commiti')])
            ->add('affecter','virtual',['title' => $this->translator->trans('Affecter',array(),'commiti')])
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'ads_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => $this->translator->trans('action.open',array(),'commiti'),
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-label btn-orange',
                                'rel' => 'tooltip',
                                'title' =>$this->translator->trans('datatables.actions.show'),
                                'role' => 'button'
                            ]
                        ],
                        [
                            'route' => 'ads_edit',
                            'route_parameters' => [
                                'projectType'  => 'typeDiffusion',
                                'remoteId'   => 'remoteId',
                            ],
                            'label' => $this->translator->trans('datatables.actions.edit'),
                            'icon' => 'glyphicon glyphicon-edit',
                            'attributes' => array(
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit'),
                                'class' => 'btn btn-primary btn-xs btn-label btn-blue',
                            )
                        ]
                    ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Ad';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ad_moderation_datatable';
    }
}