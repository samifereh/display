<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class UserDatatable
 *
 * @package AppBundle\Datatables
 */
class UserDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter(array $options = array())
    {
        $formatter = function($line){
            $roles = [];
            foreach($line['roles'] as $role) {
                $roles[] = /** @Ignore */$this->translator->trans($role,[],'commiti');
            }
            $line['roles'] = implode(', ',$roles);
            $status  = "";
            $class='glyphicon glyphicon-remove-circle icon-red';
            $enSwitchStatusTitle =  $this->translator->trans('user.switch.enable',array(),'commiti');
            $disSwitchStatusTitle =  $this->translator->trans('user.switch.disable',array(),'commiti');
            $statusTitle = $enSwitchStatusTitle;
            if($line['enabled']) {
                $class='glyphicon glyphicon-ok-circle icon-green';
                $statusTitle = $disSwitchStatusTitle;
            }
            if(
            $this->authorizationChecker->isGranted('ROLE_ADMIN') ||
            $this->authorizationChecker->isGranted('ROLE_MARKETING')
            ) {
                $status .= '<a href="javascript:void(0);" title="'.$statusTitle.'" data-en-switch-title="'.$enSwitchStatusTitle.'"
                data-dis-switch-title="'.$disSwitchStatusTitle.'"
                class="btn btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            } else {
                $status .= '<div class="btn-stat btn-icon btn-2x"><i class="'.$class.'"></i></div></div>';
            }
            $line['enabled'] = $status;

            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/users.js.twig'
            ),
        ));
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div><div id="delete_user_confirm" class="hide">'.$this->translator->trans('datatables.actions.delete_user_confirm', [] ,'commiti').'</div> ',
            'actions' => array(
                array(
                    'route' => $this->router->generate('users_admin_create'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $this->ajax->set(array(
            'url' => $this->router->generate('api_users'),
            'type' => 'GET'
        ));
        
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('username', 'column', array('visible' => false))
            ->add('name', 'column', array(
                'title' => $this->translator->trans('Nom',array(),'commiti'),
            ))
            ->add('groups.name', 'array', array(
                'title' => $this->translator->trans('Groupe',array(),'commiti'),
                'orderable'=>false,
                'searchable'=>true,
                'data' => 'groups[, ].name' // required option
            ))
            ->add('roles', 'column', array(
                'title' => $this->translator->trans('user.type',array(),'commiti')
            ))
            ->add('enabled', 'column', array(
                'title' => $this->translator->trans('user.enabled',array(),'commiti'),
                'class' => "text-center"

            ))
            ->add(null, 'action', array(
                'title' => $this->translator->trans('datatables.actions.title'),
                'actions' => array(
                    array(
                        'route' => 'agency_employee_show',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'agency_employee_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'users_admin_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "L'utilisateur a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label user-delete-confirm',
                        )
                    ),
                    array(
                        'route' => 'homepage',
                        'route_parameters' => array(
                            '_switch_user' => 'username'
                        ),
                        'render_if' => function($row) {
                            return ($this->authorizationChecker->isGranted('ROLE_ADMIN'));
                        },
                        'label' => '',
                        'icon' => 'fa fa-exchange',
                        'attributes' => array(
                            'title' =>  $this->translator->trans('datatables.actions.switch_user'),
                            'class' => 'btn btn-blue btn-xs btn-without-label',
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\User';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user_datatable';
    }
}
