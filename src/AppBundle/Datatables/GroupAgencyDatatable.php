<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\AuthorizedAgency;
use AppBundle\Entity\User;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class GroupAgencyDatatable
 *
 * @package AppBundle\Datatables
 */
class GroupAgencyDatatable extends AbstractDatatableView
{

    public function buildDatatable(array $options = array())
    {
        $groupRemoteId = isset($options['group']) ? $options['group']->getRemoteId() : null;
        /** @var User $user */
        $user = $this->securityToken->getToken()->getUser();
        $remoteId = $user->getRemoteId();
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->authorizationChecker->isGranted('ROLE_ADMIN') ?
                        $this->router->generate('groups_admin_add',['remoteId' => $groupRemoteId]) :
                        $this->router->generate('group_agency_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                    'render_if' => function() use ($user) {
                        if(!$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                            foreach($user->getGroups() as $group ) {
                                foreach($group->getAuthorizedAgency() as $autorization) {
                                    if(is_null($autorization->getGroup()))
                                        return true;
                                }
                            }
                            return false;
                        }
                        return true;
                    }
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $this->ajax->set(array(
            'url' => $this->router->generate('api_group_agency',['remoteId' => $groupRemoteId]),
            'type' => 'GET'
        ));

        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );

        $this->columnBuilder
            ->add('remoteId', 'column',array(
                'title' => $this->translator->trans('Nom',array(),'commiti'),
                'class'=> 'hidden'))
            ->add('name', 'column', array('title' => $this->translator->trans('Nom',array(),'commiti')))
            ->add('parentGroup.name', 'column', array('searchable'=>true,'title' => $this->translator->trans('Groupe',array(),'commiti') ))
            ->add(null, 'action', array(
                'title' => '',
                'actions' => array(
                    array(
                        'route' => 'group_agency_show',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'group_agency_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'group_agency_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "Le groupe a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Group';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'group_agency_datatable';
    }
}
