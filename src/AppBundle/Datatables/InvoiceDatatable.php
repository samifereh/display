<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;


use AppBundle\Entity\Payment\Order;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;


/**
 * Class InvoiceDatatable
 *
 * @package AppBundle\Datatables
 */
class InvoiceDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {
        $remoteId = isset($options['group']) && $options['group'] != 'all' ? $options['group']->getRemoteId() : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_invoices',['remoteId'=>$remoteId]),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [4, 'desc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter(array $options = array())
    {
        $formatter = function($line){

            if( $line['status'] =='canceled' || $line['status'] =='failed') { $line['actions'] = ''; }
            $line['totalAmount'] = number_format($line['totalAmount'],2,',',' ').'€';
            $status = $line['status'];
            $line['status'] = /** @Ignore */$this->translator->trans('invoices.'.$line['status'],array(),'commiti');
            if($line['payMethod'])
                $line['payMethod'] = /** @Ignore */$this->translator->trans('invoices.method.'.$line['payMethod'],array(),'commiti');
            if($status == 'waiting_admin_validation')
                $line['status'].= ' (<b class="green"><a href="'.$this->router->generate('bankwire_show',['remoteId'=>$line['remoteId']]).'">'.$this->translator->trans('invoice.transfert_object',array(),'commiti').'</a></b>)';
            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        $this->columnBuilder
            ->add('remoteId','column',['visible' => false])
            ->add('group.name','column',['searchable' => false,'orderable' => false,'title' => $this->translator->trans('Organisme',array(),'commiti')])
            ->add('totalAmount','column',['title' => $this->translator->trans('Montant',array(),'commiti')])
            ->add('payMethod','column',['title' => $this->translator->trans('Paiement',array(),'commiti')])

            ->add(
                'createdAt','datetime',
                [
                    'title' => $this->translator->trans('created_at',array(),'commiti'),
                    'date_format' => 'DD-MM-YYYY'
                ]
            )
            ->add('packId', 'column', array('title' => $this->translator->trans('product',array(),'commiti') ))
            ->add('status', 'column', array('title' => $this->translator->trans('status',array(),'commiti') ))
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'invoice_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'invoice_pdf',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-save',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.export'),
                                'class' => 'btn btn-success btn-xs btn-without-label btn-green',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'invoice_delete',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'render_if' => function($row) {
                                return (
                                    $row['status'] === 'waiting'
                                );
                            },
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete'),
                                'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                                'role' => 'button',
                            ],
                        ]//todo: add continue purchase action
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Order::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'invoice_datatable';
    }
}