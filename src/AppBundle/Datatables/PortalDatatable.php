<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 11:00
 */

namespace AppBundle\Datatables;


use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

class PortalDatatable extends AbstractDatatableView
{
    public function buildDatatable(array $options = array())
    {
        $this->initTopActions($options);
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    public function initTopActions($options) {
        $this->topActions->set(
            [
                'start_html' => '<div class="row"><div class="col-sm-3">',
                'end_html' => '<hr></div></div>',
                'actions' => [
                    [
                        'route' => $this->router->generate('portal_create'),
                        'label' => $this->translator->trans('datatables.actions.new'),
                        'icon' => 'glyphicon glyphicon-plus',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.new'),
                            'class' => 'btn btn-primary',
                            'role' => 'button',
                        ],
                    ],
                ],
            ]
        );
    }

    public function initFeatures($options) {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'buttons' =>
                        array(
                            'copy' => array('extend' => 'copy','text' => 'Copie','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'excel' => array('extend' => 'excel','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'print' => array('extend' => 'print','text' => 'Imprimer','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'pdf' => array('extend' => 'pdf','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        ),
                    'responsive' => true
                )
            )
        );
    }

    public function initAjax($options) {
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_portal_list'),
                'type' => 'GET',
            )
        );
    }

    public function initOptions($options) {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false
            )
        );
    }

    public function buildColumns($options) {
        $this->columnBuilder
            ->add(
                'id', 'column', [
                    'title' => 'id',
                ]
            )
            ->add('name', 'column', [
                    'title' => $this->translator->trans('portal.name',array(),'commiti')
                ])
            ->add('icon', 'image', array(
                'title' => 'Logo',
                'relative_path' => ''
            ))
            ->add('typeDiffusion', 'column', [
                'title' => $this->translator->trans('portal.diffusion_type',array(),'commiti')
            ])
            ->add('cluster', 'column', [
                'title' => $this->translator->trans('portal.cluster',array(),'commiti')
            ])
            ->add('isAvailable', 'boolean', [
                'title' => $this->translator->trans('portal.available',array(),'commiti'),
                'true_icon' => 'glyphicon glyphicon-ok',
                'false_icon' => 'glyphicon glyphicon-remove',
                'true_label' => $this->translator->trans('Yes',array(),'commiti'),
                'false_label' => $this->translator->trans('No',array(),'commiti')
            ])
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'portal_show',
                            'route_parameters' => [
                                'id' => 'id',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'portal_edit',
                            'route_parameters' => [
                                'id' => 'id',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-pencil',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            ],
                        ],
                        [
                            'route' => 'portal_delete',
                            'route_parameters' => [
                                'id' => 'id'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete'),
                                'class' => 'btn btn-danger btn-xs need_confirmation trigger-delete btn-without-label',
                            ],
                        ],
                    ],
                ]
            );
    }

    public function getEntity() {
        return 'AppBundle\Entity\Broadcast\Portal';
    }

    public function getName() {
        return 'portal_datatable';
    }
}