<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;

use AppBundle\Entity\Ad;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class AdDatatable
 *
 * @package AppBundle\Datatables
 */
class AdByProgramDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initTopActions($options);
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
        $this->callBack($options);
    }

    protected function callBack(array $options = []) {
        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/ads_program_moderation.js.twig'
            )
        ));
    }

    protected function initTopActions(array $options = [])
    {
        $this->topActions->set(
            [
                'start_html' => '<div class="row"><div class="col-sm-3">',
                'end_html' => '<hr></div></div>',
                'actions' => [
                    [
                        'route' => $this->router->generate('ads_create_program_2',
                            ['remoteId'=>$options['program']->getremoteId()]),
                        'label' => $this->translator->trans('Ajouter un lot'),
                        'icon' => 'glyphicon glyphicon-plus',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('Ajouter un lot'),
                            'class' => 'btn btn-primary',
                            'role' => 'button',
                        ],
                    ],
                ],
            ]
        );
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_ads_by_program',['remoteId'=>$options['program']->getremoteId()]),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 4,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Ad');
        $formatter = function($line) use ($repository){
            /** @var Ad $ad */
            $ad = $repository->find($line['id']);

            $line['diffused'] = $ad->getDiffusion() ? 'Oui' : 'Non';
            $line['ref'] = '<b>'.$ad->getReference() . '</b><br/><small>' . $ad->getLibelle().'</small>';
            $line['date'] = '<b>Remontée:</b> '.$ad->getUpdatedAt()->format('d/m/Y') . '<br/><b>Créée le: </b>' . $ad->getCreatedAt()->format('d/m/Y');

            $line['prix'] = number_format($line['prix'],2,',',' ').' €';
            switch($ad->getStatValue()) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange';$status = 'En attente'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red';$status = 'Annuler'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green';$status = 'Valider'; break;
                case 'reserver': $class='glyphicon glyphicon-lock icon-blue';$status = 'Réserver'; break;
                case 'vendu': $class='glyphicon glyphicon-euro icon-purple';$status = 'Vendu'; break;
            }
            $line['stat'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i><p>'.$status.'</p></a>';
            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        $this->columnBuilder
            ->add('remoteId','column',['visible' => false])
            ->add('ref','virtual',['title' => $this->translator->trans('Réf. + Nom',array(),'commiti')])
            ->add('prix','column',['title' => $this->translator->trans('ads.ad.price',array(),'commiti')])
            ->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('diffused','virtual',['title' => $this->translator->trans('Diffuser seule',array(),'commiti')])
            ->add('stat','virtual',['title' =>  $this->translator->trans('ads.ad.status',array(),'commiti')])
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'ads_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ]
                        ],
                        [
                            'route' => 'ads_edit_program',
                            'route_parameters' => [
                                'remoteId'   => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-pencil',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            ]
                        ],
                        [
                            'route' => 'diffusion_ads_project',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-send',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.diffuse',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-green',
                            ],
                        ],
                        [
                            'route' => 'ads_delete',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete',array(),'commiti'),
                                'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                            ]
                        ]
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Ad';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ad_datatable';
    }
}