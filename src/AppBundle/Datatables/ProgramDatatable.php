<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;


use AppBundle\Entity\Group;
use AppBundle\Entity\Program;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;


/**
 * Class ProgramDatatable
 *
 * @package AppBundle\Datatables
 */
class ProgramDatatable extends AbstractDatatableView
{
    private $trackingAdvantage;
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initTopActions($options);
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);

        $callbacks = array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/programs.js.twig'
            ),
        );
        if(isset($options["trackingAdvantage"])){
            $callbacks['draw_callback'] = array(
                'template' => 'AppBundle:Common:Datatable/draw_callback.js.twig',
                'vars' => array('id' => $options["trackingAdvantage"]->getId()),
            );
        }

        $this->callbacks->set($callbacks);

    }

    protected function initTopActions(array $options = [])
    {
        $urlCreateProgramme = $this->router->generate('program_create_1');
        $this->topActions->set(
            [
                'start_html' => '<div class="row"><div class="col-sm-3">',
                'end_html' => '<hr></div></div>',
                'actions' => [
                    [
                        'route' => $this->authorizationChecker->isGranted("ROLE_MARKETING") ?
                                    $this->router->generate('switch_commercial', [
                                        'redirect' => $urlCreateProgramme
                                    ]):
                                    $urlCreateProgramme,
                        'label' => $this->translator->trans('datatables.actions.new'),
                        'icon' => 'glyphicon glyphicon-plus',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.new'),
                            'class' => 'btn btn-success',
                            'role' => 'button',
                        ],
                    ],
                ],
            ]
        );
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'buttons' =>
                        array(
                            'copy' => array('extend' => 'copy','text' => 'Copie','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'excel' => array('extend' => 'excel','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'print' => array('extend' => 'print','text' => 'Imprimer','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'pdf' => array('extend' => 'pdf','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        ),
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {
        $groupId = isset($options['group']) ? ($options['group'] instanceof Group ? $options['group']->getId() :
            $options['group']) : 0;
        $data = isset($options['data']) ? http_build_query($options['data']) : null;

        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_program', array('id' => $groupId, 'data' => $data)),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Program');

        $formatter = function($line) use ($repository){
            /** @var Program $program */
            $program = $repository->find($line['id']);

            $line['type'] = ucfirst($program->getType());
            $line['ref'] = '<b>'.$program->getReference() . '</b><br/><small>' . $program->getTitle().'('.$program->getCodePostal().')</small>';
            $line['date'] = '<b>'.$this->translator->trans('remonter',array(),'commiti').':</b> '.$program->getUpdatedAt()->format('d/m/Y') . '<br/><b>'.$this->translator->trans('created_at',array(),'commiti').': </b>' . $program->getCreatedAt()->format('d/m/Y');

            switch($program->getStatValue()) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green'; break;
            }
            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
                $line['stat'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            } else {
                $line['stat'] = '<div class="text-center btn-stat btn-icon btn-2x"><i class="'.$class.'"></i></div>';
            }

            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER') && $program->getTrackingPhone()){
                $sum = $this->em->getRepository("TrackingBundle:Statistique\TrackingCallStatistique")->getPhoneStats(
                    $program->getTrackingPhone()
                );
                $statistics = "<p>".$this->translator->trans('TrackingPhone',array(),'commiti')." ".
                    $program->getTrackingPhone()."<br />".
                    ($sum > 1 ?
                        $this->translator->trans('Calls',array("%total%" => $sum),'commiti') :
                        $this->translator->trans('Call',array("%total%" => $sum),'commiti')
                    ).
                    "</p>";
                $line['stat'].=$statistics;
            }

            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        $this->trackingAdvantage = (isset($options["trackingAdvantage"]) ? $options["trackingAdvantage"] : "");
        $this->columnBuilder
            ->add('remoteId','column',['title' => 'Id', 'class'=> 'hidden'])
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'programs_actions',
                        'route_parameters' => [
                            'action' => 'delete',
                        ],
                        'label' => $this->translator->trans('datatables.actions.delete'),
                        'icon' => 'fa fa-trash',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'programs_actions',
                        'route_parameters' => [
                            'action' => 'remonter',
                        ],
                        'label' => $this->translator->trans('action.remonter',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-send',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.remonter'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-green',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'programs_actions',
                        'route_parameters' => [
                            'action' => 'tracking',
                        ],
                        'label' => 'Associer',
                        'icon' => 'glyphicon pe-7s-call',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.associate'),
                            'class' => 'btn btn-info btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return (
                                $this->authorizationChecker->isGranted('ROLE_BRANDLEADER') &&
                                $this->trackingAdvantage
                            );
                        }
                    )
                )
            ))
            ->add('ref','virtual',['title' => $this->translator->trans('Réf. + Nom',array(),'commiti')]);
        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                $this->columnBuilder->add('group.parentGroup.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder
                ->add('group.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Agence',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('owner.username', 'column', array('searchable'=>false, 'title' => $this->translator->trans('Commercial',array(),'commiti') ));
        $this->columnBuilder->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('type','virtual',['title' => 'Type'])
            ->add('date','virtual',['title' => 'Date'])
            ->add('stat','virtual',['title' => 'Publiée'])
            ->add('percent', 'progress_bar', array(
                'title'         => $this->translator->trans('Complete',array(),'commiti'),
                'render'        => 'render_progress_bar_extend',
                'label'         => true,
                'class'         => 'progress m-t-xs full',
                'value_min'     => '0',
                'value_max'     => '100',
                'multi_color'   => true
            ))
            ->add(
                null,
                'action',
                [
                    'title' => '',
                    'actions' => [
                        [
                            'route' => 'program_view',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => '',
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'program_edit',
                            'route_parameters' => [
                                'remoteId'   => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-pencil',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            ],
                        ],
                        [
                            'route' => 'program_remonter',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-open',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-blue ',
                            ],
                        ],
                        [
                            'route' => 'diffusion_program',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-send',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-green',
                            ],
                        ],
                        [
                            'route' => 'program_delete',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                            ],
                        ]
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Program';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'program_datatable';
    }
}