<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class DiffusionPortalsDatatable
 * @package AppBundle\Datatables
 */
class DiffusionPortalsDatatable extends AbstractDatatableView
{
    public $group = null;

    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    public function initFeatures($options) {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'buttons' =>
                        array(
                            'copy' => array('extend' => 'copy','text' => 'Copie','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'excel' => array('extend' => 'excel','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'print' => array('extend' => 'print','text' => 'Imprimer','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'pdf' => array('extend' => 'pdf','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        ),
                    'responsive' => true
                )
            )
        );
    }

    public function initAjax($options) {
        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_diffusion_portals_list',['data' => $data]),
                'type' => 'GET',
            )
        );
    }

    public function initOptions($options) {
        $this->group = isset($options['group']) ? $options['group'] : null;
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false
            )
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Broadcast\\Portal');
        $portalRegistrationRepository        = $this->em->getRepository('AppBundle:Broadcast\\PortalRegistration');
        $commercialPortalLimitRepository     = $this->em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
        $adDailyLimitRepository     = $this->em->getRepository('AppBundle:Broadcast\\AdDailyLimit');
        $formatter = function($line) use ($repository, $adDailyLimitRepository, $portalRegistrationRepository, $commercialPortalLimitRepository){
            /** @var Portal $portal */

            $portal = $repository->find($line['id']);
            $line['cat'] = '<ul class="small-list-padding">';
            if($portal->getCategories())
                foreach ($portal->getCategories() as $category){
                    $line['cat'] .= '<li>'.$category.'</li>';
                }
            $line['cat'] .= '</ul>';


            $line['type'] = '<ul class="small-list-padding">';
            if($types = $portal->getTypeDiffusion()) {
                foreach ($types as $type){
                    $line['type'] .= '<li>'.$type.'</li>';
                }
            }
            $line['type'] .= '</ul>';
            $line['actions'] = '<ul class="small-list-padding">';
            $line['actions'] .= '<li>
                    <a class="btn btn-primary btn-xs btn-without-label" 
                        href="'.$this->router->generate('portails_portail_view',[
                            'slug' => $portal->getSlug(),
                            'id' => $this->group instanceof Group ? $this->group->getId() : null ]).'">
                            Voir la fiche détaillée du portail
                        </a>
                    </li>';
            if($portal->getEspacePro())
                $line['actions'] .= '<li><a class="btn btn-primary btn-xs btn-without-label btn-green" target="_blank" href="'.$portal->getEspacePro().'">Aller sur l’espace Pro du portail</a></li>';
            $line['actions'] .= '</ul>';

            if($this->group instanceof Group) {
                $portalRegistration = $portalRegistrationRepository->findOneBy(['group' => $this->group, 'broadcastPortal' => $portal]);
                $used = $adDailyLimitRepository->getUsedByGroupAndPortal($this->group, $portal);
                $rest = $portalRegistration->getAdLimit()-$used;
                $limit = $portalRegistration->getAdLimit();


                $percent = $limit > 0 ? round(100*($used/$limit),2) : 0;
                $line['volume'] = $line['cluster'] == 'GRATUIT'  ?
                    '---' :
                    '<div class=" progress m-t-xs full">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" 
                                 aria-valuemin="0" aria-valuemax="'.$limit.'" style="width:'.$percent.'%; color: black;">'
                                .$used.'/'.$limit.'
                            </div>
                        </div>
                    </div>';
            }


            return $line;
        };

        return $formatter;
    }

    public function buildColumns($options) {
        $this->columnBuilder
            ->add('id','column',['visible' => false])
            ->add('slug','column',['visible' => false])
            ->add('categories','column',['visible' => false])
            ->add('espacePro','column',['visible' => false])
            ->add('name', 'column', [ 'title' => $this->translator->trans('portal.name',array(),'commiti')])
            ->add('icon', 'image', array( 'title' => 'Logo', 'relative_path' => '' ))
            ->add('zoneGeographique', 'column', [ 'title' => $this->translator->trans('portal.zone_geographique',array(),'commiti')])
            ->add('cat', 'virtual', [ 'title' => $this->translator->trans('portal.categories',array(),'commiti')])
            ->add('type', 'virtual', [ 'title' => $this->translator->trans('portal.diffusion_type',array(),'commiti')])
            ->add('cluster', 'column', [ 'title' => $this->translator->trans('portal.cluster',array(),'commiti') ])
            ->add('volume', 'virtual', [ 'title' => $this->translator->trans('portal.volume_diffusion',array(),'commiti') ])
            ->add('actions', 'virtual', [ 'title' => $this->translator->trans('portal.volume_diffusion',array(),'commiti') ]);
//            ->add(
//                null,
//                'action',
//                [
//                    'title' => 'Actions',
//                    'actions' => [
//                        [
//                            'route' => 'portails_portail_view',
//                            'route_parameters' => [
//                                'slug' => 'slug',
//                            ],
//                            'label' => 'Voir la fiche détaillée du portail',
//                            'icon' => 'glyphicon glyphicon-eye-open',
//                            'attributes' => [
//                                'rel' => 'tooltip',
//                                'title' => 'Voir',
//                                'role' => 'button',
//                                'class' => 'btn btn-primary btn-xs btn-without-label',
//                            ],
//                        ],
//                        [
//                            'route' => 'portails_espace_pro',
//                            'route_parameters' => [
//                                'slug' => 'slug',
//                            ],
//                            'render_if' => function($row) {
//                                return ($this->authorizationChecker->isGranted('ROLE_BRANDLEADER') && $row['cluster'] == 'PAYANT' && $row['espacePro']);
//                            },
//                            'label' => 'Aller sur l’espace Pro du portail',
//                            'icon' => 'glyphicon glyphicon-send',
//                            'attributes' => [
//                                'rel' => 'tooltip',
//                                'title' => 'Envoyer',
//                                'class' => 'btn btn-primary btn-xs btn-without-label btn-green',
//                                'target' => '_blank'
//                            ]
//                        ]
//                    ],
//                ]
//            );
    }

    public function getEntity() {
        return Portal::class;
    }

    public function getName() {
        return 'diffusion_portals_datatable';
    }
}