<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;


use AppBundle\Entity\Ad;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Filter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;


/**
 * Class AdDatatable
 *
 * @package AppBundle\Datatables
 */
class AdDatatable extends AbstractDatatableView
{
    private $trackingAdvantage;
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
        $callbacks = array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/ads.js.twig'
            )

        );
        if(isset($options["trackingAdvantage"])){
            $callbacks['draw_callback'] = array(
                'template' => 'AppBundle:Common:Datatable/draw_callback.js.twig',
                'vars' => array('id' => $options["trackingAdvantage"]->getId()),
            );
        }
        $this->callbacks->set($callbacks);
    }

    protected function initTopActions(array $options = [])
    {

        $this->topActions->set(
            [
                'start_html' => '<div class="row"><div class="col-sm-3">',
                'end_html' => '<hr></div></div>',
                'actions' => [
                    [
                        'route' => $this->router->generate('ads_create'),
                        'label' => $this->translator->trans('datatables.actions.new'),
                        'icon' => 'glyphicon glyphicon-plus',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.new'),
                            'class' => 'btn btn-primary',
                            'role' => 'button',
                        ],
                    ],
                ],
            ]
        );
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'buttons' =>
                    array(
                        'copy' => array('extend' => 'copy','text' => 'Copie','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        'excel' => array('extend' => 'excel','exportOptions' => array( 'columns' => array('12','13','14','7','17','15','8','9','19'))),
                        'print' => array('extend' => 'print','text' => 'Imprimer','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        'pdf' => array('extend' => 'pdf','exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                    ),
                'responsive' => true,
                'attr' => [
                    'class' => 'col-lg-1 col-centered'
                ],
            )
        ));
    }

    protected function initAjax(array $options = [])
    {
        $groupId = isset($options['group']) ? ($options['group'] instanceof \AppBundle\Entity\Group ? $options['group']->getRemoteId() : $options['group']) : 0;
        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_ads',['remoteId'=>$groupId,'data' => $data]),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE,
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Ad');

        $formatter = function($line) use ($repository){
            /** @var Ad $ad */
            $ad = $repository->find($line['id']);

            $line['ref'] = '<b>'.$ad->getReferenceV2() . '</b><br/><small>' . $ad->getLibelle().'</small>';
            $line['cp_ville'] = '<b>'.$ad->getVille() . '</b>&nbsp;<small>' . $ad->getCodePostal().'</small>';
            $line['date'] = '<b>'.$this->translator->trans('action.remonter',array(),'commiti').':</b> '.$ad->getUpdatedAt()->format('d/m/Y') . '<br/><b>'.$this->translator->trans('created_at',array(),'commiti').': </b>' . $ad->getCreatedAt()->format('d/m/Y');

            $type = '<div class="text-center">';
            switch($ad->getTypeDiffusion()) {
                default:case 'projet': $type .= 'Projet'; break;
                case 'terrain': $type .= 'Terrain seul'; break;
                case 'duo': $type .= 'Maison & Terrain + Terrain seul'; break;
            }

            switch($ad->getStatValue()) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange'; break;
                case 'suspendu': $class='fa fa-ban icon-red'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green'; break;
            }
            if($ad->getStatValue() == 'suspendu') {
                $type .= '<a href="'.$this->router->generate('ads_reaffect_housemodel',['remoteId' => $ad->getRemoteId()]).'" class="btn btn-icon"><i class="'.$class.'"></i></a>';
            } else {
                $type .= '<a href="javascript:void(0);" class="btn btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            }
            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER') && $ad->getTrackingPhone()){
                $sum = $this->em->getRepository("TrackingBundle:Statistique\TrackingCallStatistique")->getPhoneStats($ad->getTrackingPhone());
                $statistics = "<p>".$this->translator->trans('TrackingPhone',array(),'commiti')." ".
                    $ad->getTrackingPhone()."<br />".
                    ($sum > 1 ?
                        $this->translator->trans('Calls',array("%total%" => $sum),'commiti') :
                        $this->translator->trans('Call',array("%total%" => $sum),'commiti')
                    ).
                    "</p>";
                $type.=$statistics;
            }
            $line['type'] = $type;
            $line['price'] = number_format($line['prix'],2,',',' ').'€';

            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        $this->trackingAdvantage = (isset($options["trackingAdvantage"]) ? $options["trackingAdvantage"] : "");
        $this->group = (isset($options["group"]) ? $options["group"] : "");

        $this->columnBuilder
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'delete',
                        ],
                        'label' => $this->translator->trans('datatables.actions.delete', array(),'commiti'),
                        'icon' => 'fa fa-trash',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.delete', array(),'commiti'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return (
                            $this->securityToken->getToken()->getUser()->hasFullAccess()
                            );
                        }
                    ),
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'remonter',
                        ],
                        'label' =>   $this->translator->trans('datatables.actions.remonter',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-send',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.remonter',array(),'commiti'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-green',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'tracking',
                        ],
                        'label' => $this->translator->trans('Associer',array(),'commiti'),
                        'icon' => 'glyphicon pe-7s-call',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('Associer',array(),'commiti'),
                            'class' => 'btn btn-info btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return (
                                (
                                    $this->authorizationChecker->isGranted('ROLE_DIRECTOR') ||
                                    $this->authorizationChecker->isGranted('ROLE_MARKETING')
                                )&&
                                $this->trackingAdvantage
                            );
                        }
                    )
                )
            ))
            ->add('remoteId','column',['visible' => true, 'class' => 'hidden'])
            ->add('stat','column',['visible' => false])
            ->add('prix','column',['visible' => true, 'class' => 'hidden'])
            ->add('reference','column',['visible' => true, 'class' => 'hidden'])
            ->add('referenceV1','column',['visible' => true, 'class' => 'hidden'])
            ->add('libelle','column',['visible' => true, 'class' => 'hidden'])
            ->add('ref','virtual',['title' => $this->translator->trans('Réf. + Nom',array(),'commiti') ])
            ->add('cp_ville','virtual',['title' => $this->translator->trans('Ville + CP',array(),'commiti') ])
            ->add('price','virtual',['title' => $this->translator->trans('ads.ad.price',array(),'commiti')])
            ->add('codePostal', 'column',['visible' => true, 'class' => 'hidden'])
            ->add('ville', 'column',['visible' => true, 'class' => 'hidden']);
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder->add('group.parentGroup.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder
                ->add('group.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('ROLE_AGENCY',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('owner.username', 'column', array('title' => $this->translator->trans('Commercial',array(),'commiti') ));
        $this->columnBuilder->add('date','virtual',['title' => $this->translator->trans('Date',array(),'commiti')])
            ->add('imagePrincipale', 'image', array(
                'title' => 'Image',
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('type', 'virtual', array('title' => 'Type'))
            ->add('percent', 'progress_bar', array(
                'title'         => $this->translator->trans('Complete',array(),'commiti'),
                'render'        => 'render_progress_bar_extend',
                'label'         => true,
                'class'         => 'progress m-t-xs full',
                'value_min'     => '0',
                'value_max'     => '100',
                'multi_color'   => true
            ))
            ->add('description', 'column',['title'=> "Description",'visible' => true, 'class' => 'hidden'])
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'ads_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'ads_edit',
                            'route_parameters' => [
                                'remoteId'   => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-edit',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_remonter',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.remonter',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-blue',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU;
                            }
                        ],
                        [
                            'route' => 'diffusion_ads_project',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-send',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.diffuse',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-green',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_print_pdf',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'fa fa-file-pdf-o',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.export_pdf',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-rose',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_print',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'fa fa-print',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.print',array(),'commiti'),
                                'target' => '_blank',
                                'class' => 'btn btn-xs btn-without-label btn-blue-sky',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_duplicate',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-duplicate',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.duplicate',array(),'commiti'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-orange',
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] != Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_reaffect_housemodel',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'fa fa-ban',
                            'attributes' => [
                                'class' => 'btn btn-success btn-xs btn-without-label btn-red',
                                'title' => 'Le modèle n’est plus disponible'
                            ],
                            'render_if' => function($row) {
                                return $row['stat'] == Ad::SUSPENDU &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_delete',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete',array(),'commiti'),
                                'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                            ],

                            'render_if' => function($row) {
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ],
                        [
                            'route' => 'ads_affect_commercial',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'fa fa-flag',
                            'attributes' => [
                                'class' => 'btn btn-datatable-affect2 btn-without-label btn-default',
                                'data-target' => "#affectModal2",
                                'data-toggle'=>"modal"
                            ],
                            'render_if' => function($row) {
                                return $this->authorizationChecker->isGranted('ROLE_MARKETING') &&
                                $this->securityToken->getToken()->getUser()->hasFullAccess();
                            }
                        ]
                    ],
                ]
            );

    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Ad::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ad_datatable';
    }
}