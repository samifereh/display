<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\HouseModel;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class HouseModelDatatable
 *
 * @package AppBundle\Datatables
 */
class HouseModelDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('housemodel_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'buttons' =>
                    array(
                        'copy' => array(
                            'extend' => 'copy',
                            'text' => $this->translator->trans('datatables.actions.copy'),
                            'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))
                        ),
                        'excel' => array(
                            'extend' => 'excel',
                            'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))
                        ),
                        'print' => array(
                            'extend' => 'print',
                            'text' =>  $this->translator->trans('datatables.actions.print'),
                            'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        'pdf' => array(
                            'extend' => 'pdf',
                            'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                    ),
                'responsive' => true
            )
        ));

        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $groupId = isset($options['group']) &&
        $options['group'] != 'all' ? ($options['group'] instanceof \AppBundle\Entity\Group ?
            $options['group']->getId() : $options['group']) : 0;
        $this->ajax->set(array(
            'url' => $this->router->generate('api_housemodels', ['id' => $groupId, 'data' => $data]),
            'type' => 'GET'
        ));

        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/housemodel_moderation.js.twig'
            )
        ));

        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE,
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));
        $this->columnBuilder->add('remoteId','column',['visible' => true, 'class' => 'hidden']);
//        if($options['group'] == 'all') {
//            $this->columnBuilder->add('group.name','column',['title' => 'Agence']);
//        }
        $this->columnBuilder
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'delete',
                        ],
                        'label' =>  $this->translator->trans('datatables.actions.delete'),
                        'icon' => 'fa fa-trash',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' =>  $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    )
                )
            ))
            ->add('name', 'column', array('title' => 'Titre'));
        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
            $this->columnBuilder->add('group.parentGroup.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder->add('group.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Agence',array(),'commiti')));
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('owner.username', 'column', array('searchable'=>false,'title' => $this->translator->trans('Commercial',array(),'commiti') ));
        $this->columnBuilder->add('date','virtual',['title' => $this->translator->trans('Date',array(),'commiti') ])
            ->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('percent', 'progress_bar', array(
                'title'         => $this->translator->trans('Complete',array(),'commiti'),
                'render'        => 'render_progress_bar_extend',
                'label'         => true,
                'class'         => 'progress m-t-xs full',
                'value_min'     => '0',
                'value_max'     => '100',
                'multi_color'   => true
            ))
            ->add('stat','column',['title' => $this->translator->trans('Publiée',array(),'commiti')]);
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('visibleByAll','virtual',['title' => $this->translator->trans('visibility',array(),'commiti')]);
            $this->columnBuilder->add(null, 'action', array(
                'title' => 'Actions',
                'actions' => array(
                    array(
                        'route' => 'housemodel_show',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' =>  $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button',
                        ]
                    ),
                    array(
                        'route' => 'housemodel_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-pencil',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            'role' => 'button'

                        ]
                    ),
                    [
                        'route' => 'housemodel_print',
                        'route_parameters' => [
                            'remoteId' => 'remoteId'
                        ],
                        'label' => '',
                        'icon' => 'fa fa-print',
                        'attributes' => [
                            'class' => 'btn btn-xs btn-without-label btn-blue-sky',
                        ],
                    ],
                    [
                        'route' => 'housemodel_print_pdf',
                        'route_parameters' => [
                            'remoteId' => 'remoteId'
                        ],
                        'label' => '',
                        'icon' => 'fa fa-file-pdf-o',
                        'attributes' => [
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-rose',
                        ],
                    ],
                    array(
                        'route' => 'housemodel_duplicate',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-duplicate',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('duplicate'),
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-orange',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'housemodel_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "housemodel_deleted",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                        ),
                    )/*,
                    [
                        'route' => 'housemodel_affect_commercial',
                        'route_parameters' => [
                            'remoteId' => 'remoteId'
                        ],
                        'label' => '',
                        'icon' => 'fa fa-flag',
                        'attributes' => [
                            'class' => 'btn btn-datatable-affect2 btn-without-label btn-default',
                            'data-target' => "#affectModal",
                            'data-toggle'=>"modal"
                        ],
                        'render_if' => function($row) {
                            return $this->authorizationChecker->isGranted('ROLE_MARKETING');
                        }
                    ]*/
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:HouseModel');

        $formatter = function($line) use ($repository) {
            /** @var HouseModel $model */
            $model = $repository->find($line['id']);
            $line['date'] = '<b>'.$this->translator->trans('updated_at',array(),'commiti').':</b> '.$model->getUpdatedAt()->format('d/m/Y') . '<br/><b>'.$this->translator->trans('created_at',array(),'commiti').': </b>' . $model->getCreatedAt()->format('d/m/Y');

            switch($line['stat']) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green'; break;
            }

            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
                $line['stat'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
                $class = $model->getVisibleByAll() ? 'glyphicon glyphicon-eye-open icon-green' : 'glyphicon glyphicon-eye-close icon-red';
                $line['visibleByAll'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-visibility" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            } else {
                $line['stat'] = '<div class="btn-stat btn-icon btn-2x text-center"><i class="'.$class.'"></i></div></div>';
            }
            $line['informer'] = '<a href="javascript:void(0);" data-target="#notificationModal" data-toggle="modal"  
            class="btn btn-stat btn-icon btn-datatable-informer" data-id="'.$line['remoteId'].'">
            <i class="glyphicon glyphicon-send"></i></a>';
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return HouseModel::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'housemodel_datatable';
    }
}
