<?php

namespace AppBundle\Datatables\Gestion;

use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class ClientsDatatable
 *
 * @package AppBundle\Datatables
 */
class ClientsDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $formatter = function($line){
            $line['name'] = '<b>'. $line['prenom'] . ' '. $line['nom'] .'</b>';
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $groupId = isset($options['group']) ? ($options['group'] instanceof Group ? $options['group']->getRemoteId() : $options['group']) : null;
        $contactList = isset($options['contactList']) ? ($options['contactList'] instanceof ContactList ? $options['contactList']->getRemoteId() : $options['contactList']) : null;
        $this->ajax->set(array(
            'url' => $this->router->generate('api_client',['remoteId' => $groupId, 'contactList' => $contactList]),
            'type' => 'GET'
        ));

        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('prenom', 'column', array('visible' => false))
            ->add('nom', 'column', array('visible' => false))
            ->add('name', 'virtual', array( 'title' => $this->translator->trans('Nom',array(),'commiti')))
            ->add('telephone', 'column', array('title' => $this->translator->trans('Phone',array(),'commiti')))
            ->add('group.name', 'array', array(
                'title' => $this->translator->trans('Groupe',array(),'commiti'),
                'data' => 'group.name' // required option
            ))
            ->add(null, 'action', array(
                'title' => 'Actions',
                'actions' => array(
                    array(
                        'route' => 'gestion_client_view',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'gestion_client_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'gestion_client_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "L'utilisateur a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                        )
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Contact::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'client_datatable';
    }
}
