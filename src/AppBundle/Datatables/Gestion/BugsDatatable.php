<?php

namespace AppBundle\Datatables\Gestion;

use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class BugsDatatable
 *
 * @package AppBundle\Datatables
 */
class BugsDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $formatter = function($line){
            switch($line['status']) {
                default:case Bug::OPEN: $class='label-info';$text='Ouvert'; break;
                case Bug::PROGRESS: $class='label-warning';$text='En cours'; break;
                case Bug::RESOLVED: $class='label-success';$text='Résolu'; break;
            }
            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
                $line['stat'] = '<a href="javascript:void(0);" class="label '.$class.' btn-datatable-status" data-id="'.$line['remoteId'].'">'.$text.'</a>';
            } else {
                $line['stat'] = '<span class="label '.$class.'" >'.$text.'</span>';
            }


            switch($line['priority']) {
                default:case Bug::PRIORITY_MEDIUM: $class='label-warning';$text='Moyen'; break;
                case Bug::PRIORITY_HIGH: $class='label-danger';$text='Élevé'; break;
                case Bug::PRIORITY_LOW: $class='label-info';$text='bas'; break;
            }
            $line['level'] = '<span class="label '.$class.'" >'.$text.'</span>';

            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('gestion_bugs_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,

            'extensions' => array(
                'responsive' => true
            )
        ));

        $groupId = isset($options['group']) ? ($options['group'] instanceof Group ? $options['group']->getRemoteId() : $options['group']) : null;
        $this->ajax->set(array(
            'url' => $this->router->generate('api_bugs_list',['remoteId' => $groupId ]),
            'type' => 'GET'
        ));
        
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('status', 'column', array('visible' => false))
            ->add('priority', 'column', array('visible' => false))
            ->add('title', 'column', array('title' => $this->translator->trans('Nom',array(),'commiti')));

        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
            $this->columnBuilder->add('group.parentGroup.name', 'column', array('title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder->add('group.name', 'column', array('title' => 'Agence' ));
        $this->columnBuilder->add('stat', 'virtual', ['title' => $this->translator->trans('status',array(),'commiti')]);
        $this->columnBuilder->add('level', 'virtual', ['title' => $this->translator->trans('priority',array(),'commiti')]);

        $this->columnBuilder
        ->add(null, 'action', array(
                'title' => 'actions',
                'actions' => array(
                    array(
                        'route' => 'gestion_bugs_view',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'gestion_bugs_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'gestion_bugs_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_ADMIN'));
                        },
                        'attributes' => array(
                            'data-confirm-title' => "La demande a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                        )
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Bug::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'bugs_datatable';
    }
}
