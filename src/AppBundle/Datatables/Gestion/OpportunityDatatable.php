<?php

namespace AppBundle\Datatables\Gestion;

use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\DemandeContact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Repository\Gestion\AgendaEventsRepository;
use AppBundle\Repository\Gestion\OpportunityRepository;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class OpportunityDatatable
 *
 * @package AppBundle\Datatables
 */
class OpportunityDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        /** @var AgendaEventsRepository $repositoryEvents */
        $repositoryEvents = $this->em->getRepository('AppBundle:Gestion\\AgendaEvent');
        /** @var OpportunityRepository $repositoryOpportunity */
        $repositoryOpportunity = $this->em->getRepository('AppBundle:Gestion\\Opportunity');
        $formatter = function($line) use($repositoryOpportunity, $repositoryEvents){
            $opportunity = $repositoryOpportunity->findOneBy(['remoteId' => $line['remoteId']]);
            $line['contact_name'] = '<b>'. $line['prenom'] . ' '. $line['nom'] .'</b>';
            switch($line['status']) {
                default:case Opportunity::CLOSED: $class='label-danger';$text='Fermé'; break;
                case Opportunity::OPEN: $class='label-success';$text='Ouvert'; break;
                case Opportunity::WAITING: $class='label-warning';$text='En attente'; break;
            }
            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
                $line['stat'] = '<a href="javascript:void(0);" class="label '.$class.' btn-datatable-status" data-id="'.$line['remoteId'].'">'.$text.'</a>';
            } else {
                $line['stat'] = '<span class="label '.$class.'" >'.$text.'</span>';
            }
            $line['nbrMeetings'] = $repositoryEvents->getWaitingsEventsByOpportunity($opportunity);
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('gestion_opportunity_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));
        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $data = isset($options['data']) ? http_build_query($options['data']) : null;

        $this->ajax->set(array(
            'url' => $this->router->generate('api_opportunity',['data' => $data]),
            'type' => 'GET'
        ));

        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/opportunity.js.twig'
            )
        ));

        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('prenom', 'column', array('visible' => false))
            ->add('nom', 'column', array('visible' => false))
            ->add('status', 'column', array('visible' => false))
            ->add('title', 'column', ['title' => $this->translator->trans('opportunity.title',array(),'commiti')])
            ->add('ad.reference', 'column', ['title' => $this->translator->trans('ad.reference',array(),'commiti')])
            ->add('contact_name', 'virtual', ['title' => $this->translator->trans('opportunity.contact_name',array(),'commiti')])
            //->add('email', 'column', ['title' => 'Email'])
            ->add('telephone', 'column', ['title' => 'Telephone']);

            if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                $this->columnBuilder->add('opportunityGroup.parentGroup.name', 'column', array('title' => $this->translator->trans('Groupe',array(),'commiti') ));
            if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
                $this->columnBuilder->add('opportunityGroup.name', 'column', array('title' => 'Agence' ));
            $this->columnBuilder->add('stat', 'virtual', ['title' => $this->translator->trans('status',array(),'commiti')]);
            $this->columnBuilder->add('nbrMeetings', 'virtual', ['title' => $this->translator->trans('Rendez-vous',array(),'commiti')]);

        $this->columnBuilder->add('progress', 'progress_bar', array(
            'title'         => 'Complete',
            'render'        => 'render_progress_bar_extend',
            'label'         => true,
            'class'         => 'progress m-t-xs full',
            'value_min'     => '0',
            'value_max'     => '100',
            'multi_color'   => true
        ));
        $this->columnBuilder->add(null, 'action', array(
                'title' => 'Actions',
                'actions' => array(
                    array(
                        'route' => 'gestion_opportunity_view',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'gestion_opportunity_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'gestion_opportunity_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "L'opportunité a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                        )
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Opportunity::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'opportunity_datatable';
    }
}
