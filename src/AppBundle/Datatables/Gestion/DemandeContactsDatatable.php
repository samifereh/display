<?php

namespace AppBundle\Datatables\Gestion;

use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\DemandeContact;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class DemandeContactsDatatable
 *
 * @package AppBundle\Datatables
 */
class DemandeContactsDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $formatter = function($line){
            $line['contact_name'] = '<b>'. $line['contact']['prenom'] . ' '. $line['contact']['nom'] .'</b>';
            $line['commercial_name'] = '<b>'. $line['commercial']['name'] .'</b>';

            $ref = $line['ad'] ? $line['ad']['reference'] : $line['program']['reference'];
            $line['reference'] = '<b>'. $ref .'</b>';

            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        if(!isset($options['contact'])) {
            $this->topActions->set(array(
                'start_html' => '<div class="row"><div class="col-sm-3">',
                'end_html' => '<hr></div></div>',
                'actions' => array(
                    array(
                        'route' => $this->router->generate('gestion_demandes_contact_add'),
                        'label' => $this->translator->trans('datatables.actions.new'),
                        'icon' => 'glyphicon glyphicon-plus',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.new'),
                            'class' => 'btn btn-success',
                            'role' => 'button'
                        ),
                    )
                )
            ));
        }
        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));
        $groupId = isset($options['group']) ? ($options['group'] instanceof Group ? $options['group']->getRemoteId() : $options['group']) : null;
        $contact = isset($options['contact']) ? ($options['contact'] instanceof Contact ? $options['contact']->getRemoteId() : $options['contact']) : null;
        $this->ajax->set(array(
            'url' => $this->router->generate('api_demandes_contact',['remoteId' => $groupId, 'contact' => $contact]),
            'type' => 'GET'
        ));
        
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('contact.prenom', 'column', array('visible' => false))
            ->add('contact.nom', 'column', array('visible' => false))
            ->add('commercial.name', 'column', array('visible' => false))
            ->add('ad.reference', 'column', array('visible' => false))
            ->add('program.reference', 'column', array('visible' => false))
            ->add(
                'date','datetime',
                [
                    'title' => $this->translator->trans('date',array(),'commiti'),
                    'date_format' => 'DD-MM-YYYY'
                ]
            )
            ->add('reference', 'virtual', array( 'title' => $this->translator->trans('Référence',array(),'commiti')))
            ->add('contact_name', 'virtual', array( 'title' => $this->translator->trans('Contact',array(),'commiti')));
            if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
                $this->columnBuilder->add('group.parentGroup.name', 'column', array('title' => $this->translator->trans('Groupe',array(),'commiti') ));
            if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
                $this->columnBuilder
                    ->add('group.name', 'column', array('title' => 'Agence' ));
            $this->columnBuilder->add('commercial_name', 'virtual', array( 'title' => $this->translator->trans('Commercial',array(),'commiti')))
            ->add('contact.telephone', 'column', array( 'title' => $this->translator->trans('Téléphone',array(),'commiti')))
            ->add(null, 'action', array(
                'title' => 'Actions',
                'actions' => array(
                    array(
                        'route' => 'gestion_demandes_contact_view',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.show'),
                            'class' => 'btn btn-primary btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'gestion_demandes_contact_edit',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'render_if' => function() {
                            return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                        },
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => array(
                            'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                        ),
                    ),
                    array(
                        'route' => 'gestion_demandes_contact_delete',
                        'route_parameters' => array(
                            'remoteId' => 'remoteId'
                        ),
                        'label' => '',
                        'icon' => 'glyphicon glyphicon-trash',
                        'attributes' => array(
                            'data-confirm-title' => "La demande de contact a été supprimé",
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                        )
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return DemandeContact::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'demandes_contact_datatable';
    }
}
