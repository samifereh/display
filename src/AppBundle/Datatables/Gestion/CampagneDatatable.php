<?php

namespace AppBundle\Datatables\Gestion;

use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class CampagneDatatable
 *
 * @package AppBundle\Datatables
 */
class CampagneDatatable extends AbstractDatatableView
{

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $formatter = function($line){
            switch($line['status']) {
                default:case Campagne::DRAFT: $class='label-danger';$text='Brouillon'; break;
                case Campagne::PROGRAMMED: $class='label-warning';$text='En cours'; break;
                case Campagne::FINISHED: $class='label-success';$text='Complete'; break;
            }
            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER')) {
                $line['stat'] = '<a href="javascript:void(0);" class="label '.$class.' btn-datatable-status" data-id="'.$line['remoteId'].'">'.$text.'</a>';
            } else {
                $line['stat'] = '<span class="label '.$class.'" >'.$text.'</span>';
            }
            $line['statistique'] = '<span class="label '.$class.'" >'.$text.'</span>';
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->topActions->set(array(
            'start_html' => '<div class="row"><div class="col-sm-3">',
            'end_html' => '<hr></div></div>',
            'actions' => array(
                array(
                    'route' => $this->router->generate('gestion_campagnes_add'),
                    'label' => $this->translator->trans('datatables.actions.new'),
                    'icon' => 'glyphicon glyphicon-plus',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.new'),
                        'class' => 'btn btn-success',
                        'role' => 'button'
                    ),
                )
            )
        ));

        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array(
                'responsive' => true
            )
        ));

        $groupId = isset($options['group']) ? ($options['group'] instanceof Group ? $options['group']->getRemoteId() : $options['group']) : null;
        $this->ajax->set(array(
            'url' => $this->router->generate('api_compagne',['remoteId' => $groupId]),
            'type' => 'GET'
        ));
        
        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array('visible' => false))
            ->add('status', 'column', array('visible' => false));

        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
            $this->columnBuilder->add('group.parentGroup.name', 'column', array('title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder->add('group.name', 'column', array('title' => 'Agence' ));

        $this->columnBuilder
        ->add('title', 'column', array( 'title' => $this->translator->trans('Nom',array(),'commiti')))
        ->add('contactList.name', 'column', array('title' => $this->translator->trans('Liste de contact',array(),'commiti')))
        ->add(
            'date','datetime',
            [
                'title' => $this->translator->trans('Date de début',array(),'commiti'),
                'date_format' => 'DD-MM-YYYY HH:mm:ss'
            ]
        )
        ->add('stat', 'virtual', array( 'title' => $this->translator->trans('Status',array(),'commiti')))
        ->add('percent', 'progress_bar', array(
            'title'         => 'Complete',
            'render'        => 'render_progress_bar_extend',
            'label'         => true,
            'class'         => 'progress m-t-xs full',
            'value_min'     => '0',
            'value_max'     => '100',
            'multi_color'   => true
        ))
        ->add(null, 'action', array(
            'title' => 'Actions',
            'actions' => array(
                array(
                    'route' => 'gestion_campagnes_view',
                    'route_parameters' => array(
                        'remoteId' => 'remoteId'
                    ),
                    'label' => '',
                    'icon' => 'glyphicon glyphicon-eye-open',
                    'attributes' => array(
                        'rel' => 'tooltip',
                        'title' => $this->translator->trans('datatables.actions.show'),
                        'class' => 'btn btn-primary btn-xs btn-without-label',
                        'role' => 'button'
                    ),
                ),
                array(
                    'route' => 'gestion_campagnes_edit',
                    'route_parameters' => array(
                        'remoteId' => 'remoteId'
                    ),
                    'render_if' => function() {
                        return ($this->authorizationChecker->isGranted('ROLE_MARKETING'));
                    },
                    'label' => '',
                    'icon' => 'glyphicon glyphicon-edit',
                    'attributes' => array(
                        'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                    ),
                ),
                [
                    'route' => 'gestion_campagnes_duplicate',
                    'route_parameters' => [
                        'remoteId' => 'remoteId'
                    ],
                    'label' => '',
                    'icon' => 'glyphicon glyphicon-duplicate',
                    'attributes' => [
                        'rel' => 'tooltip',
                        'title' => 'Dupliquer',
                        'class' => 'btn btn-primary btn-xs btn-without-label btn-orange',
                    ]
                ],
                array(
                    'route' => 'gestion_campagnes_delete',
                    'route_parameters' => array(
                        'remoteId' => 'remoteId'
                    ),
                    'label' => '',
                    'icon' => 'glyphicon glyphicon-trash',
                    'attributes' => array(
                        'data-confirm-title' => "Le campagne a été supprimé",
                        'title' => $this->translator->trans('datatables.actions.delete'),
                        'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label',
                    )
                )
            )
        ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Campagne::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'campagne_datatable';
    }
}
