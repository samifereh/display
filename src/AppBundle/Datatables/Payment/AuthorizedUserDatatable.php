<?php

namespace AppBundle\Datatables\Payment;


use AppBundle\Entity\Payment\AuthorizedUser;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

class AuthorizedUserDatatable extends AbstractDatatableView
{
    public function buildDatatable(array $options = array())
    {
        $this->initTopActions($options);
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    public function initTopActions($options) {
        $group = $options['group'];
        if($group) {
            $this->topActions->set(
                [
                    'start_html' => '<div class="row"><div class="col-sm-3">',
                    'end_html' => '<hr></div></div>',
                    'actions' => [
                        [
                            'route' => $this->router->generate('autorization_create',['remoteId' => $group->getRemoteId()]),
                            'label' => $this->translator->trans('datatables.actions.new'),
                            'icon' => 'glyphicon glyphicon-plus',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.new'),
                                'class' => 'btn btn-primary',
                                'role' => 'button',
                            ],
                        ],
                    ],
                ]
            );
        }
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'buttons' =>
                        array(
                            'copy' => array('extend' => 'copy','exportOptions' => array( 'columns' => array('2','3','4','5','6','7','8', '9'))),
                            'excel' => array('extend' => 'excel','exportOptions' => array( 'columns' => array('2','3','4','5','6','7','8', '9'))),
                            'print' => array('extend' => 'print','exportOptions' => array( 'columns' => array('2','3','4','5','6','7','8', '9'))),
                            'pdf' => array('extend' => 'pdf','exportOptions' => array( 'columns' => array('2','3','4','5','6','7','8', '9'))),
                        ),
                    'responsive' => true
                )
            )
        );
    }

    public function initAjax($options) {
        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_authorized_users',['data' => $data]),
                'type' => 'GET',
            )
        );
    }

    public function initOptions($options) {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => true,
            )
        );
    }

    public function buildColumns($options) {
        $this->columnBuilder
            ->add('id', 'column', ['visible' => false])
            ->add('group.remoteId', 'column', ['visible' => false])
            ->add('role', 'column', [
                    'title' => $this->translator->trans('user.role',array(),'commiti')
                ])
            ->add(
                'startDate','datetime',
                [
                    'title' => $this->translator->trans('autorized_user.start_date',array(),'commiti'),
                    'date_format' => 'DD-MM-YYYY'
                ]
            )
            ->add(
                'endDate','datetime',
                [
                    'title' => $this->translator->trans('autorized_user.end_date',array(),'commiti'),
                    'date_format' => 'DD-MM-YYYY'
                ]
            )
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'autorization_delete',
                            'route_parameters' => [
                                'id' => 'id'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => 'Supprimer',
                                'class' => 'btn btn-danger btn-xs need_confirmation trigger-delete btn-without-label',
                            ],
                        ],
                    ],
                ]
            );
    }

    public function getEntity() {
        return AuthorizedUser::class;
    }

    public function getName() {
        return 'authorized_user_datatable';
    }
}