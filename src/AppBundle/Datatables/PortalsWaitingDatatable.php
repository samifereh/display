<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class PortalsWaitingDatatable
 * @package AppBundle\Datatables
 */
class PortalsWaitingDatatable extends AbstractDatatableView
{
    public function buildDatatable(array $options = array())
    {
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
    }

    public function initFeatures($options) {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => false,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'buttons' =>
                        array(
                            'copy' => array(
                                'extend' => 'copy',
                                'text' => $this->translator->trans('datatables.actions.copy'),
                                'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'excel' => array(
                                'extend' => 'excel',
                                'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'print' => array(
                                'extend' => 'print',
                                'text' => $this->translator->trans('datatables.actions.print'),
                                'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                            'pdf' => array('extend' => 'pdf',
                                'exportOptions' => array( 'columns' => array('1','2','3','4','5','6','7'))),
                        ),
                    'responsive' => true
                )
            )
        );
    }

    public function initAjax($options) {
        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_portals_waiting',['data' => $data]),
                'type' => 'GET',
            )
        );
    }

    public function initOptions($options) {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 10,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false
            )
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $portalRegistrationRepository        = $this->em->getRepository('AppBundle:Broadcast\\PortalRegistration');
        $formatter = function($line) use ($portalRegistrationRepository){
            /** @var Portal $portal */

            $portalRegistration = $portalRegistrationRepository->find($line['id']);

            $line['portal'] = $portalRegistration->getBroadcastPortal() ? $portalRegistration
                ->getBroadcastPortal()->getName() : '';
            $line['icon'] = $portalRegistration->getBroadcastPortal() ? "<img src='/".$portalRegistration
                    ->getBroadcastPortal()->getIcon()."' class='img-responsive' height='80px' />" : '';
            $status = "";
            switch ($portalRegistration->getStatus()){
                case PortalRegistration::STATUS_SENT_TO_COMMITI : $status = $this->translator
                    ->trans("portal.status_sent_to_commiti",array(),'commiti') ;break;
                case PortalRegistration::STATUS_WAITING_SUPPORT : $status = $this->translator
                    ->trans("portal.status_waiting_support",array(),'commiti') ;break;
            }
            $line['agency'] = $portalRegistration->getGroup()->getAgency() ? $portalRegistration
                ->getGroup()->getAgency()->getName() : $portalRegistration->getGroup()->getName();
            $line['status'] = $status;

            return $line;
        };

        return $formatter;
    }

    public function buildColumns($options) {
        $this->columnBuilder
            ->add('id','column',['visible' => false])
            ->add('portal','virtual',['visible' => false,
                'title' => $this->translator->trans('ad_mention_legals.portals_list',array(),'commiti')])
            ->add('icon', 'virtual', array( 'title' => 'Logo', 'width' => "50%" ))
            ->add('agency', 'virtual', [ 'title' => $this->translator->trans('Agence',array(),'commiti'), 'width' => "20%"])
            ->add('status', 'column', [
                'title' => $this->translator->trans('portal.status',array(),'commiti'),
                'class' => 'text-center',
                'width' => "20%"
            ])
            ->add(
                null,
                'action',
                [
                    'title' => "Actions",
                    'add_if' => function() {
                        return ($this->authorizationChecker->isGranted('ROLE_ADMIN'));
                    },
                    'actions' => [
                        [
                            'route' => 'portal_registration_edit',
                            'route_parameters' => [
                                'id' => 'id',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-edit',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                                'role' => 'button',
                            ],
                        ],
                        [
                            'route' => 'portal_registration_delete',
                            'route_parameters' => [
                                'id' => 'id'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete'),
                                'class' => 'btn btn-danger btn-xs need_confirmation trigger-delete btn-without-label',
                            ],
                        ],
                    ]
                ]
            );
    }

    public function getEntity() {
        return PortalRegistration::class;
    }

    public function getName() {
        return 'portals_waiting_datatable';
    }
}