<?php

namespace AppBundle\Datatables;

use AppBundle\Entity\HouseModel;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class HouseModelModerationDatatable
 *
 * @package AppBundle\Datatables
 */
class HouseModelModerationDatatable extends AbstractDatatableView
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->features->set(array(
            'auto_width' => true,
            'defer_render' => false,
            'info' => true,
            'jquery_ui' => false,
            'length_change' => true,
            'ordering' => true,
            'paging' => true,
            'processing' => true,
            'searching' => true,
            'state_save' => false,
            'delay' => 0,
            'extensions' => array()
        ));
        $groupId = isset($options['group']) ? ($options['group'] instanceof \AppBundle\Entity\Group ?
            $options['group']->getId() : $options['group']) : 0;
        $data    = isset($options['data']) ? http_build_query($options['data']) : null;

        $this->ajax->set(array(
            'url' => $this->router->generate('api_moderation_housemodels', ['id' => $groupId, 'data' => $data]),
            'type' => 'GET'
        ));

        $this->callbacks->set(array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/housemodel_moderation.js.twig'
            )
        ));


        $this->options->set(array(
            'display_start' => 0,
            'defer_loading' => -1,
            'dom' => 'lfrtip',
            'length_menu' => array(10, 25, 50, 100),
            'order_classes' => true,
            'order' => array(array(0, 'asc')),
            'order_multi' => true,
            'page_length' => 10,
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'class' => Style::BOOTSTRAP_3_STYLE,
            'use_integration_options' => true,
            'renderer' => '',
            'scroll_collapse' => false,
            'search_delay' => 0,
            'state_duration' => 7200,
            'stripe_classes' => array(),
            'individual_filtering' => false,
            'individual_filtering_position' => 'foot',
            'force_dom' => false
        ));

        $this->columnBuilder
            ->add('remoteId', 'column', array( 'visible' => false))
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'delete',
                        ],
                        'label' => $this->translator->trans('action.delete',array(),'commiti'),
                        'icon' => 'fa fa-trash',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' =>  $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'visible',
                        ],
                        'label' => $this->translator->trans('action.visible',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-eye-open',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' =>  $this->translator->trans('datatables.actions.all_visible'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-green',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'invisible',
                        ],
                        'label' => $this->translator->trans('action.invisible',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-eye-close',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.all_visible'),
                            'class' => 'btn btn-danger btn-xs btn-without-label',
                            'role' => 'button'
                        )
                    ),
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'activer',
                        ],
                        'label' => $this->translator->trans('action.activate',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-check',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-blue',
                            'role' => 'button'
                        ),
                    ),
                    array(
                        'route' => 'housemodel_actions',
                        'route_parameters' => [
                            'action' => 'desactiver',
                        ],
                        'label' => $this->translator->trans('action.desactiver',array(),'commiti'),
                        'icon' => 'glyphicon glyphicon-off',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('datatables.actions.delete'),
                            'class' => 'btn btn-danger btn-xs btn-without-label btn-orange',
                            'role' => 'button'
                        ),
                    )
                )
            ));
        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
            $this->columnBuilder->add('group.parentGroup.name', 'column', array('searchable'=>false,
                'title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder
                ->add('group.name', 'column', array('searchable'=>false,'title' => 'Agence' ));
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('owner.username', 'column', array('searchable'=>false,
                'title' => $this->translator->trans('Commercial',array(),'commiti') ));
        $this->columnBuilder->add('name', 'column', array('title' => 'name'))
            ->add('date','virtual',['title' => $this->translator->trans('Date',array(),'commiti')])
            ->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small'
            ))
            ->add('visibleByAll','virtual',['title' => $this->translator->trans('visibility',array(),'commiti')])
            ->add('stat','column',['title' => $this->translator->trans('published',array(),'commiti')])
            ->add('informer','virtual',['title' => $this->translator->trans('Informer',array(),'commiti')])
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'housemodel_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => $this->translator->trans('action.open',array(),'commiti'),
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-label btn-orange',
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show'),
                                'role' => 'button'
                            ]
                        ],
                        [
                            'route' => 'housemodel_edit',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => $this->translator->trans('datatables.actions.edit'),
                            'icon' => 'glyphicon glyphicon-edit',
                            'attributes' => array(
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit'),
                                'class' => 'btn btn-primary btn-xs btn-label btn-blue',
                                'role' => 'button'
                            )
                        ]/*,
                        [
                            'route' => 'housemodel_informer',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => $this->translator->trans('action.send',array(),'commiti'),
                            'icon' => 'glyphicon glyphicon-send',
                            'attributes' => [
                                'class' => 'btn btn-primary btn-xs btn-label btn-green',
                                'rel' => 'tooltip',
                                'title' => "Envoyer",
                                'role' => 'button'
                            ]
                        ]*/
                    ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:HouseModel');

        $formatter = function($line) use ($repository) {
            /** @var HouseModel $model */
            $model = $repository->find($line['id']);
            $line['date'] = '<b>'.$this->translator->trans('updated_at',array(),'commiti').'
            :</b> '.$model->getUpdatedAt()->format('d/m/Y') .
                '<br/><b>'.$this->translator->trans('created_at',array(),'commiti').': </b>' .
                $model->getCreatedAt()->format('d/m/Y');

            switch($line['stat']) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green'; break;
            }

            $line['informer'] = '<a href="javascript:void(0);" data-target="#notificationModal" data-toggle="modal"  
            class="btn btn-stat btn-icon btn-datatable-informer" data-id="'.$line['remoteId'].'">
            <i class="glyphicon glyphicon-send"></i></a>';

            $line['stat'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon btn-datatable-status" 
            data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            $class = $model->getVisibleByAll() ? 'glyphicon glyphicon-eye-open icon-green' :
                'glyphicon glyphicon-eye-close icon-red';

            $line['visibleByAll'] = '<a href="javascript:void(0);" class="btn btn-stat btn-icon 
            btn-datatable-visibility" data-id="'.$line['remoteId'].'"><i class="'.$class.'"></i></a>';
            return $line;
        };

        return $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return HouseModel::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'housemodel_moderation_datatable';
    }
}
