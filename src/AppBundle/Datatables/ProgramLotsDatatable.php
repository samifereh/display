<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:00
 */

namespace AppBundle\Datatables;

use AppBundle\Entity\Ad;
use Sg\DatatablesBundle\Datatable\View\AbstractDatatableView;
use Sg\DatatablesBundle\Datatable\View\Style;

/**
 * Class ProgramLotsDatatable
 *
 * @package AppBundle\Datatables
 */
class ProgramLotsDatatable extends AbstractDatatableView
{
    private $trackingAdvantage;
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->initTopActions($options);
        $this->initFeatures($options);
        $this->initAjax($options);
        $this->initOptions($options);
        $this->buildColumns($options);
        $this->callBacks($options);
    }

    protected function callBacks(array $options = []) {
        $callbacks = array(
            'draw_callback' => array(
                'template' => 'AppBundle:Common:Datatable/ads_program_moderation.js.twig'
            ),
        );

        if(isset($options["trackingAdvantage"])){
            $this->trackingAdvantage = $options["trackingAdvantage"];
            $callbacks['draw_callback'] = array(
                'template' => 'AppBundle:Common:Datatable/draw_callback.js.twig',
                'vars' => array('id' => $options["trackingAdvantage"]->getId()),
            );
        }

        $this->callbacks->set($callbacks);

    }

    protected function initTopActions(array $options = [])
    {
    }

    protected function initFeatures(array $options = [])
    {
        $this->features->set(
            array(
                'auto_width' => true,
                'defer_render' => false,
                'info' => true,
                'jquery_ui' => false,
                'length_change' => true,
                'ordering' => true,
                'paging' => true,
                'processing' => true,
                'searching' => true,
                'state_save' => false,
                'delay' => 0,
                'extensions' => array(
                    'responsive' => true
                )
            )
        );
    }

    protected function initAjax(array $options = [])
    {
        $data = isset($options['data']) ? http_build_query($options['data']) : null;
        $this->ajax->set(
            array(
                'url' => $this->router->generate('api_lots_list',['data' => $data]),
                'type' => 'GET',
            )
        );
    }

    protected function initOptions(array $options = [])
    {
        $this->options->set(
            array(
                'display_start' => 0,
                'defer_loading' => -1,
                'dom' => "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                'length_menu' => array(10, 25, 50, 100),
                'order_classes' => true,
                'order' => [
                    [0, 'asc']
                ],
                'order_multi' => true,
                'page_length' => 4,
                'paging_type' => Style::FULL_NUMBERS_PAGINATION,
                'class' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE . ' table-condensed'
                'use_integration_options' => true,
                'renderer' => '',
                'scroll_collapse' => false,
                'search_delay' => 0,
                'state_duration' => 7200,
                'stripe_classes' => [],
                'individual_filtering' => false,
                'individual_filtering_position' => 'foot',
                'force_dom' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getLineFormatter()
    {
        $repository = $this->em->getRepository('AppBundle:Ad');

        $formatter = function($line) use ($repository){
            /** @var Ad $ad */
            $ad = $repository->find($line['id']);

            $line['diffused'] = $ad->getDiffusion() ? 'Oui' : 'Non';
            $line['ref_program'] = '<b>'.$ad->getProgram()->getReference() . '</b><br/><small>' . $ad->getProgram()->getTitle().'</small>';
            $line['ref_lot'] = '<b>'.$ad->getReference() . '</b><br/><small>' . $ad->getLibelle().'</small>';
            $line['date'] = '<b>Remontée:</b> '.$ad->getUpdatedAt()->format('d/m/Y') . '<br/><b>Créée le: </b>' . $ad->getCreatedAt()->format('d/m/Y');

            $line['prix'] = number_format($line['prix'],2,',',' ').' €';
            switch($ad->getStatValue()) {
                default:case 'en-attente': $class='fa fa-clock-o icon-orange';$status = 'En attente'; break;
                case 'annuler': $class='glyphicon glyphicon-remove-circle icon-red';$status = 'Annuler'; break;
                case 'valider': $class='glyphicon glyphicon-ok-circle icon-green';$status = 'Valider'; break;
                case 'reserver': $class='glyphicon glyphicon-lock icon-blue';$status = 'Réserver'; break;
                case 'vendu': $class='glyphicon glyphicon-euro icon-purple';$status = 'Vendu'; break;
            }

            $line['stat'] = '<a href="javascript:void(0);" class="btn btn-icon btn-datatable-status" data-id="'.$line['remoteId'].'">
            <i class="'.$class.'"></i><p>'.$status.'</p>
            </a>';


            if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER') && $ad->getTrackingPhone()){
                $sum = $this->em->getRepository("TrackingBundle:Statistique\TrackingCallStatistique")->getPhoneStats($ad->getTrackingPhone());
                $statistics = "<p>".$this->translator->trans('TrackingPhone',array(),'commiti')." ".
                    $ad->getTrackingPhone()."<br />".
                    ($sum > 1 ?
                        $this->translator->trans('Calls',array("%total%" => $sum),'commiti') :
                        $this->translator->trans('Call',array("%total%" => $sum),'commiti')
                    ).
                    "</p>";
                $line['stat'].=$statistics;
            }
            return $line;
        };

        return $formatter;
    }

    protected function buildColumns(array $options = [])
    {
        if(isset($options["trackingAdvantage"])) {
            $this->trackingAdvantage = $options["trackingAdvantage"];
        }
        $this->columnBuilder
            ->add(null, 'multiselect', array(
                'start_html' => '<div class="thin">',
                'end_html' => '</div>',
                'attributes' => array(
                    'class' => 'i-checks'
                ),
                'actions' => array(
                    array(
                        'route' => 'ads_actions',
                        'route_parameters' => [
                            'action' => 'tracking-lot'
                        ],
                        'label' => 'Associer',
                        'icon' => 'glyphicon pe-7s-call',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('Associer',array(),'commiti'),
                            'class' => 'btn btn-info btn-xs btn-without-label',
                            'role' => 'button'
                        ),
                        'render_if' => function($row) {
                            return (
                            (
                                $this->authorizationChecker->isGranted('ROLE_DIRECTOR') ||
                                $this->authorizationChecker->isGranted('ROLE_MARKETING')
                            )&&
                                $this->trackingAdvantage
                            );
                        }
                    )
                )
            ))
            ->add('remoteId','column',['visible' => false])
            ->add('ref_program','virtual',['title' => $this->translator->trans('Réf. Programme',array(),'commiti')])
            ->add('ref_lot','virtual',['title' => $this->translator->trans('Réf. Lot',array(),'commiti')])
            ->add('prix','column',['title' => $this->translator->trans('ads.ad.price',array(),'commiti')])
            ->add('imagePrincipale', 'image', array(
                'title' => $this->translator->trans('Image',array(),'commiti'),
                'relative_path' => '',
                'imagine_filter' => 'datatable_small',
            ))
            ->add('codePostal', 'column', ['title' => $this->translator->trans('ads.ad.zipcode',array(),'commiti'), 'name' => "codePostal" ])
            ->add('ville', 'column', array('title' => $this->translator->trans('ads.ad.city',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_ADMIN'))
            $this->columnBuilder->add('program.group.parentGroup.name', 'column', array('searchable'=>false,'title' => $this->translator->trans('Groupe',array(),'commiti') ));
        if($this->authorizationChecker->isGranted('ROLE_MARKETING'))
            $this->columnBuilder
                ->add('program.group.name', 'column', array('searchable'=>false,'title' => 'Agence' ));
        if($this->authorizationChecker->isGranted('ROLE_BRANDLEADER'))
            $this->columnBuilder->add('owner.username', 'column', array('title' => $this->translator->trans('Commercial',array(),'commiti') ));
        $this->columnBuilder->add('date','virtual',['title' => $this->translator->trans('Date',array(),'commiti')])
            ->add('stat','virtual',['title' =>  $this->translator->trans('ads.ad.status',array(),'commiti'), 'class' => "text-center"])
            ->add('percent', 'progress_bar', array(
                'title'         => 'Complete',
                'render'        => 'render_progress_bar_extend',
                'label'         => true,
                'class'         => 'progress m-t-xs full',
                'value_min'     => '0',
                'value_max'     => '100',
                'multi_color'   => true
            ))
            ->add(
                null,
                'action',
                [
                    'title' => 'Actions',
                    'actions' => [
                        [
                            'route' => 'ads_show',
                            'route_parameters' => [
                                'remoteId' => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-eye-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.show'),
                                'class' => 'btn btn-primary btn-xs btn-without-label',
                                'role' => 'button',
                            ]
                        ],
                        [
                            'route' => 'ads_edit_program',
                            'route_parameters' => [
                                'remoteId'   => 'remoteId',
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-pencil',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.edit'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-purple',
                            ]
                        ],
                        [
                            'route' => 'diffusion_ads_project',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-send',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.diffuse'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-green',
                            ],
                        ],
                        [
                            'route' => 'ads_remonter',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-open',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.remonter'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-blue',
                            ],
                        ],
                        [
                            'route' => 'ads_print',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'fa fa-file-pdf-o',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => 'Export PDF',
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-rose',
                            ],
                        ],
                        [
                            'route' => 'ads_duplicate',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-duplicate',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.duplicate'),
                                'class' => 'btn btn-primary btn-xs btn-without-label btn-orange',
                            ],
                        ],
                        [
                            'route' => 'ads_delete',
                            'route_parameters' => [
                                'remoteId' => 'remoteId'
                            ],
                            'label' => '',
                            'icon' => 'glyphicon glyphicon-trash',
                            'attributes' => [
                                'rel' => 'tooltip',
                                'title' => $this->translator->trans('datatables.actions.delete'),
                                'class' => 'btn btn-danger btn-xs need_confirmation btn-without-label trigger-delete',
                            ]
                        ]
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return Ad::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lots_datatable';
    }
}