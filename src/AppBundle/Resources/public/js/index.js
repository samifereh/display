$(function() {
    aide_navigation.init();
    tabs.init();
    divers.init();
});

var divers = function () {
    function _init() {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[data-toggle="tab"]').removeClass('btn-primary');
            $('a[data-toggle="tab"]').addClass('btn-default');
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
        });

        $('.tab-next').click(function(){
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href="#'+nextId+'"]').tab('show');
        });

        $('.tab-prev').click(function(){
            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href="#'+prevId+'"]').tab('show');
        });

        $('.changeAvatar').click(function(){
            $(this).parent().find('input[type="file"]').click();
        });

        $('.select2').select2();
        $('.select2-once').select2({maximumSelectionLength: 1});


        $(document).on('click', '#add-another-employee', function(e){
            var $container = $('#group_agency_users');
            var index = (typeof $container.data('index') === 'undefined') ? 1 : $container.data('index');
            var prototype = $container.data('prototype');
            var html      = prototype.replace(/__employees__/g, index);

            var $item = $(html);

            $container.data('index', index + 1);
            $container.append($item);

            $item.find('.select2-once').each(function (index, value) {
                $(value).select2({maximumSelectionLength: 1});
            });
        });

        $(document).on('click', '.need_confirmation', function(e){

            var element = $(this);
            e.preventDefault();

            var text = "Vous ne pourrez plus revenir en arrière";
            var customTextElement = $("#delete_user_confirm");
            if(element.hasClass("user-delete-confirm") && customTextElement.size()>0)
                text = customTextElement.html();

            swal({
                    title: "Etes-vous sûr ?",
                    text: text,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, je suis sûr",
                    cancelButtonText: "Non, annuler",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                function (isConfirm) {
                    if (isConfirm) {
                        var titleSuccess = (element.data('confirm-title') != undefined) ? element.data('confirm-title') : "Fait!";
                        var msgSuccess = (element.data('confirm-msg') != undefined) ? element.data('confirm-msg') : "";
                        swal({ title: titleSuccess, text: msgSuccess, type: "success"});
                        if(element.data('href') != undefined)
                            document.location  = element.data('href');
                        else if(element.attr('href') != undefined)
                            document.location  = element.attr('href');
                    } else {
                        var titleCancel = (element.data('cancel-title') != undefined) ? element.data('cancel-title') : "Annulé !";
                        var msgCancel = (element.data('cancel-msg') != undefined) ? element.data('cancel-msg') : "";
                        swal({ title: titleCancel, text: msgCancel, type: "error"});
                    }
                });

        });

        $('.datepicker').datepicker({
            language: 'fr'
        });
        $('.datetimepicker').datetimepicker({format: 'DD/MM/YYYY HH:mm'});

        var cookie = $("#alert-cookie");
        if (cookie.length) {
            if (!$.cookie('accept_cookie')) {
                $("#alert-cookie .accept_cookies").click(function() {
                    var date = new Date();
                    date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
                    $.cookie('accept_cookie', '1', { expires: date, path: '/' });
                    $('#alert-cookie').slideUp(500);
                });
            } else{
                cookie.remove();
            }
        }

        $('.textarea-editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                ['textsize', ['fontsize']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']]
            ]
        });

        $('.textarea-editor-full').summernote();

        $(".surface").TouchSpin({
            min: 0,
            max: 99999999999999999999,
            step: 0.05,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            prefix: 'm²'
        });

        $(".surface_number, .si_number").TouchSpin({
            min: 0,
            max: 99999999999999999999,
            step: 1,
            decimals: 0,
            boostat: 5,
            maxboostedstep: 10
        });

        $('.surface_container .condition :radio,.surface_number_container .condition :radio,.si_number_container .condition :radio').on('change',function () {
            if($(this).val() === '1') {
                $(this).closest('.row').removeClass('inactive');
            } else {
                $(this).closest('.row').addClass('inactive');
            }
        });
        $('.si_number_container .condition input:checked').each(function() {
            if ($(this).val() === '1') {
                $(this).closest('.row').removeClass('inactive');
            }
        });
        $('.surface_container .condition input:checked').each(function() {
            if ($(this).val() === '1') {
                $(this).closest('.row').removeClass('inactive');
            }
        });
        $('.surface_number_container .condition input:checked').each(function() {
            if ($(this).val() === '1') {
                $(this).closest('.row').removeClass('inactive');
            }
        });
        $(":file").filestyle();
        $('#commercials').on('change',function(){
            var date = new Date();
            date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
            $.cookie('write_as_commercial', $(this).val(), { expires: date, path: '/' });
        });
        if($("#commercialForm").length) {
            $("#commercialForm").validate({
                rules: {
                    commercials: {
                        required: true,
                        maxlength: 32
                    },
                    agences: {
                        required: true,
                        maxlength: 32
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                },
                errorPlacement: function(error, element) {
                    $( element ).closest( "form" ).addClass('has-error');
                    $( element ).closest( "form" ).find('.help-block').html( error );
                }
            });
        }

        var date = new Date();
        date.setTime(date.getTime() + (24 * 60 * 60 * 1000));

        if($('#groups').length) {
            //if(!$.cookie('use_group')) { $.cookie('use_group', $("#groups option").first().val(), { expires: date, path: '/' }); }
            $('#groups').on('change',function(){
                if($(this).val() == '')
                    $.removeCookie('use_group',{ path: '/' });
                else
                    $.cookie('use_group', $(this).val(), { expires: date, path: '/' });
                $.getJSON(
                    Routing.generate('switch_commercial'),
                    {'parentGroupId':$(this).val()},
                    function (data) {
                        $("#agences").html('');
                        $.each(data.agences,function (index, item) {
                            $("#agences").append('<option value="'+item.id+'">'+item.text+'</option>')
                        });
                        $("#agences option").first().change();
                    }
                );
            });
        }
        if($('#agences').length) {
            if(!$.cookie('use_agency')) { $.cookie('use_agency', $("#agences option").first().val(), { expires: date, path: '/' }); }
            $('#agences').on('change',function(){
                $.cookie('use_agency', $(this).val(), { expires: date, path: '/' });
                $('.choose_model .modal-body').html('<div class="loading"></div>');
                $.getJSON(
                    Routing.generate('switch_commercial'),
                    {'groupId' : $(this).val()},
                    function (data) {
                        $("#commercials").html('');
                        $.each(data.commercials,function (index, item) {
                            $("#commercials").append('<option value="'+item.id+'">'+item.text+'</option>')
                        });
                        $("#commercials option").first().change();
                        $('#choose_duo_model .modal-body').html($(data.houseModels.duo).find('.modal-body').detach());
                        $('#choose_projet_model .modal-body').html($(data.houseModels.projet).find('.modal-body').detach());
                    }
                );
            });
        }

        /*if(!$.cookie('use_group')) {
            $.cookie('use_group', $("#group_list li").first().find('a').data('id'), { expires: date, path: '/' });
        }
        if($.cookie('use_group') && !$.cookie('use_agency')) {
            $.cookie('use_agency', $("#agencies_list li").first().find('a').data('id'), { expires: date, path: '/' });
        }
        if($.cookie('use_agency') && !$.cookie('write_as_commercial')) {
            $.cookie('write_as_commercial', $(".commercials li").first().find('a').data('id'), { expires: date, path: '/' });
        }*/
        $('.commercials a').on('click',function(){
            setCommercialCookie($(this).data('id'));
        });

        $('#filter_submit').on('click',function(){
            setCommercialCookie($("#filter_commercials_list").val());
        });


        function setCommercialCookie(value){
            var date = new Date();
            date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
            $.cookie('write_as_commercial', value, { expires: date, path: '/' });
            location.reload();
        }


        $('#agencies_list a').on('click',function(){
            setAgencyCookie($(this).data('id'));
        });

        $('#filter_agencies_list').on("change", function (value) {
            setAgencyCookie($(this).val());
        });


        function setAgencyCookie(value){
            var date = new Date();
            date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
            $.cookie('use_agency', value, { expires: date, path: '/' });
            $.removeCookie('write_as_commercial',{ path: '/' });
            location.reload();
        }
        $('#group_list a').on('click',function(){
            setGroupCookie($(this).data('id'));
        });

        $('#filter_group_list').on("change", function (value) {
            setGroupCookie($(this).val());
        });

        function setGroupCookie(value){
            if(value == '') {
                $.removeCookie('use_group',{ path: '/' });
            } else {
                $.cookie('use_group', value, { expires: date, path: '/' });
            }
            $.removeCookie('use_agency',{ path: '/' });
            $.removeCookie('write_as_commercial',{ path: '/' });
            location.reload();
        }



        postalCode.init();

        $(".card-expire").datepicker( {
            format: "mm/yyyy",
            viewMode: "months",
            minViewMode: "months"
        });

        $("span.pie").peity("pie", {
            fill: ["#62cb31", "#edf0f5"]
        });

        $('.color_picker').minicolors({
            control: 'wheel',
            inline: false,
            letterCase: 'lowercase',
            opacity: false,
            theme: 'bootstrap'
        });

        $('[data-plugin-slider]').each(function() {
            var $this = $( this ),
                opts = {};

            var pluginOptions = $this.data('plugin-options');
            if (pluginOptions) {
                opts = pluginOptions;
            }

            $this.themePluginSlider(opts);
        });

        if($('.summernote-textarea').length) {
            $(".summernote-textarea").summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
                    ['textsize', ['fontsize']],
                    ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']]
                ],
                onChange: function(value) {
                    convertText('#descriptionReturn', value);
                }
            });

            $('#ad_rewite_title').on('keyup',function(){
                convertText('#titleReturn', $(this).val());
            });
        }

    }
    return {init: _init}
}();

var convertText = function(destination, value) {
    var _vars = {
        '[VILLE]':              'Saint-Etienne',
        '[ENSEIGNE]' :          '{{ group.name }}',
        '[SURFACE MAISON]' :    '115',
        '[SURFACE TERRAIN]' :   '850',
        '[REFERENCE ANNONCE]' : 'PRI_785796'
    };


    for (var val in _vars) {
        value = value.split(val).join(_vars[val]);
    }

    var pattern = /\{(.*?)\}/g;
    var match;
    while ((match = pattern.exec(value)) != null)
    {
        var items = match[1].split('~');
        var item = items[Math.floor(Math.random()*items.length)];
        value = value.replace(match[0],item);
    }
    $(destination).html(value);
}

var fileRender = function() {
    function _formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }

    function _formatBitrate(bits) {
        if (typeof bits !== 'number') {
            return '';
        }
        if (bits >= 1000000000) {
            return (bits / 1000000000).toFixed(2) + ' Gbit/s';
        }
        if (bits >= 1000000) {
            return (bits / 1000000).toFixed(2) + ' Mbit/s';
        }
        if (bits >= 1000) {
            return (bits / 1000).toFixed(2) + ' kbit/s';
        }
        return bits.toFixed(2) + ' bit/s';
    }

    function _formatTime (seconds) {
        var date = new Date(seconds * 1000),
            days = Math.floor(seconds / 86400);
        days = days ? days + 'd ' : '';
        return days +
            ('0' + date.getUTCHours()).slice(-2) + ':' +
            ('0' + date.getUTCMinutes()).slice(-2) + ':' +
            ('0' + date.getUTCSeconds()).slice(-2);
    }

    function _formatPercentage (floatValue) {
        return (floatValue * 100).toFixed(2) + ' %';
    }

    function _renderExtendedProgress(data) {
        return _formatBitrate(data.bitrate) + ' | ' +
            _formatTime(
                (data.total - data.loaded) * 8 / data.bitrate
            ) + ' | ' +
            _formatPercentage(
                data.loaded / data.total
            ) + ' | ' +
            _formatFileSize(data.loaded) + ' / ' +
            _formatFileSize(data.total);
    }

    return {renderExtendedProgress: _renderExtendedProgress}
}();

/* =AIDE_NAVIGATION*/
var aide_navigation = function(){
    function _init() {
        // permet d'afficher les liens d'évitement si une navigation au clavier est détectée (focus sur un lien hors de vue).
        var evitement = $('#aide_navigation');
        $('a',evitement).one('focus',function(){
            evitement.addClass('visible');
        })
    }
    return {init:_init}
}();

/* =AIDE_NAVIGATION*/
var postalCode = function(){
    function _init() {
        $.typeahead({
            input: '.city-list',
            order: "desc",
            minLength: 3,
            dynamic: true,
            delay: 500,
            template: function (query, item) {
                return '<span>({{text}})</span>';
            },
            source: {
                display: "text",
                data: function () {
                    var deferred = $.Deferred(),
                        query = this.query;
                    $.getJSON(
                        Routing.generate('api_search_cities'),{ q: query },
                        function (data) {
                            deferred.resolve(data)
                        }
                    );
                    return deferred;
                }
            },
            callback: {
                onClick: function (node, a, item, event) {
                    $('.cp').val(item.cp);
                },
                onClickAfter: function (node, a, item, event) {
                    $('.city-list').val(item.name);
                }
            }

        });

        $.typeahead({
            input: '.city-list-2',
            order: "desc",
            minLength: 3,
            dynamic: true,
            delay: 500,
            template: function (query, item) {
                return '<span>({{text}})</span>';
            },
            source: {
                display: "text",
                data: function () {
                    var deferred = $.Deferred(),
                        query = this.query;
                    $.getJSON(
                        Routing.generate('api_search_cities'),{ q: query },
                        function (data) {
                            deferred.resolve(data)
                        }
                    );
                    return deferred;
                }
            },
            callback: {
                onClick: function (node, a, item, event) {
                    $('.cp-2').val(item.cp);
                },
                onClickAfter: function (node, a, item, event) {
                    $('.city-list-2').val(item.name);
                }
            }

        });
    }
    return {init:_init}
}();


/* =AIDE_NAVIGATION*/
var tabs = function(){
    function _init() {

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('a[data-toggle="tab"]').removeClass('btn-primary');
            $('a[data-toggle="tab"]').addClass('btn-default');
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
        });

        if(window.location.hash) { $('[href="'+window.location.hash+'"]').tab('show'); }

        $('.next').click(function(){
            var nextId = $(this).parents('.tab-pane').next().attr("id");
            $('[href="#'+nextId+'"]').tab('show');
        });

        $('.prev').click(function(){
            var prevId = $(this).parents('.tab-pane').prev().attr("id");
            $('[href="#'+prevId+'"]').tab('show');
        });
    }
    return {init:_init}
}();


var showResults = function(){
    function _form(formID) {
        if($('#table-review').length) {
            $('#table-review').find('tr').remove();
            $('#'+formID).find('tr').each(function() {
                var td   = $( this ).find('td:eq(1)');
                var label = '<b>'+$( this ).find('td:eq(0) label').text()+'</b>';
                if(td.find('input[type="text"]').length && td.find('input[type="text"]').val()) {
                    $('#table-review').append('<tr><td>'+label+'</td><td>'+td.find('input[type="text"]').val()+'</td></tr>');
                } else if(td.find('select.select2').length && $(td.find('select.select2')).select2('data')[0].text) {
                    $('#table-review').append('<tr><td>'+label+'</td><td>'+$(td.find('select.select2')).select2('data')[0].text+'</td></tr>');
                } else if(td.find('.surface').length && td.find('.surface').val()) {
                    $('#table-review').append('<tr><td>'+label+'</td><td>'+td.find('.surface').val()+'m²</td></tr>');
                } else if(td.find('input[type="radio"]').length && td.find('input[type="radio"]:checked').val()) {
                    $('#table-review').append('<tr><td>'+label+'</td><td>'+(td.find('input[type="radio"]:checked').val() == '1' ? 'Oui' : 'Non' )+'</td></tr>');
                } else if(td.find('input[type="checkbox"]').length) {
                    $('#table-review').append('<tr><td>'+label+'</td><td>'+(td.find('input[type="checkbox"]').prop( "checked") ? 'Oui' : 'Non' )+'</td></tr>');
                }
            });
        }
    }

    function _terrain() {
        if($('#terrain-table-review').length) {
            $('#terrain-table-review').find('tr').remove();
            $('#terrain-table').find('tr').each(function() {
                var td   = $( this ).find('td:eq(1)');
                var label = '<b>'+$( this ).find('td:eq(0) label').text()+'</b>';
                if(td.find('input[type="text"]').length && td.find('input[type="text"]').val()) {
                    $('#terrain-table-review').append('<tr><td>'+label+'</td><td>'+td.find('input[type="text"]').val()+'</td></tr>');
                } else if(td.find('select.select2').length && $(td.find('select.select2')).select2('data')[0].text) {
                    $('#terrain-table-review').append('<tr><td>'+label+'</td><td>'+$(td.find('select.select2')).select2('data')[0].text+'</td></tr>');
                } else if(td.find('.surface').length && td.find('.surface').val()) {
                    $('#terrain-table-review').append('<tr><td>'+label+'</td><td>'+td.find('.surface').val()+'m²</td></tr>');
                } else if(td.find('input[type="radio"]').length && td.find('input[type="radio"]:checked').val()) {
                    $('#terrain-table-review').append('<tr><td>'+label+'</td><td>'+(td.find('input[type="radio"]:checked').val() == '1' ? 'Oui' : 'Non' )+'</td></tr>');
                }
            });
        }
    }

    function _annonce() {
        if($('#annonce-table-review').length) {
            $('#annonce-table-review').find('tr').remove();
            $('#annonce-table').find('tr').each(function() {
                var td   = $( this ).find('td:eq(1)');
                var label = '<b>'+$( this ).find('td:eq(0) label').text()+'</b>';
                if(td.find('input[type="text"]').length && td.find('input[type="text"]').val()) {
                    $('#annonce-table-review').append('<tr><td>'+label+'</td><td>'+td.find('input[type="text"]').val()+'</td></tr>');
                } else if(td.find('select.select2').length && $(td.find('select.select2')).select2('data')[0].text) {
                    $('#annonce-table-review').append('<tr><td>'+label+'</td><td>'+$(td.find('select.select2')).select2('data')[0].text+'</td></tr>');
                } else if (td.find('textarea').length) {
                    $('#annonce-table-review').append('<tr><td>'+label+'</td><td>'+$(td.find('textarea')).val()+'</td></tr>');
                }else if(td.find('img.filestyle').length) {
                    $('#annonce-table-review').append('<tr><td>'+label+'</td><td><img class="col-sm-6" src="'+td.find('img.filestyle').attr('src')+'" /></td></tr>');
                }
            });
        }
    }

    function _maison(fromId) {
        fromId = fromId||'#maison-table';
        if($('#maison-table-review').length) {
            $('#maison-table-review').find('tr').remove();
            $(fromId).find('tr').each(function() {
                var td   = $( this ).find('td:eq(1)');
                var label = '<b>'+$( this ).find('td:eq(0) label').text()+'</b>';
                if(td.find('.surface').length && td.find('.surface').val()) {
                    $('#maison-table-review').append('<tr><td>'+label+'</td><td>'+td.find('.surface').val()+'m²</td></tr>');
                } else if(td.find('img.filestyle').length) {
                    $('#maison-table-review').append('<tr><td>'+label+'</td><td><img class="col-sm-6" src="'+td.find('img.filestyle').attr('src')+'" /></td></tr>');
                } else if(td.find('.surface_number').length && td.find('.surface_number').val()) {
                    $('#maison-table-review').append('<tr><td>'+label+'</td><td>'+td.find('.surface_number').val()+'</td></tr>');
                } else if(td.find('.select2').length && $('#'+td.find('.select2').attr('id')).select2('data')[0].text) {
                    $('#maison-table-review').append('<tr><td>'+label+'</td><td>'+$('#'+td.find('.select2').attr('id')).select2('data')[0].text+'</td></tr>');
                } else if(td.find('.si_number_container').length && td.find('.si_number_container .condition input:checked').val()) {
                    if(td.find('.si_number_container .condition input:checked').val() === '1') {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Oui<br/>Nombre: '+td.find('.si_number_container .si_number').val()+'</td></tr>');
                    } else {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Non</td></tr>');
                    }
                } else if(td.find('.surface_container').length && td.find('.surface_container .surface').val()) {
                    if(td.find('.surface_container .condition input:checked').val() === '1') {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Oui<br/>Surface: '+td.find('.surface_container .surface').val()+'</td></tr>');
                    } else {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Non</td></tr>');
                    }
                } else if(td.find('.surface_number_container').length && td.find('.surface_number_container .si_number').val()) {
                    if(td.find('.surface_number_container .condition input:checked').val() === '1') {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Oui<br/>Nombre: '+td.find('.surface_number_container .si_number').val()+'<br/>'+td.find('.surface_number_container .surface').val()+'</td></tr>');
                    } else {
                        $('#maison-table-review').append('<tr><td>'+label+'</td><td>Non</td></tr>');
                    }
                }
            });
        }
    }


    return {
        terrain: _terrain,
        annonce: _annonce,
        maison: _maison,
        form: _form
    }
}();


function render_progress_bar_extend(data, type, row, meta, barClasses, min, max, label, multi) {
    if (!data) {
        return '';
    }

    var valueNow = 'aria-valuenow="' + data + '"';
    var valueMin = 'aria-valuemin="' + min + '"';
    var valueMax = 'aria-valuemax="' + max + '"';
    var intData = parseInt(data, 10);
    var size = (intData-min)*100/(max-min);
    var classNames = 'class="progress-bar ';

    if (true == multi) {
        if (size <= 35) {
            classNames += 'progress-bar-danger' + '"';
        }
        if (size > 35 && size <= 70) {
            classNames += 'progress-bar-warning' + '"';
        }
        if (size > 70) {
            classNames += 'progress-bar-success' + '"';
        }
    } else {
        classNames += barClasses + '"';
    }

    var style = 'style="width:' + size +  '%; color: black;"';
    var result = '<div class="progress">';
    result += '<div ' + classNames;
    result += 'role="progressbar"' + valueNow + ' ' + valueMin + ' ' + valueMax + ' ' + style + '>';
    if (true == label) {
        result += data+'%';
    }
    result += '</div></div>';

    return result;
}

// Slider
(function(theme, $) {

    theme = theme || {};

    var instanceName = '__slider';

    var PluginSlider = function($el, opts) {
        return this.initialize($el, opts);
    };

    PluginSlider.defaults = {

    };

    PluginSlider.prototype = {
        initialize: function($el, opts) {
            if ( $el.data( instanceName ) ) {
                return this;
            }

            this.$el = $el;

            this
                .setVars()
                .setData()
                .setOptions(opts)
                .build();

            return this;
        },

        setVars: function() {
            var $output = $( this.$el.data('plugin-slider-output') );
            this.$output = $output.get(0) ? $output : null;

            return this;
        },

        setData: function() {
            this.$el.data(instanceName, this);

            return this;
        },

        setOptions: function(opts) {
            var _self = this;
            this.options = $.extend( true, {}, PluginSlider.defaults, opts );

            if ( this.$output ) {
                $.extend( this.options, {
                    slide: function( event, ui ) {
                        _self.onSlide( event, ui );
                    }
                });
            }

            return this;
        },

        build: function() {
            this.$el.slider( this.options );

            return this;
        },

        onSlide: function( event, ui ) {
            if ( !ui.values ) {
                this.$output.val( ui.value );
            } else {
                this.$output.val( ui.values[ 0 ] + '/' + ui.values[ 1 ] );
            }

            this.$output.trigger('change');
        }
    };

    // expose to scope
    $.extend(theme, {
        PluginSlider: PluginSlider
    });

    // jquery plugin
    $.fn.themePluginSlider = function(opts) {
        return this.each(function() {
            var $this = $(this);

            if ($this.data(instanceName)) {
                return $this.data(instanceName);
            } else {
                return new PluginSlider($this, opts);
            }

        });
    }

}).apply(this, [window.theme, jQuery]);