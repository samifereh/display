/**
 * Created by loic on 02/08/16.
 */
var customForm = {
    el: {
        trigger: document.createElement('button'),
        advancedFields: document.querySelectorAll('.advanced')
    },
    run: function (formName) {
        console.log('customForm is running');
        customForm.el.trigger.className = 'trigger-advanced btn btn-primary';
        customForm.el.trigger.innerHTML = 'Afficher les options avancées';
        document.querySelector('form[name="' + formName + '"]').parentNode.insertBefore(customForm.el.trigger, document.querySelector('form[name="' + formName + '"]'));
        customForm.toggleFields();
        customForm.bindAction();
    },
    toggleFields: function () {
        for (var i = 0; i < customForm.el.advancedFields.length; i++) {
            var field = customForm.el.advancedFields[i];
            var pNode = field.parentNode;
            while (pNode !== undefined && pNode != null && !pNode.classList.contains('form-group')) {
                pNode = pNode.parentNode;
            }
            if (pNode !== undefined && pNode !== null) {
                pNode.classList.toggle('hidden');
            }
        }
    },
    toogleTrigger: function () {
        if (customForm.el.trigger.innerHTML == 'Afficher les options avancées') {
            customForm.el.trigger.innerHTML = 'Cacher les options avancées';
        } else {
            customForm.el.trigger.innerHTML = 'Afficher les options avancées';
        }
        console.log('toogle Trigger');
    },
    bindAction: function () {
        customForm.el.trigger.addEventListener('click', function (e) {
            customForm.toggleFields();
            customForm.toogleTrigger();
            e.preventDefault();
        });
    }
}

