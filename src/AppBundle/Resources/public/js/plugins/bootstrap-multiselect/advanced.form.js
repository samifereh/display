/*
Name: 			Forms / Advanced - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.5.4
*/


// MultiSelect
(function(theme, $) {

    theme = theme || {};

    var instanceName = '__multiselect';

    var PluginMultiSelect = function($el, opts) {
        return this.initialize($el, opts);
    };

    PluginMultiSelect.defaults = {
        templates: {
            filter: '<div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div>'
        }
    };

    PluginMultiSelect.prototype = {
        initialize: function($el, opts) {
            if ( $el.data( instanceName ) ) {
                return this;
            }

            this.$el = $el;

            this
                .setData()
                .setOptions(opts)
                .build();

            return this;
        },

        setData: function() {
            this.$el.data(instanceName, this);

            return this;
        },

        setOptions: function(opts) {
            this.options = $.extend( true, {}, PluginMultiSelect.defaults, opts );

            return this;
        },

        build: function() {
            this.$el.multiselect( this.options );

            return this;
        }
    };

    // expose to scope
    $.extend(theme, {
        PluginMultiSelect: PluginMultiSelect
    });

    // jquery plugin
    $.fn.themePluginMultiSelect = function(opts) {
        return this.each(function() {
            var $this = $(this);

            if ($this.data(instanceName)) {
                return $this.data(instanceName);
            } else {
                return new PluginMultiSelect($this, opts);
            }

        });
    }

}).apply(this, [window.theme, jQuery]);


// MultiSelect
(function($) {

    'use strict';

    if ( $.isFunction( $.fn[ 'multiselect' ] ) ) {

        $(function() {
            $( '[data-plugin-multiselect]' ).each(function() {

                var $this = $( this ),
                    opts = {};

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginMultiSelect(opts);

            });
        });

    }

}).apply(this, [jQuery]);

(function($) {


	/*
	Multi Select: Toggle All Button
	*/
	function multiselect_selected($el) {
		var ret = true;
		$('option', $el).each(function(element) {
			if (!!!$(this).prop('selected')) {
				ret = false;
			}
		});
		return ret;
	}

	function multiselect_selectAll($el, $r) {
		$('option', $el).each(function(element) {
            $r.multiselect('select', $(this).val());
		});
	}

	function multiselect_deselectAll($el, $r) {
		$('option', $el).each(function(element) {
            $r.multiselect('deselect', $(this).val());
		});
	}

	function multiselect_toggle($el, $btn) {
		if (multiselect_selected($el)) {
			multiselect_deselectAll($el);
			$btn.text("Tout Séléctionner");
		}
		else {
			multiselect_selectAll($el);
			$btn.text("Tout Déséléctionner");
		}
	}

    $('input[name="ad_mention_legals[type][]"]').on('change',function(e){
		e.preventDefault();
        if($(this).prop('checked')) {
            multiselect_selectAll('#ad_mention_legals_portals optgroup[label="'+$(this).val()+'"]',$('#ad_mention_legals_portals'))
        } else {
            multiselect_deselectAll('#ad_mention_legals_portals optgroup[label="'+$(this).val()+'"]',$('#ad_mention_legals_portals'))
        }
	});
}(jQuery));