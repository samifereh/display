<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use AppBundle\Interfaces\The;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgencyRepository")
 * @ORM\Table(name="`agency`")
 * @Gedmo\Uploadable(
 *     path="uploads/avatars/agency",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Agency
{

    public function __construct(){
        $this->setDate(new \DateTime("now"));
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Group")
     */
    protected $group;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the agency name.")
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The agency name is too short.",
     *     maxMessage="The agency name is too long."
     * )
     */
    protected $name;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg"}
     * )
     */
    private $avatar = "";

    /**
     * @ORM\Column(name="voicemail", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"audio/mpeg", "audio/x-mpeg", "audio/mp3", "audio/x-mp3",
     *     "audio/mpeg3", "audio/x-mpeg3", "audio/mpg", "audio/x-mpg", "audio/x-mpegaudio"
     *      }
     * )
     */
    private $voicemail = "";

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $voicemailVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=14, nullable=true)
     */
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_phone", type="string", length=14, nullable=true)
     */
    protected $trackingPhone;


    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    protected $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_alias", type="string", length=255, nullable=true)
     */
    protected $emailAlias;


    /**
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", length=7, nullable=true)
     */
    protected $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=128, nullable=true)
     */
    protected $ville;
    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=255, nullable=true)
     */
    protected $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=7, nullable=true)
     */
    protected $color;



    /**
     * The content of the news
     * @ORM\Column(type="text", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter a content.")
     */
    protected $content;

    /**
     * The creating? date of the news
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * The closing date of the agency
     * @ORM\Column(name="closing_date", type="datetime", nullable=true)
     */
    protected $closingDate;

    /**
     * The reopening date of the agency
     * @ORM\Column(type="datetime", name="reopening_date", nullable=true)
     */
    protected $reopeningDate;

    /**
     * The closing phone number
     * @ORM\Column(name="closing_phone", type="string", length=14, nullable=true)
     */
    protected $closingPhone;


    /**
     * The slug of the views. It's created from the title
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;


    /**
     * @var string
     *
     * @ORM\Column(name="facturation_nomEntite", type="string", length=255, nullable=true)
     */
    protected $facturationNomEntite;
    /**
     * @var string
     *
     * @ORM\Column(name="facturation_codePostal", type="string", length=7, nullable=true)
     */
    protected $facturationCodePostal;
    /**
     * @var string
     *
     * @ORM\Column(name="facturation_ville", type="string", length=255, nullable=true)
     */
    protected $facturationVille;

    /**
     * @var string
     *
     * @ORM\Column(name="facturation_adresse", type="string", length=255, nullable=true)
     */
    protected $facturationAdresse;


    /**
     *
     * @var ArrayCollection
     * @OneToMany(targetEntity="TrackingBundle\Entity\Advantage", mappedBy="agency",cascade={"persist"}, fetch="EAGER")
     */
    protected $trackingAdvantages;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName(){ return $this->name; }
    public function setName($name){ $this->name = $name; }

    public function setContent($content){ $this->content = $content; }
    public function getContent(){ return $this->content; }

    public function getAvatar(){ return $this->avatar; }
    public function setAvatar($avatar){ $this->avatar = $avatar; }

    public function getVoicemail(){ return $this->voicemail; }
    public function setVoicemail($voicemail){ $this->voicemail = $voicemail; }

    public function setDate($date){ $this->date = $date; }
    public function getDate(){ return $this->date; }

    public function setGroup($group){ $this->group = $group; }
    public function getGroup() { return $this->group; }

    public function setSlug($slug){ $this->slug = $slug; }
    public function getSlug(){ return $this->slug; }



    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Agency
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Agency
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Agency
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Agency
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Agency
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Agency
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return Agency
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Agency
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set facturationNomEntite
     *
     * @param string $facturationNomEntite
     *
     * @return Agency
     */
    public function setFacturationNomEntite($facturationNomEntite)
    {
        $this->facturationNomEntite = $facturationNomEntite;

        return $this;
    }

    /**
     * Get facturationNomEntite
     *
     * @return string
     */
    public function getFacturationNomEntite()
    {
        return $this->facturationNomEntite;
    }

    /**
     * Set facturationCodePostal
     *
     * @param string $facturationCodePostal
     *
     * @return Agency
     */
    public function setFacturationCodePostal($facturationCodePostal)
    {
        $this->facturationCodePostal = $facturationCodePostal;

        return $this;
    }

    /**
     * Get facturationCodePostal
     *
     * @return string
     */
    public function getFacturationCodePostal()
    {
        return $this->facturationCodePostal;
    }

    /**
     * Set facturationVille
     *
     * @param string $facturationVille
     *
     * @return Agency
     */
    public function setFacturationVille($facturationVille)
    {
        $this->facturationVille = $facturationVille;

        return $this;
    }

    /**
     * Get facturationVille
     *
     * @return string
     */
    public function getFacturationVille()
    {
        return $this->facturationVille;
    }

    /**
     * Set facturationAdresse
     *
     * @param string $facturationAdresse
     *
     * @return Agency
     */
    public function setFacturationAdresse($facturationAdresse)
    {
        $this->facturationAdresse = $facturationAdresse;

        return $this;
    }

    /**
     * Get facturationAdresse
     *
     * @return string
     */
    public function getFacturationAdresse()
    {
        return $this->facturationAdresse;
    }

    /**
     * @return string
     */
    public function getTrackingPhone()
    {
        return preg_replace("/^33/", "0", $this->trackingPhone,1);
    }

    /**
     * @param string $trackingPhone
     */
    public function setTrackingPhone($trackingPhone)
    {
        $this->trackingPhone = $trackingPhone;
    }

    /**
     * @return mixed
     */
    public function getTrackingAdvantages()
    {
        return $this->trackingAdvantages;
    }

    /**
     * @param mixed $trackingAdvantages
     */
    public function setTrackingAdvantages($trackingAdvantages)
    {
        $this->trackingAdvantages = $trackingAdvantages;
    }

    /**
     * @return \DateTime
     */
    public function getClosingDate()
    {
        return $this->closingDate;
    }

    /**
     * @param \DateTime $closingDate
     */
    public function setClosingDate($closingDate)
    {
        $this->closingDate = $closingDate;
    }

    /**
     * @return \DateTime
     */
    public function getReopeningDate()
    {
        return $this->reopeningDate;
    }

    /**
     * @param \DateTime $reopeningDate
     */
    public function setReopeningDate($reopeningDate)
    {
        $this->reopeningDate = $reopeningDate;
    }

    /**
     * @return mixed
     */
    public function getClosingPhone()
    {
        return $this->closingPhone;
    }

    /**
     * @param mixed $closingPhone
     */
    public function setClosingPhone($closingPhone)
    {
        $this->closingPhone = $closingPhone;
    }



    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Agency
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set emailAlias
     *
     * @param string $emailAlias
     *
     * @return Agency
     */
    public function setEmailAlias($emailAlias)
    {
        $this->emailAlias = $emailAlias;

        return $this;
    }

    /**
     * Get emailAlias
     *
     * @return string
     */
    public function getEmailAlias()
    {
        return $this->emailAlias;
    }

    /**
     * Add trackingAdvantage
     *
     * @param \TrackingBundle\Entity\Advantage $trackingAdvantage
     *
     * @return Agency
     */
    public function addTrackingAdvantage(\TrackingBundle\Entity\Advantage $trackingAdvantage)
    {
        $this->trackingAdvantages[] = $trackingAdvantage;

        return $this;
    }

    /**
     * Remove trackingAdvantage
     *
     * @param \TrackingBundle\Entity\Advantage $trackingAdvantage
     */
    public function removeTrackingAdvantage(\TrackingBundle\Entity\Advantage $trackingAdvantage)
    {
        $this->trackingAdvantages->removeElement($trackingAdvantage);
    }

    /**
     * Set voicemailVersion
     *
     * @param integer $voicemailVersion
     *
     * @return Agency
     */
    public function setVoicemailVersion($voicemailVersion)
    {
        $this->voicemailVersion = $voicemailVersion;

        return $this;
    }

    /**
     * Get voicemailVersion
     *
     * @return integer
     */
    public function getVoicemailVersion()
    {
        return $this->voicemailVersion;
    }

    /**
     * increment the current voicemail version
     */
    public function incrementVoicemailVersion(){
        $this->voicemailVersion++;
    }

    public function getExportTelephone(){
        if($this->getTrackingPhone())
            return $this->getTrackingPhone();

        $currentTime = new \DateTime("now");
        if(
            $this->getClosingPhone() &&
            $this->getClosingDate() && $this->getReopeningDate() &&
            $this->getClosingDate() <= $currentTime && $this->getReopeningDate() > $currentTime
        )
            return $this->getClosingPhone();

        return $this->getTelephone();
    }
}
