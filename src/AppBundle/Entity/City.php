<?php

namespace Bundle\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * City
 *
 * @author y.chakroun <y.chakroun@novactive.com>
 *
 * @ORM\Table(name="villes_france")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository");
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ville_id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_code_postal", type="string", length=5, nullable=false)
     */
    private $code_postal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville_nom_reel", type="string", length=255, nullable=false)
     */
    private $ville;
    /**
     * @var string
     *
     * @ORM\Column(name="ville_latitude_deg", type="string", length=255, nullable=false)
     */
    private $latitude;
    /**
     * @var string
     *
     * @ORM\Column(name="ville_longitude_deg", type="string", length=255, nullable=false)
     */
    private $longitude;


    /**
     * Get ville_id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get ville_code_postal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->code_postal;
    }


    /**
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }


    /**
     * Get Nom_commune
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->attribute('ville_longitude_deg');
    }

    /**
     * Get Nom_commune
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->attribute('ville_latitude_deg');
    }


}