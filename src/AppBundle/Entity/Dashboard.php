<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dashboard
 *
 * @ORM\Table(name="dashboard")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DashboardRepository")
 */
class Dashboard
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="welcome", type="blob")
     */
    private $welcome;

    /**
     * @var int
     *
     * @ORM\Column(name="nbNews", type="integer", nullable=true)
     */
    private $nbNews;

    /**
     * @ORM\Column(name="faqText", type="blob", nullable=true)
     */
    private $faqText;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set welcome
     *
     * @param string $welcome
     *
     * @return Dashboard
     */
    public function setWelcome($welcome)
    {
        $this->welcome = $welcome;

        return $this;
    }

    /**
     * Get welcome
     *
     * @return string
     */
    public function getWelcome()
    {
        if($this->welcome) {
            rewind($this->welcome);
            return stream_get_contents($this->welcome);
        }
        return null;
    }

    /**
     * Set nbNews
     *
     * @param integer $nbNews
     *
     * @return Dashboard
     */
    public function setNbNews($nbNews)
    {
        $this->nbNews = $nbNews;

        return $this;
    }

    /**
     * Get nbNews
     *
     * @return int
     */
    public function getNbNews()
    {
        return $this->nbNews;
    }

    /**
     * Set faqText
     *
     * @param string $faqText
     *
     * @return Dashboard
     */
    public function setFaqText($faqText)
    {
        $this->faqText = $faqText;

        return $this;
    }

    /**
     * Get faqText
     *
     * @return string
     */
    public function getFaqText()
    {
        if($this->faqText) {
            rewind($this->faqText);
            return stream_get_contents($this->faqText);
        }
        return null;
    }
}

