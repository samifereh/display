<?php

namespace AppBundle\Entity\Configuration;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Configuration\AdvantageRepository")
 * @ORM\Table(name="advantages")
 */
class Advantage
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = true;

    /**
     * @var boolean
     * @ORM\Column(name="recurrent", type="boolean")
     */
    protected $recurrent;

    /**
     * @ORM\Column(name="name", type="string")
     * @var integer
     */
    protected $name;

    /**
     * @ORM\Column(name="unite", type="string",nullable=true)
     * @var string
     */
    protected $unite;

    /**
     * @ORM\Column(name="group_type", type="string",nullable=true)
     * @var string
     */
    protected $groupType;

    /**
     * @ORM\Column(name="slug", type="string")
     * @var string
     */
    protected $slug;

    /**
     * @ORM\Column(name="description", type="text")
     * @var integer
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="AdvantageOption", mappedBy="advantage",cascade={"persist", "remove"}, fetch="EAGER")
     */
    protected $options;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupAdvantage", mappedBy="advantage",cascade={"remove"}, fetch="EXTRA_LAZY")
     */
    protected $groupsAdvantage;

    /**
     * @ORM\ManyToOne(targetEntity="Configuration", inversedBy="advantages",cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="configuration_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     */
    protected $configuration;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Advantage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add groupsAdvantage
     *
     * @param \AppBundle\Entity\GroupAdvantage $groupsAdvantage
     *
     * @return Advantage
     */
    public function addGroupsAdvantage(\AppBundle\Entity\GroupAdvantage $groupsAdvantage)
    {
        $this->groupsAdvantage[] = $groupsAdvantage;

        return $this;
    }

    /**
     * Remove groupsAdvantage
     *
     * @param \AppBundle\Entity\GroupAdvantage $groupsAdvantage
     */
    public function removeGroupsAdvantage(\AppBundle\Entity\GroupAdvantage $groupsAdvantage)
    {
        $this->groupsAdvantage->removeElement($groupsAdvantage);
    }

    /**
     * Get groupsAdvantage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupsAdvantage()
    {
        return $this->groupsAdvantage;
    }

    /**
     * Add option
     *
     * @param \AppBundle\Entity\Configuration\AdvantageOption $option
     *
     * @return Advantage
     */
    public function addOption(\AppBundle\Entity\Configuration\AdvantageOption $option)
    {
        $option->setAdvantage($this);
        $this->options[] = $option;
        return $this;
    }

    /**
     * Remove option
     *
     * @param \AppBundle\Entity\Configuration\AdvantageOption $option
     */
    public function removeOption(\AppBundle\Entity\Configuration\AdvantageOption $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get options
     *
     * @return \AppBundle\Entity\Configuration\AdvantageOption $option
     */
    public function getCheapestOption()
    {
        $return = null;
        if($this->options) {
            foreach ($this->options as $option) {
                if(!is_null($return)) {
                    if($return->getPrice() > $option->getPrice())
                        $return = $option;
                } else {
                    $return = $option;
                }
            }
        }
        return $return;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Advantage
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set configuration
     *
     * @param \AppBundle\Entity\Configuration\Configuration $configuration
     *
     * @return Advantage
     */
    public function setConfiguration(\AppBundle\Entity\Configuration\Configuration $configuration = null)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return \AppBundle\Entity\Configuration\Configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Advantage
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Advantage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return Advantage
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set groupType
     *
     * @param string $groupType
     *
     * @return Advantage
     */
    public function setGroupType($groupType)
    {
        $this->groupType = $groupType;

        return $this;
    }

    /**
     * Get groupType
     *
     * @return string
     */
    public function getGroupType()
    {
        return $this->groupType;
    }

    /**
     * Set recurrent
     *
     * @param boolean $recurrent
     *
     * @return Advantage
     */
    public function setRecurrent($recurrent)
    {
        $this->recurrent = $recurrent;

        return $this;
    }

    /**
     * Get recurrent
     *
     * @return boolean
     */
    public function getRecurrent()
    {
        return $this->recurrent;
    }
}
