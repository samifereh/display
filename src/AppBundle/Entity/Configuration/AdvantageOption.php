<?php

namespace AppBundle\Entity\Configuration;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Configuration\AdvantageOptionRepository")
 * @ORM\Table(name="advantage_options")
 */
class AdvantageOption
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = true;

    /**
     * @ORM\Column(name="reference", type="string", nullable=true)
     * @var integer
     */
    protected $reference;

    /**
     * @ORM\Column(name="unit", type="integer", nullable=true)
     * @var integer
     */
    protected $unit;

    /**
     * @ORM\Column(name="month", type="integer", nullable=true)
     * @var integer
     */
    protected $month;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="Advantage", inversedBy="options")
     * @ORM\JoinColumn(name="advantage_id", referencedColumnName="id",onDelete="SET NULL",nullable=true)
     */
    protected $advantage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return AdvantageOption
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return AdvantageOption
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set unit
     *
     * @param integer $unit
     *
     * @return AdvantageOption
     */
    public function setUnit($unit)
    {
        if(is_null($unit))
            $unit = -1;
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return integer
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set month
     *
     * @param integer $month
     *
     * @return AdvantageOption
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return AdvantageOption
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set advantage
     *
     * @param \AppBundle\Entity\Configuration\Advantage $advantage
     *
     * @return AdvantageOption
     */
    public function setAdvantage(\AppBundle\Entity\Configuration\Advantage $advantage = null)
    {
        $this->advantage = $advantage;

        return $this;
    }

    /**
     * Get advantage
     *
     * @return \AppBundle\Entity\Configuration\Advantage
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

}
