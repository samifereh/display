<?php

namespace AppBundle\Entity\Configuration;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Configuration\ConfigurationRepository")
 * @ORM\Table(name="configuration")
 */
class Configuration
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToMany(targetEntity="Advantage", mappedBy="configuration",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $advantages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->advantages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add advantage
     *
     * @param \AppBundle\Entity\Configuration\Advantage $advantage
     *
     * @return Configuration
     */
    public function addAdvantage(\AppBundle\Entity\Configuration\Advantage $advantage)
    {
        $advantage->setConfiguration($this);
        $this->advantages[] = $advantage;

        return $this;
    }

    /**
     * Remove advantage
     *
     * @param \AppBundle\Entity\Configuration\Advantage $advantage
     */
    public function removeAdvantage(\AppBundle\Entity\Configuration\Advantage $advantage)
    {
        $this->advantages->removeElement($advantage);
    }

    /**
     * Get advantages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvantages()
    {
        return $this->advantages;
    }
}
