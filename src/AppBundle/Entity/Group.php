<?php
// src/MyProject/MyBundle/Entity/Group.php

namespace AppBundle\Entity;

use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

// * @ORM\AttributeOverrides({
//*     @ORM\AttributeOverride(name="name", column=@ORM\Column(type="string", name="name", length=255, unique=false, nullable=false))
//* })
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupRepository")
 * @ORM\Table(name="`user_group`")
 * @UniqueEntity(fields={"name", "parentGroup"}, errorPath="name", message="fos_user.group_name.already_used")
 */
class Group extends BaseGroup implements Comparable, EqualityComparable
{

    const ROLES = ['ROLE_AGENCY','ROLE_GROUP'];
    public function __construct()
    {
        $this->roles = [];
        $this->advantagesDiscount = new ArrayCollection();
        $this->advantages = new ArrayCollection();
    }

    /**
     * The unique id of the group
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\Column(type="string",unique=true, length=4, nullable=true)
     */
    protected $prefix;

    /**
     * The slug of the views. It's created from the title
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $mailjetId;

    /**
     * All the users being in the group
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="groups",cascade={"persist","remove"})
     */
    protected $users;

    /**
     * All the users being in the group
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Agency", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $agency;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AdRewrite", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="ad_rewrite_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $adRewrite;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AdMentionLegals", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="ad_mention_legals_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $adMentionLegals;

    /**
     *
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $document;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Ad", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $ad;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\HouseModel", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $housemodel;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\BienImmobilier", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $biens;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Broadcast\PortalRegistration",cascade={"persist", "remove"}, mappedBy="group", fetch="EXTRA_LAZY")
     */
    protected $broadcastPortalRegistrations;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Payment\Order", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $orders;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Payment\AuthorizedUser",cascade={"persist", "remove"}, fetch="EXTRA_LAZY", mappedBy="group")
     */
    protected $authorizedUser;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Payment\AuthorizedAgency",cascade={"persist", "remove"}, fetch="EXTRA_LAZY", mappedBy="parentGroup")
     */
    protected $authorizedAgency;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\GroupAdvantageDiscount",cascade={"persist", "remove"}, fetch="EXTRA_LAZY", mappedBy="group")
     */
    protected $advantagesDiscount;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Program", mappedBy="group",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $programs;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Group", mappedBy="parentGroup",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $agencyGroups;

    /**
     * @var
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="agencyGroups",cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $parentGroup;

    /**
     * @var boolean
     *
     * @ORM\Column(name="blocked", type="boolean")
     */
    protected $blocked = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="annonces_option", type="boolean")
     */
    protected $annonces_option = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_option", type="boolean")
     */
    protected $program_option = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="suivi_option", type="boolean")
     */
    protected $suivi_option = false;

    #### PROGRAM RESTRICTIONS ####
    /**
     * @var boolean
     *
     * @ORM\Column(name="program_maison_option", type="boolean")
     */
    protected $programMaisonOption = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_appartement_option", type="boolean")
     */
    protected $programAppartementOption = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_terrain_option", type="boolean")
     */
    protected $programTerrainOption = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_mixte_option", type="boolean")
     */
    protected $programMixteOption = false;

    ####RESTRICTIONS####
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupAdvantage", mappedBy="group", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $advantages;

    ####IMPORT D'ANNONCES####
    /**
     * @var string
     *
     * @ORM\Column(name="import_host", type="string", length=255, nullable=true)
     */
    protected $importHost;

    /**
     * @var string
     *
     * @ORM\Column(name="import_user", type="string", length=255, nullable=true)
     */
    protected $importUser;

    /**
     * @var string
     *
     * @ORM\Column(name="import_password", type="string", length=255, nullable=true)
     */
    protected $importPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="import_file_name", type="string", length=255, nullable=true)
     */
    protected $importFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="email_alias", type="string", length=255, nullable=true)
     */
    protected $emailAlias;

    /**
     * @var string
     *
     * @ORM\Column(name="email_alias_id", type="string", nullable=true)
     */
    protected $emailAliasId;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $oneMonthPrice;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $threeMonthPrice;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $sixMonthPrice;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $oneYearPrice;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $trialStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $trialEnd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $periodStart;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $periodEnd;

    /**
     * @var boolean
     *
     * @ORM\Column(name="diffusion_option", type="boolean",  options={"default"=true})
     */
    protected $diffusionOption = true;


    public function getId()
    {
        return $this->id;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return \AppBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param $agency \AppBundle\Entity\Agency
     */
    public function setAgency($agency)
    {
        $agency->setGroup($this);
        $this->agency = $agency;
    }

    public function compareTo($obj)
    {
        if ($obj == null) {
            throw new NullPointerException(${$obj});
        }
        if (!($obj instanceof Group)) {
            throw new ClassCastException(${$obj});
        }

        if ($this->getId() < $obj->getid()) {
            return -1;
        } else if ($this->getId() > $obj->getid()) {
            return 1;
        } else {
            return 0;
        }

    }

    public function __toString()
    {
        return $this->getId() . ' ';
    }

    public function equals($o) {
        if ($o == null) {
            throw new NullPointerException(${$o});
        }
        if (!($o instanceof Group)) {
            throw new ClassCastException(${$o});
        }

        return $this->getId() == $o->getId();
    }

    public function contains(User $user) {
        foreach ($this->getUsers() as $currentUser) {
            if($user->equals($currentUser)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Group
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return Group
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $this->document[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->document->removeElement($document);
    }

    /**
     * Get document
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return Group
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }

    /**
     * Get ad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Add broadcastPortalRegistration
     *
     * @param \AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration
     *
     * @return Group
     */
    public function addBroadcastPortalRegistration(\AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration)
    {
        $this->broadcastPortalRegistrations[] = $broadcastPortalRegistration;

        return $this;
    }

    /**
     * Remove broadcastPortalRegistration
     *
     * @param \AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration
     */
    public function removeBroadcastPortalRegistration(\AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration)
    {
        $this->broadcastPortalRegistrations->removeElement($broadcastPortalRegistration);
    }

    /**
     * Get broadcastPortalRegistrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPortalRegistrations()
    {
        return $this->broadcastPortalRegistrations;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     *
     * @return Group
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }


    public function setSlug($slug){ $this->slug = $slug; }
    public function getSlug(){ return $this->slug; }


    /**
     * Set blocked
     *
     * @param boolean $blocked
     *
     * @return Group
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * Get blocked
     *
     * @return boolean
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Payment\Order $order
     *
     * @return Group
     */
    public function addOrder(\AppBundle\Entity\Payment\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Payment\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Payment\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add authorizedUser
     *
     * @param \AppBundle\Entity\Payment\AuthorizedUser $authorizedUser
     *
     * @return Group
     */
    public function addAuthorizedUser(\AppBundle\Entity\Payment\AuthorizedUser $authorizedUser)
    {
        $this->authorizedUser[] = $authorizedUser;

        return $this;
    }

    /**
     * Remove authorizedUser
     *
     * @param \AppBundle\Entity\Payment\AuthorizedUser $authorizedUser
     */
    public function removeAuthorizedUser(\AppBundle\Entity\Payment\AuthorizedUser $authorizedUser)
    {
        $this->authorizedUser->removeElement($authorizedUser);
    }

    /**
     * Get authorizedUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizedUser()
    {
        return $this->authorizedUser;
    }

    /**
     * Add program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Group
     */
    public function addProgram(\AppBundle\Entity\Program $program)
    {
        $this->programs[] = $program;

        return $this;
    }

    /**
     * Remove program
     *
     * @param \AppBundle\Entity\Program $program
     */
    public function removeProgram(\AppBundle\Entity\Program $program)
    {
        $this->programs->removeElement($program);
    }

    /**
     * Get programs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrograms()
    {
        return $this->programs;
    }

    /**
     * Get agencyGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencyGroups()
    {
        return $this->agencyGroups;
    }

    /**
     * Set agencyGroups
     *
     * @param \AppBundle\Entity\Group $agencyGroups
     *
     * @return Group
     */
    public function setAgencyGroups(\AppBundle\Entity\Group $agencyGroups = null)
    {
        $this->agencyGroups = $agencyGroups;

        return $this;
    }

    /**
     * Add agencyGroup
     *
     * @param \AppBundle\Entity\Group $agencyGroup
     *
     * @return Group
     */
    public function addAgencyGroup(\AppBundle\Entity\Group $agencyGroup)
    {
        $this->agencyGroups[] = $agencyGroup;

        return $this;
    }

    /**
     * Remove agencyGroup
     *
     * @param \AppBundle\Entity\Group $agencyGroup
     */
    public function removeAgencyGroup(\AppBundle\Entity\Group $agencyGroup)
    {
        $this->agencyGroups->removeElement($agencyGroup);
    }

    /**
     * Set parentGroup
     *
     * @param \AppBundle\Entity\Group $parentGroup
     *
     * @return Group
     */
    public function setParentGroup(\AppBundle\Entity\Group $parentGroup = null)
    {
        $this->parentGroup = $parentGroup;

        return $this;
    }

    /**
     * Get parentGroup
     *
     * @return \AppBundle\Entity\Group
     */
    public function getParentGroup()
    {
        return $this->parentGroup;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return Ad
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    public function isMemberOf($group){
        if($group instanceof \Doctrine\ORM\PersistentCollection) {
            foreach ($group as $ga) {
                foreach ($ga->getAgencyGroups() as $g) {
                    if($g->getRemoteId() == $this->remoteId) { return true; }
                }
            }
        } else {
            foreach ($group->getAgencyGroups() as $g) {
                if($g->getRemoteId() == $this->remoteId) { return true; }
            }
            if($group->equals($group))
                return true;
            return false;
        }
        return false;
    }


    /**
     * Set annoncesOption
     *
     * @param boolean $annoncesOption
     *
     * @return Group
     */
    public function setAnnoncesOption($annoncesOption)
    {
        $this->annonces_option = $annoncesOption;

        return $this;
    }

    /**
     * Get annoncesOption
     *
     * @return boolean
     */
    public function getAnnoncesOption()
    {
        return $this->annonces_option;
    }

    /**
     * Set programOption
     *
     * @param boolean $programOption
     *
     * @return Group
     */
    public function setProgramOption($programOption)
    {
        $this->program_option = $programOption;

        return $this;
    }

    /**
     * Get programOption
     *
     * @return boolean
     */
    public function getProgramOption()
    {
        return $this->program_option;
    }

    /**
     * Add housemodel
     *
     * @param \AppBundle\Entity\HouseModel $housemodel
     *
     * @return Group
     */
    public function addHousemodel(\AppBundle\Entity\HouseModel $housemodel)
    {
        $this->housemodel[] = $housemodel;

        return $this;
    }

    /**
     * Remove housemodel
     *
     * @param \AppBundle\Entity\HouseModel $housemodel
     */
    public function removeHousemodel(\AppBundle\Entity\HouseModel $housemodel)
    {
        $this->housemodel->removeElement($housemodel);
    }

    /**
     * Get housemodel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHousemodel()
    {
        return $this->housemodel;
    }

    /**
     * Add bien
     *
     * @param \AppBundle\Entity\BienImmobilier $bien
     *
     * @return Group
     */
    public function addBien(\AppBundle\Entity\BienImmobilier $bien)
    {
        $this->biens[] = $bien;

        return $this;
    }

    /**
     * Remove bien
     *
     * @param \AppBundle\Entity\BienImmobilier $bien
     */
    public function removeBien(\AppBundle\Entity\BienImmobilier $bien)
    {
        $this->biens->removeElement($bien);
    }

    /**
     * Get biens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBiens()
    {
        return $this->biens;
    }

    public function hasProgramOption() {
        if(!$this->hasRole('ROLE_GROUP'))
            return $this->getProgramOption();

        foreach ($this->getAgencyGroups() as $agency) {
            if($agency->getProgramOption())
                return true;
        }
        return false;
    }

    public function hasAnnoncesOption() {
        if(!$this->hasRole('ROLE_GROUP'))
            return $this->getAnnoncesOption();

        foreach ($this->getAgencyGroups() as $agency) {
            if($agency->getAnnoncesOption())
                return true;
        }
        return false;
    }

    public function hasSuiviOption() {
        if(!$this->hasRole('ROLE_GROUP'))
            return $this->getSuiviOption();

        foreach ($this->getAgencyGroups() as $agency) {
            if($agency->getSuiviOption())
                return true;
        }
        return false;
    }

    /**
     * Set importHost
     *
     * @param string $importHost
     *
     * @return Group
     */
    public function setImportHost($importHost)
    {
        $this->importHost = $importHost;

        return $this;
    }

    /**
     * Get importHost
     *
     * @return string
     */
    public function getImportHost()
    {
        return $this->importHost;
    }

    /**
     * Set importUser
     *
     * @param string $importUser
     *
     * @return Group
     */
    public function setImportUser($importUser)
    {
        $this->importUser = $importUser;

        return $this;
    }

    /**
     * Get importUser
     *
     * @return string
     */
    public function getImportUser()
    {
        return $this->importUser;
    }

    /**
     * Set importPassword
     *
     * @param string $importPassword
     *
     * @return Group
     */
    public function setImportPassword($importPassword)
    {
        $this->importPassword = $importPassword;

        return $this;
    }

    /**
     * Get importPassword
     *
     * @return string
     */
    public function getImportPassword()
    {
        return $this->importPassword;
    }

    /**
     * Set importFileName
     *
     * @param string $importFileName
     *
     * @return Group
     */
    public function setImportFileName($importFileName)
    {
        $this->importFileName = $importFileName;

        return $this;
    }

    /**
     * Get importFileName
     *
     * @return string
     */
    public function getImportFileName()
    {
        return $this->importFileName;
    }

    /**
     * Set programMaisonOption
     *
     * @param boolean $programMaisonOption
     *
     * @return Group
     */
    public function setProgramMaisonOption($programMaisonOption)
    {
        $this->programMaisonOption = $programMaisonOption;

        return $this;
    }

    /**
     * Get programMaisonOption
     *
     * @return boolean
     */
    public function getProgramMaisonOption()
    {
        return $this->programMaisonOption;
    }

    /**
     * Set programAppartementOption
     *
     * @param boolean $programAppartementOption
     *
     * @return Group
     */
    public function setProgramAppartementOption($programAppartementOption)
    {
        $this->programAppartementOption = $programAppartementOption;

        return $this;
    }

    /**
     * Get programAppartementOption
     *
     * @return boolean
     */
    public function getProgramAppartementOption()
    {
        return $this->programAppartementOption;
    }

    /**
     * Set programTerrainOption
     *
     * @param boolean $programTerrainOption
     *
     * @return Group
     */
    public function setProgramTerrainOption($programTerrainOption)
    {
        $this->programTerrainOption = $programTerrainOption;

        return $this;
    }

    /**
     * Get programTerrainOption
     *
     * @return boolean
     */
    public function getProgramTerrainOption()
    {
        return $this->programTerrainOption;
    }

    /**
     * Set programMixteOption
     *
     * @param boolean $programMixteOption
     *
     * @return Group
     */
    public function setProgramMixteOption($programMixteOption)
    {
        $this->programMixteOption = $programMixteOption;

        return $this;
    }

    /**
     * Get programMixteOption
     *
     * @return boolean
     */
    public function getProgramMixteOption()
    {
        return $this->programMixteOption;
    }

    /**
     * Add advantage
     *
     * @param \AppBundle\Entity\GroupAdvantage $advantage
     *
     * @return Group
     */
    public function addAdvantage(\AppBundle\Entity\GroupAdvantage $advantage)
    {
        $advantage->setGroup($this);
        $this->advantages[] = $advantage;

        return $this;
    }

    /**
     * Remove advantage
     *
     * @param \AppBundle\Entity\GroupAdvantage $advantage
     */
    public function removeAdvantage(\AppBundle\Entity\GroupAdvantage $advantage)
    {
        $this->advantages->removeElement($advantage);
    }

    /**
     * Get advantages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvantages()
    {
        return $this->advantages;
    }

    /**
     * Add authorizedAgency
     *
     * @param \AppBundle\Entity\Payment\AuthorizedAgency $authorizedAgency
     *
     * @return Group
     */
    public function addAuthorizedAgency(\AppBundle\Entity\Payment\AuthorizedAgency $authorizedAgency)
    {
        $authorizedAgency->setGroup($this);
        $this->authorizedAgency[] = $authorizedAgency;

        return $this;
    }

    /**
     * Remove authorizedAgency
     *
     * @param \AppBundle\Entity\Payment\AuthorizedAgency $authorizedAgency
     */
    public function removeAuthorizedAgency(\AppBundle\Entity\Payment\AuthorizedAgency $authorizedAgency)
    {
        $this->authorizedAgency->removeElement($authorizedAgency);
    }

    /**
     * Get authorizedAgency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizedAgency()
    {
        return $this->authorizedAgency;
    }

    /**
     * Add advantagesDiscount
     *
     * @param \AppBundle\Entity\GroupAdvantageDiscount $advantagesDiscount
     *
     * @return Group
     */
    public function addAdvantagesDiscount(\AppBundle\Entity\GroupAdvantageDiscount $advantagesDiscount)
    {
        $advantagesDiscount->setGroup($this);
        $this->advantagesDiscount[] = $advantagesDiscount;

        return $this;
    }

    /**
     * Remove advantagesDiscount
     *
     * @param \AppBundle\Entity\GroupAdvantageDiscount $advantagesDiscount
     */
    public function removeAdvantagesDiscount(\AppBundle\Entity\GroupAdvantageDiscount $advantagesDiscount)
    {
        $this->advantagesDiscount->removeElement($advantagesDiscount);
    }

    /**
     * Get advantagesDiscount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvantagesDiscount()
    {
        return $this->advantagesDiscount;
    }

    /**
     * Set mailjetId
     *
     * @param integer $mailjetId
     *
     * @return Group
     */
    public function setMailjetId($mailjetId)
    {
        $this->mailjetId = $mailjetId;

        return $this;
    }

    /**
     * Get mailjetId
     *
     * @return integer
     */
    public function getMailjetId()
    {
        return $this->mailjetId;
    }

    /**
     * Set adRewrite
     *
     * @param \AppBundle\Entity\AdRewrite $adRewrite
     *
     * @return Group
     */
    public function setAdRewrite(\AppBundle\Entity\AdRewrite $adRewrite = null)
    {
        $adRewrite->setGroup($this);
        $this->adRewrite = $adRewrite;

        return $this;
    }

    /**
     * Get adRewrite
     *
     * @return \AppBundle\Entity\AdRewrite
     */
    public function getAdRewrite()
    {
        return $this->adRewrite;
    }

    /**
     * Set emailAlias
     *
     * @param string $emailAlias
     *
     * @return Group
     */
    public function setEmailAlias($emailAlias)
    {
        $this->emailAlias = $emailAlias;

        return $this;
    }

    /**
     * Get emailAlias
     *
     * @return string
     */
    public function getEmailAlias()
    {
        return $this->emailAlias;
    }

    /**
     * Set emailAliasId
     *
     * @param integer $emailAliasId
     *
     * @return Group
     */
    public function setEmailAliasId($emailAliasId)
    {
        $this->emailAliasId = $emailAliasId;

        return $this;
    }

    /**
     * Get emailAliasId
     *
     * @return integer
     */
    public function getEmailAliasId()
    {
        return $this->emailAliasId;
    }

    /**
     * Set adMentionLegals
     *
     * @param \AppBundle\Entity\AdMentionLegals $adMentionLegals
     *
     * @return Group
     */
    public function setAdMentionLegals(\AppBundle\Entity\AdMentionLegals $adMentionLegals = null)
    {
        $this->adMentionLegals = $adMentionLegals;

        return $this;
    }

    /**
     * Get adMentionLegals
     *
     * @return \AppBundle\Entity\AdMentionLegals
     */
    public function getAdMentionLegals()
    {
        return $this->adMentionLegals;
    }

    /**
     * Set oneMonthPrice
     *
     * @param float $oneMonthPrice
     *
     * @return Group
     */
    public function setOneMonthPrice($oneMonthPrice)
    {
        $this->oneMonthPrice = $oneMonthPrice;

        return $this;
    }

    /**
     * Get oneMonthPrice
     *
     * @return float
     */
    public function getOneMonthPrice()
    {
        return $this->oneMonthPrice;
    }

    /**
     * Set sixMonthPrice
     *
     * @param float $sixMonthPrice
     *
     * @return Group
     */
    public function setSixMonthPrice($sixMonthPrice)
    {
        $this->sixMonthPrice = $sixMonthPrice;

        return $this;
    }

    /**
     * Get sixMonthPrice
     *
     * @return float
     */
    public function getSixMonthPrice()
    {
        return $this->sixMonthPrice;
    }

    /**
     * Set oneYearPrice
     *
     * @param float $oneYearPrice
     *
     * @return Group
     */
    public function setOneYearPrice($oneYearPrice)
    {
        $this->oneYearPrice = $oneYearPrice;

        return $this;
    }

    /**
     * Get oneYearPrice
     *
     * @return float
     */
    public function getOneYearPrice()
    {
        return $this->oneYearPrice;
    }

    /**
     * Set threeMonthPrice
     *
     * @param float $threeMonthPrice
     *
     * @return Group
     */
    public function setThreeMonthPrice($threeMonthPrice)
    {
        $this->threeMonthPrice = $threeMonthPrice;

        return $this;
    }

    /**
     * Get threeMonthPrice
     *
     * @return float
     */
    public function getThreeMonthPrice()
    {
        return $this->threeMonthPrice;
    }

    /**
     * Set trialStart
     *
     * @param \DateTime $trialStart
     *
     * @return Group
     */
    public function setTrialStart($trialStart)
    {
        $this->trialStart = $trialStart;
        //Trial end 1 Month Later
        $end = (clone $trialStart)->add(new \DateInterval('P1M'));
        $this->trialEnd = $end;
        return $this;
    }

    /**
     * Get trialStart
     *
     * @return \DateTime
     */
    public function getTrialStart()
    {
        return $this->trialStart;
    }

    /**
     * Set trialEnd
     *
     * @param \DateTime $trialEnd
     *
     * @return Group
     */
    public function setTrialEnd($trialEnd)
    {
        $this->trialEnd = $trialEnd;

        return $this;
    }

    /**
     * Get trialEnd
     *
     * @return \DateTime
     */
    public function getTrialEnd()
    {
        return $this->trialEnd;
    }

    /**
     * Set periodStart
     *
     * @param \DateTime $periodStart
     *
     * @return Group
     */
    public function setPeriodStart($periodStart)
    {
        $this->periodStart = $periodStart;

        return $this;
    }

    /**
     * Get periodStart
     *
     * @return \DateTime
     */
    public function getPeriodStart()
    {
        return $this->periodStart;
    }

    /**
     * Set periodEnd
     *
     * @param \DateTime $periodEnd
     *
     * @return Group
     */
    public function setPeriodEnd($periodEnd)
    {
        $this->periodEnd = $periodEnd;

        return $this;
    }

    /**
     * Get periodEnd
     *
     * @return \DateTime
     */
    public function getPeriodEnd()
    {
        return $this->periodEnd;
    }

    public function getSubscribeIsEnded()
    {
        $now       = new \DateTime('now');
        $days = 0;
        if(is_null($this->periodEnd) && !is_null($this->trialEnd))
            $days      = $now->diff($this->trialEnd)->days;
        elseif(!is_null($this->periodEnd))
            $days      = $now->diff($this->periodEnd)->days;

        if($days <= 15)
            return $days;
        return false;
    }

    /**
     * Set suiviOption
     *
     * @param boolean $suiviOption
     *
     * @return Group
     */
    public function setSuiviOption($suiviOption)
    {
        $this->suivi_option = $suiviOption;

        return $this;
    }

    /**
     * Get suiviOption
     *
     * @return boolean
     */
    public function getSuiviOption()
    {
        return $this->suivi_option;
    }

    /**
     * @return bool
     */
    public function isDiffusionOption()
    {
        return $this->diffusionOption;
    }

    /**
     * @param bool $diffusionOption
     */
    public function setDiffusionOption($diffusionOption)
    {
        $this->diffusionOption = $diffusionOption;
    }


}
