<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Debug\Exception\UndefinedMethodException;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Validator\Constraints;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaisonRepository")
 * @ORM\Table(name="appartement")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Appartement extends BienImmobilier
{

	public function __construct()
	{
		parent::__construct();
	}
    const KITCHEN_TYPES = [
        1 => 'Aucune',
        2 => 'Américaine',
        3 => 'Séparée',
        4 => 'Industrielle',
        5 => 'Coin cuisine',
        6 => 'Américaine équipée',
        7 => 'Séparée équipée',
        8 => 'Coin cuisine équipé',
        9 => 'Équipée'
    ];

    const REVETEMENT_SOL = [
        'béton ciré'        => 'béton ciré',
        'carrelage'         => 'carrelage',
        'fibre végétale'    => 'fibre végétale',
        'linoléum'          => 'linoléum',
        'moquette'          => 'moquette',
        'PVC'               => 'PVC',
        'marbre'            => 'marbre',
        'parquet'           => 'parquet',
        'parquet contrecollé' => 'parquet contrecollé',
        'parquet flottant'    => 'parquet flottant',
        'parquet massif'      => 'parquet massif',
        'parquet stratifié'   => 'parquet stratifié',
        'pierre naturelle'    => 'pierre naturelle',
        'tomette'             => 'tomette',
        'vinyle'              => 'vinyle'
    ];

    const TYPES = [
        'T1'  => 'T1',
        'T1 bis'  => 'T1 bis',
        'T2'      => 'T2',
        'T3'      => 'T3',
        'T4'      => 'T4',
        'T5'      => 'T5',
        'T6 et plus'  => 'T6 et plus'
    ];

    /**
     * Indicate the king of kitchen
     * @var integer
     *
     * @ORM\Column(name="string" ,length=255, nullable=true)
     */
    protected $type;

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $lotNumber;

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbEtages;
	
	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbPieces;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbChambres;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbSalleBain;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salle_eau", type="boolean", nullable=false)
	 */
	protected $siSalleEau;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_ascenseur", type="boolean", nullable=true)
	 */
	protected $siAscenseur;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salon", type="boolean", nullable=true)
	 */
	protected $siSalon;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_garage", type="boolean", nullable=true)
	 */
	protected $siGarage;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $calme = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $belleVue = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $adresseBulleDeVente;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $vueSurMer = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_volets_roulants_electriques", type="boolean", nullable=true)
     */
    protected $siVoletsRoulantsElectriques = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_tv", type="boolean", nullable=false)
     */
    protected $siTV;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_PMR", type="boolean", nullable=false)
     */
    protected $siPMR;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_Piscine", type="boolean", nullable=false)
     */
    protected $siPiscine;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbSalleEau;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_parking", type="boolean", nullable=false)
	 */
	protected $siParking;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbParking;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_box", type="boolean", nullable=false)
	 */
	protected $siBox;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbBox;
	
	/**
	 * Indicate the total size of the parking
	 * @var float
	 *
	 * @ORM\Column(name="surface_parking", type="float", nullable=true)
	 */
	protected $surfaceParking;


	/////////////////////// Les Champs non requis ///////////////////////////
    /**
     * @var string
     * @ORM\Column(name="environnement", type="string", length=20, nullable=true)
     */
    protected $environnement;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_toilete", type="boolean", nullable=true)
	 */
	protected $siToilette;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbToilette;

	/**
	 * Indicate if the property has a living room
	 * @var boolean
	 *
	 * @ORM\Column(name="si_sejour", type="boolean", nullable=true)
	 */
	protected $siSejour;

	/**
	 * @var float
	 * @ORM\Column(name="surface_sejour", type="float", nullable=true)
	 */
	protected $surfaceSejour;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salle_a_manger", type="boolean", nullable=true)
	 */
	protected $siSalleAManger;

	/**
	 * Indicate the area of the dining room
	 * @var float
	 *
	 * @ORM\Column(name="surface_salle_a_manger", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceSalleAManger;

	/**
	 * Indicate the king of kitchen
	 * @var integer
	 *
	 * @ORM\Column(name="type_cuisine", type="integer",length=1, nullable=true)
	 */
	protected $typeCuisine;

	/**
	 * Indicate if the property is an office building
	 * @var boolean
	 *
	 * @ORM\Column(name="si_bureau", type="boolean", nullable=true)
	 */
	protected $siBureau;

	/**
	 * Indicate area of an office (only for offices)
	 * @var float
	 *
	 * @ORM\Column(name="surface_bureau", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBureau;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_buanderie", type="boolean", nullable=true)
	 */
	protected $siBuanderie;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_buanderie", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBuanderie;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_dressing", type="boolean", nullable=true)
	 */
	protected $siDressing;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_dressing", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceDressing;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_terrasse", type="boolean", nullable=true)
	 */
	protected $siTerrasse;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_terrasse", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceTerrasse;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbTerrasse;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_balcons", type="boolean", nullable=true)
	 */
	protected $siBalcons;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_balcons", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBalcons;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbBalcons;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_jardin", type="boolean", nullable=true)
	 */
	protected $siJardin;

    /**
     * @var string
     *
     * @ORM\Column(name="type_jardin", type="string", nullable=true)
     */
    protected $typeJardin;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_duplex", type="boolean", nullable=true)
	 */
	protected $siDuplex;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_cave", type="boolean", nullable=true)
	 */
	protected $siCave;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_cave", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceCave;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_veranda", type="boolean", nullable=true)
	 */
	protected $siVeranda;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_veranda", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceVeranda;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_cheminee", type="boolean", nullable=true)
	 */
	protected $siCheminee;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_cheminee", type="integer", nullable=true)
     */
    protected $numberCheminee;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_placards", type="boolean", nullable=true)
	 */
	protected $siPlacards;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_placards", type="integer", nullable=true)
     */
    protected $numberPlacards;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $dpeValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dpe", type="string",length=2, nullable=true)
	 */
	protected $dpe;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $gesValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ges", type="string",length=2, nullable=true)
	 */
	protected $ges;

	/**
	 * genre carrelage, etc
	 * @var string
	 *
	 * @ORM\Column(name="revetement_sol", type="string", length=48, nullable=true)
	 */
	protected $revetementDeSol;

	/*Sécurité / Protection*/
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_gardien", type="boolean", nullable=true)
	 */
	protected $siGardien;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_video", type="boolean", nullable=true)
	 */
	protected $siVideophone;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_interphone", type="boolean", nullable=true)
	 */
	protected $siInterphone;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_digicode", type="boolean", nullable=true)
	 */
	protected $siDigicode;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_alarme", type="boolean", nullable=true)
	 */
	protected $siAlarme;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_loggia", type="boolean", nullable=true)
     */
    protected $siLoggia = false;

    /**
     * @var string
     * @ORM\Column(name="type_de_chauffage", type="integer", length=6, nullable=true)
     */
    protected $typeDeChauffage;

    /**
     * @var string
     * @ORM\Column(name="energie_de_chauffage", type="string", length=20, nullable=true)
     */
    protected $energieDeChauffage;

    /**
     * @var string
     *
     * @ORM\Column(name="vitrage", type="string", length=48, nullable=true)
     */
    protected $vitrage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vis_a_vis", type="boolean", nullable=true)
     */
    protected $vis_a_vis = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $degagement = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="toit_Terrasse", type="boolean", nullable=true)
     */
    protected $toitTerrasse;

    /**
     * @var string
     *
     * @ORM\Column(name="grenier", type="string", length=48)
     */
    protected $grenier;

    public function fromArray(array $data) {
        foreach($data as $key => $field){
            $keyUp = ucfirst($key);
            if(method_exists($this,'set'.$keyUp)){
                call_user_func(array($this,'set'.$keyUp),$field);
            } else {
                throw new UndefinedMethodException(get_called_class() . '->set' . $keyUp . ' is undefined.', new \ErrorException());
            }
        }
        return $this;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function clearId()
    {
        return $this->id = null;
    }

    /**
     * Set nbChambres
     *
     * @param integer $nbChambres
     *
     * @return Appartement
     */
    public function setNbChambres($nbChambres)
    {
        $this->nbChambres = $nbChambres;

        return $this;
    }

    /**
     * Get nbChambres
     *
     * @return integer
     */
    public function getNbChambres()
    {
        return $this->nbChambres;
    }

    /**
     * Set nbSalleBain
     *
     * @param integer $nbSalleBain
     *
     * @return Maison
     */
    public function setNbSalleBain($nbSalleBain)
    {
        $this->nbSalleBain = $nbSalleBain;

        return $this;
    }

    /**
     * Get nbSalleBain
     *
     * @return integer
     */
    public function getNbSalleBain()
    {
        return $this->nbSalleBain;
    }

    /**
     * Set siSalleEau
     *
     * @param array|boolean $SalleEau
     *
     * @return Maison
     */
    public function setSiSalleEau($SalleEau)
    {
        $this->siSalleEau = $SalleEau['si'];
        $this->nbSalleEau = $SalleEau['number'];

        return $this;
    }

    /**
     * Get siSalleEau
     *
     * @return boolean
     */
    public function getSiSalleEau()
    {
		return [
			'si' => $this->siSalleEau,
			'number' => $this->nbSalleEau
		];
    }
    /**
     * Get siSalleEau
     *
     * @return boolean
     */
    public function getSiSalleEauValue()
    {
		return $this->siSalleEau;
    }

    /**
     * Set nbSalleEau
     *
     * @param integer $nbSalleEau
     *
     * @return Maison
     */
    public function setNbSalleEau($nbSalleEau)
    {
        $this->nbSalleEau = $nbSalleEau;

        return $this;
    }

    /**
     * Get nbSalleEau
     *
     * @return integer
     */
    public function getNbSalleEau()
    {
        return $this->nbSalleEau;
    }

	/**
	 * Set siParking
	 *
	 * @param boolean $siParking
	 *
	 * @return Maison
	 */
	public function setSiParkingValue($siParking)
	{
		$this->siParking 	  = $siParking;
		return $this;
	}

    /**
     * Set siParking
     *
     * @param boolean $siParking
     *
     * @return Maison
     */
    public function setSiParking($siParking)
    {
        $this->siParking 	  = $siParking['si'];
        $this->nbParking 	  = $siParking['number'];
        $this->surfaceParking = $siParking['surface'];

        return $this;
    }

    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParking()
    {
		return [
			'si'		=> $this->siParking,
			'number'	=> $this->nbParking,
			'surface'	=>$this->surfaceParking
		];
    }
    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParkingValue()
    {
		return $this->siParking;
    }

    /**
     * Set nbParking
     *
     * @param integer $nbParking
     *
     * @return Maison
     */
    public function setNbParking($nbParking)
    {
        $this->nbParking = $nbParking;

        return $this;
    }

    /**
     * Get nbParking
     *
     * @return integer
     */
    public function getNbParking()
    {
        return $this->nbParking;
    }

    /**
     * Set surfaceParking
     *
     * @param float $surfaceParking
     *
     * @return Maison
     */
    public function setSurfaceParking($surfaceParking)
    {
        $this->surfaceParking = $surfaceParking;

        return $this;
    }

    /**
     * Get surfaceParking
     *
     * @return float
     */
    public function getSurfaceParking()
    {
        return $this->surfaceParking;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Maison
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Maison
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Maison
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set siToilette
     *
     * @param array $Toilette
     *
     * @return Maison
     */
    public function setSiToilette($Toilette)
    {
        $this->siToilette = $Toilette['si'];
        $this->nbToilette = $Toilette['number'];

        return $this;
    }

    /**
     * Get siToilette
     *
     * @return boolean
     */
    public function getSiToilette()
    {
        return [
			'si'		=> $this->siToilette,
			'number'	=> $this->nbToilette
		];
    }
    /**
     * Get siToilette
     *
     * @return boolean
     */
    public function getSiToiletteValue()
    {
        return $this->siToilette;
    }

    /**
     * Set nbToilette
     *
     * @param integer $nbToilette
     *
     * @return Maison
     */
    public function setNbToilette($nbToilette)
    {
        $this->nbToilette = $nbToilette;

        return $this;
    }

    /**
     * Get nbToilette
     *
     * @return integer
     */
    public function getNbToilette()
    {
        return $this->nbToilette;
    }

    /**
     * Set siSejour
     *
     * @param boolean $siSejour
     *
     * @return Maison
     */
    public function setSiSejour($siSejour)
    {
        $this->siSejour 	 = $siSejour['si'];
        $this->surfaceSejour = $siSejour['surface'];

        return $this;
    }

    /**
     * Get siSejour
     *
     * @return boolean
     */
    public function getSiSejour()
    {
        return [
			'si'		=> $this->siSejour,
			'surface'	=> $this->surfaceSejour
		];
    }
    /**
     * Get siSejour
     *
     * @return boolean
     */
    public function getSiSejourValue()
    {
        return $this->siSejour;
    }

    /**
     * Set surfaceSejour
     *
     * @param float $surfaceSejour
     *
     * @return Maison
     */
    public function setSurfaceSejour($surfaceSejour)
    {
        $this->surfaceSejour = $surfaceSejour;

        return $this;
    }

    /**
     * Get surfaceSejour
     *
     * @return float
     */
    public function getSurfaceSejour()
    {
        return $this->surfaceSejour;
    }

    /**
     * Set siSalleAManger
     *
     * @param array $SalleAManger
     *
     * @return Maison
     */
    public function setSiSalleAManger($SalleAManger)
    {
        $this->siSalleAManger 	   = $SalleAManger['si'];
        $this->surfaceSalleAManger = $SalleAManger['surface'];

        return $this;
    }

    /**
     * Get siSalleAManger
     *
     * @return boolean
     */
    public function getSiSalleAManger()
    {
        return [
			'si'		=> $this->siSalleAManger,
			'surface'	=>$this->surfaceSalleAManger
		];
    }
    /**
     * Get siSalleAManger
     *
     * @return boolean
     */
    public function getSiSalleAMangerValue()
    {
        return $this->siSalleAManger;
    }

    /**
     * Set surfaceSalleAManger
     *
     * @param float $surfaceSalleAManger
     *
     * @return Maison
     */
    public function setSurfaceSalleAManger($surfaceSalleAManger)
    {
        $this->surfaceSalleAManger = $surfaceSalleAManger;

        return $this;
    }

    /**
     * Get surfaceSalleAManger
     *
     * @return float
     */
    public function getSurfaceSalleAManger()
    {
        return $this->surfaceSalleAManger;
    }

    /**
     * Set typeCuisine
     *
     * @param string $typeCuisine
     *
     * @return Maison
     */
    public function setTypeCuisine($typeCuisine)
    {
        $this->typeCuisine = $typeCuisine;

        return $this;
    }

    /**
     * Get typeCuisine
     *
     * @return string
     */
    public function getTypeCuisine()
    {
        return $this->typeCuisine;
    }

    /**
     * Get typeCuisine
     *
     * @return string
     */
    public function getTypeCuisineValue()
    {
        return self::KITCHEN_TYPES[$this->typeCuisine];
    }

    /**
     * Set siBureau
     *
     * @param array $Bureau
     *
     * @return Maison
     */
    public function setSiBureau($Bureau)
    {
        $this->siBureau 	 = $Bureau['si'];
        $this->surfaceBureau = $Bureau['surface'];

        return $this;
    }

    /**
     * Get siBureau
     *
     * @return boolean
     */
    public function getSiBureau()
    {
        return [
			'si'		=> $this->siBureau,
			'surface'	=> $this->surfaceBureau
		];
    }

    /**
     * Get siBureau
     *
     * @return boolean
     */
    public function getSiBureauValue()
    {
        return $this->siBureau;
    }

    /**
     * Set surfaceBureau
     *
     * @param float $surfaceBureau
     *
     * @return Maison
     */
    public function setSurfaceBureau($surfaceBureau)
    {
        $this->surfaceBureau = $surfaceBureau;

        return $this;
    }

    /**
     * Get surfaceBureau
     *
     * @return float
     */
    public function getSurfaceBureau()
    {
        return $this->surfaceBureau;
    }

    /**
     * Set siBuanderie
     *
     * @param array $Buanderie
     *
     * @return Maison
     */
    public function setSiBuanderie($Buanderie)
    {
        $this->siBuanderie 	    = $Buanderie['si'];
        $this->surfaceBuanderie = $Buanderie['surface'];

        return $this;
    }

    /**
     * Get siBuanderie
     *
     * @return boolean
     */
    public function getSiBuanderie()
    {
        return [
			'si'		=> $this->siBuanderie,
			'surface'	=>$this->surfaceBuanderie
		];
    }
    /**
     * Get siBuanderie
     *
     * @return boolean
     */
    public function getSiBuanderieValue()
    {
        return $this->siBuanderie;
    }

    /**
     * Set surfaceBuanderie
     *
     * @param float $surfaceBuanderie
     *
     * @return Maison
     */
    public function setSurfaceBuanderie($surfaceBuanderie)
    {
        $this->surfaceBuanderie = $surfaceBuanderie;

        return $this;
    }

    /**
     * Get surfaceBuanderie
     *
     * @return float
     */
    public function getSurfaceBuanderie()
    {
        return $this->surfaceBuanderie;
    }

    /**
     * Set siDressing
     *
     * @param array $Dressing
     *
     * @return Maison
     */
    public function setSiDressing($Dressing)
    {
        $this->siDressing 	   = $Dressing['si'];
        $this->surfaceDressing = $Dressing['surface'];

        return $this;
    }

    /**
     * Get siDressing
     *
     * @return boolean
     */
    public function getSiDressing()
    {
        return [
			'si'		=> $this->siDressing,
			'surface'	=> $this->surfaceDressing
		];
    }
    /**
     * Get siDressing
     *
     * @return boolean
     */
    public function getSiDressingValue()
    {
        return $this->siDressing;
    }

    /**
     * Set surfaceDressing
     *
     * @param float $surfaceDressing
     *
     * @return Maison
     */
    public function setSurfaceDressing($surfaceDressing)
    {
        $this->surfaceDressing = $surfaceDressing;

        return $this;
    }

    /**
     * Get surfaceDressing
     *
     * @return float
     */
    public function getSurfaceDressing()
    {
        return $this->surfaceDressing;
    }


	/**
	 * Set siTerrasse
	 *
	 * @param boolean $Terrasse
	 *
	 * @return Maison
	 */
	public function setSiTerrasseValue($Terrasse)
	{
		$this->siTerrasse 	   = $Terrasse;
		return $this;
	}

    /**
     * Set siTerrasse
     *
     * @param array $Terrasse
     *
     * @return Maison
     */
    public function setSiTerrasse($Terrasse)
    {
        $this->siTerrasse 	   = $Terrasse['si'];
        $this->nbTerrasse 	   = $Terrasse['number'];
        $this->surfaceTerrasse = $Terrasse['surface'];

        return $this;
    }

    /**
     * Get siTerrasse
     *
     * @return boolean
     */
    public function getSiTerrasse()
    {
        return [
			'si'		=> $this->siTerrasse,
			'number'	=> $this->nbTerrasse,
			'surface'	=> $this->surfaceTerrasse
		];
    }

    /**
     * Get siTerrasse
     *
     * @return boolean
     */
    public function getSiTerrasseValue()
    {
        return $this->siTerrasse;
    }

    /**
     * Set surfaceTerrasse
     *
     * @param float $surfaceTerrasse
     *
     * @return Maison
     */
    public function setSurfaceTerrasse($surfaceTerrasse)
    {
        $this->surfaceTerrasse = $surfaceTerrasse;

        return $this;
    }

    /**
     * Get surfaceTerrasse
     *
     * @return float
     */
    public function getSurfaceTerrasse()
    {
        return $this->surfaceTerrasse;
    }

    /**
     * Set nbTerrasse
     *
     * @param integer $nbTerrasse
     *
     * @return Maison
     */
    public function setNbTerrasse($nbTerrasse)
    {
        $this->nbTerrasse = $nbTerrasse;

        return $this;
    }

    /**
     * Get nbTerrasse
     *
     * @return integer
     */
    public function getNbTerrasse()
    {
        return $this->nbTerrasse;
    }

	/**
	 * Set siBalcons
	 *
	 * @param boolean $Balcons
	 *
	 * @return Maison
	 */
	public function setSiBalconsValue($Balcons)
	{
		$this->siBalcons 	  = $Balcons;

		return $this;
	}

    /**
     * Set siBalcons
     *
     * @param array $Balcons
     *
     * @return Maison
     */
    public function setSiBalcons($Balcons)
    {
        $this->siBalcons 	  = $Balcons['si'];
        $this->nbBalcons 	  = $Balcons['number'];
        $this->surfaceBalcons = $Balcons['surface'];

        return $this;
    }

    /**
     * Get siBalcons
     *
     * @return boolean
     */
    public function getSiBalcons()
    {
        return [
			'si'		=> $this->siBalcons,
			'number'	=> $this->nbBalcons,
			'surface'	=> $this->surfaceBalcons
		];
    }

    /**
     * Get siBalcons
     *
     * @return boolean
     */
    public function getSiBalconsValue()
    {
        return $this->siBalcons;
    }

    /**
     * Set surfaceBalcons
     *
     * @param float $surfaceBalcons
     *
     * @return Maison
     */
    public function setSurfaceBalcons($surfaceBalcons)
    {
        $this->surfaceBalcons = $surfaceBalcons;

        return $this;
    }

    /**
     * Get surfaceBalcons
     *
     * @return float
     */
    public function getSurfaceBalcons()
    {
        return $this->surfaceBalcons;
    }

    /**
     * Set nbBalcons
     *
     * @param integer $nbBalcons
     *
     * @return Maison
     */
    public function setNbBalcons($nbBalcons)
    {
        $this->nbBalcons = $nbBalcons;

        return $this;
    }

    /**
     * Get nbBalcons
     *
     * @return integer
     */
    public function getNbBalcons()
    {
        return $this->nbBalcons;
    }


	/**
	 * Set siCave
	 *
	 * @param boolean $Cave
	 *
	 * @return Maison
	 */
	public function setSiCaveValue($Cave)
	{
		$this->siCave 	   = $Cave;

		return $this;
	}
	
    /**
     * Set siCave
     *
     * @param array $Cave
     *
     * @return Maison
     */
    public function setSiCave($Cave)
    {
        $this->siCave 	   = $Cave['si'];
        $this->surfaceCave = $Cave['surface'];

        return $this;
    }

    /**
     * Get siCave
     *
     * @return boolean
     */
    public function getSiCave()
    {
        return [
			'si'		=> $this->siCave,
			'surface'	=>$this->surfaceCave
		];
    }
    /**
     * Get siCave
     *
     * @return boolean
     */
    public function getSiCaveValue()
    {
        return $this->siCave;
    }

    /**
     * Set surfaceCave
     *
     * @param float $surfaceCave
     *
     * @return Maison
     */
    public function setSurfaceCave($surfaceCave)
    {
        $this->surfaceCave = $surfaceCave;

        return $this;
    }

    /**
     * Get surfaceCave
     *
     * @return float
     */
    public function getSurfaceCave()
    {
        return $this->surfaceCave;
    }

    /**
     * Set siVeranda
     *
     * @param array $Veranda
     *
     * @return Maison
     */
    public function setSiVeranda($Veranda)
    {
        $this->siVeranda 	  = $Veranda['si'];
        $this->surfaceVeranda = $Veranda['surface'];

        return $this;
    }

    /**
     * Get siVeranda
     *
     * @return boolean
     */
    public function getSiVeranda()
    {
        return [
			'si'		=> $this->siVeranda,
			'surface'	=> $this->surfaceVeranda
		];
    }
    /**
     * Get siVeranda
     *
     * @return boolean
     */
    public function getSiVerandaValue()
    {
        return $this->siVeranda;
    }

    /**
     * Set surfaceVeranda
     *
     * @param float $surfaceVeranda
     *
     * @return Maison
     */
    public function setSurfaceVeranda($surfaceVeranda)
    {
        $this->surfaceVeranda = $surfaceVeranda;

        return $this;
    }

    /**
     * Get surfaceVeranda
     *
     * @return float
     */
    public function getSurfaceVeranda()
    {
        return $this->surfaceVeranda;
    }

    /**
     * Set siCheminee
     *
     * @param array $Cheminee
     *
     * @return Maison
     */
    public function setSiCheminee($Cheminee)
    {
        $this->siCheminee 	   = $Cheminee['si'];
        $this->numberCheminee = $Cheminee['number'];

        return $this;
    }

    /**
     * Get siCheminee
     *
     * @return boolean
     */
    public function getSiCheminee()
    {
        return [
			'si'		=> $this->siCheminee,
            'number'	=> $this->numberCheminee
		];
    }

    /**
     * Get siCheminee
     *
     * @return boolean
     */
    public function getSiChemineeValue()
    {
        return $this->siCheminee;
    }

    /**
     * Set numberCheminee
     *
     * @param integer $numberCheminee
     *
     * @return Appartement
     */
    public function setNumberCheminee($numberCheminee)
    {
        $this->numberCheminee = $numberCheminee;

        return $this;
    }

    /**
     * Get numberCheminee
     *
     * @return integer
     */
    public function getNumberCheminee()
    {
        return $this->numberCheminee;
    }


	/**
	 * Set siPlacards
	 *
	 * @param boolean $Placards
	 *
	 * @return Maison
	 */
	public function setSiPlacardsValue($Placards)
	{
		$this->siPlacards 	   = $Placards;
		return $this;
	}

    /**
     * Set siPlacards
     *
     * @param array $Placards
     *
     * @return Maison
     */
    public function setSiPlacards($Placards)
    {
        $this->siPlacards 	   = $Placards['si'];
        $this->numberPlacards = $Placards['number'];

        return $this;
    }

    /**
     * Get siPlacards
     *
     * @return boolean
     */
    public function getSiPlacards()
    {
        return [
			'si'		=> $this->siPlacards,
			'number'	=> $this->numberPlacards
		];
    }

    /**
     * Get siPlacards
     *
     * @return boolean
     */
    public function getSiPlacardsValue()
    {
        return $this->siPlacards;
    }

    /**
     * Set numberPlacards
     *
     * @param integer $numberPlacards
     *
     * @return Maison
     */
    public function setNumberPlacards($numberPlacards)
    {
        $this->numberPlacards = $numberPlacards;

        return $this;
    }

    /**
     * Get numberPlacards
     *
     * @return integer
     */
    public function getNumberPlacards()
    {
        return $this->numberPlacards;
    }

    /**
     * Set dpe
     *
     * @param string $dpe
     *
     * @return Maison
     */
    public function setDPE($dpe)
    {
        $this->dpe = $dpe;

        return $this;
    }

    /**
     * Get dpe
     *
     * @return string
     */
    public function getDPE()
    {
        return $this->dpe;
    }
    /**
     * Set ges
     *
     * @param string $ges
     *
     * @return Maison
     */
    public function setGES($ges)
    {
        $this->ges = $ges;

        return $this;
    }

    /**
     * Get ges
     *
     * @return string
     */
    public function getGES()
    {
        return $this->ges;
    }
    /**
     * Set revetementDeSol
     *
     * @param string $revetementDeSol
     *
     * @return Maison
     */
    public function setRevetementDeSol($revetementDeSol)
    {
        $this->revetementDeSol = $revetementDeSol;

        return $this;
    }

    /**
     * Get revetementDeSol
     *
     * @return string
     */
    public function getRevetementDeSol()
    {
        return $this->revetementDeSol;
    }

    /**
     * @return string
     */
    public function getRevetementDeSolValue()
    {
        if(array_key_exists($this->revetementDeSol, self::REVETEMENT_SOL))
            return self::REVETEMENT_SOL[$this->revetementDeSol];
        else
            return "";
    }

	public function __clone() {
		if ($this->id) {
			$this->clearId();
			$this->createdAt = new \DateTime();
			$this->updatedAt = new \DateTime();
		}
	}

    /**
     * Set dpeValue
     *
     * @param integer $dpeValue
     *
     * @return Maison
     */
    public function setDpeValue($dpeValue)
    {
        $this->dpeValue = $dpeValue;

        return $this;
    }

    /**
     * Get dpeValue
     *
     * @return integer
     */
    public function getDpeValue()
    {
        return $this->dpeValue;
    }

    /**
     * Set gesValue
     *
     * @param integer $gesValue
     *
     * @return Maison
     */
    public function setGesValue($gesValue)
    {
        $this->gesValue = $gesValue;

        return $this;
    }

    /**
     * Get gesValue
     *
     * @return integer
     */
    public function getGesValue()
    {
        return $this->gesValue;
    }

    /**
     * Set titreImagePrincipale
     *
     * @param string $titreImagePrincipale
     *
     * @return Maison
     */
    public function setTitreImagePrincipale($titreImagePrincipale)
    {
        $this->titreImagePrincipale = $titreImagePrincipale;

        return $this;
    }

    /**
     * Get titreImagePrincipale
     *
     * @return string
     */
    public function getTitreImagePrincipale()
    {
        return $this->titreImagePrincipale;
    }

    /**
     * Set nbEtages
     *
     * @param integer $nbEtages
     *
     * @return Appartement
     */
    public function setNbEtages($nbEtages)
    {
        $this->nbEtages = $nbEtages;

        return $this;
    }

    /**
     * Get nbEtages
     *
     * @return integer
     */
    public function getNbEtages()
    {
        return $this->nbEtages;
    }

    /**
     * Set siAscenseur
     *
     * @param boolean $siAscenseur
     *
     * @return Appartement
     */
    public function setSiAscenseur($siAscenseur)
    {
        $this->siAscenseur = $siAscenseur;

        return $this;
    }

    /**
     * Get siAscenseur
     *
     * @return boolean
     */
    public function getSiAscenseur()
    {
        return $this->siAscenseur;
    }

    /**
     * Set siJardin
     *
     * @param boolean $siJardin
     *
     * @return Appartement
     */
    public function setSiJardin($siJardin)
    {
        $this->siJardin = $siJardin;

        return $this;
    }

    /**
     * Get siJardin
     *
     * @return boolean
     */
    public function getSiJardin()
    {
        return $this->siJardin;
    }

    /**
     * Set siAlarme
     *
     * @param boolean $siAlarme
     *
     * @return Appartement
     */
    public function setSiAlarme($siAlarme)
    {
        $this->siAlarme = $siAlarme;

        return $this;
    }

    /**
     * Get siAlarme
     *
     * @return boolean
     */
    public function getSiAlarme()
    {
        return $this->siAlarme;
    }

    /**
     * Set siGardien
     *
     * @param boolean $siGardien
     *
     * @return Appartement
     */
    public function setSiGardien($siGardien)
    {
        $this->siGardien = $siGardien;

        return $this;
    }

    /**
     * Get siGardien
     *
     * @return boolean
     */
    public function getSiGardien()
    {
        return $this->siGardien;
    }

    /**
     * Set siVideophone
     *
     * @param boolean $siVideophone
     *
     * @return Appartement
     */
    public function setSiVideophone($siVideophone)
    {
        $this->siVideophone = $siVideophone;

        return $this;
    }

    /**
     * Get siVideophone
     *
     * @return boolean
     */
    public function getSiVideophone()
    {
        return $this->siVideophone;
    }

    /**
     * Set siInterphone
     *
     * @param boolean $siInterphone
     *
     * @return Appartement
     */
    public function setSiInterphone($siInterphone)
    {
        $this->siInterphone = $siInterphone;

        return $this;
    }

    /**
     * Get siInterphone
     *
     * @return boolean
     */
    public function getSiInterphone()
    {
        return $this->siInterphone;
    }

    /**
     * Set siDigicode
     *
     * @param boolean $siDigicode
     *
     * @return Appartement
     */
    public function setSiDigicode($siDigicode)
    {
        $this->siDigicode = $siDigicode;

        return $this;
    }

    /**
     * Get siDigicode
     *
     * @return boolean
     */
    public function getSiDigicode()
    {
        return $this->siDigicode;
    }

    /**
     *
     * @param boolean $Box
     *
     * @return Appartement
     */
    public function setSiBoxValue($Box)
    {
        $this->siBox 	   = $Box;
        return $this;
    }

    /**
     * Set siDressing
     *
     * @param array $Box
     *
     * @return Appartement
     */
    public function setSiBox($Box)
    {
        $this->siBox 	   = $Box['si'];
        $this->nbBox       = $Box['number'];

        return $this;
    }

    /**
     * Get siBox
     *
     * @return boolean
     */
    public function getSiBox()
    {
        return [
            'si'		=> $this->siBox,
            'number'	=> $this->nbBox
        ];
    }

    /**
     * Get siBoxValue
     *
     * @return boolean
     */
    public function getSiBoxValue()
    {
        return $this->siBox;
    }

    /**
     * Set nbBox
     *
     * @param integer $nbBox
     *
     * @return Appartement
     */
    public function setNbBox($nbBox)
    {
        $this->nbBox = $nbBox;

        return $this;
    }

    /**
     * Get nbBox
     *
     * @return integer
     */
    public function getNbBox()
    {
        return $this->nbBox;
    }

    /**
     * Set siDuplex
     *
     * @param boolean $siDuplex
     *
     * @return Appartement
     */
    public function setSiDuplex($siDuplex)
    {
        $this->siDuplex = $siDuplex;

        return $this;
    }

    /**
     * Get siDuplex
     *
     * @return boolean
     */
    public function getSiDuplex()
    {
        return $this->siDuplex;
    }

    /**
     * Set nbPieces
     *
     * @param integer $nbPieces
     *
     * @return Appartement
     */
    public function setNbPieces($nbPieces)
    {
        $this->nbPieces = $nbPieces;

        return $this;
    }

    /**
     * Get nbPieces
     *
     * @return integer
     */
    public function getNbPieces()
    {
        return $this->nbPieces;
    }

    public function calculatePercent()
    {
        $proprety = [$this->nbPieces, $this->siJardin, $this->nbBox, $this->siBox, $this->gesValue, $this->dpeValue, $this->revetementDeSol, $this->ges, $this->dpe, $this->numberPlacards, $this->siPlacards,
            $this->siCheminee, $this->numberCheminee, $this->siVeranda, $this->surfaceVeranda, $this->surfaceCave, $this->siCave, $this->siBalcons, $this->nbBalcons, $this->surfaceBalcons, $this->siTerrasse, $this->nbTerrasse,
            $this->nbTerrasse, $this->siDressing, $this->surfaceDressing, $this->siBuanderie, $this->surfaceBuanderie, $this->siBureau, $this->surfaceBureau, $this->typeCuisine,
            $this->siSalleAManger, $this->surfaceSalleAManger,$this->siSejour, $this->surfaceSejour, $this->siToilette, $this->nbToilette,
            $this->nbBox, $this->siParking, $this->nbParking, $this->surfaceParking, $this->siSalleEau, $this->nbSalleEau,
            $this->nbSalleBain, $this->nbChambres, $this->surface, $this->siDuplex, $this->siAlarme, $this->siAscenseur, $this->siGardien, $this->siDigicode, $this->siInterphone, $this->siVideophone
        ];
        $percent  = 0;
        $part     = 100/count($proprety);
        foreach ($proprety as $item) {
            if(!is_null($item) && $item !== '')
                $percent += $part;
        }
        return $percent;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Appartement
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set typeDeChauffage
     *
     * @param integer $typeDeChauffage
     *
     * @return Appartement
     */
    public function setTypeDeChauffage($typeDeChauffage)
    {
        $this->typeDeChauffage = $typeDeChauffage;

        return $this;
    }

    /**
     * Get typeDeChauffage
     *
     * @return integer
     */
    public function getTypeDeChauffage()
    {
        return $this->typeDeChauffage;
    }

    /**
     * Set energieDeChauffage
     *
     * @param string $energieDeChauffage
     *
     * @return Appartement
     */
    public function setEnergieDeChauffage($energieDeChauffage)
    {
        $this->energieDeChauffage = $energieDeChauffage;

        return $this;
    }

    /**
     * Get energieDeChauffage
     *
     * @return string
     */
    public function getEnergieDeChauffage()
    {
        return $this->energieDeChauffage;
    }

    /**
     * Set vitrage
     *
     * @param string $vitrage
     *
     * @return Appartement
     */
    public function setVitrage($vitrage)
    {
        $this->vitrage = $vitrage;

        return $this;
    }

    /**
     * Get vitrage
     *
     * @return string
     */
    public function getVitrage()
    {
        return $this->vitrage;
    }

    /**
     * Set typeJardin
     *
     * @param string $typeJardin
     *
     * @return Appartement
     */
    public function setTypeJardin($typeJardin)
    {
        $this->typeJardin = $typeJardin;

        return $this;
    }

    /**
     * Get typeJardin
     *
     * @return string
     */
    public function getTypeJardin()
    {
        return $this->typeJardin;
    }

    /**
     * Set siLoggia
     *
     * @param boolean $siLoggia
     *
     * @return Appartement
     */
    public function setSiLoggia($siLoggia)
    {
        $this->siLoggia = $siLoggia;

        return $this;
    }

    /**
     * Get siLoggia
     *
     * @return boolean
     */
    public function getSiLoggia()
    {
        return $this->siLoggia;
    }

    /**
     * Set visAVis
     *
     * @param boolean $visAVis
     *
     * @return Appartement
     */
    public function setVisAVis($visAVis)
    {
        $this->vis_a_vis = $visAVis;

        return $this;
    }

    /**
     * Get visAVis
     *
     * @return boolean
     */
    public function getVisAVis()
    {
        return $this->vis_a_vis;
    }

    /**
     * Set degagement
     *
     * @param boolean $degagement
     *
     * @return Appartement
     */
    public function setDegagement($degagement)
    {
        $this->degagement = $degagement;

        return $this;
    }

    /**
     * Get degagement
     *
     * @return boolean
     */
    public function getDegagement()
    {
        return $this->degagement;
    }

    /**
     * Set toitTerrasse
     *
     * @param boolean $toitTerrasse
     *
     * @return Appartement
     */
    public function setToitTerrasse($toitTerrasse)
    {
        $this->toitTerrasse = $toitTerrasse;

        return $this;
    }

    /**
     * Get toitTerrasse
     *
     * @return boolean
     */
    public function getToitTerrasse()
    {
        return $this->toitTerrasse;
    }

    /**
     * Set grenier
     *
     * @param string $grenier
     *
     * @return Appartement
     */
    public function setGrenier($grenier)
    {
        $this->grenier = $grenier;

        return $this;
    }

    /**
     * Get grenier
     *
     * @return string
     */
    public function getGrenier()
    {
        return $this->grenier;
    }

    /**
     * Set siSalon
     *
     * @param boolean $siSalon
     *
     * @return Appartement
     */
    public function setSiSalon($siSalon)
    {
        $this->siSalon = $siSalon;

        return $this;
    }

    /**
     * Get siSalon
     *
     * @return boolean
     */
    public function getSiSalon()
    {
        return $this->siSalon;
    }

    /**
     * Set siVoletsRoulantsElectriques
     *
     * @param boolean $siVoletsRoulantsElectriques
     *
     * @return Appartement
     */
    public function setSiVoletsRoulantsElectriques($siVoletsRoulantsElectriques)
    {
        $this->siVoletsRoulantsElectriques = $siVoletsRoulantsElectriques;

        return $this;
    }

    /**
     * Get siVoletsRoulantsElectriques
     *
     * @return boolean
     */
    public function getSiVoletsRoulantsElectriques()
    {
        return $this->siVoletsRoulantsElectriques;
    }

    /**
     * Set siGarage
     *
     * @param boolean $siGarage
     *
     * @return Appartement
     */
    public function setSiGarage($siGarage)
    {
        $this->siGarage = $siGarage;

        return $this;
    }

    /**
     * Get siGarage
     *
     * @return boolean
     */
    public function getSiGarage()
    {
        return $this->siGarage;
    }

    /**
     * Set calme
     *
     * @param boolean $calme
     *
     * @return Appartement
     */
    public function setCalme($calme)
    {
        $this->calme = $calme;

        return $this;
    }

    /**
     * Get calme
     *
     * @return boolean
     */
    public function getCalme()
    {
        return $this->calme;
    }

    /**
     * Set belleVue
     *
     * @param string $belleVue
     *
     * @return Appartement
     */
    public function setBelleVue($belleVue)
    {
        $this->belleVue = $belleVue;

        return $this;
    }

    /**
     * Get belleVue
     *
     * @return string
     */
    public function getBelleVue()
    {
        if(is_string($this->belleVue))
            return $this->belleVue === "1"  ? true : false;
        return $this->belleVue;
    }

    /**
     * Set vueSurMer
     *
     * @param boolean $vueSurMer
     *
     * @return Appartement
     */
    public function setVueSurMer($vueSurMer)
    {
        $this->vueSurMer = $vueSurMer;

        return $this;
    }

    /**
     * Get vueSurMer
     *
     * @return boolean
     */
    public function getVueSurMer()
    {
        return $this->vueSurMer;
    }

    /**
     * Set siTV
     *
     * @param boolean $siTV
     *
     * @return Appartement
     */
    public function setSiTV($siTV)
    {
        $this->siTV = $siTV;

        return $this;
    }

    /**
     * Get siTV
     *
     * @return boolean
     */
    public function getSiTV()
    {
        return $this->siTV;
    }

    /**
     * Set siPMR
     *
     * @param boolean $siPMR
     *
     * @return Appartement
     */
    public function setSiPMR($siPMR)
    {
        $this->siPMR = $siPMR;

        return $this;
    }

    /**
     * Get siPMR
     *
     * @return boolean
     */
    public function getSiPMR()
    {
        return $this->siPMR;
    }

    /**
     * Set siPiscine
     *
     * @param boolean $siPiscine
     *
     * @return Appartement
     */
    public function setSiPiscine($siPiscine)
    {
        $this->siPiscine = $siPiscine;

        return $this;
    }

    /**
     * Get siPiscine
     *
     * @return boolean
     */
    public function getSiPiscine()
    {
        return $this->siPiscine;
    }

    /**
     * Set environnement
     *
     * @param string $environnement
     *
     * @return Appartement
     */
    public function setEnvironnement($environnement)
    {
        $this->environnement = $environnement;

        return $this;
    }

    /**
     * Get environnement
     *
     * @return string
     */
    public function getEnvironnement()
    {
        return $this->environnement;
    }

    /**
     * Set adresseBulleDeVente
     *
     * @param string $adresseBulleDeVente
     *
     * @return Appartement
     */
    public function setAdresseBulleDeVente($adresseBulleDeVente)
    {
        $this->adresseBulleDeVente = $adresseBulleDeVente;

        return $this;
    }

    /**
     * Get adresseBulleDeVente
     *
     * @return string
     */
    public function getAdresseBulleDeVente()
    {
        return $this->adresseBulleDeVente;
    }

    /**
     * Set lotNumber
     *
     * @param integer $lotNumber
     *
     * @return Appartement
     */
    public function setLotNumber($lotNumber)
    {
        $this->lotNumber = $lotNumber;

        return $this;
    }

    /**
     * Get lotNumber
     *
     * @return integer
     */
    public function getLotNumber()
    {
        return $this->lotNumber;
    }
}
