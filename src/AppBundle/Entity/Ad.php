<?php

namespace AppBundle\Entity;

use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdRepository")
 * @ORM\Table(name="ad")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Ad implements EqualityComparable, Comparable
{

	const TYPE_DUO     = 'duo';
	const TYPE_PROJECT = 'projet';
	const TYPE_TERRAIN = 'terrain';
	const TYPE_PROGRAM = 'program';

	const TYPES_LOTS = array(
        'terrain'     => 'Terrain',
        'maison'  	   => 'Maison',
        'appartement'   => 'Appartement',
    );

	const TYPES_ADS = array(
        'terrain'     => 'Terrain seul',
        'projet'  	   => 'Maison & Terrain'
    );

	const STATUS = array(
        'en-attente' => 'En attente de validation',
        'valider'  	 => 'Validé',
        'reserver'   => 'Réservé',
        'vendu'      => 'Vendu',
        'suspendu'   => 'Suspendu',
        'annuler'    => 'Annuler'
    );

	const STATUS_ADS = array(
        'en-attente' => 'En attente de validation',
        'valider'  	 => 'Validé',
        'suspendu'   => 'Suspendu',
        'annuler'    => 'Annuler'
    );

    const PENDING  = 'en-attente';
    const VALIDATE = 'valider';
    const ANNULER  = 'annuler';
    const VENDU    = 'vendu';
    const RESERVER = 'reserver';
    const SUSPENDU = 'suspendu';
    const CONSOMATION = [
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
        'E' => 'E',
        'F' => 'F',
        'G' => 'G',
        'VI' => 'Vierge',
        'NS' => 'Non soumis'
    ];

	public function __construct()
	{
		$this->createdAt = new \DateTime();
		$this->dateRemonter = new \DateTime("now");
		$this->updatedAt = new \DateTime();
        $this->bienImmobilier = new ArrayCollection();
	}

	/**
	 * Unique id of the Ad
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $importIdentifier;

    /**
     * The owner group of the ad
     * @ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="ad")
     * @JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL") //todo: on delete affect le ad to othe commercial
     */
	protected $owner;

    /**
     * The owner group of the ad
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="ad", cascade={"persist"})
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
	protected $group;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Program", inversedBy="ads", fetch="EAGER", cascade={"remove"}))
     * @JoinColumn(name="program_id", referencedColumnName="id")
     */
	protected $program;

	/**
	 * The creating date of the house model
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="create")
	 */
	protected $createdAt;

	/**
	 * The updating date of the house model
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="update")
	 */
	protected $updatedAt;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 */
	protected $dateRemonter;

    /**
     * @ORM\Column(type="integer", length=3,nullable=false)
     */
    protected $nbRemonte = 0;

	//Annonce Proprety
	/**
	 * @var string
	 *
	 * @ORM\Column(name="libelle", type="string", length=64, nullable=true)
	 */
	protected $libelle;

	/**
     * @ORM\Column(type="string", nullable=true)
	 */
	protected $reference;

	/**
     * @ORM\Column(type="string", nullable=true)
	 */
	protected $referenceV1;

	/**
	 * The name of the ad
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $referenceInterne;

	/**
	 * @ORM\Column(type="string",length=50)
	 */
	protected $pays;

	/**
	 * @ORM\Column(type="string",length=50)
	 */
	protected $ville;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $codePostal;

	/**
	 * The property address
	 * @var string
	 *
	 * @ORM\Column(name="adresse", type="string", length=128, nullable=true)
	 */
	protected $adresse;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lien_visit_virtuel", type="string", length=255, nullable=true)
	 */
	protected $lienVisitVirtuel;

	/**
	 * The price of the ad
	 * @var float
	 *
	 * @ORM\Column(name="prix", type="float", nullable=false)
	 */
	protected $prix;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="percent", type="float", nullable=true)
	 */
	protected $percent;

    /**
     * @ORM\Column(type="text",length=4000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="imagePrincipale", type="string", length=255, nullable=false)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg"}
     * )
     */
    protected $imagePrincipale = "";
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titreImagePrincipale;

	/**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BienImmobilier", mappedBy="ad", cascade={"persist", "remove","merge"}, fetch="EXTRA_LAZY")
	 */
	protected $bienImmobilier;

    /**
     * The owner housemodel of the ad
     * @ManyToOne(targetEntity="AppBundle\Entity\HouseModel",
     *     inversedBy="ad", cascade={"remove"})
     * @JoinColumn(name="houseModel_id", referencedColumnName="id", onDelete="SET NULL")
     */
	protected $houseModel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_housemodel", type="boolean")
     */
    protected $hasHouseModel = false;

	/**
	 * @var enum
	 * @ORM\Column(name="typeDiffusion", type="string", columnDefinition="enum('projet', 'terrain','program')",nullable=false)
	 */
	protected $typeDiffusion;

    /**
     * @ORM\JoinColumn(name="diffusion",referencedColumnName="id", onDelete="CASCADE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Broadcast\Diffusion", cascade={"persist", "remove"}, fetch="EAGER", orphanRemoval=true)
     */
    protected $diffusion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_diffuse", type="boolean")
     */
    protected $programDiffuse = false;

    /**
     * @var string
     * @ORM\Column(name="stat", type="string" ,nullable=false)
     */
    protected $stat;

    /**
     *
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="ad",cascade={"persist"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_phone", type="string", length=14, nullable=true)
     */
    protected $trackingPhone;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Statistique\OpportunityStatistique", mappedBy="ad")
     */
    protected $opportunityStatistique;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Ad
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Ad
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set nbRemonte
     *
     * @param integer $nbRemonte
     *
     * @return Ad
     */
    public function setNbRemonte($nbRemonte)
    {
        $this->nbRemonte = $nbRemonte;

        return $this;
    }

    public function addNbRemonte()
    {
        $this->nbRemonte++;

        return $this;
    }

    /**
     * Get nbRemonte
     *
     * @return integer
     */
    public function getNbRemonte()
    {
        return $this->nbRemonte;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Ad
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set referenceInterne
     *
     * @param string $referenceInterne
     *
     * @return Ad
     */
    public function setReferenceInterne($referenceInterne)
    {
        $this->referenceInterne = $referenceInterne;

        return $this;
    }

    /**
     * Get referenceInterne
     *
     * @return string
     */
    public function getReferenceInterne()
    {

        return $this->referenceInterne;
    }
    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Ad
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }


    /**
     * Get reference for datable
     *
     * @return string
     */
    public function getReferenceV2()
    {
        if(isset($this->reference) && $this->reference) return $this->reference;
        if(isset($this->referenceV1)) return $this->referenceV1;
        return $this->referenceInterne;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->getReferenceV2();
    }


    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Ad
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Ad
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Ad
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Ad
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set lienVisitVirtuel
     *
     * @param string $lienVisitVirtuel
     *
     * @return Ad
     */
    public function setLienVisitVirtuel($lienVisitVirtuel)
    {
        $this->lienVisitVirtuel = $lienVisitVirtuel;

        return $this;
    }

    /**
     * Get lienVisitVirtuel
     *
     * @return string
     */
    public function getLienVisitVirtuel()
    {
        return $this->lienVisitVirtuel;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Ad
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Ad
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set typeDiffusion
     *
     * @param string $typeDiffusion
     *
     * @return Ad
     */
    public function setTypeDiffusion($typeDiffusion)
    {
        $this->typeDiffusion = $typeDiffusion;

        return $this;
    }

    /**
     * Get typeDiffusion
     *
     * @return string
     */
    public function getTypeDiffusion()
    {
        return $this->typeDiffusion;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return Ad
     */
    public function setOwner(\AppBundle\Entity\User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Ad
     */
    public function setGroup(\AppBundle\Entity\Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }


    /**
     * Set houseModel
     *
     * @param \AppBundle\Entity\houseModel $houseModel
     *
     * @return Ad
     */
    public function setHouseModel(\AppBundle\Entity\houseModel $houseModel = null)
    {
        $this->houseModel = $houseModel;

        return $this;
    }

    /**
     * Get houseModel
     *
     * @return \AppBundle\Entity\houseModel
     */
    public function getHouseModel()
    {
        return $this->houseModel;
    }

    /**
     * Set diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     *
     * @return Ad
     */
    public function setDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion = null)
    {
        $this->diffusion = $diffusion;

        return $this;
    }

    /**
     * Get diffusion
     *
     * @return \AppBundle\Entity\Broadcast\Diffusion
     */
    public function getDiffusion()
    {
        return $this->diffusion;
    }


    /**
     * Set stat
     *
     * @param string $stat
     *
     * @return Ad
     */
    public function setStat($stat)
    {
        $this->stat = $stat;

        return $this;
    }

    /**
     * Get stat
     *
     * @return string
     */
    public function getStat()
    {
        return $this->stat ? self::STATUS[$this->stat] : '';
    }

    public function getStatValue()
    {
        return $this->stat;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Ad
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }


    public function getImagePrincipale(){ return $this->imagePrincipale; }
    public function setImagePrincipale($imagePrincipale){
        $this->imagePrincipale = $imagePrincipale;
    }

    /**
     * Set titreImagePrincipale
     *
     * @param string $titreImagePrincipale
     *
     * @return Ad
     */
    public function setTitreImagePrincipale($titreImagePrincipale)
    {
        $this->titreImagePrincipale = $titreImagePrincipale;

        return $this;
    }

    /**
     * Get titreImagePrincipale
     *
     * @return string
     */
    public function getTitreImagePrincipale()
    {
        return $this->titreImagePrincipale;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return Ad
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Set program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Ad
     */
    public function setProgram(\AppBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \AppBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * bienImmobilier
     *
     * @return Ad
     */
    public function getBienImmobilier()
    {
        return $this->bienImmobilier;
    }

    /**
     * bienImmobilier
     *
     * @return Ad
     */
    public function setBienImmobilier(ArrayCollection $biensImmobilier)
    {
        $this->bienImmobilier = $biensImmobilier;
        return  $this;
    }


    /**
     * Add bienImmobilier
     *
     * @param \AppBundle\Entity\BienImmobilier $bienImmobilier
     *
     * @return Ad
     */
    public function addBienImmobilier(\AppBundle\Entity\BienImmobilier $bienImmobilier)
    {
        $bienImmobilier->setAd($this);
        $this->bienImmobilier[] = $bienImmobilier;

        return $this;
    }

    /**
     * Remove bienImmobilier
     *
     * @param \AppBundle\Entity\BienImmobilier $bienImmobilier
     */
    public function removeBienImmobilier(\AppBundle\Entity\BienImmobilier $bienImmobilier)
    {
        $this->bienImmobilier->removeElement($bienImmobilier);
    }

    public function existInBienImmobilier($class) {
        foreach($this->bienImmobilier as $bien) {
            if(get_class($bien) == $class) {
                return true;
            }
        }
        return false;
    }
    public function isMaisonOnly() {
        return (count($this->bienImmobilier) == 1) && (get_class($this->bienImmobilier[0]) == Maison::class);
    }
    public function isTerrainOnly() {
        return (count($this->bienImmobilier) == 1) && (get_class($this->bienImmobilier[0]) == Terrain::class);
    }
    public function isProject() {
        return (count($this->bienImmobilier) == 2) && $this->existInBienImmobilier(Terrain::class) && $this->existInBienImmobilier(Maison::class);
    }
    public function isMixte() {
        return (count($this->bienImmobilier) == 2) && $this->existInBienImmobilier(Appartement::class) && $this->existInBienImmobilier(Maison::class);
    }

    public function getTerrain() {
        foreach($this->bienImmobilier as $bien) {
            if(get_class($bien) == Terrain::class) {
                return $bien;
            }
        }
        return null;
    }

    public function getMaison() {
        foreach($this->bienImmobilier as $bien) {
            if(get_class($bien) == Maison::class) {
                return $bien;
            }
        }
        return null;
    }

    public function getAppartement() {
        foreach($this->bienImmobilier as $bien) {
            if(get_class($bien) == Appartement::class) {
                return $bien;
            }
        }
        return null;
    }

    public function equals($o)
    {
        if(!$o instanceof BienImmobilier) {
            return false;
        }
        return $o->getId() == $this->getId();
    }

    public function compareTo($obj)
    {
        if ($obj == null) {
            throw new NullPointerException(${$obj});
        }
        if (!($obj instanceof BienImmobilier)) {
            throw new ClassCastException(${$obj});
        }
        return $this->getId() != $obj->getid() ? false : true;
    }

    public function existIn($aObj)
    {
        foreach ($aObj as $obj) {
            if($this->compareTo($obj)) {
                return true;
            }
        }
        return false;
    }

    public function synchroniseProgram(Program $program) {
        $this->setPays($program->getPays());
        $this->setVille($program->getVille());
        $this->setAdresse($program->getAdresse());
        $this->setCodePostal($program->getCodePostal());
        $this->setDescription($program->getDescription());
        $this->setImagePrincipale($program->getImagePrincipale());
        $this->setTitreImagePrincipale($program->getTitreImagePrincipale());
        $this->setReferenceInterne($program->getReferenceInterne() ? $program->getReferenceInterne().'-'.(count($program->getAds())+1) : '');
        $this->setLibelle($program->getTitle());
        $typeBien    = $program->getType() == 'mixte' ? ($this->isMaisonOnly() ? 'maison' : 'appartement') : $program->getType();
        $this->documents = new ArrayCollection();
        foreach ($program->getDocuments() as $document) {
            $cloneDocument = clone $document;
            $this->addDocument($cloneDocument);
        }
        switch ($typeBien) {
            case 'maison':
                /** @var \AppBundle\Entity\Maison $bien */
                $bien = $this->getMaison();
                $bien->setDPE($program->getDpe());
                $bien->setDpeValue($program->getDpeValue());
                $bien->setGES($program->getGes());
                $bien->setGesValue($program->getGesValue());
                $bien->setRevetementDeSol($program->getRevetementDeSol());
                $bien->setSiPlacardsValue($program->getSiPlacards());
                $bien->setSiParkingValue($program->getSiParkingValue());
                $bien->setNbParking($program->getNbParking());
                $bien->setSiTerrasseValue($program->getSiTerrasse());
                $bien->setSiBalconsValue($program->getSiBalcons());
                $bien->setSiCaveValue($program->getSiCave());
                $bien->setSiAlarme($program->getSiAlarme());
                $bien->setSiPMR($program->getSiPMR());
                $bien->setSiLoggia($program->getSiLoggia());
                $bien->setTypeDeChauffage($program->getTypeDeChauffage());
                $bien->setEnergieDeChauffage($program->getEnergieDeChauffage());
                $bien->setNbEtages($program->getNbetages());
                $bien->setExposition($program->getExposition());
                $bien->setEnvironnement($program->getEnvironnement());
                $bien->setGrenier($program->getGrenier());
                $bien->setVitrage($program->getVitrage());
                $bien->setSiVoletsRoulantsElectriques($program->getSiVoletsRoulantsElectriques());
                $bien->setSiTV($program->getSiTV());
                $bien->setSiPiscine($program->getSiPiscine());
                $bien->setSiPMR($program->getSiPMR());
                break;
            case 'appartement':
                /** @var \AppBundle\Entity\Appartement $bien */
                $bien = $this->getAppartement();
                $bien->setDPE($program->getDpe());
                $bien->setDpeValue($program->getDpeValue());
                $bien->setGES($program->getGes());
                $bien->setGesValue($program->getGesValue());
                $bien->setRevetementDeSol($program->getRevetementDeSol());
                $bien->setSiPlacardsValue($program->getSiPlacards());
                $bien->setSiParkingValue($program->getSiParkingValue());
                $bien->setNbParking($program->getNbParking());
                $bien->setSiTerrasseValue($program->getSiTerrasse());
                $bien->setSiBalconsValue($program->getSiBalcons());
                $bien->setSiCaveValue($program->getSiCave());
                $bien->setNbEtages($program->getNbetages());
                $bien->setSiAlarme($program->getSiAlarme());
                $bien->setSiDigicode($program->getSiDigicode());
                $bien->setSiInterphone($program->getSiInterphone());
                $bien->setSiVideophone($program->getSiVideophone());
                $bien->setSiGardien($program->getSiGardien());
                $bien->setTypeDeChauffage($program->getTypeDeChauffage());
                $bien->setEnergieDeChauffage($program->getEnergieDeChauffage());
                $bien->setSiLoggia($program->getSiLoggia());
                $bien->setSiBoxValue($program->getSiBox());
                $bien->setGrenier($program->getGrenier());
                $bien->setVitrage($program->getVitrage());
                $bien->setSiVoletsRoulantsElectriques($program->getSiVoletsRoulantsElectriques());
                $bien->setSiTV($program->getSiTV());
                $bien->setSiPiscine($program->getSiPiscine());
                $bien->setSiPMR($program->getSiPMR());
                break;
        }
    }

    /**
     * Set programDiffuse
     *
     * @param boolean $programDiffuse
     *
     * @return Ad
     */
    public function setProgramDiffuse($programDiffuse)
    {
        $this->programDiffuse = $programDiffuse;

        return $this;
    }

    /**
     * Get programDiffuse
     *
     * @return boolean
     */
    public function getProgramDiffuse()
    {
        return $this->programDiffuse;
    }

    /**
     * @return Ad
     */
    public function setImportIdentifier($identifier)
    {
        $this->importIdentifier = $identifier;

        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getImportIdentifier()
    {
        return $this->importIdentifier;
    }


    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return Ad
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $document->setAd($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }


    public function setDocuments($documents = null)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPdfDocuments()
    {
        $return = [];
        if($this->documents) {
            foreach ($this->documents as $document) {
                if($document->getMimeType() == 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return $return;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagesDocuments()
    {
        $return = [];
        if($this->documents) {
            foreach ($this->documents as $document) {
                if($document->getMimeType() != 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return is_null($return) || count($return) == 0 ? [] : $return;
    }


    public function calculatePercent()
    {
        $proprety = [$this->libelle,$this->reference,$this->adresse, $this->codePostal, $this->description, $this->ville, $this->lienVisitVirtuel, $this->titreImagePrincipale, $this->imagePrincipale,$this->prix,$this->pays];
        if(count($this->getImagesDocuments()) > 0)  { $proprety[] =  null; } else { $proprety[] = count($this->getImagesDocuments()); }
        if(count($this->getPdfDocuments()) > 0)  { $proprety[] =  null; } else { $proprety[] = count($this->getPdfDocuments()); }

        $percent  = 0;
        $part     = 40/count($proprety);
        foreach ($proprety as $item) {
            if($item)
                $percent += $part;
        }
        $count           = count($this->bienImmobilier);
        $percentageBiens = 0;
        if($count > 0) {
            foreach ($this->bienImmobilier as $bien)
                $percentageBiens += $bien->calculatePercent();
            $percent += (($percentageBiens / $count) / 100) * 60;
        }
        return number_format($percent,2);
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return Ad
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
        $this->libelle = $this->libelle.' (clone)';
        $this->remoteId = null;
        $this->reference = null;
        $this->referenceInterne = null;
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->diffusion = null;
        $biens = $this->bienImmobilier;
        $this->bienImmobilier = new ArrayCollection();
        foreach ($biens as $bien) {
            $cloneBien = clone $bien;
            $cloneBien->setAd($this);
            $this->bienImmobilier->add($cloneBien);
        }
        $documents = $this->documents;
        if(is_array($documents)){
            $this->documents = new ArrayCollection();
            foreach ($documents as $document) {
                $cloneDocument = clone $document;
                $cloneDocument->setAd($this);
                $this->documents->add($cloneDocument);
            }
        }
        $this->stat = self::PENDING;
        $this->published = false;
        $this->id = null;
    }

    public function synchroDuoStep1($request) {
        $parameters = $request->request->get('ad');
        /** @var BienImmobilier $bien */
        foreach ($this->getBienImmobilier() as $bien) {
            $bien->setSiProximiteCommerces(isset($parameters['siProximiteCommerces']));
            $bien->setSiProximiteBus(isset($parameters['siProximiteBus']));
            $bien->setSiProximiteTramway(isset($parameters['siProximiteTramway']));
            $bien->setSiProximiteMetro(isset($parameters['siProximiteMetro']));
            $bien->setSiProximiteGare(isset($parameters['siProximiteGare']));
            $bien->setSiProximiteCreche(isset($parameters['siProximiteCreche']));
            $bien->setSiProximiteEcolePrimaire(isset($parameters['siProximiteEcolePrimaire']));
            $bien->setSiProximiteCollege(isset($parameters['siProximiteCollege']));
            $bien->setSiProximiteLycee(isset($parameters['siProximiteLycee']));
            $bien->setSiProximiteUniversite(isset($parameters['siProximiteUniversite']));
            $bien->setSiProximiteSortieAutoroute(isset($parameters['siProximiteSortieAutoroute']));
            $bien->setSiProximiteAeroport(isset($parameters['siProximiteAeroport']));
            $bien->setSiProximiteParking(isset($parameters['siProximiteParking']));
            $bien->setSiProximiteEspacesVerts(isset($parameters['siProximiteEspacesVerts']));
            $bien->setSiProximiteCentreville(isset($parameters['siProximiteCentreville']));
            $bien->setExposition($parameters['exposition']);
            if($bien instanceof Terrain) {
                if(isset($parameters['belleVue']))
                    $bien->setBelleVue($parameters['belleVue']);
                if(isset($parameters['calme']))
                    $bien->setCalme($parameters['calme']);
            } elseif($bien instanceof Maison) {
                if(isset($parameters['environnement']))
                    $bien->setEnvironnement($parameters['environnement']);
            }
        }
    }

    /**
     * Set hasHouseModel
     *
     * @param boolean $hasHouseModel
     *
     * @return Ad
     */
    public function setHasHouseModel($hasHouseModel)
    {
        $this->hasHouseModel = $hasHouseModel;

        return $this;
    }

    /**
     * Get hasHouseModel
     *
     * @return boolean
     */
    public function getHasHouseModel()
    {
        return $this->hasHouseModel;
    }

    /**
     * Set referenceV1
     *
     * @param string $referenceV1
     *
     * @return Ad
     */
    public function setReferenceV1($referenceV1)
    {
        $this->referenceV1 = $referenceV1;

        return $this;
    }

    /**
     * Get referenceV1
     *
     * @return string
     */
    public function getReferenceV1()
    {
        return $this->referenceV1;
    }

    /**
     * @return string
     */
    public function getTrackingPhone()
    {
        return preg_replace("/^33/", "0", $this->trackingPhone,1);
    }

    /**
     * @param string $trackingPhone
     */
    public function setTrackingPhone($trackingPhone)
    {
        $this->trackingPhone = $trackingPhone;
    }


    /**
     * Add opportunityStatistique
     *
     * @param \AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique
     *
     * @return Ad
     */
    public function addOpportunityStatistique(\AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique)
    {
        $this->opportunityStatistique[] = $opportunityStatistique;

        return $this;
    }

    /**
     * Remove opportunityStatistique
     *
     * @param \AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique
     */
    public function removeOpportunityStatistique(\AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique)
    {
        $this->opportunityStatistique->removeElement($opportunityStatistique);
    }

    /**
     * Get opportunityStatistique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpportunityStatistique()
    {
        return $this->opportunityStatistique;
    }

    /**
     * Set dateRemonter
     *
     * @param \DateTime $dateRemonter
     *
     * @return Ad
     */
    public function setDateRemonter($dateRemonter)
    {
        $this->dateRemonter = $dateRemonter;

        return $this;
    }

    /**
     * Get dateRemonter
     *
     * @return \DateTime
     */
    public function getDateRemonter()
    {
        return $this->dateRemonter;
    }
}
