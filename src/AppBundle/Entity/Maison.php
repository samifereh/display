<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Debug\Exception\UndefinedMethodException;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Validator\Constraints;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaisonRepository")
 * @ORM\Table(name="maison")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Maison extends BienImmobilier
{

	public function __construct()
	{
		parent::__construct();
        $this->siSalleEau = false;
        $this->siParking = false;
        $this->siBox = false;
        $this->typeArchitecture = 0;
        $this->siToilette = false;
        $this->siSejour = false;
        $this->siSalleAManger = false;
	}

	const KITCHEN_TYPES = [
        1 => 'Aucune',
        2 => 'Américaine',
        3 => 'Séparée',
        4 => 'Industrielle',
        5 => 'Coin cuisine',
        6 => 'Américaine équipée',
        7 => 'Séparée équipée',
        8 => 'Coin cuisine équipé',
        9 => 'Équipée'
    ];
	const HOUSE_TYPES = [
        ''              => '---',
        'Plein pied'   => 'Plein pied',
        'Sous-sol complet'  => 'Sous-sol complet',
        'Demi niveau'   => 'Demi niveau',
        'Etage'      => 'Etage'
    ];

    const ARCHITECTURE_TYPES = [
        ''         => '---',
        'villa',
        'Bastide',
        'Bastidon',
        'Bergerie',
        'Cabanon',
        'Chambre de service',
        'Corps de ferme',
        'Demeure',
        'Domaine',
        'Duplex',
        'échoppe',
        'Entrepôt',
        'Exploitation agricole',
        'Exploitation viticole',
        'ferme',
        'Fermette',
        'grange',
        'Île',
        'Immeuble commercial',
        'Immeuble de bureaux',
        'Immeuble mixte',
        "Local d'activités",
        'Local de stockage',
        'Loft',
        'Lotissement',
        'Maison ancienne',
        'Maison basque',
        'Maison charentaise',
        'Maison contemporaine',
        "maison d'architecte",
        'Maison d\'hôte',
        'Maison de loisirs',
        'Maison de maître',
        'Maison de village',
        'Maison en pierre',
        'Maison jumelée',
        'Maison landaise',
        'Maison longère',
        'Maison provençale',
        'Maison traditionnelle',
        'Manoir',
        'Mas',
        'Mazet',
        'Moulin',
        'Pavillon',
        'Programme',
        'Projet (uniquement pour "Construire")',
        'propriété',
        'Propriété de chasse',
        'Propriété équestre',
        'Restauration',
        'Riad',
        'Studette',
        'toulousaine',
        'Triplex',
    ];

    const REVETEMENT_SOL = [
        'béton ciré'        => 'béton ciré',
        'carrelage'         => 'carrelage',
        'fibre végétale'    => 'fibre végétale',
        'linoléum'          => 'linoléum',
        'moquette'          => 'moquette',
        'PVC'               => 'PVC',
        'marbre'            => 'marbre',
        'parquet'           => 'parquet',
        'parquet contrecollé' => 'parquet contrecollé',
        'parquet flottant'    => 'parquet flottant',
        'parquet massif'      => 'parquet massif',
        'parquet stratifié'   => 'parquet stratifié',
        'pierre naturelle'    => 'pierre naturelle',
        'tomette'             => 'tomette',
        'vinyle'              => 'vinyle'
    ];

    const CHAUFFAGE_TYPES = [
        '4096'          => 'Collectif',
        '8192'          => 'individuel'
    ];

    const CHARPENTE_TYPES = [
        'traditionelle'          => 'Traditionelle',
        'fermette'               => 'Fermette',
        'lamelle_colle'          => 'Lamellé collé'
    ];

    const TUILE_TYPES = [
        'tuile_plate'            => 'Tuile plate',
        'tuile_canal'            => 'Tuile canal',
        'tuile_petit_moule'      => 'Tuile petit moule',
        'tuile_grand_moule'      => 'Tuile grand moule',
        'tuile_faible_galbe'     => 'Tuile faible galbe',
        'tuile_à_cote'           => 'Tuile à coté',
        'ardoises'               => 'Ardoises',
        'Ardoises naturelles'    => 'Ardoises naturelles',
        'Ardoises synthétiques'  => 'Ardoises synthétiques',
        'Chaume'                 => 'Chaume',
        'Schiste'                => 'Schiste',
        'Shingle'                => 'Shingle',
        'Terrasse'               => 'Terrasse',
        'Tuiles'                 => 'Tuiles',
        'Tuiles canal'           => 'Tuiles canal',
        'Tuiles champenoises'    => 'Tuiles champenoises',
        'Tuiles mécaniques'      => 'Tuiles mécaniques',
        'Tuiles naturelles'      => 'Tuiles naturelles',
        'Tuiles romaines'        => 'Tuiles romaines',
        'Tôle'                   => 'Tôle',
        'Vegetalisee'            => 'Vegetalisee'
    ];

    const MATERIAUX_TUILE_TYPES = [
        'beton'            => 'Béton',
        'terre_cuite'      => 'Terre cuite'
    ];

    const VITRAGES = [
        'double'                => 'Double',
        'double_triple'         => 'Double & Triple',
        'simple'                => 'Simple',
        'simple_double'         => 'Simple & Double',
        'survitrage'            => 'Survitrage',
        'triple'                => 'Triple',
    ];

    const ENERGIE_CHAUFFAGE = [
        'aquathermie'          => 'Aquathermie',
        'aérothermie'          => 'Aérothermie',
        'biocombustible'       => 'Biocombustible',
        'bois'                 => 'Bois',
        'charbon'              => 'Charbon',
        'climatisation réversible'              => 'Climatisation réversible',
        'fuel'              => 'Fuel',
        'gaz'              => 'Gaz',
        'granulés'              => 'Granulés',
        'géothermie'              => 'Géothermie',
        'pompe à chaleur'              => 'Pompe à chaleur',
        'solaire'              => 'Solaire',
        'éléctricité'              => 'éléctricité',
        'éolienne'              => 'éolienne',
    ];

    const ZINGUERIE = [
        'Galvanisé'          => 'Galvanisé',
        'Zing'          => 'Zing',
        'Aluminuim'       => 'Aluminuim'
    ];

    const MENUISERIE = [
        'PVC'          => 'PVC',
        'Bois'          => 'Bois',
        'Aluminium'       => 'Aluminium'
    ];

    const VOLETS = [
        'Battant bois'          => 'Battant bois',
        'Battant PVC'          => 'Battant PVC',
        'VR manuel'       => 'VR manuel',
        'VR motorisé'       => 'VR motorisé',
        'VR aluminium Manuel'       => 'VR aluminium Manuel',
        'VR aluminium motorisé'       => 'VR aluminium motorisé'
    ];

    const PORTES_GARAGE = [
        'Basculante manuelle'           => 'Basculante manuelle',
        'Basculante motorisée'          => 'Basculante motorisée',
        'Sectionnelle manuelle'         => 'Sectionnelle manuelle',
        'Sectionnelle motorisée'       => 'Sectionnelle motorisée'
    ];

	/**
	 * King of property
	 * @var string
	 *
	 * @ORM\Column(name="typebien", type="string", length=48)
	 */
	protected $type;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $zinguerie;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $menuiserie;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $volets;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $porteGarage;

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbPieces;

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbChambres;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $nbSalleBain;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salle_eau", type="boolean", nullable=false)
	 */
	protected $siSalleEau = false;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbSalleEau;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_parking", type="boolean", nullable=false)
	 */
	protected $siParking = false;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbParking;

    /**
     * Indicate the total size of the parking
     * @var float
     *
     * @ORM\Column(name="surface_parking", type="float", nullable=true)
     */
    protected $surfaceParking;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_box", type="boolean", nullable=false)
	 */
	protected $siBox = false;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbBox;


	/**
	 * @var string
	 *
	 * @ORM\Column(name="type_architecture", type="string", length=255, nullable=true)
	 */
	protected $typeArchitecture;

	/////////////////////// Les Champs non requis ///////////////////////////

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_toilete", type="boolean", nullable=true)
	 */
	protected $siToilette = false;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbToilette;

	/**
	 * Indicate if the property has a living room
	 * @var boolean
	 *
	 * @ORM\Column(name="si_sejour", type="boolean", nullable=true)
	 */
	protected $siSejour = false;

	/**
	 * @var float
	 * @ORM\Column(name="surface_sejour", type="float", nullable=true)
	 */
	protected $surfaceSejour;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salle_a_manger", type="boolean", nullable=true)
	 */
	protected $siSalleAManger = false;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_salon", type="boolean", nullable=true)
	 */
	protected $siSalon = false;

	/**
	 * Indicate the area of the dining room
	 * @var float
	 *
	 * @ORM\Column(name="surface_salle_a_manger", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceSalleAManger;

	/**
	 * Indicate the king of kitchen
	 * @var integer
	 *
	 * @ORM\Column(name="type_cuisine", type="integer",length=1, nullable=true)
	 */
	protected $typeCuisine;

	/**
	 * Indicate if the property is an office building
	 * @var boolean
	 *
	 * @ORM\Column(name="si_bureau", type="boolean", nullable=true)
	 */
	protected $siBureau = false;

	/**
	 * Indicate area of an office (only for offices)
	 * @var float
	 *
	 * @ORM\Column(name="surface_bureau", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBureau;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_buanderie", type="boolean", nullable=true)
	 */
	protected $siBuanderie = false;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_buanderie", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBuanderie;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_dressing", type="boolean", nullable=true)
	 */
	protected $siDressing = false;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_dressing", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceDressing;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_terrasse", type="boolean", nullable=true)
	 */
	protected $siTerrasse = false;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_terrasse", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceTerrasse;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbTerrasse;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_balcons", type="boolean", nullable=true)
	 */
	protected $siBalcons = false;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_balcons", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceBalcons;

	/**
	 * The name of the house model
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $nbBalcons;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_jardin", type="boolean", nullable=true)
	 */
	protected $siJardin = false;

    /**
     * @var string
     *
     * @ORM\Column(name="type_jardin", type="string", nullable=true)
     */
    protected $typeJardin;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_cave", type="boolean", nullable=true)
	 */
	protected $siCave = false;


	
	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_cave", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceCave;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_veranda", type="boolean", nullable=true)
	 */
	protected $siVeranda = false;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="surface_veranda", type="float", precision=10, scale=0, nullable=true)
	 */
	protected $surfaceVeranda;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_cheminee", type="boolean", nullable=true)
	 */
	protected $siCheminee = false;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="number_cheminee", type="integer", nullable=true)
	 */
	protected $numberCheminee;
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_placards", type="boolean", nullable=true)
	 */
	protected $siPlacards = false;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="number_placards", type="integer", nullable=true)
	 */
	protected $numberPlacards;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $dpeValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dpe", type="string",length=2, nullable=true)
	 */
	protected $dpe;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $gesValue;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ges", type="string",length=2, nullable=true)
	 */
	protected $ges;

	/**
	 * genre carrelage, etc
	 * @var string
	 *
	 * @ORM\Column(name="revetement_sol", type="string", length=48, nullable=true)
	 */
	protected $revetementDeSol;

	/*Sécurité / Protection*/
	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="si_alarme", type="boolean", nullable=true)
	 */
	protected $siAlarme = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_gardien", type="boolean", nullable=true)
     */
    protected $siGardien = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_video", type="boolean", nullable=true)
     */
    protected $siVideophone = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_interphone", type="boolean", nullable=true)
     */
    protected $siInterphone = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_digicode", type="boolean", nullable=true)
     */
    protected $siDigicode = false;

    /* Autres */

    /**
     * @var boolean
     * @ORM\Column(name="investisseur", type="boolean", nullable=true)
     */
    protected $investisseur = false;

    /**
     * @var boolean
     * @ORM\Column(name="habitat", type="boolean", nullable=true)
     */
    protected $habitat = false;

    /**
     * @var boolean
     * @ORM\Column(name="prestige", type="boolean", nullable=true)
     */
    protected $prestige = false;

    /**
     * @var string
     * @ORM\Column(name="typologie_de_residence", type="string", length=128, nullable=true)
     */
    protected $typologieResidence = false;

    /**
     * @var string
     * @ORM\Column(name="environnement", type="string", length=20, nullable=true)
     */
    protected $environnement;

    /**
     * @var string
     * @ORM\Column(name="type_de_chauffage", type="integer", length=6, nullable=true)
     */
    protected $typeDeChauffage;

    /**
     * @var string
     * @ORM\Column(name="energie_de_chauffage", type="string", length=20, nullable=true)
     */
    protected $energieDeChauffage;

    /**
     * @var string
     *
     * @ORM\Column(name="vitrage", type="string", length=48, nullable=true)
     */
    protected $vitrage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_loggia", type="boolean", nullable=true)
     */
    protected $siLoggia = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_volets_roulants_electriques", type="boolean", nullable=true)
     */
    protected $siVoletsRoulantsElectriques = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_climatise", type="boolean", nullable=true)
     */
    protected $siClimatise = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_tv", type="boolean", nullable=true)
     */
    protected $siTV = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_PMR", type="boolean", nullable=true)
     */
    protected $siPMR = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_piscine", type="boolean", nullable=true)
     */
    protected $siPiscine = false;

    /*Avantages environnementaux*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siEcoQuartier = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siBioConstruction = false;
    /*Avantages Investissement*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $loiPinel = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $loiDuflot = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $censiBouvard = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $LMP_LMNP = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $loi_Girardin_Paul = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $loi_Malraux = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $loi_Demessine = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $regime_SCPI = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $nue_Propriete = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pret_Locatif_Social = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $PSLA = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $monumentHistorique = false;
    /**
     * Elligible au Prêt à taux zéro +
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $elligiblePret = false;
    /**
     * Bénéficie de la TVA 5,5%
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $tva_1 = false;
    /**
     * Bénéficie de la TVA 7%
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $tva_2 = false;
    /*Certifications et labels*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hauteQualiteEnvironnementale = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $nfLogement = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $labelHabitatEnvironnement = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $THPE = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $HPE_ENR = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $labelBatimentBasseConsommation = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $labelHabitatEnvironnementEffinergie = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $planLocalHabitat = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $RT2012 = false;


    /**
     * @var string
     *
     * @ORM\Column(name="type_charpante", type="string", length=48)
     */
    protected $typeCharpante;

    /**
     * @var string
     *
     * @ORM\Column(name="type_tuile", type="string", length=48)
     */
    protected $typeTuile;

    /**
     * @var string
     *
     * @ORM\Column(name="materiaux_tuile", type="string", length=48)
     */
    protected $materiauxTuile;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $nbEtages;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $degagement = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vis_a_vis", type="boolean", nullable=true)
     */
    protected $vis_a_vis = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="toit_Terrasse", type="boolean", nullable=true)
     */
    protected $toitTerrasse;

    /**
     * @var string
     *
     * @ORM\Column(name="grenier", type="string", length=48)
     */
    protected $grenier;

    public function fromArray(array $data) {
        foreach($data as $key => $field){
            $keyUp = ucfirst($key);
            if(method_exists($this,'set'.$keyUp)){
                call_user_func(array($this,'set'.$keyUp),$field);
            } else {
                throw new UndefinedMethodException(get_called_class() . '->set' . $keyUp . ' is undefined.', new \ErrorException());
            }
        }
        return $this;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function clearId()
    {
        return $this->id = null;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Maison
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getTypeValue()
    {
        return self::HOUSE_TYPES[$this->type];
    }

    /**
     * Set nbChambres
     *
     * @param integer $nbChambres
     *
     * @return Maison
     */
    public function setNbChambres($nbChambres)
    {
        $this->nbChambres = $nbChambres;

        return $this;
    }

    /**
     * Get nbChambres
     *
     * @return integer
     */
    public function getNbChambres()
    {
        return $this->nbChambres;
    }

    /**
     * Set nbSalleBain
     *
     * @param integer $nbSalleBain
     *
     * @return Maison
     */
    public function setNbSalleBain($nbSalleBain)
    {
        $this->nbSalleBain = $nbSalleBain;

        return $this;
    }

    /**
     * Get nbSalleBain
     *
     * @return integer
     */
    public function getNbSalleBain()
    {
        return $this->nbSalleBain;
    }

    /**
     * Set siSalleEau
     *
     * @param array|boolean $SalleEau
     *
     * @return Maison
     */
    public function setSiSalleEau($SalleEau)
    {
        $this->siSalleEau = $SalleEau['si'];
        $this->nbSalleEau = $SalleEau['number'];

        return $this;
    }

    /**
     * Get siSalleEau
     *
     * @return boolean
     */
    public function getSiSalleEau()
    {
		return [
			'si' => $this->siSalleEau,
			'number' => $this->nbSalleEau
		];
    }
    /**
     * Get siSalleEau
     *
     * @return boolean
     */
    public function getSiSalleEauValue()
    {
		return $this->siSalleEau;
    }

    /**
     * Set nbSalleEau
     *
     * @param integer $nbSalleEau
     *
     * @return Maison
     */
    public function setNbSalleEau($nbSalleEau)
    {
        $this->nbSalleEau = $nbSalleEau;

        return $this;
    }

    /**
     * Get nbSalleEau
     *
     * @return integer
     */
    public function getNbSalleEau()
    {
        return $this->nbSalleEau;
    }

    /**
     * Set siParking
     *
     * @param boolean $siParking
     *
     * @return Maison
     */
    public function setSiParkingValue($siParking)
    {
        $this->siParking 	  = $siParking;
        return $this;
    }

    /**
     * Set siParking
     *
     * @param boolean $siParking
     *
     * @return Maison
     */
    public function setSiParking($siParking)
    {
        $this->siParking 	  = $siParking['si'];
        $this->nbParking 	  = $siParking['number'];
        $this->surfaceParking = $siParking['surface'];

        return $this;
    }

    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParking()
    {
		return [
			'si'		=> $this->siParking,
			'number'	=> $this->nbParking,
			'surface'	=>$this->surfaceParking
		];
    }
    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParkingValue()
    {
		return $this->siParking;
    }

    /**
     * Set nbParking
     *
     * @param integer $nbParking
     *
     * @return Maison
     */
    public function setNbParking($nbParking)
    {
        $this->nbParking = $nbParking;

        return $this;
    }

    /**
     * Get nbParking
     *
     * @return integer
     */
    public function getNbParking()
    {
        return $this->nbParking;
    }

    /**
     * Set surfaceParking
     *
     * @param float $surfaceParking
     *
     * @return Maison
     */
    public function setSurfaceParking($surfaceParking)
    {
        $this->surfaceParking = $surfaceParking;

        return $this;
    }

    /**
     * Get surfaceParking
     *
     * @return float
     */
    public function getSurfaceParking()
    {
        return $this->surfaceParking;
    }

    /**
     * Set typeArchitecture
     *
     * @param string $typeArchitecture
     *
     * @return Maison
     */
    public function setTypeArchitecture($typeArchitecture)
    {
        $this->typeArchitecture = $typeArchitecture;

        return $this;
    }

    /**
     * Get typeArchitecture
     *
     * @return string
     */
    public function getTypeArchitecture()
    {
        return $this->typeArchitecture;
    }

    /**
     * Get typeArchitecture
     *
     * @return string
     */
    public function getTypeArchitectureValue()
    {
        return self::ARCHITECTURE_TYPES[$this->typeArchitecture];
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Maison
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Maison
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Maison
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set siToilette
     *
     * @param array $Toilette
     *
     * @return Maison
     */
    public function setSiToilette($Toilette)
    {
        $this->siToilette = $Toilette['si'];
        $this->nbToilette = $Toilette['number'];

        return $this;
    }

    /**
     * Get siToilette
     *
     * @return boolean
     */
    public function getSiToilette()
    {
        return [
			'si'		=> $this->siToilette,
			'number'	=> $this->nbToilette
		];
    }
    /**
     * Get siToilette
     *
     * @return boolean
     */
    public function getSiToiletteValue()
    {
        return $this->siToilette;
    }

    /**
     * Set nbToilette
     *
     * @param integer $nbToilette
     *
     * @return Maison
     */
    public function setNbToilette($nbToilette)
    {
        $this->nbToilette = $nbToilette;

        return $this;
    }

    /**
     * Get nbToilette
     *
     * @return integer
     */
    public function getNbToilette()
    {
        return $this->nbToilette;
    }

    /**
     * Set siSejour
     *
     * @param boolean $siSejour
     *
     * @return Maison
     */
    public function setSiSejour($siSejour)
    {
        $this->siSejour 	 = $siSejour['si'];
        $this->surfaceSejour = $siSejour['surface'];

        return $this;
    }


    public function setSiSejourValue($siSejour)
    {
        $this->siSejour 	 = $siSejour;
        return $this;
    }

    /**
     * Get siSejour
     *
     * @return boolean
     */
    public function getSiSejour()
    {
        return [
			'si'		=> $this->siSejour,
			'surface'	=> $this->surfaceSejour
		];
    }
    /**
     * Get siSejour
     *
     * @return boolean
     */
    public function getSiSejourValue()
    {
        return $this->siSejour;
    }

    /**
     * Set surfaceSejour
     *
     * @param float $surfaceSejour
     *
     * @return Maison
     */
    public function setSurfaceSejour($surfaceSejour)
    {
        $this->surfaceSejour = $surfaceSejour;

        return $this;
    }

    /**
     * Get surfaceSejour
     *
     * @return float
     */
    public function getSurfaceSejour()
    {
        return $this->surfaceSejour;
    }

    /**
     * Set siSalleAManger
     *
     * @param boolean $SalleAManger
     *
     * @return Maison
     */
    public function setSiSalleAMangerValue($SalleAManger)
    {
        $this->siSalleAManger 	   = $SalleAManger;

        return $this;
    }

    public function setSiSalleAManger($SalleAManger)
    {
        $this->siSalleAManger 	   = $SalleAManger['si'];
        $this->surfaceSalleAManger = $SalleAManger['surface'];

        return $this;
    }

    /**
     * Get siSalleAManger
     *
     * @return boolean
     */
    public function getSiSalleAManger()
    {
        return [
			'si'		=> $this->siSalleAManger,
			'surface'	=>$this->surfaceSalleAManger
		];
    }
    /**
     * Get siSalleAManger
     *
     * @return boolean
     */
    public function getSiSalleAMangerValue()
    {
        return $this->siSalleAManger;
    }

    /**
     * Set surfaceSalleAManger
     *
     * @param float $surfaceSalleAManger
     *
     * @return Maison
     */
    public function setSurfaceSalleAManger($surfaceSalleAManger)
    {
        $this->surfaceSalleAManger = $surfaceSalleAManger;

        return $this;
    }

    /**
     * Get surfaceSalleAManger
     *
     * @return float
     */
    public function getSurfaceSalleAManger()
    {
        return $this->surfaceSalleAManger;
    }

    /**
     * Set typeCuisine
     *
     * @param string $typeCuisine
     *
     * @return Maison
     */
    public function setTypeCuisine($typeCuisine)
    {
        $this->typeCuisine = $typeCuisine;

        return $this;
    }

    /**
     * Get typeCuisine
     *
     * @return string
     */
    public function getTypeCuisine()
    {
        return $this->typeCuisine;
    }

    /**
     * Get typeCuisine
     *
     * @return string
     */
    public function getTypeCuisineValue()
    {

        return (array_key_exists($this->typeCuisine, self::KITCHEN_TYPES) ?
                self::KITCHEN_TYPES[$this->typeCuisine] : false
        );
    }

    /**
     * Set siBureau
     *
     * @param array $Bureau
     *
     * @return Maison
     */
    public function setSiBureau($Bureau)
    {
        $this->siBureau 	 = $Bureau['si'];
        $this->surfaceBureau = $Bureau['surface'];

        return $this;
    }

    /**
     * Get siBureau
     *
     * @return boolean
     */
    public function getSiBureau()
    {
        return [
			'si'		=> $this->siBureau,
			'surface'	=> $this->surfaceBureau
		];
    }

    /**
     * Get siBureau
     *
     * @return boolean
     */
    public function getSiBureauValue()
    {
        return $this->siBureau;
    }

    /**
     * Set surfaceBureau
     *
     * @param float $surfaceBureau
     *
     * @return Maison
     */
    public function setSurfaceBureau($surfaceBureau)
    {
        $this->surfaceBureau = $surfaceBureau;

        return $this;
    }

    /**
     * Get surfaceBureau
     *
     * @return float
     */
    public function getSurfaceBureau()
    {
        return $this->surfaceBureau;
    }

    /**
     * Set siBuanderie
     *
     * @param array $Buanderie
     *
     * @return Maison
     */
    public function setSiBuanderie($Buanderie)
    {
        $this->siBuanderie 	    = $Buanderie['si'];
        $this->surfaceBuanderie = $Buanderie['surface'];

        return $this;
    }

    /**
     * Get siBuanderie
     *
     * @return boolean
     */
    public function getSiBuanderie()
    {
        return [
			'si'		=> $this->siBuanderie,
			'surface'	=>$this->surfaceBuanderie
		];
    }
    /**
     * Get siBuanderie
     *
     * @return boolean
     */
    public function getSiBuanderieValue()
    {
        return $this->siBuanderie;
    }

    /**
     * Set surfaceBuanderie
     *
     * @param float $surfaceBuanderie
     *
     * @return Maison
     */
    public function setSurfaceBuanderie($surfaceBuanderie)
    {
        $this->surfaceBuanderie = $surfaceBuanderie;

        return $this;
    }

    /**
     * Get surfaceBuanderie
     *
     * @return float
     */
    public function getSurfaceBuanderie()
    {
        return $this->surfaceBuanderie;
    }

    /**
     * Set siDressing
     *
     * @param array $Dressing
     *
     * @return Maison
     */
    public function setSiDressing($Dressing)
    {
        $this->siDressing 	   = $Dressing['si'];
        $this->surfaceDressing = $Dressing['surface'];

        return $this;
    }

    /**
     * Get siDressing
     *
     * @return boolean
     */
    public function getSiDressing()
    {
        return [
			'si'		=> $this->siDressing,
			'surface'	=> $this->surfaceDressing
		];
    }
    /**
     * Get siDressing
     *
     * @return boolean
     */
    public function getSiDressingValue()
    {
        return $this->siDressing;
    }

    /**
     * Set surfaceDressing
     *
     * @param float $surfaceDressing
     *
     * @return Maison
     */
    public function setSurfaceDressing($surfaceDressing)
    {
        $this->surfaceDressing = $surfaceDressing;

        return $this;
    }

    /**
     * Get surfaceDressing
     *
     * @return float
     */
    public function getSurfaceDressing()
    {
        return $this->surfaceDressing;
    }

    /**
     * Set siTerrasse
     *
     * @param boolean $Terrasse
     *
     * @return Maison
     */
    public function setSiTerrasseValue($Terrasse)
    {
        $this->siTerrasse 	   = $Terrasse;
        return $this;
    }

    /**
     * Set siTerrasse
     *
     * @param array $Terrasse
     *
     * @return Maison
     */
    public function setSiTerrasse($Terrasse)
    {
        $this->siTerrasse 	   = $Terrasse['si'];
        $this->nbTerrasse 	   = $Terrasse['number'];
        $this->surfaceTerrasse = $Terrasse['surface'];

        return $this;
    }

    /**
     * Get siTerrasse
     *
     * @return boolean
     */
    public function getSiTerrasse()
    {
        return [
			'si'		=> $this->siTerrasse,
			'number'	=> $this->nbTerrasse,
			'surface'	=> $this->surfaceTerrasse
		];
    }

    /**
     * Get siTerrasse
     *
     * @return boolean
     */
    public function getSiTerrasseValue()
    {
        return $this->siTerrasse;
    }

    /**
     * Set surfaceTerrasse
     *
     * @param float $surfaceTerrasse
     *
     * @return Maison
     */
    public function setSurfaceTerrasse($surfaceTerrasse)
    {
        $this->surfaceTerrasse = $surfaceTerrasse;

        return $this;
    }

    /**
     * Get surfaceTerrasse
     *
     * @return float
     */
    public function getSurfaceTerrasse()
    {
        return $this->surfaceTerrasse;
    }

    /**
     * Set nbTerrasse
     *
     * @param integer $nbTerrasse
     *
     * @return Maison
     */
    public function setNbTerrasse($nbTerrasse)
    {
        $this->nbTerrasse = $nbTerrasse;

        return $this;
    }

    /**
     * Get nbTerrasse
     *
     * @return integer
     */
    public function getNbTerrasse()
    {
        return $this->nbTerrasse;
    }

    /**
     * Set siBalcons
     *
     * @param boolean $Balcons
     *
     * @return Maison
     */
    public function setSiBalconsValue($Balcons)
    {
        $this->siBalcons 	  = $Balcons;

        return $this;
    }

    /**
     * Set siBalcons
     *
     * @param array $Balcons
     *
     * @return Maison
     */
    public function setSiBalcons($Balcons)
    {
        $this->siBalcons 	  = $Balcons['si'];
        $this->nbBalcons 	  = $Balcons['number'];
        $this->surfaceBalcons = $Balcons['surface'];

        return $this;
    }

    /**
     * Get siBalcons
     *
     * @return boolean
     */
    public function getSiBalcons()
    {
        return [
			'si'		=> $this->siBalcons,
			'number'	=> $this->nbBalcons,
			'surface'	=> $this->surfaceBalcons
		];
    }

    /**
     * Get siBalcons
     *
     * @return boolean
     */
    public function getSiBalconsValue()
    {
        return $this->siBalcons;
    }

    /**
     * Set surfaceBalcons
     *
     * @param float $surfaceBalcons
     *
     * @return Maison
     */
    public function setSurfaceBalcons($surfaceBalcons)
    {
        $this->surfaceBalcons = $surfaceBalcons;

        return $this;
    }

    /**
     * Get surfaceBalcons
     *
     * @return float
     */
    public function getSurfaceBalcons()
    {
        return $this->surfaceBalcons;
    }

    /**
     * Set nbBalcons
     *
     * @param integer $nbBalcons
     *
     * @return Maison
     */
    public function setNbBalcons($nbBalcons)
    {
        $this->nbBalcons = $nbBalcons;

        return $this;
    }

    /**
     * Get nbBalcons
     *
     * @return integer
     */
    public function getNbBalcons()
    {
        return $this->nbBalcons;
    }

    /**
     * Set siCave
     *
     * @param boolean $Cave
     *
     * @return Maison
     */
    public function setSiCaveValue($Cave)
    {
        $this->siCave 	   = $Cave;

        return $this;
    }
    /**
     * Set siCave
     *
     * @param array $Cave
     *
     * @return Maison
     */
    public function setSiCave($Cave)
    {
        $this->siCave 	   = $Cave['si'];
        $this->surfaceCave = $Cave['surface'];

        return $this;
    }

    /**
     * Get siCave
     *
     * @return boolean
     */
    public function getSiCave()
    {
        return [
			'si'		=> $this->siCave,
			'surface'	=>$this->surfaceCave
		];
    }
    /**
     * Get siCave
     *
     * @return boolean
     */
    public function getSiCaveValue()
    {
        return $this->siCave;
    }


	/**
	 * Set siAlarme
	 *
	 * @param boolean $siAlarme
	 *
	 * @return Appartement
	 */
	public function setSiAlarme($siAlarme)
	{
		$this->siAlarme = $siAlarme;

		return $this;
	}

	/**
	 * Get siAlarme
	 *
	 * @return boolean
	 */
	public function getSiAlarme()
	{
		return $this->siAlarme;
	}

    /**
     * Set surfaceCave
     *
     * @param float $surfaceCave
     *
     * @return Maison
     */
    public function setSurfaceCave($surfaceCave)
    {
        $this->surfaceCave = $surfaceCave;

        return $this;
    }

    /**
     * Get surfaceCave
     *
     * @return float
     */
    public function getSurfaceCave()
    {
        return $this->surfaceCave;
    }

    /**
     * Set siVeranda
     *
     * @param array $Veranda
     *
     * @return Maison
     */
    public function setSiVeranda($Veranda)
    {
        $this->siVeranda 	  = $Veranda['si'];
        $this->surfaceVeranda = $Veranda['surface'];

        return $this;
    }

    /**
     * Get siVeranda
     *
     * @return boolean
     */
    public function getSiVeranda()
    {
        return [
			'si'		=> $this->siVeranda,
			'surface'	=> $this->surfaceVeranda
		];
    }
    /**
     * Get siVeranda
     *
     * @return boolean
     */
    public function getSiVerandaValue()
    {
        return $this->siVeranda;
    }

    /**
     * Set surfaceVeranda
     *
     * @param float $surfaceVeranda
     *
     * @return Maison
     */
    public function setSurfaceVeranda($surfaceVeranda)
    {
        $this->surfaceVeranda = $surfaceVeranda;

        return $this;
    }

    /**
     * Get surfaceVeranda
     *
     * @return float
     */
    public function getSurfaceVeranda()
    {
        return $this->surfaceVeranda;
    }

    /**
     * Set siCheminee
     *
     * @param boolean $Cheminee
     *
     * @return Maison
     */
    public function setSiChemineeValue($Cheminee)
    {
        $this->siCheminee 	   = $Cheminee;

        return $this;
    }

    /**
     * Set siCheminee
     *
     * @param array $Cheminee
     *
     * @return Maison
     */
    public function setSiCheminee($Cheminee)
    {
        $this->siCheminee 	   = $Cheminee['si'];
        $this->numberCheminee = $Cheminee['number'];

        return $this;
    }

    /**
     * Get siCheminee
     *
     * @return boolean
     */
    public function getSiCheminee()
    {
        return [
			'si'		=> $this->siCheminee,
			'number'	=> $this->numberCheminee
		];
    }

    /**
     * Get siCheminee
     *
     * @return boolean
     */
    public function getSiChemineeValue()
    {
        return $this->siCheminee;
    }

    /**
     * Set numberCheminee
     *
     * @param integer $numberCheminee
     *
     * @return Appartement
     */
    public function setNumberCheminee($numberCheminee)
    {
        $this->numberCheminee = $numberCheminee;

        return $this;
    }

    /**
     * Get numberCheminee
     *
     * @return integer
     */
    public function getNumberCheminee()
    {
        return $this->numberCheminee;
    }

    /**
     * Set siPlacards
     *
     * @param array $Placards
     *
     * @return Maison
     */
    public function setSiPlacards($Placards)
    {
        $this->siPlacards 	   = $Placards['si'];
        $this->numberPlacards = $Placards['number'];

        return $this;
    }

    /**
     * Set siPlacards
     *
     * @param boolean $Placards
     *
     * @return Maison
     */
    public function setSiPlacardsValue($Placards)
    {
        $this->siPlacards 	   = $Placards;
        return $this;
    }

    /**
     * Get siPlacards
     *
     * @return boolean
     */
    public function getSiPlacards()
    {
        return [
			'si'		=> $this->siPlacards,
			'number'	=> $this->numberPlacards
		];
    }

    /**
     * Get siPlacards
     *
     * @return boolean
     */
    public function getSiPlacardsValue()
    {
        return $this->siPlacards;
    }

    /**
     * Set numberPlacards
     *
     * @param float $numberPlacards
     *
     * @return Maison
     */
    public function setNumberPlacards($numberPlacards)
    {
        $this->numberPlacards = $numberPlacards;

        return $this;
    }

    /**
     * Get numberPlacards
     *
     * @return float
     */
    public function getNumberPlacards()
    {
        return $this->numberPlacards;
    }

    /**
     * Set dpe
     *
     * @param string $dpe
     *
     * @return Maison
     */
    public function setDPE($dpe)
    {
        $this->dpe = $dpe;

        return $this;
    }

    /**
     * Get dpe
     *
     * @return string
     */
    public function getDPE()
    {
        return $this->dpe;
    }
    /**
     * Set ges
     *
     * @param string $ges
     *
     * @return Maison
     */
    public function setGES($ges)
    {
        $this->ges = $ges;

        return $this;
    }

    /**
     * Get ges
     *
     * @return string
     */
    public function getGES()
    {
        return $this->ges;
    }
    /**
     * Set revetementDeSol
     *
     * @param string $revetementDeSol
     *
     * @return Maison
     */
    public function setRevetementDeSol($revetementDeSol)
    {
        $this->revetementDeSol = $revetementDeSol;

        return $this;
    }

    /**
     * Get revetementDeSol
     *
     * @return string
     */
    public function getRevetementDeSol()
    {
        return $this->revetementDeSol;
    }

    /**
     * @return string
     */
    public function getRevetementDeSolValue()
    {
        if(!$this->revetementDeSol)
            return null;
        return self::REVETEMENT_SOL[$this->revetementDeSol];
    }

	public function __clone() {
		if ($this->id) {
			$this->clearId();
			$this->createdAt = new \DateTime();
			$this->updatedAt = new \DateTime();
		}
	}

    /**
     * Set dpeValue
     *
     * @param integer $dpeValue
     *
     * @return Maison
     */
    public function setDpeValue($dpeValue)
    {
        $this->dpeValue = $dpeValue;

        return $this;
    }

    /**
     * Get dpeValue
     *
     * @return integer
     */
    public function getDpeValue()
    {
        return $this->dpeValue;
    }

    /**
     * Set gesValue
     *
     * @param integer $gesValue
     *
     * @return Maison
     */
    public function setGesValue($gesValue)
    {
        $this->gesValue = $gesValue;

        return $this;
    }

    /**
     * Get gesValue
     *
     * @return integer
     */
    public function getGesValue()
    {
        return $this->gesValue;
    }

    /**
     * Set siDressing
     *
     * @param array $Box
     *
     * @return Maison
     */
    public function setSiBox($Box)
    {
        $this->siBox 	   = $Box['si'];
        $this->nbBox       = $Box['number'];

        return $this;
    }

    /**
     * Get siBox
     *
     * @return boolean
     */
    public function getSiBox()
    {
        return [
            'si'		=> $this->siBox,
            'number'	=> $this->nbBox
        ];
    }

    /**
     * Get siBoxValue
     *
     * @return boolean
     */
    public function getSiBoxValue()
    {
        return $this->siBox;
    }


    /**
     * Set siJardin
     *
     * @param boolean $siJardin
     *
     * @return Maison
     */
    public function setSiJardin($siJardin)
    {
        $this->siJardin = $siJardin;

        return $this;
    }

    /**
     * Get siJardin
     *
     * @return boolean
     */
    public function getSiJardin()
    {
        return $this->siJardin;
    }

    /**
     * Set nbPieces
     *
     * @param integer $nbPieces
     *
     * @return Maison
     */
    public function setNbPieces($nbPieces)
    {
        $this->nbPieces = $nbPieces;

        return $this;
    }

    /**
     * Get nbPieces
     *
     * @return integer
     */
    public function getNbPieces()
    {
        return $this->nbPieces;
    }

    public function calculatePercent()
    {
        $proprety = [$this->nbPieces, $this->siBox, $this->gesValue, $this->dpeValue, $this->revetementDeSol, $this->ges, $this->dpe,$this->siPlacards,
            $this->siCheminee,$this->siCave, $this->siBalcons,$this->siTerrasse, $this->siDressing, $this->siBuanderie, $this->siBureau,
            $this->typeCuisine, $this->siSalleAManger, $this->siSejour, $this->siToilette, $this->typeArchitecture, $this->siParking,
            $this->siSalleEau, $this->nbSalleBain, $this->nbChambres, $this->surface, $this->type,
        ];
        if($this->siBox)  { $proprety[] =  $this->nbBox; }
        if($this->siPlacards)  { $proprety[] =  $this->numberPlacards; }
        if($this->siCheminee)  { $proprety[] =  $this->numberCheminee; }
        if($this->siVeranda)  { $proprety[] =  $this->surfaceVeranda; }
        if($this->siCave)  { $proprety[] =  $this->surfaceCave; }
        if($this->siBalcons)  { $proprety[] =  $this->nbBalcons;$proprety[] =  $this->surfaceBalcons; }
        if($this->siTerrasse)  { $proprety[] =  $this->nbTerrasse; }
        if($this->siDressing)  { $proprety[] =  $this->surfaceDressing; }
        if($this->siBuanderie)  { $proprety[] =  $this->surfaceBuanderie; }
        if($this->siBureau)  { $proprety[] =  $this->surfaceBureau; }
        if($this->siSalleAManger)  { $proprety[] =  $this->surfaceSalleAManger; }
        if($this->siSejour)  { $proprety[] =  $this->surfaceSejour; }
        if($this->siToilette)  { $proprety[] =  $this->nbToilette; }
        if($this->siSalleEau)  { $proprety[] =  $this->nbSalleEau; }
        if($this->siParking)  { $proprety[] =  $this->nbParking; $proprety[] = $this->surfaceParking; }

        $percent  = 0;
        $part     = 100/count($proprety);
        foreach ($proprety as $item) {
            if(!is_null($item) && $item !== '')
                $percent += $part;
        }

        return $percent;
    }

    /**
     * Set nbBox
     *
     * @param integer $nbBox
     *
     * @return Maison
     */
    public function setNbBox($nbBox)
    {
        $this->nbBox = $nbBox;

        return $this;
    }

    /**
     * Get nbBox
     *
     * @return integer
     */
    public function getNbBox()
    {
        return $this->nbBox;
    }

    /**
     * Set siGardien
     *
     * @param boolean $siGardien
     *
     * @return Maison
     */
    public function setSiGardien($siGardien)
    {
        $this->siGardien = $siGardien;

        return $this;
    }

    /**
     * Get siGardien
     *
     * @return boolean
     */
    public function getSiGardien()
    {
        return $this->siGardien;
    }

    /**
     * Set siVideophone
     *
     * @param boolean $siVideophone
     *
     * @return Maison
     */
    public function setSiVideophone($siVideophone)
    {
        $this->siVideophone = $siVideophone;

        return $this;
    }

    /**
     * Get siVideophone
     *
     * @return boolean
     */
    public function getSiVideophone()
    {
        return $this->siVideophone;
    }

    /**
     * Set siInterphone
     *
     * @param boolean $siInterphone
     *
     * @return Maison
     */
    public function setSiInterphone($siInterphone)
    {
        $this->siInterphone = $siInterphone;

        return $this;
    }

    /**
     * Get siInterphone
     *
     * @return boolean
     */
    public function getSiInterphone()
    {
        return $this->siInterphone;
    }

    /**
     * Set siDigicode
     *
     * @param boolean $siDigicode
     *
     * @return Maison
     */
    public function setSiDigicode($siDigicode)
    {
        $this->siDigicode = $siDigicode;

        return $this;
    }

    /**
     * Get siDigicode
     *
     * @return boolean
     */
    public function getSiDigicode()
    {
        return $this->siDigicode;
    }

    /**
     * Set investisseur
     *
     * @param boolean $investisseur
     *
     * @return Maison
     */
    public function setInvestisseur($investisseur)
    {
        $this->investisseur = $investisseur;

        return $this;
    }

    /**
     * Get investisseur
     *
     * @return boolean
     */
    public function getInvestisseur()
    {
        return $this->investisseur;
    }

    /**
     * Set habitat
     *
     * @param boolean $habitat
     *
     * @return Maison
     */
    public function setHabitat($habitat)
    {
        $this->habitat = $habitat;

        return $this;
    }

    /**
     * Get habitat
     *
     * @return boolean
     */
    public function getHabitat()
    {
        return $this->habitat;
    }

    /**
     * Set prestige
     *
     * @param boolean $prestige
     *
     * @return Maison
     */
    public function setPrestige($prestige)
    {
        $this->prestige = $prestige;

        return $this;
    }

    /**
     * Get prestige
     *
     * @return boolean
     */
    public function getPrestige()
    {
        return $this->prestige;
    }

    /**
     * Set typologieResidence
     *
     * @param string $typologieResidence
     *
     * @return Maison
     */
    public function setTypologieResidence($typologieResidence)
    {
        $this->typologieResidence = $typologieResidence;

        return $this;
    }

    /**
     * Get typologieResidence
     *
     * @return string
     */
    public function getTypologieResidence()
    {
        return $this->typologieResidence;
    }

    /**
     * Set environnement
     *
     * @param string $environnement
     *
     * @return Maison
     */
    public function setEnvironnement($environnement)
    {
        $this->environnement = $environnement;

        return $this;
    }

    /**
     * Get environnement
     *
     * @return string
     */
    public function getEnvironnement()
    {
        return $this->environnement;
    }

    /**
     * Set typeDeChauffage
     *
     * @param integer $typeDeChauffage
     *
     * @return Maison
     */
    public function setTypeDeChauffage($typeDeChauffage)
    {
        $this->typeDeChauffage = $typeDeChauffage;

        return $this;
    }

    /**
     * Get typeDeChauffage
     *
     * @return integer
     */
    public function getTypeDeChauffage()
    {
        return $this->typeDeChauffage;
    }

    /**
     * Get typeDeChauffage
     *
     * @return integer
     */
    public function getTypeDeChauffageValue()
    {
        if(!in_array($this->typeDeChauffage, array_keys(self::CHAUFFAGE_TYPES)))
            return null;
        return self::CHAUFFAGE_TYPES[$this->typeDeChauffage];
    }

    /**
     * Set energieDeChauffage
     *
     * @param string $energieDeChauffage
     *
     * @return Maison
     */
    public function setEnergieDeChauffage($energieDeChauffage)
    {
        $this->energieDeChauffage = $energieDeChauffage;

        return $this;
    }

    /**
     * Get energieDeChauffage
     *
     * @return string
     */
    public function getEnergieDeChauffage()
    {
        return $this->energieDeChauffage;
    }

    /**
     * Set vitrage
     *
     * @param string $vitrage
     *
     * @return Maison
     */
    public function setVitrage($vitrage)
    {
        $this->vitrage = $vitrage;

        return $this;
    }

    /**
     * Get vitrage
     *
     * @return string
     */
    public function getVitrage()
    {
        return $this->vitrage;
    }

    /**
     * Set siLoggia
     *
     * @param boolean $siLoggia
     *
     * @return Maison
     */
    public function setSiLoggia($siLoggia)
    {
        $this->siLoggia = $siLoggia;

        return $this;
    }

    /**
     * Get siLoggia
     *
     * @return boolean
     */
    public function getSiLoggia()
    {
        return $this->siLoggia;
    }

    /**
     * Set siVoletsRoulantsElectriques
     *
     * @param boolean $siVoletsRoulantsElectriques
     *
     * @return Maison
     */
    public function setSiVoletsRoulantsElectriques($siVoletsRoulantsElectriques)
    {
        $this->siVoletsRoulantsElectriques = $siVoletsRoulantsElectriques;

        return $this;
    }

    /**
     * Get siVoletsRoulantsElectriques
     *
     * @return boolean
     */
    public function getSiVoletsRoulantsElectriques()
    {
        return $this->siVoletsRoulantsElectriques;
    }

    /**
     * Set siClimatise
     *
     * @param boolean $siClimatise
     *
     * @return Maison
     */
    public function setSiClimatise($siClimatise)
    {
        $this->siClimatise = $siClimatise;

        return $this;
    }

    /**
     * Get siClimatise
     *
     * @return boolean
     */
    public function getSiClimatise()
    {
        return $this->siClimatise;
    }

    /**
     * Set siTV
     *
     * @param boolean $siTV
     *
     * @return Maison
     */
    public function setSiTV($siTV)
    {
        $this->siTV = $siTV;

        return $this;
    }

    /**
     * Get siTV
     *
     * @return boolean
     */
    public function getSiTV()
    {
        return $this->siTV;
    }

    /**
     * Set siPMR
     *
     * @param boolean $siPMR
     *
     * @return Maison
     */
    public function setSiPMR($siPMR)
    {
        $this->siPMR = $siPMR;

        return $this;
    }

    /**
     * Get siPMR
     *
     * @return boolean
     */
    public function getSiPMR()
    {
        return $this->siPMR;
    }

    /**
     * Set siPiscine
     *
     * @param boolean $siPiscine
     *
     * @return Maison
     */
    public function setSiPiscine($siPiscine)
    {
        $this->siPiscine = $siPiscine;

        return $this;
    }

    /**
     * Get siPiscine
     *
     * @return boolean
     */
    public function getSiPiscine()
    {
        return $this->siPiscine;
    }

    /**
     * Set siEcoQuartier
     *
     * @param boolean $siEcoQuartier
     *
     * @return Maison
     */
    public function setSiEcoQuartier($siEcoQuartier)
    {
        $this->siEcoQuartier = $siEcoQuartier;

        return $this;
    }

    /**
     * Get siEcoQuartier
     *
     * @return boolean
     */
    public function getSiEcoQuartier()
    {
        return $this->siEcoQuartier;
    }

    /**
     * Set siBioConstruction
     *
     * @param boolean $siBioConstruction
     *
     * @return Maison
     */
    public function setSiBioConstruction($siBioConstruction)
    {
        $this->siBioConstruction = $siBioConstruction;

        return $this;
    }

    /**
     * Get siBioConstruction
     *
     * @return boolean
     */
    public function getSiBioConstruction()
    {
        return $this->siBioConstruction;
    }

    /**
     * Set loiPinel
     *
     * @param boolean $loiPinel
     *
     * @return Maison
     */
    public function setLoiPinel($loiPinel)
    {
        $this->loiPinel = $loiPinel;

        return $this;
    }

    /**
     * Get loiPinel
     *
     * @return boolean
     */
    public function getLoiPinel()
    {
        return $this->loiPinel;
    }

    /**
     * Set loiDuflot
     *
     * @param boolean $loiDuflot
     *
     * @return Maison
     */
    public function setLoiDuflot($loiDuflot)
    {
        $this->loiDuflot = $loiDuflot;

        return $this;
    }

    /**
     * Get loiDuflot
     *
     * @return boolean
     */
    public function getLoiDuflot()
    {
        return $this->loiDuflot;
    }

    /**
     * Set censiBouvard
     *
     * @param boolean $censiBouvard
     *
     * @return Maison
     */
    public function setCensiBouvard($censiBouvard)
    {
        $this->censiBouvard = $censiBouvard;

        return $this;
    }

    /**
     * Get censiBouvard
     *
     * @return boolean
     */
    public function getCensiBouvard()
    {
        return $this->censiBouvard;
    }

    /**
     * Set lMPLMNP
     *
     * @param boolean $lMPLMNP
     *
     * @return Maison
     */
    public function setLMPLMNP($lMPLMNP)
    {
        $this->LMP_LMNP = $lMPLMNP;

        return $this;
    }

    /**
     * Get lMPLMNP
     *
     * @return boolean
     */
    public function getLMPLMNP()
    {
        return $this->LMP_LMNP;
    }

    /**
     * Set loiGirardinPaul
     *
     * @param boolean $loiGirardinPaul
     *
     * @return Maison
     */
    public function setLoiGirardinPaul($loiGirardinPaul)
    {
        $this->loi_Girardin_Paul = $loiGirardinPaul;

        return $this;
    }

    /**
     * Get loiGirardinPaul
     *
     * @return boolean
     */
    public function getLoiGirardinPaul()
    {
        return $this->loi_Girardin_Paul;
    }

    /**
     * Set loiMalraux
     *
     * @param boolean $loiMalraux
     *
     * @return Maison
     */
    public function setLoiMalraux($loiMalraux)
    {
        $this->loi_Malraux = $loiMalraux;

        return $this;
    }

    /**
     * Get loiMalraux
     *
     * @return boolean
     */
    public function getLoiMalraux()
    {
        return $this->loi_Malraux;
    }

    /**
     * Set loiDemessine
     *
     * @param boolean $loiDemessine
     *
     * @return Maison
     */
    public function setLoiDemessine($loiDemessine)
    {
        $this->loi_Demessine = $loiDemessine;

        return $this;
    }

    /**
     * Get loiDemessine
     *
     * @return boolean
     */
    public function getLoiDemessine()
    {
        return $this->loi_Demessine;
    }

    /**
     * Set regimeSCPI
     *
     * @param boolean $regimeSCPI
     *
     * @return Maison
     */
    public function setRegimeSCPI($regimeSCPI)
    {
        $this->regime_SCPI = $regimeSCPI;

        return $this;
    }

    /**
     * Get regimeSCPI
     *
     * @return boolean
     */
    public function getRegimeSCPI()
    {
        return $this->regime_SCPI;
    }

    /**
     * Set nuePropriete
     *
     * @param boolean $nuePropriete
     *
     * @return Maison
     */
    public function setNuePropriete($nuePropriete)
    {
        $this->nue_Propriete = $nuePropriete;

        return $this;
    }

    /**
     * Get nuePropriete
     *
     * @return boolean
     */
    public function getNuePropriete()
    {
        return $this->nue_Propriete;
    }

    /**
     * Set pretLocatifSocial
     *
     * @param boolean $pretLocatifSocial
     *
     * @return Maison
     */
    public function setPretLocatifSocial($pretLocatifSocial)
    {
        $this->pret_Locatif_Social = $pretLocatifSocial;

        return $this;
    }

    /**
     * Get pretLocatifSocial
     *
     * @return boolean
     */
    public function getPretLocatifSocial()
    {
        return $this->pret_Locatif_Social;
    }

    /**
     * Set pSLA
     *
     * @param boolean $pSLA
     *
     * @return Maison
     */
    public function setPSLA($pSLA)
    {
        $this->PSLA = $pSLA;

        return $this;
    }

    /**
     * Get pSLA
     *
     * @return boolean
     */
    public function getPSLA()
    {
        return $this->PSLA;
    }

    /**
     * Set monumentHistorique
     *
     * @param boolean $monumentHistorique
     *
     * @return Maison
     */
    public function setMonumentHistorique($monumentHistorique)
    {
        $this->monumentHistorique = $monumentHistorique;

        return $this;
    }

    /**
     * Get monumentHistorique
     *
     * @return boolean
     */
    public function getMonumentHistorique()
    {
        return $this->monumentHistorique;
    }

    /**
     * Set elligiblePret
     *
     * @param boolean $elligiblePret
     *
     * @return Maison
     */
    public function setElligiblePret($elligiblePret)
    {
        $this->elligiblePret = $elligiblePret;

        return $this;
    }

    /**
     * Get elligiblePret
     *
     * @return boolean
     */
    public function getElligiblePret()
    {
        return $this->elligiblePret;
    }

    /**
     * Set tva1
     *
     * @param boolean $tva1
     *
     * @return Maison
     */
    public function setTva1($tva1)
    {
        $this->tva_1 = $tva1;

        return $this;
    }

    /**
     * Get tva1
     *
     * @return boolean
     */
    public function getTva1()
    {
        return $this->tva_1;
    }

    /**
     * Set tva2
     *
     * @param boolean $tva2
     *
     * @return Maison
     */
    public function setTva2($tva2)
    {
        $this->tva_2 = $tva2;

        return $this;
    }

    /**
     * Get tva2
     *
     * @return boolean
     */
    public function getTva2()
    {
        return $this->tva_2;
    }

    /**
     * Set hauteQualiteEnvironnementale
     *
     * @param boolean $hauteQualiteEnvironnementale
     *
     * @return Maison
     */
    public function setHauteQualiteEnvironnementale($hauteQualiteEnvironnementale)
    {
        $this->hauteQualiteEnvironnementale = $hauteQualiteEnvironnementale;

        return $this;
    }

    /**
     * Get hauteQualiteEnvironnementale
     *
     * @return boolean
     */
    public function getHauteQualiteEnvironnementale()
    {
        return $this->hauteQualiteEnvironnementale;
    }

    /**
     * Set nfLogement
     *
     * @param boolean $nfLogement
     *
     * @return Maison
     */
    public function setNfLogement($nfLogement)
    {
        $this->nfLogement = $nfLogement;

        return $this;
    }

    /**
     * Get nfLogement
     *
     * @return boolean
     */
    public function getNfLogement()
    {
        return $this->nfLogement;
    }

    /**
     * Set labelHabitatEnvironnement
     *
     * @param boolean $labelHabitatEnvironnement
     *
     * @return Maison
     */
    public function setLabelHabitatEnvironnement($labelHabitatEnvironnement)
    {
        $this->labelHabitatEnvironnement = $labelHabitatEnvironnement;

        return $this;
    }

    /**
     * Get labelHabitatEnvironnement
     *
     * @return boolean
     */
    public function getLabelHabitatEnvironnement()
    {
        return $this->labelHabitatEnvironnement;
    }

    /**
     * Set tHPE
     *
     * @param boolean $tHPE
     *
     * @return Maison
     */
    public function setTHPE($tHPE)
    {
        $this->THPE = $tHPE;

        return $this;
    }

    /**
     * Get tHPE
     *
     * @return boolean
     */
    public function getTHPE()
    {
        return $this->THPE;
    }

    /**
     * Set hPEENR
     *
     * @param boolean $hPEENR
     *
     * @return Maison
     */
    public function setHPEENR($hPEENR)
    {
        $this->HPE_ENR = $hPEENR;

        return $this;
    }

    /**
     * Get hPEENR
     *
     * @return boolean
     */
    public function getHPEENR()
    {
        return $this->HPE_ENR;
    }

    /**
     * Set labelBatimentBasseConsommation
     *
     * @param boolean $labelBatimentBasseConsommation
     *
     * @return Maison
     */
    public function setLabelBatimentBasseConsommation($labelBatimentBasseConsommation)
    {
        $this->labelBatimentBasseConsommation = $labelBatimentBasseConsommation;

        return $this;
    }

    /**
     * Get labelBatimentBasseConsommation
     *
     * @return boolean
     */
    public function getLabelBatimentBasseConsommation()
    {
        return $this->labelBatimentBasseConsommation;
    }

    /**
     * Set labelHabitatEnvironnementEffinergie
     *
     * @param boolean $labelHabitatEnvironnementEffinergie
     *
     * @return Maison
     */
    public function setLabelHabitatEnvironnementEffinergie($labelHabitatEnvironnementEffinergie)
    {
        $this->labelHabitatEnvironnementEffinergie = $labelHabitatEnvironnementEffinergie;

        return $this;
    }

    /**
     * Get labelHabitatEnvironnementEffinergie
     *
     * @return boolean
     */
    public function getLabelHabitatEnvironnementEffinergie()
    {
        return $this->labelHabitatEnvironnementEffinergie;
    }

    /**
     * Set planLocalHabitat
     *
     * @param boolean $planLocalHabitat
     *
     * @return Maison
     */
    public function setPlanLocalHabitat($planLocalHabitat)
    {
        $this->planLocalHabitat = $planLocalHabitat;

        return $this;
    }

    /**
     * Get planLocalHabitat
     *
     * @return boolean
     */
    public function getPlanLocalHabitat()
    {
        return $this->planLocalHabitat;
    }

    /**
     * Set rT2012
     *
     * @param boolean $rT2012
     *
     * @return Maison
     */
    public function setRT2012($rT2012)
    {
        $this->RT2012 = $rT2012;

        return $this;
    }

    /**
     * Get rT2012
     *
     * @return boolean
     */
    public function getRT2012()
    {
        return $this->RT2012;
    }

    /**
     * Set typeCharpante
     *
     * @param string $typeCharpante
     *
     * @return Maison
     */
    public function setTypeCharpante($typeCharpante)
    {
        $this->typeCharpante = $typeCharpante;

        return $this;
    }

    /**
     * Get typeCharpante
     *
     * @return string
     */
    public function getTypeCharpante()
    {
        return $this->typeCharpante;
    }

    /**
     * Set typeTuile
     *
     * @param string $typeTuile
     *
     * @return Maison
     */
    public function setTypeTuile($typeTuile)
    {
        $this->typeTuile = $typeTuile;

        return $this;
    }

    /**
     * Get typeTuile
     *
     * @return string
     */
    public function getTypeTuile()
    {
        return $this->typeTuile;
    }

    /**
     * Set materiauxTuile
     *
     * @param string $materiauxTuile
     *
     * @return Maison
     */
    public function setMateriauxTuile($materiauxTuile)
    {
        $this->materiauxTuile = $materiauxTuile;

        return $this;
    }

    /**
     * Get materiauxTuile
     *
     * @return string
     */
    public function getMateriauxTuile()
    {
        return $this->materiauxTuile;
    }

    /**
     * Set nbEtages
     *
     * @param integer $nbEtages
     *
     * @return Maison
     */
    public function setNbEtages($nbEtages)
    {
        $this->nbEtages = $nbEtages;

        return $this;
    }

    /**
     * Get nbEtages
     *
     * @return integer
     */
    public function getNbEtages()
    {
        return $this->nbEtages;
    }

    /**
     * Set typeJardin
     *
     * @param string $typeJardin
     *
     * @return Maison
     */
    public function setTypeJardin($typeJardin)
    {
        $this->typeJardin = $typeJardin;

        return $this;
    }

    /**
     * Get typeJardin
     *
     * @return string
     */
    public function getTypeJardin()
    {
        return $this->typeJardin;
    }

    /**
     * Set visAVis
     *
     * @param boolean $visAVis
     *
     * @return Maison
     */
    public function setVisAVis($visAVis)
    {
        $this->vis_a_vis = $visAVis;

        return $this;
    }

    /**
     * Get visAVis
     *
     * @return boolean
     */
    public function getVisAVis()
    {
        return $this->vis_a_vis;
    }

    /**
     * Set degagement
     *
     * @param boolean $degagement
     *
     * @return Maison
     */
    public function setDegagement($degagement)
    {
        $this->degagement = $degagement;

        return $this;
    }

    /**
     * Get degagement
     *
     * @return boolean
     */
    public function getDegagement()
    {
        return $this->degagement;
    }

    /**
     * Set toitTerrasse
     *
     * @param boolean $toitTerrasse
     *
     * @return Maison
     */
    public function setToitTerrasse($toitTerrasse)
    {
        $this->toitTerrasse = $toitTerrasse;

        return $this;
    }

    /**
     * Get toitTerrasse
     *
     * @return boolean
     */
    public function getToitTerrasse()
    {
        return $this->toitTerrasse;
    }

    /**
     * Set grenier
     *
     * @param string $grenier
     *
     * @return Maison
     */
    public function setGrenier($grenier)
    {
        $this->grenier = $grenier;

        return $this;
    }

    /**
     * Get grenier
     *
     * @return string
     */
    public function getGrenier()
    {
        return $this->grenier;
    }

    /**
     * Set zinguerie
     *
     * @param string $zinguerie
     *
     * @return Maison
     */
    public function setZinguerie($zinguerie)
    {
        $this->zinguerie = $zinguerie;

        return $this;
    }

    /**
     * Get zinguerie
     *
     * @return string
     */
    public function getZinguerie()
    {
        return $this->zinguerie;
    }

    /**
     * Set menuiserie
     *
     * @param string $menuiserie
     *
     * @return Maison
     */
    public function setMenuiserie($menuiserie)
    {
        $this->menuiserie = $menuiserie;

        return $this;
    }

    /**
     * Get menuiserie
     *
     * @return string
     */
    public function getMenuiserie()
    {
        return $this->menuiserie;
    }

    /**
     * Set volets
     *
     * @param string $volets
     *
     * @return Maison
     */
    public function setVolets($volets)
    {
        $this->volets = $volets;

        return $this;
    }

    /**
     * Get volets
     *
     * @return string
     */
    public function getVolets()
    {
        return $this->volets;
    }

    /**
     * Set porteGarage
     *
     * @param string $porteGarage
     *
     * @return Maison
     */
    public function setPorteGarage($porteGarage)
    {
        $this->porteGarage = $porteGarage;

        return $this;
    }

    /**
     * Get porteGarage
     *
     * @return string
     */
    public function getPorteGarage()
    {
        return $this->porteGarage;
    }

    /**
     * Set siSalon
     *
     * @param boolean $siSalon
     *
     * @return Maison
     */
    public function setSiSalon($siSalon)
    {
        $this->siSalon = $siSalon;

        return $this;
    }

    /**
     * Get siSalon
     *
     * @return boolean
     */
    public function getSiSalon()
    {
        return $this->siSalon;
    }
}
