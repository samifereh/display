<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\BugCommentRepository")
 * @ORM\Table(name="`bugs_comments`")
 */
class BugComment
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    /**
     * The content of the news
     * @ORM\Column(name="comment", type="text")
     */
    protected $comment;

    /**
     * The creating date of the bugComment
     * @ORM\Column(type="datetime")
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="viewed", type="boolean")
     */
    protected $viewed;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Bug", inversedBy="comments")
     */
    protected $bug;

    public function __construct()
    {
        $this->viewed = false;
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return BugComment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return BugComment
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set bug
     *
     * @param \AppBundle\Entity\Gestion\Bug $bug
     *
     * @return BugComment
     */
    public function setBug(\AppBundle\Entity\Gestion\Bug $bug = null)
    {
        $this->bug = $bug;

        return $this;
    }

    /**
     * Get bug
     *
     * @return \AppBundle\Entity\Gestion\Bug
     */
    public function getBug()
    {
        return $this->bug;
    }

    /**
     * Set viewed
     *
     * @param boolean $viewed
     *
     * @return BugComment
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return boolean
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return HouseModel
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
