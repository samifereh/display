<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\OpportunityPropositionsRelanceRepository")
 * @ORM\Table(name="`opportunity_propositions_relances`")
 */
class OpportunityPropositionRelance
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="date",type="datetime")
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="objectif", type="text")
     */
    protected $objectif;

    #RUBRIQUE RELANCE
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\OpportunityProposition", inversedBy="relances")
     */
    protected $opportunityProposal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->relances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OpportunityPropositionRelance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     *
     * @return OpportunityPropositionRelance
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return OpportunityPropositionRelance
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }


    /**
     * Set opportunityProposal
     *
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $opportunityProposal
     *
     * @return OpportunityPropositionRelance
     */
    public function setOpportunityProposal(\AppBundle\Entity\Gestion\OpportunityProposition $opportunityProposal = null)
    {
        $this->opportunityProposal = $opportunityProposal;

        return $this;
    }

    /**
     * Get opportunityProposal
     *
     * @return \AppBundle\Entity\Gestion\OpportunityProposition
     */
    public function getOpportunityProposal()
    {
        return $this->opportunityProposal;
    }
}
