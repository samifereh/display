<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\AgendaEventsRepository")
 * @ORM\Table(name="`agenda_events`")
 */
class AgendaEvent
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="title", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", name="note", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="string", name="color", length=7, nullable=true)
     */
    protected $color;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $dateStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $dateEnd;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Opportunity", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $opportunity;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     */
    protected $sharedUsers;

    protected $_type;

    /**
     * @ORM\Column(name="remind", type="boolean", nullable=true)
     */
    private $remind;

    /**
     * @ORM\Column(name="temps", type="string", nullable=true)
     */
    private $temps;

    /**
     * @ORM\Column(name="unit", type="string", nullable=true)
     */
    private $unit;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sharedUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->color       = '#83c340';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return AgendaEvent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return AgendaEvent
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return AgendaEvent
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return AgendaEvent
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return AgendaEvent
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return AgendaEvent
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set opportunity
     *
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     *
     * @return AgendaEvent
     */
    public function setOpportunity(\AppBundle\Entity\Gestion\Opportunity $opportunity = null)
    {
        $this->opportunity = $opportunity;

        return $this;
    }

    /**
     * Get opportunity
     *
     * @return \AppBundle\Entity\Gestion\Opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Add sharedUser
     *
     * @param \AppBundle\Entity\User $sharedUser
     *
     * @return AgendaEvent
     */
    public function addSharedUser(\AppBundle\Entity\User $sharedUser)
    {
        $this->sharedUsers[] = $sharedUser;

        return $this;
    }

    /**
     * Remove sharedUser
     *
     * @param \AppBundle\Entity\User $sharedUser
     */
    public function removeSharedUser(\AppBundle\Entity\User $sharedUser)
    {
        $this->sharedUsers->removeElement($sharedUser);
    }

    public function cleanSharedUsers()
    {
        $this->sharedUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get sharedUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSharedUsers()
    {
        return $this->sharedUsers;
    }

    public function getType()
    {
        if($this->opportunity) return 'listOpportunity';
        elseif(!$this->sharedUsers->isEmpty()) return 'listSharedUsers';
        else return null;
    }

    public function setType($type)
    {
        return $this->_type = $type;
    }

    /**
     * Set remind
     *
     * @param boolean $remind
     *
     * @return AgendaEvent
     */
    public function setRemind($remind)
    {
        $this->remind = $remind;

        return $this;
    }

    /**
     * Get remind
     *
     * @return boolean
     */
    public function getRemind()
    {
        return $this->remind;
    }

    /**
     * Set temps
     *
     * @param string $temps
     *
     * @return AgendaEvent
     */
    public function setTemps($temps)
    {
        $this->temps = $temps;

        return $this;
    }

    /**
     * Get temps
     *
     * @return string
     */
    public function getTemps()
    {
        return $this->temps;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return AgendaEvent
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }
}
