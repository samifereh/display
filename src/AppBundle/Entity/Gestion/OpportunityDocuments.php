<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\OpportunityDocumentRepository")
 * @ORM\Table(name="`opportunity_documents`")
 */
class OpportunityDocuments
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    /**
     * The content of the news
     * @ORM\Column(name="comment", type="text")
     */
    protected $message;

    /**
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="opportunity_documents",cascade={"persist", "remove"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Opportunity", inversedBy="comments")
     */
    protected $opportunity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set opportunity
     *
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     *
     * @return OpportunityComment
     */
    public function setOpportunity(\AppBundle\Entity\Gestion\Opportunity $opportunity = null)
    {
        $this->opportunity = $opportunity;

        return $this;
    }

    /**
     * Get opportunity
     *
     * @return \AppBundle\Entity\Gestion\Opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return OpportunityComment
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return OpportunityDocuments
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return OpportunityDocuments
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $document->setOpportunityDocuments($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
