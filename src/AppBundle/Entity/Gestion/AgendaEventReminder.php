<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\AgendaEventRemindersRepository")
 * @ORM\Table(name="`agenda_event_reminders`")
 */
class AgendaEventReminder
{

    const WAITING  = 'waiting';
    const COMPLETE = 'complete';

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Gestion\AgendaEvent", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $agendaEvent;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="_status", type="string")
     */
    private $status;


    public function __construct()
    {
        $this->status   = self::WAITING;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AgendaEventReminder
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return AgendaEventReminder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set agendaEvent
     *
     * @param \AppBundle\Entity\Gestion\AgendaEvent $agendaEvent
     *
     * @return AgendaEventReminder
     */
    public function setAgendaEvent(\AppBundle\Entity\Gestion\AgendaEvent $agendaEvent = null)
    {
        $this->agendaEvent = $agendaEvent;

        return $this;
    }

    /**
     * Get agendaEvent
     *
     * @return \AppBundle\Entity\Gestion\AgendaEvent
     */
    public function getAgendaEvent()
    {
        return $this->agendaEvent;
    }
}
