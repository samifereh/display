<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\BugsRepository")
 * @ORM\Table(name="`FAQ`")
 */
class FAQ
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected $question;

    /**
     * @ORM\Column(name="answer", type="text")
     */
    private $answer;

    /**
     * @var string
     *
     * @ORM\Column(name="youtubeEmbed", type="text", nullable=true)
     */
    protected $youtubeEmbed;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $owner;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return FAQ
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return FAQ
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return FAQ
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set youtubeEmbed
     *
     * @param string $youtubeEmbed
     *
     * @return FAQ
     */
    public function setYoutubeEmbed($youtubeEmbed)
    {
        $this->youtubeEmbed = $youtubeEmbed;

        return $this;
    }

    /**
     * Get youtubeEmbed
     *
     * @return string
     */
    public function getYoutubeEmbed()
    {
        return $this->youtubeEmbed;
    }
}
