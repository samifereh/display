<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactStatistics
 *
 * @ORM\Table(name="contact_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\ContactStatisticsRepository")
 */
class ContactStatistics
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $group;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Agency")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $agency;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Broadcast\Portal")
     * @ORM\JoinColumn(name="portal_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $portal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ad")
     * @ORM\JoinColumn(name="ad_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $ad;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Program")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $program;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Contact")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $contact;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer")
     */
    private $value;

    public function __construct()
    {
        $this->date = new \DateTime('today');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param mixed $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return mixed
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * @param mixed $portal
     */
    public function setPortal($portal)
    {
        $this->portal = $portal;
    }

    /**
     * @return mixed
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @param mixed $ad
     */
    public function setAd($ad)
    {
        $this->ad = $ad;
    }

    /**
     * @return mixed
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * @param mixed $program
     */
    public function setProgram($program)
    {
        $this->program = $program;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return contactStatistics
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return contactStatistics
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }
}

