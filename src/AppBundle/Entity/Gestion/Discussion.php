<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 *
 * @ORM\Table(name="discussion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\DiscussionRepository")
 */
class Discussion
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255,nullable=true)
     */
    protected $subject;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\Message", mappedBy="discussion" ,cascade={"persist","remove"}, fetch="EAGER")
     * @ORM\JoinColumn(onDelete="cascade")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $messages;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="discussions")
     * @ORM\JoinColumn(onDelete="cascade")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(
     *      name="discussion_users_non_vue",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="user_id",
     *              referencedColumnName="id"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="discussion_id",
     *              referencedColumnName="id"
     *          )
     *      }
     *  )
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $listNonVue;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinTable(
     *      name="discussion_users_remove",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="user_id",
     *              referencedColumnName="id"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="discussion_id",
     *              referencedColumnName="id"
     *          )
     *      }
     *  )
     */
    private $listWhoRemove;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $sender;

    /**
     * @var boolean
     * @ORM\Column(name="is_draft", type="boolean")
     */
    protected $isDraft = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages   = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users      = new \Doctrine\Common\Collections\ArrayCollection();
        $this->listNonVue = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return Discussion
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Discussion
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Gestion\Message $message
     *
     * @return Discussion
     */
    public function addMessage(\AppBundle\Entity\Gestion\Message $message)
    {
        $message->setDiscussion($this);
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Gestion\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Gestion\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Discussion
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set sender
     *
     * @param \AppBundle\Entity\User $sender
     *
     * @return Discussion
     */
    public function setSender(\AppBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \AppBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }


    /**
     * Add listNonVue
     *
     * @param \AppBundle\Entity\User $listNonVue
     *
     * @return Discussion
     */
    public function addListNonVue(\AppBundle\Entity\User $listNonVue)
    {
        $this->listNonVue[] = $listNonVue;

        return $this;
    }

    /**
     * Remove listNonVue
     *
     * @param \AppBundle\Entity\User $listNonVue
     */
    public function removeListNonVue(\AppBundle\Entity\User $listNonVue)
    {
        $this->listNonVue->removeElement($listNonVue);
    }

    /**
     * Get listNonVue
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListNonVue()
    {
        return $this->listNonVue;
    }

    /**
     * @return Discussion
     */
    public function setListNonVue($listNonVue)
    {
        return $this->listNonVue = $listNonVue;
    }

    public function hasAttachement() {
        foreach ($this->messages as $message) {
            if(count($message->getDocuments()) > 0)
                return true;
        }
        return false;
    }

    /**
     * Add listWhoRemove
     *
     * @param \AppBundle\Entity\User $listWhoRemove
     *
     * @return Discussion
     */
    public function addListWhoRemove(\AppBundle\Entity\User $listWhoRemove)
    {
        $this->listWhoRemove[] = $listWhoRemove;

        return $this;
    }

    /**
     * Remove listWhoRemove
     *
     * @param \AppBundle\Entity\User $listWhoRemove
     */
    public function removeListWhoRemove(\AppBundle\Entity\User $listWhoRemove)
    {
        $this->listWhoRemove->removeElement($listWhoRemove);
    }

    /**
     * Get listWhoRemove
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListWhoRemove()
    {
        return $this->listWhoRemove;
    }

    public function getListWhoDontRemove($currentUser)
    {
        $users = new ArrayCollection();
        foreach ($this->users as $user) {
            if(!$this->listWhoRemove->contains($user) && $user->getId() != $currentUser->getId()) {
                $users->add($user);
            }
        }
        return $users;
    }

    public function hasDraft($currentUser)
    {
        /** @var Message $message */
        foreach ($this->messages as $message) {
            if($message->getIsDraft() && $message->getUser()->getId() == $currentUser->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Set isDraft
     *
     * @param boolean $isDraft
     *
     * @return Discussion
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    /**
     * Get isDraft
     *
     * @return boolean
     */
    public function getIsDraft()
    {
        return $this->isDraft;
    }
}
