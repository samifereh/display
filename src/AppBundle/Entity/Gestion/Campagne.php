<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

//@ORM\HasLifecycleCallbacks()
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\CampagneRepository")
 * @ORM\Table(name="`campagnes`")
 */
class Campagne
{
    const DRAFT           = 'draft';
    const PROGRAMMED      = 'programmed';
    const FINISHED        = 'finished';

    const STATUS = [
      self::DRAFT,
      self::PROGRAMMED,
      self::FINISHED
    ];

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $mailjetId;

    /**
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     */
    protected $group;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    protected $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="sender_name", type="string", length=255, nullable=true)
     */
    protected $senderName;

    /**
     * @ORM\Column(name="body_text", type="text", nullable=true)
     */
    private $bodyText;

    /**
     * @ORM\Column(name="body_html", type="text", nullable=true)
     */
    private $bodyHtml;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    protected $status = self::DRAFT;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\ContactList")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $contactList;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Statistique\CampagneStatistique", cascade={"persist","remove"})
     */
    protected $statistique;

    /**
     * @ORM\Column(name="isClotured", type="boolean")
     */
    protected $isClotured = false;

    /**
     * @ORM\Column(name="percent", type="float", nullable=true)
     */
    public $percent;

    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
        $this->status = self::DRAFT;
        $this->remoteId = null;
        $this->mailjetId = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return Campagne
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set mailjetId
     *
     * @param integer $mailjetId
     *
     * @return Campagne
     */
    public function setMailjetId($mailjetId)
    {
        $this->mailjetId = $mailjetId;

        return $this;
    }

    /**
     * Get mailjetId
     *
     * @return integer
     */
    public function getMailjetId()
    {
        return $this->mailjetId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Campagne
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Campagne
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     *
     * @return Campagne
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

    /**
     * Get senderName
     *
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set bodyText
     *
     * @param string $bodyText
     *
     * @return Campagne
     */
    public function setBodyText($bodyText)
    {
        $this->bodyText = $bodyText;

        return $this;
    }

    /**
     * Get bodyText
     *
     * @return string
     */
    public function getBodyText()
    {
        return $this->bodyText;
    }

    /**
     * Set bodyHtml
     *
     * @param string $bodyHtml
     *
     * @return Campagne
     */
    public function setBodyHtml($bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;

        return $this;
    }

    /**
     * Get bodyHtml
     *
     * @return string
     */
    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Campagne
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Campagne
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Campagne
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return Campagne
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set contactList
     *
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     *
     * @return Campagne
     */
    public function setContactList(\AppBundle\Entity\Gestion\ContactList $contactList = null)
    {
        $this->contactList = $contactList;

        return $this;
    }

    /**
     * Get contactList
     *
     * @return \AppBundle\Entity\Gestion\ContactList
     */
    public function getContactList()
    {
        return $this->contactList;
    }

    /**
     * Set statistique
     *
     * @param \AppBundle\Entity\Statistique\CampagneStatistique $statistique
     *
     * @return Campagne
     */
    public function setStatistique(\AppBundle\Entity\Statistique\CampagneStatistique $statistique = null)
    {
        $this->statistique = $statistique;

        return $this;
    }

    /**
     * Get statistique
     *
     * @return \AppBundle\Entity\Statistique\CampagneStatistique
     */
    public function getStatistique()
    {
        return $this->statistique;
    }

    /**
     * Set isClotured
     *
     * @param boolean $isClotured
     *
     * @return Campagne
     */
    public function setIsClotured($isClotured)
    {
        $this->isClotured = $isClotured;

        return $this;
    }

    /**
     * Get isClotured
     *
     * @return boolean
     */
    public function getIsClotured()
    {
        return $this->isClotured;
    }

    public function getPercent()
    {
        $this->percent;
    }

//    /**
//     * @ORM\PrePersist
//     * @ORM\PreUpdate
//     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
        return $this;

//        if($this->getStatistique()) {
//            return 100*($this->getStatistique()->getProcessedCount()/$this->getStatistique()->getDeliveredCount());
//        }
//        return 0;
    }
}
