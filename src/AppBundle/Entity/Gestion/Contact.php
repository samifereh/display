<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\ContactRepository")
 * @ORM\Table(name="`contacts`")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"email", "group"},
 *     message="Cette adresse email est déjà enregistrer."
 * )
 */
class Contact
{

    const CLIENT  = 'client';
    const CONTACT = 'contact';

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $mailjetId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="civilite", type="string", length=255, nullable=true)
     */
    protected $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    protected $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=14, nullable=true)
     */
    protected $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=14, nullable=true)
     */
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_bureau", type="string", length=14, nullable=true)
     */
    protected $telephoneBureau;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="birthday", nullable=true)
     */
    protected $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", length=5, nullable=true)
     */
    protected $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=128, nullable=true)
     */
    protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=255, nullable=true)
     */
    protected $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="organisation", type="string", length=255, nullable=true)
     */
    protected $organisation;

    #CONJOINT
    /**
     * @var string
     *
     * @ORM\Column(name="conjoint_nom", type="string", length=255, nullable=true)
     */
    protected $conjoint_nom;

    /**
     * @var string
     *
     * @ORM\Column(name="conjoint_prenom", type="string", length=255, nullable=true)
     */
    protected $conjoint_prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="conjoint_telephone", type="string", length=255, nullable=true)
     */
    protected $conjoint_telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="conjoint_mobile", type="string", length=255, nullable=true)
     */
    protected $conjoint_mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="conjoint_email", type="string", length=255, nullable=true)
     */
    protected $conjoint_email;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Gestion\ContactList", inversedBy="contacts", cascade={"persist"})
     */
    protected $group_contacts;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Contact
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Contact
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Contact
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Contact
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Contact
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Contact
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Contact
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return Contact
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function calculateRemoteId()
    {
        if(!is_null($this->id)) {
            $remoteId = 'immo-contact-'.$this->id;
            $this->remoteId = md5($remoteId);
        }
    }

    /**
     * Set mailjetId
     *
     * @param integer $mailjetId
     *
     * @return Contact
     */
    public function setMailjetId($mailjetId)
    {
        $this->mailjetId = $mailjetId;

        return $this;
    }

    /**
     * Get mailjetId
     *
     * @return integer
     */
    public function getMailjetId()
    {
        return $this->mailjetId;
    }


    public function setDataFromOpportunity(Opportunity $opportunity)
    {
        $this->group      = $opportunity->getOpportunityGroup();
        $this->nom        = $opportunity->getNom();
        $this->prenom     = $opportunity->getPrenom();
        $this->ville      = $opportunity->getVille();
        $this->address    = $opportunity->getAddress();
        $this->email      = $opportunity->getEmail();
        $this->telephone  = $opportunity->getTelephone();
        $this->codePostal = $opportunity->getCodePostal();
        $this->organisation = $opportunity->getOrganisation();
        return $this;
    }

    /**
     * Set organisation
     *
     * @param string $organisation
     *
     * @return Contact
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return string
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->type           = self::CONTACT;
        $this->group_contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupContact
     *
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     *
     * @return Contact
     */
    public function addGroupContact(\AppBundle\Entity\Gestion\ContactList $contactList)
    {
        $this->group_contacts[] = $contactList;

        return $this;
    }

    public function setGroupContact(\Doctrine\Common\Collections\ArrayCollection $contactGroups)
    {
        $this->group_contacts = $contactGroups;

        return $this;
    }

    /**
     * Remove groupContact
     *
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     */
    public function removeGroupContact(\AppBundle\Entity\Gestion\ContactList $contactList)
    {
        $this->group_contacts->removeElement($contactList);
    }

    /**
     * Get groupContacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupContacts()
    {
        return $this->group_contacts;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     *
     * @return Contact
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Contact
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set telephoneBureau
     *
     * @param string $telephoneBureau
     *
     * @return Contact
     */
    public function setTelephoneBureau($telephoneBureau)
    {
        $this->telephoneBureau = $telephoneBureau;

        return $this;
    }

    /**
     * Get telephoneBureau
     *
     * @return string
     */
    public function getTelephoneBureau()
    {
        return $this->telephoneBureau;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Contact
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Contact
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set conjointNom
     *
     * @param string $conjointNom
     *
     * @return Contact
     */
    public function setConjointNom($conjointNom)
    {
        $this->conjoint_nom = $conjointNom;

        return $this;
    }

    /**
     * Get conjointNom
     *
     * @return string
     */
    public function getConjointNom()
    {
        return $this->conjoint_nom;
    }

    /**
     * Set conjointPrenom
     *
     * @param string $conjointPrenom
     *
     * @return Contact
     */
    public function setConjointPrenom($conjointPrenom)
    {
        $this->conjoint_prenom = $conjointPrenom;

        return $this;
    }

    /**
     * Get conjointPrenom
     *
     * @return string
     */
    public function getConjointPrenom()
    {
        return $this->conjoint_prenom;
    }

    /**
     * Set conjointTelephone
     *
     * @param string $conjointTelephone
     *
     * @return Contact
     */
    public function setConjointTelephone($conjointTelephone)
    {
        $this->conjoint_telephone = $conjointTelephone;

        return $this;
    }

    /**
     * Get conjointTelephone
     *
     * @return string
     */
    public function getConjointTelephone()
    {
        return $this->conjoint_telephone;
    }

    /**
     * Set conjointMobile
     *
     * @param string $conjointMobile
     *
     * @return Contact
     */
    public function setConjointMobile($conjointMobile)
    {
        $this->conjoint_mobile = $conjointMobile;

        return $this;
    }

    /**
     * Get conjointMobile
     *
     * @return string
     */
    public function getConjointMobile()
    {
        return $this->conjoint_mobile;
    }

    /**
     * Set conjointEmail
     *
     * @param string $conjointEmail
     *
     * @return Contact
     */
    public function setConjointEmail($conjointEmail)
    {
        $this->conjoint_email = $conjointEmail;

        return $this;
    }

    /**
     * Get conjointEmail
     *
     * @return string
     */
    public function getConjointEmail()
    {
        return $this->conjoint_email;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return Contact
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Contact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
