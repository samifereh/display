<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\OpportunityPropositionRepository")
 * @ORM\Table(name="`opportunity_propositions`")
 */
class OpportunityProposition
{

    const EN_DEMANDE = 'En demande';
    const A_RELANCER = 'A relancer';
    const ECHEANCE   = 'echéancé';
    const RENDRE_VOUS_EN_COURS   = 'Rendez-vous en cours';
    const SIGNER       = 'Signé';
    const ABANDONNER   = 'Abondonné';

    const ETATS = [
        self::EN_DEMANDE =>  self::EN_DEMANDE,
        self::A_RELANCER =>  self::A_RELANCER,
        self::ECHEANCE =>  self::ECHEANCE,
        self::RENDRE_VOUS_EN_COURS =>  self::RENDRE_VOUS_EN_COURS,
        self::SIGNER =>  self::SIGNER,
        self::ABANDONNER =>  self::ABANDONNER
    ];

    const APPARTEMENT       = 'Appartement';
    const MAISON            = 'Maison';
    const SURFACE           = 'Surface';
    const NBR_CHAMBRE       = 'Nombre de chambre';

    const LOGEMENT = [
        self::APPARTEMENT => self::APPARTEMENT,
        self::MAISON => self::MAISON
    ];


    const PRIMO                     = 'Primo';
    const SECUNDO                   = 'Secundo';
    const INVESTISSEMENT_LOCATIF    = 'Investissement locatif';

    const ACQUISITION = [
        self::PRIMO => self::PRIMO,
        self::SECUNDO => self::SECUNDO,
        self::INVESTISSEMENT_LOCATIF => self::INVESTISSEMENT_LOCATIF
    ];

    const POSSESSION_01 = [
        'Diffus'        => 'Diffus',
        'Lotissement'   => 'Lotissement'
    ];

    const POSSESSION_02 = [
        'A rechercher'  => 'A rechercher',
        'En vue'        => 'En vue'
    ];


    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    protected $status;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="logement_actuel", type="string", length=50, nullable=true)
     */
    protected $logementActuel;

    /**
     * @var string
     *
     * @ORM\Column(name="type_acquisition", type="string", length=50, nullable=true)
     */
    protected $typeAcquisition;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
     */
    protected $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu_alentour", type="string", length=255, nullable=true)
     */
    protected $lieuAlentour;

    /**
     * @var string
     *
     * @ORM\Column(name="possession_terrain", type="string", length=255, nullable=true)
     */
    protected $possessionTerrain;

    /**
     * @var string
     *
     * @ORM\Column(name="possession_terrain_detail", type="string", length=255, nullable=true)
     */
    protected $possessionTerrainDetail;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_terrain_from", type="float", length=255, nullable=true)
     */
    protected $budgetTerrainFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_terrain_souhaiter_from", type="float", length=255, nullable=true)
     */
    protected $surfaceTerrainSouhaiter;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_terrain_to", type="float", length=255, nullable=true)
     */
    protected $budgetTerrainTo;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_maison_from", type="float", length=255, nullable=true)
     */
    protected $budgetMaisonFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_maison_to", type="float", length=255, nullable=true)
     */
    protected $budgetMaisonTo;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_maison_souhaiter_from", type="float", length=255, nullable=true)
     */
    protected $surfaceMaisonSouhaiterFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_maison_souhaiter_to", type="float", length=255, nullable=true)
     */
    protected $surfaceMaisonSouhaiterTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombre_chambre_from", type="integer", length=255, nullable=true)
     */
    protected $nombreChambreFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombre_chambre_to", type="integer", length=255, nullable=true)
     */
    protected $nombreChambreTo;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_total_from", type="float", length=255, nullable=true)
     */
    protected $budgetTotalFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="budget_total_to", type="float", length=255, nullable=true)
     */
    protected $budgetTotalTo;

    /**
     * @var float
     *
     * @ORM\Column(name="apport_personnel", type="float", length=255, nullable=true)
     */
    protected $apportPersonnel;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateEnd;

    /**
     * @var integer
     *
     * @ORM\Column(name="progress", type="integer", length=3)
     */
    protected $progress = 0;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Ad", cascade={"persist", "remove"})
     * @ORM\JoinTable(
     *      name="opportunity_proposals_ads",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="proposal_id",
     *              referencedColumnName="id",
     *              onDelete="cascade"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="ad_id",
     *              referencedColumnName="id",
     *              onDelete="cascade"
     *          )
     *      }
     *  )
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $ads;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\HouseModel", cascade={"persist", "remove"})
     * @ORM\JoinTable(
     *      name="opportunity_proposals_modeles",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="proposal_id",
     *              referencedColumnName="id",
     *              onDelete="cascade"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="model_id",
     *              referencedColumnName="id",
     *          )
     *      }
     *  )
     *  @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $modeles;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_comment", type="text", nullable=true)
     */
    protected $customComment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="compromis_terrain", type="boolean")
     */
    protected $compromisTerrain = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateCompromisTerrain;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contrat_construction", type="boolean")
     */
    protected $contratConstruction = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateContratConstruction;

    /**
     * @var boolean
     *
     * @ORM\Column(name="permis_construire_deposer", type="boolean")
     */
    protected $permisConstruireDeposer = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $datePermisConstruireDeposer;

    #RUBRIQUE ABANDON
    /**
     * @var \DateTime
     * @ORM\Column(name="date_abandon",type="datetime", nullable=true)
     */
    protected $dateAbandon;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_abandon", type="text", nullable=true)
     */
    protected $raisonAbandon;

    #RUBRIQUE RELANCE
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\OpportunityPropositionRelance", mappedBy="opportunityProposal",cascade={"persist", "remove"})
     */
    protected $relances;

    ####
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     */
    protected $commercials;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Opportunity", inversedBy="comments")
     */
    protected $opportunity;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="proposition", cascade={"persist","remove"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modeles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commercials = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return OpportunityProposition
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set logementActuel
     *
     * @param string $logementActuel
     *
     * @return OpportunityProposition
     */
    public function setLogementActuel($logementActuel)
    {
        $this->logementActuel = $logementActuel;

        return $this;
    }

    /**
     * Get logementActuel
     *
     * @return string
     */
    public function getLogementActuel()
    {
        return $this->logementActuel;
    }

    /**
     * Set typeAcquisition
     *
     * @param string $typeAcquisition
     *
     * @return OpportunityProposition
     */
    public function setTypeAcquisition($typeAcquisition)
    {
        $this->typeAcquisition = $typeAcquisition;

        return $this;
    }

    /**
     * Get typeAcquisition
     *
     * @return string
     */
    public function getTypeAcquisition()
    {
        return $this->typeAcquisition;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return OpportunityProposition
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set lieuAlentour
     *
     * @param string $lieuAlentour
     *
     * @return OpportunityProposition
     */
    public function setLieuAlentour($lieuAlentour)
    {
        $this->lieuAlentour = $lieuAlentour;

        return $this;
    }

    /**
     * Get lieuAlentour
     *
     * @return string
     */
    public function getLieuAlentour()
    {
        return $this->lieuAlentour;
    }

    /**
     * Set possessionTerrain
     *
     * @param string $possessionTerrain
     *
     * @return OpportunityProposition
     */
    public function setPossessionTerrain($possessionTerrain)
    {
        $this->possessionTerrain = $possessionTerrain;

        return $this;
    }

    /**
     * Get possessionTerrain
     *
     * @return string
     */
    public function getPossessionTerrain()
    {
        return $this->possessionTerrain;
    }

    /**
     * Set possessionTerrainDetail
     *
     * @param string $possessionTerrainDetail
     *
     * @return OpportunityProposition
     */
    public function setPossessionTerrainDetail($possessionTerrainDetail)
    {
        $this->possessionTerrainDetail = $possessionTerrainDetail;

        return $this;
    }

    /**
     * Get possessionTerrainDetail
     *
     * @return string
     */
    public function getPossessionTerrainDetail()
    {
        return $this->possessionTerrainDetail;
    }

    /**
     * Set budgetTerrainFrom
     *
     * @param float $budgetTerrainFrom
     *
     * @return OpportunityProposition
     */
    public function setBudgetTerrainFrom($budgetTerrainFrom)
    {
        $this->budgetTerrainFrom = $budgetTerrainFrom;

        return $this;
    }

    /**
     * Get budgetTerrainFrom
     *
     * @return float
     */
    public function getBudgetTerrainFrom()
    {
        return $this->budgetTerrainFrom;
    }

    /**
     * Set budgetTerrainTo
     *
     * @param float $budgetTerrainTo
     *
     * @return OpportunityProposition
     */
    public function setBudgetTerrainTo($budgetTerrainTo)
    {
        $this->budgetTerrainTo = $budgetTerrainTo;

        return $this;
    }

    /**
     * Get budgetTerrainTo
     *
     * @return float
     */
    public function getBudgetTerrainTo()
    {
        return $this->budgetTerrainTo;
    }

    /**
     * Set budgetMaisonFrom
     *
     * @param float $budgetMaisonFrom
     *
     * @return OpportunityProposition
     */
    public function setBudgetMaisonFrom($budgetMaisonFrom)
    {
        $this->budgetMaisonFrom = $budgetMaisonFrom;

        return $this;
    }

    /**
     * Get budgetMaisonFrom
     *
     * @return float
     */
    public function getBudgetMaisonFrom()
    {
        return $this->budgetMaisonFrom;
    }

    /**
     * Set budgetMaisonTo
     *
     * @param float $budgetMaisonTo
     *
     * @return OpportunityProposition
     */
    public function setBudgetMaisonTo($budgetMaisonTo)
    {
        $this->budgetMaisonTo = $budgetMaisonTo;

        return $this;
    }

    /**
     * Get budgetMaisonTo
     *
     * @return float
     */
    public function getBudgetMaisonTo()
    {
        return $this->budgetMaisonTo;
    }

    /**
     * Set surfaceMaisonSouhaiterFrom
     *
     * @param float $surfaceMaisonSouhaiterFrom
     *
     * @return OpportunityProposition
     */
    public function setSurfaceMaisonSouhaiterFrom($surfaceMaisonSouhaiterFrom)
    {
        $this->surfaceMaisonSouhaiterFrom = $surfaceMaisonSouhaiterFrom;

        return $this;
    }

    /**
     * Get surfaceMaisonSouhaiterFrom
     *
     * @return float
     */
    public function getSurfaceMaisonSouhaiterFrom()
    {
        return $this->surfaceMaisonSouhaiterFrom;
    }

    /**
     * Set surfaceMaisonSouhaiterTo
     *
     * @param float $surfaceMaisonSouhaiterTo
     *
     * @return OpportunityProposition
     */
    public function setSurfaceMaisonSouhaiterTo($surfaceMaisonSouhaiterTo)
    {
        $this->surfaceMaisonSouhaiterTo = $surfaceMaisonSouhaiterTo;

        return $this;
    }

    /**
     * Get surfaceMaisonSouhaiterTo
     *
     * @return float
     */
    public function getSurfaceMaisonSouhaiterTo()
    {
        return $this->surfaceMaisonSouhaiterTo;
    }

    /**
     * Set nombreChambreFrom
     *
     * @param integer $nombreChambreFrom
     *
     * @return OpportunityProposition
     */
    public function setNombreChambreFrom($nombreChambreFrom)
    {
        $this->nombreChambreFrom = $nombreChambreFrom;

        return $this;
    }

    /**
     * Get nombreChambreFrom
     *
     * @return integer
     */
    public function getNombreChambreFrom()
    {
        return $this->nombreChambreFrom;
    }

    /**
     * Set nombreChambreTo
     *
     * @param integer $nombreChambreTo
     *
     * @return OpportunityProposition
     */
    public function setNombreChambreTo($nombreChambreTo)
    {
        $this->nombreChambreTo = $nombreChambreTo;

        return $this;
    }

    /**
     * Get nombreChambreTo
     *
     * @return integer
     */
    public function getNombreChambreTo()
    {
        return $this->nombreChambreTo;
    }

    /**
     * Set budgetTotalFrom
     *
     * @param float $budgetTotalFrom
     *
     * @return OpportunityProposition
     */
    public function setBudgetTotalFrom($budgetTotalFrom)
    {
        $this->budgetTotalFrom = $budgetTotalFrom;

        return $this;
    }

    /**
     * Get budgetTotalFrom
     *
     * @return float
     */
    public function getBudgetTotalFrom()
    {
        return $this->budgetTotalFrom;
    }

    /**
     * Set budgetTotalTo
     *
     * @param float $budgetTotalTo
     *
     * @return OpportunityProposition
     */
    public function setBudgetTotalTo($budgetTotalTo)
    {
        $this->budgetTotalTo = $budgetTotalTo;

        return $this;
    }

    /**
     * Get budgetTotalTo
     *
     * @return float
     */
    public function getBudgetTotalTo()
    {
        return $this->budgetTotalTo;
    }

    /**
     * Set apportPersonnel
     *
     * @param float $apportPersonnel
     *
     * @return OpportunityProposition
     */
    public function setApportPersonnel($apportPersonnel)
    {
        $this->apportPersonnel = $apportPersonnel;

        return $this;
    }

    /**
     * Get apportPersonnel
     *
     * @return float
     */
    public function getApportPersonnel()
    {
        return $this->apportPersonnel;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return OpportunityProposition
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return OpportunityProposition
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set progress
     *
     * @param integer $progress
     *
     * @return OpportunityProposition
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get progress
     *
     * @return integer
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set dateAbandon
     *
     * @param \DateTime $dateAbandon
     *
     * @return OpportunityProposition
     */
    public function setDateAbandon($dateAbandon)
    {
        $this->dateAbandon = $dateAbandon;

        return $this;
    }

    /**
     * Get dateAbandon
     *
     * @return \DateTime
     */
    public function getDateAbandon()
    {
        return $this->dateAbandon;
    }

    /**
     * Set raisonAbandon
     *
     * @param string $raisonAbandon
     *
     * @return OpportunityProposition
     */
    public function setRaisonAbandon($raisonAbandon)
    {
        $this->raisonAbandon = $raisonAbandon;

        return $this;
    }

    /**
     * Get raisonAbandon
     *
     * @return string
     */
    public function getRaisonAbandon()
    {
        return $this->raisonAbandon;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return OpportunityProposition
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ads[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ads->removeElement($ad);
    }

    /**
     * Get ads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAds()
    {
        return $this->ads;
    }

    /**
     * Add modele
     *
     * @param \AppBundle\Entity\HouseModel $modele
     *
     * @return OpportunityProposition
     */
    public function addModele(\AppBundle\Entity\HouseModel $modele)
    {
        $this->modeles[] = $modele;

        return $this;
    }

    /**
     * Remove modele
     *
     * @param \AppBundle\Entity\HouseModel $modele
     */
    public function removeModele(\AppBundle\Entity\HouseModel $modele)
    {
        $this->modeles->removeElement($modele);
    }

    /**
     * Get modeles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModeles()
    {
        return $this->modeles;
    }

    /**
     * Add commercial
     *
     * @param \AppBundle\Entity\User $commercial
     *
     * @return OpportunityProposition
     */
    public function addCommercial(\AppBundle\Entity\User $commercial)
    {
        $this->commercials[] = $commercial;

        return $this;
    }

    /**
     * Remove commercial
     *
     * @param \AppBundle\Entity\User $commercial
     */
    public function removeCommercial(\AppBundle\Entity\User $commercial)
    {
        $this->commercials->removeElement($commercial);
    }

    /**
     * Get commercials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercials()
    {
        return $this->commercials;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return OpportunityProposition
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set opportunity
     *
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     *
     * @return OpportunityProposition
     */
    public function setOpportunity(\AppBundle\Entity\Gestion\Opportunity $opportunity = null)
    {
        $this->opportunity = $opportunity;

        return $this;
    }

    /**
     * Get opportunity
     *
     * @return \AppBundle\Entity\Gestion\Opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * Set compromisTerrain
     *
     * @param boolean $compromisTerrain
     *
     * @return OpportunityProposition
     */
    public function setCompromisTerrain($compromisTerrain)
    {
        $this->compromisTerrain = $compromisTerrain;

        return $this;
    }

    /**
     * Get compromisTerrain
     *
     * @return boolean
     */
    public function getCompromisTerrain()
    {
        return $this->compromisTerrain;
    }

    /**
     * Set contratConstruction
     *
     * @param boolean $contratConstruction
     *
     * @return OpportunityProposition
     */
    public function setContratConstruction($contratConstruction)
    {
        $this->contratConstruction = $contratConstruction;

        return $this;
    }

    /**
     * Get contratConstruction
     *
     * @return boolean
     */
    public function getContratConstruction()
    {
        return $this->contratConstruction;
    }

    /**
     * Set permisConstruireDeposer
     *
     * @param boolean $permisConstruireDeposer
     *
     * @return OpportunityProposition
     */
    public function setPermisConstruireDeposer($permisConstruireDeposer)
    {
        $this->permisConstruireDeposer = $permisConstruireDeposer;

        return $this;
    }

    /**
     * Get permisConstruireDeposer
     *
     * @return boolean
     */
    public function getPermisConstruireDeposer()
    {
        return $this->permisConstruireDeposer;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return OpportunityProposition
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set surfaceTerrainSouhaiter
     *
     * @param float $surfaceTerrainSouhaiter
     *
     * @return OpportunityProposition
     */
    public function setSurfaceTerrainSouhaiter($surfaceTerrainSouhaiter)
    {
        $this->surfaceTerrainSouhaiter = $surfaceTerrainSouhaiter;

        return $this;
    }

    /**
     * Get surfaceTerrainSouhaiter
     *
     * @return float
     */
    public function getSurfaceTerrainSouhaiter()
    {
        return $this->surfaceTerrainSouhaiter;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return OpportunityProposition
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set dateCompromisTerrain
     *
     * @param \DateTime $dateCompromisTerrain
     *
     * @return OpportunityProposition
     */
    public function setDateCompromisTerrain($dateCompromisTerrain)
    {
        $this->dateCompromisTerrain = $dateCompromisTerrain;

        return $this;
    }

    /**
     * Get dateCompromisTerrain
     *
     * @return \DateTime
     */
    public function getDateCompromisTerrain()
    {
        return $this->dateCompromisTerrain;
    }

    /**
     * Set dateContratConstruction
     *
     * @param \DateTime $dateContratConstruction
     *
     * @return OpportunityProposition
     */
    public function setDateContratConstruction($dateContratConstruction)
    {
        $this->dateContratConstruction = $dateContratConstruction;

        return $this;
    }

    /**
     * Get dateContratConstruction
     *
     * @return \DateTime
     */
    public function getDateContratConstruction()
    {
        return $this->dateContratConstruction;
    }

    /**
     * Set datePermisConstruireDeposer
     *
     * @param \DateTime $datePermisConstruireDeposer
     *
     * @return OpportunityProposition
     */
    public function setDatePermisConstruireDeposer($datePermisConstruireDeposer)
    {
        $this->datePermisConstruireDeposer = $datePermisConstruireDeposer;

        return $this;
    }

    /**
     * Get datePermisConstruireDeposer
     *
     * @return \DateTime
     */
    public function getDatePermisConstruireDeposer()
    {
        return $this->datePermisConstruireDeposer;
    }

    /**
     * Add relance
     *
     * @param \AppBundle\Entity\Gestion\OpportunityPropositionRelance $relance
     *
     * @return OpportunityProposition
     */
    public function addRelance(\AppBundle\Entity\Gestion\OpportunityPropositionRelance $relance)
    {
        $relance->setOpportunityProposal($this);
        $this->relances[] = $relance;

        return $this;
    }

    /**
     * Remove relance
     *
     * @param \AppBundle\Entity\Gestion\OpportunityPropositionRelance $relance
     */
    public function removeRelance(\AppBundle\Entity\Gestion\OpportunityPropositionRelance $relance)
    {
        $this->relances->removeElement($relance);
    }

    public function getlastRelanceDate()
    {
        if(!$this->relances) return null;
        $date = null;
        /** @var \AppBundle\Entity\Gestion\OpportunityPropositionRelance $relance $relance */
        foreach ($this->relances as $relance) {
            if( is_null($date) || (!is_null($date) && $date < $relance->getDate()) )
                $date = $relance->getDate();
        }
        return $date;
    }

    /**
     * Get relances
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelances()
    {
        return $this->relances;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OpportunityProposition
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set customComment
     *
     * @param string $customComment
     *
     * @return OpportunityProposition
     */
    public function setCustomComment($customComment)
    {
        $this->customComment = $customComment;

        return $this;
    }

    /**
     * Get customComment
     *
     * @return string
     */
    public function getCustomComment()
    {
        return $this->customComment;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return OpportunityProposition
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $document->setProposition($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
