<?php

namespace AppBundle\Entity\Gestion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Gestion\OpportunityRepository")
 * @ORM\Table(name="`opportunity`")
 * @ORM\HasLifecycleCallbacks()
 */
class Opportunity
{
    const WAITING   = 'waiting';
    const OPEN      = 'open';
    const CLOSED    = 'closed';
    const ARCHIVED  = 'archived';

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\JoinColumn(name="opportunityGroup", onDelete="SET NULL")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     */
    protected $opportunityGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    protected $source;

    /**
     * @var string
     *
     * @ORM\Column(name="source_autre", type="string", length=255, nullable=true)
     */
    protected $sourceAutre;

    /**
     * @var string
     *
     * @ORM\Column(name="organisation", type="string", length=255, nullable=true)
     */
    protected $organisation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Gestion\Contact")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    protected $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=14, nullable=true)
     */
    protected $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=14, nullable=true)
     */
    protected $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_bureau", type="string", length=14, nullable=true)
     */
    protected $telephoneBureau;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostal", type="string", length=5, nullable=true)
     */
    protected $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=128, nullable=true)
     */
    protected $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    protected $status;

    /**
     * The content of the news
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $commercial;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\OpportunityCall", mappedBy="opportunity",cascade={"persist","remove"})
     */
    protected $calls;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\OpportunityComment", mappedBy="opportunity",cascade={"persist","remove"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $comments;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Gestion\OpportunityProposition", cascade={"persist","remove"})
     */
    protected $propositionDetails;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\OpportunityProposition", mappedBy="opportunity",cascade={"persist","remove"})
     */
    protected $propositions;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\OpportunityDocuments", mappedBy="opportunity",cascade={"persist","remove"})
     */
    protected $documents;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ad")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $ad;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Program")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $program;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Broadcast\Portal", mappedBy="opportunitys",cascade={"persist","remove"})
     */
    protected $portals;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\AgendaEvent", mappedBy="opportunity", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $events;

    /**
     * @var integer
     * @ORM\Column(name="progress", type="integer", length=3)
     */
    protected $progress = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->calls = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->meetings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status   = self::OPEN;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return Opportunity
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Opportunity
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set source
     *
     * @param string $source
     *
     * @return Opportunity
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set organisation
     *
     * @param string $organisation
     *
     * @return Opportunity
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return string
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Opportunity
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Opportunity
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Opportunity
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Opportunity
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Opportunity
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Opportunity
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Opportunity
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Opportunity
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Opportunity
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set commercial
     *
     * @param \AppBundle\Entity\User $commercial
     *
     * @return Opportunity
     */
    public function setCommercial(\AppBundle\Entity\User $commercial = null)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial
     *
     * @return \AppBundle\Entity\User
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Add call
     *
     * @param \AppBundle\Entity\Gestion\OpportunityCall $call
     *
     * @return Opportunity
     */
    public function addCall(\AppBundle\Entity\Gestion\OpportunityCall $call)
    {
        $call->setOpportunity($this);
        $this->calls[] = $call;

        return $this;
    }

    /**
     * Remove call
     *
     * @param \AppBundle\Entity\Gestion\OpportunityCall $call
     */
    public function removeCall(\AppBundle\Entity\Gestion\OpportunityCall $call)
    {
        $this->calls->removeElement($call);
    }

    /**
     * Get calls
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Gestion\OpportunityComment $comment
     *
     * @return Opportunity
     */
    public function addComment(\AppBundle\Entity\Gestion\OpportunityComment $comment)
    {
        $comment->setOpportunity($this);
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Gestion\OpportunityComment $comment
     */
    public function removeComment(\AppBundle\Entity\Gestion\OpportunityComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return Opportunity
     */
    public function setAd(\AppBundle\Entity\Ad $ad = null)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Opportunity
     */
    public function setProgram(\AppBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \AppBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set contact
     *
     * @param \AppBundle\Entity\Gestion\Contact $contact
     *
     * @return Opportunity
     */
    public function setContact(\AppBundle\Entity\Gestion\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \AppBundle\Entity\Gestion\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @ORM\PostUpdate()
     */
    public function calculateRemoteId()
    {
        if(!is_null($this->id)) {
            $remoteId = 'immo-opportunity-'.$this->id;
            $this->remoteId = md5($remoteId);
        }
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Gestion\OpportunityDocuments $document
     *
     * @return Opportunity
     */
    public function addDocument(\AppBundle\Entity\Gestion\OpportunityDocuments $document)
    {
        $document->setOpportunity($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Gestion\OpportunityDocuments $document
     */
    public function removeDocument(\AppBundle\Entity\Gestion\OpportunityDocuments $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Opportunity
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set telephoneBureau
     *
     * @param string $telephoneBureau
     *
     * @return Opportunity
     */
    public function setTelephoneBureau($telephoneBureau)
    {
        $this->telephoneBureau = $telephoneBureau;

        return $this;
    }

    /**
     * Get telephoneBureau
     *
     * @return string
     */
    public function getTelephoneBureau()
    {
        return $this->telephoneBureau;
    }

    /**
     * Set sourceAutre
     *
     * @param string $sourceAutre
     *
     * @return Opportunity
     */
    public function setSourceAutre($sourceAutre)
    {
        $this->sourceAutre = $sourceAutre;

        return $this;
    }

    /**
     * Get sourceAutre
     *
     * @return string
     */
    public function getSourceAutre()
    {
        return $this->sourceAutre;
    }

    /**
     * Add proposition
     *
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     *
     * @return Opportunity
     */
    public function addProposition(\AppBundle\Entity\Gestion\OpportunityProposition $proposition)
    {
        $proposition->setOpportunity($this);
        $this->propositions[] = $proposition;

        return $this;
    }

    /**
     * Remove proposition
     *
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     */
    public function removeProposition(\AppBundle\Entity\Gestion\OpportunityProposition $proposition)
    {
        $this->propositions->removeElement($proposition);
    }

    /**
     * Get propositions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPropositions()
    {
        return $this->propositions;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Opportunity
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set opportunityGroup
     *
     * @param \AppBundle\Entity\Group $opportunityGroup
     *
     * @return Opportunity
     */
    public function setOpportunityGroup(\AppBundle\Entity\Group $opportunityGroup = null)
    {
        $this->opportunityGroup = $opportunityGroup;

        return $this;
    }

    /**
     * Get opportunityGroup
     *
     * @return \AppBundle\Entity\Group
     */
    public function getOpportunityGroup()
    {
        return $this->opportunityGroup;
    }

    /**
     * Add portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     *
     * @return Opportunity
     */
    public function addPortal(\AppBundle\Entity\Broadcast\Portal $portal)
    {
        $this->portals[] = $portal;

        return $this;
    }

    /**
     * Remove portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     */
    public function removePortal(\AppBundle\Entity\Broadcast\Portal $portal)
    {
        $this->portals->removeElement($portal);
    }

    /**
     * Get portals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortals()
    {
        return $this->portals;
    }

    /**
     * Add event
     *
     * @param \AppBundle\Entity\Gestion\AgendaEvent $event
     *
     * @return Opportunity
     */
    public function addEvent(\AppBundle\Entity\Gestion\AgendaEvent $event)
    {
        $event->setOpportunity($this);
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \AppBundle\Entity\Gestion\AgendaEvent $event
     */
    public function removeEvent(\AppBundle\Entity\Gestion\AgendaEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set progress
     *
     * @param integer $progress
     *
     * @return Opportunity
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get progress
     *
     * @return integer
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set propositionDetails
     *
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $propositionDetails
     *
     * @return Opportunity
     */
    public function setPropositionDetails(\AppBundle\Entity\Gestion\OpportunityProposition $propositionDetails = null)
    {
        $this->propositionDetails = $propositionDetails;

        return $this;
    }

    /**
     * Get propositionDetails
     *
     * @return \AppBundle\Entity\Gestion\OpportunityProposition
     */
    public function getPropositionDetails()
    {
        return $this->propositionDetails;
    }
}
