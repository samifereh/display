<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Gestion\Message;
use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 * @ORM\Table(name="`document`")
 * @Gedmo\Uploadable(
 *     path="uploads/documents",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Document
{

    public function __construct(){
        $this->created = new \DateTime();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="document")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var \Datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="string", length=255, nullable = false)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255, nullable = true)
     */
    protected $originaleFileName;

    /**
     * @ORM\Column(name="document", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg", "application/pdf"}
     * )
     */
    private $document = "";

    /**
     * @var string
     *
     * @ORM\Column(name="mimetype", type="string", length=255)
     */
    private $mimeType;

    /**
     * @var Brouillon
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Brouillon", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="brouillon_id", referencedColumnName="id",onDelete="set NULL")
     */
    protected $brouillon;

    /**
     * @var Ad
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Ad", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="ad_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $ad;

    /**
     * @var Ad
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Gestion\OpportunityDocuments", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="opportunity_documents_id", referencedColumnName="id")
     */
    protected $opportunity_documents;

    /**
     * @var Message
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Gestion\Message", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="message_id", referencedColumnName="id")
     */
    protected $message;

    /**
     * @var houseModel
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\HouseModel", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="houseModel_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $houseModel;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Program", inversedBy="documents",cascade={"persist"})
     * @JoinColumn(name="program_id", referencedColumnName="id")
     */
    protected $program;

    /**
     * @ManyToOne(targetEntity="AppBundle\Entity\Gestion\OpportunityProposition", inversedBy="documents",cascade={"persist"})
     */
    protected $proposition;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName(){ return $this->name; }
    public function setName($name){ $this->name = $name; }

    public function getTitle(){ return $this->title; }
    public function setTitle($title){ $this->title = $title; }

    public function getOriginalFileName(){ return $this->originaleFileName; }
    public function setOriginalFileName($originaleFileName){ $this->originaleFileName = $originaleFileName; }

    public function getDocument(){ return $this->document; }
    public function setDocumentValue($document){
        $this->document = $document;
        return $this;
    }
    public function setDocument($document){
        $this->document = $document;
        $this->mimeType = $document->getMimeType();
        $this->name = $document->getFilename();
    }

    /**
     * Set created
     * @param \DateTime $created
     * @return Document
     */
    public function setCreated( $created ) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    public function getMimeType() {
        return $this->mimeType;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return Document
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    public function setGroup($group){ $this->group = $group; }
    public function getGroup() { return $this->group; }

    public function setBrouillon($brouillon){ $this->brouillon = $brouillon; }
    public function getBrouillon() { return $this->brouillon; }

    /**
     * Set originaleFileName
     *
     * @param string $originaleFileName
     *
     * @return Document
     */
    public function setOriginaleFileName($originaleFileName)
    {
        $this->originaleFileName = $originaleFileName;

        return $this;
    }

    /**
     * Get originaleFileName
     *
     * @return string
     */
    public function getOriginaleFileName()
    {
        return $this->originaleFileName;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     *
     * @return Document
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return Document
     */
    public function setAd(\AppBundle\Entity\Ad $ad = null)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set houseModel
     *
     * @param \AppBundle\Entity\HouseModel $houseModel
     *
     * @return Document
     */
    public function setHouseModel(\AppBundle\Entity\HouseModel $houseModel = null)
    {
        $this->houseModel = $houseModel;

        return $this;
    }

    /**
     * Get houseModel
     *
     * @return \AppBundle\Entity\HouseModel
     */
    public function getHouseModel()
    {
        return $this->houseModel;
    }

    /**
     * Set program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Document
     */
    public function setProgram(\AppBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \AppBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    public function __clone() {
        if ($this->id) {
            $this->id = null;
            $this->remoteId = null;
        }
        return $this;
    }

    /**
     * Set message
     *
     * @param \AppBundle\Entity\Gestion\Message $message
     *
     * @return Document
     */
    public function setMessage(\AppBundle\Entity\Gestion\Message $message = null)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \AppBundle\Entity\Gestion\Message
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * Set opportunityDocuments
     *
     * @param \AppBundle\Entity\Gestion\OpportunityDocuments $opportunityDocuments
     *
     * @return Document
     */
    public function setOpportunityDocuments(\AppBundle\Entity\Gestion\OpportunityDocuments $opportunityDocuments = null)
    {
        $this->opportunity_documents = $opportunityDocuments;

        return $this;
    }

    /**
     * Get opportunityDocuments
     *
     * @return \AppBundle\Entity\Gestion\OpportunityDocuments
     */
    public function getOpportunityDocuments()
    {
        return $this->opportunity_documents;
    }

    /**
     * Set proposition
     *
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     *
     * @return Document
     */
    public function setProposition(\AppBundle\Entity\Gestion\OpportunityProposition $proposition = null)
    {
        $this->proposition = $proposition;

        return $this;
    }

    /**
     * Get proposition
     *
     * @return \AppBundle\Entity\Gestion\OpportunityProposition
     */
    public function getProposition()
    {
        return $this->proposition;
    }
}
