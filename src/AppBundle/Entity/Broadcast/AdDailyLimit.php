<?php

namespace AppBundle\Entity\Broadcast;

use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * AdDailyLimit
 *
 * @ORM\Table(name="daily_limit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\AdDailyLimitRepository")
 */
class AdDailyLimit
{

    public function __construct()
    {
        $this->limit = 0;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $day;

    /**
     * @var int
     *
     * @ORM\Column(name="rest", type="integer")
     */
    protected $rest = 0;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Broadcast\CommercialPortalLimit", inversedBy="dailyAdLimit",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false,onDelete="CASCADE")
     */
    protected $commercialPortal;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     *
     * @return AdDailyLimit
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime
     */
    public function getDay()
    {
        return $this->day;
    }



    public function __toString()
    {
        return $this->getId() . ' ';
    }

    /**
     * Set rest
     *
     * @param integer $rest
     *
     * @return AdDailyLimit
     */
    public function setRest($rest)
    {
        $this->rest = $rest;

        return $this;
    }

    /**
     * Get rest
     *
     * @return integer
     */
    public function getRest()
    {
        return $this->rest;
    }

    /**
     * Set commercialPortal
     *
     * @param \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortal
     *
     * @return AdDailyLimit
     */
    public function setCommercialPortal(\AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortal)
    {
        $this->commercialPortal = $commercialPortal;

        return $this;
    }

    /**
     * Get commercialPortal
     *
     * @return \AppBundle\Entity\Broadcast\CommercialPortalLimit
     */
    public function getCommercialPortal()
    {
        return $this->commercialPortal;
    }

}
