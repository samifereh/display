<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 05/08/16
 * Time: 16:01
 */

namespace AppBundle\Entity\Broadcast;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Class Diffusion
 * @package AppBundle\Entity\Broadcast
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\DiffusionRepository")
 * @ORM\Table("diffusion")
 */
class Diffusion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToMany(targetEntity="AppBundle\Entity\Broadcast\Portal",inversedBy="diffusion", cascade={"persist"}, fetch="EAGER")
     * @JoinTable(name="diffusion_portals",
     *      joinColumns={@JoinColumn(name="diffusion_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@JoinColumn(name="portal_id", referencedColumnName="id")}
     *      )
     */
    protected $broadcastPortals;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateValidity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="diffusions",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $commercial;

    /**
     * @var
     *
     * @ORM\Column(name="log_error", type="string", length=255, nullable=true)
     */
    protected $logError;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->broadcastPortals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Diffusion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Diffusion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set dateValidity
     *
     * @param \DateTime $dateValidity
     *
     * @return Diffusion
     */
    public function setDateValidity($dateValidity)
    {
        $this->dateValidity = $dateValidity;

        return $this;
    }

    /**
     * Get dateValidity
     *
     * @return \DateTime
     */
    public function getDateValidity()
    {
        return $this->dateValidity;
    }

    /**
     * Set logError
     *
     * @param string $logError
     *
     * @return Diffusion
     */
    public function setLogError($logError)
    {
        $this->logError = $logError;

        return $this;
    }

    /**
     * Get logError
     *
     * @return string
     */
    public function getLogError()
    {
        return $this->logError;
    }

    /**
     * Add broadcastPortal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $broadcastPortal
     *
     * @return Diffusion
     */
    public function addBroadcastPortal(\AppBundle\Entity\Broadcast\Portal $broadcastPortal)
    {
        $this->broadcastPortals[] = $broadcastPortal;

        return $this;
    }

    /**
     * Remove broadcastPortal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $broadcastPortal
     */
    public function removeBroadcastPortal(\AppBundle\Entity\Broadcast\Portal $broadcastPortal)
    {
        $this->broadcastPortals->removeElement($broadcastPortal);
    }

    /**
     * Get broadcastPortals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPortals()
    {
        return $this->broadcastPortals;
    }

    /**
     * Set commercial
     *
     * @param \AppBundle\Entity\User $commercial
     *
     * @return Diffusion
     */
    public function setCommercial(\AppBundle\Entity\User $commercial)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial
     *
     * @return \AppBundle\Entity\User
     */
    public function getCommercial()
    {
        return $this->commercial;
    }
}
