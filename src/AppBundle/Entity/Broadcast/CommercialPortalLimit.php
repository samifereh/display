<?php

namespace AppBundle\Entity\Broadcast;

use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\SequenceGenerator;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * CommercialPortalLimit
 *
 * @ORM\Table(name="commercial_portal_limit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\CommercialPortalLimitRepository")
 */
class CommercialPortalLimit
{

    public function __construct()
    {
        $this->limit = 0;
    }

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Broadcast\Portal", inversedBy="commercialsPortal",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="portal_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $portal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="commercialsPortal",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="commercial_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $commercial;

    /**
     * @var int
     *
     * @ORM\Column(name="_limit", type="integer", length=255)
     */
    private $_limit;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Broadcast\AdDailyLimit", mappedBy="commercialPortal")
     */
    protected $dailyAdLimit;

    /**
     * Set limit
     *
     * @param integer $limit
     *
     * @return CommercialPortalLimit
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;

        return $this;
    }

    /**
     * Get limit
     *
     * @return integer
     */
    public function getLimit()
    {
        return $this->_limit;
    }

    /**
     * Set portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     *
     * @return CommercialPortalLimit
     */
    public function setPortal(\AppBundle\Entity\Broadcast\Portal $portal)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal
     *
     * @return \AppBundle\Entity\Broadcast\Portal
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set commercial
     *
     * @param \AppBundle\Entity\User $commercial
     *
     * @return CommercialPortalLimit
     */
    public function setCommercial(\AppBundle\Entity\User $commercial)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercialId
     *
     * @return \AppBundle\Entity\User
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get dailyAdLimit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDailyAdLimit()
    {
        return $this->dailyAdLimit;
    }


    public function __toString()
    {
        return $this->getId() . ' ';
    }

    /**
     * Add dailyAdLimit
     *
     * @param \AppBundle\Entity\Broadcast\AdDailyLimit $dailyAdLimit
     *
     * @return CommercialPortalLimit
     */
    public function addDailyAdLimit(\AppBundle\Entity\Broadcast\AdDailyLimit $dailyAdLimit)
    {
        $this->dailyAdLimit[] = $dailyAdLimit;

        return $this;
    }

    /**
     * Remove dailyAdLimit
     *
     * @param \AppBundle\Entity\Broadcast\AdDailyLimit $dailyAdLimit
     */
    public function removeDailyAdLimit(\AppBundle\Entity\Broadcast\AdDailyLimit $dailyAdLimit)
    {
        $this->dailyAdLimit->removeElement($dailyAdLimit);
    }

}
