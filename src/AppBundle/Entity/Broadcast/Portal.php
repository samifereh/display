<?php

namespace AppBundle\Entity\Broadcast;

use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinTable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Portal
 *
 * @ORM\Table(name="broadcast_portal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\PortalRepository")
 * @Gedmo\Uploadable(
 *     path="uploads/portals",
 *     allowOverwrite=true,
 * )
 */
class Portal implements EqualityComparable, Comparable
{

    const ZONES = [
        'Internationale' => 'Internationale',
        'Nationale'      => 'Nationale',
        'Régionale'      => 'Régionale',
    ];

    const CLUSTER = [
        'GRATUIT'=> 'GRATUIT',
        'PAYANT' => 'PAYANT'
    ];

    const CATEGORIES = [
        'Vente maison'                  => 'Vente maison',
        'Vente appartement'             => 'Vente appartement',
        'Vente terrain'                 => 'Vente terrain',
        'Vente de programme de CMI'     => 'Vente de programme de CMI',
        'Vente de programme promoteur'  => 'Vente de programme promoteur',
        'Vente de programme lotisseur'  => 'Vente de programme lotisseur',
        'Vente de neuf'                 => 'Vente de neuf',
        'Location saisonnière'          => 'Location saisonnière'
    ];

    const TYPES = [
        'Maison'            => 'Maison',
        'Terrain'           => 'Terrain',
        'Programme Terrain + Maison'        => 'Programme Terrain + Maison',
        'Programme neuf'        =>'Programme neuf',
        'Appartement neuf'  => 'Appartement neuf'
    ];

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=true)
     * @ORM\Column(type="string",length=128)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpHost", type="string", length=255, nullable=true)
     */
    private $ftpHost;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpUser", type="string", length=255, nullable=true)
     */
    private $ftpUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpPassword", type="string", length=255, nullable=true)
     */
    private $ftpPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="export_file_name", type="string", length=255, nullable=true)
     */
    private $exportFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="cluster", type="string", length=255)
     */
    private $cluster;

    /**
     * @var array
     * @ORM\Column(name="typeDiffusion", type="array", nullable=true)
     */
    protected $typeDiffusion;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $exportHouseModel;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAvailable", type="boolean")
     */
    private $isAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="websiteUrl", type="string", length=255, nullable=true)
     */
    private $websiteUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="espace_pro", type="string", length=255, nullable=true)
     */
    private $espacePro;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="faxNumber", type="string", length=255, nullable=true)
     */
    private $faxNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     */
    protected $icon = "";

    /**
     * @var string
     *
     * @ORM\Column(name="wording", type="string", length=255, nullable=true)
     */
    private $wording;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="description_offre", type="text", nullable=true)
     */
    private $descriptionOffre;

    /**
     * @var int
     *
     * @ORM\Column(name="monthlyVisits", type="integer", nullable=true)
     */
    private $monthlyVisits;

    /**
     * @var string
     *
     * @ORM\Column(name="viewedPagesPerMonth", type="string", nullable=true)
     */
    private $viewedPagesPerMonth;

    /**
     * @var bool
     *
     * @ORM\Column(name="hasMobileApp", type="boolean")
     */
    private $hasMobileApp;

    /**
     * @var
     *
     * @ManyToMany(targetEntity="AppBundle\Entity\Broadcast\Diffusion", mappedBy="broadcastPortals")
     * @JoinColumn(name="portal_id", referencedColumnName="id")
     */
    private $diffusion;

    /**
     * @var bool
     *
     * @ORM\Column(name="diffuseProgram", type="boolean")
     */
    private $diffuseProgram;

    /**
     * @var bool
     *
     * @ORM\Column(name="diffuseTerrainOnly", type="boolean")
     */
    private $diffuseTerrainOnly = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mondat", type="boolean")
     */
    protected $afficherMondat = true;
    /**
     * @var boolean
     *
     * @ORM\Column(name="tracking", type="boolean")
     */
    protected $afficherNumerotraking = false;

    /**
     * @OneToMany(
     *     targetEntity="AppBundle\Entity\Broadcast\PortalRegistration",
     *     mappedBy="broadcastPortal",
     *     cascade={"remove"}
     * )
     */
    protected $broadcastPortalRegistrations;


    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Broadcast\CommercialPortalLimit", mappedBy="portal")
     */
    protected $commercialsPortal;

    /**
     * @var string
     *
     * @ORM\Column(name="annee_creation", type="string", nullable=true)
     */
    private $anneeCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="nbr_annonces", type="string", nullable=true)
     */
    private $nbrAnnonces;

    /**
     * @var string
     *
     * @ORM\Column(name="ratio_pro_particuliers", type="string", nullable=true)
     */
    private $ratioProsParticuliers;

    /**
     * @var string
     *
     * @ORM\Column(name="frequence_de_mise_a_jour", type="string", nullable=true)
     */
    private $frequenceMaj;

    /**
     * @var boolean
     *
     * @ORM\Column(name="multi_diffusion_vers_autre_portails", type="boolean")
     */
    protected $multiDiffusionVersAutresPortails = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dedoublonnage_des_annonces", type="boolean")
     */
    protected $dedoublonnageAnnonces = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichage_dans_liste_resultats", type="boolean")
     */
    protected $affichageDansListeResultats = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichage_details_annonce", type="boolean")
     */
    protected $affichageDetailsAnnonce = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="affichage_nom_annonceur", type="boolean")
     */
    protected $affichageNomAnnonceur = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="formulaire_de_contact", type="boolean")
     */
    protected $formulaireDeContact = false;

    /**
     * @var string
     *
     * @ORM\Column(name="zone_geographique", type="string", nullable=true)
     */
    private $zoneGeographique;

    /**
     * @var array
     *
     * @ORM\Column(name="categories", type="array", nullable=true)
     */
    private $categories = [];

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Statistique\OpportunityStatistique", mappedBy="portal")
     */
    protected $opportunityStatistique;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Gestion\Opportunity", inversedBy="portals")
     */
    protected $opportunitys;

    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Portal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ftpHost
     *
     * @param string $ftpHost
     *
     * @return Portal
     */
    public function setFtpHost($ftpHost)
    {
        $this->ftpHost = $ftpHost;

        return $this;
    }

    /**
     * Get ftpHost
     *
     * @return string
     */
    public function getFtpHost()
    {
        return $this->ftpHost;
    }

    /**
     * Set ftpUser
     *
     * @param string $ftpUser
     *
     * @return Portal
     */
    public function setFtpUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;

        return $this;
    }

    /**
     * Get ftpUser
     *
     * @return string
     */
    public function getFtpUser()
    {
        return $this->ftpUser;
    }

    /**
     * Set ftpPassword
     *
     * @param string $ftpPassword
     *
     * @return Portal
     */
    public function setFtpPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;

        return $this;
    }

    /**
     * Get ftpPassword
     *
     * @return string
     */
    public function getFtpPassword()
    {
        return $this->ftpPassword;
    }

    /**
     * Set cluster
     *
     * @param string $cluster
     *
     * @return Portal
     */
    public function setCluster($cluster)
    {
        $this->cluster = $cluster;

        return $this;
    }

    /**
     * Get cluster
     *
     * @return string
     */
    public function getCluster()
    {
        return $this->cluster;
    }

    /**
     * Set typeDiffusion
     *
     * @param array $typeDiffusion
     *
     * @return Portal
     */
    public function setTypeDiffusion($typeDiffusion)
    {
        $this->typeDiffusion = $typeDiffusion;

        return $this;
    }

    /**
     * Get typeDiffusion
     *
     * @return array
     */
    public function getTypeDiffusion()
    {
        return $this->typeDiffusion;
    }

    /**
     * Set isAvailable
     *
     * @param boolean $isAvailable
     *
     * @return Portal
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * Get isAvailable
     *
     * @return boolean
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Portal
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Portal
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Portal
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set websiteUrl
     *
     * @param string $websiteUrl
     *
     * @return Portal
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return Portal
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Portal
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Portal
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Portal
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set faxNumber
     *
     * @param string $faxNumber
     *
     * @return Portal
     */
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }

    /**
     * Get faxNumber
     *
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Portal
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Portal
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set wording
     *
     * @param string $wording
     *
     * @return Portal
     */
    public function setWording($wording)
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * Get wording
     *
     * @return string
     */
    public function getWording()
    {
        return $this->wording;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Portal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Portal
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Portal
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set monthlyVisits
     *
     * @param integer $monthlyVisits
     *
     * @return Portal
     */
    public function setMonthlyVisits($monthlyVisits)
    {
        $this->monthlyVisits = $monthlyVisits;

        return $this;
    }

    /**
     * Get monthlyVisits
     *
     * @return integer
     */
    public function getMonthlyVisits()
    {
        return $this->monthlyVisits;
    }

    /**
     * Set viewedPagesPerMonth
     *
     * @param integer $viewedPagesPerMonth
     *
     * @return Portal
     */
    public function setViewedPagesPerMonth($viewedPagesPerMonth)
    {
        $this->viewedPagesPerMonth = $viewedPagesPerMonth;

        return $this;
    }

    /**
     * Get viewedPagesPerMonth
     *
     * @return integer
     */
    public function getViewedPagesPerMonth()
    {
        return $this->viewedPagesPerMonth;
    }

    /**
     * Set hasMobileApp
     *
     * @param boolean $hasMobileApp
     *
     * @return Portal
     */
    public function setHasMobileApp($hasMobileApp)
    {
        $this->hasMobileApp = $hasMobileApp;

        return $this;
    }

    /**
     * Get hasMobileApp
     *
     * @return boolean
     */
    public function getHasMobileApp()
    {
        return $this->hasMobileApp;
    }

    /**
     * Add diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     *
     * @return Portal
     */
    public function addDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion)
    {
        $this->diffusion[] = $diffusion;

        return $this;
    }

    /**
     * Remove diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     */
    public function removeDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion)
    {
        $this->diffusion->removeElement($diffusion);
    }

    /**
     * Get diffusion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiffusion()
    {
        return $this->diffusion;
    }

    /**
     * Return if the current object is the same as $o
     * @param $o Portal object to compare
     * @return bool
     * @throws NullPointerException If the specified object is null
     * @throws ClassCastException If the specified object's type prevents it from being compared to this object.
     */
    public function equals($o)
    {
        if(!$o instanceof Portal) {
            return false;
        }
        return $o->getId() == $this->getId();
    }

    public function compareTo($obj)
    {
        if ($obj == null) {
            throw new NullPointerException(${$obj});
        }
        if (!($obj instanceof Portal)) {
            throw new ClassCastException(${$obj});
        }
        return $this->getId() != $obj->getid() ? false : true;
    }

    public function existIn($aObj)
    {
        foreach ($aObj as $obj) {
            if($this->compareTo($obj)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add broadcastPortalRegistration
     *
     * @param \AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration
     *
     * @return Portal
     */
    public function addBroadcastPortalRegistration(\AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration)
    {
        $this->broadcastPortalRegistrations[] = $broadcastPortalRegistration;

        return $this;
    }

    /**
     * Remove broadcastPortalRegistration
     *
     * @param \AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration
     */
    public function removeBroadcastPortalRegistration(\AppBundle\Entity\Broadcast\PortalRegistration $broadcastPortalRegistration)
    {
        $this->broadcastPortalRegistrations->removeElement($broadcastPortalRegistration);
    }

    /**
     * Get broadcastPortalRegistrations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBroadcastPortalRegistrations()
    {
        return $this->broadcastPortalRegistrations;
    }


    /**
     * Add commercialsPortal
     *
     * @param \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal
     *
     * @return Portal
     */
    public function addCommercialsPortal(\AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal)
    {
        $this->commercialsPortal[] = $commercialsPortal;

        return $this;
    }

    /**
     * Remove commercialsPortal
     *
     * @param \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal
     */
    public function removeCommercialsPortal(\AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal)
    {
        $this->commercialsPortal->removeElement($commercialsPortal);
    }

    /**
     * Get commercialsPortal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialsPortal()
    {
        return $this->commercialsPortal;
    }

    public function setSlug($slug){ $this->slug = $slug; }
    public function getSlug(){ return $this->slug; }

    /**
     * Set afficherMondat
     *
     * @param boolean $afficherMondat
     *
     * @return Portal
     */
    public function setAfficherMondat($afficherMondat)
    {
        $this->afficherMondat = $afficherMondat;

        return $this;
    }

    /**
     * Get afficherMondat
     *
     * @return boolean
     */
    public function getAfficherMondat()
    {
        return $this->afficherMondat;
    }

    /**
     * Set afficherNumerotraking
     *
     * @param boolean $afficherNumerotraking
     *
     * @return Portal
     */
    public function setAfficherNumerotraking($afficherNumerotraking)
    {
        $this->afficherNumerotraking = $afficherNumerotraking;

        return $this;
    }

    /**
     * Get afficherNumerotraking
     *
     * @return boolean
     */
    public function getAfficherNumerotraking()
    {
        return $this->afficherNumerotraking;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Portal
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set diffuseProgram
     *
     * @param boolean $diffuseProgram
     *
     * @return Portal
     */
    public function setDiffuseProgram($diffuseProgram)
    {
        $this->diffuseProgram = $diffuseProgram;

        return $this;
    }

    /**
     * Get diffuseProgram
     *
     * @return boolean
     */
    public function getDiffuseProgram()
    {
        return $this->diffuseProgram;
    }

    /**
     * Set espacePro
     *
     * @param string $espacePro
     *
     * @return Portal
     */
    public function setEspacePro($espacePro)
    {
        $this->espacePro = $espacePro;

        return $this;
    }

    /**
     * Get espacePro
     *
     * @return string
     */
    public function getEspacePro()
    {
        return $this->espacePro;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->diffusion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->broadcastPortalRegistrations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commercialsPortal = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set anneeCreation
     *
     * @param integer $anneeCreation
     *
     * @return Portal
     */
    public function setAnneeCreation($anneeCreation)
    {
        $this->anneeCreation = $anneeCreation;

        return $this;
    }

    /**
     * Get anneeCreation
     *
     * @return integer
     */
    public function getAnneeCreation()
    {
        return $this->anneeCreation;
    }

    /**
     * Set nbrAnnonces
     *
     * @param integer $nbrAnnonces
     *
     * @return Portal
     */
    public function setNbrAnnonces($nbrAnnonces)
    {
        $this->nbrAnnonces = $nbrAnnonces;

        return $this;
    }

    /**
     * Get nbrAnnonces
     *
     * @return integer
     */
    public function getNbrAnnonces()
    {
        return $this->nbrAnnonces;
    }

    /**
     * Set ratioProsParticuliers
     *
     * @param string $ratioProsParticuliers
     *
     * @return Portal
     */
    public function setRatioProsParticuliers($ratioProsParticuliers)
    {
        $this->ratioProsParticuliers = $ratioProsParticuliers;

        return $this;
    }

    /**
     * Get ratioProsParticuliers
     *
     * @return string
     */
    public function getRatioProsParticuliers()
    {
        return $this->ratioProsParticuliers;
    }

    /**
     * Set frequenceMaj
     *
     * @param string $frequenceMaj
     *
     * @return Portal
     */
    public function setFrequenceMaj($frequenceMaj)
    {
        $this->frequenceMaj = $frequenceMaj;

        return $this;
    }

    /**
     * Get frequenceMaj
     *
     * @return string
     */
    public function getFrequenceMaj()
    {
        return $this->frequenceMaj;
    }

    /**
     * Set multiDiffusionVersAutresPortails
     *
     * @param boolean $multiDiffusionVersAutresPortails
     *
     * @return Portal
     */
    public function setMultiDiffusionVersAutresPortails($multiDiffusionVersAutresPortails)
    {
        $this->multiDiffusionVersAutresPortails = $multiDiffusionVersAutresPortails;

        return $this;
    }

    /**
     * Get multiDiffusionVersAutresPortails
     *
     * @return boolean
     */
    public function getMultiDiffusionVersAutresPortails()
    {
        return $this->multiDiffusionVersAutresPortails;
    }

    /**
     * Set dedoublonnageAnnonces
     *
     * @param boolean $dedoublonnageAnnonces
     *
     * @return Portal
     */
    public function setDedoublonnageAnnonces($dedoublonnageAnnonces)
    {
        $this->dedoublonnageAnnonces = $dedoublonnageAnnonces;

        return $this;
    }

    /**
     * Get dedoublonnageAnnonces
     *
     * @return boolean
     */
    public function getDedoublonnageAnnonces()
    {
        return $this->dedoublonnageAnnonces;
    }

    /**
     * Set affichageDansListeResultats
     *
     * @param boolean $affichageDansListeResultats
     *
     * @return Portal
     */
    public function setAffichageDansListeResultats($affichageDansListeResultats)
    {
        $this->affichageDansListeResultats = $affichageDansListeResultats;

        return $this;
    }

    /**
     * Get affichageDansListeResultats
     *
     * @return boolean
     */
    public function getAffichageDansListeResultats()
    {
        return $this->affichageDansListeResultats;
    }

    /**
     * Set affichageDetailsAnnonce
     *
     * @param boolean $affichageDetailsAnnonce
     *
     * @return Portal
     */
    public function setAffichageDetailsAnnonce($affichageDetailsAnnonce)
    {
        $this->affichageDetailsAnnonce = $affichageDetailsAnnonce;

        return $this;
    }

    /**
     * Get affichageDetailsAnnonce
     *
     * @return boolean
     */
    public function getAffichageDetailsAnnonce()
    {
        return $this->affichageDetailsAnnonce;
    }

    /**
     * Set affichageNomAnnonceur
     *
     * @param boolean $affichageNomAnnonceur
     *
     * @return Portal
     */
    public function setAffichageNomAnnonceur($affichageNomAnnonceur)
    {
        $this->affichageNomAnnonceur = $affichageNomAnnonceur;

        return $this;
    }

    /**
     * Get affichageNomAnnonceur
     *
     * @return boolean
     */
    public function getAffichageNomAnnonceur()
    {
        return $this->affichageNomAnnonceur;
    }

    /**
     * Set formulaireDeContact
     *
     * @param boolean $formulaireDeContact
     *
     * @return Portal
     */
    public function setFormulaireDeContact($formulaireDeContact)
    {
        $this->formulaireDeContact = $formulaireDeContact;

        return $this;
    }

    /**
     * Get formulaireDeContact
     *
     * @return boolean
     */
    public function getFormulaireDeContact()
    {
        return $this->formulaireDeContact;
    }

    /**
     * Set zoneGeographique
     *
     * @param string $zoneGeographique
     *
     * @return Portal
     */
    public function setZoneGeographique($zoneGeographique)
    {
        $this->zoneGeographique = $zoneGeographique;

        return $this;
    }

    /**
     * Get zoneGeographique
     *
     * @return string
     */
    public function getZoneGeographique()
    {
        return $this->zoneGeographique;
    }

    /**
     * Set categories
     *
     * @param array $categories
     *
     * @return Portal
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * Get categories
     *
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set diffuseTerrainOnly
     *
     * @param boolean $diffuseTerrainOnly
     *
     * @return Portal
     */
    public function setDiffuseTerrainOnly($diffuseTerrainOnly)
    {
        $this->diffuseTerrainOnly = $diffuseTerrainOnly;

        return $this;
    }

    /**
     * Get diffuseTerrainOnly
     *
     * @return boolean
     */
    public function getDiffuseTerrainOnly()
    {
        return $this->diffuseTerrainOnly;
    }

    /**
     * Set exportFileName
     *
     * @param string $exportFileName
     *
     * @return Portal
     */
    public function setExportFileName($exportFileName)
    {
        $this->exportFileName = $exportFileName;

        return $this;
    }

    /**
     * Get exportFileName
     *
     * @return string
     */
    public function getExportFileName()
    {
        return $this->exportFileName;
    }

    /**
     * Set descriptionOffre
     *
     * @param string $descriptionOffre
     *
     * @return Portal
     */
    public function setDescriptionOffre($descriptionOffre)
    {
        $this->descriptionOffre = $descriptionOffre;

        return $this;
    }

    /**
     * Get descriptionOffre
     *
     * @return string
     */
    public function getDescriptionOffre()
    {
        return $this->descriptionOffre;
    }

    /**
     * Add opportunityStatistique
     *
     * @param \AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique
     *
     * @return Portal
     */
    public function addOpportunityStatistique(\AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique)
    {
        $this->opportunityStatistique[] = $opportunityStatistique;

        return $this;
    }

    /**
     * Remove opportunityStatistique
     *
     * @param \AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique
     */
    public function removeOpportunityStatistique(\AppBundle\Entity\Statistique\OpportunityStatistique $opportunityStatistique)
    {
        $this->opportunityStatistique->removeElement($opportunityStatistique);
    }

    /**
     * Get opportunityStatistique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpportunityStatistique()
    {
        return $this->opportunityStatistique;
    }

    /**
     * Add opportunity
     *
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     *
     * @return Portal
     */
    public function addOpportunity(\AppBundle\Entity\Gestion\Opportunity $opportunity)
    {
        $this->opportunitys[] = $opportunity;

        return $this;
    }

    /**
     * Remove opportunity
     *
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     */
    public function removeOpportunity(\AppBundle\Entity\Gestion\Opportunity $opportunity)
    {
        $this->opportunitys->removeElement($opportunity);
    }

    /**
     * Get opportunitys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpportunitys()
    {
        return $this->opportunitys;
    }

    /**
     * Set exportHouseModel
     *
     * @param boolean $exportHouseModel
     *
     * @return Portal
     */
    public function setExportHouseModel($exportHouseModel)
    {
        $this->exportHouseModel = $exportHouseModel;

        return $this;
    }

    /**
     * Get exportHouseModel
     *
     * @return boolean
     */
    public function getExportHouseModel()
    {
        return $this->exportHouseModel;
    }
}
