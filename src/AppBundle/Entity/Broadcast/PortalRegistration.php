<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 05/08/16
 * Time: 15:23
 */

namespace AppBundle\Entity\Broadcast;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PortalRegistration
 *
 * @ORM\Table(name="broadcast_portal_registration")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\PortalRegistrationRepository")
 */
class PortalRegistration
{
    CONST STATUS_SENT_TO_COMMITI=0;
    CONST STATUS_WAITING_SUPPORT=1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Group
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="broadcastPortalRegistrations")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var Portal
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Broadcast\Portal", inversedBy="broadcastPortalRegistrations")
     * @JoinColumn(name="portal_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $broadcastPortal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", options={"default": false})
     */
    protected $isEnabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", options={"default": 0})
     */
    protected $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_used", type="boolean", options={"default": 0})
     */

    protected $isUsed;

    /**
     * @var integer
     *
     * @ORM\Column(name="adLimit", type="integer", nullable=true)
     */
    protected $adLimit;

    /**
     * @var integer
     *
     * @ORM\Column(name="adRest", type="integer", nullable=true)
     */
    protected $adRest;

    /**
     * @var string
     *
     * @ORM\Column(name="trackingPhone", type="string", length=255, nullable=true)
     */
    protected $trackingPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpHost", type="string", length=255, nullable=true)
     */
    protected $ftpHost;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpUser", type="string", length=255, nullable=true)
     */
    protected $ftpUser;

    /**
     * @var string
     *
     * @ORM\Column(name="ftpPassword", type="string", length=255, nullable=true)
     */
    protected $ftpPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    protected $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    protected $password;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $owner;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->broadcastPortal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = self::STATUS_SENT_TO_COMMITI;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return PortalRegistration
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set adLimit
     *
     * @param integer $adLimit
     *
     * @return PortalRegistration
     */
    public function setAdLimit($adLimit)
    {
        $this->adLimit = $adLimit;
        $this->adRest = is_null($this->adRest) ? $this->adLimit : $this->adRest;
        return $this;
    }

    /**
     * Get adLimit
     *
     * @return integer
     */
    public function getAdLimit()
    {
        return $this->adLimit;
    }

    /**
     * Set trackingPhone
     *
     * @param string $trackingPhone
     *
     * @return PortalRegistration
     */
    public function setTrackingPhone($trackingPhone)
    {
        $this->trackingPhone = $trackingPhone;

        return $this;
    }

    /**
     * Get trackingPhone
     *
     * @return string
     */
    public function getTrackingPhone()
    {
        return preg_replace("/^33/", "0", $this->trackingPhone,1);
    }

    /**
     * Set ftpHost
     *
     * @param string $ftpHost
     *
     * @return PortalRegistration
     */
    public function setFtpHost($ftpHost)
    {
        $this->ftpHost = $ftpHost;

        return $this;
    }

    /**
     * Get ftpHost
     *
     * @return string
     */
    public function getFtpHost()
    {
        return $this->ftpHost;
    }

    /**
     * Set ftpUser
     *
     * @param string $ftpUser
     *
     * @return PortalRegistration
     */
    public function setFtpUser($ftpUser)
    {
        $this->ftpUser = $ftpUser;

        return $this;
    }

    /**
     * Get ftpUser
     *
     * @return string
     */
    public function getFtpUser()
    {
        return $this->ftpUser;
    }

    /**
     * Set ftpPassword
     *
     * @param string $ftpPassword
     *
     * @return PortalRegistration
     */
    public function setFtpPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;

        return $this;
    }

    /**
     * Get ftpPassword
     *
     * @return string
     */
    public function getFtpPassword()
    {
        return $this->ftpPassword;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     *
     * @return PortalRegistration
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return PortalRegistration
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return PortalRegistration
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set adRest
     *
     * @param integer $adRest
     *
     * @return PortalRegistration
     *
     */
    public function setAdRest($adRest)
    {
        $this->adRest = is_null($adRest) ? $this->adLimit : $adRest;

        return $this;
    }

    /**
     * Get adRest
     *
     * @return integer
     */
    public function getAdRest()
    {
        return $this->adRest;
    }

    /**
     * Set broadcastPortal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $broadcastPortal
     *
     * @return PortalRegistration
     */
    public function setBroadcastPortal($broadcastPortal = null)
    {

        $this->broadcastPortal = $broadcastPortal;

        return $this;
    }

    /**
     * Get broadcastPortal
     *
     * @return \AppBundle\Entity\Broadcast\Portal
     */
    public function getBroadcastPortal()
    {
        return $this->broadcastPortal;
    }

    /**
     * Set isUsed
     *
     * @param boolean $isUsed
     *
     * @return PortalRegistration
     */
    public function setIsUsed($isUsed)
    {
        $this->isUsed = $isUsed;

        return $this;
    }

    /**
     * Get isUsed
     *
     * @return boolean
     */
    public function getIsUsed()
    {
        return $this->isUsed;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return PortalRegistration
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Diffusion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Diffusion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return User|null mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
        return $this;
    }

}
