<?php

namespace AppBundle\Entity\Statistique;

use Doctrine\ORM\Mapping as ORM;
/**
 * Daily
 *
 * @ORM\Table(name="statistique_daily")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Statistique\DailyRepository");
 */
class Daily
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ville_id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="statDaily", cascade={"persist"})
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ad", inversedBy="statDaily", cascade={"persist"})
     */
    protected $ad;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Broadcast\Portal", inversedBy="statDaily", cascade={"persist"})
     */
    protected $portal;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    protected $statValue;

    public function __construct()
    {
        $this->date = new \DateTime('today');
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Daily
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Daily
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set statValue
     *
     * @param integer $statValue
     *
     * @return Daily
     */
    public function setStatValue($statValue)
    {
        $this->statValue = $statValue;

        return $this;
    }

    /**
     * Get statValue
     *
     * @return integer
     */
    public function getStatValue()
    {
        return $this->statValue;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Daily
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return Daily
     */
    public function setAd(\AppBundle\Entity\Ad $ad = null)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     *
     * @return Daily
     */
    public function setPortal(\AppBundle\Entity\Broadcast\Portal $portal = null)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal
     *
     * @return \AppBundle\Entity\Broadcast\Portal
     */
    public function getPortal()
    {
        return $this->portal;
    }
}
