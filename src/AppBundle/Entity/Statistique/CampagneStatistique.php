<?php

namespace AppBundle\Entity\Statistique;

use Doctrine\ORM\Mapping as ORM;

/**
 * CampagneStatistique
 *
 * @ORM\Table(name="statistique_campagne")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Statistique\CampagneStatistiqueRepository");
 */
class CampagneStatistique
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $DeliveredCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $OpenedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $ProcessedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $QueuedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $ClickedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $UnsubscribedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $BlockedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $BouncedCount;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $SpamComplaintCount;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deliveredCount
     *
     * @param integer $deliveredCount
     *
     * @return CampagneStatistique
     */
    public function setDeliveredCount($deliveredCount)
    {
        $this->DeliveredCount = $deliveredCount;

        return $this;
    }

    /**
     * Get deliveredCount
     *
     * @return integer
     */
    public function getDeliveredCount()
    {
        return $this->DeliveredCount;
    }

    /**
     * Set openedCount
     *
     * @param integer $openedCount
     *
     * @return CampagneStatistique
     */
    public function setOpenedCount($openedCount)
    {
        $this->OpenedCount = $openedCount;

        return $this;
    }

    /**
     * Get openedCount
     *
     * @return integer
     */
    public function getOpenedCount()
    {
        return $this->OpenedCount;
    }

    /**
     * Set processedCount
     *
     * @param integer $processedCount
     *
     * @return CampagneStatistique
     */
    public function setProcessedCount($processedCount)
    {
        $this->ProcessedCount = $processedCount;

        return $this;
    }

    /**
     * Get processedCount
     *
     * @return integer
     */
    public function getProcessedCount()
    {
        return $this->ProcessedCount;
    }

    /**
     * Set queuedCount
     *
     * @param integer $queuedCount
     *
     * @return CampagneStatistique
     */
    public function setQueuedCount($queuedCount)
    {
        $this->QueuedCount = $queuedCount;

        return $this;
    }

    /**
     * Get queuedCount
     *
     * @return integer
     */
    public function getQueuedCount()
    {
        return $this->QueuedCount;
    }

    /**
     * Set clickedCount
     *
     * @param integer $clickedCount
     *
     * @return CampagneStatistique
     */
    public function setClickedCount($clickedCount)
    {
        $this->ClickedCount = $clickedCount;

        return $this;
    }

    /**
     * Get clickedCount
     *
     * @return integer
     */
    public function getClickedCount()
    {
        return $this->ClickedCount;
    }

    /**
     * Set unsubscribedCount
     *
     * @param integer $unsubscribedCount
     *
     * @return CampagneStatistique
     */
    public function setUnsubscribedCount($unsubscribedCount)
    {
        $this->UnsubscribedCount = $unsubscribedCount;

        return $this;
    }

    /**
     * Get unsubscribedCount
     *
     * @return integer
     */
    public function getUnsubscribedCount()
    {
        return $this->UnsubscribedCount;
    }

    /**
     * Set blockedCount
     *
     * @param integer $blockedCount
     *
     * @return CampagneStatistique
     */
    public function setBlockedCount($blockedCount)
    {
        $this->BlockedCount = $blockedCount;

        return $this;
    }

    /**
     * Get blockedCount
     *
     * @return integer
     */
    public function getBlockedCount()
    {
        return $this->BlockedCount;
    }

    /**
     * Set bouncedCount
     *
     * @param integer $bouncedCount
     *
     * @return CampagneStatistique
     */
    public function setBouncedCount($bouncedCount)
    {
        $this->BouncedCount = $bouncedCount;

        return $this;
    }

    /**
     * Get bouncedCount
     *
     * @return integer
     */
    public function getBouncedCount()
    {
        return $this->BouncedCount;
    }

    /**
     * Set spamComplaintCount
     *
     * @param integer $spamComplaintCount
     *
     * @return CampagneStatistique
     */
    public function setSpamComplaintCount($spamComplaintCount)
    {
        $this->SpamComplaintCount = $spamComplaintCount;

        return $this;
    }

    /**
     * Get spamComplaintCount
     *
     * @return integer
     */
    public function getSpamComplaintCount()
    {
        return $this->SpamComplaintCount;
    }

}
