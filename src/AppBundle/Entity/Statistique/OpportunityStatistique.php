<?php

namespace AppBundle\Entity\Statistique;

use Doctrine\ORM\Mapping as ORM;

/**
 * OpportunityStatistique
 *
 * @ORM\Table(name="statistique_opportunity")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Statistique\OpportunityStatistiqueRepository");
 */
class OpportunityStatistique
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Ad", inversedBy="opportunityStatistique", cascade={"persist"})
     */
    protected $ad;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Broadcast\Portal", inversedBy="opportunityStatistique", cascade={"persist"})
     */
    protected $portal;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    protected $nbOpportunityReceived;

    public function __construct()
    {
        $this->date = new \DateTime('today');
        $this->nbOpportunityReceived = 1;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return OpportunityStatistique
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return OpportunityStatistique
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     *
     * @return OpportunityStatistique
     */
    public function setPortal(\AppBundle\Entity\Broadcast\Portal $portal = null)
    {
        $this->portal = $portal;

        return $this;
    }

    /**
     * Get portal
     *
     * @return \AppBundle\Entity\Broadcast\Portal
     */
    public function getPortal()
    {
        return $this->portal;
    }

    /**
     * Set nbOpportunityReceived
     *
     * @param integer $nbOpportunityReceived
     *
     * @return OpportunityStatistique
     */
    public function setNbOpportunityReceived($nbOpportunityReceived)
    {
        $this->nbOpportunityReceived = $nbOpportunityReceived;

        return $this;
    }

    /**
     * Add nbOpportunityReceived
     *
     * @param integer $nbOpportunityReceived
     *
     * @return OpportunityStatistique
     */
    public function addNbOpportunityReceived()
    {
        $this->nbOpportunityReceived++;

        return $this;
    }

    /**
     * Get nbOpportunityReceived
     *
     * @return integer
     */
    public function getNbOpportunityReceived()
    {
        return $this->nbOpportunityReceived;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return OpportunityStatistique
     */
    public function setAd(\AppBundle\Entity\Ad $ad = null)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }
}
