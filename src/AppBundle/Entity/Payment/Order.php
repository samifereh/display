<?php

namespace AppBundle\Entity\Payment;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Payment\OrdersRepository")
 * @ORM\Table(name="orders")
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{

    use TimestampableEntity;
    /**
     * The offer was rejected
     */
    const STATUS_CANCELED            = 'canceled';
    /**
     * The offer was failed
     */
    const STATUS_FAILED              = 'failed';
    /**
     * The buyer has not paied yet
     */
    const STATUS_WAITING_FOR_PAYMENT = 'waiting';
    /**
     * The seller has not accepted or rejected the offer
     */
    const STATUS_PENDING             = 'pending';
    /**
     * The seller has accepted the order
     */
    const STATUS_VALIDATED           = 'validated';
    /**
     * The seller has paied the order
     */
    const STATUS_PAID                = 'paid';

    /**
     * The seller has paied the order
     */
    const STATUS_REFUNDED            = 'refunded';
    /**
     * The seller has paied the order
     */
    const STATUS_WAITING_ADMIN_VALIDATION = 'waiting_admin_validation';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(name="reference_command", type="string", length=255, nullable=true)
     */
    protected $referenceCommand;

    /**
     * @var string
     * @ORM\Column(name="reference_facture", type="string", length=255, nullable=true)
     */
    protected $referenceFacture;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="orders")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * The PreAuthorization result Message explaining the result code
     * @var string
     * @ORM\Column(name="resultMessage", type="string", length=255, nullable=true)
     */
    protected $resultMessage;

    /**
     * @var string
     * @ORM\Column(name="refundToken", type="string", length=255, nullable=true)
     */
    protected $refundToken;

    /**
     * @var string
     * @ORM\Column(name="payMethod", type="string", length=255, nullable=true)
     */
    protected $payMethod;

    /**
     * Execution date;
     * @var \DateTime
     * @ORM\Column(name="executionDate", type="datetime", nullable=true)
     */
    protected $executionDate;

    /**
     * Payed date;
     * @var \DateTime
     * @ORM\Column(name="payedDate", type="datetime", nullable=true)
     */
    protected $payedDate;

    /**
     * Expiry date;
     * @var \DateTime
     * @ORM\Column(name="expiryDate", type="datetime", nullable=true)
     */
    protected $expiryDate;

    /**
     * @var string
     * @ORM\Column(name="packId", type="string", length=50, nullable=true)
     */
    protected $packId;

    /**
     * @var string
     * @ORM\Column(name="commercial", type="boolean", nullable=true)
     */
    protected $commercial;

    /**
     * @var float
     * @ORM\Column(name="totalAmount", type="float", nullable=true)
     */
    protected $totalAmount;

    /**
     * @var float
     * @ORM\Column(name="HTTotalAmount", type="float", nullable=true)
     */
    protected $HTTotalAmount;

    /**
     * @var array
     * @ORM\Column(name="details", type="array", nullable=true)
     */
    protected $details;

    /**
     * @var bool
     * @ORM\Column(name="factured", type="boolean")
     */
    protected $factured;

    /**
     * @var bool
     * @ORM\Column(name="visible", type="boolean")
     */
    protected $visible;

    /**
     * @var bool
     * @ORM\Column(name="is_command", type="boolean")
     */
    protected $isCommand;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status   = self::STATUS_WAITING_FOR_PAYMENT;
        $this->factured = false;
        $this->visible = false;
        $this->isCommand = false;
    }

    public function __clone() {
        if ($this->id) {
            $this->id        = null;
            $this->remoteId  = $this->remoteId.'f';
            $this->createdAt = new \DateTime('now');
            $this->updatedAt = new \DateTime('now');
        }
    }


    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function updateRemoteId() {
        if($this->id && !$this->remoteId) {
            $remoteId = 'immo-invoice-'.$this->id;
            $this->setRemoteId(md5($remoteId));
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set resultMessage
     *
     * @param string $resultMessage
     *
     * @return Order
     */
    public function setResultMessage($resultMessage)
    {
        $this->resultMessage = $resultMessage;

        return $this;
    }

    /**
     * Get resultMessage
     *
     * @return string
     */
    public function getResultMessage()
    {
        return $this->resultMessage;
    }

    /**
     * Set refundToken
     *
     * @param string $refundToken
     *
     * @return Order
     */
    public function setRefundToken($refundToken)
    {
        $this->refundToken = $refundToken;

        return $this;
    }

    /**
     * Get refundToken
     *
     * @return string
     */
    public function getRefundToken()
    {
        return $this->refundToken;
    }

    /**
     * Set payMethod
     *
     * @param string $payMethod
     *
     * @return Order
     */
    public function setPayMethod($payMethod)
    {
        $this->payMethod = $payMethod;

        return $this;
    }

    /**
     * Get payMethod
     *
     * @return string
     */
    public function getPayMethod()
    {
        return $this->payMethod;
    }

    /**
     * Set executionDate
     *
     * @param \DateTime $executionDate
     *
     * @return Order
     */
    public function setExecutionDate($executionDate)
    {
        $this->executionDate = $executionDate;

        return $this;
    }

    /**
     * Get executionDate
     *
     * @return \DateTime
     */
    public function getExecutionDate()
    {
        return $this->executionDate;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Order
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set packId
     *
     * @param integer $packId
     *
     * @return Order
     */
    public function setPackId($packId)
    {
        $this->packId = $packId;

        return $this;
    }

    /**
     * Get packId
     *
     * @return integer
     */
    public function getPackId()
    {
        return $this->packId;
    }

    /**
     * Set commercial
     *
     * @param boolean $commercial
     *
     * @return Order
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial
     *
     * @return boolean
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return Ad
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }


    /**
     * Set totalAmount
     *
     * @param float $totalAmount
     *
     * @return Order
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set details
     *
     * @param array $details
     *
     * @return Order
     */
    public function setDetails($details)
    {
        $this->details = serialize($details);

        return $this;
    }

    /**
     * Get details
     *
     * @return array
     */
    public function getDetails()
    {
        if(is_array($this->details))
            return $this->details;
        return unserialize($this->details);
    }

    /**
     * Set payedDate
     *
     * @param \DateTime $payedDate
     *
     * @return Order
     */
    public function setPayedDate($payedDate)
    {
        $this->payedDate = $payedDate;

        return $this;
    }

    /**
     * Get payedDate
     *
     * @return \DateTime
     */
    public function getPayedDate()
    {
        return $this->payedDate;
    }

    /**
     * Set expiryDate
     *
     * @param \DateTime $expiryDate
     *
     * @return Order
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;

        return $this;
    }

    /**
     * Get expiryDate
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * Set hTTotalAmount
     *
     * @param float $hTTotalAmount
     *
     * @return Order
     */
    public function setHTTotalAmount($hTTotalAmount)
    {
        $this->HTTotalAmount = $hTTotalAmount;

        return $this;
    }

    /**
     * Get hTTotalAmount
     *
     * @return float
     */
    public function getHTTotalAmount()
    {
        return $this->HTTotalAmount;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }



    /**
     * Set factured
     *
     * @param boolean $factured
     *
     * @return Order
     */
    public function setFactured($factured)
    {
        $this->factured = $factured;

        return $this;
    }

    /**
     * Get factured
     *
     * @return boolean
     */
    public function getFactured()
    {
        return $this->factured;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Order
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Order
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set isCommand
     *
     * @param boolean $isCommand
     *
     * @return Order
     */
    public function setIsCommand($isCommand)
    {
        $this->isCommand = $isCommand;

        return $this;
    }

    /**
     * Get isCommand
     *
     * @return boolean
     */
    public function getIsCommand()
    {
        return $this->isCommand;
    }

    /**
     * Set referenceCommand
     *
     * @param string $referenceCommand
     *
     * @return Order
     */
    public function setReferenceCommand($referenceCommand)
    {
        $date = new \DateTime('today');
        $this->referenceCommand = 'CD'.$date->format('Ym').sprintf("%04d", $referenceCommand);

        return $this;
    }

    /**
     * Get referenceCommand
     *
     * @return string
     */
    public function getReferenceCommand()
    {
        return $this->referenceCommand;
    }

    /**
     * Set referenceFacture
     *
     * @param string $referenceFacture
     *
     * @return Order
     */
    public function setReferenceFacture($referenceFacture)
    {
        $date = new \DateTime('today');
        $this->referenceFacture = 'FA'.$date->format('YM').sprintf("%04d", $referenceFacture);

        return $this;
    }

    /**
     * Get referenceFacture
     *
     * @return string
     */
    public function getReferenceFacture()
    {
        return $this->referenceFacture;
    }
}
