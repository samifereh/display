<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 * @Gedmo\Uploadable(
 *     path="uploads/avatars",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg,image/png,image/x-png"
 * )
 * @UniqueEntity(fields={"email"}, errorPath="email", message="fos_user.email.already_used")
 * @UniqueEntity(fields={"username"}, errorPath="username", message="fos_user.username.already_used")
 */
class User extends BaseUser implements Comparable, EqualityComparable
{
    CONST RESTRICTED_EMAILS = ["a.pradeilles@grpta.fr", "j.braillon@grpta.fr"];

    public function __construct()
    {
        parent::__construct();
        $this->commercialsPortal = new ArrayCollection();
    }
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    protected $remoteId;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group", inversedBy="users")
     * @ORM\JoinTable(name="user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $name;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg", "image/png", "image/x-png"}
     * )
     */
    private $avatar = "";

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Ad", mappedBy="owner", cascade={"remove"})
     */
    protected $ad;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Broadcast\CommercialPortalLimit",cascade={"persist","remove"}, fetch="EXTRA_LAZY", mappedBy="commercial")

     */
    protected $commercialsPortal;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Broadcast\Diffusion",cascade={"persist"}, fetch="EXTRA_LAZY", mappedBy="commercial")
     */
    protected $diffusions;

    /**
     * @OneToMany(targetEntity="AppBundle\Entity\Payment\AuthorizedUser",cascade={"persist"}, fetch="EXTRA_LAZY", mappedBy="user")
     */
    protected $authorizedUser;

    /**
     * @ORM\OneToMany(targetEntity="NotesBundle\Entity\Notes", mappedBy="user")
     */
    protected $notes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="houseModels_moderation", type="boolean")
     */
    protected $houseModelsModeration = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ads_moderation", type="boolean")
     */
    protected $adsModeration = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="program_moderation", type="boolean")
     */
    protected $programModeration = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\Message", mappedBy="user")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Gestion\Campagne", mappedBy="campages")
     */
    protected $campagnes;

    public function getName(){ return $this->name; }
    public function setName($name){ $this->name = $name; }

    public function getAvatar(){ return $this->avatar; }
    public function setAvatar($avatar){ $this->avatar = $avatar; }

    public function getAgencies(){
        $agencies = array();
        foreach ($this->getGroups() as $g) {
            if($g->hasRole('ROLE_AGENCY'))
                array_push($agencies, $g);
        }
        return $agencies;
    }


    public function getAgenciesGroups(){
        $groups = array();
        foreach ($this->getGroups() as $g) {
            if($g->hasRole('ROLE_GROUP'))
                array_push($groups, $g);
            elseif($g->getParentGroup())
                array_push($groups, $g->getParentGroup());

        }
        return $groups;
    }

    public function getAgencyGroup(){
        $agencies = array();
        foreach ($this->getGroups() as $g) {
            if($g->hasRole('ROLE_AGENCY'))
                array_push($agencies, $g);
        }
        return isset($agencies[0]) ? $agencies[0] : null;
    }

    public function getWorkRoles(){
        $roles = array();
        foreach ($this->getRoles() as $r) {
            if($r != 'ROLE_AGENCY' && $r != 'ROLE_USER')
                array_push($roles, $r);
        }
        return $roles;
    }

    public function hasAgencies(){
        return (sizeof($this->getAgencies()) > 0);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return User
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->groups[0];
    }

    /**
     * @param mixed $groups
     * @return User
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;

        return $this;
    }

    public function isEmployeeOf(Group $agency){
        if($this->hasRole('ROLE_ADMIN') || $this->hasRole('ROLE_SUPER_ADMIN'))
            return true;
        if($agency->hasRole('ROLE_GROUP')) {
            foreach ($this->getGroups() as $group)
                if($group->getId() == $agency->getId())
                return true;
            foreach ($agency->getAgencyGroups() as $a) {
                foreach ($this->getGroups() as $group)
                    if($a->getId() === $group->getId())
                        return true;
            }
        }
        foreach ($this->getGroups() as $group)
            if($agency->getParentGroup() && ($agency->getParentGroup()->getId() == $group->getId()))
                return true;
        foreach ($this->getAgencies() as $a) {
            if($a->getId() === $agency->getId())
                return true;
        }
        return false;
    }

    public function compareTo($obj)
    {
        if ($obj == null) {
            throw new NullPointerException(${$obj});
        }
        if (!($obj instanceof Group)) {
            throw new ClassCastException(${$obj});
        }

        if ($this->getId() < $obj->getid()) {
            return -1;
        } else if ($this->getId() > $obj->getid()) {
            return 1;
        } else {
            return 0;
        }

    }

    public function equals($o) {
        if ($o == null) {
            throw new NullPointerException();
        }
        if (!($o instanceof User)) {
            throw new ClassCastException();
        }

        return $this->getId() == $o->getId();
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return User
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }

    /**
     * Get ad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Add commercialsPortal
     *
     * @param \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal
     *
     * @return User
     */
    public function addCommercialsPortal(\AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal)
    {
        $this->commercialsPortal[] = $commercialsPortal;

        return $this;
    }

    /**
     * Remove commercialsPortal
     *
     * @param \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal
     */
    public function removeCommercialsPortal(\AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal)
    {
        $this->commercialsPortal->removeElement($commercialsPortal);
    }

    /**
     * Get commercialsPortal
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommercialsPortal()
    {
        return $this->commercialsPortal;
    }

    /**
     * Set houseModelsModeration
     *
     * @param boolean $houseModelsModeration
     *
     * @return User
     */
    public function setHouseModelsModeration($houseModelsModeration)
    {
        $this->houseModelsModeration = $houseModelsModeration;

        return $this;
    }

    /**
     * Get houseModelsModeration
     *
     * @return boolean
     */
    public function getHouseModelsModeration()
    {
        return $this->houseModelsModeration;
    }

    /**
     * Set adsModeration
     *
     * @param boolean $adsModeration
     *
     * @return User
     */
    public function setAdsModeration($adsModeration)
    {
        $this->adsModeration = $adsModeration;

        return $this;
    }

    /**
     * Get adsModeration
     *
     * @return boolean
     */
    public function getAdsModeration()
    {
        return $this->adsModeration;
    }

    /**
     * Set programModeration
     *
     * @param boolean $programModeration
     *
     * @return User
     */
    public function setProgramModeration($programModeration)
    {
        $this->programModeration = $programModeration;

        return $this;
    }

    /**
     * Get programModeration
     *
     * @return boolean
     */
    public function getProgramModeration()
    {
        return $this->programModeration;
    }

    /**
     * Add authorizedUser
     *
     * @param \AppBundle\Entity\Payment\AuthorizedUser $authorizedUser
     *
     * @return User
     */
    public function addAuthorizedUser(\AppBundle\Entity\Payment\AuthorizedUser $authorizedUser)
    {
        $this->authorizedUser[] = $authorizedUser;

        return $this;
    }

    /**
     * Remove authorizedUser
     *
     * @param \AppBundle\Entity\Payment\AuthorizedUser $authorizedUser
     */
    public function removeAuthorizedUser(\AppBundle\Entity\Payment\AuthorizedUser $authorizedUser)
    {
        $this->authorizedUser->removeElement($authorizedUser);
    }

    /**
     * Get authorizedUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthorizedUser()
    {
        return $this->authorizedUser;
    }

    /**
     * Add note
     *
     * @param \NotesBundle\Entity\Notes $note
     *
     * @return User
     */
    public function addNote(\NotesBundle\Entity\Notes $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \NotesBundle\Entity\Notes $note
     */
    public function removeNote(\NotesBundle\Entity\Notes $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * Get notes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     *
     * @return User
     */
    public function addDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion)
    {
        $diffusion->setCommercial($this);
        $this->diffusions[] = $diffusion;

        return $this;
    }

    /**
     * Remove diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     */
    public function removeDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion)
    {
        $this->diffusions->removeElement($diffusion);
    }

    /**
     * Get diffusions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiffusions()
    {
        return $this->diffusions;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Gestion\Message $message
     *
     * @return User
     */
    public function addMessage(\AppBundle\Entity\Gestion\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Gestion\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Gestion\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }


    public function hasProgramOption() {
        /** @var \AppBundle\Entity\Group $group */
        foreach ($this->getGroups() as $group) {
            if(!$group->hasRole('ROLE_GROUP'))
                return $group->getProgramOption();

            foreach ($group->getAgencyGroups() as $agency) {
                if($agency->getProgramOption())
                    return true;
            }
        }
        return false;
    }

    public function hasAnnoncesOption() {
        /** @var \AppBundle\Entity\Group $group */
        foreach ($this->getGroups() as $group) {
            if(!$group->hasRole('ROLE_GROUP'))
                return $group->getAnnoncesOption();

            foreach ($group->getAgencyGroups() as $agency) {
                if($agency->getAnnoncesOption())
                    return true;
            }
        }
        return false;
    }

    public function hasSuiviOption() {
        /** @var \AppBundle\Entity\Group $group */
        foreach ($this->getGroups() as $group) {
            if(!$group->hasRole('ROLE_GROUP'))
                return $group->getSuiviOption();

            foreach ($group->getAgencyGroups() as $agency) {
                if($agency->getSuiviOption())
                    return true;
            }
        }
        return false;
    }

    /**
     * check if user has restricted access or not
     * @return bool
     */
    public function hasFullAccess() {
        return !in_array($this->getEmail(), self::RESTRICTED_EMAILS);
    }
}
