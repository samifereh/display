<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BrouillonRepository")
 * @ORM\Table(name="brouillon")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Brouillon
{
    /**
     * The unique id of the news
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

     /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     *
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="brouillon",cascade={"persist", "remove"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * @ORM\Column(name="imagePrincipale", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg"}
     * )
     */
    protected $imagePrincipale = "";

    /**
     * @ORM\Column(type="integer", length=7, nullable=true)
     */
    private $targetId;

    /**
     * @ORM\Column(length=128)
     */
    private $sessionId;

    public function __construct(){
        $this->created = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }
    /**
     * Set created
     * @param \DateTime $created
     * @return Object
     */
    public function setCreated( $created ) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    public function setSessionId( $sessionId ) {
        $this->sessionId = $sessionId;
        return $this;
    }

    public function getSessionId() {
        return $this->sessionId;
    }


    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return HouseModel
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPdfDocuments()
    {
        $return = [];
        if($this->getDocuments()) {
            foreach($this->getDocuments() as $document) {
                if($document->getMimeType() == 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return $return;
    }


    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagesDocuments()
    {
        $return = [];
        if($this->getDocuments()) {
            foreach ($this->getDocuments() as $document) {
                if ($document->getMimeType() != 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return is_null($return) || count($return) == 0 ? [] : $return;
    }

    public function setDocuments($documents)
    {
        return $this->documents = $documents;
    }

    /**
     * Set imagePrincipale
     *
     * @param string $imagePrincipale
     *
     * @return Brouillon
     */
    public function setImagePrincipale($imagePrincipale)
    {
        $this->imagePrincipale = $imagePrincipale;

        return $this;
    }

    /**
     * Get imagePrincipale
     *
     * @return string
     */
    public function getImagePrincipale()
    {
        return $this->imagePrincipale;
    }


    /**
     * Set targetId
     *
     * @param integer $targetId
     *
     * @return Brouillon
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->targetId;
    }
}
