<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PendingActions
 *
 * @ORM\Table(name="pending_actions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PendingActionsRepository")
 */
class PendingActions
{

    CONST ERROR_STATUS = -1;
    CONST STAND_BY_STATUS = 0;
    CONST PENDING_STATUS = 1;
    CONST FINISHED_STATUS = 2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=255)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="args", type="text")
     */
    private $args;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"default": -1})
     */
    private $status;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->status = self::STAND_BY_STATUS;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return PendingActions
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set args
     *
     * @param mixed $args
     *
     * @return PendingActions
     */
    public function setArgs($args)
    {
        $this->args = serialize($args);

        return $this;
    }

    /**
     * Get args
     *
     * @return string
     */
    public function getArgs()
    {
        return unserialize($this->args);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PendingActions
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PendingActions
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PendingActions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValue()
    {
        if($this->id)
            $this->updatedAt = new \DateTime();
    }

}

