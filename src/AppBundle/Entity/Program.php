<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProgramRepository")
 * @ORM\Table(name="program")
 * @Gedmo\Uploadable(
 *     path="uploads/program/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class Program
{

    const PENDING  = 'en-attente';
    const VALIDATE = 'valider';
    const ANNULER  = 'annuler';

    const STATUS = array(
        'en-attente' => 'En attente de validation',
        'publier'  	 => 'Publié',
        'annuler'    => 'Annuler'
    );

    const COMMERCIALISATION = array(
        'program.details_de_commercialisation.avant_premier' => 'program.details_de_commercialisation.avant_premier',
        'program.details_de_commercialisation.lancement_commercial'  	 => 'program.details_de_commercialisation.lancement_commercial',
        'program.details_de_commercialisation.demarrage_travaux'    => 'program.details_de_commercialisation.demarrage_travaux',
        'program.details_de_commercialisation.travaux_en_cours'    => 'program.details_de_commercialisation.travaux_en_cours',
        'program.details_de_commercialisation.livraison_immediate'    => 'program.details_de_commercialisation.livraison_immediate',
        'program.details_de_commercialisation.livraison_1e_tranche'    => 'program.details_de_commercialisation.livraison_1e_tranche',
        'program.details_de_commercialisation.livraison_2e_tranche'    => 'program.details_de_commercialisation.livraison_2e_tranche',
        'program.details_de_commercialisation.derniere_opportunite'    => 'program.details_de_commercialisation.derniere_opportunite'
    );

    /**
     * The unique id of terrain
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * The owner of the house model
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $owner;

    /**
     * The group owner of the house model
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group",inversedBy="programs")
     */
    protected $group;

    /**
     * The creating date of the house model
     * @ORM\Column(type="datetime")
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * The updating date of the house model
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * The name
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $type;

    /**
     * @ORM\Column(type="integer", length=3,nullable=false)
     */
    protected $nbRemonte = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $reference;

    /**
     * The name of the ad
     * @ORM\Column(type="string", nullable=true)
     */
    protected $referenceInterne;

    /**
     * @ORM\Column(type="text",length=4000, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string",length=50)
     */
    protected $pays;

    /**
     * @ORM\Column(type="string",length=50)
     */
    protected $ville;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $codePostal;

    /**
     * The property address
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=128, nullable=true)
     */
    protected $adresse;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $adresseBulleDeVente;

    /**
     * @ORM\Column(type="integer", length=4,nullable=false)
     */
    protected $nbLots = 0;

    /**
     * @ORM\Column(type="integer", length=4,nullable=false)
     */
    protected $nbLotsDisponible = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_visit_virtuel", type="string", length=255, nullable=true)
     */
    protected $lienVisitVirtuel;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titreImagePrincipale;

    /**
     * @ORM\Column(name="imagePrincipale", type="string", length=255, nullable=false)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg"}
     * )
     */
    protected $imagePrincipale = "";

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateCommercialisation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateLivraison;

    /**
     * @var boolean
     * @ORM\Column(name="investisseur", type="boolean")
     */
    protected $investisseur;

    /**
     * @var boolean
     * @ORM\Column(name="habitat", type="boolean")
     */
    protected $habitat;

    /**
     * @var string
     * @ORM\Column(name="typologie_de_residence", type="string", length=128, nullable=true)
     */
    protected $typologieResidence;

    /**
     * @var string
     * @ORM\Column(name="environnement", type="string", length=20, nullable=true)
     */
    protected $environnement;

    /*Prestations*/
    /**
     * @var string
     * @ORM\Column(name="nBetages", type="integer", length=3, nullable=true)
     */
    protected $nbetages;

    /**
     * @var string
     * @ORM\Column(name="type_avancement", type="string", length=60, nullable=true)
     */
    protected $typeAvancement;

    /**
     * @var string
     * @ORM\Column(name="type_de_chauffage", type="integer", length=6, nullable=true)
     */
    protected $typeDeChauffage;

    /**
     * @var string
     * @ORM\Column(name="energie_de_chauffage", type="string", length=20, nullable=true)
     */
    protected $energieDeChauffage;
    /**
     * genre carrelage, etc
     * @var string
     *
     * @ORM\Column(name="revetement_sol", type="string", length=48, nullable=true)
     */
    protected $revetementDeSol;
    /**
     * @var string
     *
     * @ORM\Column(name="vitrage", type="string", length=48, nullable=true)
     */
    protected $vitrage;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_placards", type="boolean", nullable=true)
     */
    protected $siPlacards;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_garage", type="boolean", nullable=true)
     */
    protected $siGarage;

    /**
     * @ORM\Column(name="nb_garage", type="integer", nullable=true)
     */
    protected $nbGarage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_parking", type="boolean", nullable=true)
     */
    protected $siParking;

    /**
     * @ORM\Column(name="nb_parking", type="integer", nullable=true)
     */
    protected $nbParking;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_box", type="boolean", nullable=true)
     */
    protected $siBox;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_ascenseur", type="boolean", nullable=true)
     */
    protected $siAscenseur;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_terrasse", type="boolean", nullable=true)
     */
    protected $siTerrasse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_balcons", type="boolean", nullable=true)
     */
    protected $siBalcons;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_loggia", type="boolean", nullable=true)
     */
    protected $siLoggia;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_cave", type="boolean", nullable=true)
     */
    protected $siCave;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_jardin", type="boolean", nullable=true)
     */
    protected $siJardin;

    /**
     * @var string
     *
     * @ORM\Column(name="type_jardin", type="string", nullable=true)
     */
    protected $typeJardin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_volets_roulants_electriques", type="boolean", nullable=true)
     */
    protected $siVoletsRoulantsElectriques;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_climatise", type="boolean", nullable=true)
     */
    protected $siClimatise;

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_tv", type="boolean", nullable=true)
     */
    protected $siTV;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_PMR", type="boolean", nullable=true)
     */
    protected $siPMR;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_piscine", type="boolean", nullable=true)
     */
    protected $siPiscine;

    /*Sécurité / Protection*/

    /**
     * @var boolean
     *
     * @ORM\Column(name="si_gardien", type="boolean", nullable=true)
     */
    protected $siGardien;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_video", type="boolean", nullable=true)
     */
    protected $siVideophone;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_interphone", type="boolean", nullable=true)
     */
    protected $siInterphone;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_digicode", type="boolean", nullable=true)
     */
    protected $siDigicode;
    /**
     * @var boolean
     *
     * @ORM\Column(name="si_alarme", type="boolean", nullable=true)
     */
    protected $siAlarme;

    /*Localisation*/
    /**
     * @var boolean
     * @ORM\Column(name="proximiteAeroport", type="boolean")
     */
    protected $siProximiteAeroport;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteSortieAutoroute;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteGare;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteBus;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteTramway;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteParking;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteCommerces;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteCentreCommercial;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteCentreville;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteCreche;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteEcolePrimaire;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteCollege;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteLycee;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteUniversite;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siProximiteEspacesVerts;
    /*Avantages environnementaux*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siEcoQuartier;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $siBioConstruction;
    /*Avantages Investissement*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $loiPinel;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $loiDuflot;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $censiBouvard;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $LMP_LMNP;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $loi_Girardin_Paul;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $loi_Malraux;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $loi_Demessine;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $regime_SCPI;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $nue_Propriete;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $pret_Locatif_Social;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $PSLA;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $monumentHistorique;
    /**
     * Elligible au Prêt à taux zéro +
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $elligiblePret;
    /**
     * Bénéficie de la TVA 5,5%
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $tva_1;
    /**
     * Bénéficie de la TVA 7%
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $tva_2;
    /*Certifications et labels*/
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $hauteQualiteEnvironnementale;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $nfLogement;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $labelHabitatEnvironnement;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $THPE;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $HPE_ENR;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $labelBatimentBasseConsommation;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $labelHabitatEnvironnementEffinergie;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $planLocalHabitat;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $RT2012;
    /*Bons Plans*/
    /**
     * @ORM\Column(type="string",length=70, nullable=true)
     */
    protected $titreOffre;

    /**
     * @ORM\Column(type="text",length=4000, nullable=true)
     */
    protected $descriptionOffre;

    /**
     * @ORM\Column(type="string",length=70, nullable=true)
     */
    protected $mentionLegalOffre;
    /**
     * The updating date of the house model
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateDebutOffre;
    /**
     * The updating date of the house model
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateFinOffre;
    /*DPE*/
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $dpeValue;

    /**
     * @var string
     *
     * @ORM\Column(name="dpe", type="string",length=2, nullable=true)
     */
    protected $dpe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $gesValue;

    /**
     * @var string
     *
     * @ORM\Column(name="ges", type="string",length=2, nullable=true)
     */
    protected $ges;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float", nullable=true)
     */
    protected $percent;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Ad", mappedBy="program",cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     */
    protected $ads;
    
    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="program",cascade={"persist", "remove"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * @ORM\JoinColumn(name="diffusion",referencedColumnName="id", onDelete="CASCADE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Broadcast\Diffusion", cascade={"persist", "remove"}, fetch="EAGER", orphanRemoval=true)
     */
    protected $diffusion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published = false;

    /**
     * @var string
     * @ORM\Column(name="stat", type="string" ,nullable=false)
     */
    protected $stat;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_phone", type="string", length=14, nullable=true)
     */
    protected $trackingPhone;

    /*Détails de commercialisation*/
    /**
     * @var boolean
     *
     * @ORM\Column(name="avant_premiere", type="boolean")
     */
    protected $avantPremiere = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="lancement_commercial", type="boolean")
     */
    protected $lancementCommercial = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="demarrage_travaux", type="boolean")
     */
    protected $demarrageTravaux = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="travaux_en_cours", type="boolean")
     */
    protected $travauxEnCours = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="livraison_immediate", type="boolean")
     */
    protected $livraisonImmediate = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="livraison_1e_tranche", type="boolean")
     */
    protected $livraison1eTranche = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="livraison_2e_tranche", type="boolean")
     */
    protected $livraison2eTranche = false;
    /**
     * @var boolean
     *
     * @ORM\Column(name="derniere_opportunite", type="boolean")
     */
    protected $derniereOpportunite = false;

    //ENVIRONNEMENT
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $belleVue;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $standing;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $exposition;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $vueSurMer = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $calme = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="toit_Terrasse", type="boolean", nullable=true)
     */
    protected $toitTerrasse;

    /**
     * @var string
     *
     * @ORM\Column(name="grenier", type="string", length=48, nullable=true)
     */
    protected $grenier = false;

    //LOTISSEMENT
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $libreConstruction = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siViabilise = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="array", nullable=true)
     */
    protected $viabilise;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $constructible = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $servitude = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pente = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $typeTerrain;

    public function __construct(){
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->nbLots = 0;
        $this->nbLotsDisponible = 0;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Program
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Program
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Program
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Program
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Program
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set referenceInterne
     *
     * @param string $referenceInterne
     *
     * @return Program
     */
    public function setReferenceInterne($referenceInterne)
    {
        $this->referenceInterne = $referenceInterne;

        return $this;
    }

    /**
     * Get referenceInterne
     *
     * @return string
     */
    public function getReferenceInterne()
    {
        return $this->referenceInterne;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Program
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Program
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Program
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Program
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Program
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set dateCommercialisation
     *
     * @param \DateTime $dateCommercialisation
     *
     * @return Program
     */
    public function setDateCommercialisation($dateCommercialisation)
    {
        $this->dateCommercialisation = $dateCommercialisation;

        return $this;
    }

    /**
     * Get dateCommercialisation
     *
     * @return \DateTime
     */
    public function getDateCommercialisation()
    {
        return $this->dateCommercialisation;
    }

    /**
     * Set dateLivraison
     *
     * @param \DateTime $dateLivraison
     *
     * @return Program
     */
    public function setDateLivraison($dateLivraison)
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    /**
     * Get dateLivraison
     *
     * @return \DateTime
     */
    public function getDateLivraison()
    {
        return $this->dateLivraison;
    }

    /**
     * Set investisseur
     *
     * @param boolean $investisseur
     *
     * @return Program
     */
    public function setInvestisseur($investisseur)
    {
        $this->investisseur = $investisseur;

        return $this;
    }

    /**
     * Get investisseur
     *
     * @return boolean
     */
    public function getInvestisseur()
    {
        return $this->investisseur;
    }

    /**
     * Set habitat
     *
     * @param boolean $habitat
     *
     * @return Program
     */
    public function setHabitat($habitat)
    {
        $this->habitat = $habitat;

        return $this;
    }

    /**
     * Get habitat
     *
     * @return boolean
     */
    public function getHabitat()
    {
        return $this->habitat;
    }

    /**
     * Set typologieResidence
     *
     * @param string $typologieResidence
     *
     * @return Program
     */
    public function setTypologieResidence($typologieResidence)
    {
        $this->typologieResidence = $typologieResidence;

        return $this;
    }

    /**
     * Get typologieResidence
     *
     * @return string
     */
    public function getTypologieResidence()
    {
        return $this->typologieResidence;
    }

    /**
     * Set environnement
     *
     * @param string $environnement
     *
     * @return Program
     */
    public function setEnvironnement($environnement)
    {
        $this->environnement = $environnement;

        return $this;
    }

    /**
     * Get environnement
     *
     * @return string
     */
    public function getEnvironnement()
    {
        return $this->environnement;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return Program
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return Program
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set nbRemonte
     *
     * @param integer $nbRemonte
     *
     * @return Program
     */
    public function setNbRemonte($nbRemonte)
    {
        $this->nbRemonte = $nbRemonte;

        return $this;
    }

    /**
     * Get nbRemonte
     *
     * @return integer
     */
    public function getNbRemonte()
    {
        return $this->nbRemonte;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return Program
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ads[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ads->removeElement($ad);
    }

    /**
     * Get ads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAds()
    {
        return $this->ads;
    }


    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPdfDocuments()
    {
        $return = [];
        foreach ($this->documents as $document) {
            if($document->getMimeType() == 'application/pdf') {
                $return[] = $document;
            }
        }
        return $return;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagesDocuments()
    {
        $return = [];
        foreach ($this->documents as $document) {
            if($document->getMimeType() != 'application/pdf') {
                $return[] = $document;
            }
        }
        return $return;
    }


    /**
     * Set lienVisitVirtuel
     *
     * @param string $lienVisitVirtuel
     *
     * @return Program
     */
    public function setLienVisitVirtuel($lienVisitVirtuel)
    {
        $this->lienVisitVirtuel = $lienVisitVirtuel;

        return $this;
    }

    /**
     * Get lienVisitVirtuel
     *
     * @return string
     */
    public function getLienVisitVirtuel()
    {
        return $this->lienVisitVirtuel;
    }

    /**
     * Set titreImagePrincipale
     *
     * @param string $titreImagePrincipale
     *
     * @return Program
     */
    public function setTitreImagePrincipale($titreImagePrincipale)
    {
        $this->titreImagePrincipale = $titreImagePrincipale;

        return $this;
    }

    /**
     * Get titreImagePrincipale
     *
     * @return string
     */
    public function getTitreImagePrincipale()
    {
        return $this->titreImagePrincipale;
    }

    /**
     * Set imagePrincipale
     *
     * @param string $imagePrincipale
     *
     * @return Program
     */
    public function setImagePrincipale($imagePrincipale)
    {
        $this->imagePrincipale = $imagePrincipale;

        return $this;
    }

    /**
     * Get imagePrincipale
     *
     * @return string
     */
    public function getImagePrincipale()
    {
        return $this->imagePrincipale;
    }

    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return Program
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $document->setProgram($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set remoteId
     *
     * @param string $remoteId
     *
     * @return Program
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set typeDeChauffage
     *
     * @param integer $typeDeChauffage
     *
     * @return Program
     */
    public function setTypeDeChauffage($typeDeChauffage)
    {
        $this->typeDeChauffage = $typeDeChauffage;

        return $this;
    }

    /**
     * Get typeDeChauffage
     *
     * @return integer
     */
    public function getTypeDeChauffage()
    {
        return $this->typeDeChauffage;
    }

    /**
     * Set energieDeChauffage
     *
     * @param string $energieDeChauffage
     *
     * @return Program
     */
    public function setEnergieDeChauffage($energieDeChauffage)
    {
        $this->energieDeChauffage = $energieDeChauffage;

        return $this;
    }

    /**
     * Get energieDeChauffage
     *
     * @return string
     */
    public function getEnergieDeChauffage()
    {
        return $this->energieDeChauffage;
    }

    /**
     * Set revetementDeSol
     *
     * @param string $revetementDeSol
     *
     * @return Program
     */
    public function setRevetementDeSol($revetementDeSol)
    {
        $this->revetementDeSol = $revetementDeSol;

        return $this;
    }

    /**
     * Get revetementDeSol
     *
     * @return string
     */
    public function getRevetementDeSol()
    {
        return $this->revetementDeSol;
    }

    /**
     * Set vitrage
     *
     * @param string $vitrage
     *
     * @return Program
     */
    public function setVitrage($vitrage)
    {
        $this->vitrage = $vitrage;

        return $this;
    }

    /**
     * Get vitrage
     *
     * @return string
     */
    public function getVitrage()
    {
        return $this->vitrage;
    }

    /**
     * Set siPlacards
     *
     * @param boolean $siPlacards
     *
     * @return Program
     */
    public function setSiPlacards($siPlacards)
    {
        $this->siPlacards = $siPlacards;

        return $this;
    }

    /**
     * Get siPlacards
     *
     * @return boolean
     */
    public function getSiPlacards()
    {
        return $this->siPlacards;
    }


    /**
     * Set siGarage
     *
     * @param array|boolean $garage
     *
     * @return Program
     */
    public function setSiGarage($garage)
    {
        $this->siGarage = $garage['si'];
        $this->nbGarage = $garage['number'];

        return $this;
    }

    /**
     * Get siGarage
     *
     * @return boolean
     */
    public function getSiGarage()
    {
        return [
            'si' => $this->siGarage,
            'number' => $this->nbGarage
        ];
    }

    /**
     * Get setSiGarage
     *
     * @return boolean
     */
    public function setSiGarageValue()
    {
        return $this->siGarage;
    }

    /**
     * Set siBox
     *
     * @param boolean $siBox
     *
     * @return Program
     */
    public function setSiBox($siBox)
    {
        $this->siBox = $siBox;

        return $this;
    }

    /**
     * Get siBox
     *
     * @return boolean
     */
    public function getSiBox()
    {
        return $this->siBox;
    }

    /**
     * Set siAscenseur
     *
     * @param boolean $siAscenseur
     *
     * @return Program
     */
    public function setSiAscenseur($siAscenseur)
    {
        $this->siAscenseur = $siAscenseur;

        return $this;
    }

    /**
     * Get siAscenseur
     *
     * @return boolean
     */
    public function getSiAscenseur()
    {
        return $this->siAscenseur;
    }

    /**
     * Set siTerrasse
     *
     * @param boolean $siTerrasse
     *
     * @return Program
     */
    public function setSiTerrasse($siTerrasse)
    {
        $this->siTerrasse = $siTerrasse;

        return $this;
    }

    /**
     * Get siTerrasse
     *
     * @return boolean
     */
    public function getSiTerrasse()
    {
        return $this->siTerrasse;
    }

    /**
     * Set siBalcons
     *
     * @param boolean $siBalcons
     *
     * @return Program
     */
    public function setSiBalcons($siBalcons)
    {
        $this->siBalcons = $siBalcons;

        return $this;
    }

    /**
     * Get siBalcons
     *
     * @return boolean
     */
    public function getSiBalcons()
    {
        return $this->siBalcons;
    }

    /**
     * Set siLoggia
     *
     * @param boolean $siLoggia
     *
     * @return Program
     */
    public function setSiLoggia($siLoggia)
    {
        $this->siLoggia = $siLoggia;

        return $this;
    }

    /**
     * Get siLoggia
     *
     * @return boolean
     */
    public function getSiLoggia()
    {
        return $this->siLoggia;
    }

    /**
     * Set siCave
     *
     * @param boolean $siCave
     *
     * @return Program
     */
    public function setSiCave($siCave)
    {
        $this->siCave = $siCave;

        return $this;
    }

    /**
     * Get siCave
     *
     * @return boolean
     */
    public function getSiCave()
    {
        return $this->siCave;
    }

    /**
     * Set siJardin
     *
     * @param boolean $siJardin
     *
     * @return Program
     */
    public function setSiJardin($siJardin)
    {
        $this->siJardin = $siJardin;

        return $this;
    }

    /**
     * Get siJardin
     *
     * @return boolean
     */
    public function getSiJardin()
    {
        return $this->siJardin;
    }

    /**
     * Set siVoletsRoulantsElectriques
     *
     * @param boolean $siVoletsRoulantsElectriques
     *
     * @return Program
     */
    public function setSiVoletsRoulantsElectriques($siVoletsRoulantsElectriques)
    {
        $this->siVoletsRoulantsElectriques = $siVoletsRoulantsElectriques;

        return $this;
    }

    /**
     * Get siVoletsRoulantsElectriques
     *
     * @return boolean
     */
    public function getSiVoletsRoulantsElectriques()
    {
        return $this->siVoletsRoulantsElectriques;
    }

    /**
     * Set siClimatise
     *
     * @param boolean $siClimatise
     *
     * @return Program
     */
    public function setSiClimatise($siClimatise)
    {
        $this->siClimatise = $siClimatise;

        return $this;
    }

    /**
     * Get siClimatise
     *
     * @return boolean
     */
    public function getSiClimatise()
    {
        return $this->siClimatise;
    }

    /**
     * Set siTV
     *
     * @param boolean $siTV
     *
     * @return Program
     */
    public function setSiTV($siTV)
    {
        $this->siTV = $siTV;

        return $this;
    }

    /**
     * Get siTV
     *
     * @return boolean
     */
    public function getSiTV()
    {
        return $this->siTV;
    }

    /**
     * Set siPMR
     *
     * @param boolean $siPMR
     *
     * @return Program
     */
    public function setSiPMR($siPMR)
    {
        $this->siPMR = $siPMR;

        return $this;
    }

    /**
     * Get siPMR
     *
     * @return boolean
     */
    public function getSiPMR()
    {
        return $this->siPMR;
    }

    /**
     * Set siPiscine
     *
     * @param boolean $siPiscine
     *
     * @return Program
     */
    public function setSiPiscine($siPiscine)
    {
        $this->siPiscine = $siPiscine;

        return $this;
    }

    /**
     * Get siPiscine
     *
     * @return boolean
     */
    public function getSiPiscine()
    {
        return $this->siPiscine;
    }

    /**
     * Set siGardien
     *
     * @param boolean $siGardien
     *
     * @return Program
     */
    public function setSiGardien($siGardien)
    {
        $this->siGardien = $siGardien;

        return $this;
    }

    /**
     * Get siGardien
     *
     * @return boolean
     */
    public function getSiGardien()
    {
        return $this->siGardien;
    }

    /**
     * Set siVideophone
     *
     * @param boolean $siVideophone
     *
     * @return Program
     */
    public function setSiVideophone($siVideophone)
    {
        $this->siVideophone = $siVideophone;

        return $this;
    }

    /**
     * Get siVideophone
     *
     * @return boolean
     */
    public function getSiVideophone()
    {
        return $this->siVideophone;
    }

    /**
     * Set siInterphone
     *
     * @param boolean $siInterphone
     *
     * @return Program
     */
    public function setSiInterphone($siInterphone)
    {
        $this->siInterphone = $siInterphone;

        return $this;
    }

    /**
     * Get siInterphone
     *
     * @return boolean
     */
    public function getSiInterphone()
    {
        return $this->siInterphone;
    }

    /**
     * Set siDigicode
     *
     * @param boolean $siDigicode
     *
     * @return Program
     */
    public function setSiDigicode($siDigicode)
    {
        $this->siDigicode = $siDigicode;

        return $this;
    }

    /**
     * Get siDigicode
     *
     * @return boolean
     */
    public function getSiDigicode()
    {
        return $this->siDigicode;
    }

    /**
     * Set siAlarme
     *
     * @param boolean $siAlarme
     *
     * @return Program
     */
    public function setSiAlarme($siAlarme)
    {
        $this->siAlarme = $siAlarme;

        return $this;
    }

    /**
     * Get siAlarme
     *
     * @return boolean
     */
    public function getSiAlarme()
    {
        return $this->siAlarme;
    }

      /**
     * Set loiPinel
     *
     * @param boolean $loiPinel
     *
     * @return Program
     */
    public function setLoiPinel($loiPinel)
    {
        $this->loiPinel = $loiPinel;

        return $this;
    }

    /**
     * Get loiPinel
     *
     * @return boolean
     */
    public function getLoiPinel()
    {
        return $this->loiPinel;
    }

    /**
     * Set loiDuflot
     *
     * @param boolean $loiDuflot
     *
     * @return Program
     */
    public function setLoiDuflot($loiDuflot)
    {
        $this->loiDuflot = $loiDuflot;

        return $this;
    }

    /**
     * Get loiDuflot
     *
     * @return boolean
     */
    public function getLoiDuflot()
    {
        return $this->loiDuflot;
    }

    /**
     * Set censiBouvard
     *
     * @param boolean $censiBouvard
     *
     * @return Program
     */
    public function setCensiBouvard($censiBouvard)
    {
        $this->censiBouvard = $censiBouvard;

        return $this;
    }

    /**
     * Get censiBouvard
     *
     * @return boolean
     */
    public function getCensiBouvard()
    {
        return $this->censiBouvard;
    }

    /**
     * Set lMPLMNP
     *
     * @param boolean $lMPLMNP
     *
     * @return Program
     */
    public function setLMPLMNP($lMPLMNP)
    {
        $this->LMP_LMNP = $lMPLMNP;

        return $this;
    }

    /**
     * Get lMPLMNP
     *
     * @return boolean
     */
    public function getLMPLMNP()
    {
        return $this->LMP_LMNP;
    }

    /**
     * Set loiGirardinPaul
     *
     * @param boolean $loiGirardinPaul
     *
     * @return Program
     */
    public function setLoiGirardinPaul($loiGirardinPaul)
    {
        $this->loi_Girardin_Paul = $loiGirardinPaul;

        return $this;
    }

    /**
     * Get loiGirardinPaul
     *
     * @return boolean
     */
    public function getLoiGirardinPaul()
    {
        return $this->loi_Girardin_Paul;
    }

    /**
     * Set loiMalraux
     *
     * @param boolean $loiMalraux
     *
     * @return Program
     */
    public function setLoiMalraux($loiMalraux)
    {
        $this->loi_Malraux = $loiMalraux;

        return $this;
    }

    /**
     * Get loiMalraux
     *
     * @return boolean
     */
    public function getLoiMalraux()
    {
        return $this->loi_Malraux;
    }

    /**
     * Set loiDemessine
     *
     * @param boolean $loiDemessine
     *
     * @return Program
     */
    public function setLoiDemessine($loiDemessine)
    {
        $this->loi_Demessine = $loiDemessine;

        return $this;
    }

    /**
     * Get loiDemessine
     *
     * @return boolean
     */
    public function getLoiDemessine()
    {
        return $this->loi_Demessine;
    }

    /**
     * Set regimeSCPI
     *
     * @param boolean $regimeSCPI
     *
     * @return Program
     */
    public function setRegimeSCPI($regimeSCPI)
    {
        $this->regime_SCPI = $regimeSCPI;

        return $this;
    }

    /**
     * Get regimeSCPI
     *
     * @return boolean
     */
    public function getRegimeSCPI()
    {
        return $this->regime_SCPI;
    }

    /**
     * Set pretLocatifSocial
     *
     * @param boolean $pretLocatifSocial
     *
     * @return Program
     */
    public function setPretLocatifSocial($pretLocatifSocial)
    {
        $this->pret_Locatif_Social = $pretLocatifSocial;

        return $this;
    }

    /**
     * Get pretLocatifSocial
     *
     * @return boolean
     */
    public function getPretLocatifSocial()
    {
        return $this->pret_Locatif_Social;
    }

    /**
     * Set pSLA
     *
     * @param boolean $pSLA
     *
     * @return Program
     */
    public function setPSLA($pSLA)
    {
        $this->PSLA = $pSLA;

        return $this;
    }

    /**
     * Get pSLA
     *
     * @return boolean
     */
    public function getPSLA()
    {
        return $this->PSLA;
    }

    /**
     * Set monumentHistorique
     *
     * @param boolean $monumentHistorique
     *
     * @return Program
     */
    public function setMonumentHistorique($monumentHistorique)
    {
        $this->monumentHistorique = $monumentHistorique;

        return $this;
    }

    /**
     * Get monumentHistorique
     *
     * @return boolean
     */
    public function getMonumentHistorique()
    {
        return $this->monumentHistorique;
    }

    /**
     * Set elligiblePret
     *
     * @param boolean $elligiblePret
     *
     * @return Program
     */
    public function setElligiblePret($elligiblePret)
    {
        $this->elligiblePret = $elligiblePret;

        return $this;
    }

    /**
     * Get elligiblePret
     *
     * @return boolean
     */
    public function getElligiblePret()
    {
        return $this->elligiblePret;
    }

    /**
     * Set tva1
     *
     * @param boolean $tva1
     *
     * @return Program
     */
    public function setTva1($tva1)
    {
        $this->tva_1 = $tva1;

        return $this;
    }

    /**
     * Get tva1
     *
     * @return boolean
     */
    public function getTva1()
    {
        return $this->tva_1;
    }

    /**
     * Set tva2
     *
     * @param boolean $tva2
     *
     * @return Program
     */
    public function setTva2($tva2)
    {
        $this->tva_2 = $tva2;

        return $this;
    }

    /**
     * Get tva2
     *
     * @return boolean
     */
    public function getTva2()
    {
        return $this->tva_2;
    }

    /**
     * Set hauteQualiteEnvironnementale
     *
     * @param boolean $hauteQualiteEnvironnementale
     *
     * @return Program
     */
    public function setHauteQualiteEnvironnementale($hauteQualiteEnvironnementale)
    {
        $this->hauteQualiteEnvironnementale = $hauteQualiteEnvironnementale;

        return $this;
    }

    /**
     * Get hauteQualiteEnvironnementale
     *
     * @return boolean
     */
    public function getHauteQualiteEnvironnementale()
    {
        return $this->hauteQualiteEnvironnementale;
    }

    /**
     * Set nfLogement
     *
     * @param boolean $nfLogement
     *
     * @return Program
     */
    public function setNfLogement($nfLogement)
    {
        $this->nfLogement = $nfLogement;

        return $this;
    }

    /**
     * Get nfLogement
     *
     * @return boolean
     */
    public function getNfLogement()
    {
        return $this->nfLogement;
    }

    /**
     * Set labelHabitatEnvironnement
     *
     * @param boolean $labelHabitatEnvironnement
     *
     * @return Program
     */
    public function setLabelHabitatEnvironnement($labelHabitatEnvironnement)
    {
        $this->labelHabitatEnvironnement = $labelHabitatEnvironnement;

        return $this;
    }

    /**
     * Get labelHabitatEnvironnement
     *
     * @return boolean
     */
    public function getLabelHabitatEnvironnement()
    {
        return $this->labelHabitatEnvironnement;
    }

    /**
     * Set tHPE
     *
     * @param boolean $tHPE
     *
     * @return Program
     */
    public function setTHPE($tHPE)
    {
        $this->THPE = $tHPE;

        return $this;
    }

    /**
     * Get tHPE
     *
     * @return boolean
     */
    public function getTHPE()
    {
        return $this->THPE;
    }

    /**
     * Set hPEENR
     *
     * @param boolean $hPEENR
     *
     * @return Program
     */
    public function setHPEENR($hPEENR)
    {
        $this->HPE_ENR = $hPEENR;

        return $this;
    }

    /**
     * Get hPEENR
     *
     * @return boolean
     */
    public function getHPEENR()
    {
        return $this->HPE_ENR;
    }

    /**
     * Set labelBatimentBasseConsommation
     *
     * @param boolean $labelBatimentBasseConsommation
     *
     * @return Program
     */
    public function setLabelBatimentBasseConsommation($labelBatimentBasseConsommation)
    {
        $this->labelBatimentBasseConsommation = $labelBatimentBasseConsommation;

        return $this;
    }

    /**
     * Get labelBatimentBasseConsommation
     *
     * @return boolean
     */
    public function getLabelBatimentBasseConsommation()
    {
        return $this->labelBatimentBasseConsommation;
    }

    /**
     * Set labelHabitatEnvironnementEffinergie
     *
     * @param boolean $labelHabitatEnvironnementEffinergie
     *
     * @return Program
     */
    public function setLabelHabitatEnvironnementEffinergie($labelHabitatEnvironnementEffinergie)
    {
        $this->labelHabitatEnvironnementEffinergie = $labelHabitatEnvironnementEffinergie;

        return $this;
    }

    /**
     * Get labelHabitatEnvironnementEffinergie
     *
     * @return boolean
     */
    public function getLabelHabitatEnvironnementEffinergie()
    {
        return $this->labelHabitatEnvironnementEffinergie;
    }

    /**
     * Set planLocalHabitat
     *
     * @param boolean $planLocalHabitat
     *
     * @return Program
     */
    public function setPlanLocalHabitat($planLocalHabitat)
    {
        $this->planLocalHabitat = $planLocalHabitat;

        return $this;
    }

    /**
     * Get planLocalHabitat
     *
     * @return boolean
     */
    public function getPlanLocalHabitat()
    {
        return $this->planLocalHabitat;
    }

    /**
     * Set rT2012
     *
     * @param boolean $rT2012
     *
     * @return Program
     */
    public function setRT2012($rT2012)
    {
        $this->RT2012 = $rT2012;

        return $this;
    }

    /**
     * Get rT2012
     *
     * @return boolean
     */
    public function getRT2012()
    {
        return $this->RT2012;
    }

    /**
     * Set titreOffre
     *
     * @param string $titreOffre
     *
     * @return Program
     */
    public function setTitreOffre($titreOffre)
    {
        $this->titreOffre = $titreOffre;

        return $this;
    }

    /**
     * Get titreOffre
     *
     * @return string
     */
    public function getTitreOffre()
    {
        return $this->titreOffre;
    }

    /**
     * Set descriptionOffre
     *
     * @param string $descriptionOffre
     *
     * @return Program
     */
    public function setDescriptionOffre($descriptionOffre)
    {
        $this->descriptionOffre = $descriptionOffre;

        return $this;
    }

    /**
     * Get descriptionOffre
     *
     * @return string
     */
    public function getDescriptionOffre()
    {
        return $this->descriptionOffre;
    }

    /**
     * Set mentionLegalOffre
     *
     * @param string $mentionLegalOffre
     *
     * @return Program
     */
    public function setMentionLegalOffre($mentionLegalOffre)
    {
        $this->mentionLegalOffre = $mentionLegalOffre;

        return $this;
    }

    /**
     * Get mentionLegalOffre
     *
     * @return string
     */
    public function getMentionLegalOffre()
    {
        return $this->mentionLegalOffre;
    }

    /**
     * Set dateDebutOffre
     *
     * @param \DateTime $dateDebutOffre
     *
     * @return Program
     */
    public function setDateDebutOffre($dateDebutOffre)
    {
        $this->dateDebutOffre = $dateDebutOffre;

        return $this;
    }

    /**
     * Get dateDebutOffre
     *
     * @return \DateTime
     */
    public function getDateDebutOffre()
    {
        return $this->dateDebutOffre;
    }

    /**
     * Set dateFinOffre
     *
     * @param \DateTime $dateFinOffre
     *
     * @return Program
     */
    public function setDateFinOffre($dateFinOffre)
    {
        $this->dateFinOffre = $dateFinOffre;

        return $this;
    }

    /**
     * Get dateFinOffre
     *
     * @return \DateTime
     */
    public function getDateFinOffre()
    {
        return $this->dateFinOffre;
    }

    /**
     * Set dpeValue
     *
     * @param integer $dpeValue
     *
     * @return Program
     */
    public function setDpeValue($dpeValue)
    {
        $this->dpeValue = $dpeValue;

        return $this;
    }

    /**
     * Get dpeValue
     *
     * @return integer
     */
    public function getDpeValue()
    {
        return $this->dpeValue;
    }

    /**
     * Set dpe
     *
     * @param string $dpe
     *
     * @return Program
     */
    public function setDpe($dpe)
    {
        $this->dpe = $dpe;

        return $this;
    }

    /**
     * Get dpe
     *
     * @return string
     */
    public function getDpe()
    {
        return $this->dpe;
    }

    /**
     * Set gesValue
     *
     * @param integer $gesValue
     *
     * @return Program
     */
    public function setGesValue($gesValue)
    {
        $this->gesValue = $gesValue;

        return $this;
    }

    /**
     * Get gesValue
     *
     * @return integer
     */
    public function getGesValue()
    {
        return $this->gesValue;
    }

    /**
     * Set ges
     *
     * @param string $ges
     *
     * @return Program
     */
    public function setGes($ges)
    {
        $this->ges = $ges;

        return $this;
    }

    /**
     * Get ges
     *
     * @return string
     */
    public function getGes()
    {
        return $this->ges;
    }

    /**
     * Set siEcoQuartier
     *
     * @param boolean $siEcoQuartier
     *
     * @return Program
     */
    public function setSiEcoQuartier($siEcoQuartier)
    {
        $this->siEcoQuartier = $siEcoQuartier;

        return $this;
    }

    /**
     * Get siEcoQuartier
     *
     * @return boolean
     */
    public function getSiEcoQuartier()
    {
        return $this->siEcoQuartier;
    }

    /**
     * Set siBioConstruction
     *
     * @param boolean $siBioConstruction
     *
     * @return Program
     */
    public function setSiBioConstruction($siBioConstruction)
    {
        $this->siBioConstruction = $siBioConstruction;

        return $this;
    }

    /**
     * Get siBioConstruction
     *
     * @return boolean
     */
    public function getSiBioConstruction()
    {
        return $this->siBioConstruction;
    }

    /**
     * Set siProximiteAeroport
     *
     * @param boolean $siProximiteAeroport
     *
     * @return Program
     */
    public function setSiProximiteAeroport($siProximiteAeroport)
    {
        $this->siProximiteAeroport = $siProximiteAeroport;

        return $this;
    }

    /**
     * Get siProximiteAeroport
     *
     * @return boolean
     */
    public function getSiProximiteAeroport()
    {
        return $this->siProximiteAeroport;
    }

    /**
     * Set siProximiteSortieAutoroute
     *
     * @param boolean $siProximiteSortieAutoroute
     *
     * @return Program
     */
    public function setSiProximiteSortieAutoroute($siProximiteSortieAutoroute)
    {
        $this->siProximiteSortieAutoroute = $siProximiteSortieAutoroute;

        return $this;
    }

    /**
     * Get siProximiteSortieAutoroute
     *
     * @return boolean
     */
    public function getSiProximiteSortieAutoroute()
    {
        return $this->siProximiteSortieAutoroute;
    }

    /**
     * Set siProximiteGare
     *
     * @param boolean $siProximiteGare
     *
     * @return Program
     */
    public function setSiProximiteGare($siProximiteGare)
    {
        $this->siProximiteGare = $siProximiteGare;

        return $this;
    }

    /**
     * Get siProximiteGare
     *
     * @return boolean
     */
    public function getSiProximiteGare()
    {
        return $this->siProximiteGare;
    }

    /**
     * Set siProximiteBus
     *
     * @param boolean $siProximiteBus
     *
     * @return Program
     */
    public function setSiProximiteBus($siProximiteBus)
    {
        $this->siProximiteBus = $siProximiteBus;

        return $this;
    }

    /**
     * Get siProximiteBus
     *
     * @return boolean
     */
    public function getSiProximiteBus()
    {
        return $this->siProximiteBus;
    }

    /**
     * Set siProximiteTramway
     *
     * @param boolean $siProximiteTramway
     *
     * @return Program
     */
    public function setSiProximiteTramway($siProximiteTramway)
    {
        $this->siProximiteTramway = $siProximiteTramway;

        return $this;
    }

    /**
     * Get siProximiteTramway
     *
     * @return boolean
     */
    public function getSiProximiteTramway()
    {
        return $this->siProximiteTramway;
    }

    /**
     * Set siProximiteParking
     *
     * @param boolean $siProximiteParking
     *
     * @return Program
     */
    public function setSiProximiteParking($siProximiteParking)
    {
        $this->siProximiteParking = $siProximiteParking;

        return $this;
    }

    /**
     * Get siProximiteParking
     *
     * @return boolean
     */
    public function getSiProximiteParking()
    {
        return $this->siProximiteParking;
    }

    /**
     * Set siProximiteCommerces
     *
     * @param boolean $siProximiteCommerces
     *
     * @return Program
     */
    public function setSiProximiteCommerces($siProximiteCommerces)
    {
        $this->siProximiteCommerces = $siProximiteCommerces;

        return $this;
    }

    /**
     * Get siProximiteCommerces
     *
     * @return boolean
     */
    public function getSiProximiteCommerces()
    {
        return $this->siProximiteCommerces;
    }

    /**
     * Set siProximiteCentreCommercial
     *
     * @param boolean $siProximiteCentreCommercial
     *
     * @return Program
     */
    public function setSiProximiteCentreCommercial($siProximiteCentreCommercial)
    {
        $this->siProximiteCentreCommercial = $siProximiteCentreCommercial;

        return $this;
    }

    /**
     * Get siProximiteCentreCommercial
     *
     * @return boolean
     */
    public function getSiProximiteCentreCommercial()
    {
        return $this->siProximiteCentreCommercial;
    }

    /**
     * Set siProximiteCentreville
     *
     * @param boolean $siProximiteCentreville
     *
     * @return Program
     */
    public function setSiProximiteCentreville($siProximiteCentreville)
    {
        $this->siProximiteCentreville = $siProximiteCentreville;

        return $this;
    }

    /**
     * Get siProximiteCentreville
     *
     * @return boolean
     */
    public function getSiProximiteCentreville()
    {
        return $this->siProximiteCentreville;
    }

    /**
     * Set siProximiteCreche
     *
     * @param boolean $siProximiteCreche
     *
     * @return Program
     */
    public function setSiProximiteCreche($siProximiteCreche)
    {
        $this->siProximiteCreche = $siProximiteCreche;

        return $this;
    }

    /**
     * Get siProximiteCreche
     *
     * @return boolean
     */
    public function getSiProximiteCreche()
    {
        return $this->siProximiteCreche;
    }

    /**
     * Set siProximiteEcolePrimaire
     *
     * @param boolean $siProximiteEcolePrimaire
     *
     * @return Program
     */
    public function setSiProximiteEcolePrimaire($siProximiteEcolePrimaire)
    {
        $this->siProximiteEcolePrimaire = $siProximiteEcolePrimaire;

        return $this;
    }

    /**
     * Get siProximiteEcolePrimaire
     *
     * @return boolean
     */
    public function getSiProximiteEcolePrimaire()
    {
        return $this->siProximiteEcolePrimaire;
    }

    /**
     * Set siProximiteCollege
     *
     * @param boolean $siProximiteCollege
     *
     * @return Program
     */
    public function setSiProximiteCollege($siProximiteCollege)
    {
        $this->siProximiteCollege = $siProximiteCollege;

        return $this;
    }

    /**
     * Get siProximiteCollege
     *
     * @return boolean
     */
    public function getSiProximiteCollege()
    {
        return $this->siProximiteCollege;
    }

    /**
     * Set siProximiteLycee
     *
     * @param boolean $siProximiteLycee
     *
     * @return Program
     */
    public function setSiProximiteLycee($siProximiteLycee)
    {
        $this->siProximiteLycee = $siProximiteLycee;

        return $this;
    }

    /**
     * Get siProximiteLycee
     *
     * @return boolean
     */
    public function getSiProximiteLycee()
    {
        return $this->siProximiteLycee;
    }

    /**
     * Set siProximiteUniversite
     *
     * @param boolean $siProximiteUniversite
     *
     * @return Program
     */
    public function setSiProximiteUniversite($siProximiteUniversite)
    {
        $this->siProximiteUniversite = $siProximiteUniversite;

        return $this;
    }

    /**
     * Get siProximiteUniversite
     *
     * @return boolean
     */
    public function getSiProximiteUniversite()
    {
        return $this->siProximiteUniversite;
    }

    /**
     * Set siProximiteEspacesVerts
     *
     * @param boolean $siProximiteEspacesVerts
     *
     * @return Program
     */
    public function setSiProximiteEspacesVerts($siProximiteEspacesVerts)
    {
        $this->siProximiteEspacesVerts = $siProximiteEspacesVerts;

        return $this;
    }

    /**
     * Get siProximiteEspacesVerts
     *
     * @return boolean
     */
    public function getSiProximiteEspacesVerts()
    {
        return $this->siProximiteEspacesVerts;
    }

    /**
     * Set nuePropriete
     *
     * @param boolean $nuePropriete
     *
     * @return Program
     */
    public function setNuePropriete($nuePropriete)
    {
        $this->nue_Propriete = $nuePropriete;

        return $this;
    }

    /**
     * Get nuePropriete
     *
     * @return boolean
     */
    public function getNuePropriete()
    {
        return $this->nue_Propriete;
    }

    /**
     * Set nbetages
     *
     * @param integer $nbetages
     *
     * @return Program
     */
    public function setNbetages($nbetages)
    {
        $this->nbetages = $nbetages;

        return $this;
    }

    /**
     * Get nbetages
     *
     * @return integer
     */
    public function getNbetages()
    {
        return $this->nbetages;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Program
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set stat
     *
     * @param string $stat
     *
     * @return Program
     */
    public function setStat($stat)
    {
        $this->stat = $stat;

        return $this;
    }

    /**
     * Get stat
     *
     * @return string
     */
    public function getStat()
    {
        return $this->stat ? self::STATUS[$this->stat] : '';
    }

    public function getStatValue()
    {
        return $this->stat;
    }

    /**
     * Set diffusion
     *
     * @param \AppBundle\Entity\Broadcast\Diffusion $diffusion
     *
     * @return Program
     */
    public function setDiffusion(\AppBundle\Entity\Broadcast\Diffusion $diffusion = null)
    {
        $this->diffusion = $diffusion;

        return $this;
    }

    /**
     * Get diffusion
     *
     * @return \AppBundle\Entity\Broadcast\Diffusion
     */
    public function getDiffusion()
    {
        return $this->diffusion;
    }

    /**
     * Set typeAvancement
     *
     * @param string $typeAvancement
     *
     * @return Program
     */
    public function setTypeAvancement($typeAvancement)
    {
        $this->typeAvancement = $typeAvancement;

        return $this;
    }

    /**
     * Get typeAvancement
     *
     * @return string
     */
    public function getTypeAvancement()
    {
        return $this->typeAvancement;
    }

    /**
     * @return string
     */
    public function calculatePercent()
    {
        $proprety = [$this->title,$this->reference,$this->adresse, $this->codePostal, $this->description, $this->ville, $this->lienVisitVirtuel, $this->titreImagePrincipale, $this->imagePrincipale,$this->pays,
            $this->dateCommercialisation, $this->dateLivraison, $this->investisseur, $this->habitat,
            $this->typologieResidence, $this->environnement, $this->nbetages, $this->typeAvancement, $this->typeDeChauffage, $this->energieDeChauffage, $this->revetementDeSol, $this->vitrage,
            $this->siPlacards, $this->siGarage, $this->siInterphone, $this->siGardien, $this->siAscenseur, $this->siAlarme, $this->siBalcons, $this->siBox, $this->siCave,
            $this->siBioConstruction, $this->siClimatise, $this->siVoletsRoulantsElectriques, $this->siTV, $this->siPMR, $this->siPiscine, $this->siProximiteAeroport, $this->siProximiteBus,
            $this->siProximiteCentreville, $this->siProximiteCollege, $this->siProximiteCommerces, $this->siProximiteCreche, $this->siProximiteEcolePrimaire, $this->siProximiteGare, $this->siProximiteEspacesVerts,
            $this->siProximiteSortieAutoroute, $this->siProximiteTramway, $this->siProximiteTramway, $this->siProximiteUniversite, $this->siEcoQuartier,
            $this->loi_Demessine, $this->loi_Girardin_Paul, $this->loi_Malraux, $this->loiDuflot, $this->loiPinel, $this->censiBouvard, $this->LMP_LMNP,
            $this->regime_SCPI, $this->nue_Propriete, $this->pret_Locatif_Social, $this->monumentHistorique, $this->PSLA, $this->elligiblePret, $this->tva_1, $this->tva_2,
            $this->hauteQualiteEnvironnementale, $this->nfLogement, $this->labelHabitatEnvironnement, $this->THPE, $this->HPE_ENR, $this->labelBatimentBasseConsommation, $this->labelHabitatEnvironnementEffinergie,
            $this->planLocalHabitat, $this->RT2012
        ];
        $percent  = 0;
        $part     = 40/count($proprety);
        foreach ($proprety as $item) {
            if(!is_null($item) && $item !== '')
                $percent += $part;
        }
        $count           = count($this->ads);
        $percentageAds   = 0;
        if($count > 0) {
            /** @var \AppBundle\Entity\Ad $ad */
            foreach ($this->ads as $ad)
                $percentageAds += $ad->calculatePercent();
            $percent += (($percentageAds/$count)/100)*60;
        }
        return number_format($percent,2);
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return Program
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set typeJardin
     *
     * @param string $typeJardin
     *
     * @return Program
     */
    public function setTypeJardin($typeJardin)
    {
        $this->typeJardin = $typeJardin;

        return $this;
    }

    /**
     * Get typeJardin
     *
     * @return string
     */
    public function getTypeJardin()
    {
        return $this->typeJardin;
    }

    /**
     * Set avantPremiere
     *
     * @param boolean $avantPremiere
     *
     * @return Program
     */
    public function setAvantPremiere($avantPremiere)
    {
        $this->avantPremiere = $avantPremiere;

        return $this;
    }

    /**
     * Get avantPremiere
     *
     * @return boolean
     */
    public function getAvantPremiere()
    {
        return $this->avantPremiere;
    }

    /**
     * Set lancementCommercial
     *
     * @param boolean $lancementCommercial
     *
     * @return Program
     */
    public function setLancementCommercial($lancementCommercial)
    {
        $this->lancementCommercial = $lancementCommercial;

        return $this;
    }

    /**
     * Get lancementCommercial
     *
     * @return boolean
     */
    public function getLancementCommercial()
    {
        return $this->lancementCommercial;
    }

    /**
     * Set demarrageTravaux
     *
     * @param boolean $demarrageTravaux
     *
     * @return Program
     */
    public function setDemarrageTravaux($demarrageTravaux)
    {
        $this->demarrageTravaux = $demarrageTravaux;

        return $this;
    }

    /**
     * Get demarrageTravaux
     *
     * @return boolean
     */
    public function getDemarrageTravaux()
    {
        return $this->demarrageTravaux;
    }

    /**
     * Set travauxEnCours
     *
     * @param boolean $travauxEnCours
     *
     * @return Program
     */
    public function setTravauxEnCours($travauxEnCours)
    {
        $this->travauxEnCours = $travauxEnCours;

        return $this;
    }

    /**
     * Get travauxEnCours
     *
     * @return boolean
     */
    public function getTravauxEnCours()
    {
        return $this->travauxEnCours;
    }

    /**
     * Set livraisonImmediate
     *
     * @param boolean $livraisonImmediate
     *
     * @return Program
     */
    public function setLivraisonImmediate($livraisonImmediate)
    {
        $this->livraisonImmediate = $livraisonImmediate;

        return $this;
    }

    /**
     * Get livraisonImmediate
     *
     * @return boolean
     */
    public function getLivraisonImmediate()
    {
        return $this->livraisonImmediate;
    }

    /**
     * Set livraison1eTranche
     *
     * @param boolean $livraison1eTranche
     *
     * @return Program
     */
    public function setLivraison1eTranche($livraison1eTranche)
    {
        $this->livraison1eTranche = $livraison1eTranche;

        return $this;
    }

    /**
     * Get livraison1eTranche
     *
     * @return boolean
     */
    public function getLivraison1eTranche()
    {
        return $this->livraison1eTranche;
    }

    /**
     * Set livraison2eTranche
     *
     * @param boolean $livraison2eTranche
     *
     * @return Program
     */
    public function setLivraison2eTranche($livraison2eTranche)
    {
        $this->livraison2eTranche = $livraison2eTranche;

        return $this;
    }

    /**
     * Get livraison2eTranche
     *
     * @return boolean
     */
    public function getLivraison2eTranche()
    {
        return $this->livraison2eTranche;
    }

    /**
     * Set derniereOpportunite
     *
     * @param boolean $derniereOpportunite
     *
     * @return Program
     */
    public function setDerniereOpportunite($derniereOpportunite)
    {
        $this->derniereOpportunite = $derniereOpportunite;

        return $this;
    }

    /**
     * Get derniereOpportunite
     *
     * @return boolean
     */
    public function getDerniereOpportunite()
    {
        return $this->derniereOpportunite;
    }

    /**
     * Set nbGarage
     *
     * @param integer $nbGarage
     *
     * @return Program
     */
    public function setNbGarage($nbGarage)
    {
        $this->nbGarage = $nbGarage;

        return $this;
    }

    /**
     * Get nbGarage
     *
     * @return integer
     */
    public function getNbGarage()
    {
        return $this->nbGarage;
    }

    /**
     * Set nbParking
     *
     * @param integer $nbParking
     *
     * @return Program
     */
    public function setNbParking($nbParking)
    {
        $this->nbParking = $nbParking;

        return $this;
    }

    /**
     * Get nbParking
     *
     * @return integer
     */
    public function getNbParking()
    {
        return $this->nbParking;
    }

    /**
     * Set siParking
     *
     * @param boolean $siParking
     *
     * @return Program
     */
    public function setSiParking($siParking)
    {
        $this->siParking 	  = $siParking['si'];
        $this->nbParking 	  = $siParking['number'];

        return $this;
    }

    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParking()
    {
        return [
            'si'		=> $this->siParking,
            'number'	=> $this->nbParking,
        ];
    }
    /**
     * Get siParking
     *
     * @return boolean
     */
    public function getSiParkingValue()
    {
        return $this->siParking;
    }

    /**
     * Set belleVue
     *
     * @param boolean $belleVue
     *
     * @return Program
     */
    public function setBelleVue($belleVue)
    {
        $this->belleVue = $belleVue;

        return $this;
    }

    /**
     * Get belleVue
     *
     * @return boolean
     */
    public function getBelleVue()
    {
        return $this->belleVue;
    }

    /**
     * Set standing
     *
     * @param string $standing
     *
     * @return Program
     */
    public function setStanding($standing)
    {
        $this->standing = $standing;

        return $this;
    }

    /**
     * Get standing
     *
     * @return string
     */
    public function getStanding()
    {
        return $this->standing;
    }

    /**
     * Set exposition
     *
     * @param string $exposition
     *
     * @return Program
     */
    public function setExposition($exposition)
    {
        $this->exposition = $exposition;

        return $this;
    }

    /**
     * Get exposition
     *
     * @return string
     */
    public function getExposition()
    {
        return $this->exposition;
    }

    /**
     * Set vueSurMer
     *
     * @param boolean $vueSurMer
     *
     * @return Program
     */
    public function setVueSurMer($vueSurMer)
    {
        $this->vueSurMer = $vueSurMer;

        return $this;
    }

    /**
     * Get vueSurMer
     *
     * @return boolean
     */
    public function getVueSurMer()
    {
        return $this->vueSurMer;
    }

    /**
     * Set calme
     *
     * @param boolean $calme
     *
     * @return Program
     */
    public function setCalme($calme)
    {
        $this->calme = $calme;

        return $this;
    }

    /**
     * Get calme
     *
     * @return boolean
     */
    public function getCalme()
    {
        return $this->calme;
    }

    /**
     * Set toitTerrasse
     *
     * @param boolean $toitTerrasse
     *
     * @return Program
     */
    public function setToitTerrasse($toitTerrasse)
    {
        $this->toitTerrasse = $toitTerrasse;

        return $this;
    }

    /**
     * Get toitTerrasse
     *
     * @return boolean
     */
    public function getToitTerrasse()
    {
        return $this->toitTerrasse;
    }

    /**
     * Set grenier
     *
     * @param string $grenier
     *
     * @return Program
     */
    public function setGrenier($grenier)
    {
        $this->grenier = $grenier;

        return $this;
    }

    /**
     * Get grenier
     *
     * @return string
     */
    public function getGrenier()
    {
        return $this->grenier;
    }

    /**
     * Set nbLots
     *
     * @param integer $nbLots
     *
     * @return Program
     */
    public function setNbLots($nbLots)
    {
        $this->nbLots = $nbLots;

        return $this;
    }

    /**
     * Get nbLots
     *
     * @return integer
     */
    public function getNbLots()
    {
        return $this->nbLots;
    }

    /**
     * Set nbLotsDisponible
     *
     * @param integer $nbLotsDisponible
     *
     * @return Program
     */
    public function setNbLotsDisponible($nbLotsDisponible)
    {
        $this->nbLotsDisponible = $nbLotsDisponible;

        return $this;
    }

    /**
     * Get nbLotsDisponible
     *
     * @return integer
     */
    public function getNbLotsDisponible()
    {
        return $this->nbLotsDisponible;
    }

    /**
     * Set libreConstruction
     *
     * @param boolean $libreConstruction
     *
     * @return Program
     */
    public function setLibreConstruction($libreConstruction)
    {
        $this->libreConstruction = $libreConstruction;

        return $this;
    }

    /**
     * Get libreConstruction
     *
     * @return boolean
     */
    public function getLibreConstruction()
    {
        return $this->libreConstruction;
    }


    /**
     * Set constructible
     *
     * @param boolean $constructible
     *
     * @return Program
     */
    public function setConstructible($constructible)
    {
        $this->constructible = $constructible;

        return $this;
    }

    /**
     * Get constructible
     *
     * @return boolean
     */
    public function getConstructible()
    {
        return $this->constructible;
    }

    /**
     * Set servitude
     *
     * @param boolean $servitude
     *
     * @return Program
     */
    public function setServitude($servitude)
    {
        $this->servitude = $servitude;

        return $this;
    }

    /**
     * Get servitude
     *
     * @return boolean
     */
    public function getServitude()
    {
        return $this->servitude;
    }

    /**
     * Set pente
     *
     * @param boolean $pente
     *
     * @return Program
     */
    public function setPente($pente)
    {
        $this->pente = $pente;

        return $this;
    }

    /**
     * Get pente
     *
     * @return boolean
     */
    public function getPente()
    {
        return $this->pente;
    }

    /**
     * Set typeTerrain
     *
     * @param string $typeTerrain
     *
     * @return Program
     */
    public function setTypeTerrain($typeTerrain)
    {
        $this->typeTerrain = $typeTerrain;

        return $this;
    }

    /**
     * Get typeTerrain
     *
     * @return string
     */
    public function getTypeTerrain()
    {
        return $this->typeTerrain;
    }

    /**
     * Set siViabilise
     *
     * @param boolean $siViabilise
     *
     * @return Program
     */
    public function setSiViabilise($siViabilise)
    {
        $this->siViabilise 	  = $siViabilise['si'];
        $this->viabilise 	  = $siViabilise['choices'];

        return $this;
    }

    /**
     * Get siViabilise
     *
     * @return array
     */
    public function getSiViabilise()
    {
        return [
            'si'		=> $this->siViabilise,
            'choices'	=> $this->viabilise,
        ];
    }

    /**
     * Set viabilise
     *
     * @param array $viabilise
     *
     * @return Program
     */
    public function setViabilise($viabilise)
    {
        $this->viabilise = $viabilise;

        return $this;
    }

    /**
     * Get viabilise
     *
     * @return array
     */
    public function getViabilise()
    {
        return $this->viabilise;
    }

    /**
     * Set adresseBulleDeVente
     *
     * @param string $adresseBulleDeVente
     *
     * @return Program
     */
    public function setAdresseBulleDeVente($adresseBulleDeVente)
    {
        $this->adresseBulleDeVente = $adresseBulleDeVente;

        return $this;
    }

    /**
     * Get adresseBulleDeVente
     *
     * @return string
     */
    public function getAdresseBulleDeVente()
    {
        return $this->adresseBulleDeVente;
    }

    /**
     * @return string
     */
    public function getTrackingPhone()
    {
        return preg_replace("/^33/", "0", $this->trackingPhone,1);
    }

    public function getNbAds()
    {
        return count($this->getAds());
    }

    /**
     * @param string $trackingPhone
     */
    public function setTrackingPhone($trackingPhone)
    {
        $this->trackingPhone = $trackingPhone;
    }
}
