<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;
use AppBundle\Interfaces\Comparable;
use AppBundle\Interfaces\EqualityComparable;
use AppBundle\Interfaces\The;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HouseModelRepository")
 * @ORM\Table(name="house_model")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 */
class HouseModel
{
    const PENDING  = 'en-attente';
    const VALIDATE = 'valider';
    const ANNULER  = 'annuler';
    const STATUS = array(
        'en-attente' => 'En&nbsp;attente de&nbsp;validation',
        'valider'    => 'Valider',
        'annuler'    => 'Annuler'
    );

	public function __construct()
	{
		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();
        $this->maison    = new Maison();
	}

    /**
     * The unique id of the house model
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $remoteId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $importIdentifier;

	/**
     * The owner of the house model
     * @ORM\JoinColumn(onDelete="cascade")
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 */
	protected $owner;

    /**
     * The owner group of the housemodel
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="housemodel")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
	protected $group;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Please enter the agency name.")
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The agency name is too short.",
     *     maxMessage="The agency name is too long."
     * )
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $titreImagePrincipale;

    /**
     * @ORM\Column(name="imagePrincipale", type="string", length=255, nullable=true)
     * @Gedmo\UploadableFilePath
     * @Assert\File(
     *     mimeTypes={"image/jpeg", "image/pjpeg"}
     * )
     */
    private $imagePrincipale = "";

    /**
     *
     * @var ArrayCollection
     * @OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="houseModel",cascade={"persist", "remove"}, fetch="EAGER")
     */
    protected $documents;

    /**
     * The creating date of the house model
     * @ORM\Column(type="datetime")
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * The updating date of the house model
     * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="update")
	 */
    protected $updatedAt;

	/**
     * @var \AppBundle\Entity\Maison
     * @ORM\JoinColumn(onDelete="CASCADE")
	 * @ORM\OneToOne(targetEntity="AppBundle\Entity\Maison",cascade={"persist", "remove"}, fetch="EAGER")
	 */
	protected $maison;

    /**
     * @var
     * @OneToMany(targetEntity="AppBundle\Entity\Ad", mappedBy="houseModel")
     *@JoinColumn(onDelete="SET NULL")
     */
    protected $ad;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float", nullable=true)
     */
    protected $percent;

    /**
     * @var string
     * @ORM\Column(name="stat", type="string" ,nullable=false)
     */
    protected $stat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible_by_all", type="boolean")
     */
    protected $visibleByAll = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return HouseModel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add document
     *
     * @param \AppBundle\Entity\Document $document
     *
     * @return Ad
     */
    public function addDocument(\AppBundle\Entity\Document $document)
    {
        $document->setHouseModel($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \AppBundle\Entity\Document $document
     */
    public function removeDocument(\AppBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }


    public function setDocuments($documents = null)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPdfDocuments()
    {
        $return = [];
        if($this->documents) {
            foreach ($this->documents as $document) {
                if($document->getMimeType() == 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return $return;
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagesDocuments()
    {
        $return = [];
        if($this->documents) {
            foreach ($this->documents as $document) {
                if($document->getMimeType() != 'application/pdf') {
                    $return[] = $document;
                }
            }
        }
        return $return;
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return HouseModel
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return HouseModel
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set maison
     *
     * @param \AppBundle\Entity\Maison $maison
     *
     * @return HouseModel
     */
    public function setMaison(\AppBundle\Entity\Maison $maison)
    {
        $this->maison = $maison;

        return $this;
    }

    /**
     * Get maison
     *
     * @return \AppBundle\Entity\Maison
     */
    public function getMaison()
    {
        return $this->maison;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return HouseModel
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return HouseModel
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set stat
     *
     * @param string $stat
     *
     * @return Maison
     */
    public function setStat($stat)
    {
        $this->stat = $stat;

        return $this;
    }

    /**
     * Get stat
     *
     * @return string
     */
    public function getStat()
    {
        return $this->stat ? self::STATUS[$this->stat] : '';
    }

    public function getStatvalue()
    {
        return $this->stat;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return HouseModel
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return User
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

	public function __clone() {
		if ($this->id) {
			$this->setName($this->name.' (clone)');
			$this->remoteId = null;
			$maison = clone $this->maison;
			$this->setMaison($maison);
			$this->id = null;

            $documents = $this->documents;
            $this->documents = new ArrayCollection();
            foreach ($documents as $document) {
                $cloneDocument = clone $document;
                $cloneDocument->setHouseModel($this);
                $this->documents->add($cloneDocument);
            }
		}
	}

    /**
     * Set visibleByAll
     *
     * @param boolean $visibleByAll
     *
     * @return HouseModel
     */
    public function setVisibleByAll($visibleByAll)
    {
        $this->visibleByAll = $visibleByAll;

        return $this;
    }

    /**
     * Get visibleByAll
     *
     * @return boolean
     */
    public function getVisibleByAll()
    {
        return $this->visibleByAll;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return HouseModel
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }

    /**
     * Get ad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set titreImagePrincipale
     *
     * @param string $titreImagePrincipale
     *
     * @return HouseModel
     */
    public function setTitreImagePrincipale($titreImagePrincipale)
    {
        $this->titreImagePrincipale = $titreImagePrincipale;

        return $this;
    }

    /**
     * Get titreImagePrincipale
     *
     * @return string
     */
    public function getTitreImagePrincipale()
    {
        return $this->titreImagePrincipale;
    }

    /**
     * Set imagePrincipale
     *
     * @param string $imagePrincipale
     *
     * @return HouseModel
     */
    public function setImagePrincipale($imagePrincipale)
    {
        $this->imagePrincipale = $imagePrincipale;

        return $this;
    }

    /**
     * Get imagePrincipale
     *
     * @return string
     */
    public function getImagePrincipale()
    {
        return $this->imagePrincipale;
    }


    public function calculatePercent()
    {
        return number_format($this->getMaison()->calculatePercent(),2);
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return HouseModel
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set importIdentifier
     *
     * @param string $importIdentifier
     *
     * @return HouseModel
     */
    public function setImportIdentifier($importIdentifier)
    {
        $this->importIdentifier = $importIdentifier;

        return $this;
    }

    /**
     * Get importIdentifier
     *
     * @return string
     */
    public function getImportIdentifier()
    {
        return $this->importIdentifier;
    }
}
