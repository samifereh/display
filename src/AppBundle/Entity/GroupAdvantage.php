<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupAdvantageRepository")
 * @ORM\Table(name="group_advantages")
 **/
class GroupAdvantage
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    protected $quantite;

    /**
     * @var boolean
     * @ORM\Column(name="payed", type="boolean")
     */
    protected $payed = false;

    /**
     * @ORM\Column(name="date_begin", type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateBegin;

    /**
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateEnd;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Configuration\Advantage", inversedBy="groupsAdvantage",cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="advantage_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advantage;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="advantages")
     * @ORM\JoinColumn(onDelete="SET NULL",nullable=true)
     */
    protected $group;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return GroupAdvantage
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set dateBegin
     *
     * @param \DateTime $dateBegin
     *
     * @return GroupAdvantage
     */
    public function setDateBegin($dateBegin)
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    /**
     * Get dateBegin
     *
     * @return \DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return GroupAdvantage
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return GroupAdvantage
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return boolean
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set advantage
     *
     * @param \AppBundle\Entity\Configuration\Advantage $advantage
     *
     * @return GroupAdvantage
     */
    public function setAdvantage(\AppBundle\Entity\Configuration\Advantage $advantage = null)
    {
        $this->advantage = $advantage;

        return $this;
    }

    /**
     * Get advantage
     *
     * @return \AppBundle\Entity\Configuration\Advantage
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return GroupAdvantage
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return GroupAdvantage
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
}
