<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 05/08/16
 * Time: 15:23
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinColumns;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\PersistentCollection;

/**
 * AdMentionLegals
 *
 * @ORM\Table(name="ad_mentions_legals")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Broadcast\AdMentionLegalsRepository")
 */
class AdMentionLegals
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Group
     *
     * @ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="adRewrite")
     * @JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var Portal
     *
     * @ManyToMany(targetEntity="AppBundle\Entity\Broadcast\Portal", cascade={"persist"}, fetch="EAGER")
     */
    protected $portals;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     */
    protected $body;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->portals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return AdMentionLegals
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return AdMentionLegals
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Add portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     *
     * @return AdMentionLegals
     */
    public function addPortal(\AppBundle\Entity\Broadcast\Portal $portal)
    {
        $this->portals[] = $portal;

        return $this;
    }

    /**
     * Remove portal
     *
     * @param \AppBundle\Entity\Broadcast\Portal $portal
     */
    public function removePortal(\AppBundle\Entity\Broadcast\Portal $portal)
    {
        $this->portals->removeElement($portal);
    }

    /**
     * Get portals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPortals()
    {
        return $this->portals;
    }
}
