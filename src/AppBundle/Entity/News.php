<?php

// src/AppBundle/Entity/News.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * The unique id of the news
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;

     /**
      * The title of the news
      * @ORM\Column(type="string", length=255)
      *
      * @Assert\NotBlank(message="Please enter a title.")
      * @Assert\Length(
      *     min=3,
      *     max=255,
      *     minMessage="The title is too short.",
      *     maxMessage="The title is too long.",
      * )
      */
    protected $title;

     /**
      * The content of the news
     * @ORM\Column(type="blob")
     *
     * @Assert\NotBlank(message="Please enter a content.")
     */
    protected $content;

     /**
      * The creating? date of the news
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * The author of the news
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
    */
    protected $author;

    /**
     * The slug of the views. It's created from the title
    * @Gedmo\Slug(fields={"title"})
    * @ORM\Column(length=128, unique=true)
    */
    private $slug;

    /**
     * The number of views of the news
    * @ORM\Column(type="integer")
    */
    private $nbViews = 0;

    public function __construct()
    {
    }

    public function setId($id){ $this->id = $id; }
    public function getId(){ return $this->id; }

    public function setTitle($title){ $this->title = $title; }
    public function getTitle(){ return $this->title; }

    public function setContent($content){ $this->content = $content; }
    public function getContent(){
        if($this->content) {
            rewind($this->content);
            return stream_get_contents($this->content);
        }
        return null;
    }

    public function setDate($date){ $this->date = $date; }
    public function getDate(){ return $this->date; }

    public function setAuthor(User $author){ $this->author = $author; }
    public function getAuthor(){ return $this->author; }

    public function setSlug($slug){ $this->slug = $slug; }
    public function getSlug(){ return $this->slug; }

    public function getNbViews(){ return $this->nbViews; }
    public function setNbViews($nbViews){ $this->nbViews = $nbViews; }
    public function incrementNbViews(){ $this->nbViews++; }

}
