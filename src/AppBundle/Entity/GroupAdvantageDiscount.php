<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupAdvantageDiscountRepository")
 * @ORM\Table(name="group_advantages_discounts")
 **/
class GroupAdvantageDiscount
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float", nullable=true)
     */
    protected $percent;

    /**
     * @ORM\Column(name="discount_date_begin", type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateBeginDiscount;

    /**
     * @ORM\Column(name="discount_date_end", type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $dateEndDiscount;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Configuration\Advantage", inversedBy="groupsAdvantage",cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="advantage_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $advantage;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="GroupAdvantage")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return GroupAdvantageDiscount
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set dateBeginDiscount
     *
     * @param \DateTime $dateBeginDiscount
     *
     * @return GroupAdvantageDiscount
     */
    public function setDateBeginDiscount($dateBeginDiscount)
    {
        $this->dateBeginDiscount = $dateBeginDiscount;

        return $this;
    }

    /**
     * Get dateBeginDiscount
     *
     * @return \DateTime
     */
    public function getDateBeginDiscount()
    {
        return $this->dateBeginDiscount;
    }

    /**
     * Set dateEndDiscount
     *
     * @param \DateTime $dateEndDiscount
     *
     * @return GroupAdvantageDiscount
     */
    public function setDateEndDiscount($dateEndDiscount)
    {
        $this->dateEndDiscount = $dateEndDiscount;

        return $this;
    }

    /**
     * Get dateEndDiscount
     *
     * @return \DateTime
     */
    public function getDateEndDiscount()
    {
        return $this->dateEndDiscount;
    }

    /**
     * Set advantage
     *
     * @param \AppBundle\Entity\Configuration\Advantage $advantage
     *
     * @return GroupAdvantageDiscount
     */
    public function setAdvantage(\AppBundle\Entity\Configuration\Advantage $advantage = null)
    {
        $this->advantage = $advantage;

        return $this;
    }

    /**
     * Get advantage
     *
     * @return \AppBundle\Entity\Configuration\Advantage
     */
    public function getAdvantage()
    {
        return $this->advantage;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return GroupAdvantageDiscount
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }
}
