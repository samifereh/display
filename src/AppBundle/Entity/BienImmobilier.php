<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="BienImmobilier")
 * @Gedmo\Uploadable(
 *     path="uploads/models/images",
 *     allowOverwrite=true,
 *     allowedTypes="image/jpeg,image/pjpeg"
 * )
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type_bien", type="string")
 * @DiscriminatorMap({
 *     "maison" = "Maison",
 *     "terrain" = "Terrain",
 *     "appartement" = "Appartement"
 * })
 */
abstract class BienImmobilier
{
    const EXPOSITION = [
        ''                      => '---',
        'est'                   => 'Est',
        'est-ouest'             => 'Est-Ouest',
        'nord'                  => 'Nord',
        'nord-est'              => 'Nord-est',
        'nord-est/sud-ouest'    => 'Nord-est/Sud-ouest',
        'nord-ouest'            => 'Nord-ouest',
        'nord-ouest/sud-est'    => 'Nord-ouest/Sud-est',
        'nord-sud'              => 'Nord-sud',
        'ouest'                 => 'Ouest',
        'sud-est'               => 'Sud-est',
        'sud-ouest'             => 'Sud-ouest',
        'traversant'            => 'Tranversant',
        'traversant-est-ouest'  => 'Tranversant-est-ouest',
        'traversant-nord-sud'   => 'Traversant-nord-sud'
    ];

    const STANDING = [
        ''                         => '---',
        'non'                      => 'Non',
        'normal'                   => 'Normal',
        'bon'                      => 'Bon standing',
        'grand'                    => 'Grand standing',
        'luxe'                     => 'Luxe',
        'Résidence de prestige'    => 'Résidence de prestige'
    ];

    const GARNIER = [
        'non'                  => 'Non',
        'oui'                  => 'Oui',
        'amenageable'          => 'Aménageable',
        'amenage'              => 'Aménagé',
        'non_amenageable'      => 'Non aménageable'
    ];
    const ENVIRONNEMENT = [
        'campagne'      => 'Campagne',
        'centre ville'  => 'Centre ville',
        'montagne'      => 'Montagne',
        'océan'         => 'Océan',
        'ville'         => 'Ville',
        'autre'         => 'Autre'
    ];

	public function __construct()
	{
		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();
	}

	/**
	 * Unique id of the Ad
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

    /**
     * @ORM\Column(type="string",unique=true, nullable=true)
     */
    protected $remoteId;

	/**
	 * The owner of the house model
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @JoinColumn(onDelete="cascade")
	 */
	protected $owner;

	/**
	 * The group owner of the house model
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="biens")
     * @JoinColumn(name="group_id", referencedColumnName="id")
	 */
	protected $group;

	/**
	 * The creating date of the house model
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="create")
	 */
	protected $createdAt;

	/**
	 * The updating date of the house model
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 * @Gedmo\Timestampable(on="update")
	 */
	protected $updatedAt;

	/**
	 * The name
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $title;

	/**
	 * @ORM\Column(type="text",length=4000, nullable=true)
	 */
	protected $description;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $titreImagePrincipale;

	/**
	 * @ORM\Column(name="imagePrincipale", type="string", length=255, nullable=true)
	 * @Gedmo\UploadableFilePath
	 * @Assert\File(
	 *     mimeTypes={"image/jpeg", "image/pjpeg"}
	 * )
	 */
	protected $imagePrincipale = "";

    /**
     * Indicate the total size of the parking
     * @var float
     *
     * @ORM\Column(name="surface", type="float", nullable=false)
     */
    protected $surface;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $exposition;

	/**
	 * @var Maison
	 *
	 * @ManyToOne(targetEntity="AppBundle\Entity\Ad", inversedBy="bienImmobilier",cascade={"persist"})
	 * @JoinColumn(name="ad_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $ad;

    /*Localisation*/
    /**
     * @var boolean
     * @ORM\Column(name="proximiteMetro", type="boolean", nullable=true)
     */
    protected $siProximiteMetro = false;
    /**
     * @var boolean
     * @ORM\Column(name="proximiteAeroport", type="boolean", nullable=true)
     */
    protected $siProximiteAeroport = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteSortieAutoroute = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteGare = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteBus = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteTramway = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteParking = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteCommerces = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteCentreCommercial = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteCentreville = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteCreche = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteEcolePrimaire = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteCollege = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteLycee = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteUniversite = false;
    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siProximiteEspacesVerts = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     * @return Ad
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BienImmobilier
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return AdTest
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BienImmobilier
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BienImmobilier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set titreImagePrincipale
     *
     * @param string $titreImagePrincipale
     *
     * @return BienImmobilier
     */
    public function setTitreImagePrincipale($titreImagePrincipale)
    {
        $this->titreImagePrincipale = $titreImagePrincipale;

        return $this;
    }

    /**
     * Get titreImagePrincipale
     *
     * @return string
     */
    public function getTitreImagePrincipale()
    {
        return $this->titreImagePrincipale;
    }

    /**
     * Set imagePrincipale
     *
     * @param string $imagePrincipale
     *
     * @return BienImmobilier
     */
    public function setImagePrincipale($imagePrincipale)
    {
        $this->imagePrincipale = $imagePrincipale;

        return $this;
    }

    /**
     * Get imagePrincipale
     *
     * @return string
     */
    public function getImagePrincipale()
    {
        return $this->imagePrincipale;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return BienImmobilier
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group $group
     *
     * @return BienImmobilier
     */
    public function setGroup(\AppBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return BienImmobilier
     */
    public function setAd(\AppBundle\Entity\Ad $ad = null)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set exposition
     *
     * @param string $exposition
     *
     * @return BienImmobilier
     */
    public function setExposition($exposition)
    {
        $this->exposition = $exposition;

        return $this;
    }

    /**
     * Get exposition
     *
     * @return string
     */
    public function getExposition()
    {
        return $this->exposition;
    }

    public function getExpositionValue()
    {
        return self::EXPOSITION[$this->exposition];
    }

    /**
     * Set surface
     *
     * @param float $surface
     *
     * @return BienImmobilier
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return float
     */
    public function getSurface()
    {
        return $this->surface;
    }

    public function __clone() {
        if ($this->id) {
            $this->id = null;
            $this->remoteId = null;
        }
    }

    /**
     * Set siProximiteAeroport
     *
     * @param boolean $siProximiteAeroport
     *
     * @return BienImmobilier
     */
    public function setSiProximiteAeroport($siProximiteAeroport)
    {
        $this->siProximiteAeroport = $siProximiteAeroport;

        return $this;
    }

    /**
     * Get siProximiteAeroport
     *
     * @return boolean
     */
    public function getSiProximiteAeroport()
    {
        return $this->siProximiteAeroport;
    }

    /**
     * Set siProximiteSortieAutoroute
     *
     * @param boolean $siProximiteSortieAutoroute
     *
     * @return BienImmobilier
     */
    public function setSiProximiteSortieAutoroute($siProximiteSortieAutoroute)
    {
        $this->siProximiteSortieAutoroute = $siProximiteSortieAutoroute;

        return $this;
    }

    /**
     * Get siProximiteSortieAutoroute
     *
     * @return boolean
     */
    public function getSiProximiteSortieAutoroute()
    {
        return $this->siProximiteSortieAutoroute;
    }

    /**
     * Set siProximiteGare
     *
     * @param boolean $siProximiteGare
     *
     * @return BienImmobilier
     */
    public function setSiProximiteGare($siProximiteGare)
    {
        $this->siProximiteGare = $siProximiteGare;

        return $this;
    }

    /**
     * Get siProximiteGare
     *
     * @return boolean
     */
    public function getSiProximiteGare()
    {
        return $this->siProximiteGare;
    }

    /**
     * Set siProximiteBus
     *
     * @param boolean $siProximiteBus
     *
     * @return BienImmobilier
     */
    public function setSiProximiteBus($siProximiteBus)
    {
        $this->siProximiteBus = $siProximiteBus;

        return $this;
    }

    /**
     * Get siProximiteBus
     *
     * @return boolean
     */
    public function getSiProximiteBus()
    {
        return $this->siProximiteBus;
    }

    /**
     * Set siProximiteTramway
     *
     * @param boolean $siProximiteTramway
     *
     * @return BienImmobilier
     */
    public function setSiProximiteTramway($siProximiteTramway)
    {
        $this->siProximiteTramway = $siProximiteTramway;

        return $this;
    }

    /**
     * Get siProximiteTramway
     *
     * @return boolean
     */
    public function getSiProximiteTramway()
    {
        return $this->siProximiteTramway;
    }

    /**
     * Set siProximiteParking
     *
     * @param boolean $siProximiteParking
     *
     * @return BienImmobilier
     */
    public function setSiProximiteParking($siProximiteParking)
    {
        $this->siProximiteParking = $siProximiteParking;

        return $this;
    }

    /**
     * Get siProximiteParking
     *
     * @return boolean
     */
    public function getSiProximiteParking()
    {
        return $this->siProximiteParking;
    }

    /**
     * Set siProximiteCommerces
     *
     * @param boolean $siProximiteCommerces
     *
     * @return BienImmobilier
     */
    public function setSiProximiteCommerces($siProximiteCommerces)
    {
        $this->siProximiteCommerces = $siProximiteCommerces;

        return $this;
    }

    /**
     * Get siProximiteCommerces
     *
     * @return boolean
     */
    public function getSiProximiteCommerces()
    {
        return $this->siProximiteCommerces;
    }

    /**
     * Set siProximiteCentreCommercial
     *
     * @param boolean $siProximiteCentreCommercial
     *
     * @return BienImmobilier
     */
    public function setSiProximiteCentreCommercial($siProximiteCentreCommercial)
    {
        $this->siProximiteCentreCommercial = $siProximiteCentreCommercial;

        return $this;
    }

    /**
     * Get siProximiteCentreCommercial
     *
     * @return boolean
     */
    public function getSiProximiteCentreCommercial()
    {
        return $this->siProximiteCentreCommercial;
    }

    /**
     * Set siProximiteCentreville
     *
     * @param boolean $siProximiteCentreville
     *
     * @return BienImmobilier
     */
    public function setSiProximiteCentreville($siProximiteCentreville)
    {
        $this->siProximiteCentreville = $siProximiteCentreville;

        return $this;
    }

    /**
     * Get siProximiteCentreville
     *
     * @return boolean
     */
    public function getSiProximiteCentreville()
    {
        return $this->siProximiteCentreville;
    }

    /**
     * Set siProximiteCreche
     *
     * @param boolean $siProximiteCreche
     *
     * @return BienImmobilier
     */
    public function setSiProximiteCreche($siProximiteCreche)
    {
        $this->siProximiteCreche = $siProximiteCreche;

        return $this;
    }

    /**
     * Get siProximiteCreche
     *
     * @return boolean
     */
    public function getSiProximiteCreche()
    {
        return $this->siProximiteCreche;
    }

    /**
     * Set siProximiteEcolePrimaire
     *
     * @param boolean $siProximiteEcolePrimaire
     *
     * @return BienImmobilier
     */
    public function setSiProximiteEcolePrimaire($siProximiteEcolePrimaire)
    {
        $this->siProximiteEcolePrimaire = $siProximiteEcolePrimaire;

        return $this;
    }

    /**
     * Get siProximiteEcolePrimaire
     *
     * @return boolean
     */
    public function getSiProximiteEcolePrimaire()
    {
        return $this->siProximiteEcolePrimaire;
    }

    /**
     * Set siProximiteCollege
     *
     * @param boolean $siProximiteCollege
     *
     * @return BienImmobilier
     */
    public function setSiProximiteCollege($siProximiteCollege)
    {
        $this->siProximiteCollege = $siProximiteCollege;

        return $this;
    }

    /**
     * Get siProximiteCollege
     *
     * @return boolean
     */
    public function getSiProximiteCollege()
    {
        return $this->siProximiteCollege;
    }

    /**
     * Set siProximiteLycee
     *
     * @param boolean $siProximiteLycee
     *
     * @return BienImmobilier
     */
    public function setSiProximiteLycee($siProximiteLycee)
    {
        $this->siProximiteLycee = $siProximiteLycee;

        return $this;
    }

    /**
     * Get siProximiteLycee
     *
     * @return boolean
     */
    public function getSiProximiteLycee()
    {
        return $this->siProximiteLycee;
    }

    /**
     * Set siProximiteUniversite
     *
     * @param boolean $siProximiteUniversite
     *
     * @return BienImmobilier
     */
    public function setSiProximiteUniversite($siProximiteUniversite)
    {
        $this->siProximiteUniversite = $siProximiteUniversite;

        return $this;
    }

    /**
     * Get siProximiteUniversite
     *
     * @return boolean
     */
    public function getSiProximiteUniversite()
    {
        return $this->siProximiteUniversite;
    }

    /**
     * Set siProximiteEspacesVerts
     *
     * @param boolean $siProximiteEspacesVerts
     *
     * @return BienImmobilier
     */
    public function setSiProximiteEspacesVerts($siProximiteEspacesVerts)
    {
        $this->siProximiteEspacesVerts = $siProximiteEspacesVerts;

        return $this;
    }

    /**
     * Get siProximiteEspacesVerts
     *
     * @return boolean
     */
    public function getSiProximiteEspacesVerts()
    {
        return $this->siProximiteEspacesVerts;
    }

    /**
     * Set siProximiteMetro
     *
     * @param boolean $siProximiteMetro
     *
     * @return BienImmobilier
     */
    public function setSiProximiteMetro($siProximiteMetro)
    {
        $this->siProximiteMetro = $siProximiteMetro;

        return $this;
    }

    /**
     * Get siProximiteMetro
     *
     * @return boolean
     */
    public function getSiProximiteMetro()
    {
        return $this->siProximiteMetro;
    }
}
