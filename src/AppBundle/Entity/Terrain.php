<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TerrainRepository")
 * @ORM\Table(name="terrain")
 */
class Terrain extends BienImmobilier
{

    const TERRAIN_TYPES = [
        'terrain_a_batir'       => 'Terrain à batir',
        'zi/zac'                => 'Zi/Zac',
        'lotissement'           => 'Lotissement',
        'program'               => 'Programme terrain+maison',
        'terrain_a_amenager'    => 'Terrain à eménager',
        'jardin'                => 'Jardin',
        'terrain_de_loisir'     => 'Terrain de loisir',
        'terrain_agricole'      => 'Terrain Agricole',
        'ile'                   => 'Ile',
        'divers_terrains'       => 'Divers Terrains',
        'foret'                 => 'Forêt',
        'golf'                  => 'Golf',
        'etang'                 => 'Etang'
    ];

    const TERRAIN_ENV = [
        'campagne'       => 'Campagne',
        'centre ville'       => 'Centre ville',
        'montagne'       => 'Montagne',
        'ocean'       => 'Océan',
        'ville'       => 'Ville',
        'autre'       => 'Autre',
    ];

    const ASSAINISSEMENT_EAU_USEES = [
        'Tout à l\'égout'           => 'Tout à l\'égout',
        'Assainissement autonome'   => 'Assainissement autonome'
    ];

    const ASSAINISSEMENT_EAU_PLUVIALE = [
        'Puit perdu'           => 'Puit perdu',
        'Drain pluviale'   => 'Drain pluviale',
        'Cuve rétention'   => 'Cuve rétention',
        'Cuve de récupération'   => 'Cuve de récupération'
    ];

    const VIABILISE = [
        'Eau' => 'Eau',
        'Electricité' => 'Electricité',
        'Télécom' => 'Télécom',
        'Eau usées' => 'Eau usées',
        'Eau pluviale' => 'Eau pluviale'
    ];

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $type;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    protected $prix;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $environnement;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $belleVue = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="vis_a_vis", type="boolean", nullable=true)
     */
    protected $vis_a_vis = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $maisonGroupee = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $calme = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $procheMetro = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $procheGare = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $procheEcole = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $procheCreche = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $procheEspaceVerts = false;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $libreConstruction = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $siViabilise = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="array", nullable=true)
     */
    protected $viabilise;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $constructible = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $servitude = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pente = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $assainissementEauPluviale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $assainissementEauUsees;

    /**
     * @var float
     * @ORM\Column(name="surface_plancher", type="float", nullable=false)
     */
    protected $surfacePlancher;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $certificatUrbanismeDelivrer = false;

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Terrain
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTypeValue()
    {
        return (array_key_exists( $this->type, self::TERRAIN_TYPES ) ? self::TERRAIN_TYPES[$this->type] : "");
    }

    /**
     * Set surface
     *
     * @param string $surface
     *
     * @return Terrain
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }


    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Terrain
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set environnement
     *
     * @param string $environnement
     *
     * @return Terrain
     */
    public function setEnvironnement($environnement)
    {
        $this->environnement = $environnement;

        return $this;
    }

    /**
     * Get environnement
     *
     * @return string
     */
    public function getEnvironnement()
    {
        return $this->environnement;
    }

    /**
     * Set belleVue
     *
     * @param string $belleVue
     *
     * @return Terrain
     */
    public function setBelleVue($belleVue)
    {
        $this->belleVue = $belleVue;

        return $this;
    }

    /**
     * Get belleVue
     *
     * @return string
     */
    public function getBelleVue()
    {
        return $this->belleVue;
    }

    /**
     * Set visAVis
     *
     * @param boolean $visAVis
     *
     * @return Terrain
     */
    public function setVisAVis($visAVis)
    {
        $this->vis_a_vis = $visAVis;

        return $this;
    }

    /**
     * Get visAVis
     *
     * @return boolean
     */
    public function getVisAVis()
    {
        return $this->vis_a_vis;
    }

    /**
     * Set maisonGroupee
     *
     * @param boolean $maisonGroupee
     *
     * @return Terrain
     */
    public function setMaisonGroupee($maisonGroupee)
    {
        $this->maisonGroupee = $maisonGroupee;

        return $this;
    }

    /**
     * Get maisonGroupee
     *
     * @return boolean
     */
    public function getMaisonGroupee()
    {
        return $this->maisonGroupee;
    }

    public function calculatePercent()
    {
        $proprety = [$this->maisonGroupee, $this->vis_a_vis, $this->belleVue, $this->environnement, $this->prix, $this->surface, $this->type];
        $percent  = 0;
        $part     = 100/count($proprety);
        foreach ($proprety as $item) {
            if(!is_null($item) && $item !== '')
                $percent += $part;
        }
        return $percent;
    }

    /**
     * Set calme
     *
     * @param boolean $calme
     *
     * @return Terrain
     */
    public function setCalme($calme)
    {
        $this->calme = $calme;

        return $this;
    }

    /**
     * Get calme
     *
     * @return boolean
     */
    public function getCalme()
    {
        return $this->calme;
    }

    /**
     * Set procheMetro
     *
     * @param boolean $procheMetro
     *
     * @return Terrain
     */
    public function setProcheMetro($procheMetro)
    {
        $this->procheMetro = $procheMetro;

        return $this;
    }

    /**
     * Get procheMetro
     *
     * @return boolean
     */
    public function getProcheMetro()
    {
        return $this->procheMetro;
    }

    /**
     * Set procheGare
     *
     * @param boolean $procheGare
     *
     * @return Terrain
     */
    public function setProcheGare($procheGare)
    {
        $this->procheGare = $procheGare;

        return $this;
    }

    /**
     * Get procheGare
     *
     * @return boolean
     */
    public function getProcheGare()
    {
        return $this->procheGare;
    }

    /**
     * Set procheCommerces
     *
     * @param boolean $procheCommerces
     *
     * @return Terrain
     */
    public function setProcheCommerces($procheCommerces)
    {
        $this->procheCommerces = $procheCommerces;

        return $this;
    }

    /**
     * Get procheCommerces
     *
     * @return boolean
     */
    public function getProcheCommerces()
    {
        return $this->procheCommerces;
    }

    /**
     * Set procheEcole
     *
     * @param boolean $procheEcole
     *
     * @return Terrain
     */
    public function setProcheEcole($procheEcole)
    {
        $this->procheEcole = $procheEcole;

        return $this;
    }

    /**
     * Get procheEcole
     *
     * @return boolean
     */
    public function getProcheEcole()
    {
        return $this->procheEcole;
    }

    /**
     * Set procheCreche
     *
     * @param boolean $procheCreche
     *
     * @return Terrain
     */
    public function setProcheCreche($procheCreche)
    {
        $this->procheCreche = $procheCreche;

        return $this;
    }

    /**
     * Get procheCreche
     *
     * @return boolean
     */
    public function getProcheCreche()
    {
        return $this->procheCreche;
    }

    /**
     * Set procheEspaceVerts
     *
     * @param boolean $procheEspaceVerts
     *
     * @return Terrain
     */
    public function setProcheEspaceVerts($procheEspaceVerts)
    {
        $this->procheEspaceVerts = $procheEspaceVerts;

        return $this;
    }

    /**
     * Get procheEspaceVerts
     *
     * @return boolean
     */
    public function getProcheEspaceVerts()
    {
        return $this->procheEspaceVerts;
    }

    /**
     * Set libreConstruction
     *
     * @param boolean $libreConstruction
     *
     * @return Terrain
     */
    public function setLibreConstruction($libreConstruction)
    {
        $this->libreConstruction = $libreConstruction;

        return $this;
    }

    /**
     * Get libreConstruction
     *
     * @return boolean
     */
    public function getLibreConstruction()
    {
        return $this->libreConstruction;
    }

    /**
     * Set viabilise
     *
     * @param boolean $viabilise
     *
     * @return Terrain
     */
    public function setViabilise($viabilise)
    {
        $this->viabilise = $viabilise;

        return $this;
    }

    /**
     * Get viabilise
     *
     * @return boolean
     */
    public function getViabilise()
    {
        return $this->viabilise;
    }

    /**
     * Set constructible
     *
     * @param boolean $constructible
     *
     * @return Terrain
     */
    public function setConstructible($constructible)
    {
        $this->constructible = $constructible;

        return $this;
    }

    /**
     * Get constructible
     *
     * @return boolean
     */
    public function getConstructible()
    {
        return $this->constructible;
    }

    /**
     * Set servitude
     *
     * @param boolean $servitude
     *
     * @return Terrain
     */
    public function setServitude($servitude)
    {
        $this->servitude = $servitude;

        return $this;
    }

    /**
     * Get servitude
     *
     * @return boolean
     */
    public function getServitude()
    {
        return $this->servitude;
    }

    /**
     * Set pente
     *
     * @param boolean $pente
     *
     * @return Terrain
     */
    public function setPente($pente)
    {
        $this->pente = $pente;

        return $this;
    }

    /**
     * Get pente
     *
     * @return boolean
     */
    public function getPente()
    {
        return $this->pente;
    }

    /**
     * Set surfacePlancher
     *
     * @param float $surfacePlancher
     *
     * @return Terrain
     */
    public function setSurfacePlancher($surfacePlancher)
    {
        $this->surfacePlancher = $surfacePlancher;

        return $this;
    }

    /**
     * Get surfacePlancher
     *
     * @return float
     */
    public function getSurfacePlancher()
    {
        return $this->surfacePlancher;
    }

    /**
     * Set certificatUrbanismeDelivrer
     *
     * @param boolean $certificatUrbanismeDelivrer
     *
     * @return Terrain
     */
    public function setCertificatUrbanismeDelivrer($certificatUrbanismeDelivrer)
    {
        $this->certificatUrbanismeDelivrer = $certificatUrbanismeDelivrer;

        return $this;
    }

    /**
     * Get certificatUrbanismeDelivrer
     *
     * @return boolean
     */
    public function getCertificatUrbanismeDelivrer()
    {
        return $this->certificatUrbanismeDelivrer;
    }

    /**
     * Set assainissementEauPluviale
     *
     * @param string $assainissementEauPluviale
     *
     * @return Terrain
     */
    public function setAssainissementEauPluviale($assainissementEauPluviale)
    {
        $this->assainissementEauPluviale = $assainissementEauPluviale;

        return $this;
    }

    /**
     * Get assainissementEauPluviale
     *
     * @return string
     */
    public function getAssainissementEauPluviale()
    {
        return $this->assainissementEauPluviale;
    }

    /**
     * Set assainissementEauUsees
     *
     * @param string $assainissementEauUsees
     *
     * @return Terrain
     */
    public function setAssainissementEauUsees($assainissementEauUsees)
    {
        $this->assainissementEauUsees = $assainissementEauUsees;

        return $this;
    }

    /**
     * Get assainissementEauUsees
     *
     * @return string
     */
    public function getAssainissementEauUsees()
    {
        return $this->assainissementEauUsees;
    }

    /**
     * Set siViabilise
     *
     * @param boolean $siViabilise
     *
     * @return Terrain
     */
    public function setSiViabilise($siViabilise)
    {
        $this->siViabilise 	  = $siViabilise['si'];
        $this->viabilise 	  = $siViabilise['choices'];

        return $this;
    }

    /**
     * Get siViabilise
     *
     * @return array
     */
    public function getSiViabilise()
    {
        return [
            'si'		=> $this->siViabilise,
            'choices'	=> $this->viabilise,
        ];
    }
}
