<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 11/08/2016
 * Time: 10:45
 */

namespace AppBundle\Entity;


use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class File
{
    const WRITE_MODE = 'w';
    const READ_MODE = 'r';
    const EXECUTABLE_MODE = 'x';

    /**
     * The default context of the File system
     * . correspond to root\web folder
     * @var string
     */
    protected static $context = '.';
    protected $binary = null;
    protected $pathName = null;

    public function __construct($objectPath = null)
    {
        if($objectPath) {
            if (!is_string($objectPath)) {
                if (!method_exists($objectPath, 'toString')) {
                    throw new InvalidParameterException('Parameter must be a string', 0, null);
                } else {
                    $path = $objectPath->toString();
                }
            } else {
                $path = $objectPath;
            }
            try {
                if(!is_dir($path) && file_exists($path)) {
                    $this->binary = fopen(self::$context.DIRECTORY_SEPARATOR.$path, self::READ_MODE);
                    fclose($this->binary);
                }
                $this->pathName = $path;
            } catch (Exception $e) {
                $this->binary = null;
            }
        }
    }

    public static function mkFile($path, $override = false)
    {
        if(!$override) {
            $existFile = new File($path);
            if($existFile->exists()) {
                return $existFile;
            }
        }
        $file = new File();
        try {
            $file->binary = fopen(self::$context.DIRECTORY_SEPARATOR.$path, self::WRITE_MODE);
            fclose($file->binary);
            $file->pathName = $path;
        } catch (Exception $e) {
            $file->binary = null;
        } finally {
            return $file;
        }
    }

    public static function mkDir($pathname, $mode = 0777, $recursive = false)
    {
        if (mkdir(self::$context . DIRECTORY_SEPARATOR . $pathname, $mode, $recursive)) {
            return new File($pathname);
        } else {
            return new File();
        }
    }

    public function isFile()
    {
        if (!$this->pathName) {
            return false;
        }
        return is_file(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function isDirectory()
    {
        if (!$this->pathName) {
            return false;
        }
        return is_dir(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function exists()
    {
        if (!$this->pathName) {
            return false;
        }
        return file_exists(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function isReadable()
    {
        if (!$this->pathName) {
            return false;
        }
        return is_readable(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function isWritable()
    {
        if (!$this->pathName) {
            return false;
        }
        return is_writable(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function isExecutable()
    {
        if (!$this->pathName) {
            return false;
        }
        return is_executable(self::$context.DIRECTORY_SEPARATOR.$this->pathName);
    }

    public function setMode($mode)
    {
        if (!$this->pathName) {
            return false;
        }
        return chmod(self::$context.DIRECTORY_SEPARATOR.$this->pathName, $mode);
    }

    public static function cd($path)
    {
        try {
            if (is_dir($path)) {
                self::$context = $path;
            } else {
                self::$context = '.';
            }
        } catch (ContextErrorException $e) {
            dump($path);exit;
        }

    }

    public function getContent() {
        return file_get_contents(self::$context . DIRECTORY_SEPARATOR . $this->pathName, true);
    }

    public function write($content) {
        $this->binary = fopen(self::$context . DIRECTORY_SEPARATOR . $this->pathName, self::WRITE_MODE);
        $result = false != fwrite($this->binary, $content);
        fclose($this->binary);
        return $result;
    }

    public function move($directoryPath)
    {
        if(!$directoryPath instanceof File) {
            $directory = new File($directoryPath);
        } else {
            $directory = $directoryPath;
        }
        if($directory->isDirectory()) {
            return rename(self::$context . DIRECTORY_SEPARATOR . $this->pathName, self::$context . DIRECTORY_SEPARATOR . $directory->pathName . DIRECTORY_SEPARATOR . $this->pathName);
        } else {
            return false;
        }
    }

    public function copy($pathName) {
        $copy = new File($pathName);
        $copy->write($this->getContent());
        return $copy;
    }

    public function rename($name, $override = false) {
        if(!$override) {
            if((new File($name))->exists()) {
                return false;
            }
        }
        if(preg_match('#[\\|/]#', $name)) {
            return false;
        } else {
            if(rename(self::$context . DIRECTORY_SEPARATOR . $this->pathName, self::$context . DIRECTORY_SEPARATOR . $name)) {
                $this->pathName = $name;
                return true;
            } else {
                return false;
            }
        }
    }

    public function isEmpty() {
        if($this->isDirectory()) {
            return count(scandir($this->pathName)) > 2;
        } else {
            return $this->getContent() == '';
        }
    }

    public function delete() {
        return unlink(self::$context . DIRECTORY_SEPARATOR . $this->pathName);
    }

    public function getPath() {
        return self::$context . DIRECTORY_SEPARATOR . $this->pathName;
    }

    public function getName() {
        return $this->pathName;
    }

}