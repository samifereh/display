<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 19/07/2016
 * Time: 10:38
 */

namespace AppBundle\Exceptions;


class NullPointerException extends \Exception
{
    public function __construct($code = 124, Exception $previous = null)
    {
        parent::__construct("The specified object is null", $code, $previous);
    }
}