<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 19/07/2016
 * Time: 10:47
 */

namespace AppBundle\Exceptions;


use Symfony\Component\Config\Definition\Exception\Exception;

class ClassCastException extends Exception
{
    public function __construct($code = 123, Exception $previous = null)
    {
        parent::__construct("The specified object can't be compare to the current object", $code, $previous);
    }
}