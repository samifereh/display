<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AdMentionLegals;
use AppBundle\Entity\AdRewrite;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use AppBundle\Entity\GroupAdvantageDiscount;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Form\Type\AdMentionLegalsType;
use AppBundle\Form\Type\AgencyGroupType;
use AppBundle\Form\Type\AgencyType;
use AppBundle\Form\Type\GroupAdvantagesDiscountType;
use AppBundle\Form\Type\GroupsAgencyType;
use AppBundle\Form\Type\GroupsType;
use AppBundle\Form\Type\RewriteAdType;
use AppBundle\Repository\GroupAdvantageDiscountRepository;
use AppBundle\Repository\GroupAdvantageRepository;
use AppBundle\Service\FtpClient;
use AppBundle\Service\Payment;
use Mailjet\Api\RequestApi;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class GroupAgenciesController
 * @package AppBundle\Controller
 * @Route("/group")
 */
class GroupAgenciesController extends BaseController
{
    /**
     * @return Response
     *
     * @Route("/liste-agences", name="group_agencies_list" )
     * @Method({"GET"})
     * @Template("AppBundle:GroupAgencies:list.html.twig")
     **/
    public function listAgencyAction(Request $request){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$this->getUser()->getGroup()->hasRole('ROLE_GROUP'))
            throw new AccessDeniedHttpException();

        $groups = $this->getUser()->getGroups();

        $datatable = $this->get('app.datatable.group_agency');
        $datatable->buildDatatable();

        $em        = $this->getDoctrine()->getManager();
        /** @var GroupAdvantageRepository $groupAdvantagesRepository */
        $groupAdvantagesRepository = $em->getRepository('AppBundle:GroupAdvantage');
        $groupAdvantages = $groupAdvantagesRepository->findCurrentGroupAdvantages($groups,'group');
        $payedAdvantages = [];
        foreach ($groupAdvantages as $groupAdvantage)
            $payedAdvantages[]  = $groupAdvantage->getAdvantage();

        /** @var GroupAdvantageDiscountRepository $groupAdvantageDiscountRepository */
        $groupAdvantageDiscountRepository = $em->getRepository('AppBundle:GroupAdvantageDiscount');
        $discounts = $groupAdvantageDiscountRepository->findCurrentGroupAdvantagesDiscounts($groups,'group');

        return [
            'datatable'         => $datatable,
            'groups'            => $groups,
            'discounts'         => $discounts,
            'groupAdvantages'   => $groupAdvantages,
            'payedAdvantages'   => $payedAdvantages
        ];

    }

    /**
     * @return Response
     *
     * @Route("/agences/ajouter/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="group_agency_add" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:GroupAgencies:add.html.twig")
     **/
    public function addAgencyAction(Request $request, Group $group = null){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$this->getUser()->getGroup()->hasRole('ROLE_GROUP'))
            throw new AccessDeniedHttpException();
        $parentGroup = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ) ? $group : $this->getUser()->getGroup();

        $authorizedAgencyRepository = $this->getDoctrine()->getRepository('AppBundle:Payment\\AuthorizedAgency');
        $hasCredit = $authorizedAgencyRepository->findOneBy(['group'=>null,'parentGroup'=>$parentGroup]);
        if(!$hasCredit) {
            $this->addFlash('error', 'Vous devez acheter un crédit agences pour pouvoir en ajouter');
            return $this->redirectToRoute('group_agency_show', ['remoteId' => $parentGroup->getRemoteId()]);
        }
        $user = $this->getUser();
        $group = new Group();
        $group->addRole('ROLE_AGENCY');
        $group->setAnnoncesOption(true);
        $avatar = null;
        if(
            $request->files &&
            $request->files->get("group_agency") &&
            $request->files->get("group_agency")["agency"] &&
            $request->files->get("group_agency")["agency"]["avatar"]
        ) {
            $avatar = $request->files->get("group_agency")["agency"]["avatar"];
        }

        $form = $this->createForm(
            GroupsAgencyType::class,
            $group,
            ['user' => $user, "avatar" => $avatar]
        );
        $form->handleRequest($request);
        $errorDuplication = false;
        if ($form->isValid()) {
            $group->setRoles(['ROLE_AGENCY']);
            $group->setParentGroup($this->getUser()->getGroup());
            $em = $this->getDoctrine()->getManager();
            $groupRepository = $em->getRepository('AppBundle:Group');
            $exist = $groupRepository->findOneBy(['name' => $group->getName(), 'parentGroup'=>$parentGroup]);
            if(!$exist) {
                $group->getAgency()->setName($group->getName());
                $group->setParentGroup($parentGroup);
                if($group->getAgency()->getAvatar()) {
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($group->getAgency(), $group->getAgency()->getAvatar());
                }

                $slug = 'commiti_'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $group->getPrefix())));
                $password = $this->getPasswordGenerator()->generate();
                $group->setImportHost($this->getParameter('ftp_host'));
                $group->setImportPassword($password);
                $group->setImportUser($slug);
                $group->setImportFileName($slug.'.zip');
                FtpClient::addFTP($slug,$password);

                try {
                    $alias = 'commiti'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '', $group->getPrefix()))).'@mestatimmo.fr';
                    $result = $this->getOvh()->post(
                        sprintf('/email/domain/%s/account', 'mestatimmo.fr'),
                        [
                            'accountName' => $slug,
                            'password'    => $password
                        ]
                    );
                    if(isset($result['id'])){
                        $group->setEmailAlias($slug.'@mestatimmo.fr');
                        $group->setEmailAliasId($result['id']);
                    }

                    $this->getOvh()->post(
                        sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                        [
                            'from'       => $alias,
                            'localCopy'  => false,
                            'to'         => $group->getAgency()->getEmail()
                        ]
                    );
                } catch( \Exception $e) {
                }

                $em->persist($group);
                $em->flush();

                $hasCredit->setGroup($group);
                $em->persist($hasCredit);

                $authorizedRoles = Payment::PACK_GROUP_AGENCY;
                foreach ($authorizedRoles as $role => $userCount) {
                    for($i = 0; $i < $userCount; $i++) {
                        $instance      = new AuthorizedUser();
                        $instance->setGroup($group)->setRole($role)->setStartDate(new \DateTime('today'))->setEndDate(null);
                        $em->persist($instance);
                    }
                }
                $em->flush();
                $this->addFlash("success", $this->get('translator')->trans("agences.agency_added_to_group", [], 'commiti'));
                return $this->redirectToRoute('group_agency_show', ['remoteId' => $group->getRemoteId()]);
            } else {
                $errorDuplication = true;
            }
        }
        return [
            'form'  => $form->createView(),
            'errorDuplication' => $errorDuplication,
            'mode'              =>  'add'
                ];
    }

    /**
     * @return Response
     *
     * @Route("/agences/editer/{remoteId}", options={"expose"=true}, name="group_agency_edit" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:GroupAgencies:edit.html.twig")
     **/
    public function editAgencyAction(Request $request, Group $group){
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ||
            !$this->getUser()->getGroup()->hasRole('ROLE_GROUP') ||
            ($this->getUser()->getGroup()->hasRole('ROLE_GROUP') && !$group->isMemberOf($this->getUser()->getGroup())))
            throw new AccessDeniedHttpException();
        $errorDuplication = false;
        $oldGroupName = $group->getName();
        $parentGroup = $this->getUser()->getGroup();
        /** @var \AppBundle\Entity\Agency $agency */
        $agency = $group->getAgency();
        $avatar = $agency->getAvatar();
        $voicemail = $agency->getVoicemail();
        $updateWannaspeakVoiceMail = false;
        $form = $this->createForm(AgencyType::class,$agency);
        $formAgencyGroup = $this->createForm(AgencyGroupType::class,$group);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();
            if ($form->isValid()) {

                if($oldGroupName != $agency->getName()) {
                    $groupRepository = $em->getRepository('AppBundle:Group');
                    if($groupRepository->findOneBy(['name' => $agency->getName(), 'parentGroup'=>$parentGroup]))
                        return [
                            'form'  => $form->createView(),
                            'formAgencyGroup'  => $formAgencyGroup->createView(),
                            'errorDuplication' => true
                        ];
                }

                if(null === $form->get("avatar")->getData())
                    $agency->setAvatar($avatar);
                else{
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($agency, $agency->getAvatar());
                }
                $group->setName($agency->getName());

                if($form->get("voicemail")->getData()){
                    $currentDir = $this->get('kernel')->getRootDir().'/..';
                    $newVoiceMail = $form->get("voicemail")->getData();
                    $newVoiceMailFileName = $agency->getSlug().".mp3";
                    $agency->incrementVoicemailVersion();
                    $newVoiceMail->move($currentDir.'/web/uploads/voicemail/', $newVoiceMailFileName);
                    $voicemail = '/web/uploads/voicemail/'.$newVoiceMailFileName;
                    $updateWannaspeakVoiceMail = true;
                }
                $agency->setVoicemail($voicemail);
                $em->persist($agency);

                if(!$group->getEmailAlias() && $group->getAgency()->getEmail()) {
                    $alias = 'commiti'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '', $group->getPrefix()))).'@mestatimmo.fr';
                    try {
                        $result = $this->getOvh()->post(
                            sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                            [
                                'from'       => $alias,
                                'localCopy'  => false,
                                'to'         => 'contact@commiti-immo.fr'
                            ]
                        );
                        if(isset($result['id'])){
                            $group->setEmailAlias($alias);
                            $group->setEmailAliasId($result['id']);
                        }

                        $this->getOvh()->post(
                            sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                            [
                                'from'       => $alias,
                                'localCopy'  => false,
                                'to'         => $group->getAgency()->getEmail()
                            ]
                        );
                    } catch( \Exception $e) {
                    }
                }
                $this->get("app.agency.helpers")->registerClosing(
                    $group, "closing", false, $updateWannaspeakVoiceMail
                );
                $em->persist($group);
                $em->flush();
                $this->addFlash("success", $this->get('translator')->trans("agences.agency_modified", [], 'commiti'));
                return $this->redirectToRoute('group_agency_show', ['remoteId' => $group->getRemoteId()]);
            }
            $formAgencyGroup->handleRequest($request);
            if ($formAgencyGroup->isSubmitted() && $formAgencyGroup->isValid()) {
                $em->persist($group);
                $em->flush();
            }
        }
        return [
            'form'  => $form->createView(),
            'formAgencyGroup'  => $formAgencyGroup->createView(),
            'errorDuplication' => $errorDuplication,
            'edit' => true,
            'mode' =>  'edit'
                ];
    }

    /**
     * @return Response
     *
     * @Route("/agences/voir/{remoteId}", options={"expose"=true}, name="group_agency_show" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:GroupAgencies:voir.html.twig")
     **/
    public function voirAgencyAction(Request $request, Group $group){
        if($group->hasRole('ROLE_GROUP'))
            return $this->redirectToRoute('group_agencies_list');

        if(!$this->isGranted('ROLE_ADMIN') &&
            (
                !$this->isGranted('ROLE_MARKETING') ||
                !$this->getUser()->getGroup()->hasRole('ROLE_GROUP') && $group != $this->getUser()->getGroup() ||
                ($this->getUser()->getGroup()->hasRole('ROLE_GROUP') && !$group->isMemberOf($this->getUser()->getGroup()))
            )
        )
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $houseModelCount = $em->getRepository('AppBundle:HouseModel')->getHousemodelsCountByGroup($group);
        $adRepository = $em->getRepository('AppBundle:Ad');
        $adCount = $adRepository->getAdCountByGroup($group);
        $employeeCount = $em->getRepository('AppBundle:User')->getEmployeeCountByGroup($group);

        $employeeDatatable = $this->get('app.datatable.agencyemployee');
        $employeeDatatable->buildDatatable(['group' => $group]);

        $invoicesDatatable = $this->get('app.datatable.invoices');
        $invoicesDatatable->buildDatatable(['group'=>$group]);

        $groupAdvantages = $em->getRepository('AppBundle:GroupAdvantage')->findCurrentGroupAdvantages($group,'agence');

        $payedAdvantages = [];
        foreach ($groupAdvantages as $groupAdvantage)
            $payedAdvantages[]  = $groupAdvantage->getAdvantage();

        $adsDatatable = $houseModeleDatatable = null;
        if($group->hasRole('ROLE_AGENCY') && $group->hasAnnoncesOption()) {
            $adsDatatable = $this->get('app.datatable.ad');
            $adsDatatable->buildDatatable(['group' => $group]);

            $houseModeleDatatable = $this->get('app.datatable.housemodel');
            $houseModeleDatatable->buildDatatable(['group'=>$group]);
        }
        $discounts = $em->getRepository('AppBundle:GroupAdvantageDiscount')->findCurrentGroupAdvantagesDiscounts($group,'agence');


        $statCallRepo = $em->getRepository("TrackingBundle:Statistique\TrackingCallStatistique");
        $statistics = [];
        $emptyTrackingAdvantage = [];
        // to do not show empty advantage
        // portal
        $maxToTrack = 0;
        /** @var PortalRegistration $portalRegistration */
        foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration ){
            if(
                $portalRegistration->getBroadcastPortal() &&
                $portalRegistration->getBroadcastPortal()->getCluster() == "PAYANT" &&
                $portalRegistration->getIsEnabled()
            ) {
                $maxToTrack++;
                if($portalRegistration->getTrackingPhone()) {
                    $sum = $statCallRepo->getPhoneStats($portalRegistration->getTrackingPhone());
                    $statistics["call"]["portal"][$portalRegistration->getId()] = $sum;
                }
            }
        }
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-portal';

        // Annonce
        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidAdsCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-ad';

        // Program
        $maxToTrack = $em->getRepository("AppBundle:Program")->getValidProgramCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-program';

        // Lots
        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidLotsCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-lot';

        // call tracking statistics
        if($group->getAgency() && $group->getAgency()->getTrackingPhone())
            $statistics["call"]["agency"] = $statCallRepo->getPhoneStats(
                $group->getAgency()->getTrackingPhone()
            );

        /**
         * tracking portal
         */
        $trackingRepository = $em->getRepository("TrackingBundle:Advantage");
        /** @var Advantage $trackingAdvantage */
        $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), "portal");

        $myTrackingAdvantages = $trackingRepository->findAgencyTrackingAdvantage($group->getAgency());

        $adRewrite = $group->getAdRewrite() ? $group->getAdRewrite() : new AdRewrite();
        $adRewrite->setGroup($group);
        $formRewrite = $this->createForm(RewriteAdType::class,$adRewrite);
        $formRewrite->handleRequest($request);
        if ($formRewrite->isSubmitted() && $formRewrite->isValid()) {
            $group->setAdRewrite($adRewrite);
            $em->persist($group);
            $em->flush();
        }

        $adMentionLegals = $group->getAdMentionLegals() ? $group->getAdMentionLegals() : new AdMentionLegals();
        $adMentionLegals->setGroup($group);
        $formMention = $this->createForm($this->get("app.form.ad_mention_legals"),$adMentionLegals);
        $formMention->handleRequest($request);
        if ($formMention->isSubmitted() && $formMention->isValid()) {
            $group->setAdMentionLegals($adMentionLegals);
            $em->persist($group);
            $em->flush();
        }

        if($request->request->has("agency_distribution_submit")) {
            $result = $this->updateCommercialDistribution($request, $group);
            if($result['success']){
                $this->addFlash('success', $this->get('translator')->trans($result['message'], [], 'commiti'));
            }else
                $this->addFlash('error', $this->get('translator')->trans($result['error'], [], 'commiti'));

        }

        /** @var \AppBundle\Repository\Broadcast\AdDailyLimitRepository $adDailyLimitRepository **/
        $commercials = $em->getRepository('AppBundle:User')->getCommercialOnly($group);
        $commercialMap = [];
        /**
         * construct commercialMap
         * @var \AppBundle\Entity\User $commercial
         */
        foreach ($commercials as $commercial) {
            $portalLimit = [];
            /**@var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialsPortal **/
            foreach ($commercial->getCommercialsPortal() as $commercialsPortal) {
                if($commercialsPortal->getPortal()->getCluster() == 'PAYANT'){
                    $used = $adRepository->getOlderAdsByCommercialCount($commercial, $commercialsPortal->getPortal());
                    $portalLimit["portal_".$commercialsPortal->getPortal()->getId()] = [
                        "id"    => $commercialsPortal->getId(),
                        "limit" => $commercialsPortal->getLimit(),
                        "used"  => $used
                    ];
                }

            }
            $commercialMap[] = [
                "id"=> $commercial->getId(),
                "name"=> $commercial->getName(),
                "portals"=> $portalLimit,
            ];
        }

        return [
            'group'             => $group,
            'groupAdvantages'   => $groupAdvantages,
            'payedAdvantages'   => $payedAdvantages,
            'houseModelCount'   => $houseModelCount,
            'adCount'           => $adCount,
            'trackingAdvantage' => $trackingAdvantage,
            'myTrackingAdvantages' => $myTrackingAdvantages,
            'emptyTrackingAdvantage' => $emptyTrackingAdvantage,
            'statistics'        => $statistics,
            'employeeCount'     => $employeeCount,
            'employeeDatatable' => $employeeDatatable,
            'invoicesDatatable' => $invoicesDatatable,
            'adsDatatable'           => $adsDatatable,
            'houseModeleDatatable'   => $houseModeleDatatable,
            'discounts'              => $discounts,
            'commercialMap'          => $commercialMap,
            'formRewrite'            => $formRewrite->createView(),
            'formMention'            => $formMention->createView()
        ];
    }

    /**
     * @return Response
     *
     * @Route("/agences/delete/{remoteId}", options={"expose"=true}, name="group_agency_delete" )
     * @Method({"GET"})
     **/
    public function deleteAgencyAction(Group $group){
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$this->getUser()->getGroup()->hasRole('ROLE_GROUP') || ($this->getUser()->getGroup()->hasRole('ROLE_GROUP') && !$group->isMemberOf($this->getUser()->getGroup())))
            throw new AccessDeniedHttpException();
        $parentGroup = !is_null($group) && ($this->getUser()->hasRole('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ) ? $group : $this->getUser()->getGroup();

        $em = $this->getDoctrine()->getManager();
        if($group->getAgency()){
            $em->getRepository("TrackingBundle:Phone")->deleteAgencyPhones($group->getAgency());
        }
        $em->remove($group);
        $em->flush();
        $this->addFlash('success', 'agency_deleted');
        if($this->getUser()->hasRole('ROLE_ADMIN'))
            return $this->redirectToRoute('groups_admin_show', ['remoteId' => $parentGroup->getRemoteId()]);
        return $this->redirectToRoute('group_agencies_list');
    }

}