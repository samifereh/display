<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Program;
use AppBundle\Form\Type\UserType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\File\File;

use AppBundle\Form\Type\ImageUploadableType;

use AppBundle\Entity\User as User;

use AppBundle\Repository\UserRepository as UserRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class UsersController
 * @package AppBundle\Controller
 * @Route("/admin/utilisateurs")
 * @Security("has_role('ROLE_ADMIN')")
 */
class UsersController extends BaseController
{

    /**
     * @Route("/", name="users_admin" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Users:admin_list.html.twig")
     **/
    public function adminListAction(){

        $datatable = $this->get('app.datatable.user');
        $datatable->buildDatatable();

        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="users_admin_show" )
     * @Method({"GET"})
     * @Template("AppBundle:Users:admin_show.html.twig")
     **/
    public function adminShowAction(User $user){

        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Repository\UserRepository $repo */
        $repo = $em->getRepository('AppBundle:User');
        /** @var \AppBundle\Entity\User $repo */
        $user = $repo->find($user->getId());

        if($user === null)
            throw $this->createNotFoundException('Unknown user ' . $user->getId());

        $portalsRegistrationsList = null;
        if($user->hasRole('ROLE_SALESMAN')) {
            $em = $this->getDoctrine()->getManager();
            $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
            $portalsRegistrations = $portalRegistrationRepository->findBy(['group'=>$user->getAgencyGroup()]);
            foreach ($portalsRegistrations as $portalsRegistration)
                $portalsRegistrationsList[$portalsRegistration->getBroadcastPortal()->getId()] = $portalsRegistration;
        }
        return [
            'user'                  => $user,
            'portalsRegistrations'  => $portalsRegistrationsList
        ];

    }

    /**
     * @Route("/creation", options={"expose"=true}, name="users_admin_create" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Users:admin_create.html.twig")
     **/
    public function adminUserCreationAction(Request $request){

        $em         = $this->getDoctrine()->getManager();
        $user       = new User();
        $imageError = false;
        $form       = $this->createForm(UserType::class,$user,['roles' => UserRepository::getUsersRoles()]);
        if($request->getMethod() == 'POST') {
        $form->handleRequest($request);
            if ($form->isSubmitted()) {
                $user = $form->getData(); // set the $user value
                $user->setPlainPassword($form->get('password')->getData());
                if ($form->isValid()) {
                    if($user->getAvatar()) {
                        $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                        $uploadableManager->markEntityToUpload($user, $user->getAvatar());
                    }
                    if($user->hasRole('ROLE_BRANDLEADER'))
                        $user->addRole('ROLE_SALESMAN');
                    $userManager = $this->get('fos_user.user_manager');
                    $user->setEnabled(true);
                    $userManager->updateUser($user);
                    //send email for new account User
                    $password = $form->get('password')->getData();
                    $this->get("app.mailer.manager")->sendNewAccount(
                        [
                            'user'     => $user,
                            'password'  =>$password
                        ]);
                    
                    $this->addFlash("success", "Utilisateur ajouter avec succès");
                    return $this->redirectToRoute('users_admin');
                }
            }
        }

        return [
            'user_form'  => $form->createView(),
            'imageError' => $imageError
        ];
    }


    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="users_admin_edit" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Users:admin_edit.html.twig")
     **/
    public function adminEditAction(Request $request,User $user){

        $em = $this->getDoctrine()->getManager();
        $current = array(
            'avatar'   => $user->getAvatar(),
            'password' => $user->getPassword()
        );

        $form = $this->createForm(UserType::class,$user,['roles' => UserRepository::getUsersRoles()]);
        if($request->getMethod() == 'POST') {
        $form->handleRequest($request);
            if ($form->isSubmitted()) {
                $user = $form->getData(); // set the $user value
                if(!empty($form->get('password')->getData()))
                    $user->setPlainPassword($form->get('password')->getData());
                else
                    $user->setPassword($current['password']);

                if ($form->isValid()) {
                    if(null === $form->get("avatar")->getData())
                        $user->setAvatar($current['avatar']);
                    else{
                        $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                        $uploadableManager->markEntityToUpload($user, $user->getAvatar());
                    }

                    if($user->hasRole('ROLE_BRANDLEADER') && !$user->addRole('ROLE_SALESMAN'))
                        $user->addRole('ROLE_SALESMAN');

                    $em->persist($user);
                    $em->flush();

                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($user);
                    $this->addFlash("success", "Modification affecter avec success");
                    return $this->redirectToRoute('users_admin');
                }
            }
        }

        return [
            'user_form'  => $form->createView(),
            'user'       => $user
        ];
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="users_admin_delete" )
     * @Method({"GET"})
     * @Template
     **/
    public function adminDeleteAction(User $user){

        $em = $this->getDoctrine()->getManager();
        //todo: if user is commercial, recalcule portals ad rest
        $user = $em->getRepository('AppBundle:User')
                            ->find($user->getId());
        if($user->getId() == $this->getUser()->getId()) { throw $this->createNotFoundException('You can\'t remove yourself' ); }
        if($user !== NULL){
            if($user->hasRole('ROLE_SALESMAN')) {
                $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
                $commercialsPortalsRepository = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
                /** @var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortal */
                foreach ($user->getCommercialsPortal() as $commercialPortal) {
                    $portailRegistration = $portalRegistrationRepository->findOneBy(['broadcastPortal' => $commercialPortal->getPortal(), 'group'=>$user->getAgencyGroup()]);
                    if($portailRegistration) {
                        $commercialsPortals  = $commercialsPortalsRepository->getCommercialWithGroupAndPortal($commercialPortal->getPortal(), $user);
                        $portailRegistration->setAdRest($portailRegistration->getAdLimit() - $commercialsPortals['usedLimit']);
                        $em->persist($portailRegistration);
                    }
                }
                $em->flush();
            }
            $em->remove($user);
            $em->flush();
        }
        $this->addFlash("success", "Utilisateur supprimé");
        return $this->redirectToRoute('users_admin');

    }

    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="users_switch_enabled_status")
     * @Method({"GET","POST"})
     */
    public function switchEnabledStatusAction(Request $request, User $user)
    {

        if($request->isXmlHttpRequest()) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                if (!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$this->getUser()->isEmployeeOf($user->getGroup()))
                    throw new AccessDeniedHttpException();


            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(!$user->isEnabled());
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['_callback' => $user->isEnabled()]);
        }
        throw new AccessDeniedHttpException();
    }

}