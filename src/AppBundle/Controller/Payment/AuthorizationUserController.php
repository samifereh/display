<?php
/**
 * Created by PhpStorm.
 * User: yesser
 * Date: 10/08/17
 * Time: 12:08 م
 */

namespace AppBundle\Controller\Payment;

use AppBundle\Controller\BaseController;

use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Payment\AutorizedUserType;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupsController
 * @package AppBundle\Controller
 * @Route("/admin/groupes/autorization")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AuthorizationUserController extends BaseController
{
    /**
     * @Route("/{remoteId}/create", name="autorization_create")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Payment:Autorization\create.html.twig")
     */
    public function createAction(Request $request, Group $group) {
        $authorizedUser = new AuthorizedUser();
        $form = $this->createForm(new AutorizedUserType(), $authorizedUser);
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $duplicate = $request->request->has('authorized_user') ? $request->request->get('authorized_user')['duplicate'] : 1;
            $authorizedUser->setGroup($group);
            for($i = 0; $i < $duplicate; $i++) {
                $clone = clone $authorizedUser;
                $em->persist($clone);
            }
            $em->flush();
            $this->addFlash("success", "Les autorisations ont été ajouter");
            return $this->redirectToRoute('groups_admin_authorization',['remoteId' => $group->getRemoteId()]);
        }
        return [
            'form'       => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", options={"expose"=true}, name="autorization_delete")
     * @Method({"GET","POST"})
     */
    public function deleteAction(Request $request, AuthorizedUser $authorizedUser) {
        $em = $this->getDoctrine()->getManager();
        $group = $authorizedUser->getGroup();
        $em->remove($authorizedUser);
        $em->flush();
        $this->addFlash("success", "Les autorisations ont été ajouter");
        return $this->redirectToRoute('groups_admin_authorization',['remoteId' => $group->getRemoteId()]);
    }

}