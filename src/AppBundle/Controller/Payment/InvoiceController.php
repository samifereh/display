<?php
/**
 * Created by PhpStorm.
 * User: yesser
 * Date: 10/08/17
 * Time: 12:08 م
 */

namespace AppBundle\Controller\Payment;

use AppBundle\Controller\BaseController;

use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\Payment\Order;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Payment\AutorizedUserType;
use AppBundle\Form\Type\Payment\OrderType;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GroupsController
 * @package AppBundle\Controller
 * @Route("/admin/invoices")
 * @Security("has_role('ROLE_ADMIN')")
 */
class InvoiceController extends BaseController
{
    /**
     * @Route("/create", name="invoice_create")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Payment:Invoices\create.html.twig")
     */
    public function createAction(Request $request) {
        $order = new Order();
        $form = $this->createForm(new OrderType(), $order);
        $em = $this->getDoctrine()->getManager();
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->isValid()) {
                $reference = $request->request->get('invoice')['reference'];
                $order->setStatus(Order::STATUS_WAITING_FOR_PAYMENT);
                $order->setExecutionDate(new \DateTime());
                $order->setHTTotalAmount($order->getTotalAmount()/1.2);
                $order->setDetails([
                    [
                        'title'       => $order->getTitle(),
                        'discount'    => 0,
                        'quantity'    => 1,
                        'HT'          => $order->getTotalAmount()/1.2,
                        'TTC'         => $order->getTotalAmount(),
                        'reference'   => $reference,
                    ]
                ]);
                $em->persist($order);
                $em->flush();
                $this->addFlash("success", "La facture à été ajouter");
                return $this->redirectToRoute('homepage');
            }
        }
        return [
            'form'       => $form->createView()
        ];
    }
}