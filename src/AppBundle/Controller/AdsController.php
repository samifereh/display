<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Repository\Broadcast\PortalRegistrationRepository;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sluggable\Fixture\Validate;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AdsController
 * @package AppBundle\Controller
 * @Route("/annonces")
 */
class AdsController extends BaseController
{

    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="ads_index" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:index.html.twig")
     **/
    public function indexAction(Request $request)
    {
        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $user         = $this->getUser();
        $data = $request->query->has('ads_search') ? $request->query->get('ads_search') : [];
        if(empty($data['portalDiffusion'])) unset($data['portalDiffusion']);
        if(empty($data['dateRemonterMin'])) unset($data['dateRemonterMin']);
        else $data['dateRemonterMin'] = \DateTime::createFromFormat('d/m/Y',$data['dateRemonterMin']);
        if(empty($data['dateRemonterMax'])) unset($data['dateRemonterMax']);
        else $data['dateRemonterMax'] = \DateTime::createFromFormat('d/m/Y',$data['dateRemonterMax']);

        $form = $this->createForm(new AdsSearchType($this->getDoctrine(), $this->getAuthorizationChecker()),$data, ['user' => $user]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            if(!empty($data['portalDiffusion']))
            {
                $pList = [];
                foreach ($data['portalDiffusion'] as $p)
                    $pList[] = $p->getId();
                $data['portalDiffusion'] = $pList;
            }
            if(!empty($data['dateRemonterMin']))
                $data['dateRemonterMin'] = ['date' => $data['dateRemonterMin']->format('Y-m-d')];
            if(!empty($data['dateRemonterMax']))
                $data['dateRemonterMax'] = ['date' => $data['dateRemonterMax']->format('Y-m-d')];
            if(!empty($data['commercial']))
                $data['commercial'] = $data['commercial']->getId();
            if(!empty($data['group']))
            {
                $data['group'] = $data['group']->getRemoteId();
                $group = $data['group'];
            }
            if(!empty($data['bigGroups']))
            {
                $data['bigGroups'] = $data['bigGroups']->getId();
            }

        }

//       if(!$this->getUser()->hasRole('ROLE_SALESMAN') && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
//          return $this->redirectToRoute('switch_commercial');

        $trackingAdvantage = null;
        if(is_object($group)) {
            /**
             * tracking ad
             * @var \TrackingBundle\Repository\AdvantageRepository $trackingRepository
             */
            $trackingRepository = $this->getDoctrine()->getRepository("TrackingBundle:Advantage");
            /** @var \AppBundle\Entity\Configuration\Advantage $trackingAdvantage */
            $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), "ad");
        }
            $datatable = $this->get('app.datatable.ad');
            $datatable->buildDatatable([
                'group' => $group,
                'data'  => $data,
                'trackingAdvantage' => $trackingAdvantage
            ]);

        return [
            'datatable' => $datatable,
            'form'      => $form->createView(),
            'data' => $data,
            'trackingAdvantage' => $trackingAdvantage,
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation", name="ads_create")
     * @Method({"GET"})
     */
    public function createAction(Request $request)
    {
        $user         = $this->getUser();
        $groupsList   = null;
        $parentGroup  = $user->getGroup();
        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
            $groupsList  = $this->getDoctrine()->getRepository('AppBundle:Group')->findGroups();
            $parentGroup = $this->getUserService()->checkUserGroup($request);
        }

        $agenciesList = null;
        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') ||
            ($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP'))) {
            $group        = $this->getUserService()->checkUserAgency($request);
            $agenciesList = $this->getDoctrine()->getRepository('AppBundle:Group')->getAgencyGroups($parentGroup);
        } else {
            $group       = $user->getAgencyGroup();
        }

        $commercials = null;
        if($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $group) {
            $em = $this->getDoctrine()->getManager();
            $userRepository = $em->getRepository('AppBundle:User');
            $commercials = $userRepository->getCommercialOnly($group);
        }
        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if($commercial) {
            $houseModels = $this->getDoctrine()->getRepository('AppBundle:HouseModel')->findHouseModels($commercial->getGroup(),$commercial);
        } else {
            $houseModels = $this->getDoctrine()->getRepository('AppBundle:HouseModel')->findHouseModels($group,$user);
        }
        return $this->render('AppBundle:Ads:create.html.twig',[
            'houseModels' => $houseModels,
            'commercials' => $commercials,
            'agenciesList'=> $agenciesList,
            'groupsList'  => $groupsList
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/changer-commercial", options={"expose"=true}, name="switch_commercial")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:switch_commercial.html.twig")
     * @Security("has_role('ROLE_DIRECTOR') or has_role('ROLE_BRANDLEADER')")
     */
    public function switchCommercialAction(Request $request)
    {
        $user         = $this->getUser();
        $groupsList   = null;
        $parentGroup  = null;
        $agenciesList = null;
        $parentGroups  = $user->getGroups();
        $em = $this->getDoctrine()->getManager();
        if(
            $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') ||
            $this->getAuthorizationChecker()->isGranted('ROLE_GROUP') ||
            $this->getAuthorizationChecker()->isGranted('ROLE_DIRECTOR') ||
            (
                $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')
                && $parentGroups->county() > 0
            )
        ) {
            $groupsList  = $this->getDoctrine()->getRepository('AppBundle:Group')->findGroups();
//            $parentGroup = $this->getUserService()->checkUserGroup($request);
            if(!$parentGroup && $request->query->get('parentGroupId'))
                $parentGroup =  $em->getRepository('AppBundle:Group')->findOneBy(['remoteId' => $request->query->get('parentGroupId')]);

            if($parentGroup instanceof Group) {
                $agenciesList = $parentGroup->getAgencyGroups();
            } else {
                $agenciesList = new ArrayCollection();
                /** @var Group $parentGroup */
                foreach ($parentGroups as $parentGroup)
                    foreach ($parentGroup->getAgencyGroups() as $item)
                        $agenciesList->add($item);
            }
            $group        = $this->getUserService()->checkUserAgency($request);
        } else {
            $group       = $user->getAgencyGroup();
        }
        if((!$group || $group == 'all') && $request->query->get('groupId'))
            $group  = $this->getDoctrine()->getRepository('AppBundle:Group')->findOneBy(['remoteId' => $request->query->get('groupId')]);

        $userRepository = $em->getRepository('AppBundle:User');

        $commercials = $userRepository->getCommercialOnly($group);
        if($request->isXmlHttpRequest()) {
            $jsonCommecials = $jsonAgences = [['id' => '', 'text' => '------']];
            foreach ($commercials as $commercial)
                $jsonCommecials[] = ['id' => $commercial->getRemoteId(), 'text' => $commercial->getName()];
            foreach ($agenciesList as $agence)
                $jsonAgences[] = ['id' => $agence->getRemoteId(), 'text' => $agence->getName()];
            $houseModels = $this->getDoctrine()->getRepository('AppBundle:HouseModel')->findBy( [ 'group' => $group ] );
            $houseModelsDuoView = $this->renderView('AppBundle:HouseModels:includes/modal.html.twig',['houseModels' => $houseModels,
                'projectType' => 'duo']);
            $houseModelsProjectView = $this->renderView('AppBundle:HouseModels:includes/modal.html.twig',['houseModels' => $houseModels,
                'projectType' => 'projet']);
            return new JsonResponse(['commercials' => $jsonCommecials,'agences' => $jsonAgences,
                'houseModels' => ['duo' => $houseModelsDuoView, 'projet'=>$houseModelsProjectView]]);
        }
        return [
            'commercials' => $commercials,
            'agenciesList'=> $agenciesList,
            'groupsList'  => $groupsList
        ];
    }

    /**
     * @param Request $request
     * @param HouseModel $houseModel
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation/duo/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="add_ads_duo_project")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:create_duo_project.html.twig")
     */
    public function createDuoProjectAction(Request $request, HouseModel $houseModel = null)
    {
        $user        = $this->getUser();
        $houseModel = $houseModel && $houseModel->getRemoteId() ? $houseModel : null;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if($houseModel && !$user->isEmployeeOf($houseModel->getGroup()))
                throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);
        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$commercial) {
            $userRepository = $em->getRepository('AppBundle:User');
            if($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && !$userRepository->getCommercialOnly($group)) {
                $this->addFlash("error", "Vous devez avoir au moin un commercial pour crée une annonce");
                if(is_string($group))
                    return $this->redirectToRoute('homepage');
                return $this->redirectToRoute('agency_employee_add',['remoteId' => $group->getRemoteId()]);
            }
            throw new AccessDeniedHttpException();
        }

        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $session     = $request->getSession();
        $brouillonDuo= null;
        $oldImage    = null;
        $duo         = true;
        /** @var AdDuoFlow $flow */
        $flow        = $this->get('app.form.flow.ad_duo');

        if(!$session->get('brouillon_ad_duo_id')) {
            $brouillonDuo = new Brouillon();
            $brouillonDuo->setSessionId($session->getId());
            $em->persist($brouillonDuo);
            $em->flush();
            $session->set('brouillon_ad_duo_id',$brouillonDuo->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillonDuo           = $brouillonRepository->find($session->get('brouillon_ad_duo_id'));
        }
        if(!$session->get('brouillon_ad_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_ad_id',$brouillon->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_ad_id'));
        }

        $ad = new Ad();
        $ad->setTypeDiffusion('duo');
        $ad->addBienImmobilier(new Terrain());
        if(!$houseModel)
            $ad->addBienImmobilier(new Maison());
        else {
            $maison = clone $houseModel->getMaison();
            $ad->addBienImmobilier($maison);
            $ad->setDescription($houseModel->getMaison()->getDescription());
            $ad->setImagePrincipale($houseModel->getImagePrincipale());
            $ad->setTitreImagePrincipale($houseModel->getTitreImagePrincipale());
        }

        $flow->bind($ad);

        if($houseModel){
            $ad->setImagePrincipale($houseModel->getImagePrincipale());
            foreach ($houseModel->getDocuments() as $document)
                $ad->addDocument($document);
        }

        $form = $submittedForm = $flow->createForm([
            'isDuo' => true,
            'checkImageEmpty' => $houseModel ? false : true,
        ]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                $ad->synchroDuoStep1($request);
            } elseif($flow->getCurrentStepNumber() == 2) {
                if($brouillon->getImagePrincipale() || $brouillonDuo->getImagePrincipale()) {
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                } elseif(!$houseModel) {
                    $error = new FormError("l'image principale est obligatoire.");
                    $form->get('imagePrincipale')->addError($error);
                    return [
                        'form'        => $form->createView(),
                        'flow'        => $flow,
                        'ad'          => $ad,
                        'houseModel'  => $houseModel,
                        'isDuo'       => $duo,
                        'brouillon'   => $brouillon,
                        'brouillonDuo'   => $brouillonDuo
                    ];
                }
            }
            $sendMail = false;
            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                $brouillon->getImagePrincipale() ? $ad->setImagePrincipale($brouillon->getImagePrincipale()) : $ad->setImagePrincipale($houseModel->getImagePrincipale());
                $ad->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                if($user->hasRole('ROLE_SALESMAN') && $user->getAdsModeration()) {
                    $isValidated = false;
                    $ad->setStat(Ad::PENDING);
                    $ad->setPublished(false);
                    $sendMail = true;

                } else {
                    $ad->setStat(Ad::VALIDATE);
                    $ad->setPublished(true);
                    $isValidated = true;
                }
                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillonDuo->getDocuments() as $document)
                    $ad->addDocument($document);

                $ad->setTypeDiffusion(Ad::TYPE_PROJECT);

                $adTerrain = clone $ad;
                $adTerrain->setTypeDiffusion(Ad::TYPE_TERRAIN);
                $adTerrain->removeBienImmobilier($adTerrain->getMaison());
                $adTerrain->setLibelle($ad->getTerrain()->getTitle());
                $adTerrain->setPrix($ad->getTerrain()->getPrix());
                if($brouillonDuo->getImagePrincipale())
                    $adTerrain->setImagePrincipale($brouillonDuo->getImagePrincipale());
                if($session->get('flow_ad_data')['3']['bienImmobilier'][0]['titreImagePrincipale'])
                    $adTerrain->setTitreImagePrincipale($session->get('flow_ad_data')['3']['bienImmobilier'][0]['titreImagePrincipale']);
                $adTerrain->setDocuments(null);
                foreach ($brouillonDuo->getDocuments() as $document)
                    $adTerrain->addDocument($document);
                $adTerrain->setStat($isValidated ? Ad::VALIDATE : Ad::PENDING);
                $adTerrain->setPublished($isValidated ? true : false);

                $em->persist($adTerrain);
                $em->flush();

                $session->set('second_diffusion',$adTerrain->getRemoteId());

                if($brouillonDuo) {
                    $brouillonDuo->setDocuments(null);
                    $brouillonDuo->setImagePrincipale(null);
                    $em->remove($brouillonDuo);
                    $em->flush();
                    $session->remove('brouillon_ad_duo_id');
                }
                $this->resolveImages($ad);
                if($houseModel) {
                    $ad->setHouseModel($houseModel);
                    $ad->setHasHouseModel(true);
                }
                $em->persist($ad);
                $em->flush();

                //Send Email
                if($sendMail) {
                    $userRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
                    $chef = $userRepository->findByRole('ROLE_BRANDLEADER', $group);

                    $this->get("app.mailer.manager")->sendRequestAdCreate(
                        [
                            "chef"      => $chef,
                            'ad'     => $ad
                        ]);
                }
                $this->addFlash("success", "L'annonce a été ajouter");
                $flow->reset();

                return new RedirectResponse($this->generateUrl('diffusion_ads_project',['remoteId' => $ad->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'flow'        => $flow,
            'ad'          => $ad,
            'houseModel'  => $houseModel,
            'isDuo'       => $duo,
            'brouillonDuo'=> $brouillonDuo,
            'brouillon'=> $brouillon
        ];

    }

    /**
     * @param Request $request
     * @param HouseModel $houseModel
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation/{projectType}/{remoteId}",requirements={"projectType" = "terrain|projet"}, defaults={"remoteId" = null}, options={"expose"=true}, name="add_ads_project")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:create_project.html.twig")
     */
    public function createProjectAction(Request $request,$projectType, HouseModel $houseModel = null)
    {
        $user        = $this->getUser();
        $houseModel = $houseModel && $houseModel->getRemoteId() ? $houseModel : null;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if($houseModel && !$user->isEmployeeOf($houseModel->getGroup()))
            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);
        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$commercial) {
            $userRepository = $em->getRepository('AppBundle:User');
            if($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && !$userRepository->getCommercialOnly($group)) {
                $this->addFlash("error", "Vous devez avoir au moin un commercial pour crée une annonce");
                return $this->redirectToRoute('agency_employee_add',['remoteId' => $group->getRemoteId()]);
            }
            throw new AccessDeniedHttpException();
        }

        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $session     = $request->getSession();
        $terrainSeul = $projectType == 'terrain' ? true : null;
        $duo         = false;
        $brouillon   = null;
        $oldImage    = null;
        if($terrainSeul)
            $flow        = $this->get('app.form.flow.ad_land');
        else
            $flow        = $this->get('app.form.flow.ad');

        if(!$session->get('brouillon_ad_id')) {
            $brouillon = $this->getUsefulHelper()->createBrouillon('brouillon_ad_id');
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_ad_id'));
            if($brouillon && !is_null($brouillon->getTargetId())) {
                $brouillon = $this->getUsefulHelper()->createBrouillon('brouillon_ad_id');
                $flow->reset();
            }
            if(!$brouillon) {
                $brouillon = new Brouillon();
                $brouillon->setSessionId($session->getId());
                $em->persist($brouillon);
                $em->flush();
                $session->set('brouillon_ad_id',$brouillon->getId());
            }
        }

        $ad = new Ad();
        $ad->setTypeDiffusion($projectType);
        switch($projectType) {
            case Ad::TYPE_TERRAIN:
                $ad->addBienImmobilier(new Terrain());
                break;
            case Ad::TYPE_PROJECT:
                $ad->addBienImmobilier(new Terrain());
                if(!$houseModel)
                    $ad->addBienImmobilier(new Maison());
                else {
                    $maison = clone $houseModel->getMaison();
                    $ad->addBienImmobilier($maison);
                    $ad->setDescription($houseModel->getMaison()->getDescription());
                    $ad->setImagePrincipale($houseModel->getImagePrincipale());
                    $ad->setTitreImagePrincipale($houseModel->getTitreImagePrincipale());
                }
            break;
        }
        $flow->bind($ad);

        if($houseModel){
            $ad->setImagePrincipale($houseModel->getImagePrincipale());
            foreach ($houseModel->getDocuments() as $document)
                $ad->addDocument($document);
        }
        $form = $submittedForm = $flow->createForm([
            'isDuo' => false,
            'checkImageEmpty' => $houseModel ? false : true,
        ]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                if($brouillon->getImagePrincipale()) {
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                } elseif(!$houseModel) {
                    $error = new FormError("l'image principale est obligatoire.");
                    $form->get('imagePrincipale')->addError($error);
                    return [
                        'form'        => $form->createView(),
                        'flow'        => $flow,
                        'ad'          => $ad,
                        'houseModel'  => $houseModel,
                        'terrainSeul' => $terrainSeul,
                        'isDuo'       => $duo,
                        'brouillon'   => $brouillon,
                    ];
                }
            }

            $sendMail = false;
            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                $brouillon->getImagePrincipale() ? $ad->setImagePrincipale($brouillon->getImagePrincipale()) : $ad->setImagePrincipale($houseModel->getImagePrincipale());
                $ad->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                if($user->hasRole('ROLE_SALESMAN') && $user->getAdsModeration()) {
                    $ad->setStat(Ad::PENDING);
                    $ad->setPublished(false);
                    $sendMail = true;
                } else {
                    $ad->setStat(Ad::VALIDATE);
                    $ad->setPublished(true);
                }

                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillon->getDocuments() as $document)
                    $ad->addDocument($document);

                if($houseModel) {
                    $ad->setHouseModel($houseModel);
                    $ad->setHasHouseModel(true);
                }

                $em->persist($ad);
                $em->flush();

                $this->resolveImages($ad);

                $this->addFlash("success", "L'annonce a été ajouter");
                //Send Email
                if($sendMail) {
                    $userRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
                    $chef = $userRepository->findByRole('ROLE_BRANDLEADER', $group);

                    $this->get("app.mailer.manager")->sendRequestAdCreate(
                        [
                            "chef"      => $chef,
                            'ad'     => $ad
                        ]);
                }

                $flow->reset();

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $brouillon->setImagePrincipale(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_ad_id');
                }
                return new RedirectResponse($this->generateUrl('diffusion_ads_project',['remoteId' => $ad->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'flow'        => $flow,
            'ad'          => $ad,
            'houseModel'  => $houseModel,
            'terrainSeul' => $terrainSeul,
            'isDuo'       => $duo,
            'brouillon'   => $brouillon
        ];
    }


    /**
     * @param Request $request
     * @param Ad $ad
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/diffusion/{remoteId}", options={"expose"=true}, name="diffusion_ads_project")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:diffusion_project.html.twig")
     */
    public function diffusionProjectAction(Request $request, Ad $ad)
    {
        $user        = $this->getUser();
        $group       = $this->getCurrentGroup($request);
        $commercial  = $user->hasRole('ROLE_SALESMAN') ? $user : $this->getUserService()->checkCommercialUser($request, $group);

        if(!$commercial) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
                throw new AccessDeniedHttpException();
            $commercial = $ad->getOwner();
        }
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && !$commercial->equals($ad->getOwner()))
            throw new AccessDeniedHttpException();

        $agenceGroup = $commercial->getAgencyGroup();
        $diffusion   = $ad->getDiffusion() ? $ad->getDiffusion() : new Diffusion();
        $diffusionTerrain = $ad->getTypeDiffusion() == 'terrain' ?  true : false;
        $oldBroadcastPortals = clone $diffusion->getBroadcastPortals();

        $commercialPortalsLimit = $this->getDoctrine()->
        getRepository('AppBundle:Broadcast\CommercialPortalLimit')->
        findBy(['commercial'=>$commercial]);
        $adRepository = $this->getDoctrine()->getRepository('AppBundle:Ad');
        $alreadyPayedPortals = $this->getDoctrine()->getRepository('AppBundle:Broadcast\PortalRegistration')->findBy([
            'group'         => $agenceGroup,
            'isEnabled'     => true
        ]);
        $alreadyPayedPortalList = $commercialLimit = [];

        $portalsIds = [];


        /** @var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortalLimit */
        foreach ($commercialPortalsLimit as $commercialPortalLimit) {
            $commercialLimit[$commercialPortalLimit->getPortal()->getId()]['limit'] = $commercialPortalLimit;
            $used = $adRepository->getOlderAdsByCommercialCount($commercial, $commercialPortalLimit->getPortal());
            $commercialLimit[$commercialPortalLimit->getPortal()->getId()]['rest'] = $commercialPortalLimit->getLimit() - $used ;
            if(
                $commercialPortalLimit->getLimit() > 0 ||
                $commercialPortalLimit->getPortal()->getCluster() == 'GRATUIT') {
                $portalsIds[] = $commercialPortalLimit->getPortal()->getId();
            }
        }
        // get GRATUIT portal for group
        $groupFreePortals = $this->getDoctrine()->
        getRepository('AppBundle:Broadcast\PortalRegistration')
            ->getPortalRegistrationByGroup($group,false, "GRATUIT");
        /** @var PortalRegistration $groupFreePortal **/
        foreach ($groupFreePortals as $groupFreePortal) {
            $portalsIds[] = $groupFreePortal->getBroadcastPortal()->getId();
        }
        $allPortals = $this->getDoctrine()->
        getRepository('AppBundle:Broadcast\Portal')->
        getAutorizedPortals($portalsIds,false,$diffusionTerrain);
        /** @var \AppBundle\Entity\Broadcast\PortalRegistration $alreadyPayedPortal */
        foreach ($alreadyPayedPortals as $alreadyPayedPortal)
            $alreadyPayedPortalList[$alreadyPayedPortal->getId()] =
                $alreadyPayedPortal->getBroadcastPortal();

        $substractValue = 1;
        $form = $this->createForm(new DiffusionType(),$diffusion, [
            'ad'                  => $ad,
            'allPortals'          => $allPortals,
            'alreadyPayedPortals' => $alreadyPayedPortalList,
            'commercialLimit'     => $commercialLimit,
            'substractValue'      => $substractValue
        ]);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->get('cancel')->isClicked()) {
                return $this->redirectToRoute('ads_index');
            } elseif($form->get('submit')->isClicked()) {
                if ($form->isValid()) {
                    $broadcastPostals = $diffusion->getBroadcastPortals();
                    $em               = $this->getDoctrine()->getManager();

                    $deletedPortals = [];
                    foreach ($oldBroadcastPortals as $p) {
                        if(!$p->existIn($broadcastPostals)) {
                            $deletedPortals[] = $p;
                        }
                    }


                    if(count($broadcastPostals) > 0) {
                        foreach ($broadcastPostals as $broadcastPostal) {
                            if($broadcastPostal->getCluster() != 'GRATUIT') {
                                $rest = 0;
                                if($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()]))
                                    $rest = $commercialLimit[$broadcastPostal->getId()]['rest'] ? ($commercialLimit[$broadcastPostal->getId()]['rest'] - $substractValue) : ($commercialLimit[$broadcastPostal->getId()]['limit']->getLimit() - $substractValue);

                                if(
                                    ($broadcastPostal->getCluster() == 'PAYANT' && !$broadcastPostal->existIn($alreadyPayedPortalList)) ||
                                    ($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()]) && $rest < 0 )
                                ) {
                                    $diffusion->removeBroadcastPortal($broadcastPostal);
                                    $this->addFlash("error", "Pas tout les cibles selectionner serons diffuser");
                                }


                            }
                        }
                    }

                    if(count($deletedPortals) > 0) {
                        foreach ($deletedPortals as $broadcastPostal) {
                            if(
                                $broadcastPostal->getCluster() != 'GRATUIT' &&
                                $broadcastPostal->existIn($alreadyPayedPortalList) &&
                                isset($commercialLimit[$broadcastPostal->getId()])
                            ){
                                    $diffusion->removeBroadcastPortal($broadcastPostal);
                            }
                        }
                    }

                    $diffusion->setCommercial($commercial);
                    $em->persist($diffusion);
                    $em->flush();
                    $ad->setDiffusion($diffusion);
                    $em->flush();

                    $this->addFlash("success", "La diffusion a été enregistrer avec succes");
                /*
                } else {
                        $this->addFlash("warning", "Aucun portail n'a été selectionner pour la diffusion");
                    }*/
                    if($secondRemoteId = $this->get('session')->get('second_diffusion')) {
                        if($ad->getRemoteId() == $secondRemoteId) {
                            $this->get('session')->remove('second_diffusion');
                            return new RedirectResponse($this->generateUrl('ads_index'));
                        }
                        return new RedirectResponse($this->generateUrl('diffusion_ads_project',['remoteId' => $secondRemoteId]));
                    } elseif($ad->getProgram()) {
                        return $this->redirectToRoute('program_view',['remoteId' => $ad->getProgram()->getRemoteId()]);
                    }
                    return new RedirectResponse($this->generateUrl('ads_index'));
                }
            }
        }
        return [
            'ad'                           => $ad,
            'commercial'                   => $commercial,
            'commercialLimit'              => $commercialLimit,
            'substractValue'               => $substractValue,
            'form'                         => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param Ad $ad
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="ads_edit")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:edit_project.html.twig")
     */
    public function editProjectAction(Request $request, Ad $ad)
    {
        $user        = $this->getUser();
        $group = $ad->getGroup();
        if(!$group){
            $group = $user->getGroup();
        }
        //dump($group);die;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($group))
            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);
        $commercial = $ad->getOwner();

        $oldStatus = $ad->getPublished();
        if(!$commercial)
            $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->hasRole('ROLE_SALESMAN') && !$commercial->equals($ad->getOwner()))
            throw new AccessDeniedHttpException();

        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $session     = $request->getSession();
        $terrainSeul = $ad->getTypeDiffusion() == 'terrain' ? true : null;
        $duo         = $ad->getTypeDiffusion() == 'duo' ? true : false;
        $brouillon   = null;
        if($terrainSeul)
            $flow        = $this->get('app.form.flow.ad_land');
        else
            $flow        = $this->get('app.form.flow.ad');
        if(!$session->get('brouillon_ad_id')) {
            $brouillon = $this->getUsefulHelper()->createBrouillon('brouillon_ad_id',$ad->getId());
            if(!$brouillon) {
                $brouillon = new Brouillon();
                $brouillon->setSessionId($session->getId());
                $em->persist($brouillon);
                $em->flush();
                $session->set('brouillon_ad_id',$brouillon->getId());
            }
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_ad_id'));
            if($brouillon && $brouillon->getTargetId() != $ad->getId()) {
                $em->remove($brouillon);
                $em->flush();
                $brouillon = $this->getUsefulHelper()->createBrouillon('brouillon_ad_id',$ad->getId());
                $flow->reset();
            }
        }
        $houseModel = $ad->getHouseModel();
        $oldImage = $ad->getImagePrincipale();

        $flow->bind($ad);
        $form = $submittedForm = $flow->createForm([
            'checkImageEmpty' => false,
            'isDuo'           => $duo
        ]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                if($brouillon->getImagePrincipale()) {
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                }
            } else {
                $ad->setImagePrincipale($oldImage);
            }
            $sendMail = false;
            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                if($brouillon->getImagePrincipale())
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                $brouillon->setImagePrincipale(null);

                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillon->getDocuments() as $document)
                    $ad->addDocument($document);
                $brouillon->setDocuments(null);

                $ad->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                if($user->hasRole('ROLE_SALESMAN') && $user->getAdsModeration()) {
                    $ad->setStat(Ad::PENDING);
                    $ad->setPublished(false);

                } else {
                    $ad->setStat(Ad::VALIDATE);
                    $ad->setPublished(true);
                    if(!$oldStatus)
                        $sendMail = true;
                }

                $em->persist($ad);
                $em->flush();

                $this->resolveImages($ad);

                $this->addFlash("success", "L'annonce a été ajouter");
                //Send Email
                if($sendMail) {
                    $this->get("app.mailer.manager")->sendAdModerateOk(
                        [
                            "chef"      => $user,
                            'ad'     => $ad
                        ]);
                }
                $flow->reset();

                if($brouillon) {
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_ad_id');
                }
                return new RedirectResponse($this->generateUrl('diffusion_ads_project',['remoteId' => $ad->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'flow'        => $flow,
            'ad'          => $ad,
            'isDuo'       => $duo,
            'houseModel'  => $houseModel,
            'terrainSeul' => $terrainSeul,
            'brouillon'   => $brouillon,
        ];
    }

    /**
     *
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="ads_show")
     * @Method({"GET"})
     * @Template("AppBundle:Ads:show_project.html.twig")
     */
    public function showAction(Request $request, Ad $ad)
    {
        $group = $ad->getProgram() ? $ad->getProgram()->getGroup() : $ad->getGroup();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getUser()->isEmployeeOf($group))
            throw new AccessDeniedHttpException();

        return ['ad' => $ad];
    }

    /**
     *
     * @Route("/print/{remoteId}", options={"expose"=true}, name="ads_print")
     * @Method({"GET"})
     */
    public function printAction(Request $request, Ad $ad)
    {
        $group = $ad->getProgram() ? $ad->getProgram()->getGroup() : $ad->getGroup();
//        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
//        if(!$this->getUser()->isEmployeeOf($group))
//            throw new AccessDeniedHttpException();

        return $this->render('AppBundle:Ads:print.html.twig',['ad' => $ad, 'print' => true]);
    }

    /**
     *
     * @Route("/pdf/{remoteId}", options={"expose"=true}, name="ads_print_pdf")
     * @Method({"GET"})
     */
    public function pdfAction(Request $request, Ad $ad)
    {
        $group = $ad->getProgram() ? $ad->getProgram()->getGroup() : $ad->getGroup();
        $filename = (trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $ad->getLibelle()))).'-'.$ad->getReference();
//        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
//        if(!$this->getUser()->isEmployeeOf($group))
//            throw new AccessDeniedHttpException();

        $html = $this->renderView('AppBundle:Ads:print.html.twig',['ad' => $ad]);
        return new Response(
            $this->getPdfExporter()->getOutputFromHtml($html,[
//                'orientation'=>'Landscape',
                'margin-top'    => 0,
                'margin-right'  => 0,
                'margin-bottom' => 0,
                'margin-left'   => 0,
            ]),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename.'.pdf'),
            ]
        );
    }

    /**
     *
     * @Route("/remonter/{remoteId}", options={"expose"=true}, name="ads_remonter")
     * @Method({"GET"})
     */
    public function remonterAction(Request $request, Ad $ad)
    {
        if(!$this->isGranted('ROLE_ADMIN') && !$this->getUser()->isEmployeeOf($ad->getGroup()))
            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $ad->setNbRemonte($ad->getNbRemonte() + 1);
        $ad->setDateRemonter(new \DateTime('now'));
        $em->persist($ad);
        $em->flush();
        $this->addFlash("success", "L'annonce a été remonté");
        return $this->redirectToRoute('ads_index');
    }

    /**
     *
     * @Route("/delete/{remoteId}", options={"expose"=true}, name="ads_delete")
     * @Method({"GET","DELETE"})
     */
    public function deleteAction(Request $request,Ad $ad)
    {
        $user        = $this->getUser();
        $program = $ad->getProgram();
//        if(!$this->isGranted('ROLE_ADMIN'))
//        if(!$user->isEmployeeOf($ad->getProgram() ? $ad->getProgram()->getGroup() : $ad->getGroup()))
//            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        if($ad->getHouseModel() && $ad->getMaison()) {
            $ad->removeBienImmobilier($ad->getMaison());
        }
        if($program) {
            $ad->setDocuments(null);
        }
        $this->get("app.tracking.export.helpers")->remove($ad->getTrackingPhone());
        $em->remove($ad);
        $em->flush();
        $this->addFlash("success", "L'annonce a été supprimé");
        if($program)
            return $this->redirectToRoute('program_view',['remoteId' => $program->getRemoteId()]);
        return $this->redirectToRoute('ads_index');
    }

    /**
     *
     * @Route("/actions/{action}", options={"expose"=true}, name="ads_actions")
     * @Method({"GET","POST","DELETE"})
     */
    public function multipleActionAction(Request $request,$action)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $choices = $request->request->get('data');
            $token = $request->request->get('token');

            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AppBundle:Ad');

            $em = $this->getDoctrine()->getManager();
            switch($action) {
                case 'delete' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $this->get("app.tracking.export.helpers")->remove($entity->getTrackingPhone());
                        $em->remove($entity);
                    }
                    $em->flush();
                break;
                case 'remonter' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->addNbRemonte();
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
                case 'activer' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(true)->setStat(Ad::VALIDATE);
                        $em->persist($entity);
                        $this->get("app.mailer.manager")->sendAdModerateOk(
                            [
                                "chef"      => $this->getUser(),
                                'ad'     => $entity
                            ]);
                    }
                    $em->flush();
                break;
                case 'desactiver' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(false)->setStat(Ad::PENDING);
                        $this->get("app.tracking.export.helpers")->remove($entity->getTrackingPhone());
                        $em->persist($entity);
                    }
                    $em->flush();
                break;
                case 'tracking' :
                case 'tracking-lot' :
                    $type = "ad";
                    $serviceId = "app.tracking.ad.helpers";
                    if($action == 'tracking-lot') {
                        $type = "lot";
                        $serviceId = "app.tracking.lot.helpers";
                    }
                    $group = $this->getUser()->getAgencyGroup();
                    /**
                     * tracking portal
                     */
                    $trackingRepository = $this->getDoctrine()->getRepository("TrackingBundle:Advantage");
                    /** @var Advantage $trackingAdvantage */
                    $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), $type);
                    if( $trackingAdvantage){
                        $trackingOK = $trackingAdvantage->getPhones()->count();
                        foreach ($choices as $choice) {
                            if ($trackingOK == $trackingAdvantage->getAmount())
                                break;
                            $ad = $repository->find($choice['value']);

                            // create if expire or not exist
                            if (
                                $ad->getStatValue() == "valider" &&
                                !$this->get("app.tracking.export.helpers")->checkPhoneTracking($ad) &&
                                $this->get($serviceId)->getAvailableNumber($ad, $trackingAdvantage)
                            ) {
                                $trackingOK++;
                            }
                        }
                    }
                break;
            }
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * @Route("/moderation", options={"expose"=true}, name="ads_moderation")
     * @Template("AppBundle:Ads:moderation_ads.html.twig")
     */
    public function moderationAction(Request $request)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
            return $this->redirectToRoute('ads_index');

        /** @var \AppBundle\Entity\User $user */
        $group = $this->getCurrentGroup($request);

        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $user = $this->getUser();
        $data = $request->query->has('gac_search') ? $request->query->get('gac_search') : [];
        $form = $this->createForm($this->get('app.form.gac_search'),$data, ['user' => $user]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            if(!empty($data['bigGroups']))
                $data['bigGroups'] = $data['bigGroups']->getId();
            if(!empty($data['commercial']))
                $data['commercial'] = $data['commercial']->getId();
            if(!empty($data['group'])) {
                $group = $data['group'];
                $data['group'] = $data['group']->getId();
            }
        }

        $datatable = $this->get('app.datatable.ad_moderation');
        $datatable->buildDatatable([ 'group' => $group, 'data' => $data ]);

        return [
            'datatable' => $datatable,
            'form'      => $form->createView()
        ];
    }

    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="ads_switch_published_statut")
     * @Method({"GET","POST"})
     */
    public function switchPublishedStatusAction(Request $request, Ad $ad)
    {
        if($request->isXmlHttpRequest()) {
            $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if (!$this->getUser()->isEmployeeOf($entity->getGroup()))
                throw new AccessDeniedHttpException();
            $oldPublished = $ad->getPublished();
            $em = $this->getDoctrine()->getManager();
            switch ($ad->getStatvalue()) {
                case Ad::PENDING:
                    $stat = Ad::VALIDATE;
                    break;
                case Ad::VALIDATE:
                    $stat = Ad::ANNULER;
                    break;
                case Ad::ANNULER:
                    $stat = Ad::PENDING;
                    break;
            }
            $ad->setStat($stat);
            $ad->setPublished($stat == Ad::VALIDATE ? true : false);
            $em->persist($ad);
            $em->flush();
            if(!$oldPublished && $stat == Ad::VALIDATE ){
                $this->get("app.mailer.manager")->sendAdModerateOk(
                    [
                        "chef"      => $this->getUser(),
                        'ad'     => $ad
                    ]);
            }
            return new JsonResponse(['_callback' => $ad->getStatvalue()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     *
     * @Route("/etat/{remoteId}/reserver", options={"expose"=true}, name="ads_stat_reserver")
     * @Method({"GET"})
     */
    public function statReserverAction(Request $request, Ad $ad)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($ad->getProgram()->getGroup()))
            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $ad->setStat('reserver');
        $em->persist($ad);
        $em->flush();
        $this->addFlash("success", "Le status de l'annonce a été modifier");
        return $this->redirectToRoute('program_view',['remoteId' => $ad->getProgram()->getRemoteId()]);
    }


    /**
     *
     * @Route("/etat/{remoteId}/vendu", options={"expose"=true}, name="ads_stat_vendu")
     * @Method({"GET"})
     */
    public function statVenduAction(Request $request, Ad $ad)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($ad->getProgram()->getGroup()))
            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $ad->setStat('vendu');
        $em->persist($ad);
        $em->flush();
        $this->addFlash("success", "Le status de l'annonce a été modifier");
        return $this->redirectToRoute('program_view',['remoteId' => $ad->getProgram()->getRemoteId()]);
    }

    /**
     * Launch the process to create a house_model
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/duplicate/{remoteId}",options={"expose"=true}, name="ads_duplicate")
     * @Method({"GET","POST"})
     */
    public function duplicateAction(Request $request, Ad $ad)
    {
        $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getUser()->isEmployeeOf($entity->getGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $adClone = clone $ad;
        $em->persist($adClone);
        $em->flush();
        $this->addFlash("success", "l'annonce a été dupliquer");
        return new RedirectResponse($this->generateUrl('ads_edit',['remoteId' => $adClone->getRemoteId()]));
    }

    /**
     * @Route("/informer/{remoteId}", options={"expose"=true}, name="ads_informer")
     * @Method({"GET","POST"})
     */
    public function informerAction(Request $request, Ad $ad)
    {
        $entity = $ad->getProgram() ? $ad->getProgram() : $ad;

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getUser()->isEmployeeOf($entity->getGroup()))
            throw new AccessDeniedHttpException();

        $msg = $request->query->get('value');

        $commercial      = $ad->getOwner();
        $this->get("app.mailer.manager")->sendCommentAd(
            [
                "chef"      => $this->getUser(),
                'ad'     => $ad,
                'msg'     => $msg
            ]);

        $this->addFlash("success", $commercial->getName(). ' à été informer');
        return new JsonResponse(['success' => true]);
    }

    /**
     * @todo : remove it if not implement for the 3rd action
     * @Route("/persist/", options={"expose"=true}, name="ads_persist_draft")
     * @Method({"POST"})
     */
    public function persisteAction(Request $request)
    {
        $session = $request->getSession();
        /** @var flow_ad_data $flow */
        $flow = $session->get('flow_ad_data');

        if($request->request->has('bienImmobilier') && $flow[2] && $flow[2]["bienImmobilier"]) {
            $bienImmo = $request->request->get("bienImmobilier");
            foreach($bienImmo as $index => $value) {
                if(isset($flow[2]["bienImmobilier"][$index])) {
                    $flow[2]["bienImmobilier"][$index] = $value;
                }
            }
        }
        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/reaffect-housemodel/{remoteId}", options={"expose"=true}, name="ads_reaffect_housemodel")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:reaffect_housemodel.html.twig")
     */
    public function reaffectHouseModelAction(Request $request, Ad $ad)
    {
        $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($entity->getGroup()))
                throw new AccessDeniedHttpException();

        if($ad->getStatValue() != Ad::SUSPENDU)
            throw new AccessDeniedHttpException();

        $user         = $this->getUser();
        $houseModels = $this->getDoctrine()->getRepository('AppBundle:HouseModel')->findHouseModels($ad->getGroup(),$user);
        return [
            'houseModels' => $houseModels,
            'ad'          => $ad
        ];
    }

    /**
     * @Route("/reaffect/{remoteId}/{housemodel_remoteId}", options={"expose"=true}, name="ads_reaffect")
     * @ParamConverter("ad", options={"mapping": {"remoteId": "remoteId"}})
     * @ParamConverter("houseModel", options={"mapping": {"housemodel_remoteId": "remoteId"}})
     * @Method({"GET","POST"})
     */
    public function reaffectAction(Request $request, Ad $ad, HouseModel $houseModel)
    {
        $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($entity->getGroup()))
                throw new AccessDeniedHttpException();

        if($ad->getStatValue() != Ad::SUSPENDU)
            throw new AccessDeniedHttpException();

        $maison = clone $houseModel->getMaison();
        $ad->addBienImmobilier($maison);
        $ad->setDescription($houseModel->getMaison()->getDescription());
        $ad->setImagePrincipale($houseModel->getImagePrincipale());
        $ad->setTitreImagePrincipale($houseModel->getTitreImagePrincipale());
        $ad->setPublished(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($ad);
        $em->flush();

        return $this->redirectToRoute('ads_edit',['remoteId' => $ad->getRemoteId()]);
    }

    /**
     * @Route("/affect/{remoteId}", options={"expose"=true}, name="ads_affect_commercial")
     */
    public function reaffectCommercialAction(Request $request, Ad $ad)
    {
        $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($entity->getGroup()))
                throw new AccessDeniedHttpException();

        $remoteId = $request->request->get('commercial');
        if(!$remoteId)
            $this->redirect($request->headers->get('referer'));

        $commercial = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['remoteId' => $remoteId]);
        $ad->setOwner($commercial);
        $em = $this->getDoctrine()->getManager();
        $em->persist($ad);
        $em->flush();
        return $this->redirect($request->headers->get('referer'));
    }

    private function resolveImages(Ad $ad) {

        if($ad->getImagePrincipale()) {
            $command = new ResolveCacheCommand();
            $command->setContainer($this->container);
            $input = new ArrayInput([
                'paths' => [ $ad->getImagePrincipale() ],
                '--filters' => ['cover_paysage']
            ]);
            $output = new NullOutput();
            $command->run($input, $output);
        }

        if($ad->getGroup()->getAgency()->getAvatar()) {
            $command = new ResolveCacheCommand();
            $command->setContainer($this->container);
            $input = new ArrayInput([
                'paths' => [ $ad->getGroup()->getAgency()->getAvatar() ],
                '--filters' => ['cover_logo']
            ]);
            $output = new NullOutput();
            $command->run($input, $output);
        }
    }
}