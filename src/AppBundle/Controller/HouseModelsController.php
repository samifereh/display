<?php
namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\Vente;
use AppBundle\Entity\User;
use AppBundle\Form\Type\MaisonType;
use AppBundle\Form\Type\PolirisAdType;
use AppBundle\Form\Type\TerrainType;
use AppBundle\Form\Type\VenteType;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;

use AppBundle\Entity\HouseModel;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class HouseModelsController
 * @package AppBundle\Controller
 * @Route("/modeles")
 */
class HouseModelsController extends BaseController
{
    public $DISPLAYED_ITEMS = ['typebien', 'nbsdb', 'nbsdeau', 'nbwc', 'surf_sejour', 'typecuisine', 'surf_maxbureau'];

    /**
     * Return the view of the available actions
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/", name="housemodels_list")
     * @Method({"GET"})
     * @Template("AppBundle:HouseModels:list.html.twig")
     */
    public function listAction(Request $request){

        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $user         = $this->getUser();
        $form         = null;
        $data = $request->query->has('model_search') ? $request->query->get('model_search') : [];
        if($this->isGranted('ROLE_BRANDLEADER')) {
            $form = $this->createForm($this->get('app.form.model_search'),$data, ['user' => $user]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $data = $form->getData();
                if(!empty($data['bigGroups']))
                    $data['bigGroups'] = $data['bigGroups']->getId();
                if(!empty($data['commercial']))
                    $data['commercial'] = $data['commercial']->getId();
                if(!empty($data['group'])) {
                    $data['group'] = $data['group']->getId();
                    $group = $data['group'];
                }
            }
        }

        $datatable = $this->get('app.datatable.housemodel');
        $datatable->buildDatatable([ 'group' => $group, 'data' => $data ]);

        return [
            'datatable' => $datatable,
            'form'      => $form ? $form->createView() : null,
        ];

    }

    /**
     * Return the view of the available actions
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/moderation", name="housemodels_moderation_list")
     * @Method({"GET"})
     * @Template("AppBundle:HouseModels:list.html.twig")
     */
    public function listModerationAction(Request $request){

        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasAnnoncesOption');
        if(!is_null($authorized))
            return $authorized;

        $user = $this->getUser();
        $data = $request->query->has('model_search') ? $request->query->get('model_search') : [];
        $form = null;
        if($this->isGranted('ROLE_BRANDLEADER')) {
            $form = $this->createForm($this->get('app.form.model_search'),$data, ['user' => $user]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $data = $form->getData();
                if(!empty($data['bigGroups']))
                    $data['bigGroups'] = $data['bigGroups']->getId();
                if(!empty($data['commercial']))
                    $data['commercial'] = $data['commercial']->getId();
                if(!empty($data['group'])) {
                    $group = $data['group'];
                    $data['group'] = $data['group']->getId();
                }
            }
        }
        $datatable = $this->get('app.datatable.housemodel_moderation');
        $datatable->buildDatatable([ 'group' => $group, 'data' => $data ]);

        return [
            'datatable' => $datatable,
            'form'      => $form ? $form->createView() : null
        ];

    }

    /**
     * Display a house_model
     * @param HouseModel $houseModel
     * @return Response
     *
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="housemodel_show")
     * @Method({"GET"})
     * @Template("AppBundle:HouseModels:show.html.twig")
     */
    public function showAction(HouseModel $houseModel){
        return ['houseModel' => $houseModel];
    }

    /**
     * Launch the process to create a house_model
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/ajouter", options={"expose"=true}, name="housemodel_add")
     * @Method({"GET","POST"})
     * @Template("AppBundle:HouseModels:add.html.twig")
     */
    public function addAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $session    = $request->getSession();

        $group = $this->getCurrentGroup($request);
        $commercial = $this->getUserService()->checkCommercialUser($request,$group);

//        if(!$commercial) {
//            return new RedirectResponse($this->generateUrl('switch_commercial',['redirect'=>$this->generateUrl('housemodel_add')]));
//        }
        if(!$session->get('brouillon_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_id',$brouillon->getId());
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_id'));
            if(!$brouillon) {
                $brouillon = new Brouillon();
                $brouillon->setSessionId($session->getId());
                $em->persist($brouillon);
                $em->flush();
                $session->set('brouillon_id',$brouillon->getId());
            }
        }

        $houseModel = new HouseModel();
        $oldImage    = null;
        $flow = $this->get('app.form.flow.model');
        $flow->bind($houseModel);
        $form = $submittedForm = $flow->createForm(['checkImageEmpty' => false]);
        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                if(
                    !$houseModel->getImagePrincipale() &&
                    (!$brouillon || !$brouillon->getImagePrincipale())
                ){
                    $error = new FormError("l'image principale est obligatoire.");
                    $form->get('imagePrincipale')->addError($error);
                    return [
                        'form'              => $form->createView(),
                        'flow'              => $flow,
                        'houseModel'        => $houseModel,
                        'brouillon'         => $brouillon,
                        'mode'              =>  'add'
                    ];
                }
                if($brouillon && !$brouillon->getImagePrincipale()) {
                    $brouillon->setImagePrincipale($houseModel->getImagePrincipale());
                    $uploadableManager->markEntityToUpload($brouillon, $brouillon->getImagePrincipale());
                    $em->persist($brouillon);
                    $em->flush();

                }
            }
            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                // flow finished
                if($brouillon) {
                $houseModel->setImagePrincipale($brouillon->getImagePrincipale());
                    $brouillon->setImagePrincipale(null);

                    /** @var \AppBundle\Entity\Document $document */
                    foreach ($brouillon->getDocuments() as $document)
                        $houseModel->addDocument($document);
                    $brouillon->setDocuments(null);
                }
                $sendMail = false;
                $user = $this->getUser();
                if($user->hasRole('ROLE_SALESMAN') && $user->getHouseModelsModeration()) {
                    $houseModel->setStat(HouseModel::PENDING);
                    $houseModel->setPublished(false);
                    $sendMail=true;
                } else {
                    $houseModel->setStat(HouseModel::VALIDATE);
                    $houseModel->setPublished(true);
                }
                $houseModel->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                $em->persist($houseModel);
                $em->flush();

                //Send Email
                if($sendMail) {
                    $userRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
                    $chef = $userRepository->findByRole('ROLE_BRANDLEADER', $group);

                    $this->get("app.mailer.manager")->sendRequestModelAdd(
                        [
                            "chef"      => $chef,
                            'user'     => $user,
                            'model'     => $houseModel
                        ]);
                }
                //En deux étapes pour évité la supp du brouillon en cas d'erreur

                if($brouillon) {
                    $em->persist($brouillon);
                    $em->flush();
                }
                $session->remove('brouillon_id');
                $this->addFlash("success", "Le modèle a été ajouter");
                $flow->reset();
                return new RedirectResponse($this->generateUrl('housemodels_list'));
            }
        }
        return [
            'form'              => $form->createView(),
            'flow'              => $flow,
            'houseModel'        => $houseModel,
            'brouillon'         => $brouillon,
            'mode'              =>  'add'
        ];
    }

    /**
     * Launch the process to create a house_model
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/duplicate/{remoteId}",options={"expose"=true}, name="housemodel_duplicate")
     * @Method({"GET","POST"})
     */
    public function duplicateAction(Request $request, HouseModel $houseModel)
    {
        $em = $this->getDoctrine()->getManager();
        $modelClone = clone $houseModel;
        $em->persist($modelClone->getMaison());
        $em->persist($modelClone);
        $em->flush();
        return new RedirectResponse($this->generateUrl('housemodel_edit',['remoteId' => $modelClone->getRemoteId()]));
    }

    /**
     * Edit an existing house_model
     * @param Request $request
     * @param HouseModel $model
     * @return RedirectResponse|Response
     *
     * @Route("/editer/{remoteId}",options={"expose"=true}, name="housemodel_edit")
     * @Method({"GET","POST"})
     * @Template("AppBundle:HouseModels:edit.html.twig")
     */
    public function editAction(Request $request, HouseModel $houseModel)
    {
        $em        = $this->getDoctrine()->getManager();
        $session   = $request->getSession();
        $brouillon = null;
        $user       = $this->getUser();
        $group = $this->getCurrentGroup($request);
        $commercial = $this->getUserService()->checkCommercialUser($request,$group);
        if(!$commercial)
            $commercial = $houseModel->getOwner();

        $oldImage = $houseModel->getImagePrincipale();

        $flow     = $this->get('app.form.flow.model');

        $oldStatus = $houseModel->getPublished();
        if($session->get('edit_brouillon_id')) {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('edit_brouillon_id'));
            $oldImage = $houseModel->getImagePrincipale();
            if(!$brouillon) {
                $brouillon = new Brouillon();
                $brouillon->setSessionId($session->getId());
                $em->persist($brouillon);
                $em->flush();
                $session->set('edit_brouillon_id',$brouillon->getId());
            }
        }
        if(!$brouillon || !$session->get('edit_brouillon_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('edit_brouillon_id', $brouillon->getId());
            $flow->reset();
        }

        $flow->bind($houseModel);
        $form = $submittedForm = $flow->createForm(['checkImageEmpty' => false]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1 && $submittedForm->get('imagePrincipale')->getData()) {
                if(!is_string($submittedForm->get('imagePrincipale')->getData())) {
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $brouillon->setImagePrincipale($houseModel->getImagePrincipale());
                    $uploadableManager->markEntityToUpload($brouillon, $brouillon->getImagePrincipale());
                    $em->persist($brouillon);
                    $em->flush();
                }
            } else {
                $houseModel->setImagePrincipale($oldImage);
            }

            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                // flow finished
                try {
                    if($brouillon->getImagePrincipale()) {
                        $houseModel->setImagePrincipale($brouillon->getImagePrincipale());
                        $brouillon->setImagePrincipale(null);
                    } else {
                        $houseModel->setImagePrincipale($oldImage);
                    }
                    $em->persist($houseModel->getMaison());
                    if($brouillon->getDocuments() ) {
                        /** @var \AppBundle\Entity\Document $document */
                        foreach ($brouillon->getDocuments() as $document)
                            $houseModel->addDocument($document);
                        $brouillon->setDocuments(null);
                    }
                    $user = $this->getUser();
                    if($user->hasRole('ROLE_SALESMAN') && $user->getHouseModelsModeration()) {
                        $houseModel->setStat($houseModel::PENDING);
                        $houseModel->setPublished(false);
                    } else {
                        $houseModel->setStat($houseModel::VALIDATE);
                        $houseModel->setPublished(true);
                        if(!$oldStatus){
                            $this->get("app.mailer.manager")->sendModelModerateOk(
                                [
                                    'user'     => $user,
                                    'model'     => $houseModel
                                ]);
                        }
                    }
                    $houseModel->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                    $houseModel->setPercent($houseModel->calculatePercent());
                    $em->persist($houseModel);

                    //En deux étapes pour évité la supp du brouillon en cas d'erreur
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('edit_brouillon_id');
                    $this->addFlash("success", "Le modèle a été modifié");
                    $flow->reset();

                    $this->resolveImages($houseModel);

                    return $this->redirectToRoute('housemodels_list');
                } catch (\Exception $e) {
                    $this->addFlash("error", "Erreur lors de la modification du modèle :".$e->getMessage());
                }
            }
        }
        return [
            'form'              => $form->createView(),
            'flow'              => $flow,
            'houseModel'        => $houseModel,
            'brouillon'         => $brouillon,
            'oldImage'          => $oldImage,
            'mode'              => 'edit'
        ];
    }

    /**
     * @param HouseModel $houseModel
     * @return RedirectResponse
     *
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="housemodel_delete")
     * @Method({"GET"})
     */
    public function deleteAction(HouseModel $houseModel){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$this->getUser()->isEmployeeOf($houseModel->getGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();

        /** @var Ad $ad */
        foreach ($houseModel->getAd() as $ad) {
            $ad->setStat(Ad::SUSPENDU);
            $em->persist($ad);
        }
        $em->remove($houseModel);
        $em->flush();
        $this->addFlash('success', $this->get('translator')->trans("model.house_model_deleted", [], 'commiti'));

        return $this->redirectToRoute('housemodels_list');

    }
    /**
     *
     * @Route("/actions/{action}", options={"expose"=true}, name="housemodel_actions")
     * @Method({"GET","POST","DELETE"})
     */
    public function multipleActionAction(Request $request,$action)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $choices = $request->request->get('data');
            $token = $request->request->get('token');

            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AppBundle:HouseModel');

            $em = $this->getDoctrine()->getManager();
            switch($action) {
                case 'delete' :
                    foreach ($choices as $choice) {
                        /** @var HouseModel $entity */
                        $entity = $repository->find($choice['value']);
                        $em->remove($entity);
                    }
                    $em->flush();
                    break;
                case 'visible' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setVisibleByAll(true);
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
                case 'invisible' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setVisibleByAll(false);
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
                case 'activer' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(true)->setStat(HouseModel::VALIDATE);
                        $em->persist($entity);
                    }
                    $em->flush();
                    //Send Mail
                    $user = $this->getUser();
                    $this->get("app.mailer.manager")->sendModelModerateOk(
                        [
                            'user'     => $user,
                            'model'     => $entity
                        ]);
                    break;
                case 'desactiver' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(false)->setStat(HouseModel::ANNULER);
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
            }
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }

    /**
     * @Route("/status/{remoteId}", options={"expose"=true}, name="housemodel_switch_statut")
     * @Method({"GET"})
     */
    public function switchStatusAction(Request $request, HouseModel $houseModel)
    {
        if($request->isXmlHttpRequest()) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($houseModel->getGroup()))
                throw new AccessDeniedHttpException();

            $em = $this->getDoctrine()->getManager();
            switch($houseModel->getStatvalue()) {
                case HouseModel::PENDING: $stat = HouseModel::VALIDATE; break;
                case HouseModel::VALIDATE: $stat = HouseModel::ANNULER; break;
                case HouseModel::ANNULER: $stat = HouseModel::PENDING; break;
            }
            $houseModel->setStat($stat);
            $houseModel->setPublished($stat == HouseModel::VALIDATE ? true : false);
            $em->persist($houseModel);
            $em->flush();
            return new JsonResponse(['_callback' => $houseModel->getStatvalue()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/visibility/{remoteId}", options={"expose"=true}, name="housemodel_switch_visibility")
     * @Method({"GET"})
     */
    public function switchVisibilityAction(Request $request, HouseModel $houseModel)
    {
        if($request->isXmlHttpRequest()) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($houseModel->getGroup()))
                throw new AccessDeniedHttpException();

            $em         = $this->getDoctrine()->getManager();
            $houseModel->setVisibleByAll($houseModel->getVisibleByAll() ? false : true);
            $em->persist($houseModel);
            $em->flush();
            return new JsonResponse(['_callback' => $houseModel->getVisibleByAll()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/informer/{remoteId}", options={"expose"=true}, name="housemodel_informer")
     * @Method({"GET","POST"})
     */
    public function informerAction(Request $request, HouseModel $houseModel)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') ||
            !$this->getUser()->isEmployeeOf($houseModel->getGroup()))
            throw new AccessDeniedHttpException();

        $msg = $request->query->get('value');
        $chef = $this->getUser();
        $this->get("app.mailer.manager")->sendModelModerateReply(
            [
                'chef'       => $chef,
                'model'       =>$houseModel,
                'msg'       =>$msg,
            ]);

        $this->addFlash("success", $houseModel->getOwner()->getName(). ' à été informer');
        return $this->redirectToRoute('housemodels_moderation_list');
    }


    /**
     *
     * @Route("/pdf/{remoteId}", options={"expose"=true}, name="housemodel_print_pdf")
     * @Method({"GET"})
     */
    public function pdfAction(Request $request, HouseModel $houseModel)
    {
        $group = $houseModel->getGroup();
        $filename = (trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $houseModel->getName())));
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($group))
                throw new AccessDeniedHttpException();

        $html = $this->renderView('AppBundle:HouseModels:print.html.twig',['houseModel' => $houseModel]);

        return new Response(
            $this->getPdfExporter()->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $filename.'.pdf'),
            ]
        );
    }

    /**
     *
     * @Route("/print/{remoteId}", options={"expose"=true}, name="housemodel_print")
     * @Method({"GET"})
     */
    public function printAction(Request $request, HouseModel $houseModel)
    {
        $group = $houseModel->getGroup();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($group))
                throw new AccessDeniedHttpException();

        return $this->render('AppBundle:HouseModels:print.html.twig',['houseModel' => $houseModel, 'print' => true]);
    }

    private function resolveImages(HouseModel $houseModel) {

        if($houseModel->getImagePrincipale()) {
            $command = new ResolveCacheCommand();
            $command->setContainer($this->container);
            $input = new ArrayInput([
                'paths' => [ $houseModel->getImagePrincipale() ],
                '--filters' => ['cover_paysage']
            ]);
            $output = new NullOutput();
            $command->run($input, $output);
        }

        if($houseModel->getGroup()->getAgency()->getAvatar()) {
            $command = new ResolveCacheCommand();
            $command->setContainer($this->container);
            $input = new ArrayInput([
                'paths' => [ $houseModel->getGroup()->getAgency()->getAvatar() ],
                '--filters' => ['cover_logo']
            ]);
            $output = new NullOutput();
            $command->run($input, $output);
        }
    }

    /**
     * @Route("/affect/{remoteId}", options={"expose"=true}, name="housemodel_affect_commercial")
     */
    public function reaffectCommercialAction(Request $request, HouseModel $houseModel)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getUser()->isEmployeeOf($houseModel->getGroup()))
                throw new AccessDeniedHttpException();

        $remoteId = $request->request->get('commercial');
        if(!$remoteId)
            $this->redirect($request->headers->get('referer'));

        $commercial = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['remoteId' => $remoteId]);
        $houseModel->setOwner($commercial);
        $em = $this->getDoctrine()->getManager();
        $em->persist($houseModel);
        $em->flush();
        return $this->redirect($request->headers->get('referer'));
    }

}