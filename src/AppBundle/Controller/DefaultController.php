<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dashboard;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\DashboardType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends BaseController
{
    /**
     * Secure the access to the website
     * @param Request $request
     * @return RedirectResponse|Response
     * 
     * @Route("/", name="homepage" , options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Template("AppBundle:Default:index.html.twig")
     */
    public function homeAction(Request $request)
    {

        $securityContext = $this->container->get('security.authorization_checker');

        if (!$securityContext->isGranted('IS_AUTHENTICATED_FULLY'))
            return new RedirectResponse($this->generateUrl('fos_user_security_login'));

        if(!$this->getUser()->hasFullAccess())
            return $this->redirectToRoute("ads_index");

        $dashboardStatistiques = $this->getStatistiquesService()->getDashboardStatistique($request);
        $dashboardStatistiques["filters"]  = $this->getFilters($request);


        return $dashboardStatistiques;

    }

    /**
     * Secure the access to the website
     * @param Request $request
     * @return RedirectResponse|Response
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @Route("/editer", name="homepage_edit" , options={"expose"=true})
     * @Method({"POST","GET"})
     * @Template("AppBundle:Dashboard:edit.html.twig")
     */
    public function editAction(Request $request)
    {
        if(!$this->getUser()->hasFullAccess())
            return $this->redirectToRoute("ads_index");

        $em = $this->getDoctrine()->getManager();

        //Latest Actuality
        /** @var \AppBundle\Repository\DashboardRepository $repo */
        $repo = $em->getRepository('AppBundle:Dashboard');
        $dashboard = $repo->findOneBy([]);
        if(is_null($dashboard))
            $dashboard = new Dashboard();

        $form = $this->createForm(new DashboardType(),$dashboard);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($dashboard);
                $em->flush();
                return new RedirectResponse($this->generateUrl('homepage'));
            }
        }

        return [
            'form'  => $form->createView()
        ];

    }
    /**
     * Secure the access to the website
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/lang/{_locale}", requirements={"_locale" = "fr|en"}, name="switch_locale" )
     * @Method({"GET"})
     * @Template("AppBundle:Default:index.html.twig")
     */
    public function languagesAction(Request $request,$_locale = 'fr')
    {
        $request->getSession()->set('_locale', $_locale);
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Secure the access to the website
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function groupMenuAction(Request $request)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            return $this->render('AppBundle:Common:group_menu.html.twig');

        $vars = $this->getUsefulHelper()->getFilters($request);

        $response = $this->render('AppBundle:Common:group_menu_admin.html.twig',$vars);
        return $response;
    }

    /**
     * Secure the access to the website
     * @param Request $request
     * @return mixed $vars|Response
     */
    function getFilters(Request $request){


        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')){
            $userService =  $this->getUserService();
            if($this->getAuthorizationChecker()->isGranted('ROLE_GROUP')){
                $groupsList = $this->getUser()->getGroups();
                $agenciesList = [];
                $commercials = [];
                $agency = $userService->checkStatUserAgency($request);
                $group = $userService->checkStatUserGroup($request);
                if($agency){
                    $commercials = $this->getDoctrine()->getRepository("AppBundle:User")->getCommercialOnly($agency);
                }

                if($group){
                    $agenciesList = $group->getAgencyGroups();
                }else {
                    /** @var Group $groupItem */
                    foreach ($groupsList as $groupItem) {
                        $agenciesList = array_merge($agenciesList, $groupItem->getAgencyGroups()->toArray());
                    }
                }
                if(empty($commercials))
                    $commercials = $this->getDoctrine()->getRepository("AppBundle:User")->getCommercialOnly($agenciesList);


                return [
                    'commercials' => $commercials,
                    'agenciesList'=> $agenciesList,
                    'groupsList'  => $groupsList
                ];
            }


            return [];
        }


        $vars = $this->getUsefulHelper()->getStatFilters($request);

        return $vars;

    }
}
