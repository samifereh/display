<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\CommercialPortalLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Form\Type\AgencyType;
use AppBundle\Form\Type\UserType;
use AppBundle\Service\AgendaManager;
use AppBundle\Service\EmailsManager;
use AppBundle\Service\NotificationManager;
use AppBundle\Service\Passwords;
use Payum\Core\Payum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Group as Group;
use AppBundle\Entity\User as User;

use AppBundle\Repository\UserRepository as UserRepository;

use FOS\UserBundle\Form\Type\RegistrationFormType as RegistrationFormType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class AgenciesController
 * @package AppBundle\Controller
 */
class BaseController extends Controller{

    /**
     * @return \AppBundle\Service\userService
     */
    public function getUserService() {
        return $this->get('app.user.service');
    }
    /**
     * @return \AppBundle\Service\userService
     */
    public function getBroadcastRegistrationFormType() {
        return $this->get('app.form.broadcast_registration');
    }
    /**
     * @return \AppBundle\Form\Type\Broadcast\GroupPortalRegistrationsType
     */
    public function getGroupPortalRegistrationsFormType() {
        return $this->get('app.form.group_broadcast');
    }

    /**
     * @return \AppBundle\Form\Type\Broadcast\CommercialPortalLimitType
     */
    public function getCommercialPortalsLimitFormType() {
        return $this->get('app.form.commercial_portals_limit');
    }

    /**
     * @return \AppBundle\Form\Type\Broadcast\BroadcastPortalRegistrationType
     */
    public function getBroadcastPortalRegistrationFormType() {
        return $this->get('app.form.broadcast_registration');
    }

    /** @return AuthorizationChecker */
    public function getAuthorizationChecker() {
        return $this->get('security.authorization_checker');
    }

    /**
     * @return \AppBundle\Service\ExportHelpers
     */
    public function getExportHelper() {
        return $this->get('app.helpers');
    }

    /**
     * @return \AppBundle\Service\Helpers
     */
    public function getUsefulHelper() {
        return $this->get('app.useful_helpers');
    }

    public function isBlockedGroup() {
        $user        = $this->getUser();
        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {return false;}
        /** @var \AppBundle\Entity\Group $group */
        $group       = $user->getAgencyGroup();
        return $group->getBlocked();
    }

    /**
     * @return Passwords
     */
    protected function getPasswordGenerator()
    {
        return $this->get('app.services.passwords');
    }

    /**
     * @return Payum
     */
    protected function getPayum()
    {
        return $this->get('payum');
    }

    /**
     * @return \Ovh\Api
     */
    protected function getOvh()
    {
        return $this->get('ovh');
    }

    /**
     * @return \Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator
     */
    protected function getPdfExporter()
    {
        return $this->get('knp_snappy.pdf');
    }

    /**
     * @return \AppBundle\Service\Mailjet
     */
    protected function getMailjet()
    {
        return $this->get('app.mailjet');
    }

    /**
     * @return \AppBundle\Service\Dexem
     */
    protected function getDexem()
    {
        return $this->get('app.api.dexem');
    }

    /**
     * @return \Mailjet\Api\Client
     */
    protected function getMailjetApi1()
    {
        return $this->get('knp_mailjet.api');
    }

    /**
     * @return EmailsManager
     */
    protected function getEmailsManager()
    {
        return $this->get('app.service.email');
    }

    /**
     * @return NotificationManager
     */
    protected function getNotificationManager()
    {
        return $this->get('app.service.notifications');
    }

    /**
     * @return AgendaManager
     */
    protected function getAgendaManager()
    {
        return $this->get('app.services.agenda');
    }

    /**
     * @return \AppBundle\Service\StatistiquesService
     */
    public function getStatistiquesService() {
        return $this->get('app.statistique.service');
    }

    protected function getCurrentGroup($request)
    {
        $user = $this->getUser();
        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || (
                $this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP')
            )
        ) {
            $group = $this->getUserService()->checkUserAgency($request,true);
        } else {
            $group = $user->getAgencyGroup();
        }
        return $group;
    }

    protected function groupIsAutorised($group,$method) {
        if($group instanceof Group && !$group->$method()) {
            if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP')) {
                $this->addFlash("error", "Autorisé non accorder a cette agence");
                return $this->redirectToRoute('homepage');
            } else {
                throw new AccessDeniedHttpException();
            }
        }
        return null;
    }

    /**
     * update commercial distribution
     * @param $request
     * @param \AppBundle\Entity\Group $group
     * @return array
     */
    function updateCommercialDistribution($request, $group){

        $em = $this->getDoctrine()->getManager();
        $commercialLimitRepo = $em->getRepository("AppBundle:Broadcast\\CommercialPortalLimit");
        /** @var \AppBundle\Repository\AdRepository $adRepository */
        $adRepository = $em->getRepository("AppBundle:Ad");
        $userRepo = $em->getRepository("AppBundle:User");
        $commercialLimit = $request->request->get("commercial_limit");
        $groupPortals = $group->getBroadcastPortalRegistrations();

        /** @var PortalRegistration $portal */
        foreach ($groupPortals as $portal){
            $limit = $portal->getAdLimit();
            // loop commercial portal limit and check sum
            if(
                $limit &&
                $portal->getBroadcastPortal() &&
                isset($commercialLimit[$portal->getBroadcastPortal()->getId()])
            ){
                $sum = 0;
                foreach ($commercialLimit[$portal->getBroadcastPortal()->getId()] as $commercialId => $commercialPortalContent){
                    $commercialPortalCache = $commercialLimitRepo->find($commercialPortalContent['id']);
                    $commercial            = $userRepo->find($commercialId);
                    if(!$commercialPortalCache){
                        $commercialPortalCache = new CommercialPortalLimit();
                        $commercialPortalCache->setPortal($portal->getBroadcastPortal());
                        $commercialPortalCache->setCommercial($commercial);
                    }
                    $ads = $adRepository->getOlderAdsByCommercial($commercial,$portal->getBroadcastPortal());
                    $nbrToDelete = count($ads) - $commercialPortalContent['limit'];
                    if($nbrToDelete > 0) {
                        $i = 0;
                        while($nbrToDelete > 0) {
                            /** @var Diffusion $diffusion */
                            $diffusion = $ads[$i]->getDiffusion();
                            foreach ($diffusion->getBroadcastPortals() as $diffusionBroadcastPortal) {
                                if($diffusionBroadcastPortal->getId() == $portal->getBroadcastPortal()->getId()) {
                                    $diffusion->removeBroadcastPortal($diffusionBroadcastPortal);
                                    break;
                                }
                            }
                            $i++;
                            $nbrToDelete--;
                        }
                        $em->flush();
                    }

                    $commercialPortalCache->setLimit($commercialPortalContent['limit']);
                    $sum += $commercialPortalContent['limit'];
                    $em->persist($commercialPortalCache);
                }
                if($sum > $limit){
                    return [
                        "success" => false,
                        'error' => "please, check limit sum"
                    ];
                }
            }
        }
        $em->flush();
        return[
            "success" => true,
            "message" => "update_ok"
        ];
    }
}