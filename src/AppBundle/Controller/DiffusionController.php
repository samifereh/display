<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use AppBundle\Form\Type\Broadcast\BroadcastPortalRegistrationType;
use AppBundle\Form\Type\Broadcast\PortalRegistrationSearchType;
use AppBundle\Form\Type\Broadcast\PortalRegistrationType;
use AppBundle\Form\Type\Diffusion\ContactPortalAdminType;
use AppBundle\Form\Type\Diffusion\DiffusionSearchType;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DiffusionController extends BaseController
{

    /**
     * @Route("/passerelles/catalogue", name="portails_catalogue_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Diffusion:catalogue.html.twig")
     **/
    public function catalogueAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Repository\Broadcast\PortalRepository $repo */
        $repo = $em->getRepository('AppBundle:Broadcast\Portal');

        $data = [];
        $user = $this->getUser();
        $group = $user->getGroup() && $user->getGroup()->hasRole('ROLE_AGENCY') ? $user->getGroup() : null;
        $dataForm = $request->query->has('diffusion_search') ? $request->query->get('diffusion_search') : [];
        $form = $this->createForm($this->get('app.form.diffusion_search'),$dataForm);
        if($dataForm ) {

            $items = $repo->getAvailablePortal(
                isset($dataForm["bigGroups"]) && $dataForm["bigGroups"] ? $dataForm["bigGroups"] : $group,
                isset($dataForm["group"]) && $dataForm["group"] ?
                    $dataForm["group"] :
                    ($group && $group->getAgencyGroups()->count() ? $group->getAgencyGroups() : null)
            );
        }elseif($user->getAgencies()){
            /** @var User $user */
            $items = $repo->getAvailablePortal(null, $user->getAgencies());
        }else{

            $items = $repo->findAll();
        }


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $group = $this->getCurrentGroup($request);
            $datatable = $this->get('app.datatable.diffusion_portals');
            if(isset($data['bigGroups']) && $data['bigGroups']) { $data['bigGroups'] = $data['bigGroups']->getId(); }
            if(isset($data['group']) && $data['group']) { $group = $data['group'];$data['group'] = $data['group']->getId(); }
            $datatable->buildDatatable(['data' => $data, 'group' => $group]);
            //todo: a revoir
            return $this->render('AppBundle:Diffusion:catalogue.html.twig',[
                'datatable' => $datatable,
                'form'       => $form->createView(),
                'items'      => $items,
                'currentGroup' => $group
            ]);
        }

        return [
            'items'      => $items,
            'form'       => $form->createView(),
            'currentGroup' => $group
        ];
    }

    /**
     * @Route("/passerelles/actifs", name="portails_actifs_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Diffusion:actifs.html.twig")
     **/
    public function portailsActifsAction(Request $request){

        /** @var \AppBundle\Repository\Broadcast\PortalRepository $repo */
        $data = [];
        $group = $this->getCurrentGroup($request);
        $dataForm = $request->query->has('diffusion_search') ? $request->query->get('diffusion_search') : [];
        $form = $this->createForm($this->get('app.form.diffusion_search'),$dataForm);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            if(isset($data['bigGroups']) && $data['bigGroups'] ) { $data['bigGroups'] = $data['bigGroups']->getId(); }
            if($data['group']) { $group = $data['group'];$data['group'] = $data['group']->getId(); }
        }

        $data['isEnabled'] = true;
        $datatable = $this->get('app.datatable.diffusion_portals');
        $datatable->buildDatatable(['data' => $data, 'group' => $group]);

        $sendForm = null;

        return $this->render('AppBundle:Diffusion:actifs.html.twig',[
            'datatable' => $datatable,
            'form'       => $form->createView()
        ]);
    }

    /**
     * @Route("/passerelles/register/{slug}/{id}", defaults={"id" = null}, options={"expose"=true}, name="portails_portail_register" )
     * @ParamConverter("portal", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("group", options={"mapping": {"id": "id"}})
     * @Method({"GET", "POST"})
     * @Template("AppBundle:Diffusion:view.html.twig")
     **/
    public function registerPortalAction(Request $request, Portal $portal, Group $group = null)
    {

        if(!$group) {
            $this->addFlash("error", $this->get('translator')->trans("portal.you_must_choose_a_group", [], 'commiti'));
            return $this->redirectToRoute('portails_catalogue_list');
        }

        $portalRegistrationRepo = $this->getDoctrine()->getRepository("AppBundle:Broadcast\PortalRegistration");
        $existPortatRegistration = $portalRegistrationRepo->findOneBy(["group" => $group, "broadcastPortal" => $portal]);
        $user = $this->getUser();
        if (!$existPortatRegistration)
        {
            $newRegistration = new PortalRegistration();
            $newRegistration->setBroadcastPortal($portal)
                ->setGroup($group);
            $newRegistration->setIsEnabled(false);
            $newRegistration->setStatus(PortalRegistration::STATUS_SENT_TO_COMMITI);
            $newRegistration->setIsUsed(false);
            $newRegistration->setOwner($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($newRegistration);
            $em->flush();
            $this->get("app.mailer.manager")->sendPortalRequestRegistration(
                [
                "user"      => $user,
                'group'     => $group,
                'portal'    => $portal
                ]);

            $this->addFlash("success", $this->get('translator')->trans("portal.registration_mail_alert_sent", [], 'commiti'));

        }else{
            $this->addFlash("error", $this->get('translator')->trans("portal.registration_exist_error", [], 'commiti'));
        }

        return new RedirectResponse(
            $this->generateUrl('portails_portail_view', [
                    "slug" => $portal->getSlug(),
                    "id" => $group->getId()
                ]
            )
        );
    }

    /**
     * @Route("/passerelles/view/{slug}/{id}", defaults={"id" = null}, options={"expose"=true}, name="portails_portail_view" )
     * @ParamConverter("portal", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("group", options={"mapping": {"id": "id"}})
     * @Method({"GET", "POST"})
     * @Template("AppBundle:Diffusion:view.html.twig")
     **/
    public function viewPortalAction(Request $request, Portal $portal, Group $group = null){

        if(!$group) {
            $this->addFlash("error", $this->get('translator')->trans("portal.you_must_choose_a_group", [], 'commiti'));
            return $this->redirectToRoute('portails_catalogue_list');
        }

        $formErrors = false;
        $data = [];
        $form = $this->createForm(new ContactPortalAdminType(),$data);
        $form->handleRequest($request);
        $portalRegistrationRepo = $this->getDoctrine()->getRepository("AppBundle:Broadcast\PortalRegistration");
        $existPortatRegistration = $portalRegistrationRepo->findOneBy(["group" => $group, "broadcastPortal" => $portal]);

        return [
            'formErrors' => $formErrors,
            'portal'     => $portal,
            'group'     => $group,
            'existPortatRegistration'     => $existPortatRegistration,
            'form'       => $form->createView()
        ];
    }

    /**
     * @Route("/passerelles/attente", name="portails_waiting" )
     * @Method({"GET"})
     * @Template("AppBundle:Diffusion:waiting.html.twig")
     **/
    public function waitingPortalAction(Request $request){

        $group = $this->getCurrentGroup($request);

        $data = [];
        $dataForm = $request->query->has('portal_registration_search') ?
            $request->query->get('portal_registration_search') : [];
        $form = $this->createForm($this->get('app.form.portal_registration_search'),$dataForm);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $group = $this->getCurrentGroup($request);
            if(isset($data['bigGroups']) && $data['bigGroups']) {
                $data['bigGroups'] = $data['bigGroups']->getId();
            }
            if($data['group']) { $data['group'] = $data['group']->getId();$group = $data['group']; }
        }

        $datatable = $this->get('app.datatable.portals_waiting');
        $datatable->buildDatatable(['data' => $data, 'group' => $group]);

        return [
            'datatable'      => $datatable,
            'form'       => $form->createView()
        ];
    }


    /**
     * @Route("/passerelles/registration/{id}", options={"expose"=true}, name="portal_registration_edit" )
     * @param PortalRegistration $portalRegistration
     * @Method({"GET","POST"})
     * @Template("AppBundle:Diffusion:waiting_edit.html.twig")
     **/
    public function portalRegistrationEditAction(Request $request, PortalRegistration $portalRegistration){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedHttpException();
        $oldStatus = $portalRegistration->getStatus();
        $oldIsEnabled = $portalRegistration->getIsEnabled();
        $form = $this->createForm(new PortalRegistrationType(),$portalRegistration);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() ) {
            if($form->get("submit")->isClicked() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $portalRegistration->setIsUsed(true);
                $em->flush();
                // sent mail if enabled
                if( $oldStatus != $portalRegistration->getStatus() || $oldIsEnabled != $portalRegistration->getIsEnabled()){
                    $status = "";
                    if($oldIsEnabled != $portalRegistration->getIsEnabled() && $portalRegistration->getIsEnabled()) {
                        $status = $this->get('translator')->trans("portal.set_enbaled", array(), 'commiti');

                    } else {
                        switch ($portalRegistration->getStatus()) {
                            case PortalRegistration::STATUS_SENT_TO_COMMITI :
                                $status = $this->get('translator')->trans("portal.status_sent_to_commiti", array(), 'commiti');
                                break;
                            case PortalRegistration::STATUS_WAITING_SUPPORT :
                                $status = $this->get('translator')->trans("portal.status_waiting_support", array(), 'commiti');
                                break;
                        }
                    }

                    //Sen Email
                    $this->get("app.mailer.manager")->sendPortalEditStatutRegistration(
                        [
                            "portalRegistration"      =>  $portalRegistration,
                            'status'     => $status,
                        ]);
                }
                $this->addFlash("success", $this->get('translator')->trans("portal.registration.set_enabled",
                    [], 'commiti'));
            }
            return $this->redirectToRoute('portails_waiting');

        }

        return [
            'form' => $form->createView(),
            'portalRegistration' => $portalRegistration
        ];
    }

    /**
     * delete registration portal
     * @Route("/passerelles/registration/delete/{id}", options={"expose"=true}, name="portal_registration_delete" )
     * @param PortalRegistration $portalRegistration
     * @return RedirectResponse
     **/
    public function portalRegistrationDeleteAction(PortalRegistration $portalRegistration){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $this->get("app.tracking.export.helpers")->remove($portalRegistration->getTrackingPhone());
        $em->remove($portalRegistration);
        $em->flush();

        $this->addFlash("success", $this->get('translator')->trans("portal.registration_deleted", [], 'commiti'));

        return new RedirectResponse($this->generateUrl('portails_waiting'));
    }

    /**
     * @Route("/passerelles/espace-pro/{slug}", options={"expose"=true}, name="portails_espace_pro" )
     **/
    public function portailespaceProAction(Request $request, Portal $portal){
        if(!$portal->getEspacePro())
            return $this->redirect('/');
        return $this->redirect($portal->getEspacePro());
    }

}