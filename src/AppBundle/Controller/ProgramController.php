<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\ProgramFlow;
use AppBundle\Form\Type\ProgramType;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use TrackingBundle\Entity\Advantage;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class ProgramController
 * @package AppBundle\Controller
 * @Route("/programme")
 */
class ProgramController extends BaseController
{

    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="program_index" )
     * @Method({"GET"})
     * @Template("AppBundle:Program:index.html.twig")
     **/
    public function indexAction(Request $request)
    {
        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $trackingAdvantage = null;
        $groupAgency = $this->getUser()->getAgencyGroup();

        if(is_object($groupAgency)) {
            /** tracking program */
            $trackingRepository = $this->getDoctrine()->getRepository("TrackingBundle:Advantage");
            /** @var Advantage $trackingAdvantage */
            $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($groupAgency->getAgency(), "program");
        }

        $user   = $this->getUser();
        $form   = null;
        $data   = $request->query->has('gac_search') ? $request->query->get('gac_search') : [];
        if($this->isGranted('ROLE_BRANDLEADER')) {
            $form = $this->createForm($this->get('app.form.gac_search'),$data, ['user' => $user]);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $data = $form->getData();

                if(!empty($data['bigGroups']))
                    $data['bigGroups'] = $data['bigGroups']->getId();
                if(!empty($data['commercial']))
                    $data['commercial'] = $data['commercial']->getId();
                if(!empty($data['group'])) {
                    $data['group'] = $data['group']->getId();
                    $group = $data['group'];
                }
            }
        }

        $datatable = $this->get('app.datatable.program');
        $datatable->buildDatatable([
            'group'             => $group,
            'trackingAdvantage' => $trackingAdvantage,
            'data'              => $data
        ]);

        return [
            'datatable'         => $datatable,
            'trackingAdvantage' => $trackingAdvantage,
            'form'              => $form ? $form->createView() : null
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="program_view")
     * @Method({"GET"})
     * @Template("AppBundle:Program:view.html.twig")
     */
    public function viewAction(Request $request, Program $program)
    {
        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Repository\AdRepository $adRepository */
        $adRepository = $em->getRepository('AppBundle:Ad');
        $report       = $adRepository->getProgramReport($program);
        $programParams = [
            'nbAds'     => $adRepository->getNbAdCountByProgram($program),
            'nbAdsRest' => $adRepository->getNbAdRestByProgram($program),
            'surfaceMin'=> $report['minSurface'],
            'surfaceMax'=> $report['maxSurface'],
            'prixMin'   => $report['minPrix'],
            'prixMax'   => $report['maxPrix'],
        ];

        if ($group) {
            $datatable = $this->get('app.datatable.ad_by_program');
            $datatable->buildDatatable(['program'=>$program]);
            return [
                'datatable'     => $datatable,
                'program'       => $program,
                'programParams' => $programParams
            ];
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation", options={"expose"=true}, name="program_create_1")
     * @Method({"GET"})
     * @Template("AppBundle:Program:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $user         = $this->getUser();
        $group = $this->getCurrentGroup($request);
        $groupsList   = null;
        $group  = $user->getGroup();
        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
            $groupsList  = $this->getDoctrine()->getRepository('AppBundle:Group')->findGroups();
            $group = $this->getUserService()->checkUserGroup($request);
        }

//        $agenciesList = null;
//        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') ||
//            ($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP'))
//        ) {
//            $group        = $this->getUserService()->checkUserAgency($request);
//            $agenciesList = $this->getDoctrine()->getRepository('AppBundle:Group')->getAgencyGroups($parentGroup);
//        } else {
//            $group       = $user->getAgencyGroup();
//        }

        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $commercial        = $this->getUserService()->checkCommercialUser($request);
        $group = $commercial->getGroup();

//        $commercials = null;
//        if(!$user->hasRole('ROLE_SALESMAN') && $group) {
//            $em = $this->getDoctrine()->getManager();
//            $userRepository = $em->getRepository('AppBundle:User');
//            $commercials = $userRepository->getCommercialOnly($group);
//            if(!$commercials) {
//                $this->addFlash("error", "Vous devez avoir au moin un commercial pour crée un programme");
//                return $this->redirectToRoute('agency_employee_add',['remoteId' => $group->getRemoteId()]);
//            }
//        }

        return [
//            'agenciesList'=> $agenciesList,
//            'commercials' => $commercials,
            'groupsList'  => $groupsList,
            'group'      => $group,
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation/{projectType}", options={"expose"=true},
     *     requirements={"projectType" = "maison|terrain|appartement|mixte"}, name="program_create_2")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Program:create_program.html.twig")
     */
    public function createProgramAction(Request $request,$projectType)
    {
        $user        = $this->getUser();
        $em          = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);

        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        if(($projectType == 'maison' && !$group->getProgramMaisonOption())   ||
           ($projectType == 'terrain' && !$group->getProgramTerrainOption()) ||
           ($projectType == 'appartement' && !$group->getProgramAppartementOption()) ||
           ($projectType == 'mixte' && !$group->getProgramMixteOption())) {
            throw new AccessDeniedHttpException();
        }

        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$commercial) {
            $userRepository = $em->getRepository('AppBundle:User');
            if($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && !$userRepository->getCommercialOnly($group)) {
                $this->addFlash("error", "Vous devez avoir au moin un commercial pour crée une annonce");
                return $this->redirectToRoute('agency_employee_add',['remoteId' => $group->getRemoteId()]);
            }
            throw new AccessDeniedHttpException();
        }

        $session     = $request->getSession();
        /** @var ProgramFlow $flow */
        $flow        = $this->get('app.form.flow.program');

        if(!$session->get('brouillon_program_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_program_id',$brouillon->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_program_id'));
        }

        $program     = new Program();
        $program->setType($projectType);
        switch ($projectType) {
            case 'maison':$program->setTypologieResidence(3100); break;
            case 'appartement':$program->setTypologieResidence(3200); break;
            case 'terrain':$program->setTypologieResidence(3300); break;
            case 'mixte':$program->setTypologieResidence(3500); break;
        }

        $flow->bind($program);
        $form = $submittedForm = $flow->createForm();

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1 ) {
                if($brouillon->getImagePrincipale()) {
                    $program->setImagePrincipale($brouillon->getImagePrincipale());
                } else {
                    $error = new FormError("l'image principale est obligatoire.");
                    $form->get('imagePrincipale')->addError($error);
                    return [
                        'form'       => $form->createView(),
                        'flow'       => $flow,
                        'program'    => $program,
                        'brouillon'  => $brouillon
                    ];
                }
            }

            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                $program->setImagePrincipale($brouillon->getImagePrincipale());
                $program->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                $sendMail = false;
                if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') &&
                    $user->hasRole('ROLE_SALESMAN') && $user->getProgramModeration())  {
                    $program->setStat(Program::PENDING);
                    $program->setPublished(false);
                } else {
                    $program->setStat(Program::VALIDATE);
                    $program->setPublished(true);
                }

                foreach ($brouillon->getDocuments() as $document) {
                    $document->setBrouillon(null);
                    $program->addDocument($document);
                }

                $program->setType($projectType);
                $em->persist($program);
                $em->flush();

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $brouillon->setImagePrincipale(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_program_id');
                }
                $flow->reset();

                //Sen Email
                $userRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
                $chef = $userRepository->findByRole('ROLE_BRANDLEADER', $group);
                if($sendMail) {
                $this->get("app.mailer.manager")->sendNotifModerateCreated(
                    [
                        "chef"      => $chef,
                        "user"      => $user,
                        'program'     => $program
                    ]);}

                $this->addFlash("success", "Le program a été ajouté");
                return new RedirectResponse($this->generateUrl('diffusion_program',['remoteId' => $program->getRemoteId()]));
            }
        }
        return [
            'form'       => $form->createView(),
            'flow'       => $flow,
            'program'    => $program,
            'brouillon'  => $brouillon
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="program_edit")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Program:editer_program.html.twig")
     */
    public function editProgramAction(Request $request,Program $program)
    {
        /** @var \AppBundle\Entity\User $user */
        $user        = $this->getUser();
        $group = $this->getCurrentGroup($request);

        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$commercial) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
                throw new AccessDeniedHttpException();
            $commercial = $program->getOwner();
        }

        if(!$this->isGranted('ROLE_ADMIN'))
        if(!$user->isEmployeeOf($group))
            throw new AccessDeniedHttpException();

        $em          = $this->getDoctrine()->getManager();
        $session     = $request->getSession();
        $flow        = $this->get('app.form.flow.program');

        if(!$session->get('brouillon_program_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_program_id',$brouillon->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_program_id'));
        }
        $oldImage = $program->getImagePrincipale();

        $flow->bind($program);
        $form = $submittedForm = $flow->createForm(['checkImageEmpty' => false]);
        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1 && $brouillon->getImagePrincipale()) {
                $program->setImagePrincipale($brouillon->getImagePrincipale());
            } else {
                $program->setImagePrincipale($oldImage);
            }

            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                if($brouillon->getImagePrincipale())
                    $program->setImagePrincipale($brouillon->getImagePrincipale());
                $program->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                if($user->hasRole('ROLE_SALESMAN') && $user->getProgramModeration()) {
                    $program->setStat(Program::PENDING);
                    $program->setPublished(false);
                } else {
                    $program->setStat(Program::VALIDATE);
                    $program->setPublished(true);
                }

                $em->persist($program);
                $em->flush();
                /** @var \AppBundle\Entity\Document $document */
                if($brouillon->getDocuments()) {
                    foreach ($brouillon->getDocuments() as $document)
                        $program->addDocument($document);
                }

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $brouillon->setImagePrincipale(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_program_id');
                }
                $flow->reset();
                $this->addFlash("success", "Le program a été modifier");
                return new RedirectResponse($this->generateUrl('program_view',['remoteId' => $program->getRemoteId()]));
            }
        }
        return [
            'form'       => $form->createView(),
            'flow'       => $flow,
            'program'    => $program,
            'brouillon'  => $brouillon
        ];
    }


    /**
     * @param Request $request
     * @param Program $program
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/diffusion/{remoteId}", options={"expose"=true}, name="diffusion_program")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Program:diffusion_program.html.twig")
     */
    public function diffusionProgramAction(Request $request, Program $program)
    {
        $user        = $this->getUser();
        $group = $this->getCurrentGroup($request);

        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
            if(!$user->isEmployeeOf($program->getGroup()))
                throw new AccessDeniedHttpException();

            if(!$commercial) {
                if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
                    throw new AccessDeniedHttpException();
                $commercial = $program->getOwner();
            }
            if($user->hasRole('ROLE_SALESMAN') && !$commercial->equals($program->getOwner()))
                throw new AccessDeniedHttpException();
        } else {
            $commercial = $program->getOwner();
        }

        $agenceGroup = $commercial->getAgencyGroup();
        $diffusion   = $program->getDiffusion() ? $program->getDiffusion() : new Diffusion();
        $oldBroadcastPortals = clone $diffusion->getBroadcastPortals();

        $commercialPortalsLimit = $this->getDoctrine()->getRepository('AppBundle:Broadcast\CommercialPortalLimit')->getCommercialProgramPortal($commercial, true);
        $adDailyLimitRepository = $this->getDoctrine()->getRepository('AppBundle:Broadcast\AdDailyLimit');
        $allPortals = $this->getDoctrine()->getRepository('AppBundle:Broadcast\Portal')->findBy(['diffuseProgram'=>true]);
        $alreadyRegistredPortals = $this->getDoctrine()->getRepository('AppBundle:Broadcast\PortalRegistration')->getPortalRegistrationByGroup($agenceGroup,true);
        $alreadyPayedPortalList = $commercialLimit = [];

        /** @var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortalLimit */
        foreach ($commercialPortalsLimit as $commercialPortalLimit) {
            $commercialLimit[$commercialPortalLimit->getPortal()->getId()]['limit'] = $commercialPortalLimit;
            /** @var \AppBundle\Entity\Broadcast\AdDailyLimit $rest */
            $used = $this->getDoctrine()->getRepository('AppBundle:Broadcast\Diffusion')->getPortalCommercialDailyUsed($commercial,$commercialPortalLimit->getPortal());
            $commercialLimit[$commercialPortalLimit->getPortal()->getId()]['rest'] = $commercialPortalLimit->getLimit() - $used ;
        }
        /** @var \AppBundle\Entity\Broadcast\PortalRegistration $alreadyRegistredPortal */
        foreach ($alreadyRegistredPortals as $alreadyRegistredPortal)
            $alreadyPayedPortalList[$alreadyRegistredPortal->getId()] =   $alreadyRegistredPortal->getBroadcastPortal();

        $substractValue = count($program->getAds());
        $form = $this->createForm(new DiffusionType(),$diffusion, [
            'program'             => $program,
            'allPortals'          => $allPortals,
            'alreadyPayedPortals' => $alreadyPayedPortalList,
            'commercialLimit'     => $commercialLimit,
            'substractValue'      => $substractValue
        ]);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->get('submit')->isClicked() && $form->isValid()) {
                $broadcastPostals = $diffusion->getBroadcastPortals();
                $em         = $this->getDoctrine()->getManager();

                $deletedPortals = [];
                foreach ($oldBroadcastPortals as $p) {
                    if(!$p->existIn($broadcastPostals)) {
                        $deletedPortals[] = $p;
                    }
                }

                foreach ($broadcastPostals as $broadcastPostal) {
                    if($broadcastPostal->getCluster() != 'GRATUIT') {
                        $rest = 0;
                        if($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()]))
                            $rest = $commercialLimit[$broadcastPostal->getId()]['rest'] ? ($commercialLimit[$broadcastPostal->getId()]['rest'] - $substractValue) : ($commercialLimit[$broadcastPostal->getId()]['limit']->getLimit() - $substractValue);

                        if(
                            ($broadcastPostal->getCluster() == 'PAYANT' && !$broadcastPostal->existIn($alreadyPayedPortalList)) ||
                            ($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()]) && $rest < 0 )
                        ) {
                            $diffusion->removeBroadcastPortal($broadcastPostal);
                            $this->addFlash("error", "Pas tout les cibles selectionner serons diffuser");
                        } elseif($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()])) {
                            $commercialPortalLimit = $commercialLimit[$broadcastPostal->getId()];
                            $instance = $adDailyLimitRepository->findOneBy(['commercialPortal'=>$commercialPortalLimit]);

                            $rest = $commercialLimit[$broadcastPostal->getId()]['rest'] ? ($commercialLimit[$broadcastPostal->getId()]['rest'] - $substractValue) : ($commercialLimit[$broadcastPostal->getId()]['limit']->getLimit() - $substractValue);

                            if(!$instance) {
                                $rest = $commercialLimit[$broadcastPostal->getId()]['limit']->getLimit() - $substractValue;
                                $instance = new AdDailyLimit();
                                $instance->setRest($rest);
                                $instance->setDay(new \DateTime('today'));
                                $instance->setCommercialPortal($commercialLimit[$broadcastPostal->getId()]['limit']);
                            } else {
                                $instance->setRest($rest);
                            }
                            $em->persist($instance);
                            $em->flush();
                        }
                    }
                }

                if(count($deletedPortals) > 0) {
                    foreach ($deletedPortals as $broadcastPostal) {
                        if($broadcastPostal->getCluster() != 'GRATUIT') {
                            if($broadcastPostal->existIn($alreadyPayedPortalList) && isset($commercialLimit[$broadcastPostal->getId()]))
                                $rest = $commercialLimit[$broadcastPostal->getId()]['rest'] + $substractValue;
                            $commercialPortalLimit = $commercialLimit[$broadcastPostal->getId()];

                            /** @var AdDailyLimit $instance */
                            $instance = $adDailyLimitRepository->findOneBy(['commercialPortal'=>$commercialPortalLimit]);
                            if($instance) {
                                $instance->setRest($rest);
                                $em->persist($instance);
                                $em->flush();
                                $diffusion->removeBroadcastPortal($broadcastPostal);
                            }
                        }
                    }
                }

                /** @var \AppBundle\Entity\Ad $ad */
                foreach ($program->getAds() as $ad) {
                    if(!$ad->getProgramDiffuse()) {
                        $ad->setProgramDiffuse(true);
                        $em->persist($ad);
                    }
                }
                $diffusion->setCommercial($commercial);
                $em->persist($diffusion);
                $em->flush();
                $program->setDiffusion($diffusion);
                $em->flush();

                $this->addFlash("success", "La diffusion a été enregistrer avec succes");
                return new RedirectResponse($this->generateUrl('program_index'));
            }
        }
        return [
            'program'                      => $program,
            'alreadyRegistredPortalsCount' => $alreadyRegistredPortals,
            'commercial'                   => $commercial,
            'commercialLimit'              => $commercialLimit,
            'substractValue'               => $substractValue,
            'form'                         => $form->createView()
        ];
    }


    /**
     * @Route("/moderation", options={"expose"=true}, name="program_moderation")
     * @Template("AppBundle:Program:moderation.html.twig")
     * @Method({"GET"})
     */
    public function moderationAction(Request $request)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
            return $this->redirectToRoute('program_index');

        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        /** @var \AppBundle\Entity\User $user */
        $datatable = $this->get('app.datatable.program_moderation');
        $datatable->buildDatatable();
        return ['datatable' => $datatable];
    }

    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="program_switch_statut")
     * @Method({"GET"})
     */
    public function switchStatusAction(Request $request, Program $program)
    {
        if($request->isXmlHttpRequest()) {
            if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($program->getGroup()))
                throw new AccessDeniedHttpException();

            $em = $this->getDoctrine()->getManager();
            switch ($program->getStatvalue()) {
                case Program::PENDING:
                    $stat = Program::VALIDATE;
                    break;
                case Program::VALIDATE:
                    $stat = Program::ANNULER;
                    break;
                case Program::ANNULER:
                    $stat = Program::PENDING;
                    break;
            }
            $program->setStat($stat);
            $program->setPublished($stat == Program::VALIDATE ? true : false);
            $em->persist($program);
            /*
            $this->get("app.mailer.manager")->sendAdModerateOk(
                [
                    "chef"      => $this->getUser(),
                    'program'     => $program,
                ]);
            */
            $em->flush();
            return new JsonResponse(['_callback' => $program->getStatvalue()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/remonter/{remoteId}", options={"expose"=true}, options={"expose"=true}, name="program_remonter")
     * @Method({"GET"})
     */
    public function remonterAction(Request $request, Program $program)
    {
        if(!$this->getUser()->isEmployeeOf($program->getGroup()))
            throw new AccessDeniedHttpException();

        $user = $this->getUser();
        $group = $this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP') ? $this->getUserService()->checkUserAgency($request) : $user->getAgencyGroup();
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $em         = $this->getDoctrine()->getManager();
        $program->setNbRemonte($program->getNbRemonte() + 1);
        $em->persist($program);
        $em->flush();
        $this->addFlash("success", "Le programme a été remonté");
        return $this->redirectToRoute('program_index');
    }


    /**
     *
     * @Route("/delete/{remoteId}", options={"expose"=true}, name="program_delete")
     * @Method({"GET","DELETE"})
     */
    public function deleteAction(Request $request, Program $program)
    {
        $user        = $this->getUser();
        if(!$user->isEmployeeOf($program->getGroup()) && !$this->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedHttpException();

        $group = $this->isGranted('ROLE_ADMIN') || ($this->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP')) ? $this->getUserService()->checkUserAgency($request) : $user->getAgencyGroup();
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $em = $this->getDoctrine()->getManager();
        $this->get("app.tracking.export.helpers")->remove($program->getTrackingPhone());
        $em->remove($program);
        $em->flush();
        $this->addFlash("success", "Le programme a été supprimé");
        return $this->redirectToRoute('program_index');

    }

    /**
     *
     * @Route("/actions/{action}", options={"expose"=true}, name="programs_actions")
     * @Method({"GET","POST","DELETE"})
     */
    public function multipleActionAction(Request $request,$action)
    {
        $isAjax = $request->isXmlHttpRequest();

        if ($isAjax) {
            $choices = $request->request->get('data');
            $token = $request->request->get('token');

            if (!$this->isCsrfTokenValid('multiselect', $token)) {
                throw new AccessDeniedException('The CSRF token is invalid.');
            }

            $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository('AppBundle:Program');

            $em = $this->getDoctrine()->getManager();
            switch($action) {
                case 'delete' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $this->get("app.tracking.export.helpers")->remove($entity->getTrackingPhone());
                        $em->remove($entity);
                    }
                    $em->flush();
                    break;
                case 'remonter' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setNbRemonte($entity->getNbRemonte() + 1);
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
                case 'activer' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(true)->setStat(Program::VALIDATE);
                        $em->persist($entity);
                        $em->flush();
                        //Send Mail
                        $this->get("app.mailer.manager")->sendProgramModerateOk(
                            [
                                "chef"      => $this->getUser(),
                                'program'     => $program
                            ]);
                    }
                    break;
                case 'desactiver' :
                    foreach ($choices as $choice) {
                        $entity = $repository->find($choice['value']);
                        $entity->setPublished(true)->setStat(Program::PENDING);
                        $this->get("app.tracking.export.helpers")->remove($entity->getTrackingPhone());
                        $em->persist($entity);
                        $em->flush();
                    }
                    break;
                case 'tracking' :
                    $group = $this->getUser()->getAgencyGroup();
                    /**
                     * tracking portal
                     */
                    $trackingRepository = $this->getDoctrine()->getRepository("TrackingBundle:Advantage");
                    /** @var Advantage $trackingAdvantage */
                    $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), "program");
                    if( $trackingAdvantage){
                        $trackingOK = $trackingAdvantage->getPhones()->count();
                        foreach ($choices as $choice) {
                            if ($trackingOK == $trackingAdvantage->getAmount())
                                break;
                            $program = $repository->find($choice['value']);

                            // create if expire or not exist
                            if (
                                $program->getStatValue() == "valider" &&
                                !$this->get("app.tracking.export.helpers")->checkPhoneTracking($program) &&
                                $this->get("app.tracking.program.helpers")->getAvailableNumber($program, $trackingAdvantage)
                            ) {
                                $trackingOK++;
                            }
                        }
                    }
                    break;
            }
            return new Response('Success', 200);
        }
        return new Response('Bad Request', 400);
    }


    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="programs_switch_published_statut")
     * @Method({"GET","POST"})
     */
    public function switchPublishedStatusAction(Request $request, Program $program)
    {
        if($request->isXmlHttpRequest()) {
            if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($program->getGroup()))
                throw new AccessDeniedHttpException();

            $em = $this->getDoctrine()->getManager();
            switch ($program->getStatvalue()) {
                case Program::PENDING:
                    $stat = Program::VALIDATE;
                    break;
                case Program::VALIDATE:
                    $stat = Program::ANNULER;
                    break;
                case Program::ANNULER:
                    $stat = Program::PENDING;
                    break;
            }
            $program->setStat($stat);
            $program->setPublished($stat == HouseModel::VALIDATE ? true : false);
            $em->persist($program);
            $em->flush();
            return new JsonResponse(['_callback' => $program->getStatvalue()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/informer/{remoteId}", options={"expose"=true}, name="program_informer")
     * @Method({"GET"})
     */
    public function informerAction(Request $request, Program $program)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($program->getGroup()))
            throw new AccessDeniedHttpException();
        $commercial = $program->getOwner();
        $msg = $request->query->get('value');
        /*
        $this->get("app.mailer.manager")->sendCommentProgram(
            [
                "chef"      => $this->getUser(),
                'program'     => $program,
                'msg'     => $msg
            ]);
        */
        $this->addFlash("success", $commercial->getName(). ' à été informé');
        return $this->redirectToRoute('program_moderation');
    }
}