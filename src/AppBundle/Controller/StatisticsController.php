<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class AgenciesController
 * @package AppBundle\Controller
 * @Route("/statistics")
 */
class StatisticsController extends BaseController
{


    /**
     * Secure the access to the website
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("", name="statistics_homepage" , options={"expose"=true})
     * @Method({"GET"})
     * @Template("AppBundle:Statistics:index.html.twig")
     */
    public function indexAction(Request $request)
    {

        return [];

    }


}
