<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\Payment\Order;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class FactureController
 * @package AppBundle\Controller
 * @Route("/invoices")
 */
class FactureController extends BaseController
{

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/", name="agency_invoices", options={"expose"=true} )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Invoice:invoices.html.twig")
     **/
    public function invoicesAction(Request $request){
        $datatable = $this->get('app.datatable.invoices');
        $datatable->buildDatatable();

        return ['datatable' => $datatable];
    }

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/voir/{remoteId}", name="invoice_show", options={"expose"=true} )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Invoice:invoice_show.html.twig")
     **/
    public function showInvoiceAction(Request $request, Order $order){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $order->getGroup()->isMemberOf($user->getGroup()))
            throw new AccessDeniedHttpException();
        return [ 'order'  => $order ];
    }



    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/bank-wire/{remoteId}", name="bankwire_show")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Invoice:bankwire_show.html.twig")
     **/
    public function bankWireDetailsAction(Request $request, Order $order){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') && $order->getGroup()->isMemberOf($user->getGroup()))
                throw new AccessDeniedHttpException();
        return [ 'order'  => $order ];
    }

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/check-wire/{remoteId}", name="checkwire_show")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Invoice:checkwire_show.html.twig")
     **/
    public function checkWireDetailsAction(Request $request, Order $order){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') && $order->getGroup()->isMemberOf($user->getGroup()))
                throw new AccessDeniedHttpException();
        return [ 'order'  => $order ];
    }

    /**
     * @return Response
     *
     * @Route("/delete/{remoteId}", name="invoice_delete", options={"expose"=true} )
     * @Method({"GET","POST"})
     **/
    public function deleteInvoiceAction(Request $request,Order $order){
        if($order->getStatus() != 'waiting' || !$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$this->getUser()->getGroup()->hasRole('ROLE_GROUP') || ($this->getUser()->getGroup()->hasRole('ROLE_GROUP') && !$order->getGroup()->isMemberOf($this->getUser()->getGroup())))
            throw new AccessDeniedHttpException();
        $group = $order->getGroup();
        $em = $this->getDoctrine()->getManager();
        $em->remove($order);
        $em->flush();
        $this->addFlash('success', 'invoice_deleted');
        return $this->redirectToRoute('group_agency_show',['remoteId'=>$group->getRemoteId()]);
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return Response
     * @Route("/print/{remoteId}", name="invoice_pdf", options={"expose"=true} )
     */
    public function pdfAction(Request $request, Order $order)
    {
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $order->getGroup()->isMemberOf($user->getGroup()))
                throw new AccessDeniedHttpException();
        $agency =  $order->getGroup()->getAgency();
        $html = $this->renderView('AppBundle:Invoice:invoice_pdf.html.twig',array('order' => $order, 'agency' => $agency));

        return new Response(
            $this->getPdfExporter()->getOutputFromHtml($html),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="%s"', $order->getRemoteId().'.pdf'),
            ]
        );
    }

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/moderation", name="moderation_invoices", options={"expose"=true} )
     * @Method({"GET","POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("AppBundle:Invoice:admin_invoices.html.twig")
     **/
    public function moderationInvoiceAction(Request $request){
        $datatable = $this->get('app.datatable.moderation_invoices');
        $datatable->buildDatatable();
        return ['datatable' => $datatable];
    }

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/moderation/decline/{remoteId}", name="moderation_invoices_decline", options={"expose"=true} )
     * @Method({"GET","POST"})
     * @Security("has_role('ROLE_ADMIN')")
     **/
    public function moderationRefuseInvoiceAction(Request $request,Order $order){
        $order->setStatus(Order::STATUS_CANCELED);
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();
        return $this->redirectToRoute('moderation_invoices');
    }

    /**
     * Return the view of the admin group
     * @return Response
     * @var \AppBundle\Entity\Payment\Order
     *
     * @Route("/moderation/validate/{remoteId}", name="moderation_invoices_validate", options={"expose"=true} )
     * @Method({"GET","POST"})
     * @Security("has_role('ROLE_ADMIN')")
     **/
    public function moderationValidateInvoiceAction(Request $request,Order $order){
        $em = $this->getDoctrine()->getManager();
        if($order->getIsCommand()) {
            $oldOrder = $order;
            $oldOrder->setVisible(false);
            $oldOrder->setStatus(Order::STATUS_VALIDATED);
            $em->persist($oldOrder);

            $order = clone $order;
            /** @var \AppBundle\Repository\Payment\OrdersRepository $orderRepository */
            $orderRepository = $this->getDoctrine()->getRepository('AppBundle:Payment\\Order');
            $invoiceCount = $orderRepository->getInvoiceCountByGroup($order->getGroup());
            $order->setReferenceFacture($invoiceCount+1);
            $order->setIsCommand(false);
            $order->setVisible(true);
        }
        $order->setPayedDate(new \DateTime());
        switch($order->getPackId()) {
            case 'subscribe':
                $group = $order->getGroup();
                $now = $datePeriodeStart = new \DateTime('now');
                $monthCount = $order->getDetails()['monthCount'];
                if(!is_null($group->getTrialEnd()) && $group->getTrialEnd() < $now)
                    $datePeriodeStart = $group->getTrialEnd();
                elseif(!is_null($group->getPeriodEnd()) && $group->getPeriodEnd() < $now)
                    $datePeriodeStart = $group->getPeriodEnd();

                $group->setPeriodStart($datePeriodeStart);
                $dateEnd = clone $datePeriodeStart;
                $dateEnd->add(new \DateInterval('P'.$monthCount.'M'));
                $group->setPeriodEnd($dateEnd);
                $group->setBlocked(false);

                $order->setVisible(true);
                $order->setPayedDate($now);
                $order->setTitle($order->getDetails()['title']);

                $em->persist($group);

                $this->addFlash("success", "Le pack a été validé avec success");
                break;
            case 'commercial':
                $instance      = new AuthorizedUser();
                $dateStart     = new \DateTime('today');
                $dateEnd       = (new \DateTime('today'))->add(new \DateInterval('P1Y'));
                $instance->setGroup($order->getGroup())->setRole('ROLE_SALESMAN')->setStartDate($dateStart)->setEndDate($dateEnd);
                $em->persist($instance);
                $em->flush();
                $this->addFlash("success", "Le pack a été validé avec success");
                break;
            case 'advantages':
                $group = $order->getGroup();
                foreach ($order->getDetails() as $item) {
                    $methodeName = PaymentController::DETAILS[$item['key']]['methodName'];
                    $group->$methodeName(true);
                }

                $em->persist($group);
                $em->flush();
                $request->getSession()->remove('advantages_purchase');
                $this->addFlash("success", "Le avantages ont été validé avec success");
                break;
            default:break;
        }

        $order->setStatus(Order::STATUS_VALIDATED);
        $em->persist($order);
        $em->flush();
        //Send Confirm Mail
        $this->get("app.mailer.manager")->sendOrderPaymentOk(
            [
                "email" =>  $order->getUser()->getEmail(),
                "order" =>  $order
            ]
        );
        return $this->redirectToRoute('moderation_invoices');
    }

}