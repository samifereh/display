<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\CommercialPortalLimit;
use AppBundle\Entity\HouseModel;
use AppBundle\Form\Type\AgencyType;
use AppBundle\Form\Type\GroupsType;
use AppBundle\Form\Type\UserModerationType;
use AppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Group as Group;
use AppBundle\Entity\User as User;

use AppBundle\Repository\UserRepository as UserRepository;

use FOS\UserBundle\Form\Type\RegistrationFormType as RegistrationFormType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AgenciesController
 * @package AppBundle\Controller
 * @Route("/employees")
 */
class EmployeesController extends BaseController {

    /**
     * Return the view of the list of employees
     * @return RedirectResponse|Response
     *
     * @Route("/liste-employees/{remoteId}",defaults={"remoteId" = null}, name="agency_employees_list")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:employees/employees_list.html.twig")
     */
    public function listEmployeesAction(Request $request,Group $group = null){
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')
            || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ) ? $group : $this->getUser()->getAgencyGroup();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$group)
            throw new AccessDeniedHttpException();
        if($group){
            $datatable = $this->get('app.datatable.agencyemployee');
            $datatable->buildDatatable(['group' => $group]);
            return ['datatable' => $datatable];
        }
        return new RedirectResponse($this->generateUrl('homepage'));
    }

    /**
     * Return the view for adding an employee
     * @param Request $request
     * @return RedirectResponse|Response
     * 
     * @Route("/ajouter/{remoteId}",defaults={"group" = null}, name="agency_employee_add")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:employees/employee_add.html.twig")
     */
    public function addEmployeeAction(Request $request,Group $group = null){

        $agency = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ) ? $group : $this->getUser()->getAgencyGroup();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$agency)
            throw new AccessDeniedHttpException();

        $authorizedUserRepository = $this->getDoctrine()->getRepository('AppBundle:Payment\\AuthorizedUser');
        $authorizedRoles = $authorizedUserRepository->getEmptyAuthorizedRoles($agency);

        if(!$authorizedRoles) {

            $this->addFlash('error', $this->get('translator')->trans('Vous devez acheter un crédit de commercial pour pouvoir en ajouter'));
            return $this->redirectToRoute('group_agency_show', ['remoteId' => $agency->getRemoteId()]);
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var \AppBundle\Entity\User $user */
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $form = $formFactory->createForm(['translation_domain' => 'commiti']);
        $form->add('roles', 'choice',
            array(
                'label' => 'Role',
                'choices' =>  array_combine($authorizedRoles, $authorizedRoles), // array_combine : put values to keys
                'multiple'  => true,
                'attr' => [
                    'class' => 'select2-once'
                ]
            )
        );
        if($group->hasAnnoncesOption()) {
            $form
                ->add('houseModelsModeration', CheckboxType::class, [
                    'label'    => 'user.moderate_model',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ])
                ->add('adsModeration', CheckboxType::class, [
                    'label'    => 'user.moderate_ads',
                    'required' => false,
                    'attr' => [
                        'class' => 'i-checks'
                    ]
                ]);
        }
        if($group->hasProgramOption()) {
            $form->add('programModeration', CheckboxType::class, [
                'label' => 'user.moderate_program',
                'required' => false,
                'attr' => [
                    'class' => 'i-checks'
                ]
            ]);
        }

        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $user->addGroup($agency);
            $hasCredit = $authorizedUserRepository->findOneBy(['user'=>null,'group'=>$agency,'role'=>$user->getWorkRoles()]);
            if($hasCredit) {
                if($user->hasRole('ROLE_BRANDLEADER'))
                    $user->addRole('ROLE_SALESMAN');
                $userManager->updateUser($user);
                $hasCredit->setUser($user);
                $em = $this->getDoctrine()->getManager();
                $em->persist($hasCredit);
                $em->flush();

                if($user->hasRole('ROLE_SALESMAN')) {
                    $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
                    $commercialPortalLimitRepository = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
                    $portalsRegistrations = $portalRegistrationRepository->findBy(['group' => $agency]);
                    /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                    foreach($portalsRegistrations as $portalRegistration) {
                        $commercialPortalLimit = $commercialPortalLimitRepository->findOneBy([
                            'portal'     => $portalRegistration->getBroadcastPortal(),
                            'commercial' => $user->getId()
                        ]);
                        if(!$commercialPortalLimit) {
                            $commercialPortalLimit = new CommercialPortalLimit();
                            $commercialPortalLimit->setPortal($portalRegistration->getBroadcastPortal());
                            $commercialPortalLimit->setCommercial($user);
                            $commercialPortalLimit->setLimit(0);
                            $em->persist($commercialPortalLimit);
                        }
                    }
                    $em->flush();
                }

                $password = $form->get('plainPassword')->getData();

                //send email for new User
                $this->get("app.mailer.manager")->sendNewAccount(
                    [
                        'user'     => $user,
                        'password'  =>$password
                    ]);

                $this->addFlash('success', $this->get('translator')->trans('agence.agency_employee_added'));
                if($agency->getParentGroup())
                    return $this->redirectToRoute('group_agency_show', ['remoteId' => $agency->getRemoteId()]);
                elseif($agency->hasRole('ROLE_GROUP')) {
                    if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                        return $this->redirectToRoute('groups_admin_show',['remoteId' => $agency->getRemoteId()]);
                    else
                        return $this->redirectToRoute('group_agencies_list');
                }
                else
                    return $this->redirectToRoute('agency_my');
            } else {
                $this->addFlash('error', 'Vous devez acheter un crédit de commercial pour pouvoir en ajouter');
                return $this->redirectToRoute('group_agency_show', ['remoteId' => $agency->getRemoteId()]);
            }
        }
        return [ 'form'  => $form->createView(), 'group' => $group];
    }

    /**
     * Edit an employee
     * @param User $user
     * @param Request $request
     * @return Response
     *
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="agency_employee_edit")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:employees/employee_edit.html.twig")
     */
    public function editEmployeeAction(Request $request,User $user){

        $isGroup = $user->getGroup()->hasRole('ROLE_GROUP') ? true : false;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
            if($user->getGroup()->hasRole('ROLE_GROUP')) {
                $group = $this->getUser()->getGroup();
                if(!$user->getAgencyGroup()->isMemberOf($group))
                    throw new AccessDeniedException();
            } else {
                $group = $user->getAgencyGroup();
                if(!$user->isEmployeeOf($group))
                    throw new AccessDeniedException();
            }
        } else {
            $group = $user->getAgencyGroup();
        }

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$group)
            throw new AccessDeniedHttpException();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:User');

         /** @var \AppBundle\Entity\User $user */
        $user = $repo->find($user->getId());
        $current = array(
            'avatar'   => $user->getAvatar(),
            'password' => $user->getPassword()
        );

        $roles = $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') ?
            UserRepository::getUsersRoles() :
            (
                $isGroup ?
                    UserRepository::getGroupAgencyEmployeesRoles() :
                    UserRepository::getAgencyEmployeesRoles()
            );
        $form = $this->createForm(UserType::class,$user,
            [
                'roles' => $roles,
                'isEdit' => true
            ]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $user = $form->getData(); // set the $user value
            $user->setPassword($current['password']);

            if ($form->isValid()) {
                if(null === $form->get("avatar")->getData())
                    $user->setAvatar($current['avatar']);
                else{
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($user, $user->getAvatar());
                }
                $em->persist($user);
                $em->flush();

                //$userManager = $this->get('fos_user.user_manager');
                //$userManager->updateUser($user);
                $this->addFlash('success', $this->get('translator')->trans('agence.agency_modified'));
                if($user->getGroup()->getParentGroup())
                    return $this->redirectToRoute('group_agency_show', ['remoteId' => $user->getGroup()->getRemoteId()]);
                elseif($user->getGroup()->hasRole('ROLE_GROUP')) {
                    if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                        return $this->redirectToRoute('groups_admin_show',['remoteId' => $user->getGroup()->getRemoteId()]);
                    else
                        return $this->redirectToRoute('group_agencies_list');
                }
                else
                    return $this->redirectToRoute('agency_my');
            }
        }

        $formFactory = $this->get('fos_user.change_password.form.factory');
        $formPassword = $formFactory->createForm();
        $formPassword->setData($user);
        $formPassword->handleRequest($request);

        if ($formPassword->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
            $this->addFlash('success', $this->get('translator')->trans('agence.agency_employee_password_edited'));
            if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')){
                return $this->redirectToRoute('users_admin');
            }
            return new RedirectResponse($this->generateUrl('agency_employees_list'));
        }

        $return = [
            'form'          => $form->createView(),
            'formPassword'  => $formPassword->createView(),
            'user'          => $user
        ];

        if($user->hasRole('ROLE_SALESMAN')) {
            $em = $this->getDoctrine()->getManager();
            $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
            $commercialPortalLimitRepository = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
            $portalsRegistrations = $portalRegistrationRepository->findBy(['group'=>$user->getAgencyGroup(),
                'isUsed' => true]);
            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
            foreach($portalsRegistrations as $portalRegistration) {
                if($portalRegistration->getBroadcastPortal()->getCluster() == 'PAYANT') {
                    $commercialPortalLimit = $commercialPortalLimitRepository->findOneBy([
                        'portal'     => $portalRegistration->getBroadcastPortal(),
                        'commercial' => $user->getId()
                    ]);
                    if(!$commercialPortalLimit) {
                        $commercialPortalLimit = new CommercialPortalLimit();
                        $commercialPortalLimit->setPortal($portalRegistration->getBroadcastPortal());
                        $commercialPortalLimit->setCommercial($user);
                        $commercialPortalLimit->setLimit(0);
                        $em->persist($commercialPortalLimit);
                        $em->flush();
                    }
                }
            }

            $formCommercialPortals = $this->createForm($this->getCommercialPortalsLimitFormType(),$user);
            if($request->getMethod() == 'POST') {
                $formCommercialPortals->handleRequest($request);
                if($formCommercialPortals->get('cancel')->isClicked()) {

                    if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')){
                        return $this->redirectToRoute('users_admin');
                    }

                    if($isGroup)
                        return $this->redirectToRoute('group_agency_show',['remoteId'=>$group->getRemoteId()]);

                    return $this->redirectToRoute('agency_employees_list');
                }
                elseif($formCommercialPortals->get('submit')->isClicked()) {
                    if ($formCommercialPortals->isValid()) {
                        $commercialsPortalsRepository = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
                        /** @var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortal */
                        foreach ($user->getCommercialsPortal() as $commercialPortal) {
                            /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portailRegistration */
                            $portailRegistration = $portalRegistrationRepository->findOneBy(['broadcastPortal' => $commercialPortal->getPortal(), 'group'=>$user->getAgencyGroup()]);
                            $commercialsPortals = $commercialsPortalsRepository->getCommercialWithGroupAndPortal($commercialPortal->getPortal(), $user);
                            $em->persist($commercialPortal);
                            $em->flush();
                            $portailRegistration->setAdRest($portailRegistration->getAdLimit() - $commercialsPortals['usedLimit'] - $commercialPortal->getLimit());
                            $em->persist($portailRegistration);
                            $em->flush();
                        }
                        $this->addFlash("success", "Limite de portails affectée avec succès");
                        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()
                                ->hasRole('ROLE_GROUP'))
                            return $this->redirectToRoute('group_agency_show',['remoteId'=>$group->getRemoteId()]);
                        return new RedirectResponse($this->generateUrl('agency_employees_list'));
                    }
                }
            }
            $return['formCommercialPortals'] = $formCommercialPortals->createView();

            ## FORM Moderation ###
            $formModeration = $this->createForm(UserModerationType::class,$user);
            $formModeration->handleRequest($request);
            if ($formModeration->isSubmitted() && $formModeration->isValid()) {
                $em->persist($user);
                $em->flush();
            }
            $return['formModeration'] = $formModeration->createView();
        }

        return $return;
    }

    /**
     * Load the view of an employee
     * @param User $user
     * @return Response|array
     *
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="agency_employee_show")
     * @Template("AppBundle:Agencies:employees/employee_show.html.twig")
     */
    public function showEmployeeAction(Request $request, User $user){

        if($user === null)
            throw $this->createNotFoundException('Unknown user ' . $user->getId());
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$user->getAgencyGroup())
            throw new AccessDeniedHttpException();
        $portalsRegistrationsList = $formView = null;
        if($user->hasRole('ROLE_SALESMAN')) {
            $em = $this->getDoctrine()->getManager();
            $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
            $portalsRegistrations = $portalRegistrationRepository->findBy(['group'=>$user->getAgencyGroup(),'isUsed' => true]);
            foreach ($portalsRegistrations as $portalsRegistration)
                if($portalsRegistration->getBroadcastPortal()->getCluster() == 'PAYANT')
                    $portalsRegistrationsList[$portalsRegistration->getBroadcastPortal()->getId()] = $portalsRegistration;


        }

        return [
            'user'                  => $user,
            'form'                  => $formView,
            'portalsRegistrations'  => $portalsRegistrationsList
        ];

    }

    /**
     * Delete an employee
     * @param User $user
     * @return RedirectResponse
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="agency_employee_delete")
     */
    public function deleteEmployeeAction(User $user){

        $cannot = false;
        if($user == $this->getUser()){
            $this->addFlash("info", $this->get('translator')->trans("cannot_delete_yourself"));
            $cannot = true;
        }

        $directorGroups = $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ? $this->getUser()->getGroup() : $this->getUser()->getAgencyGroup();

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$directorGroups)
            throw new AccessDeniedHttpException();

        if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || (!$cannot && sizeof($directorGroups) && $user->isEmployeeOf($directorGroups))){
            $em = $this->getDoctrine()->getManager();
            if($user->hasRole('ROLE_SALESMAN')) {
                $portalRegistrationRepository = $em->getRepository('AppBundle:Broadcast\\PortalRegistration');
                $commercialsPortalsRepository = $em->getRepository('AppBundle:Broadcast\\CommercialPortalLimit');
                /** @var \AppBundle\Entity\Broadcast\CommercialPortalLimit $commercialPortal */
                foreach ($user->getCommercialsPortal() as $commercialPortal) {
                    $portailRegistration = $portalRegistrationRepository->findOneBy(['broadcastPortal' => $commercialPortal->getPortal(), 'group'=>$user->getAgencyGroup()]);
                    $commercialsPortals  = $commercialsPortalsRepository->getCommercialWithGroupAndPortal($commercialPortal->getPortal(), $user);
                    $portailRegistration->setAdRest($portailRegistration->getAdLimit() - $commercialsPortals['usedLimit']);
                    $em->persist($portailRegistration);
                }
                $em->flush();
            }
            $group = $user->getGroup();
            $em->remove($user);
            $em->flush();

            $this->addFlash("success", $this->get('translator')->trans("agence.employee_deleted"));
            if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                return new RedirectResponse($this->generateUrl('group_agency_show',['remoteId' => $group->getRemoteId()]));
            }

            if($this->getUser()->getGroup()->hasRole('ROLE_GROUP')) {
                return new RedirectResponse($this->generateUrl('group_agency_show',['remoteId' => $group->getRemoteId()]));
            }
            return new RedirectResponse($this->generateUrl('agency_employees_list'));
        }

        return new RedirectResponse($this->generateUrl('agency_employees_list'));
    }

    /**
     *
     * @Route("/group/commercial-list/{remoteId}", options={"expose"=true}, name="group_commercials_list")
     * @Method({"GET","POST"})
     */
    public function getCommercialListAction(Request $request, Group $group)
    {
        $em = $this->getDoctrine()->getManager();
        $commercials = $em->getRepository('AppBundle:User')->getCommercialOnly($group);
        $return  = [];
        /** @var User $commercial */
        foreach ($commercials as $commercial) {
            $return[] = [
                'id'  => $commercial->getRemoteId(),
                'nom' => $commercial->getName()
            ];
        }
        return new JsonResponse($return);
    }

    /**
     *
     * @Route("/ad/commercial-list/{remoteId}", options={"expose"=true}, name="commercials_list_by_ad")
     * @Method({"GET","POST"})
     */
    public function getCommercialListByAdAction(Request $request, Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        $commercials = $em->getRepository('AppBundle:User')->getCommercialOnly($ad->getGroup());
        $return  = [];
        /** @var User $commercial */
        foreach ($commercials as $commercial) {
            $return[] = [
                'id'  => $commercial->getRemoteId(),
                'nom' => $commercial->getName()
            ];
        }
        return new JsonResponse($return);
    }

    /**
     *
     * @Route("/model/commercial-list/{remoteId}", options={"expose"=true}, name="commercials_list_by_housemodel")
     * @Method({"GET","POST"})
     */
    public function getCommercialListByHouseModelAction(Request $request, HouseModel $houseModel)
    {
        $em = $this->getDoctrine()->getManager();
        $commercials = $em->getRepository('AppBundle:User')->getCommercialOnly($houseModel->getGroup());
        $return  = [];
        /** @var User $commercial */
        foreach ($commercials as $commercial) {
            $return[] = [
                'id'  => $commercial->getRemoteId(),
                'nom' => $commercial->getName()
            ];
        }
        return new JsonResponse($return);
    }
}