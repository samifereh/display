<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class InvoicesController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class InvoicesController extends BaseController
{
    /**
     * @Route("/invoices/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_invoices" )
     * @Method({"GET"})
     */
    public function makeInvoiceListAction(Request $request, Group $group = null)
    {
        $user  = $this->getUser();
        if(!($group && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroups()))))
            $group = $user->getGroups();

        $datatable = $this->get('app.datatable.invoices');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        /** @var DatatableQuery $query */
        $query->addWhereAll(function($qb) use(&$group) {
            /** @var QueryBuilder $qb */
            $qb->leftJoin('orders.group','g');
            $qb->andWhere('orders.visible = 1');
            $qb ->andWhere('orders.group IN (:group)')
                ->setParameter('group', $group)
                ->getQuery();

            $qb->addOrderBy('orders.executionDate','DESC');
        });

        return $query->getResponse();
    }

    /**
     * @Route("/invoices_moderation/{group}", defaults={"group" = null}, options={"expose"=true}, name="api_moderation_invoices" )
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET"})
     */
    public function makeInvoiceModerationListAction(Request $request, Group $group = null)
    {
        $datatable = $this->get('app.datatable.moderation_invoices');
        $datatable->buildDatatable();
        $translator = $this->get("translator");
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) {
            $qb ->andWhere('orders.status = :status')
                ->setParameter('status', 'waiting_admin_validation')
                ->getQuery();
        });

        $function = function($data, DatatableQuery $query) use ($translator)
        {
            $count = count($data['data']);
            for($i=0 ; $i<$count ; $i++) {
                $data['data'][$i]['totalAmount'] = number_format($data['data'][$i]['totalAmount'],2,',',' ').'€';
                $data['data'][$i]['status'] = $translator->trans(
                    "invoices.".$data['data'][$i]['status'], [], 'commiti');
                $data['data'][$i]['payMethod'] = $translator->trans(
                    "invoices.method.".$data['data'][$i]['payMethod'], [], 'commiti');
            }
            return $data;
        };
        $query->addResponseCallback($function);
        return $query->getResponse();
    }
}