<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Document;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class DocumentsController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class DocumentsController extends BaseController
{
    /**
     * @return Response
     *
     * @Route("/document/delete/{id}", options={"expose"=true}, name="api_delete_document" )
     * @Method({"DELETE","GET"})
     */
    public function deleteDocumentAction(Document $document){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$document || ($document && $document->getGroup() &&  !$this->getUser()->isEmployeeOf($document->getGroup())))
                throw new AccessDeniedHttpException("doesnt_belong_to_group");

        $type = $document->getHouseModel() ? "housemodels" : 'ad';
        $documentService = $this->container->get('app.document.service');
        $documentService->remove("/uploads/".$type."/", $document);

        return new Response();
    }

    /**
     * @return Response
     *
     * @Route("/document/caption/{remoteId}/{caption}", options={"expose"=true}, name="api_title_document" )
     * @Method({"POST"})
     */
    public function documentCaptionAction(Request $request,Document $document, $caption){

        /** @var \AppBundle\Entity\Document $document */
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            if(!$document || ($document && !$this->getUser()->isEmployeeOf($document->getGroup())))
                throw new AccessDeniedHttpException("doesnt_belong_to_group");

        $em = $this->getDoctrine()->getManager();
        $document->setTitle($caption);
        $em->persist($document);
        $em->flush();

        return new Response();
    }

    /**
     * @param Brouillon $brouillon
     * @return Response
     *
     * @Route("/housemodel/documents/{brouillon}/{sessionId}",requirements={"brouillon" = "\d+"}, options={"expose"=true}, name="api_housemodel_documents_list" )
     * @Method({"GET"})
     * @Template("AppBundle:HouseModels:includes/documents.html.twig")
     */
    public function getBouillonDocumentsAction(Request $request,Brouillon $brouillon, $sessionId){

        if($request->getSession()->getId() != $sessionId)
            return new Response();
        $return = ['documents' => $brouillon->getDocuments()];
        if(!$request->get('review') || ($request->get('review') && $request->get('review') == 'true'))
            $return['review'] = true;
        return new JsonResponse($return);
    }
}