<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class PortalsController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class PortalsController extends BaseController
{
    /**
     * @return Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_DIRECTOR')")
     * @Route("/portal", options={"expose"=true}, name="api_portal_list" )
     * @Method({"GET"})
     */
    public function portalListAction(Request $request) {
        $datatable = $this->get('app.datatable.portal');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        return $query->getResponse();
    }

    /**
     * @Route("/diffusions/{data}",defaults={"data" = null},  options={"expose"=true}, name="api_diffusion_portals_list" )
     * @Method({"GET"})
     */
    public function DiffusionPortalsAction(Request $request, $data = null)
    {
        parse_str($data,$filters);
        $group = !empty($filters['group']) ? $this->getDoctrine()->getRepository('AppBundle:Group')->find($filters['group']) : $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.diffusion_portals');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);

        /** @var DatatableQuery $query */
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use($filters, $request, $group) {

            /** @var QueryBuilder $qb */
            $qb->leftJoin('broadcast_portal.broadcastPortalRegistrations','b');

            $qb->leftJoin('b.group','groups');
            if(!empty($filters['typeDiffusion'])) {
                $conditions = $qb->expr()->andX();
                foreach ($filters['typeDiffusion'] as $key => $typeDiffusion) {
                    $condition = 'broadcast_portal.typeDiffusion LIKE :type_'.$key;
                    $conditions->add($condition);
                    $qb->setParameter('type_'.$key,"%$typeDiffusion%");
                }
                $qb ->andWhere($conditions)
                    ->getQuery();
            }
            if(!empty($filters['categories'])) {
                $conditions = $qb->expr()->andX();
                foreach ($filters['categories'] as $key => $categorie) {
                    $condition = 'broadcast_portal.categories LIKE :cat_'.$key;
                    $conditions->add($condition);
                    $qb->setParameter('cat_'.$key,"%$categorie%");
                }
                $qb ->andWhere($conditions)
                    ->getQuery();
            }

            if(!empty($filters['cluster'])) {
                $qb ->andWhere('broadcast_portal.cluster = :cluster')
                    ->setParameter('cluster',$filters['cluster'])
                    ->getQuery();
            }

            if(!empty($filters['zoneGeographique'])) {
                $qb ->andWhere('broadcast_portal.zoneGeographique = :zoneGeographique')
                    ->setParameter('zoneGeographique',$filters['zoneGeographique'])
                    ->getQuery();
            }

            if(!empty($filters['bigGroups']) && !empty($filters['group'])) {
                $qb->leftJoin('b.group','bg');
                $qb ->andWhere('bg.id = :groupId')
                    ->distinct(true)
                    ->setParameter('groupId',$filters['group'])
                    ->getQuery();
            }

            if(isset($filters['isEnabled'])) {
                $qb->andWhere('b.isEnabled = 1')
                    ->getQuery();
            }
            $commercial = null;
            $parentGroups = null;
            $user = $this->getUser();
            if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                $group = null;
                if (!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                    $group = $user->getGroup();
                    if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                        $commercial = $user;
                    }
                } else {
                    if(count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY')) {
                        $group =  $user->getGroup();
                    } else {
                        $parentGroups =   $user->getGroups();
                    }
                }

                if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                    $qb->andWhere($qb->expr()->in('b.group', ':group'))
                        ->setParameter('group', $group);
                } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                    $parentGroups = $filters['group'];
                    $group = null;
                }
            }

            if($group) {
                $qb
                    ->andWhere('b.group IN (:group)')
                    ->setParameter('group',$group)
                    ->getQuery();
            } elseif($parentGroups or !empty($filters['bigGroups'])) {
                $groups = [];
                if(!empty($filters['bigGroups'])){
                    $qb
                        ->andWhere('groups.parentGroup IN (:group)')
                        ->setParameter('group',$filters['bigGroups'])
                        ->distinct(true)
                        ->getQuery();
                }else {
                    foreach ($parentGroups as $pg) {
                        foreach ($pg->getAgencyGroups() as $g) {
                            $groups[] = $g;
                        }
                    }
                    $qb
                        ->andWhere('groups.id IN (:group)')
                        ->andWhere('b.group IN (:group)')
                        ->setParameter('group',$groups)
                        ->distinct(true)
                        ->getQuery();
                }


            } else {

                $qb
                    ->andWhere('b.group IN (:group)')
                    ->setParameter('group',null)
                    ->getQuery();
            }
        });


        return $query->getResponse();
    }

    /**
     * @Route("/portails/waiting/{data}", defaults={"data" = null},  options={"expose"=true}, name="api_portals_waiting" )
     * @Method({"GET"})
     */
    public function PortalsWaitingAction(Request $request, $data = null)
    {
        parse_str($data,$filters);

        $group = !empty($filters['group']) ?
                $this->getDoctrine()->getRepository('AppBundle:Group')->find($filters['group']) :
            $this->getCurrentGroup($request);
        if(empty($group)){
            /** @var User $user */
            $user = $this->getUser();
            // get user groups (agencies)
            $group = $user->getAgencies();

        }
        $datatable = $this->get('app.datatable.portals_waiting');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use( $group, $filters) {
            /** @var QueryBuilder $qb */

            $qb->where("broadcast_portal_registration.isEnabled = 0");
            if(isset($filters['status'])){
                $qb->andWhere('broadcast_portal_registration.status = :status')
                    ->setParameter('status', $filters['status']);
            }

//            if(isset($filters['agency'])){
//                $qb->innerJoin("broadcast_portal_registration.group", "g")
//                    ->andWhere('g.agency= :agency')
//                    ->setParameter('agency', $filters['agency']);
//            }

            if(!empty($filters['typeDiffusion']) || !empty($filters['categories']) ||
                !empty($filters['categories']) || !empty($filters['cluster'])  ||
                !empty($filters['zoneGeographique'])|| !empty($filters['group']) ||
                $group != "all"

            )
            {
                $qb->innerJoin("broadcast_portal_registration.broadcastPortal", "portal");
                if (!empty($filters['typeDiffusion']) ) {
                    $conditions = $qb->expr()->andX();
                    foreach ($filters['typeDiffusion'] as $key => $typeDiffusion) {
                        $condition = 'portal.typeDiffusion LIKE :type_' . $key;
                        $conditions->add($condition);
                        $qb->setParameter('type_' . $key, "%$typeDiffusion%");
                    }
                    $qb->andWhere($conditions);
                }
                if (!empty($filters['categories'])) {
                    $conditions = $qb->expr()->andX();
                    foreach ($filters['categories'] as $key => $categorie) {
                        $condition = 'portal.categories LIKE :cat_' . $key;
                        $conditions->add($condition);
                        $qb->setParameter('cat_' . $key, "%$categorie%");
                    }
                    $qb->andWhere($conditions);
                }

                if (!empty($filters['cluster'])) {
                    $qb->andWhere('portal.cluster = :cluster')
                        ->setParameter('cluster', $filters['cluster']);
                }

                if (!empty($filters['zoneGeographique'])) {
                    $qb->andWhere('portal.zoneGeographique = :zoneGeographique')
                        ->setParameter('zoneGeographique', $filters['zoneGeographique']);
                }

                if(!empty($filters['bigGroups'])) {
                    $agencies = [];
                    if( !empty($filters['group'])) {
                        $agencies[] = $filters['group'];
                    }else{
                        $bigGroup = $this->getDoctrine()->getRepository('AppBundle:Group')->find($filters['bigGroups']);
                        $agencies = $bigGroup->getAgencyGroups()->toArray();
                    }
                    $qb->leftJoin('broadcast_portal_registration.group','bg');
                    $qb ->andWhere($qb->expr()->in('bg.id', ':groupId'))
                        ->distinct(true)
                        ->setParameter('groupId',$agencies)
                        ->getQuery();

                } elseif((!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && $group!= 'all') || !empty($filters['group'])) {
                    $qb->andWhere($qb->expr()->in('broadcast_portal_registration.group', ':group'));
                    $qb->setParameter('group', $group);
                }

            }

            $qb->getQuery();
        });


        return $query->getResponse();
    }
}