<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Group;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends BaseController
{
    /**
     * @Route("/users", options={"expose"=true}, name="api_users" )
     * @Method({"GET"})
     */
    public function makeUsersListAction(){

        $datatable = $this->get('app.datatable.user');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        return $query->getResponse();

    }

    /**
     * @Route("/groups", options={"expose"=true}, name="api_groups" )
     * @Method({"GET"})
     */
    public function makeGroupsListAction(){

        $datatable = $this->get('app.datatable.group');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        return $query->getResponse();
    }

    /**
     * @Route("/group/liste-agences/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_group_agency" )
     * @Method({"GET"})
     */
    public function groupAgencyListAction(Request $request, Group $group = null){

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING'))
                throw new AccessDeniedHttpException();

        $groups = $this->getUser()->getGroups();
        $datatable = $this->get('app.datatable.group_agency');
        $datatable->buildDatatable();
        /** @var DatatableQuery $query */
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use(&$groups) {
            /** @var QueryBuilder $qb */
            $qb->andWhere($qb->expr()->in('user_group.parentGroup',':groups'))
                ->setParameter('groups', $groups)
                ->getQuery();
        });

        return $query->getResponse();

    }

    /**
     * @Route("/agency/employees/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_agency_employees" )
     * @Method({"GET"})
     */
    public function makeEmployeesListAction(Group $group = null){
        /** @var \AppBundle\Entity\User $user */
        $user = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || ($this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') && $user->getGroup()->hasRole('ROLE_GROUP') && $group->isMemberOf($user->getGroup()))) ? $group : $user->getAgencyGroup();

        if($group){
            $groupId = $group->getId();
            $datatable = $this->get('app.datatable.agencyemployee');
            $datatable->buildDatatable(['group'=>$group]);

            /** @var DatatableQuery $query */
            $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
            $query->addWhereAll(function($qb) use(&$groupId) {
                /** @var QueryBuilder $qb */
                $qb ->innerJoin('user.groups', 'g')
                    ->andWhere('g.id = :group_id')
                    ->setParameter('group_id', $groupId)
                    ->getQuery();
            });
            return $query->getResponse();
        }
        return "";
    }

    /**
     * @return Response
     *
     * @Route("/cities/search", options={"expose"=true}, name="api_search_cities" )
     * @Method({"GET"})
     */
    public function searchCitiesAction(Request $request)
    {
        $return = [];
        if($request->get('q') && strlen($request->get('q'))>= 3) {
            $q = '"%'.$request->get('q').'%"';
            $statement = $this->getDoctrine()->getEntityManager()->getConnection()->prepare(
                'select vf.ville_id, vf.ville_nom, vf.ville_code_postal, vf.ville_nom_simple, CONCAT(vf.ville_nom, " ",vf.ville_code_postal) as searchable From villes_france vf WHERE CONCAT(vf.ville_nom, " ",vf.ville_code_postal) LIKE '.$q.' OR vf.ville_nom_simple LIKE '.$q.' ORDER BY ville_nom ASC'
            );
            $statement->execute();
            foreach ($statement->fetchAll() as $city) {
                if(strlen($city['ville_code_postal']) > 5) {
                    $cps = explode('-',$city['ville_code_postal']);
                    foreach($cps as $cp) {
                        $return[] = [
                            'id'    => $city['ville_id'],
                            'cp'    => $cp,
                            'name'  => $city['ville_nom'],
                            'text'  => $city['ville_nom'].' '.$cp.' <span class="hidden">'.$city['ville_nom_simple'].'</span>'
                        ];
                    }
                } else {
                    $return[] = [
                        'id'    => $city['ville_id'],
                        'cp'    => $city['ville_code_postal'],
                        'name'  => $city['ville_nom'],
                        'text'  => $city['ville_nom'].' '.$city['ville_code_postal'].' <span class="hidden">'.$city['ville_nom_simple'].'</span>'
                    ];
                }
            }
        }

        $response = new JsonResponse($return);

        return $response;
    }

}