<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class AdsController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class AdsController extends BaseController
{

    /**
     * @Route("/ads/{remoteId}/{data}", defaults={"remoteId" = null, "data" = null}, options={"expose"=true}, name="api_ads" )
     * @Method({"GET"})
     */
    public function makeAdsListAction(Request $request, Group $group = null, $data = null)
    {
        $user = $this->getUser();
        parse_str($data, $filters);
        $datatable = $this->get('app.datatable.ad');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);
        /** @var DatatableQuery $query */
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        $query->addWhereAll(function ($qb) use ($group, $user, $filters) {

            $qb ->andWhere($qb->expr()->isNull('ad.program'));

            if(!$this->isGranted('ROLE_BRANDLEADER')) {
                $commercial = $user;
                $qb ->andWhere($qb->expr()->in('ad.owner', ':commercial'))
                    ->andWhere('ad.published = 1')
                    ->setParameter('commercial', $commercial);
            } else {
                $commercial = null;
                $parentGroups = null;
                if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                    $group = null;
                    if (!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                        $group = $user->getGroup();
                        if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                            $commercial = $user;
                        }
                    } else {
                        $group = count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY') ? $user->getGroup() : $user->getGroups();
                    }

                    if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                        $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                            ->setParameter('group', $group);
                    } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                        $group = $filters['group'];
                    }
                }

                if($group instanceof Group) {
                    $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                        ->setParameter('group', $group);
                } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                    $groups = [];
                    foreach ($group as $pg) {
                        foreach ($pg->getAgencyGroups() as $g) {
                            $groups[] = $g;
                        }
                    }
                    $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                        ->setParameter('group', $groups);
                } elseif(!empty($filters['bigGroups']) && !$group) {
                    $qb->leftJoin('ad.group','parentGroup')
                        ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                        ->setParameter('group', $filters['bigGroups']);
                }

                if (isset($filters['commercial']) && !empty($filters['commercial']))
                    $commercial = $filters['commercial'];

                if ($commercial) {
                    $qb ->andWhere($qb->expr()->in('ad.owner', ':commercial'))
                        ->andWhere('ad.published = 1')
                        ->setParameter('commercial', $commercial);
                }
            }


            /** @var QueryBuilder $qb */
            if (!empty($filters['typeAd'])) {
                $qb->andWhere($qb->expr()->eq('ad.typeDiffusion', ':type'))
                    ->setParameter('type', $filters['typeAd'])
                    ->getQuery();
            }

            /**
             * akossentini
             * add zipcode filter
             */
            if (!empty($filters['ville'])) {
                $qb->andWhere($qb->expr()->in('ad.ville', $filters['ville']))
                    ->getQuery();
            }

            if (!empty($filters['dateRemonterMin'])) {
                $dateMin = new \DateTime($filters['dateRemonterMin']['date']);
                $qb->andWhere('ad.dateRemonter >= :dateRemonterMin')
                    ->setParameter('dateRemonterMin', $dateMin)
                    ->getQuery();
            }

            if (!empty($filters['dateRemonterMax'])) {
                $dateMax = new \DateTime($filters['dateRemonterMax']['date']);
                $qb->andWhere('ad.dateRemonter <= :dateRemonterMax')
                    ->setParameter('dateRemonterMax', $dateMax)
                    ->getQuery();
            }

            if (!empty($filters['prixMin'])) {
                $qb->andWhere('ad.prix >= :prixMin')
                    ->setParameter('prixMin', $filters['prixMin'])
                    ->getQuery();
            }
            if (!empty($filters['prixMax'])) {
                $qb->andWhere('ad.prix <= :prixMax')
                    ->setParameter('prixMax', $filters['prixMax'])
                    ->getQuery();
            }

            if (!empty($filters['etat'])) {
                $qb->andWhere($qb->expr()->in('ad.stat', ':stat'))
                    ->setParameter('stat', $filters['etat'])
                    ->getQuery();
            }

            if (!empty($filters['portalDiffusion'])) {
                $qb->leftJoin('ad.diffusion', 'd')
                   ->leftJoin('d.broadcastPortals', 'bp')
                    ->andWhere($qb->expr()->in('bp.id',':portals' ))
                    ->setParameter('portals', $filters['portalDiffusion'])
                    ->getQuery();
            }

            $qb->addOrderBy('ad.updatedAt', 'DESC');
        });

        return $query->getResponse();
    }

    /**
     * @Route("/moderation_ads/{id}/{data}", defaults={"id" = null, "data" = null}, options={"expose"=true}, name="api_ads_moderation" )
     * @Method({"GET"})
     */
    public function makeModerationAdsListAction(Request $request, Group $group = null, $data = null)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
            throw new AccessDeniedHttpException();
        }
        $user = $this->getUser();
        parse_str($data, $filters);

        $datatable = $this->get('app.datatable.ad_moderation');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use(&$commercial, $group, $user, $filters) {

            $commercial = null;
            $parentGroups = null;
            if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                $group = null;
                if (!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                    $group = $user->getGroup();
                    if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                        $commercial = $user;
                    }
                } else {
                    $group = count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY') ? $user->getGroup() : $user->getGroups();
                }

                if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                    $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                        ->setParameter('group', $group);
                } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                    $group = $filters['group'];
                }
            }

            if($group instanceof Group) {
                $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                    ->setParameter('group', $group);
            } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                $groups = [];
                foreach ($group as $pg) {
                    foreach ($pg->getAgencyGroups() as $g) {
                        $groups[] = $g;
                    }
                }
                $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                    ->setParameter('group', $groups);
            } elseif(!empty($filters['bigGroups']) && !$group) {
                $qb->leftJoin('ad.group','parentGroup')
                    ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                    ->setParameter('group', $filters['bigGroups']);
            }

            if (isset($filters['commercial']) && !empty($filters['commercial']))
                $commercial = $filters['commercial'];
            if ($commercial) {
                $qb ->andWhere($qb->expr()->in('ad.owner', ':commercial'))
                    ->setParameter('commercial', $commercial);
            }

            $qb ->andWhere('ad.published = 0')
                ->addOrderBy('ad.updatedAt', 'DESC');


        });
        return $query->getResponse();
    }
}