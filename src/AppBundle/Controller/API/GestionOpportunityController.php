<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Class GestionOpportunityController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class GestionOpportunityController extends BaseController
{
    /**
     * @Route("/contact_lists/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_contact_lists" )
     * @Method({"GET"})
     */
    public function contactListAction(Request $request, Group $group = null)
    {
        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );

        $datatable = $this->get('app.datatable.contact_lists');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                if($group == 'all') {
                    $qb ->andWhere('contacts_lists.group IN (:group) OR contacts_lists.group = :parentGroup')
                        ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                        ->setParameter('parentGroup', $this->getUser()->getGroup())
                        ->getQuery();
                } else {
                    $qb ->andWhere('contacts_lists.group = :group')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
                $qb->addOrderBy('contacts_lists.createdAt','DESC');
            });
        return $query->getResponse();
    }

    /**
     * @Route("/contacts/{data}", defaults={"data" = null}, options={"expose"=true}, name="api_contact" )
     * @ParamConverter("contactList", options={"mapping": {"contactList": "remoteId" }})
     * @Method({"GET"})
     */
    public function contactsListAction(Request $request, $data = null)
    {
        $user  = $this->getUser();
        parse_str($data, $filters);
        /** @var \AppBundle\Repository\GroupRepository $groupRepository */
        $groupRepository = $this->getDoctrine()->getRepository('AppBundle:Group');
        $group = !empty($filters['remoteId']) ? $groupRepository->findOneBy(['remoteId' => $filters['remoteId']]) : null;

        /** @var \AppBundle\Repository\GroupRepository $groupRepository */
        $contactListRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\ContactList');
        $contactList = !empty($filters['contactList']) ? $contactListRepository->findOneBy(['remoteId' => $filters['contactList']]) : null;

        $datatable = $this->get('app.datatable.contacts');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                /** @var QueryBuilder $qb */
                if($group == 'all') {
                    $qb ->andWhere('contacts.group IN (:group) OR contacts.group = :parentGroup')
                        ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                        ->setParameter('parentGroup', $this->getUser()->getGroup())
                        ->getQuery();
                } else {
                    $qb ->andWhere('contacts.group = :group')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
                $qb->addOrderBy('contacts.createdAt','DESC');
            });
        if($contactList) {
            $query->addWhereAll(function($qb) use(&$contactList) {
                $qb->leftJoin('contacts.group_contacts','m')
                    ->andWhere(':contactList = m.id')
                    ->setParameter('contactList', $contactList->getId())
                    ->getQuery();
            });
        }
        $query->addWhereAll(function($qb) use(&$contactList) {
            $qb->andWhere('contacts.type = :type')
                ->setParameter('type', Contact::CONTACT);

        });
        return $query->getResponse();
    }


    /**
     * @Route("/clients/{remoteId}/{contactList}", defaults={"remoteId" = null, "contactList" = null}, options={"expose"=true}, name="api_client" )
     * @ParamConverter("contactList", options={"mapping": {"contactList": "remoteId" }})
     * @Method({"GET"})
     */
    public function clientsListAction(Request $request, Group $group = null, ContactList $contactList = null)
    {
        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );

        $datatable = $this->get('app.datatable.clients');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                /** @var QueryBuilder $qb */
                if($group == 'all') {
                    $qb ->andWhere('contacts.group IN (:group) OR contacts.group = :parentGroup')
                        ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                        ->setParameter('parentGroup', $this->getUser()->getGroup())
                        ->getQuery();
                } else {
                    $qb ->andWhere('contacts.group = :group')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
                $qb->addOrderBy('contacts.createdAt','DESC');
            });
        if($contactList) {
            $query->addWhereAll(function($qb) use(&$contactList) {
                $qb->leftJoin('contacts.group_contacts','m')
                    ->andWhere(':contactList = m.id')
                    ->setParameter('contactList', $contactList->getId())
                    ->getQuery();
            });
        }
        $query->addWhereAll(function($qb) use(&$contactList) {
            $qb->andWhere('contacts.type = :type')
                ->setParameter('type', Contact::CLIENT);

        });
        return $query->getResponse();
    }

    /**
     * @Route("/compagnes/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_compagne" )
     * @Method({"GET"})
     */
    public function compagnesListAction(Request $request, Group $group = null, ContactList $contactList = null)
    {
        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );

        $datatable = $this->get('app.datatable.campagne');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                if($group == 'all') {
                    $qb ->andWhere('campagnes.group IN (:group) OR campagnes.group = :parentGroup')
                        ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                        ->setParameter('parentGroup', $this->getUser()->getGroup())
                        ->getQuery();
                } else {
                    $qb ->andWhere('campagnes.group = :group')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
                $qb->addOrderBy('campagnes.createdAt','DESC');
            });
        return $query->getResponse();
    }

    /**
     * @Route("/bugs/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_bugs_list" )
     * @Method({"GET"})
     */
    public function bugsListAction(Request $request, Group $group = null)
    {
        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );

        $datatable = $this->get('app.datatable.bugs');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                if($this->isGranted('ROLE_MARKETING') && count($this->getUser()->getGroups()) > 0) {
                    $groupsId = [];
                    /** @var \AppBundle\Entity\Group $gg */
                    foreach ($this->getUser()->getGroups() as $gg) {
                        foreach ($gg->getAgencyGroups() as $g) {
                            $groupsId[] = $g->getId();
                        }
                    }
                    $qb ->andWhere('bugs.group IN (:group)')
                        ->setParameter('group', $groupsId)
                        ->getQuery();
                } else {
                    if($group == 'all') {
                        $qb ->andWhere('bugs.group IN (:group) OR bugs.group = :parentGroup')
                            ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                            ->setParameter('parentGroup', $this->getUser()->getGroup())
                            ->getQuery();
                    } else {
                        $qb ->andWhere('bugs.group = :group')
                            ->setParameter('group', $group)
                            ->getQuery();
                    }
                }
                $qb->addOrderBy('bugs.createdAt','DESC');
            });
        return $query->getResponse();
    }

    /**
     * @Route("/faq", options={"expose"=true}, name="api_faq_list" )
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET"})
     */
    public function faqsListAction(Request $request)
    {
        $datatable = $this->get('app.datatable.faq');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        return $query->getResponse();
    }

    /**
     * @Route("/demandes-contacts/{remoteId}/{contact}", defaults={"remoteId" = null}, options={"expose"=true}, name="api_demandes_contact" )
     * @ParamConverter("contact", options={"mapping": {"contact": "remoteId" }})
     * @Method({"GET"})
     */
    public function demandesContactsListAction(Request $request, Group $group = null, Contact $contact = null)
    {
        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );

        $datatable = $this->get('app.datatable.demandes_contact');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $query->addWhereAll(function($qb) use(&$group) {
                if($group == 'all') {
                    $qb ->andWhere('demandes_contact.group IN (:group) OR demandes_contact.group = :parentGroup')
                        ->setParameter('group', $this->getUser()->getGroup()->getAgencyGroups())
                        ->setParameter('parentGroup', $this->getUser()->getGroup())
                        ->getQuery();
                } else {
                    $qb ->andWhere('demandes_contact.group = :group')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
                $qb->addOrderBy('demandes_contact.date','DESC');
            });
        if($contact) {
            $query->addWhereAll(function($qb) use(&$contact) {
                $qb->andWhere('contact = :contact')->setParameter('contact',$contact);
            });
        }
        return $query->getResponse();
    }

    /**
     * @Route("/contact_details/{id}", options={"expose"=true}, name="api_contact_details" )
     * @Method({"GET"})
     */
    public function ContactDetailsAction(Request $request, Contact $contact)
    {
        $user  = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            return new JsonResponse([]);
        return new JsonResponse(
            [
                'email'      => $contact->getEmail(),
                'nom'        => $contact->getNom(),
                'prenom'     => $contact->getPrenom(),
                'telephone'  => $contact->getTelephone(),
                'ville'      => $contact->getVille(),
                'codePostal' => $contact->getCodePostal(),
                'address'    => $contact->getAddress(),
                'mobile'     => $contact->getMobile(),
                'telephoneBureau'     => $contact->getTelephoneBureau()
            ]
        );

    }

    /**
     * @Route("/opportunity/{data}", defaults={"data" = null}, options={"expose"=true}, name="api_opportunity" )
     * @Method({"GET"})
     */
    public function opportunityListAction(Request $request, $data = null)
    {

        parse_str($data, $filters);
        /** @var \AppBundle\Repository\GroupRepository $groupRepository */
        $groupRepository = $this->getDoctrine()->getRepository('AppBundle:Group');
        $group = null;
        if(!empty($filters['group']))
            $group =     $groupRepository->find($filters['group']);

        $user  = $this->getUser();
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $group->isMemberOf($user->getGroup())) ? $group : ( $this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') ?  $this->getCurrentGroup($request) : $user->getAgencyGroup() );
        $type = null;


        $datatable = $this->get('app.datatable.opportunity');
        $datatable->buildDatatable();
        /** @var DatatableQuery $query */
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use($filters, &$group) {

            $user = $this->getUser();
            $commercial = null;
            $parentGroups = null;
            if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                $group = null;
                if (!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                    $group = $user->getGroup();
                    if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                        $commercial = $user;
                    }
                } else {
                    $group = count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY') ? $user->getGroup() : $user->getGroups();
                }

                if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                    $qb->andWhere($qb->expr()->in('ad.group', ':group'))
                        ->setParameter('group', $group);
                } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                    $group = $filters['group'];
                }
            }

            /** @var QueryBuilder $qb */
            $qb->leftJoin('opportunity.propositionDetails','p');

            if($group instanceof Group) {
                $qb->andWhere($qb->expr()->in('opportunity.opportunityGroup', ':group'))
                    ->setParameter('group', $group);
            } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                $groups = [];
                foreach ($group as $pg) {
                    foreach ($pg->getAgencyGroups() as $g) {
                        $groups[] = $g;
                    }
                }
                $qb->andWhere($qb->expr()->in('opportunity.opportunityGroup', ':group'))
                    ->setParameter('group', $groups);
            } elseif(!empty($filters['bigGroups']) && !$group) {
                $qb->leftJoin('opportunity.opportunityGroup','parentGroup')
                    ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                    ->setParameter('group', $filters['bigGroups']);
            }

            if (isset($filters['commercial']) && !empty($filters['commercial']))
                $commercial = $filters['commercial'];

            if ($commercial) {
                $qb ->andWhere($qb->expr()->in('opportunity.commercial', ':commercial'))
                    ->setParameter('commercial', $commercial);
            }

            if(isset($filters['contact']) && !empty($filters['contact'])) {
                $qb->leftJoin('opportunity.contact','c');
                $qb->andWhere('c.id = :contact')->setParameter('contact',$filters['contact']);
            }
            if(isset($filters['type']) && $filters['type'] == 'list') {
                $qb->andWhere('opportunity.status IN (:status)')->setParameter('status',[Opportunity::WAITING,Opportunity::OPEN]);
            }


            if (isset($filters['dateStart']) && !empty($filters['dateStart'])) {
                $dateMin = new \DateTime($filters['dateStart']['date']);
                $qb->andWhere('p.dateStart >= :dateStart')
                    ->setParameter('dateStart', $dateMin)
                    ->getQuery();
            }
            if (isset($filters['dateEnd']) && !empty($filters['dateEnd'])) {
                $dateMax = new \DateTime($filters['dateEnd']['date']);
                $qb->andWhere('p.dateEnd <= :dateEnd')
                    ->setParameter('dateEnd', $dateMax)
                    ->getQuery();
            }

            if (isset($filters['budgetTerrainFrom']) && !empty($filters['budgetTerrainFrom'])) {
                $qb->andWhere('p.budgetTerrainFrom >= :budgetTerrainFrom')
                    ->setParameter('budgetTerrainFrom', $filters['budgetTerrainFrom'])
                    ->getQuery();
            }
            if (isset($filters['budgetTerrainTo']) && !empty($filters['budgetTerrainTo'])) {
                $qb->andWhere('p.budgetTerrainTo <= :budgetTerrainTo')
                    ->setParameter('budgetTerrainTo', $filters['budgetTerrainTo'])
                    ->getQuery();
            }

            if (isset($filters['budgetMaisonFrom']) && !empty($filters['budgetMaisonFrom'])) {
                $qb->andWhere('p.budgetMaisonFrom >= :budgetMaisonFrom')
                    ->setParameter('budgetMaisonFrom', $filters['budgetMaisonFrom'])
                    ->getQuery();
            }
            if (isset($filters['budgetMaisonTo']) && !empty($filters['budgetMaisonTo'])) {
                $qb->andWhere('p.budgetMaisonTo <= :budgetMaisonTo')
                    ->setParameter('budgetMaisonTo', $filters['budgetMaisonTo'])
                    ->getQuery();
            }

            if (isset($filters['surfaceMaisonSouhaiterFrom']) && !empty($filters['surfaceMaisonSouhaiterFrom'])) {
                $qb->andWhere('p.surfaceMaisonSouhaiterFrom >= :surfaceMaisonSouhaiterFrom')
                    ->setParameter('surfaceMaisonSouhaiterFrom', $filters['surfaceMaisonSouhaiterFrom'])
                    ->getQuery();
            }
            if (isset($filters['surfaceMaisonSouhaiterTo']) && !empty($filters['surfaceMaisonSouhaiterTo'])) {
                $qb->andWhere('p.surfaceMaisonSouhaiterTo <= :surfaceMaisonSouhaiterTo')
                    ->setParameter('surfaceMaisonSouhaiterTo', $filters['surfaceMaisonSouhaiterTo'])
                    ->getQuery();
            }

            if (isset($filters['nombreChambreFrom']) && !empty($filters['nombreChambreFrom'])) {
                $qb->andWhere('p.nombreChambreFrom >= :nombreChambreFrom')
                    ->setParameter('nombreChambreFrom', $filters['nombreChambreFrom'])
                    ->getQuery();
            }
            if (isset($filters['nombreChambreTo']) && !empty($filters['nombreChambreTo'])) {
                $qb->andWhere('p.nombreChambreTo <= :nombreChambreTo')
                    ->setParameter('nombreChambreTo', $filters['nombreChambreTo'])
                    ->getQuery();
            }

            if (isset($filters['budgetTotalFrom']) && !empty($filters['budgetTotalFrom'])) {
                $qb->andWhere('p.budgetTotalFrom >= :budgetTotalFrom')
                    ->setParameter('budgetTotalFrom', $filters['budgetTotalFrom'])
                    ->getQuery();
            }
            if (isset($filters['budgetTotalTo']) && !empty($filters['budgetTotalTo'])) {
                $qb->andWhere('p.budgetTotalTo <= :budgetTotalTo')
                    ->setParameter('budgetTotalTo', $filters['budgetTotalTo'])
                    ->getQuery();
            }


            if (isset($filters['compromisTerrain']) && !empty($filters['compromisTerrain'])) {
                $qb->andWhere('p.compromisTerrain = 1')->getQuery();
            }
            if (isset($filters['contratConstruction']) && !empty($filters['contratConstruction'])) {
                $qb->andWhere('p.contratConstruction = 1')->getQuery();
            }
            if (isset($filters['permisConstruireDeposer']) && !empty($filters['permisConstruireDeposer'])) {
                $qb->andWhere('p.permisConstruireDeposer = 1')->getQuery();
            }
            if (isset($filters['possessionTerrain']) && !empty($filters['possessionTerrain'])) {
                $qb->andWhere('p.possessionTerrain = :posession')->setParameter('posession',(bool)$filters['possessionTerrain'])->getQuery();
            }

            if (isset($filters['lieuAlentour']) && !empty($filters['lieuAlentour'])) {
                $qb->andWhere('p.lieuAlentour <= :lieuAlentour')
                    ->setParameter('lieuAlentour',$filters['lieuAlentour'])
                    ->getQuery();
            }

            if (isset($filters['apportPersonnel']) && !empty($filters['apportPersonnel'])) {
                $qb->andWhere('p.apportPersonnel <= :apportPersonnel')
                    ->setParameter('apportPersonnel',$filters['apportPersonnel'])
                    ->getQuery();
            }

            if (isset($filters['lieu']) && !empty($filters['lieu'])) {
                $qb->andWhere('p.lieu LIKE :lieu')
                    ->setParameter('lieu','%'.$filters['lieu'].'%')
                    ->getQuery();
            }

            $qb->addOrderBy('opportunity.createdAt','DESC');
        });
        return $query->getResponse();
    }

    ####### Agenda #######
    /**
     * @return Response
     *
     * @Route("/events/commercial/{remoteId}", options={"expose"=true}, name="api_events_by_commercial" )
     * @Method({"GET"})
     */
    public function agendaEventsByCommercialAction(Request $request, User $user)
    {
        if(!$this->getUser()->isEmployeeOf($user->getGroup()) && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedHttpException();

        $return     = [];
        $events = $this->getDoctrine()->getRepository('AppBundle:Gestion\\AgendaEvent')->getEventsByUser($user, true);
        if($events) {
            /** @var AgendaEvent $event */
            foreach ($events as $event) {
                $return[] = [
                    'id'    => $event->getId(),
                    'title' => $event->getTitle(),
                    'start' => $event->getDateStart()->format('Y-m-d\TH:m'),
                    'end'   => $event->getDateEnd()->format('Y-m-d\TH:m'),
                    'color' => $event->getColor(),
                    'url'   => $this->generateUrl('agenda_event_view',['id' => $event->getId()]),
                ];
            }
        }
        $response = new JsonResponse($return);

        return $response;
    }

    /**
     * @return Response
     *
     * @Route("/events/oppotunity/{remoteId}", options={"expose"=true}, name="api_events_by_oportunity" )
     * @Method({"GET"})
     */
    public function agendaEventsByOpportunityAction(Request $request, Opportunity $opportunity)
    {
        $return     = [];
        if($opportunity->getEvents())
        /** @var AgendaEvent $event */
            foreach ($opportunity->getEvents() as $event) {
            $return[] = [
                'id'    => $event->getId(),
                'title' => $event->getTitle(),
                'start' => $event->getDateStart()->format('Y-m-d\TH:m'),
                'end'   => $event->getDateEnd()->format('Y-m-d\TH:m'),
                'url'   => $this->generateUrl('agenda_event_view',['id' => $event->getId()]),
            ];
        }
        $response = new JsonResponse($return);

        return $response;
    }

    /**
     * @return Response
     *
     * @Route("/events/update/{id}", options={"expose"=true}, name="api_update_event" )
     * @Method({"GET"})
     */
    public function agendaUpdateEventsAction(Request $request, AgendaEvent $event)
    {
        $user = $this->getUser();
        if( ($event->getOpportunity() && !$user->isEmployeeOf($event->getOpportunity()->getOpportunityGroup()) && !$this->isGranted('ROLE_ADMIN')) || $user != $event->getOwner())
            throw new AccessDeniedHttpException();

        $dateStart = $request->query->get('dateStart');
        $dateEnd   = $request->query->get('dateEnd');
        $em = $this->getDoctrine()->getManager();
        $event->setDateStart(new \DateTime($dateStart));
        $event->setDateEnd(new \DateTime($dateEnd));
        $em->persist($event);
        $em->flush();
        $response = new JsonResponse(['success' => true,'status' => 'demande_contact.event_updated']);

        return $response;
    }


}