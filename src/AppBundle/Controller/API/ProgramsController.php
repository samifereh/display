<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class ProgramsController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class ProgramsController extends BaseController
{
    /**
     * @Route("/program/{id}/{data}", defaults={"id" = null,"data" = null}, options={"expose"=true}, name="api_program" )
     * @Method({"GET"})
     */
    public function makeProgramListAction(Request $request, Group $group = null, $data = null)
    {
        /** @var \AppBundle\Entity\User $user */
        $user  = $this->getUser();
        parse_str($data, $filters);
        $datatable = $this->get('app.datatable.program');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        $query->addWhereAll(function($qb) use($group, $user, $filters) {
            $commercial   = null;
            $parentGroups = null;

            if(
                $this->getAuthorizationChecker()->isGranted('ROLE_SALESMAN') &&
                !$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') &&
                !$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')
            ) {
                $commercial = $user;
            }
            if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                $group        = null;
                if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                    $group = $user->getGroup();
                    if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                        $commercial = $user;
                    }
                } else {
                    if(count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY')) {
                        $group = $user->getGroup();
                    } else {
                        $group = $user->getGroups();
                    }
                }

                if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                    $qb->andWhere($qb->expr()->in('program.group', ':group'))
                        ->setParameter('group', $group);
                } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                    $group = $filters['group'];
                }
            }
            if($group instanceof Group) {
                $qb->andWhere($qb->expr()->in('program.group', ':group'))
                    ->setParameter('group', $group);
            } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                $groups = [];
                foreach ($group as $pg) {
                    foreach ($pg->getAgencyGroups() as $g) {
                        $groups[] = $g;
                    }
                }
                $qb->andWhere($qb->expr()->in('program.group', ':group'))
                    ->setParameter('group', $groups);
            } elseif(!empty($filters['bigGroups']) && !$group) {
                $qb->leftJoin('program.group','parentGroup')
                    ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                    ->setParameter('group', $filters['bigGroups']);
            }

            if (isset($filters['commercial']) && !empty($filters['commercial']))
                $commercial = $filters['commercial'];

            if($commercial) {
                $qb->andWhere($qb->expr()->in('program.owner', ':commercial'))
                    ->setParameter('commercial', $commercial);
            }
            
        });
        $query->addWhereAll(function($qb) {
            $qb->addOrderBy('program.updatedAt','DESC');
        });
        return $query->getResponse();
    }

    /**
     * @Route("/program_ads/{remoteId}", options={"expose"=true}, name="api_ads_by_program" )
     * @Method({"GET"})
     */
    public function makeAdsListByProgramAction(Request $request, Program $program)
    {
        $commercial = null;
        if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
            $commercial = $this->getUserService()->checkCommercialUser($request);
            if (!$commercial) {
                throw new AccessDeniedHttpException();
            }
        }

        $datatable = $this->get('app.datatable.ad_by_program');
        $datatable->buildDatatable(['program' => $program]);
        /** @var DatatableQuery $query */
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function ($qb) use (&$program) {
            /** @var QueryBuilder $qb */
            $qb->andWhere('ad.program = :program')
                ->setParameter('program', $program)
                ->getQuery();
        });
        if ($commercial) {
            $query->addWhereAll(function ($qb) use (&$commercial) {
                /** @var QueryBuilder $qb */
                $qb->andWhere('ad.owner = :owner')
                    ->setParameter('owner', $commercial)
                    ->getQuery();
            });
        }
        $query->addWhereAll(function ($qb) {
            /** @var QueryBuilder $qb */
            $qb->addOrderBy('ad.updatedAt', 'DESC');
        });

        return $query->getResponse();
    }

    /**
     * @Route("/moderation_programs", options={"expose"=true}, name="api_programs_moderation" )
     * @Method({"GET"})
     */
    public function makeModerationProgramsListAction(Request $request)
    {
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
            throw new AccessDeniedHttpException();
        $user = $this->getUser();
        $group = $this->getCurrentGroup($request);

        $datatable = $this->get('app.datatable.program_moderation');
        $datatable->buildDatatable();
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use(&$commercial, $group, $user) {
            if($group == 'all') {
                if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                {
                    if($commercial) {
                        $qb
                            ->andWhere('program.published = 0')
                            ->andWhere('program.group IN (:group)')
                            ->setParameter('group', $commercial->getGroup())
                            ->getQuery();
                    }
                } else {
                    $qb ->andWhere('program.group IN (:group)')
                        ->andWhere('program.published = 0')
                        ->setParameter('group', $user->getGroup()->getAgencyGroups())
                        ->getQuery();
                }
            } else {
                $qb ->andWhere('program.group = :group')
                    ->andWhere('program.published = 0')
                    ->setParameter('group', $group)
                    ->getQuery();
            }
            $qb->addOrderBy('program.updatedAt','DESC');
        });
        return $query->getResponse();
    }

    /**
     * @Route("/mes-lots/{data}",defaults={"data" = null},  options={"expose"=true}, name="api_lots_list" )
     * @Method({"GET"})
     */
    public function MesLotsAction(Request $request, $data = null)
    {

        $user  = $this->getUser();
        $group = $this->getCurrentGroup($request);

        $commercial = null;
        if(!$user->hasRole('ROLE_SALEMAN') && !$this->isGranted('ROLE_ADMIN')) {
            $commercial = $this->getUserService()->checkCommercialUser($request,$group);
        }

        parse_str($data,$filters);
        $datatable = $this->get('app.datatable.lots');
        $datatable->buildDatatable();

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use($user, $group, $commercial, $filters, $request) {
            /** @var QueryBuilder $qb */
            $qb->andWhere($qb->expr()->isNotNull('ad.program'));
            $qb->andWhere($qb->expr()->isNotNull('ad.remoteId'));
            $qb->leftJoin('ad.program', 'p');

            if($commercial) {
                /** @var QueryBuilder $qb */
                $qb ->andWhere('ad.owner = :owner')
                    ->setParameter('owner', $commercial)
                    ->getQuery();
            } else {
                if(!$this->isGranted('ROLE_ADMIN') && $group == 'all') {
                    $groups = new ArrayCollection();
                    foreach ($user->getGroups() as $group)
                        foreach ($group->getAgencyGroups() as $agency)
                            $groups->add($agency);
                    $qb->andWhere('p.group IN (:group)')
                        ->setParameter('group', $groups)
                        ->getQuery();
                } else {
                    $qb->andWhere('p.group IN (:group)')
                        ->setParameter('group', $group)
                        ->getQuery();
                }
            }



            /** @var QueryBuilder $qb */
            if(!empty($filters['typeLot'])) {

                $type = [
                    'maison'      => Maison::class,
                    'appartement' => Appartement::class,
                    'terrain'     => Terrain::class
                ];
                $qb ->leftJoin('ad.bienImmobilier','bien')
                    ->andWhere($qb->expr()->isInstanceOf('bien',$type[$filters['typeLot']]))
                    ->getQuery();
            }

            /**
             * akossentini
             * add zipcode filter
             */
            if(!empty($filters['zipcode'])) {
                $qb->andWhere($qb->expr()->in('ad.codePostal',$filters['zipcode']))
                    ->getQuery();
            }

            if(!empty($filters['dateRemonter'])) {
                $qb ->andWhere('ad.updatedAt >= :dateRemonter')
                    ->setParameter('dateRemonter',$filters['dateRemonter']['date'])
                    ->getQuery();
            }

            if(!empty($filters['prixMax'])) {
                $qb ->andWhere('ad.prix >= :prixMax')
                    ->setParameter('prixMax',$filters['prixMax'])
                    ->getQuery();
            }

            if(!empty($filters['etat'])) {
                $qb ->andWhere($qb->expr()->in('ad.stat',':stat' ))
                    ->setParameter('stat',$filters['etat'])
                    ->getQuery();
            }
            if(!empty($filters['portalDiffusion'])) {
                $qb->leftJoin('ad.diffusion', 'd')
                    ->leftJoin('d.broadcastPortals', 'bp')
                    ->andWhere($qb->expr()->in('bp.id',':portals' ))
                    ->setParameter('portals', $filters['portalDiffusion'])
                    ->getQuery();
            }

            $qb->addOrderBy('ad.updatedAt', 'DESC');
        });

        return $query->getResponse();
    }
}