<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class AutorizedUserController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class AutorizedUserController extends BaseController
{

    /**
     * @Route("/group-authorization/{data}", defaults={"data"=null}, options={"expose"=true}, name="api_authorized_users" )
     * @Method({"GET"})
     */
    public function authorizationListAction(Request $request, $data = null)
    {
        parse_str($data, $filters);
        $group = null;
        if(isset($filters['group']) && !empty($filters['group'])) {
            $group = $this->getDoctrine()->getRepository('AppBundle:Group')->findOneBy(['remoteId' => $filters['group']]);
        }

        $datatable = $this->get('app.datatable.autorized_user');
        $datatable->buildDatatable(['group' => $group]);
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        /** @var DatatableQuery $query */
        $query->addWhereAll(function($qb) use($filters, $group) {
            /** @var QueryBuilder $qb */
            $qb ->andWhere('autorized_users.group = :group')
                ->setParameter('group', $group)
                ->andWhere('autorized_users.user IS NULL')
                ->getQuery();
            $qb->addOrderBy('autorized_users.startDate','DESC');
        });

        $function = function($data, DatatableQuery $query)
        {
            for($i=0;$i<count($data['data']);$i++) {
                $data['data'][$i]['role'] = $this->get('translator')->trans($data['data'][$i]['role'],[], 'commiti');
            }
            return $data;
        };
        $query->addResponseCallback($function);

        return $query->getResponse();
    }
}