<?php
namespace AppBundle\Controller\API;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdsSearchType;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\Flow\AdDuoFlow;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Sg\DatatablesBundle\Datatable\Data\DatatableQuery;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class ModelsController
 * @package AppBundle\Controller\API
 * @Route("/api")
 */
class ModelsController extends BaseController
{

    /**
     * Return the view of an agency
     * @param Group $agency
     * @return Response
     * @Route("/housemodels/moderation/{id}/{data}", defaults={"id" = null,"data" = null}, options={"expose"=true}, name="api_moderation_housemodels" )
     * @Method({"GET"})
     */
    public function makeHouseModelsModerationListAction(Request $request,Group $group = null, $data = null){
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER'))
            throw new AccessDeniedHttpException();
        /** @var \AppBundle\Entity\User $user */
        $user  = $this->getUser();
        parse_str($data, $filters);

        $datatable = $this->get('app.datatable.housemodel_moderation');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
        $query->addWhereAll(function($qb) use($group,$user, $filters) {

            $commercial   = null;
            $parentGroups = null;
            if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                $group        = null;
                if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                    $group = $user->getGroup();
                    if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                        $commercial = $user;
                    }
                } else {
                    if(count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY')) {
                        $group = $user->getGroup();
                    } else {
                        $group = $user->getGroups();
                    }
                }

                if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                    $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                        ->setParameter('group', $group);
                } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                    $group = $filters['group'];
                }
            }

            if($group instanceof Group) {
                $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                    ->setParameter('group', $group);
            } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                $groups = [];
                foreach ($group as $pg) {
                    foreach ($pg->getAgencyGroups() as $g) {
                        $groups[] = $g;
                    }
                }
                $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                    ->setParameter('group', $groups);
            } elseif(!empty($filters['bigGroups']) && !$group) {
                $qb->leftJoin('house_model.group','parentGroup')
                    ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                    ->setParameter('group', $filters['bigGroups']);
            }

            if (isset($filters['commercial']) && !empty($filters['commercial']))
                $commercial = $filters['commercial'];

            if($commercial) {
                $qb->andWhere($qb->expr()->in('house_model.owner', ':commercial'))
                    ->setParameter('commercial', $commercial);
            }

            $qb->andWhere('house_model.stat <> :validated')
                ->setParameter('validated', HouseModel::VALIDATE)
                ->addOrderBy('house_model.updatedAt','DESC')
                ->getQuery();
        });

        return $query->getResponse();
    }
    /**
     * Return the view of an agency
     * @param Group $agency
     * @return Response
     *
     * @Route("/housemodels/{id}/{data}", defaults={"id" = null,"data" = null}, options={"expose"=true}, name="api_housemodels" )
     * @Method({"GET"})
     */
    public function makeHouseModelsListAction(Request $request,Group $group = null, $data = null){

        /** @var \AppBundle\Entity\User $user */
        $user  = $this->getUser();
        parse_str($data, $filters);
        $Type = null;
        $status = null;

        $datatable = $this->get('app.datatable.housemodel');
        $datatable->buildDatatable(['group' => $group, 'data' => $filters]);

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        $query->addWhereAll(function ($qb) use ($group, $user, $filters) {
            /** @var QueryBuilder $qb **/


            if(!$this->isGranted('ROLE_BRANDLEADER')) {
                $commercial = $user;
                $qb->andWhere($qb->expr()->in('house_model.owner', ':commercial'))
                    ->setParameter('commercial', $commercial);
            } else {

                $commercial   = null;
                $parentGroups = null;
                if(!$group && empty($filters['bigGroups']) && empty($filters['group'])) {
                    $group        = null;
                    if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING')) {
                        $group = $user->getGroup();
                        if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                            $commercial = $user;
                        }
                    } else {
                        if(count($user->getGroups()) == 1 && $user->getGroup()->hasRole('ROLE_AGENCY')) {
                            $group = $user->getGroup();
                        } else {
                            $group = $user->getGroups();
                        }
                    }

                    if (count($group) == 0 && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
                        $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                            ->setParameter('group', $group);
                    } elseif (isset($filters['group']) && !empty($filters['group']) && !$group) {
                        $group = $filters['group'];
                    }
                }

                if($group instanceof Group) {
                    $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                        ->setParameter('group', $group);
                } elseif($group instanceof \Doctrine\ORM\PersistentCollection && count($group) > 0) {
                    $groups = [];
                    foreach ($group as $pg) {
                        foreach ($pg->getAgencyGroups() as $g) {
                            $groups[] = $g;
                        }
                    }
                    $qb->andWhere($qb->expr()->in('house_model.group', ':group'))
                        ->setParameter('group', $groups);
                } elseif(!empty($filters['bigGroups']) && !$group) {
                    $qb->leftJoin('house_model.group','parentGroup')
                        ->andWhere($qb->expr()->in('parentGroup.parentGroup', ':group'))
                        ->setParameter('group', $filters['bigGroups']);
                }

                if (isset($filters['commercial']) && !empty($filters['commercial']))
                    $commercial = $filters['commercial'];

                if($commercial) {
                    $qb->andWhere($qb->expr()->in('house_model.owner', ':commercial'))
                        ->setParameter('commercial', $commercial);
                }

                if (isset($filters['stat']) && !empty($filters['stat']))
                    $qb->andWhere($qb->expr()->eq('house_model.stat', ':status'))
                        ->setParameter('status', $filters['stat']);

                if (isset($filters['visibleByAll'])) {
                    $qb->andWhere($qb->expr()->eq('house_model.visibleByAll', ':visible'))
                        ->setParameter('visible', $filters['visibleByAll']);
                }

                // filters by join maison
                if(
                    (isset($filters['type']) && !empty($filters['type'])) ||
                    (isset($filters['MinSurface']) && !empty($filters['MinSurface'])) ||
                    (isset($filters['MaxSurface']) && !empty($filters['MaxSurface'])) ||
                    (isset($filters['nbPieces']))    ||
                    (isset($filters['nbChambres']))  ||
                    (isset($filters['nbSalleBain'])) ||
                    (isset($filters['siSalleEau']))  ||
                    (isset($filters['siParking']))   ||
                    (isset($filters['nbToilette']))  ||
                    (isset($filters['siSalleAManger']))
                )
                {
                    // add maison join
                    $qb->leftJoin('house_model.maison','H');

                    // type filter
                    if(isset($filters['type']) && !empty($filters['type'])) {
                        $qb->andWhere($qb->expr()->in('H.type', ':type'))
                            ->setParameter('type', $filters['type']);
                    }

                    // min surface filter
                    if(isset($filters['MinSurface']) && !empty($filters['MinSurface'])) {
                        $Min = (int)$filters['MinSurface'];
                        $qb->andWhere($qb->expr()->gte('H.surface' , ':minSurface'))
                        ->setParameter("minSurface", $Min);
                    }

                    // max surface filter
                    if(isset($filters['MaxSurface']) && !empty($filters['MaxSurface'])) {
                        $Max = (int)$filters['MaxSurface'];
                        $qb->andWhere($qb->expr()->lte('H.surface' , ':maxSurface'))
                            ->setParameter("maxSurface", $Max);
                    }

                    // pieces number filter
                    if(isset($filters['nbPieces']) && !empty($filters['nbPieces'])) {
                        $qb->andWhere($qb->expr()->eq('H.nbPieces' , ':NbPieces'))
                            ->setParameter("NbPieces", $filters['nbPieces']);
                    }

                    // rooms number filter
                    if(isset($filters['nbChambres']) && !empty($filters['nbChambres'])) {
                        $qb->andWhere($qb->expr()->eq('H.nbChambres' , ':NbChambres'))
                            ->setParameter("NbChambres", $filters['nbChambres']);
                    }

                    // bathrooms number filter
                    if(isset($filters['nbSalleBain']) && !empty($filters['nbSalleBain'])) {
                        $qb->andWhere($qb->expr()->eq('H.nbSalleBain' , ':NbSalleBain'))
                            ->setParameter("NbSalleBain", $filters['nbSalleBain']);
                    }

                    // SalleEau filter
                    if(isset($filters['siSalleEau']) && !empty($filters['siSalleEau'])) {
                        $qb->andWhere($qb->expr()->eq('H.siSalleEau' , ':SalleEau'))
                            ->setParameter("SalleEau", $filters['siSalleEau']);
                    }

                    // parking filter
                    if(isset($filters['siParking']) && !empty($filters['siParking'])) {
                        $qb->andWhere($qb->expr()->eq('H.siParking' , ':Parking'))
                            ->setParameter("Parking", $filters['siParking']);
                    }

                    // WC filter
                    if(isset($filters['nbToilette']) && !empty($filters['nbToilette'])) {
                        $qb->andWhere($qb->expr()->eq('H.nbToilette' , ':NbToilette'))
                            ->setParameter("NbToilette", $filters['nbToilette']);
                    }

                    // SalleAManger filter
                    if(isset($filters['siSalleAManger']) && !empty($filters['siSalleAManger'])) {
                        $qb->andWhere($qb->expr()->eq('H.siSalleAManger' , ':SalleAManger'))
                            ->setParameter("SalleAManger", $filters['siSalleAManger']);
                    }
                }
            }

            $qb->addOrderBy('house_model.updatedAt','DESC')
                ->getQuery();
        });
        return $query->getResponse();
    }




}