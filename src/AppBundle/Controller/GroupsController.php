<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AdRewrite;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\Payment\Order;
use AppBundle\Form\Type\Admin\GroupsPricingType;
use AppBundle\Form\Type\GroupsType;
use AppBundle\Form\Type\RewriteAdType;
use AppBundle\Service\FtpClient;
use AppBundle\Service\Payment;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;

use AppBundle\Entity\Group as Group;

/**
 * Class GroupsController
 * @package AppBundle\Controller
 * @Route("/admin/groupes")
 * @Security("has_role('ROLE_ADMIN')")
 */
class GroupsController extends BaseController
{

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/", name="groups_admin" )
     * @Method({"GET"})
     * @Template("AppBundle:Groups:admin_list.html.twig")
     **/
    public function adminListAction(){

        $datatable = $this->get('app.datatable.group');
        $datatable->buildDatatable();

        return ['datatable' => $datatable];

    }

    /**
     * Return the view of a group
     * @param Group $group
     * @return Response
     * @Template("AppBundle:Groups:admin_show.html.twig")
     * @Route("/voir/{remoteId}", options={"expose"=true}, name="groups_admin_show" )
     * @Method({"GET","POST"})
     **/
    public function adminShowAction(Request $request, Group $group){

        $em = $this->getDoctrine()->getManager();
        $houseModelCount = $em->getRepository('AppBundle:HouseModel')->getHousemodelsCountByGroup($group);
        $adCount = $em->getRepository('AppBundle:Ad')->getAdCountByGroup($group);
        $employeeCount = $em->getRepository('AppBundle:User')->getEmployeeCountByGroup($group);

        $employeeDatatable = $this->get('app.datatable.agencyemployee');
        $employeeDatatable->buildDatatable(['group'=>$group]);

        $adsDatatable = $houseModeleDatatable = null;
        if($group->hasRole('ROLE_AGENCY') && $group->hasAnnoncesOption()) {
            $adsDatatable = $this->get('app.datatable.ad');
            $adsDatatable->buildDatatable(['group' => $group]);

            $houseModeleDatatable = $this->get('app.datatable.housemodel');
            $houseModeleDatatable->buildDatatable(['group'=>$group]);
        }
        $groupAgenciesDatatable = null;
        if($group->hasRole('ROLE_GROUP')) {
            $groupAgenciesDatatable = $this->get('app.datatable.group_agency');
            $groupAgenciesDatatable->buildDatatable(['group'=>$group]);
        }

        $groupAdvantages = $em->getRepository('AppBundle:GroupAdvantage')->findCurrentGroupAdvantages($group,'agence');
        $invoicesDatatable = $this->get('app.datatable.invoices');
        $invoicesDatatable->buildDatatable(['group'=>$group]);

        $payedAdvantages = [];
        foreach ($groupAdvantages as $groupAdvantage)
            $payedAdvantages[]  = $groupAdvantage->getAdvantage();

        $adRewrite = $group->getAdRewrite() ? $group->getAdRewrite() : new AdRewrite();
        $formRewrite = $this->createForm(RewriteAdType::class,$adRewrite);
        $formRewrite->handleRequest($request);
        if ($formRewrite->isSubmitted() && $formRewrite->isValid()) {
            $group->setAdRewrite($adRewrite);
            $em->persist($group);
            $em->flush();
        }

        return [
            'group'                  => $group,
            'houseModelCount'        => $houseModelCount,
            'adCount'                => $adCount,
            'employeeCount'          => $employeeCount,
            'employeeDatatable'      => $employeeDatatable,
            'adsDatatable'           => $adsDatatable,
            'houseModeleDatatable'   => $houseModeleDatatable,
            'invoicesDatatable'      => $invoicesDatatable,
            'payedAdvantages'        => $payedAdvantages,
            'groupAgenciesDatatable' => $groupAgenciesDatatable,
            'formRewrite'            => $formRewrite->createView()
        ];
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     *
     * @Route("/ajouter/{remoteId}", defaults={"remoteId" = null}, name="groups_admin_add" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Groups:admin_add.html.twig")
     **/
    public function adminAddAction(Request $request,Group $parentGroup = null){
        $em = $this->getDoctrine()->getManager();
        $group = new Group($parentGroup) ;
        if($parentGroup) {
            $group->setParentGroup($parentGroup);

        }
        $group->setRoles(['ROLE_AGENCY']);
        $form = $this->createForm(GroupsType::class,$group,['isAdmin' => true]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \AppBundle\Entity\Group $group */
            $group->getAgency()->setName($group->getName());
            if($group->hasRole('ROLE_GROUP'))
                $group->setParentGroup(null);

            if($group->getAgency()->getAvatar()) {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($group->getAgency(), $group->getAgency()->getAvatar());
            }
            foreach ($group->getAdvantagesDiscount() as $discount) {
                if(!$discount->getPercent())
                    $group->removeAdvantagesDiscount($discount);
            }
            foreach ($group->getAdvantages() as $advantage) {
                if(!$advantage->getQuantite() && !$advantage->getDateEnd() && !$advantage->getDateBegin())
                    $group->removeAdvantage($advantage);
            }

            $slug = 'commiti_'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $group->getPrefix())));
            $password = $this->getPasswordGenerator()->generate(10);
            $group->setImportHost($this->getParameter('ftp_host'));
            $group->setImportPassword($password);
            $group->setImportUser($slug);
            $group->setImportFileName($slug.'.zip');
            FtpClient::addFTP($slug,$password);

            try {
                $alias = 'commiti'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '', $group->getPrefix()))).'@mestatimmo.fr';
                $result = $this->getOvh()->post(
                    sprintf('/email/domain/%s/account', 'mestatimmo.fr'),
                    [
                        'accountName' => $slug,
                        'password'    => $password
                    ]
                );
                if(isset($result['id'])){
                    $group->setEmailAlias($slug.'@mestatimmo.fr');
                    $group->setEmailAliasId($result['id']);
                }

                $this->getOvh()->post(
                    sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                    [
                        'from'       => $alias,
                        'localCopy'  => false,
                        'to'         => $group->getAgency()->getEmail()
                    ]
                );
            } catch( \Exception $e) {
            }

            $this->get("app.agency.helpers")->registerClosing($group);
            $group->setTrialStart(new \DateTime("now"));
            $group->setTrialEnd((new \DateTime('now'))->add(new \DateInterval('P1M')));
            $group->setPeriodStart(new \DateTime("now"));
            $group->setPeriodEnd((new \DateTime('now'))->add(new \DateInterval('P1M')));
            $em->persist($group);
            $em->flush();


            $authorizedRoles = $group->hasRole('ROLE_GROUP') ? Payment::PACK_GROUP : ($group->getParentGroup() ? Payment::PACK_GROUP_AGENCY : Payment::PACK_1);
            foreach ($authorizedRoles as $role => $userCount) {
                for($i = 0; $i < $userCount; $i++) {
                    $instance      = new AuthorizedUser();
                    $dateStart     = new \DateTime('today');
                    $dateEnd       = (new \DateTime('today'))->add(new \DateInterval('P1Y'));
                    $instance->setGroup($group)->setRole($role)->setStartDate($dateStart)->setEndDate($dateEnd);
                    $em->persist($instance);
                }
            }
            $em->flush();
            if($group->hasRole('ROLE_GROUP') || ($group->hasRole('ROLE_AGENCY') && !$group->getParentGroup())) {
                $order = new Order();
                $order->setTitle('Initialisation');
                $order->setStatus(Order::STATUS_WAITING_FOR_PAYMENT);
                $order->setExecutionDate(new \DateTime());
                $order->setHTTotalAmount(80/1.2);
                $order->setTotalAmount(80);
                $order->setDetails([
                    [
                        'title'       => $order->getTitle(),
                        'discount'    => 0,
                        'quantity'    => 1,
                        'HT'          => $order->getTotalAmount()/1.2,
                        'TTC'         => $order->getTotalAmount(),
                        'reference'   => "INIT".$group->getId(),
                    ]
                ]);
                $order->setGroup($group);
                $order->setVisible(true);
                $em->persist($order);
                $em->flush();
            }
            return $this->redirectToRoute('groups_admin_show',['remoteId'=>$group->getRemoteId()]);
        }
        return ['form'  => $form->createView()];
    }

    /**
     * @param Group $group
     * @param Request $request
     * @return array|RedirectResponse
     *
     * @Template("AppBundle:Groups:admin_edit.html.twig")
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="groups_admin_edit" )
     * @Method({"GET","POST"})
     **/
    public function adminEditAction(Request $request, Group $group){
        /** @var \AppBundle\Entity\Group $group */
        $em     = $this->getDoctrine()->getManager();
        $form   = $this->createForm(GroupsType::class,$group,['isAdmin' => true]);
        $avatar = ($group->getAgency() ? $group->getAgency()->getAvatar() : null);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            /** @var \AppBundle\Entity\Group $group */
            if ($form->isValid()) {
                foreach ($group->getAdvantagesDiscount() as $discount) {
                    if(!$discount->getPercent())
                        $group->removeAdvantagesDiscount($discount);
                        if($discount->getId()) $em->remove($discount);
                }
                foreach ($group->getAdvantages() as $advantage) {
                    if(!$advantage->getQuantite() && !$advantage->getDateEnd() && !$advantage->getDateBegin())
                        $group->removeAdvantage($advantage);
                        if($advantage->getId()) $em->remove($advantage);
                }
                if(null === $form->get('agency')->get("avatar")->getData())
                    $group->getAgency()->setAvatar($avatar);
                else {
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($group->getAgency(), $group->getAgency()->getAvatar());
                }

                if(!$group->getImportUser()) {
                    $slug = 'commiti_'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '_', $group->getPrefix())));
                    $password = $this->getPasswordGenerator()->generate(10);
                    $group->setImportHost($this->getParameter('ftp_host'));
                    $group->setImportPassword($password);
                    $group->setImportUser($slug);
                    $group->setImportFileName($slug.'.zip');
                    FtpClient::addFTP($slug,$password);
                }

                if(!$group->getEmailAlias() && $group->getAgency()->getEmail()) {
                    $alias = 'commiti'.strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '', $group->getPrefix()))).'@mestatimmo.fr';
                    try {
                        $result = $this->getOvh()->post(
                            sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                            [
                                'from'       => $alias,
                                'localCopy'  => false,
                                'to'         => 'contact@commiti-immo.fr'
                            ]
                        );
                        if(isset($result['id'])){
                            $group->setEmailAlias($alias);
                            $group->setEmailAliasId($result['id']);
                        }

                        $this->getOvh()->post(
                            sprintf('/email/domain/%s/redirection', 'mestatimmo.fr'),
                            [
                                'from'       => $alias,
                                'localCopy'  => false,
                                'to'         => $group->getAgency()->getEmail()
                            ]
                        );
                    } catch( \Exception $e) {
                    }
                }
                $this->get("app.agency.helpers")->registerClosing($group);
                $em->persist($group);
                $em->flush();
                $this->addFlash('success', 'group_success_modified');
                return $this->redirectToRoute('groups_admin_show',['remoteId'=>$group->getRemoteId()]);
            }
        }

        return [
            'form'               => $form->createView(),
            'group'              => $group
        ];
    }

    /**
     * @param Group $group
     * @return RedirectResponse
     *
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="groups_admin_delete" )
     * @Method({"GET"})
     **/
    public function adminDeleteAction(Group $group){

        $em = $this->getDoctrine()->getManager();
        if($group !== NULL){
            if($group->getAgency()){
                $em->getRepository("TrackingBundle:Phone")->deleteAgencyPhones($group->getAgency());
            }
            $em->remove($group);
            $em->flush();
        } else {
            $this->addFlash('error', 'group_not_found');
        }

        return new RedirectResponse($this->generateUrl('groups_admin'));
    }

    /**
     * @param Group $group
     * @return RedirectResponse
     *
     * @Route("/block/{remoteId}", options={"expose"=true}, name="groups_admin_block" )
     * @Method({"GET"})
     **/
    public function blockGroupAction(Group $group){

        $em = $this->getDoctrine()->getManager();
        if($group !== NULL){
            $group->setBlocked($group->getBlocked() ? false : true);
            $em->persist($group);
            $em->flush();
        } else {
            $this->addFlash('error', 'group_not_found');
        }

        return new RedirectResponse($this->generateUrl('groups_admin'));
    }

    /**
     * @param Group $group
     * @return array
     *
     * @Route("/authorization/{remoteId}", options={"expose"=true}, name="groups_admin_authorization" )
     * @Template("AppBundle:Groups:admin_autorized_user.html.twig")
     * @Method({"GET"})
     **/
    public function authorizationGroupAction(Group $group){

        $data = ['group' => $group->getRemoteId()];
        $datatable = $this->get('app.datatable.autorized_user');
        $datatable->buildDatatable([
            'data'  => $data,
            'group' => $group
        ]);

        return [
            'datatable' => $datatable
        ];
    }

    /**
     * @param Group $group
     * @return array
     *
     * @Route("/pricing/{remoteId}", options={"expose"=true}, name="groups_admin_pricing" )
     * @Template("AppBundle:Groups:admin_pricing.html.twig")
     * @Method({"GET","POST"})
     **/
    public function pricingGroupAction(Request $request, Group $group){

        $form = $this->createForm(GroupsPricingType::class,$group);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();
            $this->addFlash("success", "Prix enregistrer");
        }

        return [
            'form' => $form->createView()
        ];
    }
}