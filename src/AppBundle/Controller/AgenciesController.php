<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AdMentionLegals;
use AppBundle\Entity\AdRewrite;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\CommercialPortalLimit;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\GroupAdvantageDiscount;
use AppBundle\Form\Type\AgencyType;
use AppBundle\Form\Type\GroupAdvantagesDiscountType;
use AppBundle\Form\Type\GroupsType;
use AppBundle\Form\Type\RewriteAdType;
use AppBundle\Form\Type\UserModerationType;
use AppBundle\Form\Type\UserType;
use Doctrine\Common\Collections\Criteria;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Group as Group;
use AppBundle\Entity\User as User;

use AppBundle\Repository\UserRepository as UserRepository;

use FOS\UserBundle\Form\Type\RegistrationFormType as RegistrationFormType;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use TrackingBundle\Entity\Advantage;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;

/**
 * Class AgenciesController
 * @package AppBundle\Controller
 * @Route("/agence")
 */
class AgenciesController extends BaseController {

    /**
     * Load the main page of the agency
     * @return RedirectResponse|Response
     *
     * @Route("/voir", name="agency_my")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:my_agency.html.twig")
     */
    public function myAgencyAction(Request $request){
        /** @var \AppBundle\Entity\Group $group */
        $group = $this->getUser()->getAgencyGroup();
        $em = $this->getDoctrine()->getManager();
        $statCallRepo = $em->getRepository("TrackingBundle:Statistique\TrackingCallStatistique");
        $statistics = [];
        if(!$group){
            $this->addFlash('error', 'No agency found');
            return new RedirectResponse($this->generateUrl('homepage'));
        }
        $emptyTrackingAdvantage = [];
        // to do not show empty advantage
        // portal
        $maxToTrack = 0;
            /** @var PortalRegistration $portalRegistration */
            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration ){
                if(
                    $portalRegistration->getBroadcastPortal() &&
                    $portalRegistration->getBroadcastPortal()->getCluster() == "PAYANT" &&
                    $portalRegistration->getIsEnabled()
                ) {
                    $maxToTrack++;
                    if($portalRegistration->getTrackingPhone()) {
                        $sum = $statCallRepo->getPhoneStats($portalRegistration->getTrackingPhone());
                        $statistics["call"]["portal"][$portalRegistration->getId()] = $sum;
                    }
                }
            }
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-portal';

        // Annonce
        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidAdsCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-ad';

        // Program
        $maxToTrack = $em->getRepository("AppBundle:Program")->getValidProgramCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-program';

        // Lots
        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidLotsCountByGroup($group);
        if(!$maxToTrack)
            $emptyTrackingAdvantage[] = 'call-tracking-lot';

        // call tracking statistics
        if($group->getAgency() && $group->getAgency()->getTrackingPhone())
            $statistics["call"]["agency"] = $statCallRepo->getPhoneStats(
                $group->getAgency()->getTrackingPhone()
            );

        if($request->request->has("agency_distribution_submit")) {
            $result = $this->updateCommercialDistribution($request, $group);
            if($result['success']){
                $this->addFlash('success', $this->get('translator')->trans($result['message'], [], 'commiti'));
            }else
                $this->addFlash('error', $this->get('translator')->trans($result['error'], [], 'commiti'));

        }


        /**
         * tracking portal
         */
        $trackingRepository = $em->getRepository("TrackingBundle:Advantage");
        /** @var Advantage $trackingAdvantage */
        $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), "portal");

        $myTrackingAdvantages = $trackingRepository->findAgencyTrackingAdvantage($group->getAgency());


        $houseModelCount = $em->getRepository('AppBundle:HouseModel')->getHousemodelsCountByGroup($group);
        $adRepository = $em->getRepository('AppBundle:Ad');
        $adCount = $adRepository->getAdCountByGroup($group);
        $employeeCount = $em->getRepository('AppBundle:User')->getEmployeeCountByGroup($group);
        $commercials = $em->getRepository('AppBundle:User')->getCommercialOnly($group);
        $commercialMap = [];
        /**
         * construct commercialMap
         * @var User $commercial
         */


        foreach ($commercials as $commercial) {
            $portalLimit = [];
            /**@var CommercialPortalLimit $commercialsPortal **/
            foreach ($commercial->getCommercialsPortal() as $commercialsPortal) {
                if($commercialsPortal->getPortal()->getCluster() == 'PAYANT'){

                    $used = $adRepository->getOlderAdsByCommercialCount($commercial, $commercialsPortal->getPortal());

                    $portalLimit["portal_".$commercialsPortal->getPortal()->getId()] = [
                        "id"    => $commercialsPortal->getId(),
                        "limit" => $commercialsPortal->getLimit(),
                        "used"  => $used
                    ];
                }

            }
            $commercialMap[] = [
                "id"=> $commercial->getId(),
                "name"=> $commercial->getName(),
                "portals"=> $portalLimit,
            ];
        }


        $groupAdvantages = $em->getRepository('AppBundle:GroupAdvantage')->findCurrentGroupAdvantages($group,'agence');
        $payedAdvantages = [];
        foreach ($groupAdvantages as $groupAdvantage)
            $payedAdvantages[]  = $groupAdvantage->getAdvantage();

        $form = $this->createForm(GroupAdvantagesDiscountType::class,$group);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var GroupAdvantageDiscount $discount */
            foreach ($group->getAdvantagesDiscount() as $discount) {
                if(!$discount->getPercent())
                    $group->removeAdvantagesDiscount($discount);
            }
            $em->persist($group);
            $em->flush();

            $command = new ResolveCacheCommand();
            $command->setContainer($this->container);
            $input = new ArrayInput([
                'paths' => [ $group->getAgency()->getAvatar() ]
            ]);
            $output = new NullOutput();
            $command->run($input, $output);

            $this->addFlash('success', 'Les remises ont été affecter');
        }

        $adRewrite = $group->getAdRewrite() ? $group->getAdRewrite() : new AdRewrite();
        $formRewrite = $this->createForm(RewriteAdType::class,$adRewrite);
        $formRewrite->handleRequest($request);
        if ($formRewrite->isSubmitted() && $formRewrite->isValid()) {
            $group->setAdRewrite($adRewrite);
            $em->persist($group);
            $em->flush();
        }

        $employeeDatatable = $this->get('app.datatable.agencyemployee');
        $employeeDatatable->buildDatatable(['group' => $group]);

        $invoicesDatatable = $this->get('app.datatable.invoices');
        $invoicesDatatable->buildDatatable(['group'=>$group]);

        $adsDatatable = $houseModeleDatatable = null;
        if($group->hasRole('ROLE_AGENCY') && $group->hasAnnoncesOption()) {
            $adsDatatable = $this->get('app.datatable.ad');
            $adsDatatable->buildDatatable(['group' => $group]);

            $houseModeleDatatable = $this->get('app.datatable.housemodel');
            $houseModeleDatatable->buildDatatable(['group'=>$group]);
        }

        $adMentionLegals = $group->getAdMentionLegals() ? $group->getAdMentionLegals() : new AdMentionLegals();
        $formMention = $this->createForm($this->get("app.form.ad_mention_legals"),$adMentionLegals);
        $formMention->handleRequest($request);
        if ($formMention->isSubmitted() && $formMention->isValid()) {
            $group->setAdMentionLegals($adMentionLegals);
            $em->persist($group);
            $em->flush();
        }

        return [
            'group'             => $group,
            'groupAdvantages'   => $groupAdvantages,
            'payedAdvantages'   => $payedAdvantages,
            'form'              => $form->createView(),
            'houseModelCount'   => $houseModelCount,
            'adCount'           => $adCount,
            'employeeCount'     => $employeeCount,
            'employeeDatatable' => $employeeDatatable,
            'commercialMap'     => $commercialMap,
            'formRewrite'       => $formRewrite->createView(),
            'trackingAdvantage' => $trackingAdvantage,
            'myTrackingAdvantages' => $myTrackingAdvantages,
            'emptyTrackingAdvantage' => $emptyTrackingAdvantage,
            'statistics'        => $statistics,
            'employeeCount'     => $employeeCount,
            'employeeDatatable' => $employeeDatatable,
            'invoicesDatatable' => $invoicesDatatable,
            'adsDatatable'           => $adsDatatable,
            'houseModeleDatatable'   => $houseModeleDatatable,
            'formMention'            => $formMention->createView()
        ];
    }

    /**
     *
     * @Route("/editer", name="agency_my_edit")
     * @Security("has_role('ROLE_DIRECTOR')")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:edit_agency.html.twig")
     */
    public function myAgencyEditAction(Request $request){

        /** @var \AppBundle\Entity\Group $group */
        $group = $this->getUser()->getAgencyGroup();
        if(!$group){
            $this->addFlash('error', 'no_agency_or_more_than_one');
            return new RedirectResponse($this->generateUrl('homepage'));
        }
        /** @var \AppBundle\Entity\Agency $agency */
        $agency = $group->getAgency();
        $avatar = $agency->getAvatar();
        $voicemail = $agency->getVoicemail();
        $updateWannaspeakVoiceMail = false;
        $form = $this->createForm(AgencyType::class,$agency);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(null === $form->get("avatar")->getData())
                    $agency->setAvatar($avatar);
                else{
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($agency, $agency->getAvatar());
                }
                if($form->get("voicemail")->getData()){
                    $currentDir = $this->get('kernel')->getRootDir().'/..';
                    $newVoiceMail = $form->get("voicemail")->getData();
                    $newVoiceMailFileName = $agency->getSlug().".mp3";
                    $newVoiceMail->move($currentDir.'/web/uploads/voicemail/', $newVoiceMailFileName);
                    $voicemail = '/web/uploads/voicemail/'.$newVoiceMailFileName;
                    $agency->incrementVoicemailVersion();
                    $updateWannaspeakVoiceMail = true;
                }
                $agency->setVoicemail($voicemail);
                $em = $this->getDoctrine()->getManager();
                $em->persist($agency);
                $em->flush();

                $this->get("app.agency.helpers")->registerClosing(
                    $group, "closing", false, $updateWannaspeakVoiceMail
                );
                $command = new ResolveCacheCommand();
                $command->setContainer($this->container);
                $input = new ArrayInput([
                    'paths' => [ $agency->getAvatar() ]
                ]);
                $output = new NullOutput();
                $command->run($input, $output);

                $this->addFlash("success", $this->get('translator')->trans("agences.agency_modified", [], 'commiti'));
                return new RedirectResponse($this->generateUrl('agency_my'));
            }
        }
        return ['form'  => $form->createView()];
    }


    /**
     *
     * @Route("/editer/portails/{remoteId}", defaults={"remoteId" = null}, name="agency_edit_portals")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Agencies:edit_agency_portals.html.twig")
     */
    public function myAgencyEditPortalsAction(Request $request, Group $group = null){

        /** @var \AppBundle\Entity\Group $group */
        $group = !is_null($group) && ($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP') ) ? $group : $this->getUser()->getAgencyGroup();
        if(!$group){
            $this->addFlash('error', 'no_agency_or_more_than_one');
            return $this->redirectToRoute('homepage');
        }

        if(!$this->getAuthorizationChecker()->isGranted('ROLE_MARKETING') || !$group)
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $formRegistrations = $this->createForm($this->getGroupPortalRegistrationsFormType(),$group);
        if($request->getMethod() == 'POST') {
            $formRegistrations->handleRequest($request);
            if($formRegistrations->get('cancel')->isClicked()) {
                return new RedirectResponse($this->generateUrl('group_agency_show',['remoteId'=>$group->getRemoteId()]));
            } elseif($formRegistrations->get('submit')->isClicked()) {
                if ($formRegistrations->isValid()) {
                    $em->persist($group);
                    $em->flush();
                    $this->addFlash("success", "Portails affecter avec success");
                    if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') || $this->getUser()->getGroup()->hasRole('ROLE_GROUP'))
                        return $this->redirectToRoute('group_agency_show',['remoteId'=>$group->getRemoteId()]);


                    if($group->getParentGroup())
                        return $this->redirectToRoute('group_agencies_list');
                    return new RedirectResponse($this->generateUrl('agency_my'));
                }
            }
        }

        return ['formRegistrations'  => $formRegistrations->createView(), "group" => $group];
    }


    /**
     *
     * @Route("/update/tracking", name="agency_update_portal_tracking")
     * @Method({"GET","POST"})
     * @return RedirectResponse|Response
     */
    public function updateTrackingAction(Request $request){

        $group = $this->getUser()->getAgencyGroup();
        $em = $this->getDoctrine()->getManager();
        if(!$group){
            $this->addFlash('error', 'No agency found');
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        /**
         * tracking portal
         */
        $trackingRepository = $em->getRepository("TrackingBundle:Advantage");
        /** @var Advantage $trackingAdvantage */
        $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($group->getAgency(), "portal");

        if(
            $trackingAdvantage &&
            $request->request->has("form_tracking_submit") &&
            $request->request->has("portalRegistrationTracking"))
        {
            $portalRegistrationRepository = $em->getRepository("AppBundle:Broadcast\PortalRegistration");
            $trackingOK = $trackingAdvantage->getPhones()->count();
            foreach($request->request->get("portalRegistrationTracking") as $portalRegistrationId)
            {
                if($trackingOK == $trackingAdvantage->getAmount())
                    break;

                $portalRegistration = $portalRegistrationRepository->find($portalRegistrationId);

                // create if expire or not exist
                if(
                    $this->get("app.tracking.portal.helpers")->getAvailableNumber($portalRegistration,$trackingAdvantage)
                ){
                    $trackingOK++;
                }

            }
            if($trackingOK ){
                $this->addFlash('success', 'Les numéros ont été affecter');
            }
        }
        return $this->redirect($this->generateUrl('agency_my')."#tab-portail");
    }

}