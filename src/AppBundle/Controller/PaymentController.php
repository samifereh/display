<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\AdvantageOption;
use AppBundle\Entity\GroupAdvantage;
use AppBundle\Entity\Payment\AuthorizedAgency;
use AppBundle\Entity\Payment\AuthorizedUser;
use AppBundle\Entity\Broadcast\CommercialPortalLimit;
use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Payment\Order;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentDetails;
use AppBundle\Form\Type\Broadcast\BroadcastPortalRegistrationType;
use AppBundle\Form\Type\GroupsType;
use AppBundle\Form\Type\Payment\PaymentType;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\Model\DetailsAggregateInterface;
use Payum\Core\Request\GetHumanStatus;
use Payum\Core\Request\Sync;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Group as Group;

use AppBundle\Repository\UserRepository as UserRepository;
use Symfony\Component\Validator\Constraints\Range;

use Payum\Core\Payum;
use Payum\Paypal\ExpressCheckout\Nvp\Api;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;

use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\Capture;
use Payum\Core\Security\SensitiveValue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PaymentController
 * @package AppBundle\Controller
 * @Route("/payment")
 */
class PaymentController extends BaseController
{

    const LEVELS = [
            ['title'=>'Basic plan'      ,'color'=>'warning',     'icon'=>'pe pe-7s-play big-icon' ],
            ['title'=>'Standard plan'   ,'color'=>'success',     'icon'=>'pe pe-7s-light big-icon' ],
            ['title'=>'Premium plan'    ,'color'=>'info',        'icon'=>'pe pe-7s-graph1 big-icon' ],
            ['title'=>'Best plan'       ,'color'=>'danger',       'icon'=>'pe pe-7s-medal big-icon' ],
            ['title'=>'Top plan'        ,'color'=>'danger',       'icon'=>'pe pe-7s-medal big-icon' ]
    ];

    /**
     * Return the view of the admin group
     * @return Response
     *
     * @Route("/{remoteId}/advantages/{slug}/{reference}/{trackingCount}",
     *     defaults={"reference" = null, "trackingCount" = null},  name="payment_advantage",
     *     requirements={"trackingCount": "\d+"}
     * )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Payment:index.html.twig")
     *
     * @ParamConverter("group", options={"mapping": {"remoteId": "remoteId"}})
     * @ParamConverter("advantage", options={"mapping": {"slug": "slug"}})
     * @ParamConverter("advantageOption",options={"mapping": {"reference": "reference"}})
     * test card number : 5105105105105100
     **/
    public function indexAction(Request $request, Group $group, Advantage $advantage,
        AdvantageOption $advantageOption = null, $trackingCount = null){

        $form = null;
        $em   = $this->getDoctrine()->getManager();
        $discount = $em->getRepository('AppBundle:GroupAdvantageDiscount')->findOneBy([ 'group' => $group, 'advantage' => $advantage]);
        $trackingParams = [];
        $isTrackingAdvantage = false;
        $maxToTrack = 0;
        if(stripos($advantage->getSlug(), "call-tracking-") !== false) {
            $isTrackingAdvantage = str_replace("call-tracking-", "", $advantage->getSlug());
            switch ($isTrackingAdvantage) {
                case "agency" : {
                    $trackingParams["options"] = $advantage->getOptions();
                    $trackingParams["trackingCount"] = 1;
                };break;
                case "portal" :
                case "ad" :
                case "program" :
                case "lot" : {

                    switch ($isTrackingAdvantage) {
                        case "portal" : {
                            /** @var PortalRegistration $portalRegistration */
                            foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration ){
                                if(
                                    $portalRegistration->getBroadcastPortal()->getCluster() == "PAYANT" &&
                                    $portalRegistration->getIsEnabled()
                                )
                                    $maxToTrack ++;
                            }
                        }break;
                        case "ad" : {$maxToTrack = $em->getRepository("AppBundle:Ad")->getValidAdsCountByGroup($group);}break;
                        case "program" : {$maxToTrack = $em->getRepository("AppBundle:Program")->getValidProgramCountByGroup($group);}break;
                        case "lot" : {$maxToTrack = $em->getRepository("AppBundle:Ad")->getValidLotsCountByGroup($group);}break;

                    }

                $totalToTracking = ($trackingCount > 0) ? min($trackingCount, $maxToTrack) : $maxToTrack;
                $trackingParams["trackingCount"] = $totalToTracking;
                $trackingParams["options"] =
                    $this->get("app.tracking.options.helpers")->getAdvantageOptions(
                        $advantage, $totalToTracking
                    );

                }

                if($totalToTracking == 0) {
                    $this->addFlash("warning", "Pas d'élèment à tracker");
                    return $this->redirectToRoute('agency_my');
                }
            }
        }
        if($advantageOption) {
            $check = $this->get("app.tracking.options.helpers")->getAdvantageOptions(
                $advantage, $trackingCount, $advantageOption
            );
            if(!$isTrackingAdvantage || $check) {


                $form = $this->createForm(new PaymentType());
                if ($request->getMethod() == 'POST') {
                    $form->handleRequest($request);
                    if ($form->isValid()) {
                        $data = $form->getData();
                        $order = new Order();
                        $order->setGroup($group->getParentGroup() ? $group->getParentGroup() : $group);
                        $order->setUser($this->getUser());
                        $order->setStatus(Order::STATUS_WAITING_FOR_PAYMENT);
                        $order->setPackId($advantage->getSlug());
                        $order->setExecutionDate(new \DateTime());
                        $order->setExpiryDate(new \DateTime("+".$advantageOption->getMonth()." month"));
                        $firstPrice = $advantageOption->getPrice();
                        if($isTrackingAdvantage && $trackingCount) {
                            $firstPrice = ($advantageOption->getPrice() / $advantageOption->getUnit()) * $trackingCount;
                            $firstPrice = money_format('%.2n',$firstPrice);
                        }

                        $price = $discount ?
                            $firstPrice - $firstPrice / $discount->getPercent() :
                            $firstPrice;
                        $order->setHTTotalAmount($firstPrice);
                        $order->setTotalAmount(money_format('%.2n',$price * 1.2));
                        $details = [
                            'title' => $advantage->getName(),
                            'discount' => $discount ? $discount->getPercent() : null,
                            'reference' => $advantageOption->getReference(),
                            'quantity' => $trackingCount ? $trackingCount : $advantageOption->getUnit(),
                            'HT' => $order->getHTTotalAmount(),
                            'TTC' => $order->getTotalAmount(),
                            'firstPrice' => $firstPrice
                        ];
                        $order->setDetails($details);

                        switch ($data['method']) {
                            case 'credit-card':
                                $gatewayName = 'paypal_pro_checkout_display';
                                $storage = $this->getPayum()->getStorage(PaymentDetails::class);
                                $payment = $storage->create();
                                $payment['ACCT'] = new SensitiveValue($data['card']);
                                $payment['CVV2'] = new SensitiveValue($data['cvv2']);
                                $payment['EXPDATE'] = new SensitiveValue($data['exp_date']->format('my'));
                                $payment['AMT'] = number_format($order->getTotalAmount(), 2);
                                $payment['CURRENCY'] = 'EUR';
                                $storage->update($payment);
                                $order->setPayMethod('credit-card');
                                $em->persist($order);
                                $em->flush();
                                $remoteId = 'immo-invoice-'.$order->getId();
                                $remoteId = md5($remoteId);
                                $order->setRemoteId($remoteId);

                                $em->persist($order);
                                $em->flush();
                                $captureToken = $this->getPayum()->getTokenFactory()->createCaptureToken(
                                    $gatewayName,
                                    $payment,
                                    'payment_done',
                                    [
                                        'agencyRemoteId' => $group->getRemoteId(),
                                        'remoteId' => $order->getRemoteId()
                                    ]
                                );
                                return $this->forward('PayumBundle:Capture:do', array(
                                    'payum_token' => $captureToken,
                                ));
                                break;

                            case 'paypal':
                                if ($form->get('paypal_pay')->isClicked()) {
                                    $gatewayName = 'paypal_express_checkout_and_doctrine_orm';
                                    $storage = $this->get('payum')->getStorage(PaymentDetails::class);
                                    $payment = $storage->create();
                                    $payment['PAYMENTREQUEST_0_CURRENCYCODE'] = 'EUR';
                                    $payment['PAYMENTREQUEST_0_AMT'] = $order->getTotalAmount();
                                    $storage->update($payment);
                                    $order->setPayMethod('paypal');
                                    $em->persist($order);
                                    $em->flush();
                                    $remoteId = 'immo-invoice-'.$order->getId();
                                    $remoteId = md5($remoteId);
                                    $order->setRemoteId($remoteId);

                                    $em->persist($order);
                                    $em->flush();
                                    $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                                        $gatewayName,
                                        $payment,
                                        'payment_done',
                                        [
                                            'agencyRemoteId' => $group->getRemoteId(),
                                            'remoteId' => $order->getRemoteId()
                                        ]
                                    );
                                    return $this->redirect($captureToken->getTargetUrl());
                                }
                                break;
                            case 'bank-wire':
                                /** @var \AppBundle\Repository\Payment\OrdersRepository $orderRepository */
                                $orderRepository = $this->getDoctrine()->getRepository('AppBundle:Payment\\Order');
                                $commandsCount = $orderRepository->getCommandsCountByGroup($group);
                                $order->setReferenceCommand($commandsCount+1);
                                $order->setPayMethod('bankwire');
                                $order->setStatus(Order::STATUS_WAITING_ADMIN_VALIDATION);
                                $order->setIsCommand(true);
                                $em->persist($order);
                                $em->flush();

                                $remoteId = 'immo-invoice-'.$order->getId();
                                $remoteId = md5($remoteId);
                                $order->setRemoteId($remoteId);
                                $em->persist($order);
                                $em->flush();

                                $this->addFlash("success",
                                    "L'achat par virement bancaire a été prise en compte, l'administrateur va consulter le transfert.");
                                //Send Confirm Mail
                                $this->get("app.mailer.manager")->sendOrderReceived(
                                    [
                                        "email" =>  $order->getUser()->getEmail(),
                                        "advantageOption" => $advantageOption,
                                        "order" =>  $order
                                    ]
                                );
                                return $this->redirectToRoute('bankwire_show', ['remoteId' => $remoteId]);
                                break;
                            case 'check-wire':
                                /** @var \AppBundle\Repository\Payment\OrdersRepository $orderRepository */
                                $orderRepository = $this->getDoctrine()->getRepository('AppBundle:Payment\\Order');
                                $commandsCount = $orderRepository->getCommandsCountByGroup($group);
                                $order->setReferenceCommand($commandsCount+1);
                                $order->setPayMethod('checkwire');
                                $order->setStatus(Order::STATUS_WAITING_ADMIN_VALIDATION);
                                $order->setIsCommand(true);
                                $em->persist($order);
                                $em->flush();

                                $remoteId = 'immo-invoice-'.$order->getId();
                                $remoteId = md5($remoteId);
                                $order->setRemoteId($remoteId);
                                $em->persist($order);
                                $em->flush();
                                //Send Confirm Mail
                                $this->get("app.mailer.manager")->sendOrderReceived(
                                    [
                                        "email" =>  $order->getUser()->getEmail(),
                                        "advantageOption" => $advantageOption,
                                        "order" =>  $order
                                    ]
                                );
                                $this->addFlash("success",
                                    "L'achat par chèque a été prise en compte, l'administrateur va consulter le transfert.");
                                return $this->redirectToRoute('checkwire_show', ['remoteId' => $remoteId]);
                                break;
                        }

                    }
                }
            } else if(!$check) {
                $this->addFlash("warning", "Nombre à tracker n'est pas conforme à l'offre");
                return $this->redirectToRoute('payment_advantage',[
                    'remoteId' => $group->getRemoteId(),
                    'slug' => $advantage->getSlug()
                ]);
            }
        }


        $trackingParams['isTrackingAdvantage']  = $isTrackingAdvantage;
        $trackingParams['maxToTrack']  = $maxToTrack;

        return [
            'form'      => $form ? $form->createView() : null,
            'advantage' => $advantage,
            'discount'  => $discount,
            'trackingParams'  => $trackingParams,
            'group'     => $group,
            'option'    => $advantageOption,
            'levels'    => self::LEVELS
        ];
    }

    /**
     * Return the view of the admin group
     * @return Response
     * @var Group $agency
     * @var \AppBundle\Entity\Payment\Order $order
     * @Method({"GET","POST"})
     * @Route("/done/{agencyRemoteId}/{remoteId}", name="payment_done" )
     * @ParamConverter("agency", options={"mapping": {"agencyRemoteId": "remoteId"}})
     * @ParamConverter("order", options={"mapping": {"remoteId": "remoteId"}})
     * test card number : 5105105105105100
     **/
    public function doneAction(Request $request, Group $agency, Order $order)
    {
        $token = $this->getPayum()->getHttpRequestVerifier()->verify($request);
        $gateway = $this->getPayum()->getGateway($token->getGatewayName());
        try {
            $gateway->execute(new Sync($token));
        } catch (RequestNotSupportedException $e) {}
        $gateway->execute($status = new GetHumanStatus($token));
        $refundToken = null;
        //$group = $this->getGroup($request);

        $em   = $this->getDoctrine()->getManager();

        if ($status->isCaptured()) {
            $refundToken = $this->getPayum()->getTokenFactory()->createRefundToken(
                $token->getGatewayName(),
                $status->getFirstModel(),
                $request->getUri()
            );
            $order->setStatus(Order::STATUS_PAID);
            $order->setRefundToken($refundToken->getTargetUrl());
            $order->setResultMessage($status->getValue());
            $order->setPayedDate(new \DateTime());
            $group = $order->getGroup();
            $repository = $this->getDoctrine()->getRepository('AppBundle:Configuration\\AdvantageOption');
            $advantageOption = $repository->findOneBy(['reference' => $order->getDetails()['reference']]);

            switch($order->getPackId()) {
                case 'subscribe':
                    $now = $datePeriodeStart = new \DateTime('now');
                    $monthCount = $order->getDetails()['monthCount'];
                    if(!is_null($group->getTrialEnd()) && $group->getTrialEnd() < $now)
                        $datePeriodeStart = $group->getTrialEnd();
                    elseif(!is_null($group->getPeriodEnd()) && $group->getPeriodEnd() < $now)
                        $datePeriodeStart = $group->getPeriodEnd();

                    $group->setPeriodStart($datePeriodeStart);
                    $dateEnd = clone $datePeriodeStart;
                    $dateEnd->add(new \DateInterval('P'.$monthCount.'M'));
                    $group->setPeriodEnd($dateEnd);
                    $group->setBlocked(false);
                    $order->setVisible(true);
                    $order->setPayedDate($now);
                    $order->setTitle($order->getDetails()['title']);

                    $em->persist($group);
                    $em->persist($order);
                    $em->flush();


                    $this->addFlash("success", "Le pack a été acheté avec success.");
                    //Send Confirm Mail
                    $this->get("app.mailer.manager")->sendOrderPaymentOk(
                        [
                            "email" =>  $order->getUser()->getEmail(),
                            "order" =>  $order
                        ]
                    );
                    return $this->redirectToRoute('homepage',['remoteId' => $group->getRemoteId()]);
                break;
                case 'commercial':
                    for($i=0;$i<$advantageOption->getUnit();$i++) {
                        $instance      = new AuthorizedUser();
                        $dateStart     = new \DateTime('today');
                        $dateEnd       = null;
                        $instance->setGroup($group)->setRole('ROLE_SALESMAN')->setStartDate($dateStart)->setEndDate($dateEnd);
                        $em->persist($instance);
                    }
                    $em->flush();

                    //Send Confirm Mail
                    $this->get("app.mailer.manager")->sendOrderPaymentOk(
                        [
                            "email" =>  $order->getUser()->getEmail(),
                            "order" =>  $order
                        ]
                    );

                    $this->addFlash("success", "Le pack a été acheté avec success, vous pouvez ajouter vos commercials");

                    return $this->redirectToRoute('agency_employee_add',['remoteId' => $group->getRemoteId()]);
                break;
                case 'call-tracking':
                case 'mail-tracking':

                    /** @var GroupAdvantage $existantAdvantage */
                    $existantAdvantage = $this->getDoctrine()->getRepository('AppBundle:GroupAdvantage')->findExistantGroupAdvantage($group,$advantageOption->getAdvantage());
                    $advantage = $existantAdvantage ? $existantAdvantage : new GroupAdvantage();
                    if(!$existantAdvantage) $advantage->setDateBegin(new \DateTime('now'));
                    $end     = !$existantAdvantage ? new \DateTime('now') : $existantAdvantage->getDateEnd();
                    $advantage->setDateEnd($advantageOption->getMonth() > 0 ? $end->add(new \DateInterval("P{$advantageOption->getMonth()}M") ) : null);
                    $advantage->setQuantite($advantageOption->getUnit() > 0 ? ($existantAdvantage ? $advantageOption->getUnit()+$existantAdvantage->getQuantite() :$advantageOption->getUnit()) : null);
                    $advantage->setAdvantage($advantageOption->getAdvantage());
                    $advantage->setEnabled(true);
                    $advantage->setPayed(true);
                    $advantage->setGroup($group);
                    $em->persist($advantage);
                    $em->flush();
                    $this->addFlash("success", "success de paiement");
                    //Send Confirm Mail
                    $this->get("app.mailer.manager")->sendOrderPaymentOk(
                        [
                            "email" =>  $order->getUser()->getEmail(),
                            "order" =>  $order
                        ]
                    );
                    if($this->getUser()->getGroup()->hasRole('ROLE_GROUP'))
                        return $this->redirectToRoute('group_agency_show',['remoteId' => $group->getRemoteId()]);
                    return $this->redirectToRoute('agency_my');
                break;
                case 'agence':
                    for($i=0; $i<$advantageOption->getUnit(); $i++) {
                        $instance      = new AuthorizedAgency();
                        $dateStart     = new \DateTime('today');
                        //$dateEnd       = (new \DateTime('today'))->add(new \DateInterval('P1Y'));
                        $dateEnd       = null;
                        $instance->setParentGroup($group)->setStartDate($dateStart)->setEndDate($dateEnd);
                        $em->persist($instance);
                    }
                    $em->flush();
                    $this->addFlash("success", "Le pack a été acheté avec success, vous pouvez ajouter vos agences");
                    //Send Confirm Mail
                    $this->get("app.mailer.manager")->sendOrderPaymentOk(
                        [
                            "email" =>  $order->getUser()->getEmail(),
                            "order" =>  $order
                        ]
                    );
                    return $this->redirectToRoute('group_agency_add',['remoteId' => $group->getRemoteId()]);
                    break;
                default:
                    if(stripos($order->getPackId(), "call-tracking-") !== false) {
                        $type = str_replace("call-tracking-", "", $order->getPackId());
                        $tracking = $this->get("app.tracking.call");
                        switch ($type) {
                            case 'agency' : {
                                if(!$tracking->setAdvantage($order, $agency, $advantageOption)){
                                    $this->addFlash("error", "L'achat à échouter");
                                    if(!$this->isGranted('ROLE_GROUP'))
                                        return $this->redirectToRoute('agency_my');
                                    else
                                        return $this->redirectToRoute('group_agency_show', ["remoteId" => $group->getRemoteId()]);
                                    }
                            }break;
                            case 'portal' :
                            case 'ad' :
                            case 'program' :
                            case 'lot' : {
                                $maxToTrack = 0;
                            if(!$this->isGranted('ROLE_GROUP'))
                                $redirectTo = $this->redirect($this->generateUrl('agency_my')."#tab-portail");
                            else
                                $redirectTo = $this->redirect(
                                    $this->generateUrl('group_agency_show', ["remoteId" => $group->getRemoteId()])."#tab-portail");
                                switch ($type) {
                                    case 'portal' :{
                                        /** @var PortalRegistration $portalRegistration */
                                        foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration ){
                                            if(
                                                $portalRegistration->getBroadcastPortal()->getCluster() == "PAYANT" &&
                                                $portalRegistration->getIsEnabled()
                                            )
                                                $maxToTrack ++;
                                        }
                                    }break;
                                    case 'ad' :{
                                        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidAdsCountByGroup($agency);
                                        $redirectTo = $this->redirectToRoute('ads_index');
                                    }break;
                                    case 'program' :{
                                        $maxToTrack = $em->getRepository("AppBundle:Program")->getValidProgramCountByGroup($agency);
                                        $redirectTo = $this->redirectToRoute('program_index');
                                    }break;
                                    case 'lot' :{
                                        $maxToTrack = $em->getRepository("AppBundle:Ad")->getValidLotsCountByGroup($agency);
                                        $redirectTo = $this->redirectToRoute('program_lots');
                                    }break;
                                }

                                $totalToTracking = $order->getDetails()['quantity'];
                                $trackingLater = true;
                                // selection auto
                                if(
                                    $totalToTracking == $maxToTrack
                                ){
                                    $trackingLater = false;
                                }
                                if( !$tracking->setAdvantage($order, $agency, $advantageOption, $trackingLater)) {
                                    $this->addFlash("error", "L'achat à échouter");
                                    return $this->redirectToRoute('agency_my');
                                }
                                if($trackingLater == true){
                                    //Send Confirm Mail
                                    $this->get("app.mailer.manager")->sendOrderPaymentOk(
                                        [
                                            "email" =>  $order->getUser()->getEmail(),
                                            "order" =>  $order
                                        ]
                                    );
                                    $this->addFlash("success", "success de paiement");
                                    return $redirectTo;
                                }
                            }break;

                        }

                    }

                    $this->addFlash("success", "success de paiement");
                    if($this->getUser()->getGroup()->hasRole('ROLE_GROUP'))
                        return $this->redirectToRoute('group_agency_show',['remoteId' => $group->getRemoteId()]);
                    return $this->redirectToRoute('agency_my');
                    break;
            }
        } elseif($status->isFailed()) {
            $order->setStatus(Order::STATUS_FAILED);
            $this->addFlash("error", "L'achat à échouter");
            return $this->redirectToRoute('agency_my');
        } else {
            $this->addFlash("error", "Le pack n'a pas été acheter");
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * Return the view of the admin group
     * @return Response
     * @param \AppBundle\Entity\Group $group
     * @Route("/{remoteId}", name="payment_index" )
     * @Template("AppBundle:Payment:group_payment.html.twig")
     * test card number : 5105105105105100
     **/
    public function groupPaymentAction(Request $request,Group $group)
    {
        $form       = null;
        $price      = null;
        $hasChosenPay = $request->get('pay',null);
        if(!is_null($hasChosenPay)) {
            $em   = $this->getDoctrine()->getManager();
            $form = $this->createForm(new PaymentType());

            switch ($hasChosenPay) {
                case 0:default: $price = $group->getOneMonthPrice();$months = 1;break;
                case 1: $price = $group->getThreeMonthPrice();$months = 3;break;
                case 2: $price = $group->getSixMonthPrice();$months = 6;break;
                case 3: $price = $group->getOneYearPrice();$months = 12;break;
            }

            if ($request->getMethod() == 'POST') {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data = $form->getData();
                    $order = new Order();
                    $order->setGroup($group->getParentGroup() ? $group->getParentGroup() : $group);
                    $order->setUser($this->getUser());
                    $order->setStatus(Order::STATUS_WAITING_FOR_PAYMENT);
                    $order->setPackId('subscribe');
                    $order->setExecutionDate(new \DateTime());

                    $order->setHTTotalAmount($price);
                    $order->setTotalAmount(money_format('%.2n',$price * 1.2));
                    $details = [
                        'title'             => "Abonnement ".$months." mois",
                        'discount'          => null,
                        'reference'         => "AB".$months,
                        'monthCount'         => $months,
                        'quantity'          => 1,
                        'HT'                => $order->getHTTotalAmount(),
                        'TTC'               => $order->getTotalAmount(),
                        'firstPrice'        => $price
                    ];
                    $order->setDetails($details);

                    switch ($data['method']) {
                        case 'credit-card':
                            $gatewayName = 'paypal_pro_checkout_display';
                            $storage = $this->getPayum()->getStorage(PaymentDetails::class);
                            $payment = $storage->create();
                            $payment['ACCT'] = new SensitiveValue($data['card']);
                            $payment['CVV2'] = new SensitiveValue($data['cvv2']);
                            $payment['EXPDATE'] = new SensitiveValue($data['exp_date']->format('my'));
                            $payment['AMT'] = number_format($order->getTotalAmount(), 2);
                            $payment['CURRENCY'] = 'EUR';
                            $storage->update($payment);
                            $order->setPayMethod('credit-card');
                            $em->persist($order);
                            $em->flush();

                            $captureToken = $this->getPayum()->getTokenFactory()->createCaptureToken(
                                $gatewayName,
                                $payment,
                                'payment_done',
                                [
                                    'agencyRemoteId' => $group->getRemoteId(),
                                    'remoteId' => $order->getRemoteId()
                                ]
                            );
                            return $this->forward('PayumBundle:Capture:do', array(
                                'payum_token' => $captureToken,
                            ));
                            break;

                        case 'paypal':
                            if ($form->get('paypal_pay')->isClicked()) {
                                $gatewayName = 'paypal_express_checkout_and_doctrine_orm';
                                $storage = $this->get('payum')->getStorage(PaymentDetails::class);
                                $payment = $storage->create();
                                $payment['PAYMENTREQUEST_0_CURRENCYCODE'] = 'EUR';
                                $payment['PAYMENTREQUEST_0_AMT'] = $order->getTotalAmount();
                                $storage->update($payment);
                                $order->setPayMethod('paypal');
                                $em->persist($order);
                                $em->flush();
                                $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
                                    $gatewayName,
                                    $payment,
                                    'payment_done',
                                    [
                                        'agencyRemoteId' => $group->getRemoteId(),
                                        'remoteId' => $order->getRemoteId()
                                    ]
                                );
                                return $this->redirect($captureToken->getTargetUrl());
                            }
                            break;
                        case 'bank-wire':
                            $order->setPayMethod('bankwire');
                            $order->setStatus(Order::STATUS_WAITING_ADMIN_VALIDATION);
                            $em->persist($order);
                            $em->flush();

                            $remoteId = 'immo-invoice-'.$order->getId();
                            $remoteId = md5($remoteId);
                            $order->setRemoteId($remoteId);
                            $em->persist($order);
                            $em->flush();

                            $this->addFlash("success",
                                "L'achat par virement bancaire a été prise en compte,
                                 l'administrateur va consulter le transfert.");

                            return $this->redirectToRoute('bankwire_show', ['remoteId' => $order->getRemoteId()]);
                            break;
                    }
                }
            }
        }

        return [
            'levels'        => self::LEVELS,
            'group'         => $group,
            'form'          => $form ? $form->createView() : null,
            "totalPrice"    => $price
        ];
    }

    /**
     * @return Response
     * @Route("/restrict/blocked", name="payment_blocked" )
     * @Template("AppBundle:Payment:payment_blocked.html.twig")
     **/
    public function blockedPaymentAction(Request $request)
    {
        return [];
    }

}