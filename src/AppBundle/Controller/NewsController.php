<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\PendingActions;
use AppBundle\Form\Type\NewsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\News as News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends BaseController
{

    /**
     * @Route("/actualite", name="news_list" )
     * @Method({"GET"})
     * @Template("AppBundle:News:list.html.twig")
     **/
    public function listAction(){

        $em = $this->getDoctrine()->getManager();
        /** @var \AppBundle\Repository\NewsRepository $repo */
        $repo = $em->getRepository('AppBundle:News');

        $newsList = $repo->getNewsList();
        $totalNews = $repo->countAll();

        if ($totalNews === 0)
            $this->addFlash( 'info', 'empty_news');

        return [
            'newsList'      => $newsList,
            'total_news'    => $totalNews
        ];

    }

    /**
     * @Route("/actualite/{slug}", name="news_view" )
     * @Method({"GET"})
     * @Template("AppBundle:News:view.html.twig")
     * @param \AppBundle\Entity\News $news
     * @return Response
     **/
    public function viewAction(News $news){

        $em = $this->getDoctrine()->getManager();
        $news->incrementNbViews();

        $em->persist($news);
        $em->flush();

        if ($news === null)
            throw $this->createNotFoundException('Unknown news ' . $news->getId());

        return [ 'news' => $news ];

    }

    /**
     * @Route("/admin/actualite/editer/{slug}", name="news_edit" )
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"POST","GET"})
     * @Template("AppBundle:News:admin_edit.html.twig")
     * @param \AppBundle\Entity\News $news
     * @return Response
     **/
    public function adminEditAction(Request $request, News $news){
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NewsType(),$news);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($news);
                $em->flush();
                return new RedirectResponse($this->generateUrl('news_view',['slug' => $news->getSlug()]));
            }
        }

        return [
            'form'  => $form->createView(),
            'news' => $news
        ];
    }

    /**
     * @Route("/admin/actualite/ajouter", name="news_add" )
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET","POST"})
     * @Template("AppBundle:News:admin_add.html.twig")
     **/
    public function adminAddAction(Request $request){

        $news = new News();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NewsType(),$news);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $news->setAuthor($this->getUser());
            $news->setDate(new \DateTime());

            if ($form->isValid()) {
                $em->persist($news);
                $em->flush();

                $args = ["contentId" => $news->getId(), "action" => "news"];
                $service = "app.mailer.manager";
                $existAction = $em->getRepository("AppBundle:PendingActions")->findOneBy(
                    [
                        "service" => $service,
                        "args"    => serialize($args),
                        "status"  => PendingActions::STAND_BY_STATUS
                    ]
                );
                //if does not existe then create new one
                if( !$existAction) {
                    $pendingAction = new PendingActions();
                    $pendingAction->setService($service);
                    $pendingAction->setArgs($args);
                    $em->persist($pendingAction);
                    $em->flush();
                }

                return new RedirectResponse($this->generateUrl('news_list'));
            }
        }

        return ['form'  => $form->createView()];
    }

    /**
     * @Route("/admin/actualite/supprimer/{slug}", name="news_delete" )
     * @Security("has_role('ROLE_ADMIN')")
     * @Method({"GET"})
     * @Template
     *
     * @param \AppBundle\Entity\News $news
     * @return RedirectResponse
     **/

    public function adminDeleteAction(News $news = null){

        $em = $this->getDoctrine()->getManager();
        if($news !== NULL){
            $em->remove($news);
            $em->flush();
        } else {
            $this->addFlash('error', 'news_not_found');
        }

        return new RedirectResponse($this->generateUrl('news_list'));
    }

}