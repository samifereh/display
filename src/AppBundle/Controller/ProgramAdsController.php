<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Appartement;
use AppBundle\Entity\Broadcast\AdDailyLimit;
use AppBundle\Entity\Broadcast\Diffusion;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Program;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Form\Type\AdType;
use AppBundle\Form\Type\Broadcast\DiffusionType;
use AppBundle\Form\Type\Broadcast\PortalType;
use AppBundle\Form\Type\LotsSearchType;
use AppBundle\Service\Helpers;
use AppBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use TrackingBundle\Entity\Advantage;
use ZipArchive;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AdsController
 * @package AppBundle\Controller
 * @Route("/program/annonces")
 */
class ProgramAdsController extends BaseController
{
    ######################PROGRAMME###################

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation/project-type/pro-{remoteId}", name="ads_create_program_1")
     * @Method({"GET"})
     * @Template("AppBundle:Ads:create_by_program.html.twig")
     */
    public function createProgramAction(Request $request,Program $program){

        $user  = $this->getUser();
        $group = $this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') && $user->getGroup()->hasRole('ROLE_GROUP') ? $this->getUserService()->checkUserAgency($request) : $user->getAgencyGroup();
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        return ['program' => $program];
    }


    /**
     * @param Request $request
     * @param HouseModel $houseModel
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/creation/pro-{remoteId}/{projectType}",requirements={"projectType" = "appartement|maison"}, defaults={"projectType" = null}, options={"expose"=true}, name="ads_create_program_2")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:create_project_by_program.html.twig")
     */
    public function createAdByProgramAction(Request $request,Program $program,$projectType = null)
    {
        $user        = $this->getUser();
        if(!$this->isGranted('ROLE_ADMIN'))
        if(($program && !$user->isEmployeeOf($program->getGroup())) || ($projectType && $program->getType() != 'mixte'))
            throw new AccessDeniedHttpException();

        if(!$projectType && $program->getType() == 'mixte')
           return $this->redirectToRoute('ads_create_program_1',['remoteId' => $program->getRemoteId()]);

        $em         = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $commercial = $this->getUserService()->checkCommercialUser($request, $group);
//        if(!$commercial) {
//            if($this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
//                return new RedirectResponse($this->generateUrl('switch_commercial',['redirect'=> $request->getUri() ]));
//            }
//            throw new AccessDeniedHttpException();
//        }
        if($user->hasRole('ROLE_SALESMAN') && !$commercial->equals($program->getOwner()))
            throw new AccessDeniedHttpException();

        $typeBien    = $program->getType();
        $ad          = new Ad();
        switch ($program->getType()) {
            case 'maison':$ad->addBienImmobilier(new Maison()); break;
            case 'appartement':$ad->addBienImmobilier(new Appartement()); break;
            case 'terrain':$ad->addBienImmobilier(new Terrain()); break;
            case 'mixte':
                if($projectType == 'maison') {
                    $ad->addBienImmobilier(new Maison());
                    $typeBien = 'maison';
                } else {
                    $ad->addBienImmobilier(new Appartement());
                    $typeBien = 'appartement';
                }
            break;
        }

        $session   = $request->getSession();
        $brouillon = null;
        $flow      = $this->get('app.form.flow.ad');
        $oldImage  = $program->getImagePrincipale();

        if(!$session->get('brouillon_ad_program_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_ad_program_id',$brouillon->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_ad_program_id'));
        }
        $ad->synchroniseProgram($program);
        $flow->bind($ad);
        $form = $submittedForm = $flow->createForm(['checkImageEmpty' => false]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                if($brouillon->getImagePrincipale()) {
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                }
            } elseif(!$brouillon->getImagePrincipale()) {
                $ad->setImagePrincipale($program->getImagePrincipale());
            }

            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                if($brouillon->getImagePrincipale())
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                $ad->setTypeDiffusion('program');
                $ad->setOwner($commercial);
                $ad->getBienImmobilier()[0]->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                $ad->setProgram($program);

                /** @var \AppBundle\Entity\BienImmobilier $bien */
                $bien = $ad->getBienImmobilier()[0];
                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillon->getDocuments() as $document)
                    $ad->addDocument($document);

                if(!$this->isGranted('ROLE_BRANDLEADER')) {
                    $ad->setStat(Ad::PENDING);
                    $ad->setPublished(false);
                } else {
                    $ad->setStat(Ad::VALIDATE);
                    $ad->setPublished(true);
                }
                $em->persist($ad);
                $em->flush();

                if(!$program->getDiffusion()) {
                    $ad->setProgramDiffuse(false);
                } else {
                    $canDiffuse = true;
                    /** @var \AppBundle\Entity\Broadcast\Portal $broadcastPortal */
                    foreach ($program->getDiffusion()->getBroadcastPortals() as $broadcastPortal) {
                        $hasLimit = $this->getDoctrine()->getRepository('AppBundle:Broadcast\CommercialPortalLimit')->commercialHasCreditInPortalDiffusion($commercial,$broadcastPortal);
                        if(!$hasLimit) { $canDiffuse = false;break; }
                    }
                    $ad->setProgramDiffuse($canDiffuse);
                }
                $bien->setAd($ad);
                $em->persist($bien);
                $em->flush();
                $flow->reset();

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $brouillon->setImagePrincipale(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_ad_program_id');
                }

                $this->addFlash("success", "L'annonce a été ajouter au program".$program->getTitle());
                return new RedirectResponse($this->generateUrl('diffusion_ads_project',['remoteId' => $ad->getRemoteId()]));
            }
        }

        return [
            'typeBien'    => $typeBien,
            'ad'          => $ad,
            'form'        => $form->createView(),
            'flow'        => $flow,
            'program'     => $program,
            'brouillon'   => $brouillon
        ];
    }



    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/edition/pro-{remoteId}-ad", defaults={"projectType" = null}, options={"expose"=true}, name="ads_edit_program")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Ads:edit_project_by_program.html.twig")
     */
    public function editAdByProgramAction(Request $request,Ad $ad)
    {
        $user        = $this->getUser();
        $program     = $ad->getProgram();
        $projectType = null;
        if($program->getType() == 'mixte')
            $projectType = $ad->isMaisonOnly() ? 'maison' : 'appartement';

//        if(!$this->isGranted('ROLE_ADMIN'))
//        if($program && !$user->isEmployeeOf($program->getGroup()))
//            throw new AccessDeniedHttpException();

        $em         = $this->getDoctrine()->getManager();
        $group = $this->getCurrentGroup($request);
        $authorized = $this->groupIsAutorised($group,'hasProgramOption');
        if(!is_null($authorized))
            return $authorized;

        $commercial = $user->hasRole('ROLE_SALESMAN') ? $user : $this->getUserService()->checkCommercialUser($request, $group);
        if(!$commercial && !$this->isGranted('ROLE_ADMIN')) { throw new AccessDeniedHttpException(); }

        $typeBien    = $program->getType() == 'mixte' ? ($ad->isMaisonOnly() ? 'maison' : 'appartement') : $program->getType();
        $session   = $request->getSession();
        $brouillon = null;
        $flow      = $this->get('app.form.flow.ad');
        $oldImage  = $ad->getImagePrincipale();

        if(!$session->get('brouillon_ad_program_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_ad_program_id',$brouillon->getId());
            $flow->reset();
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_ad_program_id'));
        }

        $flow->bind($ad);
        $form = $submittedForm = $flow->createForm(['checkImageEmpty' => false]);

        if ($flow->isValid($submittedForm)) {
            if($flow->getCurrentStepNumber() == 1) {
                if($brouillon->getImagePrincipale()) {
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                }
            } else {
                $ad->setImagePrincipale($oldImage);
            }

            $flow->saveCurrentStepData($submittedForm);
            if ($flow->nextStep()) {
                $form = $flow->createForm();
            } else {
                if($brouillon->getImagePrincipale())
                    $ad->setImagePrincipale($brouillon->getImagePrincipale());
                $ad->setTypeDiffusion('program');
                $ad->setOwner($commercial);
                $ad->getBienImmobilier()[0]->setGroup($commercial->getAgencyGroup())->setOwner($commercial);
                $ad->setProgram($program);

                /** @var \AppBundle\Entity\BienImmobilier $bien */
                $bien = $ad->getBienImmobilier()[0];
                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillon->getDocuments() as $document) {
                    $document->setBien($bien);
                    $em->persist($document);
                }

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $brouillon->setImagePrincipale(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_ad_program_id');
                }

                if(!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER')) {
                    $ad->setStat(Ad::PENDING);
                    $ad->setPublished(false);
                } else {
                    $ad->setStat(Ad::VALIDATE);
                    $ad->setPublished(true);
                }
                $em->persist($ad);
                $em->flush();

                if(!$program->getDiffusion()) {
                    $ad->setProgramDiffuse(false);
                } else {
                    $canDiffuse = true;
                    /** @var \AppBundle\Entity\Broadcast\Portal $broadcastPortal */
                    foreach ($program->getDiffusion()->getBroadcastPortals() as $broadcastPortal) {
                        $hasLimit = $this->getDoctrine()->getRepository('AppBundle:Broadcast\CommercialPortalLimit')->commercialHasCreditInPortalDiffusion($commercial,$broadcastPortal);
                        if(!$hasLimit) { $canDiffuse = false;break; }
                    }
                    $ad->setProgramDiffuse($canDiffuse);
                }
                $bien->setAd($ad);
                $em->persist($bien);
                $em->flush();
                $flow->reset();
                $this->addFlash("success", "L'annonce a été ajouter au program '".$program->getTitle()."'");
                return new RedirectResponse($this->generateUrl('program_view',['remoteId' => $program->getRemoteId()]));
            }
        }

        return [
            'typeBien'    => $typeBien,
            'ad'          => $ad,
            'form'        => $form->createView(),
            'flow'        => $flow,
            'program'     => $program,
            'brouillon'   => $brouillon
        ];
    }

    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="ads_program_switch_statut")
     * @Method({"GET","POST"})
     */
    public function switchPublishedStatusAction(Request $request, Ad $ad)
    {
        if($request->isXmlHttpRequest()) {
            $entity = $ad->getProgram() ? $ad->getProgram() : $ad;
            if(!$this->isGranted('ROLE_ADMIN'))
            if (!$this->getAuthorizationChecker()->isGranted('ROLE_BRANDLEADER') || !$this->getUser()->isEmployeeOf($entity->getGroup()))
                throw new AccessDeniedHttpException();
            $stat = Ad::PENDING;
            $em = $this->getDoctrine()->getManager();
            switch ($ad->getStatValue()) {
                case Ad::PENDING:
                    $stat = Ad::VALIDATE;
                    break;
                case Ad::VALIDATE:
                    $stat = Ad::RESERVER;
                    break;
                case Ad::RESERVER:
                    $stat = Ad::VENDU;
                    break;
                case Ad::VENDU:
                    $stat = Ad::ANNULER;
                    break;
                case Ad::ANNULER:
                    $stat = Ad::PENDING;
                    break;
            }
            $ad->setStat($stat);
            $ad->setPublished($stat == Ad::VALIDATE ? true : false);
            $em->persist($ad);
            $em->flush();
            return new JsonResponse(['_callback' => $ad->getStatvalue()]);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/", name="program_lots")
     * @Method({"GET"})
     * @Template("AppBundle:Program:list_lots.html.twig")
     */
    public function listLotsAction(Request $request){

        $data = [];
        $form = $this->createForm(new LotsSearchType($this->getDoctrine()),$data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if(!empty($data['dateRemonter']))
                $data['dateRemonter'] = ['date' => $data['dateRemonter']->format('Y-m-d')];

            if(!empty($data['portalDiffusion'])) {
                $data['portalDiffusion'] = $data['portalDiffusion']->getId();
            }
        }

        $trackingAdvantage = null;
        $groupAgency = $this->getUser()->getAgencyGroup();

        if(is_object($groupAgency)) {
            /**
             * tracking program
             */
            $trackingRepository = $this->getDoctrine()->getRepository("TrackingBundle:Advantage");
            /** @var Advantage $trackingAdvantage */
            $trackingAdvantage = $trackingRepository->findExistantTrackingAdvantage($groupAgency->getAgency(), "lot");
        }

        $datatableSearch = $this->get('app.datatable.lots');
        $datatableSearch->buildDatatable([
            'data' => $data,
            'trackingAdvantage' => $trackingAdvantage
        ]);

        return [
            'datatableSearch'   => $datatableSearch,
            'form'              => $form->createView(),
            'trackingAdvantage' => $trackingAdvantage
        ];

    }

}