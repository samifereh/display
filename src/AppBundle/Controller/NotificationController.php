<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:55
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Notification;
use AppBundle\Form\Type\Broadcast\PortalType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class NotificationController
 * @package AppBundle\Controller
 * @Route("/notification")
 */
class NotificationController extends BaseController
{
    /**
     * @Route("/pull/{id}", requirements={"id" = "\d+"}, name="notification_pull")
     * @param Notification $notification
     * @return RedirectResponse
     */
    public function pullAction(Notification $notification) {

        $route = $notification->getRoute();
        $parameters = ($notification->getArgs() ? $notification->getArgs() : []);
        $em = $this->getDoctrine()->getManager();
        $em->remove($notification);
        $em->flush();
        return $this->redirectToRoute($route, $parameters);
    }

}