<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 10:55
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Form\Type\Broadcast\PortalType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class PortalController
 * @package AppBundle\Controller
 * @Route("/portail")
 * @Security("has_role('ROLE_ADMIN')")
 */
class PortalController extends BaseController
{
    /**
     * @Route("/", name="portal_admin")
     * @Method({"GET"})
     * @Template("AppBundle:Portal:index.html.twig")
     */
    public function indexAction(Request $request) {
        $datatable = $this->get('app.datatable.portal');
        $datatable->buildDatatable();
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/create", name="portal_create")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Portal:create.html.twig")
     */
    public function createAction(Request $request) {
        $portal = new Portal();
        $form = $this->createForm(new PortalType(), $portal);
        $imageError = false;
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if(!$portal->getIcon()) { $imageError = true; }
            if($form->isValid() && !$imageError) {
                $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                $uploadableManager->markEntityToUpload($portal, $portal->getIcon());

                $em = $this->getDoctrine()->getManager();
                $portal->setUpdatedAt(new \DateTime('now'));
                $em->persist($portal);
                $em->flush();
                $this->addFlash("success", "Le portail a été ajouter");
                return $this->redirectToRoute('portal_admin');
            }
        }
        return [
            'form'       => $form->createView(),
            'imageError' => $imageError
        ];
    }

    /**
     * @Route("/{id}/edit",requirements={"id" = "\d+"}, options={"expose"=true}, name="portal_edit")
     * @Method({"GET","POST"})
     * @Template("AppBundle:Portal:create.html.twig")
     */
    public function editAction(Request $request, Portal $portal) {
        $oldIcon = $portal->getIcon();
        $form = $this->createForm(new PortalType(), $portal,['icon'=>$oldIcon]);
        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->isValid()) {
                if(null === $form->get('icon')->getData())
                    $portal->setIcon($oldIcon);
                else {
                    $uploadableManager = $this->get('stof_doctrine_extensions.uploadable.manager');
                    $uploadableManager->markEntityToUpload($portal, $portal->getIcon());
                }

                $em = $this->getDoctrine()->getManager();
                $portal->setUpdatedAt(new \DateTime('now'));
                $em->persist($portal);
                $em->flush();
                $this->addFlash("success", "Le portail a été modifier");
                return $this->redirectToRoute('portal_admin');
            }
        }
        return [
            'form' => $form->createView(),
            'portal' => $portal,
            'imageError'  => false
        ];
    }

    /**
     * @Route("/{id}",requirements={"id" = "\d+"}, options={"expose"=true}, name="portal_show")
     * @Method({"GET"})
     * @Template("AppBundle:Portal:show.html.twig")
     */
    public function showAction(Request $request, Portal $portal) {
        if(!$portal)
            return $this->redirectToRoute('portal_admin');

        return ['portal' => $portal];
    }

    /**
     * @Route("/{id}",requirements={"id" = "\d+"}, options={"expose"=true}, name="portal_update")
     * @Method({"PUT"})
     */
    public function updateAction(Request $request, Portal $portal) {
        $em = $this->getDoctrine()->getManager();

        if(!$portal)
            return $this->redirectToRoute('portal_admin');

        $form = $this->createForm(new PortalType(), $portal);
        $form->submit($request);
        if(!$form->isValid()) {
            return $this->redirectToRoute('portal_edit', ['id' => $portal->getId()]);
        }

        $em->persist($portal);
        $em->flush();
        return $this->redirectToRoute('portal_show', ['id' => $portal->getId()]);
    }

    /**
     * @Route("/delete/{id}",requirements={"id" = "\d+"}, options={"expose"=true}, name="portal_delete")
     * @Method({"DELETE","GET"})
     */
    public function deleteAction(Portal $portal) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($portal);
        $em->flush();
        $this->addFlash("success", "portal_deleted");
        return $this->redirectToRoute('portal_admin');
    }

}