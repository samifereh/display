<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Configuration\Configuration;
use AppBundle\Form\Type\Configuration\AdvantagesType;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ConfigurationController
 * @package AppBundle\Controller
 * @Route("/configuration")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ConfigurationController extends BaseController
{
    /**
     * @Route("/advantages", name="configuration_advantages" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Configuration:Advantages/index.html.twig")
     */
    public function advantagesAction(Request $request)
    {
        $configuration = $this->getDoctrine()->getRepository('AppBundle:Configuration\Configuration')->getConfiguration();
        if(is_null($configuration))
            $configuration = new Configuration();

        $form = $this->createForm(AdvantagesType::class,$configuration);
        if ($form->handleRequest($request) && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($configuration);
            $em->flush();
            $this->addFlash("success", "Configuration enregistrer");
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/refresh-cache", name="refresh_cache" )
     */
    public function refreshCacheAction(Request $request)
    {
        $kernel = $this->get('kernel');
        $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $application->setAutoExit(false);
        $options = array('command' => 'cache:clear',"--env" => 'prod', '--no-warmup' => true);
        $application->run(new \Symfony\Component\Console\Input\ArrayInput($options));

        return $this->redirectToRoute('homepage');
    }


}
