<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\DemandeContact;
use AppBundle\Entity\Group;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/agenda")
 **/
class GestionAgendaController extends BaseController
{

    /**
     * @Route("/", name="gestion_agenda_view" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Agenda/view.html.twig")
     * @return Response
     **/
    public function viewAction(){
        return [];
    }

    /**
     * @return Response
     *
     * @Route("/event-form/{id}", defaults={"id" = null}, options={"expose"=true}, name="agenda_event_form" )
     * @Template("AppBundle:Gestion:Agenda/_form.html.twig")
     * @Method({"POST","GET"})
     */
    public function eventFormAction(Request $request, AgendaEvent $agendaEvent = null){
        $user = $this->getUser();
        if(!$this->isGranted('ROLE_ADMIN'))
            if($agendaEvent && (!$user->isEmployeeOf($agendaEvent->getOwner()->getGroup()) ) )
                throw new AccessDeniedHttpException();

        if(!$agendaEvent)
            $agendaEvent = new AgendaEvent();

        $return = ['_content' => ''];
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.events'),$agendaEvent);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            $type = null;
            if(isset($request->request->get('agenda_event')['_type'])) {
                $type = $request->request->get('agenda_event')['_type'];
                if($type == 'listOpportunity') {
                    if(!$agendaEvent->getOpportunity()) {
                        $form->get('_type')->addError(new FormError('L\'opportunité est obligatoire.'));
                    }
                } elseif($type == 'listSharedUsers') {
                    if(!count($agendaEvent->getSharedUsers())) {
                        $form->get('_type')->addError(new FormError('Vous devez selectionner au moin un membre.'));
                    }
                }
            }

            if ($form->isValid()) {
                if(!$agendaEvent->getOwner()) {
                    $agendaEvent->setOwner($user);
                }
                if($type && $type == 'listOpportunity') { $agendaEvent->cleanSharedUsers();}
                if($type && $type == 'listSharedUsers') { $agendaEvent->setOpportunity(null);}

                /** @var \AppBundle\Repository\Gestion\AgendaEventRemindersRepository $agendaEventRemindRepository */
                $agendaEventReminderRepository = $em->getRepository('AppBundle:Gestion\\AgendaEventReminder');
                $existAgendaEventRemind = $agendaEventReminderRepository->findOneBy(['agendaEvent' => $agendaEvent]);
                if($agendaEvent->getRemind()) {
                    $agendaEventRemind = $this->getAgendaManager()->getAgendaReminderFromEvent($agendaEvent, $existAgendaEventRemind);
                    $em->persist($agendaEventRemind);
                    $em->flush();
                } elseif($existAgendaEventRemind) {
                    $em->remove($existAgendaEventRemind);
                    $em->flush();
                }

                $em->persist($agendaEvent);
                $em->flush();
                $return['success'] = true;
                $this->addFlash("success", "L'événement à été enregistrer.");
                return new JsonResponse($return);
            } else {

                $return['success'] = false;
                $return['_content'] = $this->renderView("AppBundle:Gestion:Agenda/_form.html.twig",[
                    'form' => $form->createView(),
                    'agendaEvent' => $agendaEvent
                ]);
                return new JsonResponse($return);
            }
        }
        $return['_content'] = $this->renderView("AppBundle:Gestion:Agenda/_form.html.twig",[
            'form' => $form->createView(),
            'agendaEvent' => $agendaEvent
        ]);
        return new Response($return);
    }


    /**
     * @return Response
     *
     * @Route("/event-view/{id}", defaults={"id" = null}, options={"expose"=true}, name="agenda_event_view" )
     * @Template("AppBundle:Gestion:Agenda/view_event.html.twig")
     * @Method({"POST","GET"})
     */
    public function viewEventAction(Request $request, AgendaEvent $agendaEvent = null){
        $user = $this->getUser();
        if(!$this->isGranted('ROLE_ADMIN'))
            if($agendaEvent && (!$user->isEmployeeOf($agendaEvent->getOwner()->getGroup()) ) )
                throw new AccessDeniedHttpException();

        return [
            'agendaEvent' => $agendaEvent
        ];
    }

    /**
     * @return Response
     *
     * @Route("/event-delete/{id}", options={"expose"=true}, name="agenda_delete_event" )
     * @Method({"POST","GET"})
     */
    public function deleteEventAction(Request $request, AgendaEvent $agendaEvent){
        $user = $this->getUser();
        if(!$this->isGranted('ROLE_ADMIN'))
            if($agendaEvent && (!$user->isEmployeeOf(($agendaEvent->getOwner()->getAgencyGroup() ? $agendaEvent->getOwner()->getAgencyGroup() : $agendaEvent->getOwner()->getGroup())) ) )
                throw new AccessDeniedHttpException();

        if(($this->isGranted('ROLE_MARKETING') && $user->isEmployeeOf($agendaEvent->getOwner()->getAgencyGroup())) || $agendaEvent->getOwner() == $user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($agendaEvent);
            $em->flush();

            $this->addFlash("success", "L'événement a bien été supprimer");
            return $this->redirectToRoute("gestion_agenda_view");
        } else {
            $this->addFlash("error", "Vous n'êtes pas autorisé a supprimer cette événement");
            return $this->redirectToRoute("gestion_agenda_view");
        }
    }



}