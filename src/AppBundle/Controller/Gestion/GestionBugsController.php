<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\BugComment;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\Gestion\BugCommentType;
use Mailjet\Api\RequestApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/bugs")
 **/
class GestionBugsController extends BaseController
{

    /**
     * @Route("/list/{remoteId}", defaults={"remoteId" = null}, name="bugs_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Bugs/list.html.twig")
     **/
    public function listAction(Request $request,Group $group = null)
    {
        if(!$group && $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $group = $this->getCurrentGroup($request);

        $datatable = $this->get('app.datatable.bugs');
        $datatable->buildDatatable(['group' => $group]);
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_bugs_view" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Bugs/view.html.twig")
     * @param \AppBundle\Entity\Gestion\Bug $bug
     * @return Response
     **/
    public function viewAction(Request $request, Bug $bug){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($bug->getGroup()))
            throw new AccessDeniedHttpException();

        return [ 'bug' => $bug ];
    }

    /**
     * @Route("/ajouter/{remoteId}", defaults={"remoteId" = null}, name="gestion_bugs_add", options={"expose"=true})
     * @Method({"GET","POST"})
     **/
    public function addAction(Request $request, Group $group = null){
        $bug  = new Bug();
        $em   = $this->getDoctrine()->getManager();
        $bigGroup = $request->request->get('bugs') && isset($request->request->get('bugs')['bigGroups']) ? $request->request->get('bugs')['bigGroups'] : null;
        $form = $this->createForm($this->get('app.form.bug'),$bug,['bigGroup' => $bigGroup]);
        $user = $this->getUser();

        if(!$group) {
            if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                $group = $this->getCurrentGroup($request);
            elseif($user->getGroup()->hasRole('ROLE_AGENCY')) {
                $group = $user->getGroup();
            } else {
                $group = $this->getCurrentGroup($request);
            }
        }

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(!$bug->getGroup())
                    $bug->setGroup($group);
                $bug->setOwner($this->getUser());
                $em->persist($bug);
                $em->flush();
                $this->get("app.mailer.manager")->sendBugCreate(["bug" =>  $bug]);

                return new RedirectResponse($this->generateUrl('bugs_list'));
            }
        }
        return $this->render("AppBundle:Gestion:Bugs/add.html.twig",['form'  => $form->createView()]);
    }

    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="gestion_bugs_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Bugs/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\Bug $bug
     * @return Response
     **/
    public function editAction(Request $request, Bug $bug){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($bug->getGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.bug'),$bug);
        $oldStatus = $bug->getStatus();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($bug);
                $em->flush();
                if($oldStatus != $bug->getStatus()){
                    if($user != $bug->getOwner()){
                        $this->get("app.mailer.manager")->sendBugChangeStatus([ "bug" =>  $bug]);
                    }
                }
                return new RedirectResponse($this->generateUrl('gestion_bugs_view',['remoteId' => $bug->getRemoteId()]));
            }
        }

        return [
            'form'    => $form->createView(),
            'bug'     => $bug
        ];
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="gestion_bugs_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\Bug $bug
     * @return RedirectResponse
     **/
    public function deleteAction(Bug $bug){

        $em = $this->getDoctrine()->getManager();
        if($bug !== NULL){
            $em->remove($bug);
            $em->flush();
        } else {
            $this->addFlash('error', 'contact_not_found');
        }
        return new RedirectResponse($this->generateUrl('bugs_list'));
    }


    /* GESTION COMMENTAIRES */
    /**
     * @Route("/comments/{remoteId}", name="gestion_bugs_comments" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Bugs/view_comments.html.twig")
     * @param \AppBundle\Entity\Gestion\Bug $bug
     * @return Response
     **/
    public function commentsAction(Request $request, Bug $bug){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($bug->getGroup()))
            throw new AccessDeniedHttpException();


        if($bug->getComments()) {
            $em   = $this->getDoctrine()->getManager();
            /** @var BugComment $comment */
            foreach ($bug->getComments() as $comment) {
                if(!$comment->getViewed() && $comment->getOwner() != $user) {
                    $comment->setViewed(true);
                    $em->persist($comment);
                    $em->flush();
                }
            }
        }


        $em = $this->getDoctrine()->getManager();
        $comment = new BugComment();
        $form = $this->createForm(new BugCommentType(),$comment);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $comment->setOwner($user);
                $bug->addComment($comment);
                $em->persist($bug);
                $em->flush();
                // send mail to
                if($user != $bug->getOwner()){
                    $this->get("app.mailer.manager")->sendBugResponse([ "bug" =>  $bug]);
                }
                return new RedirectResponse($this->generateUrl('gestion_bugs_comments',['remoteId' => $bug->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'bug'         => $bug
        ];
    }

}