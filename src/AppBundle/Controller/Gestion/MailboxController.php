<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\DemandeContact;
use AppBundle\Entity\Gestion\Discussion;
use AppBundle\Entity\Gestion\Message;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\Gestion\MessageType;
use AppBundle\Repository\Gestion\DiscussionRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/mailbox")
 **/
class MailboxController extends BaseController
{
    const ITEM_PER_PAGE = 20;
    /**
     * @Route("/inbox/{page}",defaults={"page" = 1}, name="gestion_mailbox_inbox" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Mailbox/inbox.html.twig")
     * @return Response
     **/
    public function listAction(Request $request,$page){
        $user = $this->getUser();
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $discussions          = $discussionRepository->findDiscussionByUser($user,$page);
        $discussionsCount     = $discussionRepository->countDiscussionByUser($user);
        $unreaded             = $discussionRepository->unreadedMessages($user);
        $nbPage               = ceil( $discussionsCount / self::ITEM_PER_PAGE );

        return [
            'discussions' => $discussions,
            'unreaded'    => $unreaded,
            'nbPage'      => $nbPage
        ];
    }

    /**
     * @Route("/sent/{page}",defaults={"page" = 1}, name="gestion_mailbox_sent" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Mailbox/sent.html.twig")
     * @return Response
     **/
    public function sentAction(Request $request,$page){
        $user = $this->getUser();
        /** @var DiscussionRepository $discussionRepository */
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $discussions          = $discussionRepository->findSentDiscussionByUser($user,$page);
        $discussionsCount     = $discussionRepository->countSentDiscussionByUser($user);
        $unreaded             = $discussionRepository->unreadedMessages($user);
        $nbPage               = ceil( $discussionsCount / self::ITEM_PER_PAGE );

        return [
            'discussions' => $discussions,
            'unreaded'    => $unreaded,
            'nbPage'      => $nbPage
        ];
    }

    /**
     * @Route("/trash/{page}",defaults={"page" = 1}, name="gestion_mailbox_trash" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Mailbox/trash.html.twig")
     * @return Response
     **/
    public function trashAction(Request $request,$page){
        $user = $this->getUser();
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $discussions          = $discussionRepository->findTrashDiscussionByUser($user,$page);
        $discussionsCount     = $discussionRepository->countTrashDiscussionByUser($user);
        $unreaded             = $discussionRepository->unreadedMessages($user);
        $nbPage               = ceil( $discussionsCount / self::ITEM_PER_PAGE );

        return [
            'discussions' => $discussions,
            'unreaded'    => $unreaded,
            'nbPage'      => $nbPage
        ];
    }

    /**
     * @Route("/draft/{page}",defaults={"page" = 1}, name="gestion_mailbox_draft" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Mailbox/draft.html.twig")
     * @return Response
     **/
    public function draftAction(Request $request,$page){
        $user = $this->getUser();
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $discussions          = $discussionRepository->findDraftDiscussionByUser($user,$page);
        $discussionsCount     = $discussionRepository->countDraftDiscussionByUser($user);
        $unreaded             = $discussionRepository->unreadedMessages($user);
        $nbPage               = ceil( $discussionsCount / self::ITEM_PER_PAGE );

        return [
            'discussions' => $discussions,
            'unreaded'    => $unreaded,
            'nbPage'      => $nbPage
        ];
    }

    /**
     * @Route("/new", name="gestion_mailbox_new" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Mailbox/new.html.twig")
     * @return Response
     **/
    public function newAction(Request $request){

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $discussion = new Discussion();
        $form = $this->createForm($this->get('app.form.discussion'),$discussion);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid() || $form->get('save')->isClicked()) {
                $discussion->setIsDraft($form->get('save')->isClicked());
                /** @var Message $message */
                $message = $discussion->getMessages()[0];
                $message->setUser($user);
                $discussion->setListNonVue($discussion->getUsers());
                $discussion->setSender($user);
                $em->persist($discussion);
                $em->flush();

                foreach ($discussion->getUsers() as $currentUser) {
                    if($currentUser != $user) {
                       //Send Notify Email
                        $this->get("app.mailer.manager")->sendNewMessageReceived(
                            [
                                'recipient'     => $currentUser,
                                'user'     => $user
                            ]);
                    }
                }
                return new RedirectResponse($this->generateUrl('gestion_mailbox_inbox'));
            }
        }
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $unreaded             = $discussionRepository->unreadedMessages($user);
        return [
            'form'        => $form->createView(),
            'unreaded'    => $unreaded
        ];
    }

    /**
     * @Route("/edit/{remoteId}", name="gestion_mailbox_edit_draft" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Mailbox/new.html.twig")
     * @return Response
     **/
    public function editDraftAction(Request $request,Discussion $discussion){
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.discussion'),$discussion,['edit'=>true,'new'=>false]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid() || $form->get('save')->isClicked()) {
                $discussion->setIsDraft($form->get('save')->isClicked());
                $message = $discussion->getMessages()[0];
                $message->setUser($user);
                $discussion->setListNonVue($discussion->getUsers());
                $discussion->setSender($user);
                $em->persist($discussion);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_mailbox_inbox'));
            }
        }
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $unreaded             = $discussionRepository->unreadedMessages($user);
        return [
            'form'        => $form->createView(),
            'unreaded'    => $unreaded
        ];
    }

    /**
     * @Route("/view/{remoteId}", name="gestion_mailbox_view" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Mailbox/view.html.twig")
     * @return Response
     **/
    public function viewAction(Request $request,Discussion $discussion){
        $user    = $this->getUser();
        $em      = $this->getDoctrine()->getManager();
        $message = new Message();
        $form = $this->createForm(new MessageType(),$message);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $message->setUser($user);
                $users = $discussion->getListWhoDontRemove($user);
                $discussion->setListNonVue($users);
                $discussion->addMessage($message);
                $em->persist($discussion);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_mailbox_inbox'));
            }
        }

        if($discussion->getListNonVue()->contains($user)) {
            $discussion->removeListNonVue($user);
            $em->persist($discussion);
            $em->flush();
        }
        $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
        $unreaded             = $discussionRepository->unreadedMessages($user);

        return [
            'form'        => $form->createView(),
            'discussion'  => $discussion,
            'unreaded'    => $unreaded
        ];
    }

    /**
     * @Route("/remove/{remoteId}", defaults={"remoteId" = null}, name="gestion_mailbox_remove" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Mailbox/view.html.twig")
     * @return Response
     **/
    public function removeAction(Request $request,Discussion $discussion = null){

        if(!$discussion && !$request->request->has('ids'))
            return new RedirectResponse($this->generateUrl('gestion_mailbox_inbox'));

        $user    = $this->getUser();
        $em      = $this->getDoctrine()->getManager();
        $discussions = [];
        if($request->request->has('ids')) {
            $discussionRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\Discussion');
            foreach ($request->request->get('ids') as $id) {
                $discussions[] = $discussionRepository->findOneBy(['remoteId' => $id]);
            }
        } else {
            $discussions[] = $discussion;
        }

        foreach ($discussions as $discussion) {
            if($discussion->getUsers()->contains($user)) {
                $discussion->getUsers()->removeElement($user);
                $discussion->addListWhoRemove($user);
                $em->persist($discussion);
                $em->flush();
            } elseif($discussion->getSender()->getId() == $user->getId()) {
                $em->remove($discussion);
                $em->flush();
            }
        }
        return new RedirectResponse($this->generateUrl('gestion_mailbox_inbox'));
    }

}