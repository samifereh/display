<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\DemandeContact;
use AppBundle\Entity\Gestion\FAQ;
use AppBundle\Entity\Group;
use AppBundle\Entity\PendingActions;
use AppBundle\Form\Type\Gestion\FAQType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/FAQ")
 **/
class FAQController extends BaseController
{

    /**
     * @Route("/", name="faq_page" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:FAQ/page.html.twig")
     * @return Response
     **/
    public function faqPageAction(){
        $faqs = $this->getDoctrine()->getManager()->getRepository('AppBundle:Gestion\\FAQ')->findAll();
        return ['faqs' => $faqs];
    }

    /**
     * @Route("/list", name="faq_list_admin" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:FAQ/list.html.twig")
     * @Security("is_granted('ROLE_ADMIN')")
     * @return Response
     **/
    public function listAction(){
        $datatable = $this->get('app.datatable.faq');
        $datatable->buildDatatable();
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{id}", options={"expose"=true}, name="faq_view_admin" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:FAQ/view.html.twig")
     * @param \AppBundle\Entity\Gestion\FAQ $faq
     * @Security("is_granted('ROLE_ADMIN')")
     * @return Response
     **/
    public function viewAction(Request $request, FAQ $faq){
        return [ 'faq' => $faq ];
    }

    /**
     * @Route("/ajouter", name="faq_add_admin", options={"expose"=true})
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET","POST"})
     **/
    public function addAction(Request $request){
        $faq = new FAQ();
        $form = $this->createForm(new FAQType(),$faq);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $faq->setOwner($this->getUser());
                $em = $this->getDoctrine()->getManager();
                $em->persist($faq);
                $em->flush();

                $args = ["contentId" => $faq->getId(), "action" => "faq"];
                $service = "app.mailer.manager";
                $existAction = $em->getRepository("AppBundle:PendingActions")->findOneBy(
                    [
                        "service" => $service,
                        "args"    => serialize($args),
                        "status"  => PendingActions::STAND_BY_STATUS
                    ]
                );
                //if does not existe then create new one
                if( !$existAction) {
                    $pendingAction = new PendingActions();
                    $pendingAction->setService($service);
                    $pendingAction->setArgs($args);
                    $em->persist($pendingAction);
                    $em->flush();
                }

                return new RedirectResponse($this->generateUrl('faq_list_admin'));
            }
        }
        return $this->render("AppBundle:Gestion:FAQ/add.html.twig",['form'  => $form->createView()]);
    }

    /**
     * @Route("/editer/{id}", options={"expose"=true}, name="faq_edit_admin" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:FAQ/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\FAQ $faq
     * @return Response
     **/
    public function editAction(Request $request, FAQ $faq){

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new FAQType(),$faq);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($faq);
                $em->flush();
                return new RedirectResponse($this->generateUrl('faq_view_admin',['id' => $faq->getId()]));
            }
        }

        return [
            'form'    => $form->createView(),
            'faq'     => $faq
        ];
    }

    /**
     * @Route("/supprimer/{id}", options={"expose"=true}, name="faq_delete_admin" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\FAQ $faq
     * @return RedirectResponse
     **/
    public function deleteAction(FAQ $faq){

        $em = $this->getDoctrine()->getManager();
        if($faq !== NULL){
            $em->remove($faq);
            $em->flush();
        } else {
            $this->addFlash('error', 'faq_not_found');
        }
        return new RedirectResponse($this->generateUrl('faq_list_admin'));
    }

}