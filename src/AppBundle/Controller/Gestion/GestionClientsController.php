<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Repository\Gestion\AgendaEventsRepository;
use AppBundle\Repository\Gestion\OpportunityCallRepository;
use Mailjet\Api\RequestApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/clients")
 **/
class GestionClientsController extends BaseController
{

    /**
     * @Route("/list/{remoteId}", defaults={"remoteId" = null}, name="gestion_clients_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Client/list.html.twig")
     **/
    public function listAction(Request $request,Group $group = null)
    {
        $group = $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.clients');
        $datatable->buildDatatable(['group' => $group]);
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_client_view" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Client/view.html.twig")
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return Response
     **/
    public function viewAction(Request $request, Contact $contact){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            throw new AccessDeniedHttpException();

        return [
            'contact' => $contact
        ];
    }

    /**
     * @Route("/opportunity/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_client_opportunity" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Client/contact_opportunity.html.twig")
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return Response
     **/
    public function contactOpportunityAction(Request $request, Contact $contact){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            throw new AccessDeniedHttpException();

        $group = $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.opportunity');
        $datatable->buildDatatable(['group' => $group, 'data' => ['contact' => $contact->getId()]]);

        return [
            'datatable' => $datatable,
            'contact' => $contact
        ];
    }

    /**
     * @Route("/meetings/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_client_meetings" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Client/contact_meetings.html.twig")
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return Response
     **/
    public function contactMeetingsAction(Request $request, Contact $contact){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            throw new AccessDeniedHttpException();

        /** @var AgendaEventsRepository $agendaEventRepository */
        $agendaEventRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\AgendaEvent');
        $events = $agendaEventRepository->getEventsByContact($contact);

        return [
            'events' => $events,
            'contact' => $contact
        ];
    }

    /**
     * @Route("/calls/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_client_calls" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Client/contact_calls.html.twig")
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return Response
     **/
    public function contactCallsAction(Request $request, Contact $contact){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            throw new AccessDeniedHttpException();

        /** @var OpportunityCallRepository $opportunityCallsRepository */
        $opportunityCallsRepository = $this->getDoctrine()->getRepository('AppBundle:Gestion\\OpportunityCall');
        $calls = $opportunityCallsRepository->getCallByContact($contact);

        return [
            'calls' => $calls,
            'contact' => $contact
        ];
    }

    /**
     * @Route("/ajouter/{remoteId}", defaults={"remoteId" = null}, name="gestion_client_add", options={"expose"=true})
     * @Method({"GET","POST"})
     **/
    public function addAction(Request $request, Opportunity $opportunity = null){
        $contact = new Contact();
        $em = $this->getDoctrine()->getManager();
        if($opportunity) {
            if($this->getUser()->isEmployeeOf($opportunity->getOpportunityGroup()) || $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                $contact->setDataFromOpportunity($opportunity);
        }
        $form = $this->createForm($this->get('app.form.contact'),$contact,['isXmlHttp' => $request->isXmlHttpRequest()]);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(!$contact->getGroup())
                    $contact->setGroup($this->getUser()->getGroup());

                $contactId = $this->getMailjet()->addContact($contact);
                $contact->setMailjetId($contactId);
                $contact->setOwner($this->getUser());
                $em->persist($contact);
                $em->flush();
                $contact->setType(Contact::CONTACT);

                if($request->isXmlHttpRequest()) {
                    $opportunity->setContact($contact);
                    $em->persist($opportunity);
                    $em->flush();
                    return new JsonResponse(['success' => true, 'contact' => $contact->getId()]);
                }
                return new RedirectResponse($this->generateUrl('gestion_clients_list', ['remoteId' => $contact->getRemoteId() ]));
            }
        }
        if($request->isXmlHttpRequest())
            return new JsonResponse(['success' => false, 'content' => $this->renderView("AppBundle:Gestion:Client/add_js.html.twig",['form'  => $form->createView(), 'opportunity' => $opportunity])]);
        return $this->render("AppBundle:Gestion:Client/add.html.twig",['form'  => $form->createView()]);
    }

    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="gestion_client_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Client/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return Response
     **/
    public function editAction(Request $request, Contact $contact){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contact->getGroup()))
            throw new AccessDeniedHttpException();

        $contactList = $contact->getGroupContacts();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.contact'),$contact);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(!$contact->getMailjetId()) {
                    $contactId = $this->getMailjet()->addContact($contact);
                    $contact->setMailjetId($contactId);
                } elseif($contact->getGroupContacts() != $contactList) {
                   $this->getMailjet()->editContact($contact);
                }
                $contact->setType(Contact::CONTACT);
                $em->persist($contact);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_client_view',['remoteId' => $contact->getRemoteId()]));
            }
        }

        return [
            'form'    => $form->createView(),
            'contact' => $contact
        ];
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="gestion_client_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\Contact $contact
     * @return RedirectResponse
     **/
    public function deleteAction(Contact $contact){

        $em = $this->getDoctrine()->getManager();
        if($contact !== NULL){
            $em->remove($contact);
            $em->flush();
        } else {
            $this->addFlash('error', 'contact_not_found');
        }
        return new RedirectResponse($this->generateUrl('gestion_clients_list'));
    }

}