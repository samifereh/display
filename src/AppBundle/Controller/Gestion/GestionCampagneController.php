<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\BugComment;
use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\Gestion\BugCommentType;
use Mailjet\Api\RequestApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/campagnes")
 **/
class GestionCampagneController extends BaseController
{

    /**
     * @Route("/list/{remoteId}", defaults={"remoteId" = null}, name="gestion_campagnes_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Campagnes/list.html.twig")
     **/
    public function listAction(Request $request,Group $group = null)
    {
        if(!$group && $this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $group = $this->getCurrentGroup($request);

        $datatable = $this->get('app.datatable.campagne');
        $datatable->buildDatatable(['group' => $group]);
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_campagnes_view" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Campagnes/view.html.twig")
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return Response
     **/
    public function viewAction(Request $request, Campagne $campagne){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($campagne->getGroup()))
            throw new AccessDeniedHttpException();

        $status = $this->getMailjet()->checkStatusCampagne($campagne);
        if($status == -1 || (Campagne::STATUS[$status] != $campagne->getStatus())) {
            $campagne->setStatus($status == -1 ? Campagne::DRAFT : Campagne::STATUS[$status]);
            $em   = $this->getDoctrine()->getManager();
            $em->persist($campagne);
            $em->flush();
        }
        return [ 'campagne' => $campagne ];
    }

    /**
     * @Route("/ajouter", defaults={"remoteId" = null}, name="gestion_campagnes_add", options={"expose"=true})
     * @Method({"GET","POST"})
     **/
    public function addAction(Request $request, Group $group = null){
        $campagne  = new Campagne();
        $user     = $this->getUser();
        $em   = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.campagne'),$campagne);

        if(!$group) {
            if($this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
                $group = $this->getCurrentGroup($request);
            elseif($user->getGroup()->hasRole('ROLE_AGENCY')) {
                $group = $user->getGroup();
            } else {
                $group = $this->getCurrentGroup($request);
            }
        }

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $emailBuilder = $form->get('emailBuilder')->isClicked();
            if ($form->isValid() || $emailBuilder) {
                if(!$campagne->getGroup() && $group instanceof \AppBundle\Entity\Group)
                    $campagne->setGroup($group);
                $campagne->setOwner($this->getUser());
                if($form->get('submit')->isClicked())
                    $this->getMailjet()->createCampagne($campagne);
                $em->persist($campagne);
                $em->flush();
                if($emailBuilder)
                    return $this->redirectToRoute('gestion_campagnes_email_builder',['remoteId' => $campagne->getRemoteId()]);
                return $this->redirectToRoute('gestion_campagnes_list');
            }
        }
        return $this->render("AppBundle:Gestion:Campagnes/add.html.twig",['form'  => $form->createView()]);
    }

    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="gestion_campagnes_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Campagnes/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return Response
     **/
    public function editAction(Request $request, Campagne $campagne){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($campagne->getGroup()))
            throw new AccessDeniedHttpException();

        $isEdit = true;
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.campagne'),$campagne,['isEdit' => $isEdit]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            $emailBuilder = $form->get('emailBuilder')->isClicked();
            if ($form->isValid() || $emailBuilder) {
                if($emailBuilder)
                    return $this->redirectToRoute('gestion_campagnes_email_builder',['remoteId' => $campagne->getRemoteId()]);

                !$campagne->getMailjetId() ? $this->getMailjet()->createCampagne($campagne) : $this->getMailjet()->modifyCampagne($campagne);

                $em->persist($campagne);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_campagnes_view',['remoteId' => $campagne->getRemoteId()]));
            }
        }

        return [
            'form'     => $form->createView(),
            'campagne' => $campagne,
            'isEdit'   => $isEdit
        ];
    }

    /**
     * @Route("/duplicate/{remoteId}", options={"expose"=true}, name="gestion_campagnes_duplicate" )
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return Response
     **/
    public function duplicateAction(Request $request, Campagne $campagne){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($campagne->getGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $campagneClone = clone $campagne;
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN'))
            $campagneClone->setOwner($user);
        $em->persist($campagneClone);
        $em->flush();
        $this->addFlash("success", "la campagne a été dupliquer");
        return new RedirectResponse($this->generateUrl('gestion_campagnes_edit',['remoteId' => $campagneClone->getRemoteId()]));
    }


    /**
     * @Route("/send-draft/{remoteId}", options={"expose"=true}, name="gestion_campagnes_send_draft" )
     * @Method({"POST","GET"})
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return Response
     **/
    public function sendDraftAction(Request $request, Campagne $campagne){

        if($request->isXmlHttpRequest()) {
            $user = $this->getUser();
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($campagne->getGroup()))
                throw new AccessDeniedHttpException();
            if($request->request->has('email')) {
                if($this->getMailjet()->sendDraftTestCampagne($campagne,$request->request->get('email')))
                    return new JsonResponse(['success' => true, 'message' => 'Message Envoyer']);
                return new JsonResponse(['success' => false, 'message' => 'Erreur d\'envoie']);
            }
            return new JsonResponse(['success' => false, 'message' => 'Email introuvable']);
        }
        throw new AccessDeniedHttpException();
    }

    /**
     * @Route("/send/{remoteId}", options={"expose"=true}, name="gestion_campagnes_send" )
     * @Method({"POST","GET"})
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return Response
     **/
    public function sendAction(Request $request, Campagne $campagne){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($campagne->getGroup()))
            throw new AccessDeniedHttpException();

        if($campagne->getDate()) {
            $result = $this->getMailjet()->sendScheduleCampagne($campagne);
        } else {
            $result = $this->getMailjet()->sendImmediatlyCampagne($campagne);
        }
        if($result) {
            $this->addFlash("success", "Compagne activer avec success");
            $campagne->setStatus(is_bool($result) ? Campagne::FINISHED : $result);
            $em = $this->getDoctrine()->getManager();
            $em->persist($campagne);
            $em->flush();
        } else {
            $this->addFlash("error", "Erreur lors de l'activation du compagne");
        }
        return new RedirectResponse($this->generateUrl('gestion_campagnes_list'));
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="gestion_campagnes_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\Campagne $campagne
     * @return RedirectResponse
     **/
    public function deleteAction(Campagne $campagne){

        $em = $this->getDoctrine()->getManager();
        if($campagne !== NULL){
            $em->remove($campagne);
            $em->flush();
        } else {
            $this->addFlash('error', 'campagne_not_found');
        }
        return new RedirectResponse($this->generateUrl('gestion_campagnes_list'));
    }

    /**
     * @Route("/email-builder/{remoteId}", defaults={"remoteId" = null}, name="gestion_campagnes_email_builder", options={"expose"=true})
     * @Method({"GET","POST"})
     **/
    public function emailBuilderAction(Request $request, Campagne $campagne){
        $html = $campagne->getBodyHtml();
        if($request->request->has('export-textarea') && $request->request->get('export-textarea') != '') {
            $html = $request->request->get('export-textarea');

            $emogrifier = new \Pelago\Emogrifier();
            $cssDir = file_get_contents($this->container->get('kernel')->getRootDir().'/../src/AppBundle/Resources/public/lib/emailBuilder/_css/newsletter.css');
            $emogrifier->setHtml($html);
            $emogrifier->setCss($cssDir);
            $bodyContent = $emogrifier->emogrifyBodyContent();
            $campagne->setBodyHtml($bodyContent);
            $em = $this->getDoctrine()->getManager();
            $em->persist($campagne);
            $em->flush();
            return $this->redirectToRoute('gestion_campagnes_edit',['remoteId' => $campagne->getRemoteId()]);
        }
        return $this->render("AppBundle:Gestion:Campagnes/EmailBuilder/index.html.twig", ['html' => $html]);
    }
}