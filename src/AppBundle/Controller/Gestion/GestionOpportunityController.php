<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Gestion\AgendaEvent;
use AppBundle\Entity\Gestion\OpportunityCall;
use AppBundle\Entity\Gestion\OpportunityDocuments;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\Gestion\OpportunityPropositionRelance;
use AppBundle\Form\Type\Gestion\OpportunityCallType;
use AppBundle\Form\Type\Gestion\OpportunityCommentType;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Gestion\OpportunityComment;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\Gestion\OpportunityPropositionAbondonType;
use AppBundle\Form\Type\Gestion\OpportunityPropositionRelanceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/opportunity")
 **/
class GestionOpportunityController extends BaseController
{

    /**
     * @Route("/list/{remoteId}", defaults={"remoteId" = null}, name="gestion_opportunity_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Opportunity/list.html.twig")
     **/
    public function listAction(Request $request,Group $group = null)
    {
        $user         = $this->getUser();
        $data = $request->query->has('opportunity_search') ? $request->query->get('opportunity_search') : [];
        if(empty($data['dateStart'])) unset($data['dateStart']); else $data['dateStart'] = \DateTime::createFromFormat('d/m/Y',$data['dateStart']);
        if(empty($data['dateEnd'])) unset($data['dateEnd']); else $data['dateEnd'] = \DateTime::createFromFormat('d/m/Y',$data['dateEnd']);
        if(empty($data['apportPersonnel'])) $data['apportPersonnel'] = null;
        if(empty($data['budgetTerrainFrom']))  unset($data['budgetTerrainFrom']); else $data['budgetTerrainFrom'] = (int)$data['budgetTerrainFrom'];
        if(empty($data['budgetTerrainTo']))  unset($data['budgetTerrainTo']); else $data['budgetTerrainTo'] = (int)$data['budgetTerrainTo'];
        if(empty($data['budgetMaisonFrom']))  unset($data['budgetMaisonFrom']); else $data['budgetMaisonFrom'] = (int)$data['budgetMaisonFrom'];
        if(empty($data['budgetMaisonTo']))  unset($data['budgetMaisonTo']); else $data['budgetMaisonTo'] = (int)$data['budgetMaisonTo'];
        if(empty($data['budgetTotalFrom']))  unset($data['budgetTotalFrom']); else $data['budgetTotalFrom'] = (int)$data['budgetTotalFrom'];
        if(empty($data['apportPersonnel']))  unset($data['apportPersonnel']); else $data['apportPersonnel'] = (int)$data['apportPersonnel'];
        if(empty($data['budgetTotalTo']))  unset($data['budgetTotalTo']); else $data['budgetTotalTo'] = (int)$data['budgetTotalTo'];
        if(isset($data['compromisTerrain'])) $data['compromisTerrain'] = (bool)$data['compromisTerrain'];
        if(isset($data['contratConstruction'])) $data['contratConstruction'] = (bool)$data['contratConstruction'];
        if(isset($data['permisConstruireDeposer'])) $data['permisConstruireDeposer'] = (bool)$data['permisConstruireDeposer'];

        $form = $this->createForm($this->get("app.form.opportunity_search"),$data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $data = $form->getData();
            if(!empty($data['dateStart'])) $data['dateStart'] = ['date' => $data['dateStart']->format('Y-m-d')];
            if(!empty($data['dateEnd'])) $data['dateEnd'] = ['date' => $data['dateEnd']->format('Y-m-d')];
            if(!empty($data['commercial'])) $data['commercial'] = $data['commercial']->getId();
            if(!empty($data['group'])) $data['group'] = $data['group']->getId();
            if(!empty($data['bigGroups'])) $data['bigGroups'] = $data['bigGroups']->getId();
            if(!empty($data['contact'])) $data['contact'] = $data['contact']->getId();

            if($form->get('save')->isClicked()) {
                $dataQuery = http_build_query($data);
                return $this->redirectToRoute('gestion_contact_lists_add', [
                    'opportunity_search' => $dataQuery
                ], 307);
            }
        }

        $group = $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.opportunity');
        $datatable->buildDatatable(['group' => $group,'data' => $data+['type' => 'list']]);

        return [
            'datatable' => $datatable,
            'form'      => $form->createView(),
            'data'      => $data
        ];
    }

    /**
     * @Route("/voir/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_opportunity_view" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:Opportunity/view.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function viewAction(Opportunity $opportunity){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();
        return [ 'opportunity' => $opportunity ];
    }

    /**
     * @Route("/ajouter", name="gestion_opportunity_add" )
     * @Method({"GET","POST"})
     * @Template("AppBundle:Gestion:Opportunity/add.html.twig")
     **/
    public function addAction(Request $request){

        $opportunity = new Opportunity();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.opportunity'),$opportunity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $opportunity->setCommercial($opportunity->getAd()->getOwner());
                $opportunity->setOpportunityGroup($opportunity->getAd()->getGroup());
                $em->persist($opportunity);
                $em->flush();

                $this->get("app.mailer.manager")->sendNewOpportunity(
                    [ 'opportunity' => $opportunity ]
                );
                return new RedirectResponse($this->generateUrl('gestion_opportunity_list'));
            }
        }
        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity
        ];
    }

    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="gestion_opportunity_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function editAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.opportunity'),$opportunity);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $opportunity->setCommercial($opportunity->getAd()->getOwner());
                $opportunity->setOpportunityGroup($opportunity->getAd()->getGroup());
                $em->persist($opportunity);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_opportunity_view',['remoteId' => $opportunity->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity
        ];
    }

    /**
     * @Route("/convert/{remoteId}", options={"expose"=true}, name="gestion_opportunity_convert" )
     * @Method({"POST","GET"})
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function convertAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $opportunity->getContact()->setType(Contact::CLIENT);
        $opportunity->setStatus(Opportunity::ARCHIVED);
        $em->persist($opportunity);
        $em->flush();


        return $this->redirectToRoute('gestion_client_view',['remoteId' => $opportunity->getContact()->getRemoteId()]);
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="gestion_opportunity_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return RedirectResponse
     **/
    public function deleteAction(Opportunity $opportunity){

        $em = $this->getDoctrine()->getManager();
        if($opportunity !== NULL){
            $em->remove($opportunity);
            $em->flush();
        } else {
            $this->addFlash('error', 'contact_not_found');
        }
        return new RedirectResponse($this->generateUrl('gestion_opportunity_list'));
    }


    /* GESTION COMMENTAIRES */
    /**
     * @Route("/comments/{remoteId}", name="gestion_opportunity_comments" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_comments.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function commentsAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $comment = new OpportunityComment();
        $form = $this->createForm(new OpportunityCommentType(),$comment);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $comment->setOwner($user);
                $opportunity->addComment($comment);
                $em->persist($opportunity);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_opportunity_comments',['remoteId' => $opportunity->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity
        ];
    }

    /* GESTION CALLS */
    /**
     * @Route("/calls/{remoteId}", name="gestion_opportunity_calls" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_calls.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function callsAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $call = new OpportunityCall();
        $form = $this->createForm($this->get('app.form.opportunity_calls'),$call,['group'=>$opportunity->getOpportunityGroup()]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $call->setOwner($user);
                if(!$call->getResponsible() && $user->hasRole('ROLE_SALESMAN')) {
                    $call->setResponsible($user);
                }
                $opportunity->addCall($call);
                $em->persist($opportunity);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_opportunity_calls',['remoteId' => $opportunity->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity
        ];
    }

    /* GESTION MEETINGS */
    /**
     * @Route("/meetings/{remoteId}", name="gestion_opportunity_meetings" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_meetings.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function meetingsAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $agendaEvent = new AgendaEvent();
        $form = $this->createForm($this->get('app.form.opportunity_meetings'),$agendaEvent,['group'=>$opportunity->getOpportunityGroup()]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $agendaEvent->setOwner($user);
                $opportunity->addEvent($agendaEvent);
                $em->persist($opportunity);
                $em->flush();

                foreach ($agendaEvent->getSharedUsers() as $currentUser) {
                    if($currentUser != $user) {
                        $mail = new \Swift_Message();
                        $mail->setSubject('Rendez-vous -'. $agendaEvent->getTitle())
                            ->setFrom($this->getParameter('from_adresse'),$this->getParameter('from_name'))
                            ->setTo($currentUser->getEmail())
                            ->setBody(
                                $this->renderView(
                                    'AppBundle:Emails:notificate_meeting.html.twig', [ 'agendaEvent' => $agendaEvent ]
                                ),
                                'text/html'
                            );
                        $this->get('mailer')->send($mail);
                    }
                }

                return new RedirectResponse($this->generateUrl('gestion_opportunity_meetings',['remoteId' => $opportunity->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity
        ];
    }

    /* GESTION DOCUMENTS */
    /**
     * @Route("/documents/{remoteId}", name="gestion_opportunity_documents" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_documents.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function documentsAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        if(!$session->get('brouillon_opportunity_id')) {
            $brouillon = new Brouillon();
            $brouillon->setSessionId($session->getId());
            $em->persist($brouillon);
            $em->flush();
            $session->set('brouillon_opportunity_id',$brouillon->getId());
        } else {
            $brouillonRepository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Brouillon');
            $brouillon           = $brouillonRepository->find($session->get('brouillon_opportunity_id'));
        }

        $documents = new OpportunityDocuments();
        $form = $this->createForm($this->get('app.form.opportunity_documents'),$documents);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {

                if(!$opportunity->getEmail()) {
                    $this->addFlash("error", 'Aucune adresse email n\'a été renseigner pour cette opportunité.');
                    return new RedirectResponse($this->generateUrl('gestion_opportunity_view',['remoteId' => $opportunity->getRemoteId()]));
                }

                /** @var \AppBundle\Entity\Document $document */
                foreach ($brouillon->getDocuments() as $document) {
                    $document->setGroup($opportunity->getOpportunityGroup());
                    $documents->addDocument($document);
                }

                $documents->setOwner($user);
                $opportunity->addDocument($documents);
                $em->persist($opportunity);
                $em->flush();

                if($brouillon) {
                    $brouillon->setDocuments(null);
                    $em->remove($brouillon);
                    $em->flush();
                    $session->remove('brouillon_opportunity_id');
                }

                $message = new \Swift_Message();
                $message->setSubject($opportunity->getOpportunityGroup()->getName().' - documents à propos '.$opportunity->getTitle())
                    ->setFrom('no-reply@display-immo.fr','Display-immo')
                    ->setTo($opportunity->getEmail())
                    ->setBody(
                        $documents->getMessage(),
                        'text/html'
                    );

                foreach ($documents->getDocuments() as $document)
                    $message->attach(\Swift_Attachment::fromPath($this->container->get('templating.helper.assets')->getUrl('/uploads/opportunity/'.$document->getName(),null)));
                $this->get('mailer')->send($message);
                $this->addFlash("success", 'message envoyée à '.$opportunity->getEmail());


                return new RedirectResponse($this->generateUrl('gestion_opportunity_documents',['remoteId' => $opportunity->getRemoteId()]));
            }
        }

        return [
            'form'        => $form->createView(),
            'opportunity' => $opportunity,
            'brouillon'   => $brouillon
        ];
    }

    /**
     *
     * @Route("/status/{remoteId}", options={"expose"=true}, name="opportunity_switch_statut")
     * @Method({"GET","POST"})
     */
    public function switchPublishedStatusAction(Request $request, Opportunity $opportunity)
    {
        if($request->isXmlHttpRequest()) {
            if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$this->getUser()->isEmployeeOf($opportunity->getOpportunityGroup()))
                throw new AccessDeniedHttpException();
            $stat = null;
            $em = $this->getDoctrine()->getManager();
            switch ($opportunity->getStatus()) {
                case Opportunity::CLOSED:
                    $stat = Opportunity::OPEN;
                    $opportunity->setStatus($stat);
                    break;
                case Opportunity::OPEN:
                    $stat = Opportunity::CLOSED;
                    $opportunity->setStatus($stat);
                    break;
            }
            $em->persist($opportunity);
            $em->flush();

            return new JsonResponse(['_callback' => $stat]);
        }
        throw new AccessDeniedHttpException();
    }


    /* GESTION PROPOSITIONS */
    /**
     * @Route("/propositions/{remoteId}", name="gestion_opportunity_propositions" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_propositions.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function propositionsAction(Request $request, Opportunity $opportunity){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($opportunity->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        if(!$opportunity->getPropositionDetails())
            return $this->redirectToRoute('gestion_propositions_edit',['remoteId' => $opportunity->getRemoteId()]);

        $hasError = false;
        $errors = [];
        $em = $this->getDoctrine()->getManager();
        $proposition = new OpportunityProposition();
        $form = $this->createForm($this->get('app.form.opportunity_proposition'),$proposition,['group'=>$opportunity->getOpportunityGroup(), 'opportunity' => $opportunity]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $proposition->setOwner($user);
                if(!$proposition->getCommercials() && $user->hasRole('ROLE_SALESMAN')) {
                    $proposition->addCommercial($user);
                }
                $opportunity->addProposition($proposition);
                $em->persist($opportunity);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_opportunity_propositions',['remoteId' => $opportunity->getRemoteId()]));
            } else {
                $hasError = true;
                $fields = $form->getErrors();
                foreach ( $fields as $field ) {
                    $errors[$field->getOrigin()->getConfig()->getOption('label')] = $field->getMessage();
                }
            }
        }

        return [
            'form'        => $form->createView(),
            'hasError'    => $hasError,
            'errors'      => $errors,
            'opportunity' => $opportunity
        ];
    }

    /**
     * @Route("/proposition/view/{remoteId}", name="gestion_propositions_view" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/view_propositions_details.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function propositionsDetailsViewAction(Request $request, Opportunity $opportunity){
        return [
            'opportunity' => $opportunity
        ];
    }

    /**
     * @Route("/proposition/edit/{remoteId}", name="gestion_propositions_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/edit_propositions_details.html.twig")
     * @param \AppBundle\Entity\Gestion\Opportunity $opportunity
     * @return Response
     **/
    public function propositionsDetailsEditAction(Request $request, Opportunity $opportunity){

        $proposition = $opportunity->getPropositionDetails();
        if(!$proposition)
            $proposition = new OpportunityProposition();

        $em = $this->getDoctrine()->getManager();
        $hasError = false;
        $errors = [];
        $form = $this->createForm($this->get('app.form.opportunity_proposition_details'),$proposition,['group'=>$opportunity->getOpportunityGroup()]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(!$opportunity->getPropositionDetails()) {
                    $opportunity->setPropositionDetails($proposition);
                    $em->persist($opportunity);
                    $em->flush();
                } else {
                    $em->persist($proposition);
                    $em->flush();
                }
                $this->addFlash("success", 'Détails enregistrer avec succèes');
            } else {
                $hasError = true;
                $fields = $form->getErrors();
                foreach ( $fields as $field ) {
                    $errors[$field->getOrigin()->getConfig()->getOption('label')] = $field->getMessage();
                }
            }
        }

        return [
            'form'        => $form->createView(),
            'proposition' => $proposition,
            'opportunity' => $opportunity,
            'hasError' => $hasError,
            'errors' => $errors
        ];
    }

    /**
     * @Route("/propositions/view/{remoteId}", name="gestion_opportunity_propositions_view" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/proposition_view.html.twig")
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     * @return Response
     **/
    public function propositionViewAction(Request $request, OpportunityProposition $proposition){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($proposition->getOpportunity()->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        return [
            'proposition' => $proposition
        ];
    }

    /**
     * @Route("/propositions/edit/{remoteId}", name="gestion_opportunity_propositions_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:Opportunity/proposition_edit.html.twig")
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     * @return Response
     **/
    public function propositionEditAction(Request $request, OpportunityProposition $proposition){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($proposition->getOpportunity()->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $hasError = false;
        $errors = [];
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.opportunity_proposition'),$proposition,['group'=>$proposition->getOpportunity()->getOpportunityGroup(), 'opportunity' => $proposition->getOpportunity()]);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $session = $request->getSession();
                $vars = $session->has('vars') ? $session->get('vars') : null;
                if(isset($vars['propositions'])) {
                    /** @var \AppBundle\Repository\DocumentRepository $documentRepository */
                    $documentRepository = $em->getRepository('AppBundle:Document');
                    foreach ($vars['propositions'] as $var) {
                        $file = $documentRepository->find($var['id']);
                        if($file) $proposition->addDocument($file);
                    }
                    $session->remove('vars');
                }

                $em->persist($proposition);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_opportunity_propositions',['remoteId' => $proposition->getOpportunity()->getRemoteId()]));
            } else {
                $hasError = true;
                $fields = $form->getErrors();
                foreach ( $fields as $field ) {
                    $errors[$field->getOrigin()->getConfig()->getOption('label')] = $field->getMessage();
                }
            }
        }

        return [
            'form'        => $form->createView(),
            'proposition' => $proposition,
            'hasError'    => $hasError,
            'errors'      => $errors
        ];
    }

    /**
     * @Route("/propositions/supprimer/{remoteId}", options={"expose"=true}, name="gestion_opportunity_propositions_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     * @return RedirectResponse
     **/
    public function propositionDeleteAction(OpportunityProposition $proposition){

        $opportunityRemoteId = $proposition->getOpportunity()->getRemoteId();
        $em = $this->getDoctrine()->getManager();
        if($proposition !== NULL){
            $em->remove($proposition);
            $em->flush();
        } else {
            $this->addFlash('error', 'contact_not_found');
        }
        return new RedirectResponse($this->generateUrl('gestion_opportunity_propositions',['remoteId' => $opportunityRemoteId ]));
    }

    /**
     * @Route("/propositions/abondonner/{remoteId}", options={"expose"=true}, name="gestion_opportunity_propositions_abondonner" )
     * @Method({"POST","GET"})
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     * @return Response
     **/
    public function propositionAbondonnerAction(Request $request, OpportunityProposition $proposition){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($proposition->getOpportunity()->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OpportunityPropositionAbondonType(),$proposition);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $proposition->setStatus(OpportunityProposition::ABANDONNER);
                $em->persist($proposition);
                $em->flush();
                return new JsonResponse(['success' => true, 'content' => $this->renderView('AppBundle:Gestion:Opportunity/proposition_form_abondon.html.twig',['form' => $form->createView(), 'proposition' => $proposition]) ]);
            } else {
                return new JsonResponse(['success' => false, 'content' => $this->renderView('AppBundle:Gestion:Opportunity/proposition_form_abondon.html.twig',['form' => $form->createView(), 'proposition' => $proposition]) ]);
            }
        }
    }

    /**
     * @Route("/propositions/relancer/{remoteId}", options={"expose"=true}, name="gestion_opportunity_propositions_relancer" )
     * @Method({"POST","GET"})
     * @param \AppBundle\Entity\Gestion\OpportunityProposition $proposition
     * @return Response
     **/
    public function propositionRelancerAction(Request $request, OpportunityProposition $proposition){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($proposition->getOpportunity()->getOpportunityGroup()))
            throw new AccessDeniedHttpException();

        $opportunityPropositionRelance = new OpportunityPropositionRelance();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new OpportunityPropositionRelanceType(),$opportunityPropositionRelance);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $opportunityPropositionRelance->setOwner($user);
                $proposition->setStatus(OpportunityProposition::A_RELANCER);
                $proposition->addRelance($opportunityPropositionRelance);
                $em->persist($proposition);
                $em->flush();
                return new JsonResponse(['success' => true, 'content' => $this->renderView('AppBundle:Gestion:Opportunity/proposition_form_relance.html.twig',['form' => $form->createView(), 'proposition' => $proposition]) ]);
            } else {
                return new JsonResponse(['success' => false, 'content' => $this->renderView('AppBundle:Gestion:Opportunity/proposition_form_relance.html.twig',['form' => $form->createView(), 'proposition' => $proposition]) ]);
            }
        }
    }
}