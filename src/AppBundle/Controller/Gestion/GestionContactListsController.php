<?php

namespace AppBundle\Controller\Gestion;

use AppBundle\Controller\BaseController;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use Mailjet\Api\RequestApi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/contact_list")
 **/
class GestionContactListsController extends BaseController
{

    /**
     * @Route("/list/{remoteId}", defaults={"remoteId" = null}, name="gestion_contact_lists_list" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:ContactLists/list.html.twig")
     **/
    public function listAction(Request $request,Group $group = null)
    {
        $group = $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.contact_lists');
        $datatable->buildDatatable(['group' => $group]);
        return ['datatable' => $datatable];
    }

    /**
     * @Route("/voir/{remoteId}", defaults={"remoteId" = null}, options={"expose"=true}, name="gestion_contact_lists_view" )
     * @Method({"GET"})
     * @Template("AppBundle:Gestion:ContactLists/view.html.twig")
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     * @return Response
     **/
    public function viewAction(Request $request, ContactList $contactList){
        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contactList->getGroup()))
            throw new AccessDeniedHttpException();

        $group = $this->getCurrentGroup($request);
        $datatable = $this->get('app.datatable.contacts');
        $datatable->buildDatatable(['group' => $group, 'contactList' => $contactList]);

        return [
            'contactList' => $contactList,
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/ajouter/{remoteId}", defaults={"remoteId" = null}, name="gestion_contact_lists_add", options={"expose"=true})
     * @Method({"GET","POST"})
     **/
    public function addAction(Request $request, Group $group = null){
        $contactList = new ContactList();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.contact_lists'),$contactList);
        $user = $this->getUser();
        $data = $request->query->has('opportunity_search') ? $request->query->get('opportunity_search') : [];
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if($data) {
                    parse_str($data, $filters);
                    /** @var \AppBundle\Repository\Gestion\OpportunityRepository $opportunityRepository */
                    $opportunityRepository = $em->getRepository('AppBundle:Gestion\\Opportunity');
                    $opportunitys = $opportunityRepository->getOpportunityByFilter($filters, $user, $group);
                    if(count($opportunitys) > 0) {
                        /** @var \AppBundle\Entity\Gestion\Opportunity $opportunity */
                        foreach ($opportunitys as $opportunity) {
                            if($opportunity->getContact()) {
                                $contactList->addContact($opportunity->getContact());
                                $this->getMailjet()->affectContactToContactList($contactList, $opportunity->getContact());
                            }
                        }
                    }
                }
                if(!$contactList->getGroup()) {
                    $group = $this->getUser()->getGroup();
                    $contactList->setGroup($group);
                }
                $contactList->setOwner($user);
                $listId = $this->getMailjet()->addContactList($contactList->getGroup()->getName().' / '.$contactList->getName());
                $contactList->setMailjetId($listId);
                $em->persist($contactList);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_contact_lists_list', ['remoteId' => $contactList->getGroup()->getRemoteId() ]));
            }
        }
        if($request->isXmlHttpRequest())
            return new JsonResponse(['success' => false, 'content' => $this->renderView("AppBundle:Gestion:Contact/add_js.html.twig",['form'  => $form->createView(), 'opportunity' => $opportunity])]);
        return $this->render("AppBundle:Gestion:ContactLists/add.html.twig",['form'  => $form->createView()]);
    }

    /**
     * @Route("/editer/{remoteId}", options={"expose"=true}, name="gestion_contact_lists_edit" )
     * @Method({"POST","GET"})
     * @Template("AppBundle:Gestion:ContactLists/edit.html.twig")
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     * @return Response
     **/
    public function editAction(Request $request, ContactList $contactList){

        $user = $this->getUser();
        if(!$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN') && !$user->isEmployeeOf($contactList->getGroup()))
            throw new AccessDeniedHttpException();

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->get('app.form.contact_lists'),$contactList);
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if(!$contactList->getMailjetId()) {
                    $listId = $this->getMailjet()->addContactList($contactList->getGroup()->getName().' / '.$contactList->getName());
                    $contactList->setMailjetId($listId);
                }
                if(!$contactList->getOwner())
                    $contactList->setOwner($user);

                $em->persist($contactList);
                $em->flush();
                return new RedirectResponse($this->generateUrl('gestion_contact_lists_view',['remoteId' => $contactList->getRemoteId()]));
            }
        }

        return [
            'form'         => $form->createView(),
            'contactList' => $contactList
        ];
    }

    /**
     * @Route("/supprimer/{remoteId}", options={"expose"=true}, name="gestion_contact_lists_delete" )
     * @Method({"GET"})
     * @Template
     * @param \AppBundle\Entity\Gestion\ContactList $contactList
     * @return RedirectResponse
     **/
    public function deleteAction(ContactList $contactList){

        $em = $this->getDoctrine()->getManager();
        if($contactList !== NULL){
            $em->remove($contactList);
            $em->flush();
        } else {
            $this->addFlash('error', 'contact_not_found');
        }
        return new RedirectResponse($this->generateUrl('gestion_contact_lists_list'));
    }

}