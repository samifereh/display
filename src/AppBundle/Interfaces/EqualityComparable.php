<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 19/07/2016
 * Time: 10:45
 */

namespace AppBundle\Interfaces;


interface EqualityComparable
{
    /**
     * Return if the current object is the same as $o
     * @param $o The object to compare
     * @return bool
     * @throws NullPointerException If the specified object is null
     * @throws ClassCastException If the specified object's type prevents it from being compared to this object.
     */
    public function equals($o);
}