<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 19/07/2016
 * Time: 10:31
 */

namespace AppBundle\Interfaces;


use AppBundle\Exceptions\ClassCastException;
use AppBundle\Exceptions\NullPointerException;

interface Comparable
{
    /**
     * @param $obj
     * @return int -1 if the current object is lower than $obj, 1 if the current object is upper than $obj and 0 if the
     * current object equals $obj
     * @throws NullPointerException If the specified object is null
     * @throws ClassCastException If the specified object's type prevents it from being compared to this object.
     */
    public function compareTo($obj);
}