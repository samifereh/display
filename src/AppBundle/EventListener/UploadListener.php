<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Brouillon;
use AppBundle\Entity\Document;
use Doctrine\Common\Persistence\ObjectManager;
use Oneup\UploaderBundle\Event\PostPersistEvent;
use Oneup\UploaderBundle\Uploader\Response\EmptyResponse;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\DependencyInjection\Container;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UploadListener
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var Container
     */
    private $container;

    private $file;

    private $event;

    /** @var  TokenInterface::getUser() $user */
    private $user;

    public function __construct(ObjectManager $manager, Container $container,$user)
    {
        $this->manager      = $manager;
        $this->container    = $container;
        $this->user         = $user;
    }

    public function onUpload(PostPersistEvent $event)
    {
        $uploadType = $event->getRequest()->get('type');
        $this->event = $event;
        $this->file = $event->getFile();
        $allowedTypes = $this->container->getParameter("poliris_data")["image"]["allowed_types"];
        $allowedTypes[] = "application/pdf";
        if(in_array($this->file->getMimeType() ,$allowedTypes)) {

            switch ($uploadType) {
                case 'housemodel':
                    $this->handleHouseModelUpload($event);
                    break;
                case 'ad_duo':
                case 'ad':
                    $this->handleAdUpload($event);
                    break;
                case 'opportunity':
                    $this->handleOpportunityUpload($event);
                    break;
                case 'proposition':
                    $this->handlePropositionUpload($event);
                    break;
                case 'images':
                    $this->handleImagesUpload($event);
                    break;
                default:
                    $this->raiseExceptionAndRemoveFile("upload_not_allowed");
                    break;
            }
        }else{
            throw new UploadException($this->container->get('translator.default')->trans('image.error.message.allowed_types',array(),'commiti'));
        }
    }

    private function raiseExceptionAndRemoveFile($msg){

        $fs = new Filesystem();

        try {
            $fs->remove($this->file->getPathname());
            throw new UploadException($msg);

        } catch (IOExceptionInterface $e) {
            throw new UploadException($e->getMessage());
        }

    }


    private function handlePropositionUpload(&$event)
    {

        /** @var Request $request */
        $request = $event->getRequest();
        $session = $request->getSession();
        $sessionId = $session->getId();
        $response = $event->getResponse();
        /** @var \Symfony\Component\HttpFoundation\File\File $file */
        $file     = $event->getFile();
        $fileName = $file->getFilename();

        $document = new Document();
        $document->setDocument($this->file);
        $document->setOriginalFileName($fileName);

        $this->manager->persist($document);
        $this->manager->flush();

        $vars = $session->get('vars') ? $session->get('vars') : [];
        if(!isset($vars['propositions']))
            $vars['propositions'] = [];

        $vars['propositions'][] = [
            'id'     => $document->getId(),
            'name'   => $fileName,
            'path'   => 'uploads/opportunity/'.$file->getBaseName(),
            'type'   => $file->getMimeType()
        ];
        $session->set('vars',$vars);
        $response['id'] = $document->getId();
    }

    private function handleHouseModelUpload(&$event){

        /** @var Request $request */
        $request = $event->getRequest();
        $brouillonId = (int) $request->get('brouillonId');
        $fileName = $request->get('fileName');
        $response = $event->getResponse();

        $brouillonRepository = $this->manager->getRepository('AppBundle:Brouillon');
        $brouillon           = $brouillonRepository->find($brouillonId);
        $document = new Document();
        $document->setDocument($this->file);
        $document->setOriginalFileName($fileName);
        if($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || (
                $this->container->get('security.authorization_checker')->isGranted('ROLE_BRANDLEADER') && $this->user->getGroup()->hasRole('ROLE_GROUP')
            )
        ) {
            $group = $this->container->get('app.user.service')->checkUserAgency($request,true);
        } else {
            $group = $this->user->getAgencyGroup();
        }
        $document->setGroup($group);
        $document->setBrouillon($brouillon);

        $this->manager->persist($document);
        $this->manager->flush();
        $response['id'] = $document->getId();
    }

    private function handleAdUpload(&$event){

        /** @var Request $request */
        $request = $event->getRequest();
        $brouillonId = (int) $request->get('brouillonId');
        $fileName = $request->get('fileName');
        $response = $event->getResponse();

        $brouillonRepository = $this->manager->getRepository('AppBundle:Brouillon');
        $brouillon           = $brouillonRepository->find($brouillonId);
        $document = new Document();
        $document->setDocument($this->file);
        $document->setOriginalFileName($fileName);
        if($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN') || (
                $this->container->get('security.authorization_checker')->isGranted('ROLE_BRANDLEADER') && $this->user->getGroup()->hasRole('ROLE_GROUP')
            )
        ) {
            $group = $this->container->get('app.user.service')->checkUserAgency($request,true);
        } else {
            $group = $this->user->getAgencyGroup();
        }
        $document->setGroup($group);
        $document->setBrouillon($brouillon);

        $this->manager->persist($document);
        $this->manager->flush();
        $response['id'] = $document->getId();
    }

    private function handleOpportunityUpload(&$event){

        /** @var Request $request */
        $request = $event->getRequest();
        $brouillonId = (int) $request->get('brouillonId');
        $fileName = $request->get('fileName');
        $response = $event->getResponse();

        $brouillonRepository = $this->manager->getRepository('AppBundle:Brouillon');
        $brouillon           = $brouillonRepository->find($brouillonId);
        $document = new Document();
        $document->setDocument($this->file);
        $document->setOriginalFileName($fileName);
        $document->setBrouillon($brouillon);

        $this->manager->persist($document);
        $this->manager->flush();
        $response['id'] = $document->getId();
    }

    private function handleImagesUpload(&$event){

        /** @var Request $request */
        $request = $event->getRequest();
        $response = $event->getResponse();
        $file     = $event->getFile();

        /** @var Brouillon $brouillon */
        $brouillonId = (int) $request->get('id');
        $brouillonRepository = $this->manager->getRepository('AppBundle:Brouillon');
        $brouillon           = $brouillonRepository->find($brouillonId);
        $brouillon->setImagePrincipale('uploads/others/'.$file->getBaseName());
        $this->manager->persist($brouillon);
        $this->manager->flush();

        $response['success'] = true;
    }

}