<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class RequestListener
{
    protected $container;
    protected $router;

    public function __construct(ContainerInterface $container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if($this->container->get('security.token_storage')->getToken() && ($event->getRequest()->get('_route') != 'payment_index')) {

            /**
             * restriction for ARIA marketing
             * @see User Entity : RESTRICTED_EMAILS
             */
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if($user != 'anon.' && !$user->hasFullAccess() && $this->router->getContext()->getPathInfo() != "/"){
                //check if allow
                $notAllowUrls = [
                    "/passerelles/",
                    "/group/",
                    "/agence/",
                    "/invoices/",
                    "/modeles/",
                ];
                foreach ($notAllowUrls as $xUrl){
                    if(stripos($this->router->getContext()->getPathInfo(), $xUrl) !== false ){
                        $response = new RedirectResponse($this->router->generate('ads_index'));
                        $event->setResponse($response);
                        return;
                    }
                }
            }
            if($user != 'anon.' && !$this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                /** @var \AppBundle\Entity\Group $group */
                $group = $user->getGroup();
                $groups = $user->getGroups();
                $nbBlocked = 0;
                $gRemoteId = "";
                foreach ($groups as $grp){
                    if($grp->getBlocked()) {
                        $nbBlocked++;
                        $gRemoteId = $grp->getRemoteId();
                    }
                }
                if( $nbBlocked == $groups->count()) {
                    $response = new RedirectResponse($this->router->generate('payment_index', array(
                        'remoteId' => $gRemoteId
                    )));
                    $event->setResponse($response);
                } elseif(!is_null($group->getParentGroup()) && $group->getParentGroup()->getBlocked()) {
                    $response = new RedirectResponse($this->router->generate('payment_blocked'));
                    $event->setResponse($response);
                }
            }
        }
    }

}