<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Ad;
use AppBundle\Entity\BienImmobilier;
use AppBundle\Entity\Document;
use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\Gestion\BugComment;
use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\Gestion\ContactList;
use AppBundle\Entity\Gestion\Discussion;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Payment\Order;
use AppBundle\Entity\Program;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\Common\EventSubscriber;
use AppBundle\Entity\User;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class UpdateListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate'
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if(in_array(get_class($entity),
            [
                OpportunityProposition::class, Campagne::class, BugComment::class, Bug::class,
                ContactList::class,Discussion::class,Opportunity::class,Contact::class,Document::class,
                Group::class, User::class, HouseModel::class, Ad::class, Program::class, BienImmobilier::class
                /*Order::class*/
            ]
        )) {
            $remoteId = null;
            switch(get_class($entity)) {
                case (User::class):
                    $remoteId = 'immo-user-'.$entity->getId();
                break;
                case (HouseModel::class):
                    $remoteId = 'immo-model-'.$entity->getId();
                    $entity->setPercent($entity->calculatePercent());
                break;
                case (Ad::class):
                    $remoteId  = 'immo-ad-'.$entity->getId();
                    $group     = $entity->getProgram() ? $entity->getProgram()->getGroup() : $entity->getGroup();
                    $prefix    = $group->getPrefix() ? $group->getPrefix() : 'COMM';
                    $reference = $prefix.'-'.($entity->getId() + 1000).$entity->getNbRemonte();
                    $entity->setReference($reference);
                    $entity->setPercent($entity->calculatePercent());
                    if($entity->getProgram()) {
                        $entity->getProgram()->setPercent($entity->getProgram()->calculatePercent());
                    }
                break;
                case (BienImmobilier::class):
                    $remoteId = 'immo-bien-'.$entity->getId();
                break;
                case (Program::class):
                    $remoteId = 'immo-program-'.$entity->getId();
                    $reference = 'PRO-'.$entity->getGroup()->getPrefix().'-'.($entity->getId() + 1000).$entity->getNbRemonte();
                    $entity->setReference($reference);
                    $entity->setPercent($entity->calculatePercent());
                break;
//                case (Order::class):
//                    $remoteId = 'immo-invoice-'.$entity->getId();
//                break;
                case (Group::class):
                    $remoteId = 'immo-group-'.$entity->getId();
                break;
                case (Contact::class):
                    $remoteId = 'immo-contact-'.$entity->getId();
                break;
                case (Bug::class):
                    $remoteId = 'immo-bug-'.$entity->getId();
                break;
                case (Opportunity::class):
                    $remoteId = 'immo-opportunity-'.$entity->getId();
                break;
                case (Discussion::class):
                    $remoteId = 'immo-discussion-'.$entity->getId();
                break;
                case (ContactList::class):
                    $remoteId = 'immo-contact_lists-'.$entity->getId();
                break;
                case (Campagne::class):
                    $remoteId = 'immo-compagne-'.$entity->getId();
                break;
                case (OpportunityProposition::class):
                    $remoteId = 'immo-opportunity_proposition-'.$entity->getId();
                break;
                case (Document::class):
                    $remoteId = 'immo-document-'.$entity->getId();
                    $remoteId = md5($remoteId);
                    $entity->setRemoteId($remoteId);
                    return $entity;
                break;
            }
            if($remoteId) {
                $remoteId = md5($remoteId);
                $entity->setRemoteId($remoteId);
            }
            $entityManager->persist($entity);
            $entityManager->flush();
            return $entity;
        }
        return null;
    }
}