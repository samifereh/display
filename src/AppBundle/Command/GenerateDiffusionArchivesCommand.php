<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\PortalRegistration;
use AppBundle\Entity\Group;
use DirectoryIterator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\File;
use ZipArchive;

class GenerateDiffusionArchivesCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;
    protected $currentDir;
    protected $exportDir;
    protected $input;
    protected $output;
    protected $exportHelper;
    /**
     * contain mapping between portal and agencies already diffused
     * @var array
     */
    protected $freePortalUsed;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->currentDir = $this->container->get('kernel')->getRootDir() . '/..';
        $this->exportDir = 'web/export';
        /** @var \AppBundle\Service\ExportHelpers $exportHelper */
        $this->exportHelper = $this->container->get('app.Helpers');
        $this->freePortalUsed = [];
    }

    protected function configure()
    {
        $this->setName('export:run')
            ->setDescription('Generate Poliris Archive for all Companys');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $output->writeln("");
        $output->writeln("------Generate Poliris Archive for all Companys-----");
        $output->writeln(date("[Y-m-d H:i:s]") . " export:run");
        $groups = $this->em->getRepository('AppBundle:Group')->getAllAgency(true);

        /**Supprimer le dossier puisqu'il peut contenir des dossiers vides **/
        $this->clearFolder($this->exportDir);
        /** Réecréer le dossier qui va contenir les fichiers exportés */
        mkdir($this->exportDir);
        /** Fin Suppression  **/


        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group) {
            if ($group->getAd()) {
                File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);
                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {

                    $portal = $portalRegistration->getBroadcastPortal();
                    if ($portal) {
                        $host = $portalRegistration->getFtpHost() ? $portalRegistration->getFtpHost() : $portal->getFtpHost();
                        $username = $portalRegistration->getFtpUser() ? $portalRegistration->getFtpUser() : $portal->getFtpUser();
                        $password = $portalRegistration->getFtpPassword() ? $portalRegistration->getFtpPassword() : $portal->getFtpPassword();

                        if (
                            $portalRegistration->getBroadcastPortal() &&
                            !$portalRegistration->getBroadcastPortal()->getDiffuseProgram() &&
                            $portalRegistration->getIdentifier() &&
                            $host && $username && $password &&
                            $portalRegistration->getIdentifier() != ''
                        ) {


                            // payant
                            if ($portalRegistration->getBroadcastPortal()->getCluster() == 'PAYANT') {
                                $this->generatePayant($portalRegistration, $group);
                            } else {
                                // gratuit
                                $this->generateGratuit($portalRegistration, $group);
                            }
                            File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);

                        }
                    }

                }

            }
        }

        //************************** Export Portails Gratuits ***************************//
        //$exportType = 'gratuit';
        File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);
        $portals = $this->em->getRepository('AppBundle:Broadcast\\Portal')->findBy([
            'cluster' => 'GRATUIT',
            'diffuseProgram' => false
        ]);
        /** @var \AppBundle\Entity\Broadcast\Portal $portal */
        foreach ($portals as $portal) {
            //check if portal is already used and remove group
            $finalGroups = $groups;
            if(isset($this->freePortalUsed[$portal->getId()])) {
                $finalGroups = [];
                foreach ($groups as $group) {
                    if (!in_array($group->getId(), $this->freePortalUsed[$portal->getId()])) {
                        $finalGroups[] = $group;
                    }
                }
            }

            $this->generateGratuit($portal, $finalGroups);

        }
        File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);

        $output->writeln("------ END -----" . gc_collect_cycles());
        $output->writeln(date("[Y-m-d H:i:s]") . " [END] - export:run");
    }

    /*** Supprimer les anciens fichiers déjà exportés ***/
    function clearFolder($path)
    {
        foreach (new DirectoryIterator($path) as $item):
            if ($item->isFile()) {
                unlink($item->getRealPath());
            }
            if (!$item->isDot() && $item->isDir()) {
                $this->clearFolder($item->getRealPath());
            }
        endforeach;
        rmdir($path);
    }


    /**
     * @param PortalRegistration $portalRegistration
     * @param Group $group
     */
    function generatePayant($portalRegistration, $group){
        $portal = $portalRegistration->getBroadcastPortal();
        $portalSlug = $portal->getSlug();
        $portalFolder = new File($portalSlug);

        if (!$portalFolder->exists()) {
            $portalFolder = File::mkDir($portalSlug, 0777, true);
        } elseif ($portalFolder->exists() && !$portalFolder->isDirectory()) {
            $portalFolder->delete();
            $portalFolder = File::mkDir($portalSlug, 0777, true);
        }
        File::cd($portalFolder->getPath());

        $ads = $this->em->getRepository('AppBundle:Ad')->getAdsByPortalDiffusion(
            $group, $portal
        );
        $this->output->writeln('Payant: ' . $group->getName() . '/'.$portal->getName().' / '.
            $portalRegistration->getIdentifier().' | Archives Process'
        );
        if ($ads && count($ads) > 0) {
            $portalSlug = $portalRegistration->getBroadcastPortal()->getSlug();
            switch ($portalRegistration->getBroadcastPortal()->getType()) {
                case 'poliris':
                    $csvHouseModelFile = null;
                    $csvFile = File::mkFile('Annonces.csv', true);
                    $csvFile->write($this->exportHelper->toPolirisCSV($ads,
                        $portalRegistration));
                    $csvFile = File::mkFile('Annonces.csv', false);
                    if (!$csvFile->isEmpty()) {


                        if ($portalRegistration->getBroadcastPortal()->getExportHouseModel()) {
                            $csvHouseModelFile = File::mkFile('Modeles.csv', true);
                            $csvHouseModelFile->write(
                                $this->exportHelper->toHouseModelPolirisCSV(
                                    $group->getHousemodel(),
                                $portalRegistration));
                            $csvHouseModelFile = File::mkFile('Modeles.csv', false);
                        }

                        $configFile = new File('Config.txt');
                        $configFile->write('Version=4.08\r\nApplication=mon-display/v2\r\nDevise=Euro');

                        $photoConfFile = new File('Photos.cfg');
                        $photoConfFile->write('Mode=FULL');

                        $fileName = $portalRegistration->getIdentifier() . '.zip';
                        $fileDir = $this->currentDir . DIRECTORY_SEPARATOR .
                            $this->exportDir . DIRECTORY_SEPARATOR . $portalSlug . DIRECTORY_SEPARATOR . $fileName;
                        $zip = new ZipArchive();
                        $zip->open($fileDir, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                        $zip->addFile($csvFile->getPath(), 'Annonces.csv');
                        $zip->addFile($configFile->getPath(), 'Config.txt');
                        $zip->addFile($photoConfFile->getPath(), 'Photos.cfg');
                        if ($csvHouseModelFile) {
                            $zip->addFile($csvHouseModelFile->getPath(), 'Modeles.csv');
                        }
                        $zip->close();


                        $configFile->delete();
                        $photoConfFile->delete();
                        if ($csvHouseModelFile) {
                            $csvHouseModelFile->delete();
                        }
                    }
                    $csvFile->delete();
                    break;
                case
                'poliris_annoncesjaunes':
                    $csvFile = File::mkFile('annoncesjaunesV2.0.csv', false);
                    $csvFile->write($this->exportHelper->toAnnoncesJaunesPolirisCSV($ads,
                        $portalRegistration));

                    if (!$csvFile->isEmpty()) {
                        $fileName = $portalRegistration->getIdentifier() . '.zip';
                        $fileDir = $this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir . DIRECTORY_SEPARATOR . $portalSlug . DIRECTORY_SEPARATOR . $fileName;

                        $zip = new ZipArchive();
                        $zip->open(
                            $fileDir,
                            ZipArchive::CREATE | ZipArchive::OVERWRITE);
                        $zip->addFile($csvFile->getPath(), 'annoncesjaunesV2.0.csv');
                        $zip->close();
                    }
                    $csvFile->delete();
                    break;
                case 'xml':
                    $fileName = $portalRegistration->getIdentifier() . '.xml';
                    $xmlFile = File::mkFile($fileName, true);
                    $data = $this->exportHelper->toXML($ads, $portalRegistration);

                    if ($data && !is_string($data)) {
                        $data = $data->asXML();
                    }
                    $xmlFile->write($data);

                    if (!$xmlFile->isEmpty()) {

                        if ($portal->getSlug() == 'logicimmo') {
                            //delete old zip
                            $zipName = $portalRegistration->getIdentifier();
                            $zip = new ZipArchive();
                            $zip->open($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir . DIRECTORY_SEPARATOR . $portalSlug . DIRECTORY_SEPARATOR . $zipName . '.zip',
                                ZipArchive::CREATE | ZipArchive::OVERWRITE);
                            $zip->addFile($xmlFile->getPath(), $fileName);
                            $zip->close();

                            $xmlFile->delete();
                        }
                    } else {
                        $xmlFile->delete();
                    }
                    break;
                case 'xml_achat_terrain':
                    $fileName = $portalRegistration->getIdentifier() . '.xml';
                    $xmlFile = File::mkFile($fileName, true);
                    $data = $this->exportHelper->toAchatTerrainXML($ads, $portalRegistration);
                    if ($data && !is_string($data)) {
                        $data = $data->asXML();
                    }
                    $xmlFile->write($data);
                    if ($xmlFile->isEmpty()) {
                        $xmlFile->delete();
                    }
                    break;
                case 'xml_ubiflow':
                    $fileName = $portalRegistration->getIdentifier() . '.xml';
                    $xmlFile = File::mkFile($fileName, true);
                    $data = $this->exportHelper->toUbiflowXML($ads, $portalRegistration);
                    if ($data && !is_string($data)) {
                        $data = $data->asXML();
                    }
                    $xmlFile->write($data);
                    if ($xmlFile->isEmpty()) {
                        $xmlFile->delete();
                    }
                    break;
                default:
                    $exportPath = $this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir . DIRECTORY_SEPARATOR . $portalSlug . DIRECTORY_SEPARATOR;
                    $fileName = $portalRegistration->getIdentifier();
                    $this->container->get('app.helpers')->dispatch($group, $portalRegistration,
                        $exportPath,
                        $fileName);
                    break;
            }
        }
        $this->output->writeln('Payant: ' . $group->getName() . '/'.$portal->getName().' Archives Archived');
        File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);
    }


    /**
     * @param $portal
     * @param $groups
     */
    function generateGratuit($portalOrPortalRegistration, $groupOrGroups)
    {
        $portal = $portalOrPortalRegistration;
        $groups = $groupOrGroups;
        $exportFileName = "";
        // if sent from agency loop
        if($portalOrPortalRegistration instanceof PortalRegistration){
            $portal = $portalOrPortalRegistration->getBroadcastPortal();
            if($portalOrPortalRegistration->getIdentifier()) {
                $exportFileName = $portalOrPortalRegistration->getIdentifier();
                $this->freePortalUsed[$portal->getId()][] = $groupOrGroups->getId();
            }
            $groups = array($groupOrGroups);
        }else // sent from portal loop
            $exportFileName = $portal->getExportFileName();


        // check if we have an exportFileName
        if ($exportFileName) {
            $portalSlug = $portal->getSlug();
            $portalFolder = new File($portalSlug);

            if (!$portalFolder->exists()) {
                $portalFolder = File::mkDir($portalSlug, 0777, true);
            } elseif ($portalFolder->exists() && !$portalFolder->isDirectory()) {
                $portalFolder->delete();
                $portalFolder = File::mkDir($portalSlug, 0777, true);
            }
            File::cd($portalFolder->getPath());
            $this->output->writeln(
                'Free: ' . $portal->getName() . '-' .
                $portal->getId() .' | '.$exportFileName. ' | Archive Process'
            );

            switch ($portal->getType()) {
                case 'poliris':
                    try {
                        $csvFile = File::mkFile('Annonces.csv', true);
                        $csvFile->write(
                            $this->exportHelper->fromGroupstoPolirisCSV($groups, $portal)
                        );
                        $csvFile = File::mkFile('Annonces.csv', false);

                        $configFile = new File('Config.txt');
                        $configFile->write(
                            'Version=4.08\r\nApplication=mon-display/v2\r\nDevise=Euro'
                        );

                        $photoConfFile = new File('Photos.cfg');
                        $photoConfFile->write('Mode=FULL');

                        $zip = new ZipArchive();
                        $zip->open(
                            $this->currentDir . DIRECTORY_SEPARATOR .
                            $this->exportDir . DIRECTORY_SEPARATOR .
                            $portalSlug . DIRECTORY_SEPARATOR .
                            $exportFileName . '.zip',
                            ZipArchive::CREATE | ZipArchive::OVERWRITE
                        );
                        $zip->addFile($csvFile->getPath(), 'Annonces.csv');
                        $zip->addFile($configFile->getPath(), 'Config.txt');
                        $zip->addFile($photoConfFile->getPath(), 'Photos.cfg');
                        $zip->close();

                        $csvFile->delete();
                        $configFile->delete();
                        $photoConfFile->delete();
                    } catch (\Exception $e) {
                        File::cd(
                            $this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir
                        );
                    }
                    break;
                case 'xml':
                    $xmlFile = File::mkFile(
                        $exportFileName . '.xml', true
                    );
                    $xmlFile->write(
                        $this->exportHelper->fromGroupstoXML($groups, $portal)
                    );

                    $zipName = $exportFileName;
                    if ($portal->getId() == 7) {
                        $date = new \DateTime('now');
                        $zipName = $exportFileName . '+' .
                            $date->format('d/m/Y') . '+' .
                            $date->format('H:m:s');
                    }

                    $zip = new ZipArchive();
                    $zip->open(
                        $this->currentDir . DIRECTORY_SEPARATOR .
                        $this->exportDir . DIRECTORY_SEPARATOR .
                        $portalSlug . DIRECTORY_SEPARATOR . $zipName . '.zip',
                        ZipArchive::CREATE | ZipArchive::OVERWRITE
                    );
                    $zip->addFile(
                        $xmlFile->getPath(), $zipName . '.xml'
                    );
                    $zip->close();

                    $xmlFile->delete();

                    break;
                case 'xml_achat_terrain':
                    $xmlFile = File::mkFile(
                        $exportFileName . '.xml', true
                    );
                    $xmlFile->write(
                        $this->exportHelper->fromGroupstoAchatTerrainXML($groups, $portal)
                    );


                    $zip = new ZipArchive();
                    $zip->open(
                        $this->currentDir . DIRECTORY_SEPARATOR .
                        $this->exportDir . DIRECTORY_SEPARATOR .
                        $portalSlug . DIRECTORY_SEPARATOR .
                        $exportFileName . '.zip',
                        ZipArchive::CREATE | ZipArchive::OVERWRITE
                    );
                    $zip->addFile(
                        $xmlFile->getPath(), $exportFileName . '.xml'
                    );
                    $zip->close();

                    $xmlFile->delete();


                    break;
                default:
                    $exportPath = $this->currentDir . DIRECTORY_SEPARATOR .
                        $this->exportDir . DIRECTORY_SEPARATOR .
                        $portalSlug . DIRECTORY_SEPARATOR;
                    $this->container->get('app.helpers')->dispatch(
                        $groupOrGroups, $portalOrPortalRegistration, $exportPath, $exportFileName
                    );
                    break;
            }
            File::cd($this->currentDir . DIRECTORY_SEPARATOR . $this->exportDir);
            $this->output->writeln('Free: ' . $portal->getName() . ' Archive Generated');
        }
    }
}