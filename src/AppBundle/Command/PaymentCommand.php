<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;

class PaymentCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('payment:check')
             ->setDescription('Daily Check for Group Payment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groups = $this->em->getRepository('AppBundle:Group')->findBy(["parentGroup" => null]);
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group) {
            if($group->getSubscribeIsEnded() && $group->getSubscribeIsEnded() <= 0 && !$group->getBlocked()) {
                $group->setBlocked(true);
                $this->em->remove($group);
                $this->em->flush();
            }
        }
    }
}