<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 13/06/17
 * Time: 00:12
 */

namespace AppBundle\Command;


use AppBundle\Entity\PendingActions;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class PendingActionsCommand extends UpdateSchemaDoctrineCommand
{
    const ACTION_LIMIT = 100;
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;
    protected $logger;
    protected $error;


    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
        $this->logger = $this->container->get('app.pending.actions.logger');
        $this->error = $this->container->get('app.pending.actions.error.logger');
    }


    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setName('pending:actions:run')
            ->setDescription('Execute pending actions');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $msg = 'Fetching pending actions : ';
        $output->writeln($msg);
        $this->logger->notice($msg);
        $paRepository = $this->em->getRepository("AppBundle:PendingActions");
        $list = $paRepository->findBy([
            'status' => PendingActions::STAND_BY_STATUS
        ],["createdAt" => "ASC"], PendingActionsCommand::ACTION_LIMIT);
        foreach ($list as $action){
            try{
                $msg = '\nID: '.$action->getId().', Service: '.$action->getService();
                $output->writeln($msg);
                $this->logger->notice($msg);
                $action->setStatus(PendingActions::PENDING_STATUS)->setUpdatedAtValue();
                $this->em->persist($action);
                $this->em->flush();
                if($this->container->has($action->getService()) && $this->container->get($action->getService())->execute($action->getArgs())){
                    $action->setStatus(PendingActions::FINISHED_STATUS)->setUpdatedAtValue();
                    $this->em->persist($action);
                    $this->em->flush();
                }
                else
                    throw new \Exception("Service undefined or wrong args for : ".$action->getService());


            }catch (\Exception $e){
                $output->writeln("[Error] ".$e->getMessage());
                $this->error->error("[Error] ".$e->getMessage());
                $action->setStatus(PendingActions::ERROR_STATUS)->setUpdatedAtValue();;
                $this->em->persist($action);
                $this->em->flush();
            }
        }
        $msg = 'Number of pending actions : '.count($list);
        $output->writeln($msg);
        $this->logger->notice($msg);

        $purged = $paRepository->purgeOldActions("-3 days");
        $msg = 'Number of pending actions purged : '.$purged;
        $output->writeln($msg);
        $this->logger->notice($msg);

    }


}