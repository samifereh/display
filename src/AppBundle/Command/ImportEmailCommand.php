<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Service\EmailsManager;
use Caponica\ImapBundle\Service\CaponicaImap;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class ImportEmailCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('import:email')
             ->setDescription('Importer les emails of today');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var CaponicaImap $caponicaImap */
        $caponicaImap = $this->container->get('caponica_imap_box');
        /** @var EmailsManager $emailManager */
        $emailManager = $this->container->get('app.service.email');
        $mailbox = $caponicaImap->getImapMailbox();
        $date = date("j F Y", time() - 1*(60 * 60 * 24));
        $mailsIds = $mailbox->searchMailbox('ON "'.$date.'"');
        foreach ($mailsIds as $id) {
            $email = $mailbox->getMail($id);
            /** @var Opportunity|null $opportunity */
            $opportunity = $emailManager->convertEmailToProposal($email);
            if($opportunity)
            {
                $output->writeln('<comment>Email : '.$email->id.'</comment>');
                $mailer  = $this->container->get('mailer');
                $message = new \Swift_Message();
                $message->setSubject('Nouvelle Opportunité')
                    ->setFrom('no-reply@display-immo.fr','Display-immo')
                    ->setTo($opportunity->getCommercial()->getEmail())
                    ->setBody(
                        $this->container->get('templating')->render(
                            'AppBundle:Emails:new_opportinity.html.twig', ['opportunity' => $opportunity]
                        ),
                        'text/html'
                    );
                $mailer->send($message);
            }
        }
    }
}