<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\Order;
use AppBundle\Repository\OrdersRepository;
use AppBundle\Service\EmailsManager;
use Caponica\ImapBundle\Service\CaponicaImap;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class GenerateInvoicesCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('generate:invoices')
             ->setDescription('Générer les Invoices Mensuel');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groups = $this->em->getRepository('AppBundle:Group')->findAll();
        /** @var OrdersRepository $orderRepository */
        $orderRepository = $this->em->getRepository('AppBundle:Payment\\Order');

        $months = [
            'January' => 'Janvier',
            'February' => 'Février',
            'March' => 'Mars',
            'April' => 'Avril',
            'May' => 'Mai',
            'June' => 'Juin',
            'July' => 'Juillet',
            'August' => 'Aout',
            'September' => 'Septembre',
            'October' => 'Octobre',
            'November' => 'Novembre',
            'December' => 'Décembre'
        ];
        $date = new \DateTime();
        $date->sub(new \DateInterval('P1M'));
        $invoiceMonth = $months[$date->format('F')];

        /** @var Group $group */
        foreach ($groups as $group) {
            $orders = $orderRepository->findBy(['group' => $group, 'factured' => false, 'status' => Order::STATUS_PAID]);
            if($orders){
                $MonthOrder = new Order();
                $MonthOrder->setGroup($group);
                $MonthOrder->setUser($orders[0]->getUser());
                $MonthOrder->setStatus(Order::STATUS_PAID);
                $MonthOrder->setExecutionDate(new \DateTime('now'));
                $details    = [];
                /** @var Order $order */
                foreach ($orders as $order) {
                    $MonthOrder->setHTTotalAmount($MonthOrder->getHTTotalAmount() + $order->getHTTotalAmount());
                    $MonthOrder->setTotalAmount($MonthOrder->getTotalAmount() + $order->getTotalAmount());
                    $details[] = $order->getDetails();

                    $order->setFactured(true);
                    $this->em->persist($order);
                }
                $MonthOrder->setDetails($details);
                $MonthOrder->setVisible(true);
                $MonthOrder->setTitle('Facture mois de '.$invoiceMonth);

                $this->em->persist($MonthOrder);
                $this->em->flush();

                $remoteId = 'immo-invoice-'.$MonthOrder->getId();
                $remoteId = md5($remoteId);
                $MonthOrder->setRemoteId($remoteId);
                $this->em->persist($MonthOrder);
                $this->em->flush();
                if($MonthOrder->getUser()) {
                    $email = $MonthOrder->getUser()->getEmail();
                    $username = $MonthOrder->getUser()->getName();
                }else{
                    $email = $MonthOrder->getGroup()->getEmailAlias();
                    $username = $MonthOrder->getGroup()->getName();
                }
                $this->container->get("app.mailer.manager")->sendNewInvoice(
                    ['email' => $email, 'username' => $username, 'invoice' => $MonthOrder]
                );
                $output->writeln('Groupe: ' .$group->getName().' - '.count($orders).' Commandes générer');
            }
        }
    }
}