<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 31/03/17
 * Time: 07:17
 */

namespace AppBundle\Command;


use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class OrderExpiryDateUpdateSchemaCommand extends UpdateSchemaDoctrineCommand
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        parent::configure();
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $return = parent::execute($input, $output);

        $orderRepository = $this->em->getRepository('AppBundle:Payment\\Order');
        $AdvantageOptionRepository = $this->em->getRepository('AppBundle:Configuration\\AdvantageOption');
        $offset = 0;
        $limit = 10;

        while ($orders = $orderRepository->findBy(["expiryDate" => null], null, $limit, $offset)) {
            /*print_r(Count($orders).'lignes trouvées');die; */
            /** @var \AppBundle\Entity\Payment\Order $order **/
            while ($order = array_shift($orders)) {
                $detail = $order->getDetails();
                if(is_array($detail) && isset($detail[0])){
                    $detail = $detail[0];
                }
                $reference = $detail['reference'];
                $months = 1;
                $advantageOption = $AdvantageOptionRepository->findOneBy(["reference" => $reference]);
                if($advantageOption) {
                    $months = $advantageOption->getMonth();
                }
                $expiryDate = $order->getExecutionDate();
                $expiryDate->modify("+{$months} month");
                $order->setExpiryDate($expiryDate);
                $this->em->persist($order);
            }
            $offset = ($offset + 1) * $limit;
            $this->em->flush();
        }
        return $return;
    }
}