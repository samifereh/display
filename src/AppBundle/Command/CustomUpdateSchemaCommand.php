<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 31/03/17
 * Time: 07:17
 */

namespace AppBundle\Command;


use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class CustomUpdateSchemaCommand extends UpdateSchemaDoctrineCommand
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        parent::configure();
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Updating Portal slug : ');
        $qb = $this->em->createQueryBuilder();
        $q = $qb->update('AppBundle:Broadcast\Portal', 'p')
            ->set('p.slug', "LOWER(p.name)")
            ->where("p.slug = ''")
            ->getQuery();
        $p = $q->execute();
        $output->writeln('Number Portal updating : '.$p);

        /**
        $output->writeln('Updating Default configuration slug : ');
        $qb = $this->em->createQueryBuilder();
        $q = $qb->update('AppBundle:Configuration\Configuration', 'c')
            ->set('c.slug', ":defaultValue")
            ->where("c.id = 1")
            ->andWhere("c.slug = ''")
            ->setParameter("defaultValue", "default")
            ->getQuery();
        $p = $q->execute();
        $output->writeln('Number configuration updating : '.$p);
**/
        return parent::execute($input, $output);
    }
}