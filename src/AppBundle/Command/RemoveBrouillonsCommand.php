<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;

class RemoveBrouillonsCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('brouillons:remove')
             ->setDescription('Daily Remove Brouillons');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $brouillons = $this->em->getRepository('AppBundle:Brouillon')->findAll();
        foreach ($brouillons as $brouillons) {
            $this->em->remove($brouillons);
        }
        $this->em->flush();
    }
}