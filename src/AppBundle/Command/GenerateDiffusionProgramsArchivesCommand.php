<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;

class GenerateDiffusionProgramsArchivesCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    protected function configure()
    {
        $this->setName('export:run-programs')
             ->setDescription('Generate Poliris Programs Archive for all Companys');
    }

    function is_dir_empty($dir) {
        if (!is_readable($dir)) return NULL;
        $handle = opendir($dir);
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                return FALSE;
            }
        }
        return TRUE;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("");
        $output->writeln("------Generate Poliris Programs Archive for all Companys-----");
        $output->writeln(date("[Y-m-d H:i:s]")." export:run-programs");

        $groups = $this->em->getRepository('AppBundle:Group')->getAllAgency(true);
        $currentDir = $this->container->get('kernel')->getRootDir().'/..';
        $exportDir  = 'web/export';
        //$exportType = 'payant';
        /** @var \AppBundle\Service\ExportHelpers $exportHelper */
        $exportHelper = $this->container->get('app.Helpers');
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group){
            if($group->getPrograms()) {
                File::cd($currentDir.DIRECTORY_SEPARATOR.$exportDir.DIRECTORY_SEPARATOR);

                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {
                    if(
                        (
                            $portalRegistration->getBroadcastPortal()->getDiffusion() ||
                            $portalRegistration->getBroadcastPortal()->getDiffuseProgram()
                        )&&
                        $portalRegistration->getIdentifier() &&
                        $portalRegistration->getBroadcastPortal()->getCluster() == 'PAYANT'
                    ) {

                        $portalSlug   = $portalRegistration->getBroadcastPortal()->getSlug();
                        $portalFolder = new File($portalSlug);

                        if(!$portalFolder->exists()) {
                            $portalFolder = File::mkDir($portalSlug,0777,true);
                        } elseif( $portalFolder->exists() && !$portalFolder->isDirectory()) {
                            $portalFolder->delete();
                            $portalFolder = File::mkDir($portalSlug,0777,true);
                        }
                        File::cd($portalFolder->getPath());

                        switch($portalRegistration->getBroadcastPortal()->getType()) {
                            case 'poliris':
                                $csvFile = File::mkFile('Programmes.csv', true);
                                $csvFile->write($exportHelper->toPolirisProgramsCSV($group->getPrograms(),$portalRegistration));
                                $csvFile = File::mkFile('Programmes.csv', false);

                                $csvLotsFile = File::mkFile('Lots.csv', true);
                                $csvLotsFile->write($exportHelper->toPolirisAdsProgramsCSV($group->getPrograms(),$portalRegistration));
                                $csvLotsFile = File::mkFile('Lots.csv', false);

                                $configFile = new File('Config.txt');
                                $configFile->write('Version=1.02\r\nApplication=mon-display/v2');

                                $photoConfFile = new File('Photos.cfg');
                                $photoConfFile->write('Mode=FULL');

                                $fileName = $portalRegistration->getIdentifier().'.zip';

                                if (!$csvFile->isEmpty()|| (!$csvLotsFile->isEmpty())) {
                                    $zip = new ZipArchive();
                                    $zip->open(
                                        $currentDir . DIRECTORY_SEPARATOR .
                                        $exportDir . DIRECTORY_SEPARATOR .
                                        $portalSlug . DIRECTORY_SEPARATOR . $fileName,
                                        ZipArchive::CREATE);
                                    if (!$csvFile->isEmpty()) {
                                        $zip->addFile(
                                            $csvFile->getPath(), 'Programmes.csv');
                                    }
                                    if (!$csvLotsFile->isEmpty()) {
                                        $zip->addFile($csvLotsFile->getPath(), 'Lots.csv');
                                    }
                                    $zip->addFile($configFile->getPath(), 'Config.txt');
                                    $zip->addFile($photoConfFile->getPath(), 'Photos.cfg');
                                    $zip->close();
                                }

                                $csvFile->delete();
                                $csvLotsFile->delete();
                                $configFile->delete();
                                $photoConfFile->delete();
                                break;
                            case 'xml_program_immoneuf':
                                $fileName = $portalRegistration->getIdentifier().'.xml';
                                $xmlFile = File::mkFile($fileName, true);
                                $xmlFile->write($exportHelper->toXMLProgramImmoneuf($group->getPrograms(),$portalRegistration));
                                if($xmlFile->isEmpty())
                                    $xmlFile->delete();
                                break;
                            case 'xml':
                                $fileName = $portalRegistration->getIdentifier().'.xml';
                                $xmlFile = File::mkFile($fileName, true);
                                $xmlFile->write($exportHelper->toXMLProgram($group->getPrograms(),$portalRegistration));
                                if($xmlFile->isEmpty())
                                    $xmlFile->delete();
                                break;
                            default:
                                break;
                        }
                        File::cd($currentDir. DIRECTORY_SEPARATOR .$exportDir .DIRECTORY_SEPARATOR);
                    }
                }
                $output->writeln('Programmes: ' .$group->getName().' Archives Generated');
                File::cd($currentDir. DIRECTORY_SEPARATOR .$exportDir.DIRECTORY_SEPARATOR);
            }
        }

        $output->writeln("------ END -----".gc_collect_cycles());
        $output->writeln(date("[Y-m-d H:i:s]")." [END] - export:run-programs");
    }
}