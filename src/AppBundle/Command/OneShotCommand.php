<?php

namespace AppBundle\Command;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Document;
use AppBundle\Entity\Gestion\OpportunityProposition;
use AppBundle\Entity\Group;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Terrain;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class OneShotCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('app:one-shot')
             ->setDescription('One shot Command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \AppBundle\Repository\HouseModelRepository $adRepository */
        $adRepository = $this->em->getRepository(Ad::class);

        /** @var \AppBundle\Repository\GroupRepository $groupRepository */
        $groupRepository = $this->em->getRepository(Group::class);

        #CLEAN Biens Immobiliers
//        $groups = $groupRepository->findAll();
//        foreach ($groups as $group) {
//            $ads = $adRepository->findBy(['group' => $group]);
//            $output->writeln(count($ads)." annonces a traiter");
//            /** @var Ad $ad */
//            foreach ($ads as $ad) {
//                $Terrain = $ad->getTerrain();
//                $Maison  = $ad->getMaison();
//                foreach ($ad->getBienImmobilier() as $bien) {
//                    if($bien->getId() != $Terrain->getId() && $bien->getId() != $Maison->getId() ) {
//                        $this->em->remove($bien);
//                    }
//                }
//                $this->em->flush();
//                $output->writeln("Annonce ".$ad->getId()." adapter");
//            }
//        }
//        exit;

        #CLEAN House models
//        /** @var \AppBundle\Repository\HouseModelRepository $houseModelRepository */
//        $houseModelRepository = $this->em->getRepository(HouseModel::class);
//        /** @var \AppBundle\Repository\Gestion\OpportunityPropositionRepository $opportunityPropositionRepository */
//        $opportunityPropositionRepository = $this->em->getRepository(OpportunityProposition::class);
//        $statement = $this->em->getConnection()->prepare(
//            'SELECT DISTINCT h3.id FROM house_model h3 INNER JOIN (SELECT h1.id FROM house_model h1, house_model h2 WHERE h1.id > h2.id AND h1.importIdentifier = h2.importIdentifier AND h1.group_id IS NOT NULL AND h1.id > 4000) dup ON h3.id = dup.id;'
//        );
//        $statement->execute();
//        $ids = [];
//        foreach ($statement->fetchAll() as $item) {
//            $ids[] = $item['id'];
//        }
//
//        $houseModels = $houseModelRepository->findBy(['id' => $ids]);
//
//        /** @var HouseModel $h */
//        foreach ($houseModels as $h) {
//            try {
//                /** @var Ad $a */
//                $this->em->remove($h);
//                $this->em->flush();
//            } catch(\Exception $e) {
//                $statement = $this->em->getConnection()->prepare( 'DELETE FROM house_model WHERE id='.$h->getId().';');
//                $statement->execute();
//            }
//        }

        $groups = $groupRepository->findAll();
        foreach ($groups as $group) {
            $ads = $adRepository->findBy(['group' => $group]);
            $output->writeln(count($ads)." annonces a traiter");
            /** @var Ad $ad */
            foreach ($ads as $ad) {
                $documentTitles = [];
                /** @var Document $document */
                foreach ($ad->getDocuments() as $document) {
                    if(in_array($document->getName(), $documentTitles)) {
                        $this->em->remove($document);
                    } else {
                        $documentTitles[] = $document->getName();
                    }
                }
                $this->em->flush();
                $output->writeln("Annonce ".$ad->getId()." cleaned");
            }
        }
    }
}