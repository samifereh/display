<?php
/**
 * Created by PhpStorm.
 * User: dream
 * Date: 13/06/17
 * Time: 00:12
 */

namespace AppBundle\Command;


use AppBundle\Entity\PendingActions;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;

class AgendaReminderCommand extends UpdateSchemaDoctrineCommand
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this->setName('agenda:reminder:run')
            ->setDescription('Send Emails to Remind Agenda Event Owner');
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \AppBundle\Service\AgendaManager $agendaReminderService */
        $agendaReminderService = $this->container->get('app.services.agenda');
        $agendaReminderService->sendAgendaRemind();
    }
}