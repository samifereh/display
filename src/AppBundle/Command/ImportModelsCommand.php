<?php

namespace AppBundle\Command;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Document;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Repository\HouseModelRepository;
use AppBundle\Repository\UserRepository;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImportModelsCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    protected function configure()
    {
        $this->setName('import:models')
             ->setDescription('Import V1 models');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("");
        $output->writeln("-----Import Models V1------START");
        $output->writeln(date("[Y-m-d H:i:s]")." import:models ");

        /** @var UserRepository $commercialRepository */
        $commercialRepository = $this->em->getRepository('AppBundle:User');

        try {
            $filename = 'Modeles_ARIA_finalises.csv';
            $currentDir = $this->container->get('kernel')->getRootDir().'/..';
            $exportDir  = 'web/export';
            $csvPath = $currentDir.DIRECTORY_SEPARATOR.
                $exportDir.DIRECTORY_SEPARATOR.$filename;
            /** @var HouseModelRepository $houseModelRepository */
            $houseModelRepository = $this->em->getRepository('AppBundle:HouseModel');
            if(!file_exists($csvPath))
                throw new NotFoundHttpException("File not found");
            $headers = [];
            $notImported = [];
            $importedCount = 0;
            if (($csvFile = fopen($csvPath, "r")) !== FALSE)
            {
                while (($data = fgetcsv($csvFile, 1000, "|")) !== false) {
                    // set headers
                    if(count($headers) == 0){
                        $headers = array_flip($data);
                    }else{
                        $output->writeln("- - - - - - - - - ");
                        $output->writeln("Try importing '".$data[$headers["str_immo_modele"]]."'");
                        $houseModel = $houseModelRepository->findOneBy([
                            'importIdentifier' => $data[$headers["id_immo_modele"]]
                        ]);
                        if(!$houseModel) {
                            $houseModel = new HouseModel();
                            $maison = new Maison();
                        }else
                            $maison = $houseModel->getMaison();

                        $output->writeln("Fetching commercial ".$data[$headers["str_email"]]."'");
                        /** @var User $commercial */
                        $commercial = $commercialRepository->findOneBy(
                            ['email' => $data[$headers["str_email"]]]);
                        if (!$commercial) {
                            $notImported[] = $data;
                            $output->writeln("Not Imported - Commercial not found");
                        } else {

                            $description = html_entity_decode(
                                $data[$headers["txt_contenu_modele_texte"]]
                            );

                            $maison->setOwner($commercial);
                            $maison->setGroup($commercial->getGroup());
                            $maison->setDescription($description);
                            $maison->setSurface($data[$headers["int_surface"]]);
                            $maison->setNbPieces($data[$headers["int_nb_piece"]]);
                            if($data[$headers["img_nom"]]) {
                                $fileName = $data[$headers["img_nom"]];
                                $fileFullPath = "http://www.commiti-immo.fr/upload/".$fileName;
                                $file = new File($fileFullPath,false);
                                $mimeType = 'image/'.$file->getExtension();
                                $currentDir = getcwd();
                                $fileDir  = 'web/uploads/housemodels/'.$file->getFilename();
                                if($fileFullPath && $this->remoteFileExists($fileFullPath)) {
                                    try {
                                        $imageString = file_get_contents("{$fileFullPath}");
                                        file_put_contents($currentDir.DIRECTORY_SEPARATOR.$fileDir,$imageString);
                                        $maison->setImagePrincipale('uploads/housemodels/'.$file->getFilename());
                                    } catch(\Exception $e) {}
                                }
                            }

                            $this->em->merge($maison);
                            $this->em->flush();


                            $houseModel->setName($data[$headers["str_immo_modele"]]);
                            $houseModel->setImportIdentifier($data[$headers["id_immo_modele"]]);
                            $houseModel->setImagePrincipale($maison->getImagePrincipale());
                            $houseModel->setMaison($maison);
                            $houseModel->setStat(HouseModel::VALIDATE);
                            $houseModel->setOwner($commercial);
                            $houseModel->setGroup($commercial->getGroup());
                            $this->em->merge($houseModel);

                            $output->writeln("Imported");
                            $importedCount++;
                        }

                    }
                }
            }
            $this->em->flush();
            $output->writeln(
                "Results Import Models : \n"
            );
            $output->writeln(
                "{$importedCount} OK \n"
            );
            $output->writeln(
                count($notImported)." NOK \n"
            );
            $output->writeln("Export not imported \n");
            $exportFile = 'notImported-'.date("Y-m-d").'.csv';
            $fp = fopen(
                $currentDir.DIRECTORY_SEPARATOR.
                $exportDir.DIRECTORY_SEPARATOR.$exportFile, 'w'
            );
            fputcsv($fp, array_flip($headers), "#");
            foreach ($notImported as $fields) {
                fputcsv($fp, $fields, "#");
            }
            fclose($fp);
            $output->writeln("- - - - - - - - - - - - - - - - - - - - \n");
        } catch(\Exception $e) {
            echo $e->getMessage();
        }

        $output->writeln("-----------".gc_collect_cycles());
        $output->writeln("-----------");
        $output->writeln(date("[Y-m-d H:i:s]")." [END] - export:import_ads ");
    }

    private function remoteFileExists($url) {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    }
}