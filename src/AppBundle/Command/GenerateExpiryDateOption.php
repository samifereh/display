<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\Payment\Order;
use AppBundle\Repository\OrdersRepository;
use AppBundle\Service\EmailsManager;
use Caponica\ImapBundle\Service\CaponicaImap;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class GenerateExpiryDateOption extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('order:expiry:run')
             ->setDescription('Notifier les membres ayant des options à dates expirées');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $groups = $this->em->getRepository('AppBundle:Group')->findAll();
        /** @var OrdersRepository $orderRepository */
        $orderRepository = $this->em->getRepository('AppBundle:Payment\\Order');

        $months = [
            'January' => 'Janvier',
            'February' => 'Février',
            'March' => 'Mars',
            'April' => 'Avril',
            'May' => 'Mai',
            'June' => 'Juin',
            'July' => 'Juillet',
            'August' => 'Aout',
            'September' => 'Septembre',
            'October' => 'Octobre',
            'November' => 'Novembre',
            'December' => 'Décembre'
        ];
        $date = new \DateTime();
        $date_courante = new \DateTime(date("Y-m-d"));
        $date->sub(new \DateInterval('P1M'));
        $invoiceMonth = $months[$date->format('F')];

        /** @var Group $group */
        foreach ($groups as $group) {
            $orders = $orderRepository->findBy(['group' => $group, 'factured' => true, 'status' => Order::STATUS_PAID]);
            if($orders){
                /** @var Order $order */
                $details    = [];
                foreach ($orders as $order) {
                    $details[] = $order->getDetails();
                    $title[] = $order->getTitle();

                $MonthOrderDetails = $details;
                $date_reference = $order->getExecutionDate();
                $interval = $date_courante->diff($date_reference);

                if($order->getUser()) {
                    $email = $order->getUser()->getEmail();
                    $username = $order->getUser()->getName();
                }else{
                    $email = $order->getGroup()->getEmailAlias();
                    $username = $order->getGroup()->getName();
                }

                if ($date_courante>$date_reference) {

                    $this->container->get("app.mailer.manager")->sendAlertPaymentExpiry(
                        ['email' => $email, 'username' => $username]
                    );
                }
                else {

                    $this->container->get("app.mailer.manager")->sendAlertPaymentExpiry(
                        ['email' => $email, 'username' => $username]
                    );
                }
                    $output->writeln('Groupe: ' .$group->getName().' - '.count($orders).' Commandes générer');

                }
            }
        }
    }
}