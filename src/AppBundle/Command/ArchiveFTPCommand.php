<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\Portal;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class ArchiveFTPCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('ftp:run')
             ->setDescription('Export all archives');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Ftp $ftp */
        $ftp = $this->container->get('ijanki_ftp');

        $output->writeln("");
        $output->writeln("------Sending FTP-----");
        $output->writeln(date("[Y-m-d H:i:s]") . " export:ftp");

        $currentDir = $this->container->get('kernel')->getRootDir().'/..';
        $exportDir  = 'web/export';
        $groups = $this->em->getRepository('AppBundle:Group')->getAllAgency(true);
        /** @var \AppBundle\Entity\Group $group */
        foreach ($groups as $group){
            if($group->getAd()) {
                File::cd($currentDir.DIRECTORY_SEPARATOR.$exportDir);
                /** @var \AppBundle\Entity\Broadcast\PortalRegistration $portalRegistration */
                foreach ($group->getBroadcastPortalRegistrations() as $portalRegistration) {

                    $portal = $portalRegistration->getBroadcastPortal();
                    $host = $portalRegistration->getFtpHost() ? $portalRegistration->getFtpHost() : $portal->getFtpHost();
                    $username = $portalRegistration->getFtpUser() ? $portalRegistration->getFtpUser() : $portal->getFtpUser();
                    $password = $portalRegistration->getFtpPassword() ? $portalRegistration->getFtpPassword() : $portal->getFtpPassword();
                    $portalSlug = $portal->getSlug();
                    $fileName = null;
                    if($portalRegistration->getIdentifier()) {
                        switch($portalRegistration->getBroadcastPortal()->getType()) {
                            case 'poliris':
                                $fileName = $portalRegistration->getIdentifier().'.zip';
                                $fileDir  = $currentDir. DIRECTORY_SEPARATOR .$exportDir. DIRECTORY_SEPARATOR .
                                    $portalSlug .DIRECTORY_SEPARATOR . $fileName;

                                break;
                            case 'poliris_annoncesjaunes':
                            case 'xml':
                            case 'xml_achat_terrain':
                            case 'xml_ubiflow':
                                $fileName = $portalRegistration->getIdentifier().'.xml';
                                $fileDir  = $currentDir. DIRECTORY_SEPARATOR .$exportDir. DIRECTORY_SEPARATOR .
                                    $portalSlug .DIRECTORY_SEPARATOR . $fileName;
                                break;
                        }

                        if($portal->getSlug() == 'logicimmo') {

                            $fileName = $portalRegistration->getIdentifier().'.zip';
                            $fileDir = $currentDir . DIRECTORY_SEPARATOR . $exportDir . DIRECTORY_SEPARATOR . $portalSlug . DIRECTORY_SEPARATOR . $fileName;
                        }
                        if($fileName) {
                            $output->writeln(date("[Y-m-d H:i:s] ") . "Sending Portal $portalSlug : $fileDir : $fileName  ");
                            try {
                                $ftp->connect($host);
                                $ftp->login($username, $password);
                                $ftp->put($fileName, $fileDir, FTP_BINARY);
                            } catch (FtpException $e) {
                                // for vivastreet can conect with host/dir... must separate
                                $host = trim($host, "/");
                                $hostParts = explode("/", $host);
                                if(count($hostParts)>1){
                                    $host = array_shift($hostParts);
                                    $chDir = implode("/",$hostParts);
                                    $output->writeln(date("[Y-m-d H:i:s] ") . "Trying passiv mode  ");
                                    try {
                                        $ftp->connect($host);
                                        $ftp->login($username, $password);
                                        $ftp->chdir($chDir);
                                        $ftp->pasv(true);
                                        $ftp->put($fileName, $fileDir, FTP_BINARY);
                                    } catch (FtpException $e) {
                                        $output->writeln(date("[Y-m-d H:i:s] ") . "<error>Portal $portalSlug : ".$e->getMessage()."</error>");
                                    }
                                }else
                                    $output->writeln(date("[Y-m-d H:i:s] ") . "<error>Portal $portalSlug : ".$e->getMessage()."</error>");
                            }
                            $ftp->close();
                        }
                    }
                }
            }
        }


        $portals = $this->em->getRepository('AppBundle:Broadcast\\Portal')->getPortalsWithFTP();
        $currentDir = $this->container->get('kernel')->getRootDir().'/..';
        $exportDir  = 'web/export';
        /** @var Portal $portal */
        foreach ($portals as $portal) {
            $host     = $portal->getFtpHost();
            $username = $portal->getFtpUser();
            $password = $portal->getFtpPassword();
            /** @var Ftp $ftp */
            $ftp = $this->container->get('ijanki_ftp');

            try {

                $ftp->connect($host);
                $ftp->login($username, $password);
                $dir = $currentDir. DIRECTORY_SEPARATOR .$exportDir.DIRECTORY_SEPARATOR.$portal->getSlug();
                if(is_dir($dir)) {
                    $files = scandir($dir);
                    if($files) {
                        foreach ($files as $file) {
                            if ($file != "." && $file != ".." && $file != ".zip") {
                                try {
                                    $ftp->put($file, $dir.DIRECTORY_SEPARATOR.$file, FTP_BINARY);
                                    $output->writeln(date("[Y-m-d H:i:s] ") . "Sent".$dir.DIRECTORY_SEPARATOR.$file);
                                } catch (FtpException $e) {
                                    $output->writeln(date("[Y-m-d H:i:s] ") . "<error>".$e->getMessage()."</error>");
                                }
                            }
                        }
                    }
                }
            } catch (FtpException $e) {
                $output->writeln(date("[Y-m-d H:i:s] ") . "<error>Portal ".$portal->getSlug()." :".$e->getMessage()."</error>");
            }
            $ftp->close();
        }
        $output->writeln("------ END -----" . gc_collect_cycles());
        $output->writeln(date("[Y-m-d H:i:s]") . " [END] - export:ftp");
    }
}