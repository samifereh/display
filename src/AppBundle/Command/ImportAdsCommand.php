<?php

namespace AppBundle\Command;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Document;
use AppBundle\Entity\HouseModel;
use AppBundle\Entity\Maison;
use AppBundle\Entity\Terrain;
use AppBundle\Entity\User;
use AppBundle\Repository\HouseModelRepository;
use AppBundle\Repository\UserRepository;
use Liip\ImagineBundle\Command\ResolveCacheCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\File;

class ImportAdsCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em = $em;
        $this->container = $container;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }

    protected function configure()
    {
        $this->setName('export:import_ads')
             ->setDescription('Import ads for all Companys');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("");
        $output->writeln("-----Import ads for all Companys------");
        $output->writeln(date("[Y-m-d H:i:s]")." export:import_ads ");

        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository('AppBundle:User');

        $groups = $this->em->getRepository('AppBundle:Group')->getAllAgency();
        /** @var UserRepository $commercialRepository */
        $commercialRepository = $this->em->getRepository('AppBundle:User');

        try {
            /** @var \AppBundle\Entity\Group $group */
            foreach ($groups as $group) {
                $filename = $group->getImportFileName();
                if($group->getImportFileName()/* && $group->getRemoteId() == "141690ed7143a6394da51f94fe5647f2"*/) {
                    $ftpDir = '/home/primmo/ftp';
                    //$ftpDir = '/var/www/html/mondisplayv2/ftp';
                    /** @var HouseModelRepository $houseModelRepository */
                    $houseModelRepository = $this->em->getRepository('AppBundle:HouseModel');
                    $j = 0;
                    if(!file_exists("{$ftpDir}/{$filename}")) continue;
                    $csvFile = file("zip://{$ftpDir}/{$filename}#Annonces.csv");

                    $output->writeln("Import Des Annonces du group : ".$group->getName()." \n");
                    $adRepository = $this->em->getRepository('AppBundle:Ad');
                    $ids = [];
                    foreach ($csvFile as $line) {
                        $line = iconv('windows-1258', 'utf-8', $line);
                        $data = str_getcsv($line,'#');
                        if (!isset($data[0]) || !$data[0] || $data[0] == '!') {
                            continue;
                        }
                        array_walk($data, function (&$value, $key) {
                            if (substr($value, -1) == '!')
                                $value = substr($value, 0, strlen($value) - 1);
                        }, $data);
                        $j++;

                        $criteria = [];
                        $refV1 = $data[1];
                        if($refV1 != "")
                            $criteria['referenceV1'] = $refV1;

                        $id = $data[174];
                        $criteria['importIdentifier'] = $id;


                        $ad = $adRepository->findOneBy($criteria);
                        unset($data[0]);
                        $i = 0;
                        $hasMaison  = false;
                        $sendEmail = false;
                        $houseModel = null;

                        if(!$ad) {
                            $ad      = new Ad();
                            $ad->setGroup($group);
                            $Maison  = new Maison();
                            $Terrain = new Terrain();
                            $ad->setImportIdentifier($id);

                            if($data[138]) {
                                $isHouseModel = $data[138];
                                $houseModel = $houseModelRepository->findOneBy(['importIdentifier' => $isHouseModel]);
                                $ad->setHouseModel($houseModel);
                            }
                            $sendEmail = true;
                        } else {
                            $Maison  = $ad->getMaison();
                            $Terrain  = $ad->getTerrain();
                            $houseModel = $ad->getHouseModel();
                            $ad->setGroup($group);
                        }
                        if($data[3] != 'terrain') {
                            $hasMaison = true;
                            if(!$Maison) { $Maison = new Maison(); }
                        }

                        /** @var User $commercial */
                        $commercial = $commercialRepository->findOneBy(['email' => $data[136]]);
                        if($commercial) {
                            $ad->setOwner($commercial);
                        }
                        $ids[] = $id;
                        $imagesTitles = [];
                        $j = 0;
                        foreach ($data as $field) {
                            $i++;
                            $value = $field;
                            if($i == 1 && $value) { $ad->setReferenceV1($value); }
                            if($i == 3 && $value) { $ad->setTypeDiffusion($value == 'terrain' ? 'terrain' : 'projet'); }
                            if($i == 4 && $value) { $ad->setCodePostal($value); }
                            if($i == 5 && $value) { $ad->setVille($value); }
                            if($i == 6 && $value) { $ad->setPays(strtolower($value) == 'france' ? 'FR': '') ; }
                            if($i == 7 && $value) { $ad->setAdresse($value); }
                            if($i == 10 && $value) { $ad->setPrix($value); }
                            if($i == 15 && $value && $hasMaison) { $Maison->setSurface($value); }
                            if($i == 16 && $value) { $Terrain->setSurface($value); }
                            if($i == 17 && $value && $hasMaison) { $Maison->setNbPieces($value); }
                            if($i == 18 && $value && $hasMaison) { $Maison->setNbChambres($value); }
                            if($i == 19 && $value) { $ad->setLibelle($value); }
                            if($i == 20 && $value) { $ad->setDescription($value); }
                            if($i == 28 && $value && $hasMaison) { $Maison->setNbSalleBain($value); }
                            if($i == 29 && $value && $hasMaison) { if($value) { $Maison->setNbSalleEau($value);$Maison->setSiSalleEau(true); }}
                            if($i == 30 && $value && $hasMaison) { if($value) { $Maison->setNbToilette($value); $Maison->setSiToilette(true); } }
                            if($i == 33 && $value && $hasMaison) { $Maison->setTypeCuisine((int)$value); }
                            if($i == 34 && $value && $hasMaison) { $Terrain->setExposition(null); }
                            if($i == 38 && $value && $hasMaison) { if($value) { $Maison->setNbBalcons($value); $Maison->setSiBalconsValue(true); } }
                            if($i == 39 && $value && $hasMaison) { if($value) { $Maison->setSurfaceBalcons($value); $Maison->setSiBalconsValue(true); } }
                            if($i == 41 && $value && $hasMaison) { $Maison->setSiCaveValue($value == 'OUI' ? true : false); }
                            if($i == 42 && $value && $hasMaison) { if($value) { $Maison->setNbParking($value); $Maison->setSiParkingValue(true); } }
                            if($i == 47 && $value && $hasMaison) { $Maison->setSiTerrasseValue($value == 'OUI' ? true : false); }
                            if($i == 67 && $value && $hasMaison) { $Maison->setSiChemineeValue($value == 'OUI' ? true : false); }
                            if($i == 72 && $value && $hasMaison) { $Maison->setSiPlacardsValue($value == 'OUI' ? true : false); }
                            if($i == 84 && $value) {
                                $file = new File($value,false);
                                $mimeType = 'image/'.$file->getExtension();
                                $currentDir = getcwd();
                                $fileDir  = 'web/uploads/ad/'.$file->getFilename();
                                if($value && $this->remoteFileExists($value)) {
                                    try {
                                        $imageString = file_get_contents("{$value}");
                                        file_put_contents($currentDir.DIRECTORY_SEPARATOR.$fileDir,$imageString);
                                        $ad->setImagePrincipale('uploads/ad/'.$file->getFilename());
                                    } catch(\Exception $e) {}
                                }
                            }
                            if(in_array($i,[85,86,87,88,89,90,91,92])) {
                                $j++;
                                $file = new File($value,false);
                                $mimeType = 'image/'.$file->getExtension();
                                $currentDir = getcwd();
                                $fileDir  = 'web/uploads/ad/'.$file->getFilename();
                                if($value && !isset($ad->getImagesDocuments()[$j])) {
                                    try {
                                        $imageString = file_get_contents($value);
                                        file_put_contents($currentDir.DIRECTORY_SEPARATOR.$fileDir,$imageString);
                                        $file = new File($currentDir.DIRECTORY_SEPARATOR.$fileDir,$imageString,false);
                                        $document = new Document();
                                        $document->setGroup($group);
                                        $document->setOriginaleFileName($file->getFilename());
                                        $document->setDocument($file);
                                        $document->setTitle($data[$i+8]);
                                        $document->setMimeType($mimeType);
                                        $ad->addDocument($document);
                                        $imagesTitles[] = $file->getFilename();
                                    } catch(\Exception $e) {}
                                }
                            }
                            if($i == 103 && $value) { $ad->setLienVisitVirtuel($value); }
                            if($i == 111 && $value) { $ad->setReferenceInterne($value); }
                            if($i == 174 && $value) { $ad->setImportIdentifier($value); }
                            if($i == 175 && $value && $hasMaison) { $Maison->setDPE($value); }
                            if($i == 176 && $value && $hasMaison) { $Maison->setDpeValue((int)$value); }
                            if($i == 177 && $value && $hasMaison) { $Maison->setGES($value); }
                            if($i == 178 && $value && $hasMaison) { $Maison->setGesValue((int)$value); }
                            if($i == 180 && $value && $hasMaison) { $Maison->setTypeArchitecture($value); }
                            if($i == 220 && $value) { if($value == 'OUI') { $Terrain->setType('terrain_agricole');} }
                            if($i == 222 && $value) { if($value == 'OUI') { $Terrain->setType('terrain_a_batir');} }
                            if($i == 243 && $value && $hasMaison) { if($value) { $Maison->setNbTerrasse($value); $Maison->setSiTerrasseValue(true); } }
                            if($i == 245 && $value && $hasMaison) { if($value == 'OUI') { $Maison->setSiSalleAMangerValue(true);  } }
                            if($i == 246 && $value && $hasMaison) { if($value == 'OUI') { $Maison->setSiSejourValue(true);  } }
                            if($i == 251 && $value && $hasMaison) { if($value) { $Maison->setSurfaceCave($value);$Maison->setSiCaveValue(true); }}
                            if($i == 252 && $value && $hasMaison) { if($value) { $Maison->setSurfaceSalleAManger($value);$Maison->setSiSalleAMangerValue(true); }}
                        }
                        if($hasMaison) { $ad->addBienImmobilier($Maison); }
                        $ad->addBienImmobilier($Terrain);
                        $ad->setStat(Ad::VALIDATE);
                        $ad->setPublished(true);

                        if(!empty($data[138]) && !$houseModel && $Maison) {

                            if($data[138]) {
                                $isHouseModel = $data[138];
                                $houseModel = $houseModelRepository->findOneBy(['importIdentifier' => $isHouseModel]);
                                $ad->setHouseModel($houseModel);
                            }

                            if(!$houseModel) {
                                $MaisonHouse = clone $Maison;
                                $houseModel = new HouseModel();
                                $houseModel->setName($data[137]);
                                $houseModel->setImagePrincipale($ad->getImagePrincipale());
                                $houseModel->setTitreImagePrincipale($ad->getTitreImagePrincipale());
                                $houseModel->setImportIdentifier($data[138]);
                                $houseModel->setMaison($MaisonHouse);
                                $houseModel->setStat(HouseModel::VALIDATE);
                                $houseModel->setGroup($group);
                                $ad->setHouseModel($houseModel);
                            }
                        }

                        $owner = null;
                        if($data[136])
                        {
                            $owner = $userRepository->findOneBy(['email' => $data[136]]);
                            if($owner) $ad->setOwner($owner);
                            if($houseModel) $houseModel->setOwner($owner);
                        }




                        $this->em->merge($ad);
                        $this->em->flush();

                        $output->writeln("Annonce {$refV1}/{$id} : ".
                            $ad->getLibelle()." \n");

                        if($ad->getImagePrincipale()) {
                            $command = $this->getApplication()->find('liip:imagine:cache:resolve');
                            $input = new ArrayInput([
                                'command' => 'liip:imagine:cache:resolve',
                                'paths' => [ $ad->getImagePrincipale() ],
                                '--filters' => ['cover_paysage']
                            ]);
                            //you are redirecting the current output to null, then no console log
                            $output1 = new NullOutput();
                            $command->run($input, $output1);
                        }
                    }
                    $updateCount = $adRepository->depublicateNotImportAds($group,$ids);

                    $output->writeln("depublicateNotImportAds : {$updateCount} | ".count($ids)." \n");
                    $output->writeln("- - - - - - - - - - - - - - - - - - - - \n");
                    $output->writeln("Group import end : ".$group->getName()." \n");
                }
            }
        } catch(\Exception $e) {
            echo $e->getMessage();
        }

        $output->writeln("-----------".gc_collect_cycles());
        $output->writeln("-----------");
        $output->writeln(date("[Y-m-d H:i:s]")." [END] - export:import_ads ");
    }

    private function remoteFileExists($url) {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    }
}