<?php

namespace AppBundle\Command;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Gestion\Campagne;
use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Statistique\CampagneStatistique;
use AppBundle\Repository\Gestion\CampagneRepository;
use AppBundle\Service\EmailsManager;
use AppBundle\Service\Mailjet;
use Caponica\ImapBundle\Service\CaponicaImap;
use Ijanki\Bundle\FtpBundle\Exception\FtpException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Entity\File;
use ZipArchive;
use Ijanki\Bundle\FtpBundle\Ftp;

class CampagneStatistiqueCommand extends Command
{
    /** @var  \Doctrine\Common\Persistence\ObjectManager $em */
    protected $em;
    protected $container;
    /** @var Mailjet $Mailjet */
    protected $Mailjet;
    /** @var CampagneRepository */
    protected $campagneRepository;

    const LIMIT = 50;

    public function __construct(ObjectManager $em, $container)
    {
        parent::__construct();
        $this->em        = $em;
        $this->container = $container;
        $this->Mailjet   = $container->get('app.mailjet');
        $this->campagneRepository = $this->em->getRepository('AppBundle:Gestion\\Campagne');
    }

    protected function configure()
    {
        $this->setName('campagne:statistique')
             ->setDescription('Import daily campagne statistiques');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $offset = 0;
        while($items = $this->fetchNextItems($offset)) {
            foreach ($items as $item) {
                if(isset($item['NewsLetterID'])) {
                    /** @var Campagne $campagne */
                    $campagne = $this->campagneRepository->findOneBy(['mailjetId' => $item['NewsLetterID']]);
                    if($campagne && !$campagne->getIsClotured()) {
                        $campagneStatistique = new CampagneStatistique();
                        if($campagne->getStatistique())
                            $campagneStatistique = $campagne->getStatistique();
                        $campagneStatistique
                            ->setBlockedCount($item['BlockedCount'])
                            ->setBouncedCount($item['BouncedCount'])
                            ->setClickedCount($item['ClickedCount'])
                            ->setDeliveredCount($item['DeliveredCount'])
                            ->setOpenedCount($item['OpenedCount'])
                            ->setProcessedCount($item['ProcessedCount'])
                            ->setQueuedCount($item['QueuedCount'])
                            ->setUnsubscribedCount($item['UnsubscribedCount'])
                            ->setSpamComplaintCount($item['SpamComplaintCount'])
                            ;
                        if($item['QueuedCount'] === 0) {$campagne->setIsClotured(true);}

                        $percent = 100*($item['ProcessedCount']/$item['DeliveredCount']);
                        $campagne->setStatistique($campagneStatistique);
                        $campagne->setPercent($percent);
                        $this->em->persist($campagne);
                        $this->em->flush();
                    }
                }
            }
            $offset+= self::LIMIT;
        }
    }

    private function fetchNextItems($offset) {
        $filters = ['Offset' => $offset,'Limit' => self::LIMIT];
        return $this->Mailjet->getCampagneStatistics($filters);
    }
}