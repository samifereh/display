<?php
// src/AppBundle/Repository/GroupRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProgramRepository extends EntityRepository
{

    public function getValidProgramCountByGroup($group){

        $qb = $this->createQueryBuilder('p');
        $qb->select('count(p.id)');
        $qb->where('p.group = :group');
        $qb->andWhere('p.stat = :stat');
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', "valider");

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getValidProgramByGroup($group){

        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->where('p.group = :group');
        $qb->andWhere('p.stat = :stat');
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', "valider");

        return $qb->getQuery()->getResult();
    }

    /**
     * reset tracked ad
     * @param $trackingPhone
     * @return mixed
     */
    public function resetTracking($trackingPhone)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update("AppBundle:Program", "p")
            ->set("p.trackingPhone", "''")
            ->where($qb->expr()->in("p.trackingPhone",$trackingPhone ))
            ->getQuery();
        return $q->execute();
    }
}