<?php
namespace AppBundle\Repository\Configuration;

use Doctrine\ORM\EntityRepository;

class ConfigurationRepository extends EntityRepository
{
    public function getConfiguration() {
        $qb = $this->createQueryBuilder('c');
        $qb->setFirstResult(0);
        $qb->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}