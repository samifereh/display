<?php
namespace AppBundle\Repository\Configuration;

use Doctrine\ORM\EntityRepository;

class AdvantageRepository extends EntityRepository
{

    public function findCurrentPayedAdvantages($group) {

        $qb = $this->_em->createQueryBuilder('ad');
        $qb->select('ad')
            ->from($this->_entityName, 'ad')
            ->leftJoin('ad.advantage','ad')
            ->where($qb->expr()->eq('ga.enabled',1))
            ->andWhere($qb->expr()->eq('ga.payed',1))
            ->andWhere($qb->expr()->eq('ad.group',':group'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('ga.dateEnd'),
                'ga.dateEnd > :now'
            )
            )
            ->setParameter('group', $group)
            ->setParameter('now', (new \DateTime('now'))->format("Y-m-d")." 00:00:00");

        return $qb->getQuery()->getSingleResult();
    }
}