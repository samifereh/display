<?php
namespace AppBundle\Repository\Configuration;

use AppBundle\Entity\Configuration\Advantage;
use AppBundle\Entity\Configuration\AdvantageOption;
use Doctrine\ORM\EntityRepository;

class AdvantageOptionRepository extends EntityRepository
{

    /**
     * @param Advantage $advantage
     * @param int $trackingCount
     * @param AdvantageOption|null $advantageOption
     * @param int $limit
     * @return array
     */
    function findNearerTrackingOptions(Advantage $advantage, $trackingCount, AdvantageOption $advantageOption = null, $limit = 6)
    {
        $qbMax = $this->createQueryBuilder('omax');

        $qbMax->select("MAX(omax.unit)")
            ->where($qbMax->expr()->eq('omax.advantage',':advantage'))
            ->andWhere($qbMax->expr()->lte('omax.unit',':unit'))
            ->setParameter('advantage', $advantage)
            ->setParameter('unit', $trackingCount);

        $qb = $this->createQueryBuilder('o');
        $qb->andWhere($qb->expr()->eq('o.advantage',':advantage'))
            ->andWhere($qb->expr()->in('o.unit',':unit'));
        if ($advantageOption) {
            $qb->andWhere($qb->expr()->eq('o.id',':option'))
                ->setParameter('option', $advantageOption);
        }

        return $qb->setParameter('advantage', $advantage)
            ->setParameter('unit', $qbMax->getQuery()->getOneOrNullResult())
            ->addOrderBy("o.price", "asc")
            ->setMaxResults($limit)
            ->getQuery()->getResult();
    }

    /**
     * @param Advantage $advantage
     * @param int $trackingCount
     * @param AdvantageOption|null $advantageOption
     * @param int $limit
     * @return array
     */
    function findMinTrackingOptions(Advantage $advantage, $trackingCount, AdvantageOption $advantageOption = null, $limit = 6)
    {
        $qbMin = $this->createQueryBuilder('omin');

        $qbMin->select("MIN(omin.unit)")
            ->where($qbMin->expr()->eq('omin.advantage',':advantage'))
            ->andWhere($qbMin->expr()->gte('omin.unit',':unit'))
            ->setParameter('advantage', $advantage)
            ->setParameter('unit', $trackingCount);

        $qb = $this->createQueryBuilder('o');
        $qb->andWhere($qb->expr()->eq('o.advantage',':advantage'))
            ->andWhere($qb->expr()->in('o.unit',':unit'));
        if ($advantageOption) {
            $qb->andWhere($qb->expr()->eq('o.id',':option'))
                ->setParameter('option', $advantageOption);
        }

        return $qb->setParameter('advantage', $advantage)
            ->setParameter('unit', $qbMin->getQuery()->getOneOrNullResult())
            ->addOrderBy("o.price", "asc")
            ->setMaxResults($limit)
            ->getQuery()->getResult();
    }

}