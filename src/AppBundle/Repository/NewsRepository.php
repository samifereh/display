<?php
// src/AppBundle/Repository/NewsRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{
    public function getNewsList()
    {

        $query = $this->createQueryBuilder('n')
                      ->orderBy('n.date', 'DESC')
                      ->getQuery();

        return $query->getResult();

    }

    public function getLatestNews($limit = 1){

        $query = $this->createQueryBuilder('n')
                      ->orderBy('n.date', 'DESC')
                      ->setMaxResults($limit)
                      ->getQuery();

        return $query->getResult();

    }

    public function countAll(){

        return $this->createQueryBuilder('n')
                    ->select('COUNT(n)')
                    ->getQuery()
                    ->getSingleScalarResult();

    }

}