<?php
// src/AppBundle/Repository/UserRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

use AppBundle\Entity\Group;

class UserRepository extends EntityRepository
{

    public function getUsersList()
    {

        $query = $this->createQueryBuilder('u')
                      ->select(array(
                            'u.id',
                            'u.username',
                            'u.name',
                            'u.email',
                            'u.lastLogin',
                            'u.roles',
                        ))
                      ->getQuery();

        return $query->getResult();

    }

    /**
     * @param string $role
     *
     * @return array
     */
    public function findByRole($role, $group = null)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"'.$role.'"%');
        if($group){
            $qb->andWhere($qb->expr()->isMemberOf(":group", "u.groups"))
                ->setParameter(":group", $group);
        }

        return $qb->getQuery()->getSingleResult();
    }


    public function getCommercialOnly($group) {

        $qb = $this->_em->createQueryBuilder();
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.groups', 'g')
            ->where($qb->expr()->orX(
                $qb->expr()->like('u.roles', ':roles'),
                $qb->expr()->like('g.roles', ':roles')
            ))
            ->andWhere(':group MEMBER OF u.groups')
            ->setParameter('roles', '%"ROLE_SALESMAN"%')
            ->setParameter('group', $group);

        return $qb->getQuery()->getResult();
    }

    public function getEmployeeCountByGroup($group) {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('count(u)')
            ->from($this->_entityName, 'u')
            ->where(':group MEMBER OF u.groups')
            ->setParameter('group', $group);
        return $qb->getQuery()->getSingleScalarResult();
    }

    public static function getUsersRoles(){
        return array('ROLE_SALESMAN', 'ROLE_BRANDLEADER', 'ROLE_MARKETING', 'ROLE_DIRECTOR', 'ROLE_ADMIN', 'ROLE_DEVELOPER');
    }

    public static function getGroupsRoles(){
        return array('ROLE_USER', 'ROLE_AGENCY');
    }

    public static function getAgencyEmployeesRoles(){
        return array('ROLE_SALESMAN', 'ROLE_BRANDLEADER');//ROLE_DIRECTOR
    }

    public static function getGroupAgencyEmployeesRoles(){
        return array('ROLE_DIRECTOR', 'ROLE_MARKETING');//ROLE_DIRECTOR
    }

}