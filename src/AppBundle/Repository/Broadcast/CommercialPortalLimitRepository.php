<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 12:03
 */

namespace AppBundle\Repository\Broadcast;


use Doctrine\ORM\EntityRepository;

class CommercialPortalLimitRepository extends EntityRepository
{
    public function getCommercialWithGroupAndPortal($portal,$user) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('SUM(u._limit) as usedLimit')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercial', 'c')
            ->where(':group MEMBER OF c.groups')
            ->andWhere($qb->expr()->eq('u.portal',$portal))
            ->andWhere($qb->expr()->neq('c.id',$user->getId()))
            ->setParameter('group', $user->getAgencyGroup());

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getCommercialProgramPortal($user,$isProgramPortal = false) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercial', 'c')
            ->leftJoin('u.portal', 'p')
            ->where(':group MEMBER OF c.groups')
            ->andWhere($qb->expr()->eq('p.diffuseProgram',$isProgramPortal ? 1 : 0))
            ->andWhere($qb->expr()->eq('c.id',$user->getId()))
            ->setParameter('group', $user->getAgencyGroup());

        return $qb->getQuery()->getResult();
    }

    public function commercialHasCreditInPortalDiffusion($user,$portal) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('l.rest')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercial', 'c')
            ->leftJoin('u.dailyAdLimit', 'l')
            ->where(':group MEMBER OF c.groups')
//            ->andWhere($qb->expr()->eq('l.day',':today'))
            ->andWhere($qb->expr()->eq('u.portal',$portal))
            ->andWhere($qb->expr()->eq('c.id',$user->getId()))
            ->setParameter('group', $user->getAgencyGroup());
//            ->setParameter('today', (new \DateTime('today'))->format('Y-m-d H:i:s'));
        try {
            $result = $qb->getQuery()->getSingleScalarResult();
            return $result && $result >= 1;
        } catch(\Doctrine\ORM\NoResultException $e) {
            return true;
        }
    }

    public function getAgencyPortalRestLimit($group,$portal) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('SUM(u._limit)')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercial', 'c')
            ->leftJoin('u.portal', 'p')
            ->where(':group MEMBER OF c.groups')
            ->andWhere($qb->expr()->eq('u.portal',$portal))
            ->setParameter('group', $group);
        return $qb->getQuery()->getSingleScalarResult();
    }
}