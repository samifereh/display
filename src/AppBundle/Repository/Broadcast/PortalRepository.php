<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 12:03
 */

namespace AppBundle\Repository\Broadcast;


use AppBundle\Entity\Group;
use Doctrine\ORM\EntityRepository;

class PortalRepository extends EntityRepository
{
    public function getPortalsWithFTP() {
        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->isNotNull('u.ftpHost'))
            ->andWhere($qb->expr()->isNotNull('u.ftpUser'))
            ->andWhere($qb->expr()->isNotNull('u.ftpPassword'))
            ->andWhere($qb->expr()->eq('u.cluster',':cluster'))
            ->setParameter('cluster',"GRATUIT");

        return $qb->getQuery()->getResult();
    }

    public function getAutorizedPortals($ids,$diffuseProgram, $diffuseTerrainOnly) {
        $qb = $this->_em->createQueryBuilder('p');
        $qb->select('p')
            ->from($this->_entityName, 'p')
            ->where('p.diffuseProgram = '.(int)$diffuseProgram)
            ->andWhere($qb->expr()->in('p.id',':ids'))->setParameter('ids',$ids);

        if(!$diffuseTerrainOnly)
            $qb->andWhere('p.diffuseTerrainOnly = '.(int)$diffuseTerrainOnly);

        return $qb->getQuery()->getResult();
    }

    public function getPortalByCluster(Group $group = null) {
        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where('u.isAvailable = 1');

        $return = [];
        $items = $qb->getQuery()->getResult();
        /** @var \AppBundle\Entity\Broadcast\Portal $item */
        foreach ($items as $item) {
            $return[$item->getCluster()][] = $item;
        }
        return $return;
    }

    public function getAvailablePortal($group = null, $agency = null){
        $qbExclude = $this->_em->createQueryBuilder('pEx');
        $qb = $this->_em->createQueryBuilder('p');
        $qbExclude->select("pEx")
            ->from($this->_entityName, 'pEx')
            ->leftJoin("pEx.broadcastPortalRegistrations", "pr");

        if( $group) {
            $qbExclude->leftJoin("pr.group", "g")
                ->andWhere(
                    $qbExclude->expr()->in("g.parentGroup", ":group")
                );
            $qb->setParameter("group", $group);
        }
        if($agency) {
            $qbExclude->andWhere(
                $qbExclude->expr()->in("pr.group", ":agency")
            );
            $qb->setParameter("agency", $agency);
        }


        $qb->select("p")
            ->from($this->_entityName, 'p')
            ->andWhere($qb->expr()->notIn("p", $qbExclude->getDQL()))
        ;


        return $qb->getQuery()->getResult();
    }
}