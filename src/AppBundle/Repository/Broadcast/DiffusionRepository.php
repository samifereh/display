<?php
namespace AppBundle\Repository\Broadcast;

use AppBundle\Entity\Broadcast\Portal;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;

class DiffusionRepository extends EntityRepository
{
    public function getPortalCommercialDailyUsed($commercial,$portal) {
//        $date    = new \DateTime('today');
//        $from = new \DateTime($date->format("Y-m-d")." 00:00:00");
//        $to   = new \DateTime($date->format("Y-m-d")." 23:59:59");

        $qb = $this->_em->createQueryBuilder('d');
        $qb->select('d.id')
            ->from($this->_entityName,'d')
            ->where($qb->expr()->eq('d.commercial',':commercial'))
//            ->andWhere('d.updatedAt BETWEEN :from AND :to')
            ->andHaving(':portal MEMBER OF d.broadcastPortals')
            ->setParameter('portal', $portal )
            ->setParameter('commercial', $commercial );
//            ->setParameter('from', $from )
//            ->setParameter('to', $to);

        return count($qb->getQuery()->getResult());
    }


    /**
     * @param User $user
     * @param Group|null $agency
     * @param Group|null $group
     * @return array|null
     */
    public function getPortalDiffusionStatistics(User $user , Group $agency = null, Group $group = null)
    {

        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('d');
        $qb->select('count(d) as score');
        $qb = $this->createQBForStatistics($qb, $user, $agency, $group);
        // get total
        $total = $qb->getQuery()->getSingleScalarResult();

        $qb->resetDQLPart("select");
        $qb->select("(100*count(d)/$total) as score, bp.name as source");
        $qb->groupBy("bp.name");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["score"];
            if($itemCount["source"])
                $result[$itemCount['source']] = number_format($itemCount["score"],2);
        }

        if(count($mixedCount))
            $result["total"] = number_format($result["total"]/count($mixedCount),2);

        return $result;
    }



    public function createQBForStatistics(QueryBuilder $qb, User $user, Group $agency = null, Group $group = null)
    {

        $qb ->leftJoin("d.broadcastPortals", "bp")
            ->andWhere("bp.cluster = 'PAYANT'");
        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('d.commercial = :owner');
            $qb->setParameter('owner', $user);
        }elseif ($agency) {
            $qb->leftJoin("d.commercial" ,"c");
            $qb->andWhere(':agency MEMBER OF c.groups');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin("d.commercial" ,"c");
            $qb->leftJoin("c.groups" ,"g");
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }


        return $qb;
    }
}