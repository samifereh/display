<?php
/**
 * Created by IntelliJ IDEA.
 * User: loic
 * Date: 08/08/16
 * Time: 12:03
 */

namespace AppBundle\Repository\Broadcast;


use AppBundle\Entity\Broadcast\AdDailyLimit;
use Doctrine\ORM\EntityRepository;

class AdDailyLimitRepository extends EntityRepository
{
    public function findByCommercialPortalDaily($commercialPortal) {
        $date    = new \DateTime('today');
        $date    = $date->format('d/m/Y');
        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercialPortal', 'c')
            ->where($qb->expr()->eq('u.commercialPortal',$commercialPortal))
            ->andWhere($qb->expr()->eq('u.day',$date));

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findByCurrentDate($commercialPortal) {
        $dateBegin = new \DateTime('today');
        $dateBegin->setTime(0,0,0);
        $dateEnd = new \DateTime('today');
        $dateEnd->setTime(23,59,59);
        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->eq('u.commercialPortal',$commercialPortal))
            ->andWhere($qb->expr()->gte('u.day', "'".$dateBegin->format("Y-m-d H:i:s")."'"))
            ->andWhere($qb->expr()->lte('u.day', "'".$dateEnd->format("Y-m-d H:i:s")."'"));

        $qb->orderBy('u.day','DESC');
        return $qb->getQuery()->getResult();
    }

    public function getUsedByGroupAndPortal($group, $portal) {
        $date    = new \DateTime('today');
        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.commercialPortal', 'cp')
            ->leftJoin('cp.commercial','c')
            ->where(':group MEMBER OF c.groups')
            ->setParameter('group', $group)
            ->andWhere($qb->expr()->eq('cp.portal',$portal))
            ->andWhere($qb->expr()->eq('u.day',':date'))
            ->setParameter('date',$date);

        $results = $qb->getQuery()->getResult();
        $usage = 0;
        /** @var AdDailyLimit $commercialUsage */
        foreach ($results as $commercialUsage) {
            $usage += $commercialUsage->getCommercialPortal()->getLimit() - $commercialUsage->getRest();
        }
        return $usage;
    }
}