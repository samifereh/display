<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:29
 */

namespace AppBundle\Repository\Broadcast;

use Doctrine\ORM\EntityRepository;

class PortalRegistrationRepository extends EntityRepository
{
    /**
     * @param $group
     * @param bool $onlyPrograms
     * @param bool|string $clusterType
     * @return array
     */
    public function getPortalRegistrationByGroup($group, $onlyPrograms = false, $clusterType=false){

        $qb = $this->createQueryBuilder('p');
        $qb->select('p');
        $qb->leftJoin('p.broadcastPortal', 'b');
        $qb->andWhere('p.group = :group');
        $qb->andWhere($qb->expr()->eq('p.isEnabled',':isEnabled'));
        $qb->andWhere($qb->expr()->eq('p.isUsed',':isUsed'));
        $qb->setParameter('group', $group);
        $qb->setParameter('isEnabled', true);
        $qb->setParameter('isUsed', true);
        if(!is_null($onlyPrograms)) {
            $qb->andWhere($qb->expr()->eq('b.diffuseProgram',':diffuseProgram'));
            $qb->setParameter('diffuseProgram', $onlyPrograms);
        }
        if(!$clusterType) {
            $qb->andWhere($qb->expr()->eq('b.cluster',':cluster'));
            $qb->setParameter('cluster', $clusterType);
        }
        return $qb->getQuery()->getResult();
    }

    public function getWaitingPortalRegistrationCount(){

        $qb = $this->createQueryBuilder('p');
        $qb->select('COUNT(p)');
        $qb->innerJoin('p.broadcastPortal', 'b');
        $qb->andWhere($qb->expr()->eq('p.isEnabled',':isEnabled'));
        $qb->setParameter('isEnabled', false);
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function resetTracking($trackingPhone)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update("AppBundle:Broadcast\PortalRegistration", "p")
            ->set("p.trackingPhone", "''")
            ->where($qb->expr()->in("p.trackingPhone",$trackingPhone ))
            ->getQuery();
        return $q->execute();
    }
}