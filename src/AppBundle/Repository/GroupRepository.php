<?php
// src/AppBundle/Repository/GroupRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GroupRepository extends EntityRepository
{
    public function getAgencyGroups($parentGroup = null) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->like('u.roles', ':roles'))
            ->andWhere($qb->expr()->eq('u.blocked',0))
            ->setParameter('roles', '%ROLE_AGENCY%');
        if($parentGroup) {
            $qb ->andWhere($qb->expr()->eq('u.parentGroup',':parentGroup'))
                ->setParameter('parentGroup', $parentGroup);
        } else {
            $qb ->andWhere($qb->expr()->isNull('u.parentGroup'));
        }
        return $qb->getQuery()->getResult();
    }

    /**
     * @param bool $hasDiffusion
     * @return array
     */
    public function getAllAgency($hasDiffusion = false) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->like('u.roles', ':roles'))
            ->andWhere($qb->expr()->eq('u.blocked',0))
            ->setParameter('roles', '%ROLE_AGENCY%');
        if($hasDiffusion){
            $qb->andWhere($qb->expr()->eq('u.diffusionOption', true));
        }
        return $qb->getQuery()->getResult();
    }

    public function getFirstAgencyGroups($parentGroup = null) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->like('u.roles', ':roles'))
            ->andWhere($qb->expr()->eq('u.blocked',0))
            ->setParameter('roles', '%ROLE_AGENCY%');
        if($parentGroup) {
            $qb->andWhere($qb->expr()->eq('u.parentGroup', ':parentGroup'))
               ->setParameter('parentGroup', $parentGroup);
        }

        $qb->getFirstResult(0);
        $qb->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param bool $withResult
     * @return array
     */
    public function findGroups($withResult = true) {

        $qb = $this->_em->createQueryBuilder('u');
        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->where($qb->expr()->like('u.roles', ':roles'))
            ->andWhere($qb->expr()->eq('u.blocked',0))
            ->setParameter('roles', '%ROLE_GROUP%');

        if(!$withResult)
            return $qb;
        return $qb->getQuery()->getResult();
    }
}