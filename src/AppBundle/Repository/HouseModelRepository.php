<?php
// src/AppBundle/Repository/HouseModelRepository.php

namespace AppBundle\Repository;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

use AppBundle\Entity\HouseModel;

class HouseModelRepository extends EntityRepository
{

	public function existsWithNameForGroup($name, $group){

	    $qb = $this->createQueryBuilder('m');
	    $qb->select('count(m.id)');
	    $qb->where('m.name = :name');
	    $qb->andWhere('m.group = :group');
	    $qb->setParameter('group', $group);
	    $qb->setParameter('name', $name);

	    return ($qb->getQuery()->getSingleScalarResult() > 0);
	}

	public function existsWithNameForGroupAndIsNot($name, $group, HouseModel $houseModel){

	    $qb = $this->createQueryBuilder('m');
	    $qb->select('count(m.id)');
	    $qb->where('m.name = :name');
	    $qb->andWhere('m.group = :group');
	    $qb->andWhere('m.id <> :houseModelId');
	    $qb->setParameter('group', $group);
	    $qb->setParameter('name', $name);
	    $qb->setParameter('houseModelId', $houseModel->getId());

	    return ($qb->getQuery()->getSingleScalarResult() > 0);
	}

	public function getHousemodelsCountByGroup($group){

	    $qb = $this->createQueryBuilder('m');
	    $qb->select('count(m.id)');
	    $qb->andWhere('m.group = :group');
	    $qb->setParameter('group', $group);

	    return $qb->getQuery()->getSingleScalarResult();
	}

	public function findHouseModels($group, $commercial = null){

	    $qb = $this->createQueryBuilder('m');
	    $qb->select('m');
	    $qb->andWhere('m.group = :group')
            ->andWhere('m.stat = :validated')
            ->setParameter('validated', HouseModel::VALIDATE);
        if($commercial && $commercial->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere($qb->expr()->orX(
                'm.visibleByAll = 1',
                'm.owner = :commercial'
            ))
            ->setParameter('commercial', $commercial);

        }
	    $qb->setParameter('group', $group);

	    return $qb->getQuery()->getResult();
	}

    public function getWaitingsModels(User $user){
        $group = $user->getGroup();
        if(!$group) {return null;}

        $qb = $this->createQueryBuilder('m');
        $qb->select('COUNT(m)');
        $qb->andWhere($qb->expr()->eq('m.stat',':stat'));
        $qb->setParameter('stat', HouseModel::PENDING);
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('m.owner = :owner');
            $qb->setParameter('owner', $user);
        } else {
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('m.group','g');
                $qb->andWhere('m.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('m.group = :group');
                $qb->setParameter('group', $group);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @param Group $agency
     * @param Group $group
     * @return array|null
     */
    public function getStatistique(User $user, Group $agency = null, Group $group = null)
    {

        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('hm');
        $qb->select("COUNT(hm) as items, m.type")
            ->leftJoin('hm.maison','m');
        $qb->andWhere('hm.published = 1');
        $qb->andWhere('hm.stat = :stat');
        $qb->setParameter('stat', HouseModel::VALIDATE);
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('hm.owner = :owner');
            $qb->setParameter('owner', $user);
        } elseif ($agency) {
            $qb->andWhere('hm.group = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('hm.group' ,'g');
            $qb->andWhere(':group MEMBER OF g.agencyGroups ');
            $qb->setParameter('group', $group);
        }
        $qb->groupBy("m.type");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["items"];
            if($itemCount["type"])
                $result[$itemCount['type']] = $itemCount["items"];
        }
        return $result;
    }
}