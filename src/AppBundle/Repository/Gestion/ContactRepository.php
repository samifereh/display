<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class ContactRepository extends EntityRepository
{
    public function findContactByEmail($email){

        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->andWhere('c.email = :email')
            ->andWhere('c.mailjetId IS NOT NULL')
            ->setParameter('email', $email);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param Group $agency
     * @param Group $group
     * @return array|null
     */
    public function getContactCountByUser(User $user, Group $agency = null, Group $group = null){

        $group = $user->getGroup();
        if(!$group) {return null;}

        $qb = $this->createQueryBuilder('c');
        $qb->select('COUNT(c)')
            ->andWhere('c.mailjetId IS NOT NULL');

        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('c.owner = :owner');
            $qb->setParameter('owner', $user);
        } elseif ($agency) {
            $qb->andWhere('c.group = :group');
            $qb->setParameter('group', $agency);
        }elseif($group) {
            $qb->leftJoin('c.group','g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

}