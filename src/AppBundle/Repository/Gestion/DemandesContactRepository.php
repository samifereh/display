<?php

namespace AppBundle\Repository\Gestion;

use Doctrine\ORM\EntityRepository;

class DemandesContactRepository extends EntityRepository
{
    public function getEventByCommercial($commercial){

        $qb = $this->createQueryBuilder('d');
        $qb->select('d')
            ->andWhere('d.commercial = :commercial')
            ->setParameter('owner', $commercial);

        return $qb->getQuery()->getResult();
    }
}