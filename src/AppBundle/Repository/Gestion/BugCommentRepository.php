<?php

namespace AppBundle\Repository\Gestion;
use AppBundle\Entity\User;

/**
 * BugCommentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BugCommentRepository extends \Doctrine\ORM\EntityRepository
{
    public function getWaitingBugMessages(User $user){

        $qb = $this->createQueryBuilder('m');
        $qb->select('m');
        $qb->andWhere($qb->expr()->eq('m.viewed',':viewed'))->setParameter('viewed',false);
        $qb->andWhere($qb->expr()->neq('m.owner',':reporter'))->setParameter('reporter',$user);
        $qb->leftJoin('m.bug','b');

        if($user->hasRole('ROLE_ADMIN') || $user->hasRole('ROLE_DEVELOPER')) {
//            $qb->andWhere($qb->expr()->eq('b.reporter',':reporter'));
        } else {
            if($user->hasRole('ROLE_SALESMAN')) {
                $qb->andWhere($qb->expr()->eq('b.owner',':reporter'));
            } else {
                $group = $user->getGroup();
                if(!$group) { return null; }

                if(count($group->getAgencyGroups())) {
                    $qb->leftJoin('b.group','g');
                    $qb->andWhere('g.parentGroup = :group');
                    $qb->setParameter('group', $group);
                } else {
                    $qb->andWhere('b.group = :group');
                    $qb->setParameter('group', $group);
                }
            }
        }

        $qb->groupBy('b.id');

        return $qb->getQuery()->getResult();
    }
}
