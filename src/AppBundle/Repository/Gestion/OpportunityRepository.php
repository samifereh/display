<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Gestion\Opportunity;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class OpportunityRepository extends EntityRepository
{
    public function getWaitingsOpportunity(User $user){
        $group = $user->getGroup();
        if(!$group) {return null;}

        $qb = $this->createQueryBuilder('m');
        $qb->select('COUNT(m)');
        $qb->andWhere($qb->expr()->eq('m.status',':status'));
        $qb->setParameter('status', Opportunity::WAITING);
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('m.commercial = :commercial');
            $qb->setParameter('commercial', $user);
        } else {
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('m.opportunityGroup','g');
                $qb->andWhere('g.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('m.opportunityGroup = :group');
                $qb->setParameter('group', $group);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @param Group|null $agency
     * @param Group|null $group
     * @param string $status
     * @return mixed|null
     */
    public function getOpportunityCountByStatus(User $user, Group $agency = null, Group $group = null, $status = Opportunity::OPEN){

        $qb = $this->createQueryBuilder('o');
        $qb->select('COUNT(o)');
        $qb->andWhere($qb->expr()->eq('o.status',':status'));
        $qb->setParameter('status', $status);
        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('o.commercial = :commercial');
            $qb->setParameter('commercial', $user);
        } elseif ($agency) {
            $qb->andWhere('o.opportunityGroup = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('o.opportunityGroup' ,'g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getOpportunityByFilter($filters,User $user, $group = null){

        $qb = $this->createQueryBuilder('opportunity');
        /** @var QueryBuilder $qb */
        $qb->leftJoin('opportunity.propositionDetails','p');
        if(!empty($filters['group']))
        {
            if($group == 'all') {
                $qb ->andWhere('opportunity.opportunityGroup IN (:group) OR opportunity.opportunityGroup = :parentGroup')
                    ->setParameter('group', $user->getGroup()->getAgencyGroups())
                    ->setParameter('parentGroup', $user->getGroup())
                    ->getQuery();
            } else {
                $qb ->leftJoin('opportunity.opportunityGroup','og');
                $qb ->andWhere('og.id = :group')
                    ->setParameter('group', $filters['group'])
                    ->getQuery();
            }
        } elseif($group && !$this->getAuthorizationChecker()->isGranted('ROLE_ADMIN')) {
            /** @var QueryBuilder $qb */
            if($group == 'all') {
                $qb ->andWhere('opportunity.opportunityGroup IN (:group) OR opportunity.opportunityGroup = :parentGroup')
                    ->setParameter('group', $user->getGroup()->getAgencyGroups())
                    ->setParameter('parentGroup', $user->getGroup())
                    ->getQuery();
            } else {
                $qb ->leftJoin('opportunity.opportunityGroup','og');
                $qb ->andWhere('og.id = :group')
                    ->setParameter('group', $filters['group'])
                    ->getQuery();
            }
        }

        if(isset($filters['contact']) && !empty($filters['contact'])) {
            $qb->leftJoin('opportunity.contact','c');
            $qb->andWhere('c.id = :contact')->setParameter('contact',$filters['contact']);
        }
        if(isset($filters['type']) && $filters['type'] == 'list') {
            $qb->andWhere('opportunity.status IN (:status)')->setParameter('status',[Opportunity::WAITING,Opportunity::OPEN]);
        }


        if (isset($filters['dateStart']) && !empty($filters['dateStart'])) {
            $dateMin = new \DateTime($filters['dateStart']['date']);
            $qb->andWhere('p.dateStart >= :dateStart')
                ->setParameter('dateStart', $dateMin)
                ->getQuery();
        }
        if (isset($filters['dateEnd']) && !empty($filters['dateEnd'])) {
            $dateMax = new \DateTime($filters['dateEnd']['date']);
            $qb->andWhere('p.dateEnd <= :dateEnd')
                ->setParameter('dateEnd', $dateMax)
                ->getQuery();
        }

        if (isset($filters['budgetTerrainFrom']) && !empty($filters['budgetTerrainFrom'])) {
            $qb->andWhere('p.budgetTerrainFrom >= :budgetTerrainFrom')
                ->setParameter('budgetTerrainFrom', $filters['budgetTerrainFrom'])
                ->getQuery();
        }
        if (isset($filters['budgetTerrainTo']) && !empty($filters['budgetTerrainTo'])) {
            $qb->andWhere('p.budgetTerrainTo <= :budgetTerrainTo')
                ->setParameter('budgetTerrainTo', $filters['budgetTerrainTo'])
                ->getQuery();
        }

        if (isset($filters['budgetMaisonFrom']) && !empty($filters['budgetMaisonFrom'])) {
            $qb->andWhere('p.budgetMaisonFrom >= :budgetMaisonFrom')
                ->setParameter('budgetMaisonFrom', $filters['budgetMaisonFrom'])
                ->getQuery();
        }
        if (isset($filters['budgetMaisonTo']) && !empty($filters['budgetMaisonTo'])) {
            $qb->andWhere('p.budgetMaisonTo <= :budgetMaisonTo')
                ->setParameter('budgetMaisonTo', $filters['budgetMaisonTo'])
                ->getQuery();
        }

        if (isset($filters['surfaceMaisonSouhaiterFrom']) && !empty($filters['surfaceMaisonSouhaiterFrom'])) {
            $qb->andWhere('p.surfaceMaisonSouhaiterFrom >= :surfaceMaisonSouhaiterFrom')
                ->setParameter('surfaceMaisonSouhaiterFrom', $filters['surfaceMaisonSouhaiterFrom'])
                ->getQuery();
        }
        if (isset($filters['surfaceMaisonSouhaiterTo']) && !empty($filters['surfaceMaisonSouhaiterTo'])) {
            $qb->andWhere('p.surfaceMaisonSouhaiterTo <= :surfaceMaisonSouhaiterTo')
                ->setParameter('surfaceMaisonSouhaiterTo', $filters['surfaceMaisonSouhaiterTo'])
                ->getQuery();
        }

        if (isset($filters['nombreChambreFrom']) && !empty($filters['nombreChambreFrom'])) {
            $qb->andWhere('p.nombreChambreFrom >= :nombreChambreFrom')
                ->setParameter('nombreChambreFrom', $filters['nombreChambreFrom'])
                ->getQuery();
        }
        if (isset($filters['nombreChambreTo']) && !empty($filters['nombreChambreTo'])) {
            $qb->andWhere('p.nombreChambreTo <= :nombreChambreTo')
                ->setParameter('nombreChambreTo', $filters['nombreChambreTo'])
                ->getQuery();
        }

        if (isset($filters['budgetTotalFrom']) && !empty($filters['budgetTotalFrom'])) {
            $qb->andWhere('p.budgetTotalFrom >= :budgetTotalFrom')
                ->setParameter('budgetTotalFrom', $filters['budgetTotalFrom'])
                ->getQuery();
        }
        if (isset($filters['budgetTotalTo']) && !empty($filters['budgetTotalTo'])) {
            $qb->andWhere('p.budgetTotalTo <= :budgetTotalTo')
                ->setParameter('budgetTotalTo', $filters['budgetTotalTo'])
                ->getQuery();
        }


        if (isset($filters['compromisTerrain']) && !empty($filters['compromisTerrain'])) {
            $qb->andWhere('p.compromisTerrain = 1')->getQuery();
        }
        if (isset($filters['contratConstruction']) && !empty($filters['contratConstruction'])) {
            $qb->andWhere('p.contratConstruction = 1')->getQuery();
        }
        if (isset($filters['permisConstruireDeposer']) && !empty($filters['permisConstruireDeposer'])) {
            $qb->andWhere('p.permisConstruireDeposer = 1')->getQuery();
        }
        if (isset($filters['possessionTerrain']) && !empty($filters['possessionTerrain'])) {
            $qb->andWhere('p.possessionTerrain = :posession')->setParameter('posession',(bool)$filters['possessionTerrain'])->getQuery();
        }

        if (isset($filters['lieuAlentour']) && !empty($filters['lieuAlentour'])) {
            $qb->andWhere('p.lieuAlentour <= :lieuAlentour')
                ->setParameter('lieuAlentour',$filters['lieuAlentour'])
                ->getQuery();
        }

        if (isset($filters['apportPersonnel']) && !empty($filters['apportPersonnel'])) {
            $qb->andWhere('p.apportPersonnel <= :apportPersonnel')
                ->setParameter('apportPersonnel',$filters['apportPersonnel'])
                ->getQuery();
        }

        if (isset($filters['lieu']) && !empty($filters['lieu'])) {
            $qb->andWhere('p.lieu LIKE :lieu')
                ->setParameter('lieu','%'.$filters['lieu'].'%')
                ->getQuery();
        }

        $qb->addOrderBy('opportunity.createdAt','DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param Group|null $agency
     * @param Group|null $group
     * @param string $status
     * @return mixed|null
     */
    public function getReceivedStatistics(User $user, Group $agency = null, Group $group = null){
        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('o');
        $qb->select('count(o) as score');
        $qb = $this->createQBForStatistics($qb, $user,Opportunity::OPEN, $agency, $group);
        // get total
        $total = $qb->getQuery()->getSingleScalarResult();


        $qb->resetDQLPart("select");
        $qb->select("(100*count(o)/$total) as score, o.source");
        $qb->groupBy("o.source");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["score"];
            if($itemCount["source"])
                $result[$itemCount['source']] = number_format($itemCount["score"],2);
        }

        if(count($mixedCount))
            $result["total"] = number_format($result["total"]/count($mixedCount),2);

        return $result;
    }

    /**
     * @param QueryBuilder $qb
     * @param User $user
     * @param null $status
     * @param Group|null $agency
     * @param Group|null $group
     * @return QueryBuilder
     */
    public function createQBForStatistics(QueryBuilder $qb, User $user, $status = null, Group $agency = null, Group $group = null)
    {

        if($status) {
            $qb->andWhere($qb->expr()->eq('o.status', ':stat'));
            $qb->setParameter('stat', $status);
        }
        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('o.commercial = :commercial');
            $qb->setParameter('commercial', $user);
        } elseif ($agency) {
            $qb->andWhere('o.opportunityGroup = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('o.opportunityGroup' ,'g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }

        return $qb;
    }

    /**
     * @param User $
     * @param Group|null $agency
     * @param Group|null $group
     * @return array|null
     */
    public function getTransformStatistics(User $user , Group $agency = null, Group $group = null)
    {

        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('o');
        $qb->select('count(o) as score');
        $qb = $this->createQBForStatistics($qb, $user, null, $agency, $group);
        // get total
        $total = $qb->getQuery()->getSingleScalarResult();

        $qb->resetDQLPart("select");
        $qb->select("(100*count(o)/$total) as score, o.source");
        $qb->andWhere($qb->expr()->isNotNull("o.contact"));
        $qb->groupBy("o.source");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["score"];
            if($itemCount["source"])
                $result[$itemCount['source']] = number_format($itemCount["score"],2);
        }

        if(count($mixedCount))
            $result["total"] = number_format($result["total"]/count($mixedCount),2);

        return $result;
    }


    /**
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     * @return array
     */
    public function getOpportunityClient($dateStart = null, $dateEnd = null){
        $qb = $this->createQueryBuilder('o');
        $qb->select('o')
            ->innerJoin("o.contact", "c")
            ->andWhere($qb->expr()->isNotNull("o.contact"));
        if($dateStart) {
            $qb->andWhere($qb->expr()->gte("c.updatedAt", ":start"));
            $qb->setParameter("start", $dateStart->format("Y-m-d") . " 00:00:00");
        }
        if($dateEnd) {
            $qb->andWhere($qb->expr()->lte("c.updatedAt", ":end"));
            $qb->setParameter("end", $dateEnd->format("Y-m-d") . " 23:59:59");
        }

        return $qb->getQuery()->getResult();
    }


}