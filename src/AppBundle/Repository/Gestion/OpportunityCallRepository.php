<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Gestion\Contact;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class OpportunityCallRepository extends EntityRepository
{
    public function getWaitingsCallOpportunity(User $user){
        $group = $user->getGroup();
        if(!$group) {return null;}

        $now = new \DateTime('now');
        $qb = $this->createQueryBuilder('o');
        $qb->select('COUNT(o)');
        $qb->andWhere($qb->expr()->gte('o.date',':date'))->setParameter('date',$now);

        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('o.owner = :commercial');
            $qb->setParameter('commercial', $user);
        } else {
            $qb->leftJoin('o.opportunity','m');
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('m.opportunityGroup','g');
                $qb->andWhere('g.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('m.opportunityGroup = :group');
                $qb->setParameter('group', $group);
            }
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getCallByContact(Contact $contact){

        $qb = $this->createQueryBuilder('c');
        $qb->select('c')
            ->leftJoin('c.opportunity','o')
            ->where('o.contact = :contact')
            ->setParameter('contact',$contact)
            ->addOrderBy('c.date','DESC')
        ;
        return $qb->getQuery()->getResult();
    }
}