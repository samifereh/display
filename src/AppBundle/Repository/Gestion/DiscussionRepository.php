<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class DiscussionRepository extends EntityRepository
{
    public function findDiscussionByUser(User $user,$page = 1){

        $qb = $this->createQueryBuilder('d');
        $qb->select('d')
            ->leftJoin('d.messages','m')
            ->where($qb->expr()->orX(':user MEMBER OF d.users', ':user MEMBER OF d.listNonVue'))
            ->andWhere(':user NOT MEMBER OF d.listWhoRemove')
            ->andWhere('d.isDraft = 0')
            ->setParameter('user',$user);

        $qb->setMaxResults(20)->setFirstResult(20*($page-1));
        return $qb->getQuery()->getResult();
    }

    public function countDiscussionByUser(User $user){

        $qb = $this->createQueryBuilder('d');
        $qb->select('count(d)')
            ->leftJoin('d.messages','m')
            ->where($qb->expr()->orX(':user MEMBER OF d.users', ':user MEMBER OF d.listNonVue'))
            ->andWhere('d.isDraft = 0')
            ->setParameter('user',$user);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findSentDiscussionByUser(User $user,$page = 1){

        $qb = $this->createQueryBuilder('d');
        $qb->select('d')
            ->leftJoin('d.messages','m')
            ->where(':user = d.sender')
            ->andWhere('d.isDraft = 0')
            ->setParameter('user',$user);

        $qb->setMaxResults(20)->setFirstResult(20*($page-1));
        return $qb->getQuery()->getResult();
    }

    public function countSentDiscussionByUser(User $user){

        $qb = $this->createQueryBuilder('d');
        $qb->select('count(d)')
            ->leftJoin('d.messages','m')
            ->where(':user = d.sender')
            ->andWhere('d.isDraft = 0')
            ->setParameter('user',$user);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findTrashDiscussionByUser(User $user,$page = 1){

        $qb = $this->createQueryBuilder('d');
        $qb->select('d')
            ->leftJoin('d.messages','m')
            ->where(':user MEMBER OF d.listWhoRemove')
            ->setParameter('user',$user);

        $qb->setMaxResults(20)->setFirstResult(20*($page-1));
        return $qb->getQuery()->getResult();
    }

    public function countTrashDiscussionByUser(User $user){

        $qb = $this->createQueryBuilder('d');
        $qb->select('count(d)')
            ->leftJoin('d.messages','m')
            ->where(':user MEMBER OF d.listWhoRemove')
            ->setParameter('user',$user);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findDraftDiscussionByUser(User $user,$page = 1){

        $qb = $this->createQueryBuilder('d');
        $qb->select('d')
            ->where('d.isDraft = 1')
            ->andWhere(':user = d.sender')
            ->setParameter('user',$user);

        $qb->setMaxResults(20)->setFirstResult(20*($page-1));
        return $qb->getQuery()->getResult();
    }

    public function countDraftDiscussionByUser(User $user){

        $qb = $this->createQueryBuilder('d');
        $qb->select('count(d)')
            ->leftJoin('d.messages','m')
            ->where('d.isDraft = 1')
            ->andWhere(':user = d.sender')
            ->setParameter('user',$user);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @param Group|null $agency
     * @param Group|null $group
     * @return mixed
     */
    public function unreadedMessages(User $user, Group $agency = null, Group $group = null){

        $qb = $this->createQueryBuilder('d');
        $qb->select('COUNT(d)')
            ->leftJoin('d.messages','m')
            ->where(':user MEMBER OF d.listNonVue')
            ->andWhere(':user NOT MEMBER OF d.listWhoRemove');

        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->setParameter('user',$user);
        }elseif ($agency) {
            $qbUser = $this->createQueryBuilder('u');
            $qbUser->from("AppBundle::User", "u");
            $qbUser->andWhere(':agency MEMBER OF u.groups');
            $qbUser->setParameter('agency', $agency);
            $qb->setParameter('user',$qbUser->getDQL());
        } elseif($group) {
            $qbUser = $this->createQueryBuilder('u');
            $qbUser->from("AppBundle::User", "u");
            $qbUser->leftJoin("u.groups" ,"g");
            $qbUser->andWhere('g.parentGroup = :group');
            $qbUser->setParameter('group', $group);
            $qb->setParameter('user',$qbUser->getDQL());
        }else
            $qb->setParameter('user',$user);
        return $qb->getQuery()->getSingleScalarResult();
    }

}