<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class OpportunityMeetingRepository extends EntityRepository
{
    public function getWaitingsMeetingsOpportunity(User $user){
        $group = $user->getGroup();
        if(!$group) {return null;}

        $now = new \DateTime('now');
        $qb = $this->createQueryBuilder('o');
        $qb->select('COUNT(o)');
        $qb->andWhere($qb->expr()->gte('o.dateStart',':date'))->setParameter('date',$now);

        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('o.commercial = :commercial');
            $qb->setParameter('commercial', $user);
        } else {
            $qb->leftJoin('o.opportunity','m');
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('m.opportunityGroup','g');
                $qb->andWhere('g.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('m.opportunityGroup = :group');
                $qb->setParameter('group', $group);
            }
        }
        return $qb->getQuery()->getSingleScalarResult();
    }
    public function getMeetingsByUser(User $user){

        $group = $user->getGroup();
        if(!$group) {return null;}

        $qb = $this->createQueryBuilder('o');
        $qb->select('o');

        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('o.commercial = :commercial');
            $qb->setParameter('commercial', $user);
        } else {
            $qb->leftJoin('o.opportunity','m');
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('m.opportunityGroup','g');
                $qb->andWhere('g.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('m.opportunityGroup = :group');
                $qb->setParameter('group', $group);
            }
        }
    }
}