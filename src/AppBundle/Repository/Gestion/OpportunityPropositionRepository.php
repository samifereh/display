<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Ad;
use Doctrine\ORM\EntityRepository;

class OpportunityPropositionRepository extends EntityRepository
{
    public function getOpportunityPropositionByAd(Ad $ad){

        $qb = $this->createQueryBuilder('o');
        $qb->select('o');
        $qb->andWhere(':ad IN o.ads');
        $qb->setParameter('ad', $ad);

        $qb->getQuery()->getResult();
    }
}