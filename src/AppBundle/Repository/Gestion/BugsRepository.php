<?php

namespace AppBundle\Repository\Gestion;

use AppBundle\Entity\Gestion\Bug;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class BugsRepository extends EntityRepository
{
    public function getWaitingBug(User $user){

        $qb = $this->createQueryBuilder('m');
        $qb->select('m');
        $qb->andWhere($qb->expr()->eq('m.status',':status'))->setParameter('status',Bug::OPEN);

        return $qb->getQuery()->getResult();
    }
}