<?php
// src/AppBundle/Repository/GroupRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GroupAdvantageRepository extends EntityRepository
{

    public function findCurrentGroupAdvantages($groups,$type='agence') {

        $qb = $this->_em->createQueryBuilder('ga');
        $qb->select('ga')
            ->from($this->_entityName, 'ga')
            ->leftJoin('ga.advantage','ad')
            ->where($qb->expr()->eq('ga.enabled',1))
            ->andWhere($qb->expr()->eq('ga.payed',1))
            ->andWhere($qb->expr()->eq('ad.groupType',':type'))
            ->andWhere($qb->expr()->in('ga.group',':groups'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('ga.dateEnd'),
                'ga.dateEnd > :now'
                )
            )
            ->setParameter('groups', $groups)
            ->setParameter('type', $type)
            ->setParameter('now', (new \DateTime('now'))->format("Y-m-d")." 00:00:00");

        return $qb->getQuery()->getResult();
    }

    public function findExistantGroupAdvantage($group, $groupAdvantage) {

        $qb = $this->_em->createQueryBuilder('ga');
        $qb->select('ga')
            ->from($this->_entityName, 'ga')
            ->where($qb->expr()->eq('ga.enabled',1))
            ->andWhere($qb->expr()->eq('ga.payed',1))
            ->andWhere($qb->expr()->eq('ga.group',':group'))
            ->andWhere($qb->expr()->eq('ga.advantage',':advantage'))
            ->setParameter('group', $group)
            ->setParameter('advantage',$groupAdvantage);

        return $qb->getQuery()->getOneOrNullResult();
    }

}