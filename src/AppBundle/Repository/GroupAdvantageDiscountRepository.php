<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GroupAdvantageDiscountRepository extends EntityRepository
{
    public function findCurrentGroupAdvantagesDiscounts($groups,$type='agence') {

        $qb = $this->_em->createQueryBuilder('d');
        $qb->select('d')
            ->from($this->_entityName, 'd')
            ->leftJoin('d.advantage','a')
            ->andWhere($qb->expr()->eq('a.groupType',':type'))
            ->andWhere($qb->expr()->in('d.group',':groups'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('d.dateBeginDiscount'),
                'd.dateBeginDiscount < :now'
                )
            )
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('d.dateEndDiscount'),
                'd.dateEndDiscount > :now'
                )
            )
            ->setParameter('groups', $groups)
            ->setParameter('type', $type)
            ->setParameter('now', (new \DateTime('now'))->format("Y-m-d")." 00:00:00");

        //add indexBy()
        $discounts = $qb->getQuery()->getResult();
        if(!$discounts)
            return null;

        $return = [];
        foreach ( $discounts as $discount ) {
            $return[$discount->getAdvantage()->getId()] = $discount;
        }
        return $return;
    }

}