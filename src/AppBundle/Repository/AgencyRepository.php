<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AgencyRepository extends EntityRepository
{

    public function resetTracking($trackingPhone)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update("AppBundle:Agency", "a")
            ->set("a.trackingPhone", "''")
            ->where($qb->expr()->in("a.trackingPhone",$trackingPhone ))
            ->getQuery();
        return $q->execute();
    }

}