<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:29
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Ad;
use AppBundle\Entity\Agency;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AdRepository extends EntityRepository
{
    public function getAdCountByGroup($group){

        $qb = $this->createQueryBuilder('m');
        $qb->select('count(m.id)');
        $qb->andWhere('m.group = :group');
        $qb->setParameter('group', $group);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getNbAdCountByProgram($program){

        $qb = $this->createQueryBuilder('m');
        $qb->select('count(m.id)');
        $qb->andWhere('m.program = :program');
        $qb->setParameter('program', $program);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getAdZipCodeList(){

        $qb = $this->createQueryBuilder('m');
        $qb->select('m.codePostal');
        $qb->distinct();
        $qb->orderBy("m.codePostal", "ASC");
        $result = [];
        foreach ($qb->getQuery()->getResult() as $item){
            $val = array_pop($item);
            $result[$val] = $val;
        }
        return $result;
    }

    public function getAdCountryList(User $user = null){

        $qb = $this->createQueryBuilder('m');
        $qb->select('m.ville');
        $qb->distinct();
        $qb->orderBy("m.ville", "ASC");
/*
        $groups = $user->getGroups();
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere($qb->expr()->in('m.owner', ':user'))->setParameter('user',$user);
        } elseif($groups && count($groups) > 0) {
            if(count($groups) > 1) {
                $qb->leftJoin('m.group','g');
                $qb->andWhere($qb->expr()->in('g.parentGroup', ':group'))->setParameter('group',$groups);
            } else {
                $qb->andWhere($qb->expr()->in('m.group', ':group'))->setParameter('group',$groups);
            }
        }
*/
        $result = [];
        foreach ($qb->getQuery()->getResult() as $item){
            $val = array_pop($item);
            $result[$val] = $val;
        }
        return $result;
    }

    public function getNbAdRestByProgram($program){

        $qb = $this->createQueryBuilder('m');
        $qb->select('count(m.id)');
        $qb->andWhere('m.program = :program');
        $qb->andWhere($qb->expr()->neq('m.stat',':stat'));
        $qb->setParameter('program', $program);
        $qb->setParameter('stat', 'vendu');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getProgramReport($program){

        $qb = $this->createQueryBuilder('m');
        $qb->select('MIN(b.surface) as minSurface, MAX(b.surface) as maxSurface, MIN(m.prix) as minPrix, MAX(m.prix) as maxPrix');
        $qb->leftJoin('m.bienImmobilier', 'b');
        $qb->andWhere('m.program = :program');
        $qb->andWhere($qb->expr()->neq('m.stat',':stat'));
        $qb->setParameter('program', $program);
        $qb->setParameter('stat', 'vendu');

        return $qb->getQuery()->getSingleResult();
    }

    public function getAdsByPortalDiffusion($group,$portal){

        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->leftJoin('a.diffusion', 'd');
        $qb->andWhere(':portal MEMBER OF d.broadcastPortals');
        $qb->andWhere('a.group = :group');
        $qb->setParameter('group', $group);
        $qb->setParameter('portal', $portal);

        return $qb->getQuery()->getResult();
    }

    /**
     * get commercial diffusion ads
     * @param $commercial
     * @param $portal
     * @return array
     */
    public function getOlderAdsByCommercial($commercial,$portal){

        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->leftJoin('a.diffusion', 'd');

        $qb->andWhere('a.owner = :commercial');
        $qb->andWhere('a.diffusion IS NOT NULL');
        $qb->andWhere(':portal MEMBER OF d.broadcastPortals');
        $qb->setParameter('commercial', $commercial);
        $qb->setParameter('portal', $portal);

        $qb->orderBy('a.dateRemonter','ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * get commercial diffusion ads count
     * @param $commercial
     * @param $portal
     * @return array
     */
    public function getOlderAdsByCommercialCount($commercial,$portal){

        $qb = $this->createQueryBuilder('a');
        $qb->select('count(a)');
        $qb->leftJoin('a.diffusion', 'd');

        $qb->andWhere('a.owner = :commercial');
        $qb->andWhere('a.published = :published');
        $qb->andWhere('a.stat = :stat');
        $qb->andWhere('a.diffusion IS NOT NULL');
        $qb->andWhere(':portal MEMBER OF d.broadcastPortals');
        $qb->setParameter('commercial', $commercial);
        $qb->setParameter('published', true);
        $qb->setParameter('stat', Ad::VALIDATE);
        $qb->setParameter('portal', $portal);

        $qb->orderBy('a.dateRemonter','ASC');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getWaitingsAds(User $user){
        $group = $user->getGroup();
        if(!$group) {return null;}

        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a)');
        $qb->andWhere($qb->expr()->eq('a.stat',':stat'));
        $qb->setParameter('stat', 'en-attente');
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('a.owner = :owner');
            $qb->setParameter('owner', $user);
        } else {
            if(count($group->getAgencyGroups())) {
                $qb->leftJoin('a.group','g');
                $qb->andWhere('g.parentGroup = :group');
                $qb->setParameter('group', $group);
            } else {
                $qb->andWhere('a.group = :group');
                $qb->setParameter('group', $group);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function depublicateNotImportAds($group,$ids){

        $qb = $this->createQueryBuilder('a');
        $qb->delete($this->_entityName, 'a');
        $qb->where(
            $qb->expr()->notIn('a.importIdentifier',':ids')
        );
        $qb->andWhere(
            $qb->expr()->isNotNull('a.importIdentifier')
        );
        $qb->andWhere('a.group = :group');
        $qb->setParameter('ids', $ids);
        $qb->setParameter('group', $group);
        return $qb->getQuery()->execute();
    }

    /**
     * get valid ads count by group for tracking
     * @param $group
     * @return mixed
     */
    public function getValidAdsCountByGroup($group){

        $qb = $this->createQueryBuilder('a');
        $qb->select('count(a.id)');
        $qb->where('a.group = :group');
        $qb->andWhere('a.stat = :stat');
        $qb->andWhere($qb->expr()->isNull('a.program'));
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', Ad::VALIDATE);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * get valid ads by group for tracking
     * @param $group
     * @return array
     */
    public function getValidAdsByGroup($group){

        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->where('a.group = :group');
        $qb->andWhere('a.stat = :stat');
        $qb->andWhere($qb->expr()->isNull('a.program'));
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', "valider");

        return $qb->getQuery()->getResult();
    }


    /**
     * get valid lots count by group for tracking
     * @param $group
     * @return mixed
     */
    public function getValidLotsCountByGroup($group){

        $qb = $this->createQueryBuilder('a');
        $qb->select('count(a.id)');
        $qb->leftJoin('a.program', 'p');
        $qb->andWhere('a.stat = :stat');
        $qb->andWhere($qb->expr()->isNotNull('a.program'));
        $qb->andWhere($qb->expr()->isNotNull('a.remoteId'));
        $qb->andWhere('p.group = :group');
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', "valider");

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * get valid lots by group for tracking
     * @param $group
     * @return array
     */
    public function getValidLotsByGroup($group){

        $qb = $this->createQueryBuilder('a');
        $qb->select('a');
        $qb->leftJoin('a.program', 'p');
        $qb->andWhere('a.stat = :stat');
        $qb->andWhere($qb->expr()->isNotNull('a.program'));
        $qb->andWhere($qb->expr()->isNotNull('a.remoteId'));
        $qb->andWhere('p.group = :group');
        $qb->setParameter('group', $group);
        $qb->setParameter('stat', "valider");

        return $qb->getQuery()->getResult();
    }

    /**
     * reset tracked ad
     * @param $trackingPhone
     * @return mixed
     */
    public function resetTracking($trackingPhone)
    {
        $qb = $this->_em->createQueryBuilder();
        $q = $qb->update("AppBundle:Ad", "a")
            ->set("a.trackingPhone", "''")
            ->where($qb->expr()->in("a.trackingPhone",$trackingPhone ))
            ->getQuery();
        return $q->execute();
    }

    /**
     * @param User $user
     * @param Group $agency
     * @param Group $group
     * @return array|null
     */
    public function getAdsStatistique(User $user, Group $agency = null, Group $group = null)
    {

        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('a');
        $qb->select('COUNT(a) as items, a.typeDiffusion');
        $qb->andWhere($qb->expr()->eq('a.stat',':stat'));
        $qb->setParameter('stat', Ad::VALIDATE);
        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('a.owner = :owner');
            $qb->setParameter('owner', $user);
        } elseif ($agency) {
            $qb->andWhere('a.group = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('a.group' ,'g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }
        $qb->groupBy("a.typeDiffusion");
        $mixedCount = $qb->getQuery()->getArrayResult();


        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["items"];
            if($itemCount["typeDiffusion"])
                $result[$itemCount['typeDiffusion']] = $itemCount["items"];
        }
        return $result;
    }


    /**
     * @param User $user
     * @param Group $agency
     * @param Group $group
     * @return array|null
     */
    public function getAdsAchivementStatistics(User $user, Group $agency = null, Group $group = null)
    {

        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('a');
        $qb->select('AVG(a.percent) as score, a.typeDiffusion');
        $qb->andWhere($qb->expr()->eq('a.stat',':stat'));
        $qb->setParameter('stat', Ad::VALIDATE);
        if($user->hasRole('ROLE_SALESMAN')) {
            $qb->andWhere('a.owner = :owner');
            $qb->setParameter('owner', $user);
        } elseif ($agency) {
            $qb->andWhere('a.group = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('a.group' ,'g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }
        $qb->groupBy("a.typeDiffusion");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["score"];
            if($itemCount["typeDiffusion"])
                $result[$itemCount['typeDiffusion']] = number_format($itemCount["score"],2);
        }
        if(count($mixedCount))
            $result["total"] = number_format($result["total"]/count($mixedCount),2);
        return $result;
    }

    /**
     * @param User $user
     * @param Group $agency
     * @param Group $group
     * @return array|null
     */
    public function getAdsDiffusionStatistics(User $user, Group $agency = null, Group $group = null)
    {
        $result = ["total" => 0];
        $qb = $this->createQueryBuilder('a');
        $qb->select('count(a) as score, a.typeDiffusion');
        $qb->andWhere($qb->expr()->eq('a.stat',':stat'));
        $qb->andWhere($qb->expr()->isNotNull('a.diffusion'));
        $qb->setParameter('stat', Ad::VALIDATE);
        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('a.owner = :owner');
            $qb->setParameter('owner', $user);
        } elseif ($agency) {
            $qb->andWhere('a.group = :agency');
            $qb->setParameter('agency', $agency);
        } elseif($group) {
            $qb->leftJoin('a.group' ,'g');
            $qb->andWhere('g.parentGroup = :group');
            $qb->setParameter('group', $group);
        }
        $qb->groupBy("a.typeDiffusion");
        $mixedCount = $qb->getQuery()->getArrayResult();

        foreach ($mixedCount as $itemCount){
            $result["total"]+= $itemCount["score"];
            if($itemCount["typeDiffusion"])
                $result[$itemCount['typeDiffusion']] = $itemCount["score"];
        }
        return $result;
    }

}