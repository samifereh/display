<?php
/**
 * Created by IntelliJ IDEA.
 * User: berse
 * Date: 18/07/2016
 * Time: 10:29
 */

namespace AppBundle\Repository\Payment;


use Doctrine\ORM\EntityRepository;

class OrdersRepository extends EntityRepository
{
    public function getCommandsCountByGroup(\AppBundle\Entity\Group $group){

        $qb = $this->createQueryBuilder('o');
        $qb->select('count(o.id)');
        $qb->where('o.isCommand = :command');
        $qb->andWhere('o.group = :group');
        $qb->setParameter('group', $group);
        $qb->setParameter('command', true);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getInvoiceCountByGroup(\AppBundle\Entity\Group $group){

        $qb = $this->createQueryBuilder('o');
        $qb->select('count(o.id)');
        $qb->where('o.isCommand = :command');
        $qb->andWhere('o.group = :group');
        $qb->setParameter('group', $group);
        $qb->setParameter('command', false);

        return $qb->getQuery()->getSingleScalarResult();
    }
}