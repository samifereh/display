<?php

namespace AppBundle\Repository\Payment;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AuthorizedAgencesRepository extends EntityRepository
{
    public function getSoonExpireAuthorization(User $user) {
        $group = $user->getGroup();
        if(!$group) {return null;}
        $date = new \DateTime('now');
        $date->add(new \DateInterval('P15D'));

        $qb = $this->createQueryBuilder('m');
        $qb->select('m');
        $qb->andWhere($qb->expr()->lte('m.endDate',':date'));
        $qb->setParameter('date', $date);

        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('m.user = :user');
            $qb->setParameter('user', $user);
        } else {
            $qb->andWhere('m.group = :group');
            $qb->setParameter('group', $group);
        }

        return $qb->getQuery()->getResult();
    }
}