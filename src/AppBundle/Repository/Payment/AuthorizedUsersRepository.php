<?php

namespace AppBundle\Repository\Payment;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class AuthorizedUsersRepository extends EntityRepository
{
    public function getEmptyAuthorizedRoles($group) {

        $qb = $this->_em->createQueryBuilder('ag');
        $qb->select('distinct(ag.role) as role')
            ->from($this->_entityName, 'ag')
            ->where($qb->expr()->isNull('ag.user'))
            ->andWhere($qb->expr()->eq('ag.group',':group'))
            ->andWhere($qb->expr()->orX(
                $qb->expr()->isNull('ag.endDate'),
                'ag.endDate > :now'
                )
            )
            ->setParameter('group', $group)
            ->setParameter('now', (new \DateTime('now'))->format("Y-m-d")." 00:00:00");
        $result = $qb->getQuery()->getResult();
        if(!$result) return null;

        $roles= [];
        foreach ($result as $autorization)
            $roles[] = $autorization['role'];
        return $roles;
    }

    public function getSoonExpireAuthorization(User $user) {
        $group = $user->getGroup();
        if(!$group) {return null;}
        $date = new \DateTime('now');
        $date->add(new \DateInterval('P15D'));

        $qb = $this->createQueryBuilder('m');
        $qb->select('m');
        $qb->andWhere($qb->expr()->lte('m.endDate',':date'));
        $qb->setParameter('date', $date);

        if($user->hasRole('ROLE_SALESMAN') && !$user->hasRole('ROLE_BRANDLEADER')) {
            $qb->andWhere('m.user = :user');
            $qb->setParameter('user', $user);
        } else {
            $qb->andWhere('m.group = :group');
            $qb->setParameter('group', $group);
        }

        return $qb->getQuery()->getResult();
    }
}