<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Group;
use AppBundle\Service\NotificationManager;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

/**
 * Class GeneralExtension
 * @package Bundle\FrontBundle\Twig
 */
class AppTwigExtension extends \Twig_Extension
{
    private $configurationRepository;
    private $adRepository;
    private $user;
    private $notificationManager;
    public function __construct($em, $token, NotificationManager $notificationManager)
    {
        $this->configurationRepository  = $em->getRepository('AppBundle:Configuration\\Configuration');
        $this->adRepository             = $em->getRepository('AppBundle:Ad');
        $this->user                     = $token ? $token->getUser() : null;
        $this->notificationManager      = $notificationManager;
    }

    public function getNotifications(){
        if(!$this->user || $this->user === 'anon.') { return null; }
        return $this->notificationManager->getNotifications();
    }
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            'truncate'    => new \Twig_Filter_Method( $this, 'twig_truncate_filter' ),
            new \Twig_SimpleFilter('country', array($this, 'countryFilter'))
        );
    }

    public function getTests()
    {
        return [
            'instanceof' =>  new \Twig_Function_Method($this, 'isInstanceof')
        ];
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()

    {
        return [];
    }

    public function getGlobals()
    {
        return array(
            "configuration" => $this->configurationRepository->getConfiguration(),
            "notifications" => $this->getNotifications(),
        );
    }
    /**
     * @param        $value
     * @param int    $length
     * @param bool   $preserve
     * @param string $separator
     *
     * @return string
     */
    function twig_truncate_filter( $value, $length = 30, $preserve = false, $separator = '...')
    {
        if (mb_strlen($value)) {
            if ($preserve) {
                if (false === ($breakpoint = mb_strpos($value, ' ',$length))) {
                    return $value;
                }
                $length = $breakpoint;
            }
            $_separator= '';
            if(rtrim(mb_strlen($value) > $length)) {
                $_separator = $separator;
            }
            return rtrim(mb_substr($value, 0, $length)).$_separator;
        }
        return $value;
    }

    public function countryFilter($countryCode,$locale = "en"){
        $c = \Symfony\Component\Locale\Locale::getDisplayCountries($locale);

        return array_key_exists($countryCode, $c)
            ? $c[$countryCode]
            : $countryCode;
    }

    /**
     * @param $var
     * @param $instance
     * @return bool
     */
    public function isInstanceof($var, $instance) {
        return  $var instanceof $instance;
    }

    /**
     * Returns the name of the extension.
     * @return string The extension name
     */
    public function getName()
    {
        return 'app_twig_extension';
    }

}