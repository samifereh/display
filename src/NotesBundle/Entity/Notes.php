<?php

namespace NotesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Notes.
 *
 * @ORM\Table(name="notes")
 * @ORM\Entity(repositoryClass="NotesBundle\Repository\NotesRepository")
 */

class Notes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\ManyToMany(targetEntity="NotesLabels", inversedBy="Notes")
     * @ORM\JoinTable(name="notes_noteslabels",
     *      joinColumns={@ORM\JoinColumn(name="notes_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="notes_labels_id", referencedColumnName="id")}
     *      )
     */
    private $noteslabels;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /** 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="notes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
     protected $userId;

    public function __construct()
    {
        $this->noteslabels = new ArrayCollection();
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
    }

    public function setNotesLabels(NotesLabels $noteslabels)
    {
        $this->noteslabels = $noteslabels;
    }

    public function getNotesLabels()
    {
        return $this->noteslabels;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Notes
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Notes
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Notes
     */
    public function setUserId(\AppBundle\Entity\User $userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Add noteslabels.
     *
     * @param \NotesBundle\Entity\NotesLabels $noteslabels
     *
     * @return Notes
     */
    public function addNotesLabels(\NotesBundle\Entity\NotesLabels $noteslabels)
    {
        $this->noteslabels[] = $noteslabels;

        return $this;
    }

    /**
     * Remove noteslabels.
     *
     * @param \NotesBundle\Entity\NotesLabels $noteslabels
     */
    public function removeNotesLabels(\NotesBundle\Entity\NotesLabels $noteslabels)
    {
        $this->noteslabels->removeElement($noteslabels);
    }


}
