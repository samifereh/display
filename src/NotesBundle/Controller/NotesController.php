<?php

namespace NotesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use NotesBundle\Entity\Notes;
use NotesBundle\Entity\NotesLabels;
use NotesBundle\Repository\NotesLabelsRepository as NotesLabelsRepository;
use NotesBundle\Repository\NotesRepository as NotesRepository;

class NotesController extends Controller
{  
    public function addAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        /*Code to get all the labels start*/
            $finalUserNotesLabels = array();
            $notesLabels = $em->getRepository('NotesBundle:NotesLabels')->findAll();
            foreach ($notesLabels as $key => $value) {
                $finalUserNotesLabels[$value->getId()] = $value->getTitle();
            }
        /*Code to get all the labels end  */

        
        $notes = new Notes();
        $form = $this->createFormBuilder($notes)
            ->add('title', 'text', array('attr' => array(
                                      'required' => true
                                  )))
            ->add('description', 'textarea', array('attr' => array(
                                      'required' => true
                                  )))
            ->getForm();
        

        $data["html"] = $this->render('NotesBundle:Notes:notes.html.twig',array('labels' => $finalUserNotesLabels,'action' => 'add','form' => $form->createView()))->getContent();
        $data["labels"] = $finalUserNotesLabels;
        return new Response(json_encode($data));
    }

    protected function removeDeletedLabels($existingNotesLabels,$labelsId, $notes) 
    {
        foreach ($existingNotesLabels as $key => $value) {
            if(!in_array($value->getId(), $labelsId)) {
                $notes->removeNotesLabels($value);
            }
        }
    }

    protected function addMissingLabels($existingNotesLabels, $key, &$notes)
    {
        foreach ($existingNotesLabels as $key => $value) {
            if($value->getId() !=  $key) {
                $notes->addNotesLabels($value);
            }
        }
    }


    public function saveAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $parsedData = array();
        parse_str($data['data'], $parsedData);
        $labelsId = array();
        $notes = new Notes();

        if(!empty($parsedData['edit_note_id'])) {
            $notes = $em->getRepository('NotesBundle:Notes')->findOneBy(array('id' => $parsedData['edit_note_id'], 'userId' => $userId));
            if(!empty($notes->getId())) {
                $existingNotesLabels = $notes->getNotesLabels();
                $existingLabelIds = array();
                foreach ($existingNotesLabels as $existingNotesLabel) {
                    $existingLabelIds[$existingNotesLabel->getId()] = $existingNotesLabel;
                }
            }
        }


        $labels = isset($data['labels']) ? $data['labels'] : array();


        foreach ($labels as $key => $value) {
            if(is_numeric($key)) {
                $labelsId[] = $key;
                if(!empty($notes->getId())) {
                    if(!isset($existingLabelIds[$key])) {
                        $notesLabels = $em->getRepository('NotesBundle:NotesLabels')->findOneBy(array('id' => $key));        
                        $notes->addNotesLabels($notesLabels);
                    }
                }
            } else {
                $notesLabels = $em->getRepository('NotesBundle:NotesLabels')->findOneBy(array('title' => $value));
                if(empty($notesLabels)) {
                    $notesLabels = new NotesLabels();
                    $notesLabels->setTitle($value);
                    $em->persist($notesLabels);
                    $em->flush();

                    $notes->addNotesLabels($notesLabels);
                }
                if(!empty($notesLabels)) {
                    $labelsId[] = $notesLabels->getId();
                }
            }
        }


        if(!empty($notes->getId())) {
            foreach ($existingNotesLabels as $key => $value) {
                if(!in_array($value->getId(), $labelsId)) {
                    $notes->removeNotesLabels($value);
                }
            }
        }

        $notes->setTitle($parsedData['form']['title']);
        $notes->setDescription($parsedData['form']['description']);
        $notes->setUserId($this->getUser());
        $em->persist($notes);
        $em->flush();

        return new Response(json_encode($data));
    }

    protected function getIteratedResult($results)
    {
        $userNotes = array();
        foreach ($results as $key => $notes) {
            foreach ($notes as $columnName => $value) {
                if($columnName == "label_title") {
                    $userNotes[$key]["labels"] = "";
                    if(!empty($value)) {
                        $notesLabels = explode(",", $value);
                        foreach ($notesLabels as $notesLabel) {
                            $labels = explode("|", $notesLabel);
                            $userNotes[$key]["labels"][$labels[0]] = $labels[1];
                        }
                    }
                } else {
                    $userNotes[$key][$columnName] = $value;
                }
            }
        }
        return $userNotes;
    }

    public function listAction(Request $request)
    {

        $data               = $request->request->all();
        $userId             = $this->getUser()->getId();
        $em                 = $this->getDoctrine()->getEntityManager();
        $query              = $em->createQueryBuilder();
        $currentPage        = isset($data["cur_page"])?$data["cur_page"]:1;
        $searchKeyword      = isset($data["search_key"])?$data["search_key"]:"";
        $notesPerPage       = isset($data["notes_per_page"])?$data["notes_per_page"]:5;
        $order              = isset($data["orderby"])?$data["orderby"]:"ASC";
        $column             = isset($data["column"])?$data["column"]:"n.id";
        $offset             = ($currentPage * $notesPerPage) - $notesPerPage;
        $start              = $offset+1;

        $query
            ->select("n","nl","GROUP_CONCAT(nl.id,'|',nl.title) AS label_title")
            ->from('NotesBundle:Notes', 'n')
            ->leftJoin("n.noteslabels", "nl")
            ->where('n.userId = :uid')
            ->setParameter('uid', $userId)
            ->groupBy('n.id')
            ->orderBy($column,$order);

        if(!empty($searchKeyword)) {
            $query
            ->andWhere('n.title LIKE :searchKeyword or nl.title LIKE :searchKeyword')
            ->setParameter('searchKeyword', '%'.$searchKeyword."%");
        } 
        
        $results            = $query->getQuery()->getScalarResult();
        $queryTotalNotes    = $this->getIteratedResult($results);
        $totalNotes = count($queryTotalNotes);

        $query
            ->setFirstResult($offset)
            ->setMaxResults($notesPerPage);

        
        $results    = $query->getQuery()->getScalarResult();
        $userNotes  = $this->getIteratedResult($results);
        $end    = $offset + count($userNotes);

        if($request->isXmlHttpRequest()) {
            $notesData = array();
            $notesData['html']  =   $this->render('NotesBundle:Notes:ajaxlistnotes.html.twig',array(
                                        'notes'=> $userNotes,
                                        'totalnotes'=> $totalNotes
                                    ))->getContent();
            $notesData['start'] =   $start;
            $notesData['end']   =   $end;
            
            return new Response(json_encode($notesData));
        } else {
            return $this->render('NotesBundle:Notes:listnotes.html.twig',array(
                'notes'=>$userNotes,
                'totalnotes'=> $totalNotes,
            ));
        }
    }

    public function viewAction(Request $request)
    {
        $userId     = $this->getUser()->getId();
        $notesPost  = $request->request->all();
        $em         = $this->getDoctrine()->getManager();
        $query      = $em->createQueryBuilder();

        $query
        ->select("n","nl","GROUP_CONCAT(nl.id,'|',nl.title) AS label_title")
        ->from('NotesBundle:Notes', 'n')
        ->leftJoin("n.noteslabels", "nl")
        ->where('n.userId = :uid')
        ->setParameter('uid', $userId)
        ->andWhere('n.id = :noteId')
        ->setParameter('noteId', $notesPost['id'])
        ->groupBy('n.id');

        $results    = $query->getQuery()->getScalarResult();
        $notes      = $this->getIteratedResult($results);

        $data["html"] = $this->render('NotesBundle:Notes:notes.html.twig',array(
                            'notes' => $notes,
                            'action' => 'view',
                        ))->getContent();

        return new Response(json_encode($data));
    }

    public function editAction(Request $request)
    {
        $userId     = $this->getUser()->getId();
        $notesPost  = $request->request->all();
        $em         = $this->getDoctrine()->getManager();
        $query      = $em->createQueryBuilder();

        $query
            ->select("n","nl","GROUP_CONCAT(nl.id,'|',nl.title) AS label_title")
            ->from('NotesBundle:Notes', 'n')
            ->leftJoin("n.noteslabels", "nl")
            ->where('n.userId = :uid')
            ->setParameter('uid', $userId)
            ->andWhere('n.id = :noteId')
            ->setParameter('noteId', $notesPost['id'])
            ->groupBy('n.id');

        $results    = $query->getQuery()->getScalarResult();
        $notes      = $this->getIteratedResult($results);

        /*Code to get all the labels start*/
            $finalUserNotesLabels = array();
            $notesLabels = $em->getRepository('NotesBundle:NotesLabels')->findAll();
            foreach ($notesLabels as $key => $value) {
                $finalUserNotesLabels[$value->getId()] = $value->getTitle();
            }
        /*Code to get all the labels end  */

        $notesForm = $em->getRepository('NotesBundle:Notes')->findOneById($notesPost['id']);
        $form = $this->createFormBuilder($notesForm)
            ->add('title', 'text', array('attr' => array(
                                      'required' => true
                                  )))
            ->add('description', 'textarea', array('attr' => array(
                                      'required' => true
                                  )))
            ->getForm();

        $data["html"] = $this->render('NotesBundle:Notes:notes.html.twig',array(
                            'notes' => $notes,
                            'action' => 'edit',
                            'form' => $form->createView()
                        ))->getContent();

        $data["labels"] = $finalUserNotesLabels;
        return new Response(json_encode($data));
    }

    public function removeAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        $notesPost = $request->request->all();
        $em = $this->getDoctrine()->getManager();

        $notes = $em->getRepository('NotesBundle:Notes')->findOneBy(array('id' => $notesPost['id'], 'userId' => $userId));

        if(!empty($notes)) {
            $em->remove($notes);
            $em->flush();
        }

        return new Response(json_encode(""));
    }

}
