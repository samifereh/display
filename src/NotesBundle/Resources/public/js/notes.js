/* Global variable labels set on add and edit actions */
var labels = "";

/************* ALL FUNCTIONS START*************/

    /******* Notes Bundle functions start *****/
        function renderNotesList(showLoader)
        {
            showLoader = false;   
            var notesUrl = Routing.generate('list_notes','',true);
            var notesPerPage    = $("#notes-per-page").val();
            var searchKey       = $("#search-keyword").val();
            var curPage         = $("#current-note-page").html();
            var isSortingExist  = $("[sorted='yes']").length;
            var column          = "n.id";
            var order           = "asc";
            if(isSortingExist > 0) {
                column = $("[sorted='yes']").attr('sort-by');
                order  = $("[sorted='yes']").attr('order-by');
            }

            $.ajax({
                method: "POST",
                url: notesUrl,
                data: {
                    cur_page: curPage,
                    search_key: searchKey,
                    notes_per_page: notesPerPage,
                    column: column,
                    orderby: order,
                },
                dataType: "json",
                beforeSend: function() {
                    if(showLoader) {
                        $("#loader-background").show();
                    }
                },
                success: function(result) {
                    $('#wrapper #notes-list-rows').html(result.html);
                    $('#note-start').html(result.start);
                    $('#note-end').html(result.end);
                    $('.total-notes').html($('#total-notes').val());
                    if(showLoader) {
                        $("#loader-background").hide();
                    }
                }
            });
        }

        function viewNotes(id)
        {
            var viewNoteUrl = Routing.generate('view_notes','',true);
            $.ajax({
                method: "POST",
                url: viewNoteUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function(result) {
                    if($("#popup-wrapper").length) {
                        $("#popup-wrapper").remove();
                    }
                    $("#notes-content").append(result.html);
                    //$("#loader-background").hide();
                    $(".popup1").trigger("click");
                }
            });
        }

        function editNotes(id)
        {
            var editNoteUrl = Routing.generate('edit_notes','',true);
            $.ajax({
                method: "POST",
                url: editNoteUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function(result) {
                    if($("#popup-wrapper").length) {
                        $("#popup-wrapper").remove();
                    }
                    $("#notes-content").append(result.html);
                    labels = result.labels;
                    //$("#loader-background").hide();
                    $(".popup1").trigger("click");
                }
            });
        }

        function deleteNotes(id) 
        {
            var deleteNoteUrl = Routing.generate('remove_notes','',true);
            $.ajax({
                method: "POST",
                url: deleteNoteUrl,
                dataType: "json",
                data: {
                        id: id,
                    },
                beforeSend: function() {
                    //$("#loader-background").show();
                },
                success: function() {
                    $("#current-note-page").html('1');
                    renderNotesList(true);
                }
            });   
        }
    /******* Notes Bundle functions end *******/


$("document").ready(function(){

    /********** Notes Bundle start ***********/
        
        /********** Add Notes start ***********/
            $("body").on("click","#add_note",function(){
                var addNotesUrl = Routing.generate('add_notes','',true);
                $.ajax({
                    method: "POST",
                    url: addNotesUrl,
                    dataType: "json",
                    beforeSend: function() {
                        //$("#loader-background").show();
                    },
                    success: function(result) {
                        if($("#popup-wrapper").length) {
                            $("#popup-wrapper").remove();
                        }
                        $("#notes-content").append(result.html);
                        labels = result.labels;
                        //$("#loader-background").hide();
                        $(".popup1").trigger("click");
                    }
                });
            });
        /********** Add Notes end   ***********/

        /********** Save Notes start ***********/
        	$("body").on("submit","#note-form",function(event){
        		event.preventDefault();
        		var saveNotesUrl = Routing.generate('save_notes','',true);
        		var alllabels ={};

        		var newid=1;
        		$("[data-label]").each(function() {
        			if(typeof($(this).attr("id")) != "undefined") {
        				var tmp = $(this).attr("id");
        				alllabels[tmp] = $(this).attr("data-label");
        			} else {
        				var tmp = "new_"+newid;
        				alllabels[tmp] = $(this).attr("data-label");
        			}
        			newid++;
        		});

        		$.ajax({
                    method: "POST",
                    url: saveNotesUrl,
                    data: {
                        labels: alllabels,
                        data: $("#note-form").serialize()
                    },
                    dataType: "json",
                    beforeSend: function() {
                        /*$('#openModal').show();*/
                    },
                    success: function(result) {
                        $.fancybox.close();
                        $("#current-note-page").html('1');
                        renderNotesList(true);
                    }
                });
        	});
        /********** Save Notes end   ***********/

        /********** Suggest similar labels start ***********/
        	$("body").on("keyup","#searcher",function(){
        		var html ="";
        		if($(this).val().trim()) {
                	html  = "<li>"+$(this).val()+"</li>";
            		for(key in labels) {
            			var label = labels[key].toLowerCase();
            			var searchval = $(this).val().toLowerCase();
            			if(label != searchval) {
            				var result = label.indexOf(searchval)>=0;
            				if(result) {
            					if($("li[data-label='"+labels[key]+"']").length < 1) {
            						html=html+"<li id='"+key+"'>"+labels[key]+"</li>";
            					}
            				}
            			}
            		}
                } 
                $("#temp-tags").html(html);
            });
        /********** Suggest similar labels end   ***********/

        /********** Add suggested label to textbox start ***********/
            $("body").on("click","#temp-tags li",function(){
                
                if($("li[data-label='"+$(this).html()+"']").length < 1) {
                    $("#searcher").parent('li').remove();
                    var html = $("#tags").html();
                    var id="";
                    if($(this).attr('id')) {
                        id = " id='"+$(this).attr('id')+"'";
                    }
                	html  = html+"<li class='select2-search-choice' data-label='"+$(this).html()+"'"+id+"><div>"+$(this).html()+"</div><a tabindex='-1' class='select2-search-choice-close remove-label' href='javascript:void(0);'>del</a></li><li><input id='searcher' type='text' autocapitalize='off' autocorrect='off' autocomplete='off' placeholder='Label'></input></li>";
                    $("#tags").html(html);
                } 
                $("#searcher").val("");
                $("#searcher").focus();
                $("#temp-tags").html("");
            });
        /********** Add suggested label to textbox end   ***********/

        /********** Remove labels start ***********/
            $("body").on("click",".remove-label",function(){
                $(this).parent().remove();
                $("#searcher").focus();
            });
        /********** Remove labels end ***********/

        /********** Observe view,edit and delete from listing and popup start ***********/
            $("body").on("click","#notes-content [data-action],.fancybox-wrap .note-view [data-action]",function(){
                var id = $(this).attr("data-id");
                var action = $(this).attr("data-action");

                switch(action) {
                    case "view": viewNotes(id); break;
                    case "edit": editNotes(id); break;
                    case "delete": deleteNotes(id); break;
                }
            });
        /********** Observe view,edit and delete from listing and popup end *************/

        /********** Fancybox close start ***********/
            $("body").on("click",".fancybox-wrap [data-dismiss='modal']",function(){
                $.fancybox.close();
            });
        /********** Fancybox close end *************/

        /********** Pagination notes start ***********/
            $("body").on("change","#notes-per-page",function(){
                $("#current-note-page").html('1')
                renderNotesList(false);
            });
        /********** Pagination notes end *************/

        /********** Search notes start *************/
            $("body").on("keyup","#notes-content #search-keyword",function(){
                $("#current-note-page").html('1');
                renderNotesList(false);
            });
        /********** Search notes end ***************/

        /********** Notes Page navigation to next page start *************/
            $("body").on("click","#note-table_next",function(){
                var notesPerPage    = $("#notes-per-page").val();
                
                var curPage         = $("#current-note-page").html();
                var totalNotes      = $("#total-notes").val();
                var lastPage        = totalNotes / notesPerPage;
                
                if(curPage < lastPage) {
                    curPage             = parseInt(curPage) + 1; 

                    $("#current-note-page").html(curPage);

                    renderNotesList(false);
                }
            });
        /********** Notes Page navigation to next page end ***************/

        /********** Notes Page navigation to previous page start *************/
            $("body").on("click","#note-table_previous",function(){
                var curPage         = $("#current-note-page").html();
                if(curPage != 1) {
                    if(curPage > 1) {
                        curPage = parseInt(curPage) - 1; 
                    }

                    if (curPage <= 1) {
                        $(this).attr('disabled','true');
                    }
                    
                    $("#current-note-page").html(curPage);

                    renderNotesList(false);
                }  
            });
        /********** Notes Page navigation to previous page end ***************/

        /********** Sorting notes start *************/
            $("body").on("click","#notes-content .sorting",function(){
                var order           = "";

                if($(this).attr('order-by') == "asc") {
                   order = "desc";
                } else {
                   order = "asc";
                }

                $(".sorting").each(function(){
                    $(this).attr('sorted',"");
                    $(this).attr('order-by',"");
                });

                $(this).attr('order-by',order);
                $(this).attr('sorted',"yes");

                renderNotesList(true);
            });
        /********** Sorting notes end ***************/

        /********** Fancybox open start ***********/
            $(".popup1").fancybox({

                maxWidth   : 600,
                fitToView   : true,
                autoSize    : false,
                showCloseButton: true,
                autoHeight  : true,
                padding     : [0,0,0,0],
                margin      : [15,15,15,15],
                openSpeed   : 'fast',
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });            
        /********** Fancybox open end *************/

    /********** Notes Bundle end *************/

});